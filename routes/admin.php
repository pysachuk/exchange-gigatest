<?php

use Illuminate\Support\Facades\Route;

Route::get('/', [\App\Http\Controllers\Admin\MainController::class, 'main'])->name('main');

Route::prefix('coins/')->name('coins.')->group(function (){
    Route::get('available', \App\Http\Livewire\Admin\Coins\AvailableCoins::class)->name('available');
    Route::get('all', \App\Http\Livewire\Admin\Coins\All::class)->name('all');
    Route::get('gaps', \App\Http\Livewire\Admin\Coins\Gaps::class)->name('gaps');
});

Route::get('/orders', \App\Http\Livewire\Admin\ExchangeApplicationsList::class)->name('orders.list');
Route::get('/orders/{id}', \App\Http\Livewire\Admin\ExchangeApplicationView::class)->name('orders.view');

Route::name('settings.')->prefix('settings/')->group(function (){
    Route::get('/localization', \App\Http\Livewire\Admin\LocalizationList::class)->name('localization');
    Route::get('/main', \App\Http\Livewire\Admin\Settings::class)->name('main');
});


Route::name('pages.')->prefix('pages/')->group(function (){
    Route::get('/{key}', [\App\Http\Controllers\Admin\Pages\EditPageController::class, 'edit'])->name('edit');
    Route::post('/{key}', [\App\Http\Controllers\Admin\Pages\EditPageController::class, 'store'])->name('store');
});

Route::get('faq/list', [\App\Http\Controllers\Admin\FaqController::class, 'list'])->name('faq.list');
Route::get('faq/create', [\App\Http\Controllers\Admin\FaqController::class, 'create'])->name('faq.create');
Route::post('faq/question/store', [\App\Http\Controllers\Admin\FaqController::class, 'store'])->name('faq.store');
Route::get('faq/question/{question}/edit', [\App\Http\Controllers\Admin\FaqController::class, 'edit'])->name('faq.question.edit');
Route::put('faq/question/{question}/update', [\App\Http\Controllers\Admin\FaqController::class, 'update'])->name('faq.question.update');
Route::delete('faq/question/{question}', [\App\Http\Controllers\Admin\FaqController::class, 'delete'])->name('faq.question.delete');

