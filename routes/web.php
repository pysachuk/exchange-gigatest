<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', [\App\Http\Controllers\MainController::class, 'test']);

Route::localizedGroup(function () {

        Route::get('/', \App\Http\Livewire\Exchange::class)->name('main');
        Route::get('/order/{uuid}', \App\Http\Livewire\ViewApplication::class)
            ->name('order.view');

        Route::get('/logout', [\App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');

        Route::get('/support', [\App\Http\Controllers\PageController::class, 'support'])->name('support');
        Route::get('/reserve', [\App\Http\Controllers\PageController::class, 'reserve'])->name('reserve');
        Route::get('/crypto-check', [\App\Http\Controllers\PageController::class, 'cryptoCheck'])
            ->middleware('auth')
            ->name('crypto-check');
        Route::get('/partner', [\App\Http\Controllers\PageController::class, 'partner'])->name('partner');
        Route::get('/faq', [\App\Http\Controllers\PageController::class, 'faq'])->name('faq');
        Route::get('/history', [\App\Http\Controllers\PageController::class, 'history'])->middleware('auth')->name('history');
        Route::get('/privacy', [\App\Http\Controllers\PageController::class, 'privacy'])->name('privacy');
        Route::get('/terms', [\App\Http\Controllers\PageController::class, 'terms'])->name('terms');

        Auth::routes();
});
