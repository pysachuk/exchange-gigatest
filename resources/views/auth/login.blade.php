@extends('layouts.main')

@section('content')
        <div class="reg_login-page login-page">
            <div class="reg_login-form">
                <h2 class="text-header">{{ __('site.login') }}</h2>
                <form method="post" action="{{ route('login') }}" class="form-validate">
                    <div>
                        @csrf
                        <div class="field">
                            <input  type="text" name="login" value="{{ old('login') }}" required class="@error('login') is-invalid @enderror">
                            <div class="placeholder">{{ __('site.login') }}</div>
                            @error('login')
                            <span id="login-error" class="is-invalid invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="field">
                            <input type="password" name="password" required class="@error('password') is-invalid @enderror">
                            <div class="placeholder">{{  __('site.password') }}</div>
                            @error('password')
                            <span id="login-error" class="is-invalid invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
{{--                        <p class="recovery"><a href="recovery.html">Забыли пароль?</a></p>--}}
                    </div>
                    <div>
                        <p class="form_terms">protected by reCAPTCHA<a href="https://policies.google.com/privacy?hl=en" target="_blank" rel="noopener noreferrer">Privacy</a><a target="_blank" rel="noopener noreferrer" href="https://policies.google.com/terms?hl=en">Terms</a></p>
                        <button type="submit" class="btn-submit"><span> {{  __('site.login') }}</span></button>
                    </div>
                    <input type="hidden" name="user_auth" value="1">
                </form>
                <p class="enter-button">{{  __('site.not_registered') }} <a href="{{ route('register') }}">{{  __('site.register') }}</a></p>
            </div>
            <div class="reg_login-desc">
                <img src="/img/d/account.svg" alt="">
                {!! __('site.register_text') !!}
            </div>
        </div>
@endsection
