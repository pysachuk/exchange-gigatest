@extends('layouts.main')

@section('content')
    <div class="reg_login-page register-page">
        <div class="reg_login-form">
            <h2 class="text-header">{{ __('site.register') }}</h2>
            <form method="post"  action="{{ route('register') }}" class="form-validate">
                @csrf
                <div class="field">
                    <input value="{{ old('email') }}" type="email" name="email" required class="@error('email') is-invalid @enderror">
                    <div class="placeholder">Email</div>
                    @error('email')
                        <span id="email-error" class="is-invalid invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>
                <div class="field">
                    <input value="{{ old('login') }}" type="text" name="login" class="valid_username @error('login') is-invalid @enderror" minlength="3" required>
                    <div class="placeholder">{{ __('site.login') }}</div>
                    @error('login')
                    <span id="email-error" class="is-invalid invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>
                <div class="field">
                    <input type="password" class="@error('password') is-invalid @enderror" name="password" id="field_pass" minlength="6" required>
                    <div class="placeholder">{{  __('site.password') }}</div>
                    @error('password')
                    <span id="email-error" class="is-invalid invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>
                <div class="field">
                    <input type="password" name="password_confirmation" class="valid_pass @error('password') is-invalid @enderror" required>
                    <div class="placeholder">{{  __('site.repeat_password') }}</div>
                    @error('password')
                    <span id="email-error" class="is-invalid invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>
                <div>
                    <p class="form_terms">protected by reCAPTCHA<a href="https://policies.google.com/privacy?hl=en" target="_blank" rel="noopener noreferrer">Privacy</a><a target="_blank" rel="noopener noreferrer" href="https://policies.google.com/terms?hl=en">Terms</a></p>
                    <button type="submit" class="btn-submit"><span> {{  __('site.register') }}</span></button>
                </div>
            </form>
            <p class="enter-button">{{ __('site.already_account') }} <a href="{{ route('login') }}">{{  __('site.login') }}</a></p>
        </div>
        <div class="reg_login-desc">
            <img src="img/d/account.svg" alt="">
            {!! __('site.register_text') !!}
        </div>
    </div>
{{--<div class="container">--}}
{{--    <div class="row justify-content-center">--}}
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">{{ __('Register') }}</div>--}}

{{--                <div class="card-body">--}}
{{--                    <form method="POST" action="{{ route('register') }}">--}}
{{--                        @csrf--}}

{{--                        <div class="row mb-3">--}}
{{--                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>--}}

{{--                                @error('name')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="row mb-3">--}}
{{--                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">--}}

{{--                                @error('email')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="row mb-3">--}}
{{--                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">--}}

{{--                                @error('password')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="row mb-3">--}}
{{--                            <label for="password-confirm" class="col-md-4 col-form-label text-md-end">{{ __('Confirm Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="row mb-0">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    {{ __('Register') }}--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
@endsection
