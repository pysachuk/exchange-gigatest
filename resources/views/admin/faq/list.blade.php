@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                FAQ
            </div>
        </div>
        <div class="card-body">
            @forelse($questions as $question)
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs" id="myTab{{ $question->id }}" role="tablist">
                            @foreach(localization()->getSupportedLocalesKeys() as $locale)
                                <li class="nav-item">
                                    <a class="nav-link @if($locale === localization()->getCurrentLocale()) active  @endif" id="{{$locale}}-{{ $question->id }}-tab" data-toggle="tab" href="#tab-{{$locale}}-{{$question->id}}" role="tab" aria-controls="{{$locale}}-{{ $question->id }}" >{{ $locale }}</a>
                                </li>
                            @endforeach
                        </ul>
                        <div class="tab-content" id="myTab{{ $question->id }}Content">
                            @foreach(localization()->getSupportedLocalesKeys() as $locale)
                                <div class="tab-pane fade show @if($locale === localization()->getCurrentLocale()) active  @endif" id="tab-{{$locale}}-{{ $question->id }}" role="tabpanel" aria-labelledby="{{$locale}}-{{ $question->id }}-tab">
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="card-title">
                                                <h5>{{ $question->translate($locale)->question ?? '' }} ({{ $locale }})</h5>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            {!! $question->translate($locale)->answer ?? '' !!}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-2">
                                <a class="btn btn-primary btn-sm" href="{{ route('admin.faq.question.edit', ['question' => $question]) }}">Редактировать</a>

                            </div>
                            <div class="col-1">
                                <form method="post" action="{{ route('admin.faq.question.delete', ['question' => $question]) }}">
                                    @csrf
                                    @method('delete')
                                    <button onclick="return confirm('Вы уверены?')" class="btn btn-danger btn-sm" type="submit">Удалить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            @empty
                Пусто.. Создайте вопрос FAQ
            @endforelse

        </div>
    </div>
@endsection
@section('js')
    <script src="https://cdn.ckeditor.com/4.19.1/standard/ckeditor.js"></script>
    @foreach(localization()->getSupportedLocalesKeys() as $locale)
        <script>
            CKEDITOR.replace( '{{ $locale }}[answer]' );
        </script>

        <script>
            $('#save-{{ $locale }}').on('click', function (){
                $('#form-{{ $locale }}').submit();
            });
        </script>
    @endforeach
@endsection
