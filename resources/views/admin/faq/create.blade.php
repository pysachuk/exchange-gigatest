@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <h4>Создать вопрос FAQ</h4>
            </div>
        </div>
        <div class="card-body">
            @if($errors->any())
                <div class="row">
                    <div class="col-12">
                        <ul class="list-group bg-danger">
                            @foreach ($errors->all() as $error)
                                <li class="list-group-item">{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <br>
            @endif
            <form method="post" style="display: block" action="{{ route('admin.faq.store') }}">
                @csrf
                @foreach(localization()->getSupportedLocalesKeys() as $locale)
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                Язык - {{ $locale }}
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="question-{{ $locale }}">Вопрос ({{ $locale }})</label>
                                <input name="{{ $locale }}[question]" type="text" class="form-control" id="question-{{ $locale }}">
                            </div>
                            <label>Ответ ({{ $locale }})</label>
                            <textarea name="{{ $locale }}[answer]" class="editor" cols="30" rows="10">

                            </textarea>
                        </div>
                    </div>
                @endforeach
                <button type="submit" class="btn btn-success">Создать</button>
            </form>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://cdn.ckeditor.com/4.19.1/standard/ckeditor.js"></script>
    @foreach(localization()->getSupportedLocalesKeys() as $locale)
        <script>
            CKEDITOR.replace( '{{ $locale }}[answer]' );
        </script>
    @endforeach
@endsection
