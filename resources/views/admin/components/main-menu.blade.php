<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('admin.main') }}" class="brand-link">
        <img src="/adminlte/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">ADMIN</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="/adminlte/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <div class="d-block text-white">{{ auth()->user()->name }}</div>
            </div>
            <form method="POST" style="display: inline-block" action="{{ route('logout') }}">
                @csrf
                <button type="submit" class="btn btn-danger bg-danger btn-sm float-right">Выход</button>
            </form>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{ route('admin.main') }}" class="nav-link {{ Route::is('admin.main') ? 'active' : ''  }}">
                        <i class="nav-icon fa fa-home"></i>
                        <p>
                            Главная
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.orders.list') }}" class="nav-link {{ Route::is('admin.orders.*') ? 'active' : ''  }}">
                        <i class="fa fa-exchange" aria-hidden="true"></i>
                        <p>
                            Заявки
                        </p>
                    </a>
                </li>
                <li class="nav-item {{ Route::is('admin.coins.*') ? 'menu-open' : ''  }}">
                    <a href="#" class="nav-link {{ Route::is('admin.coins.*') ? 'active' : ''  }}">
                        <i class="fa fa-btc" aria-hidden="true"></i>
                        <p>
                            Валюты
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.coins.available') }}" class="nav-link {{ Route::is('admin.coins.available') ? 'active' : ''  }}">
                                <i class="fa fa-exchange" aria-hidden="true"></i>
                                <p>Досупные для обмена</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.coins.gaps') }}" class="nav-link {{ Route::is('admin.coins.gaps') ? 'active' : ''  }}">
                                <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                <p>Зазоры</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.coins.all') }}" class="nav-link {{ Route::is('admin.coins.all') ? 'active' : ''  }}">
                                <i class="fa fa-bars"></i>
                                <p>Список валют</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item {{ Route::is('admin.pages.*') ? 'menu-open' : ''  }}">
                    <a href="#" class="nav-link {{ Route::is('admin.pages.*') ? 'active' : ''  }}">
                        <i class="fa fa-sticky-note-o" aria-hidden="true"></i>
                        <p>
                            Статические страницы
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.pages.edit', ['key' => 'crypto-check']) }}" class="nav-link">
                                <i class="fa fa-sticky-note-o" aria-hidden="true"></i>
                                <p>Крипто проверка</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.pages.edit', ['key' => 'partner']) }}" class="nav-link">
                                <i class="fa fa-sticky-note-o" aria-hidden="true"></i>
                                <p>Партнеры</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.pages.edit', ['key' => 'terms']) }}" class="nav-link">
                                <i class="fa fa-sticky-note-o" aria-hidden="true"></i>
                                <p>Пользовательское соглашение</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.pages.edit', ['key' => 'privacy']) }}" class="nav-link">
                                <i class="fa fa-sticky-note-o" aria-hidden="true"></i>
                                <p>Политика конфиденциальности</p>
                            </a>
                        </li>
                    </ul>
                </li>


                <li class="nav-item {{ Route::is('admin.faq.*') ? 'menu-open' : ''  }}">
                    <a href="#" class="nav-link {{ Route::is('admin.faq.*') ? 'active' : ''  }}">
                        <i class="fa fa-question-circle" aria-hidden="true"></i>
                        <p>
                            FAQ
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.faq.list') }}" class="nav-link {{ Route::is('admin.faq.list') ? 'active' : ''  }}">
                                <i class="fa fa-bars"></i>
                                <p>Список вопросов</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.faq.create') }}" class="nav-link {{ Route::is('admin.faq.create') ? 'active' : ''  }}">
                                <i class="fa fa-plus-circle"></i>
                                <p>Создать вопрос FAQ</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item {{ Route::is('admin.settings.*') ? 'menu-open' : ''  }}">
                    <a href="#" class="nav-link {{ Route::is('admin.settings.*') ? 'active' : ''  }}">
                        <i class="fas fa-cog"></i>
                        <p>
                            Настройки
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.settings.localization') }}" class="nav-link {{ Route::is('admin.settings.localization') ? 'active' : ''  }}">
                                <i class="fa fa-language" aria-hidden="true"></i>
                                <p>Переводы</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.settings.main') }}" class="nav-link {{ Route::is('admin.settings.main') ? 'active' : ''  }}">
                                <i class="fas fa-cog"></i>
                                <p>Общие настройки</p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
