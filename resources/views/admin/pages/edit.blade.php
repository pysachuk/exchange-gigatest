@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                Крипто проверка
            </div>
        </div>
        <div class="card-body">
            @if($errors->any())
                <div class="row">
                    <div class="col-12">
                        <ul class="list-group bg-danger">
                            @foreach ($errors->all() as $error)
                                <li class="list-group-item">{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <br>
            @endif
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    @foreach(localization()->getSupportedLocalesKeys() as $locale)
                        <li class="nav-item">
                            <a class="nav-link @if($locale === localization()->getCurrentLocale()) active  @endif" id="home-tab" data-toggle="tab" href="#{{$locale}}" role="tab" aria-controls="{{ $locale }}" aria-selected="true">{{ $locale }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content" id="myTabContent">
                    @foreach(localization()->getSupportedLocalesKeys() as $locale)
                        <div class="tab-pane fade show @if($locale === localization()->getCurrentLocale()) active  @endif" id="{{ $locale }}" role="tabpanel" aria-labelledby="{{ $locale }}-tab">
                            <form method="post" id="form-{{ $locale }}" action="{{ route('admin.pages.store', ['key' => $key]) }}">
                                @csrf
                                <div class="form-group">
                                    <label for="exampleInputEmail123">Заголовок</label>
                                    <input name="{{ $locale }}[title]" type="text" class="form-control" id="exampleInputEmail123" value="{{ $page->translate($locale)->title ?? '' }}">
                                </div>
                                <textarea name="{{ $locale }}[text]" class="editor" cols="30" rows="10">
                                {!! $page->translate($locale)->text ?? '' !!}
                            </textarea>
                                <br>
                                <button type="button" class="btn btn-success" id="save-{{ $locale }}">Сохранить</button>
                            </form>
                        </div>
                    @endforeach
                </div>


        </div>
    </div>
@endsection
@section('js')
    <script src="https://cdn.ckeditor.com/4.19.1/standard/ckeditor.js"></script>
    @foreach(localization()->getSupportedLocalesKeys() as $locale)
        <script>
            CKEDITOR.replace( '{{ $locale }}[text]' );
        </script>

        <script>
            $('#save-{{ $locale }}').on('click', function (){
                $('#form-{{ $locale }}').submit();
            });
        </script>
    @endforeach
@endsection
