@extends('layouts.main')
@section('content')
    <div class="text-page">
        <h2 class="text-header">{{ __('site.history') }}</h2>
        <div class="info">
            <div id="history">
                <div class="table-1">
                    <div class="thead">
                        <div class="tr">
                            <div class="text-center">№</div>
                            <div class="date text-center">{{ __('history.date') }}</div>
                            <div class="name">{{ __('history.name') }}</div>
                            <div class="amount text-center">{{ __('history.sum') }}</div>
                            <div class="status text-center">{{ __('history.status') }}</div>
                        </div>
                    </div>
                    <div class="tbody">
                        @forelse($orders as $order)
                            <div class="tr">
                                <div class="text-center">
                                    <a href="{{ route('order.view', ['uuid' => $order->uuid]) }}">{{ $order->id }}</a>
                                </div>
                                <div class="date text-center">{{ $order->created_at->format('Y-m-d H:i') }}</div>
                                <div class="name"><span class="date_mobile">{{ $order->created_at->format('Y-m-d H:i') }}</span>
                                    <span class="badge-2 bg-exchange">{{ __('history.exchange') }}</span>
                                    <br>
                                    <span class="font-default oper__name"> {{ $order->fromCoin->name.' '.$order->fromCoin->symbol }} - {{ $order->toCoin->name.' '.$order->toCoin->symbol }}</span><span class="amount_mobile">{{ __('history.sum') }}: {{ $order->amount_from }} {{ $order->fromCoin->symbol }}</span><span class="status_mobile">{{ __('history.status') }}: <span class="badge-2 badge-status_canceled">{{ $order->statusName }}</span></span></div>
                                <div class="amount text-center">{{ $order->amount_from }} {{ $order->fromCoin->symbol }}</div>
                                <div class="status text-center"><span class="badge-2 badge-status_canceled">{{ $order->statusName }}</span></div>
                            </div>
                        @empty
                            <div class="tr text-center">
                                EMPTY
                            </div>
                        @endforelse

                    </div>
                </div>
            </div>
            <!-- /#history -->
        </div>
        <!-- /.info -->
    </div>
@endsection
