@extends('layouts.main')
@section('content')
    <div class="text-page">
        <h2 class="text-header">{{ $page->title }}</h2>
        <section>
            {!! $page->text !!}
        </section>
    </div>
@endsection
