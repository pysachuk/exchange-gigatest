@extends('layouts.main')
@section('content')
    <div class="text-page">
        <h2 class="text-header">{{ $page->title }}</h2>

        <section>
            <div>
                <h3>{{ $page->title }}</h3>
                {!! $page->text !!}
            </div>
        </section>
    </div>
@endsection
