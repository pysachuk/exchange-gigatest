@extends('layouts.main')
@section('content')
    <div class="text-page">
        <h2 class="text-header">{{ __('site.support') }}</h2>
        <div class="info">
            <p>{{ __('site.surrort_tg') }}&nbsp;<span style="font-size: 1rem; -webkit-tap-highlight-color: transparent; -webkit-text-size-adjust: 100%;"><a href="https://t.me/{{ getSetting('support_telegram') }}" target="_blank">{{ '@'.getSetting('support_telegram') }}</a></span><br></p>  </div>
    </div>
@endsection
