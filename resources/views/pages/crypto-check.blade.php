@extends('layouts.main')
@section('content')
        <div class="text-page">
            <div id="crypto_check">
                <div class="crypto_check_left">
                    <div class="crypto_check_left_inner">
                        <div class="crypto_check_left_inner_2">
                            <h3 class="title">{{  __('site.wallet_check') }}</h3>
                            <form action="" method="post">
                                <input type="hidden" name="pm_id" id="pm_id" value="9">
                                <div class="pm_select">
                                    <div class="btn">
                                        <div class="pm_select_img"><img src="/img/pm2/bitcoin.svg" alt="pm"></div>
                                        <span>Bitcoin</span>
                                        <svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7 10l5 5 5-5H7z" fill="#000"></path></svg>
                                    </div>
                                    <div class="dropdown" style="display: none;">
                                        <div class="dropdown_inner">
                                            <div class="option" data-id="9" data-cur="BTC" data-account="Адрес Bitcoin" data-name="Bitcoin" id="crypto_option_9">
                                                <div class="pm_select_img"><img src="/img/pm2/bitcoin.svg" alt="pm"></div><span>Bitcoin</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="10" data-cur="ETH" data-account="Адрес Etherium" data-name="Ethereum" id="crypto_option_10">
                                                <div class="pm_select_img"><img src="/img/pm2/ethereum.svg" alt="pm"></div><span>Ethereum</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="11" data-cur="USDT" data-account="Адрес Tether (ERC-20)" data-name="Tether ERC-20" id="crypto_option_11">
                                                <div class="pm_select_img"><img src="/img/pm2/tether.svg" alt="pm"></div><span>Tether ERC-20</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="12" data-cur="USDT" data-account="Адрес Tether (TRC-20)" data-name="Tether TRC-20" id="crypto_option_12">
                                                <div class="pm_select_img"><img src="/img/pm2/tethertrc20.svg" alt="pm"></div><span>Tether TRC-20</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="13" data-cur="USDT" data-account="Адрес Tether Omni" data-name="Tether Omni" id="crypto_option_13">
                                                <div class="pm_select_img"><img src="/img/pm2/tetheromni.svg" alt="pm"></div><span>Tether Omni</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="19" data-cur="LTC" data-account="Адрес Litecoin" data-name="Litecoin" id="crypto_option_19">
                                                <div class="pm_select_img"><img src="/img/pm2/litecoin.svg" alt="pm"></div><span>Litecoin</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="22" data-cur="XEM" data-account="Адрес NEM" data-name="NEM" id="crypto_option_22">
                                                <div class="pm_select_img"><img src="/img/pm2/nem.svg" alt="pm"></div><span>NEM</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="24" data-cur="ETC" data-account="Адрес Ethereum Classic" data-name="Ethereum Classic" id="crypto_option_24">
                                                <div class="pm_select_img"><img src="/img/pm2/etc.svg" alt="pm"></div><span>Ethereum Classic</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="25" data-cur="ZEC" data-account="Адрес Z-Cash" data-name="Zcash" id="crypto_option_25">
                                                <div class="pm_select_img"><img src="/img/pm2/zcash.svg" alt="pm"></div><span>Zcash</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="26" data-cur="LINK" data-account="Адрес Chainlink" data-name="Chainlink" id="crypto_option_26">
                                                <div class="pm_select_img"><img src="/img/pm2/chainlink.svg" alt="pm"></div><span>Chainlink</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="27" data-cur="XMR" data-account="Адрес Monero" data-name="Monero" id="crypto_option_27">
                                                <div class="pm_select_img"><img src="/img/pm2/monero.svg" alt="pm"></div><span>Monero</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="28" data-cur="XLM" data-account="Адрес Stellar" data-name="Stellar" id="crypto_option_28">
                                                <div class="pm_select_img"><img src="/img/pm2/stellar.svg" alt="pm"></div><span>Stellar</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="29" data-cur="DASH" data-account="Адрес DASH" data-name="Dash" id="crypto_option_29">
                                                <div class="pm_select_img"><img src="/img/pm2/dash.svg" alt="pm"></div><span>Dash</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="30" data-cur="DOGE" data-account="Адрес Doge" data-name="Dogecoin" id="crypto_option_30">
                                                <div class="pm_select_img"><img src="/img/pm2/dogecoin.svg" alt="pm"></div><span>Dogecoin</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="31" data-cur="BCH" data-account="Адрес Bitcoin Cash" data-name="Bitcoin Cash" id="crypto_option_31">
                                                <div class="pm_select_img"><img src="/img/pm2/bitcoincash.svg" alt="pm"></div><span>Bitcoin Cash</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="32" data-cur="TRX" data-account="Адрес TRON" data-name="TRON" id="crypto_option_32">
                                                <div class="pm_select_img"><img src="/img/pm2/tron.svg" alt="pm"></div><span>TRON</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="33" data-cur="XRP" data-account="Адрес Ripple" data-name="Ripple" id="crypto_option_33">
                                                <div class="pm_select_img"><img src="/img/pm2/ripple.svg" alt="pm"></div><span>Ripple</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="34" data-cur="USDC" data-account="Адрес UsdCoin" data-name="USD Coin" id="crypto_option_34">
                                                <div class="pm_select_img"><img src="/img/pm2/usdcoin.svg" alt="pm"></div><span>USD Coin</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="35" data-cur="BNB" data-account="Адрес Binance coin" data-name="Binance Coin" id="crypto_option_35">
                                                <div class="pm_select_img"><img src="/img/pm2/binance.svg" alt="pm"></div><span>Binance Coin</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="36" data-cur="NEO" data-account="Адрес NEO" data-name="NEO" id="crypto_option_36">
                                                <div class="pm_select_img"><img src="/img/pm2/neo.svg" alt="pm"></div><span>NEO</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="37" data-cur="ADA" data-account="Адрес Cardano" data-name="Cardano" id="crypto_option_37">
                                                <div class="pm_select_img"><img src="/img/pm2/cardano.svg" alt="pm"></div><span>Cardano</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="38" data-cur="TUSD" data-account="Адрес TrueUsd" data-name="TrueUSD" id="crypto_option_38">
                                                <div class="pm_select_img"><img src="/img/pm2/trueusd.svg" alt="pm"></div><span>TrueUSD</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="39" data-cur="PAX" data-account="Адрес Pax" data-name="Paxos" id="crypto_option_39">
                                                <div class="pm_select_img"><img src="/img/pm2/paxos.svg" alt="pm"></div><span>Paxos</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="40" data-cur="EOS" data-account="Адрес EOS" data-name="EOS" id="crypto_option_40">
                                                <div class="pm_select_img"><img src="/img/pm2/eos.svg" alt="pm"></div><span>EOS</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="41" data-cur="XTZ" data-account="Адрес Tezos" data-name="Tezos" id="crypto_option_41">
                                                <div class="pm_select_img"><img src="/img/pm2/tezos.svg" alt="pm"></div><span>Tezos</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                            <div class="option" data-id="42" data-cur="BTT" data-account="Адрес BitTorrent" data-name="BitTorrent" id="crypto_option_42">
                                                <div class="pm_select_img"><img src="/img/pm2/bittorrent.svg" alt="pm"></div><span>BitTorrent</span>
                                                <div class="pm_childs">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="field">
                                        <input name="account" id="crypto_check_wallet">
                                        <div class="placeholder" id="crypto_check_ac">{{ __('site.wallet') }} <span>Bitcoin</span></div>
                                    </div>
                                </div>
                                <div class="button-block">
                                    <button type="submit" class="btn-submit checkButton" disabled="disabled"><span> <span class="checkButton__text">{{ __('site.check') }}</span><span class="checkButton__value">1 <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M17.021 8.87994L13.5536 20.438L22.8 8.87994H17.021Z" fill="white"></path><path d="M6.97892 8.87994H1.19988L10.4463 20.438L6.97892 8.87994Z" fill="white"></path><path d="M22.5876 7.60592L17.9843 3.00256C17.8656 2.88389 17.7047 2.81723 17.5368 2.81723H15.1928L16.9886 7.60592H22.5876Z" fill="white"></path><path d="M7.01114 7.60592L8.80687 2.81723H6.46291C6.29508 2.81723 6.1341 2.88389 6.01542 3.00256L1.41212 7.60592H7.01114Z" fill="white"></path><path d="M15.6911 8.87994H8.30936L12.0002 21.1829L15.6911 8.87994Z" fill="white"></path><path d="M8.3721 7.60592H15.6278L13.832 2.81723H10.1679L8.3721 7.60592Z" fill="white"></path></svg></span></span></button>
                                </div>
                                <!--div class="crypto_check_footer">
                                  <div class="balance">Баланс кристаллов: <span class="balance__value">1</span> <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" class="diamond-icon"><path d="M17.021 8.87994L13.5536 20.438L22.8 8.87994H17.021Z" fill="white"></path><path d="M6.97892 8.87994H1.19988L10.4463 20.438L6.97892 8.87994Z" fill="white"></path><path d="M22.5876 7.60592L17.9843 3.00256C17.8656 2.88389 17.7047 2.81723 17.5368 2.81723H15.1928L16.9886 7.60592H22.5876Z" fill="white"></path><path d="M7.01114 7.60592L8.80687 2.81723H6.46291C6.29508 2.81723 6.1341 2.88389 6.01542 3.00256L1.41212 7.60592H7.01114Z" fill="white"></path><path d="M15.6911 8.87994H8.30936L12.0002 21.1829L15.6911 8.87994Z" fill="white"></path><path d="M8.3721 7.60592H15.6278L13.832 2.81723H10.1679L8.3721 7.60592Z" fill="white"></path></svg></div>
                                  <div class="info"><button type="button" class="info__button"><svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.33331 3.66665H7.66665V4.99998H6.33331V3.66665ZM6.33331 6.33331H7.66665V10.3333H6.33331V6.33331ZM6.99998 0.333313C3.31998 0.333313 0.333313 3.31998 0.333313 6.99998C0.333313 10.68 3.31998 13.6666 6.99998 13.6666C10.68 13.6666 13.6666 10.68 13.6666 6.99998C13.6666 3.31998 10.68 0.333313 6.99998 0.333313ZM6.99998 12.3333C4.05998 12.3333 1.66665 9.93998 1.66665 6.99998C1.66665 4.05998 4.05998 1.66665 6.99998 1.66665C9.93998 1.66665 12.3333 4.05998 12.3333 6.99998C12.3333 9.93998 9.93998 12.3333 6.99998 12.3333Z" fill="#0359FD"></path></svg>Как получить?</button></div>
                                  <div class="powered-by">Powered by <svg width="82" height="19" viewBox="0 0 83 20" fill="none" xmlns="http://www.w3.org/2000/svg"><g><path fill-rule="evenodd" clip-rule="evenodd" d="M15.1902 1.98135H9.45701H3.72379L0.300171 6.56802L9.45701 15.796L18.6138 6.56802L15.1902 1.98135Z" fill="#D1D2DA"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M23.6348 10.248V7.0264C23.6348 3.25878 25.3466 1.48418 28.8789 1.48418C32.2754 1.48418 33.9329 3.12227 34.0415 6.50766H31.6505C31.5689 4.54195 30.7538 3.6683 28.9061 3.6683C26.9226 3.6683 26.1074 4.67846 26.1074 7.08103V10.2207C26.1074 12.6232 26.9226 13.6334 28.9061 13.6334C30.7538 13.6334 31.5689 12.7597 31.6505 10.8213H34.0415C33.9329 14.2067 32.2754 15.8175 28.8789 15.8175C25.3466 15.7902 23.6348 13.9883 23.6348 10.248Z" fill="#D1D2DA"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M35.9167 5.0061H37.9817L38.0905 6.94451C38.5524 5.47022 39.449 4.81499 40.9163 4.81499H41.6771V7.13563H41.1336C39.5034 7.13563 38.6882 7.81814 38.3622 9.51085V15.5718H35.9439V5.0061H35.9167Z" fill="#D1D2DA"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M42.954 17.2644H43.9865C44.6658 17.2644 44.9647 17.0734 45.2092 16.3362L45.5624 15.2715L41.8943 5.00606H44.4756L46.8124 12.1864L48.9861 5.00606H51.4315L47.5188 16.6366C46.8395 18.6568 46.1331 19.2575 44.2582 19.2575H42.954V17.2644Z" fill="#D1D2DA"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M51.7574 12.3775H54.067C54.067 13.415 54.7463 13.9338 56.1592 13.9338C57.4634 13.9338 58.0884 13.4969 58.0884 12.7052C58.0884 11.8315 57.4634 11.4766 55.6701 11.2309C53.0073 10.9033 51.9747 10.0569 51.9747 8.0912C51.9747 5.90707 53.4692 4.76041 56.1592 4.76041C58.8492 4.76041 60.2349 5.90707 60.2349 8.06389H57.8982C57.8982 7.08106 57.3276 6.61691 56.132 6.61691C54.9908 6.61691 54.3659 7.05375 54.3659 7.81818C54.3659 8.66454 54.9908 8.99217 56.7026 9.18326C59.4198 9.48358 60.4523 10.4392 60.4523 12.4594C60.4523 14.6709 59.0122 15.7902 56.132 15.7902C53.1703 15.7902 51.7574 14.6436 51.7574 12.3775Z" fill="#D1D2DA"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M62.5717 12.1593V6.8901H60.9142V5.0063H62.626L62.7891 2.30344H65.0173V5.0063H67.381V6.8901H65.0173V12.1047C65.0173 13.2514 65.2891 13.579 66.3215 13.579H67.4895V15.5447H65.5603C63.4683 15.5447 62.5717 14.5618 62.5717 12.1593Z" fill="#D1D2DA"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M72.3529 9.15595C69.5004 9.15595 68.1142 10.3572 68.1142 12.5414C68.1142 14.6163 69.4184 15.7902 71.4839 15.7902C72.9779 15.7902 74.0652 15.2169 74.8529 14.1522L74.9342 15.5445H76.9997V8.71915C76.9997 6.01628 75.6412 4.76041 72.761 4.76041C69.9892 4.76041 68.5765 5.93437 68.4131 8.25498H70.7498C70.8317 7.24484 71.4567 6.72612 72.679 6.72612C73.9832 6.72612 74.5811 7.35407 74.5811 8.69184V9.15595H72.3529ZM74.5811 10.9033V12.2137C74.0652 13.2785 73.2226 13.8245 72.1088 13.7972C71.0487 13.7972 70.5057 13.2785 70.5057 12.3775C70.5057 11.422 71.1036 10.9033 72.679 10.9033H74.5811Z" fill="#D1D2DA"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M79.3093 0.71991H81.7273V15.5447H79.3093V0.71991Z" fill="#D1D2DA"></path></g><defs><clipPath id="clip0"><rect width="81.9867" height="18.92" fill="white" transform="translate(0.299988 0.719818)"></rect></clipPath></defs></svg></div>
                                </div-->
                            </form>
                        </div>
                    </div>
                </div>
                <!--/.crypto_check_left-->
                <div class="crypto_check_right">
                    <div class="crypto_check_info">
                        <img src="/img/d/crypto-check.svg" alt="">
                        <div class="content2">
                            <h3>{{ $page->title }}</h3>
                            {!! $page->text !!}
                        </div>
                    </div>
                    <!--/.crypto_check_info-->
                </div>
                <!--/.crypto_check_right-->
            </div>
            <!--/#crypto_check-->
        </div>
@endsection
