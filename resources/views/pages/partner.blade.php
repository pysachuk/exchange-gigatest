@extends('layouts.main')
@section('content')
    <div class="text-page">
        <h2 class="text-header">{{ $page->title }}</h2>
        <div class="info">
            {!! $page->text !!}
        </div>
    </div>

@endsection
