<div>
{{--    @livewire('admin.localization.edit-modal')--}}
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12 d-flex justify-content-end align-items-end p-3">
                    <div class=" bg-transparent">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="ibox ">
        <div class="ibox-title p-3">
            <div class="row">
                <div class="col-6 text-left">
                    <h3>Переводы</h3>
                </div>
                <div class="col-6 text-right">
                    <input type="text" class="form-control" placeholder="Поиск" wire:model="search">
                    {{--                    <a href="{{ route('admin.pages.main.block.create') }}"><button class="btn btn-primary">Створити</button></a>--}}
                </div>
            </div>


        </div>
        {{--        @if ($blocks->count())--}}
        <div class="ibox-content">
            <div class="row">
                <div class="col-2">
                    <input type="text" class="form-control" placeholder="Група" wire:model="newLine.group">
                </div>
                <div class="col-2">
                    <input type="text" class="form-control" placeholder="Ключ" wire:model="newLine.key">
                </div>
                <div class="col-2">
                    <input type="text" class="form-control" placeholder="Текст RU" wire:model="newLine.text.ru">
                </div>
                <div class="col-2">
                    <input type="text" class="form-control" placeholder="Текст EN" wire:model="newLine.text.en">
                </div>
                <div class="col-2">
                    <input type="text" class="form-control" placeholder="Текст PL" wire:model="newLine.text.pl">
                </div>
                <div class="col-2 text-right">
                    <button wire:click="saveLine" class="btn btn-primary">Создать</button>
                </div>
            </div>
            <div class="row">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Група</th>
                            <th>Ключ</th>
                            <th>Тект RU</th>
                            <th>Тект EN</th>
                            <th>Тект PL</th>
                            <th>Код</th>
                            <th>Дія</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($lines as $line)
                            <tr>
                                <td>{{ $line->id }}</td>
                                <td>{{ $line->group }}</td>
                                <td>{{ $line->key }}</td>
                                <td>{{ Str::limit($line->text['ru'], 20) }}</td>
                                <td>{{ Str::limit($line->text['en'], 20) }}</td>
                                <td>{{ Str::limit($line->text['pl'], 20) }}</td>
                                <td><input type="text" class="form-control" disabled value="__('{{ $line->group }}.{{ $line->key }}')"> </td>
                                <td>
                                    <button wire:click.prevent="deleteLineConfirmation({{ $line }})" class="btn btn-danger btn-sm" >
                                        <i class="fas fa-trash"></i>
                                    </button>
                                    <button wire:click.prevent="editLine({{ $line }})" class="btn btn-success btn-sm" >
                                        <i class="fas fa-edit"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        {{--        @endif--}}
    </div>
    <div class="mt-5 mb-5">
        {{ $lines->links() }}
    </div>

    <div class="modal" @if ($showModal) style="display:block" @endif>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form >
                    <div class="modal-header">
                        <h5 class="modal-title">TEST</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" wire:click="closeModal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Текст RU:
                        <br/>
                        <input wire:model="editLine.text.ru" class="form-control"/>
                        @error('editLine.text.ru')
                            <div style="font-size: 11px; color: red">{{ $message }}</div>
                        @enderror
                        <br/>
                        Текст EN:
                        <br/>
                        <input wire:model="editLine.text.en" class="form-control"/>
                        @error('editLine.text.en')
                            <div style="font-size: 11px; color: red">{{ $message }}</div>
                        @enderror
                        <br/>
                        Текст EN:
                        <br/>
                        <input wire:model="editLine.text.pl" class="form-control"/>
                        @error('editLine.text.pl')
                        <div style="font-size: 11px; color: red">{{ $message }}</div>
                        @enderror
                        <br/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" wire:click="saveEditLine">Зберегти</button>
                        <button type="button" class="btn btn-secondary" wire:click="closeModal" data-dismiss="modal">Закрити
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

