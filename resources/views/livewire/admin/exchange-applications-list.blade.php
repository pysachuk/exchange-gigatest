<div>
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <h4>Заявки на обмен</h4>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th scope="col">№</th>
                        <th scope="col">UUID</th>
                        <th scope="col">Отдает</th>
                        <th scope="col">Сума обмена</th>
                        <th scope="col">Получает</th>
                        <th scope="col">Сума получения</th>
                        <th scope="col">Кошелек получения</th>
                        <th scope="col">Статус</th>
                        <th scope="col">Дата обмена</th>

                    </tr>
                    </thead>
                    <tbody>
                    @forelse($applications as $order)
                        <tr wire:click="viewOrder({{ $order->id }})" style="cursor: pointer">
                            <th scope="row">{{ $order->id }}</th>
                            <td>{{ $order->uuid }}</td>
                            <td>
                                {{ $order->fromCoin->name }}
                                <br>
                                <img src="/{{ $order->fromCoin->icon }}" width="30">
                            </td>
                            <td>{{ $order->amount_from }}</td>
                            <td>
                                {{ $order->toCoin->name }}
                                <br>
                                <img src="/{{ $order->toCoin->icon }}" width="30">
                            </td>
                            <td>{{ $order->amount_to }}</td>
                            <td>{{ $order->wallet_to }}</td>
                            <td>{{ $order->status_name }}</td>
                            <td>{{ $order->created_at->format('d.m.Y H:i') }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="9">Пусто</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            {{ $applications->links() }}
        </div>
    </div>
</div>
