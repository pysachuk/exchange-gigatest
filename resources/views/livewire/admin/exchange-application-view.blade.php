<div>
    <div class="container">
        <div class="row">
            <div class="col-10 offset-1">
                <div class="card">
                    <div class="card-header">
                        Заявка на обмен № <b>{{ $exchangeApplication->id }}</b>
                    </div>
                    <div class="card-body">
                        <div class="alert alert-danger text-center">
                            Деньги должны зачислиться на кошелек {{ $exchangeApplication->fromCoin->name }}:  {{ $exchangeApplication->fromCoin->wallet }}
                        </div>
                        <ul class="list-group list-group-flush text-center">
{{--                            <li class="list-group-item">UUID: {{ $exchangeApplication->uuid }}</li>--}}
                            @if($exchangeApplication->user_id)
                                <li class="list-group-item">Ползователь: {{ $exchangeApplication->user_id }}</li>
                            @endif
                            <li class="list-group-item">
                                <h4>Обмен </h4>
                                <h3>{{ $exchangeApplication->amount_from }} {{ $exchangeApplication->fromCoin->name }}</h3>
                                <h4>></h4>
                                <h3>{{ $exchangeApplication->amount_to }} {{ $exchangeApplication->toCoin->name }}</h3>
                            </li>
                            <li class="list-group-item">
                                Кошелек {{ $exchangeApplication->toCoin->name }} пользователя: <h4>{{ $exchangeApplication->wallet_to }}</h4>
                            </li>
                            <li class="list-group-item">
                                Дата заявки: <h4>{{ $exchangeApplication->created_at->format('d.m.Y H:i') }}</h4>
                            </li>
                            <li class="list-group-item">
                                Статус: <h4>{{ $exchangeApplication->status_name }}</h4>
                            </li>
                            <div class="alert alert-success text-center">
                                Переведите {{ $exchangeApplication->amount_to }} {{ $exchangeApplication->toCoin->name }} на адрес {{ $exchangeApplication->wallet_to }}
                            </div>
                            <li class="list-group-item">
                                <button wire:click="setStatusConfirmation('{{ \App\Models\ExchangeApplication::STATUS_NEW }}')" class="btn btn-success" @if($exchangeApplication->status === 'new') disabled @endif>Новая</button>
                                >
                                <button  class="btn btn-primary" disabled >На проверке</button>
                                >
                                <button wire:click="setStatusConfirmation('{{ \App\Models\ExchangeApplication::STATUS_PAYED }}')" class="btn btn-success" @if($exchangeApplication->status === 'payed') disabled @endif>Оплачена</button>
                                >
                                {{--                    <button class="btn btn-warning" @if($exchangeApplication->status === 'confirmation') disabled @endif>Ожидает подтвреждения</button>--}}
                                {{--                    >--}}
                                <button wire:click="setStatusConfirmation('{{ \App\Models\ExchangeApplication::STATUS_COMPLETED }}')" class="btn btn-dark" @if($exchangeApplication->status === 'completed') disabled @endif>Завершена</button>
                            </li>
                            <li class="list-group-item">
                                <button wire:click="setStatusConfirmation('{{ \App\Models\ExchangeApplication::STATUS_CANCELLED }}')" class="btn btn-danger" @if($exchangeApplication->status === 'cancelled') disabled @endif>Отменить заявку</button>
                            </li>
                            <li class="list-group-item">
                                <button wire:click="deleteConfirmation({{ $exchangeApplication->id }})" class="btn btn-danger">Удалить заявку</button>
                            </li>
                        </ul>
                    </div>


                </div>
            </div>
        </div>

    </div>
</div>
