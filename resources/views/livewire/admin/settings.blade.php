<div>
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <h4>Общие настройки</h4>
            </div>
        </div>
        <div class="card-body">
            <form class="needs-validation">
                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label for="validationCustom01">Название сайта</label>
                        <input wire:model="siteName" type="text" class="form-control @error('siteName') is-invalid @enderror" id="validationCustom01" placeholder="SITENAME">
                        @error('siteName')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="validationCustom01">Email поддержки</label>
                        <input wire:model="supportEmail" type="text" class="form-control @error('supportEmail') is-invalid @enderror" id="validationCustom01" placeholder="SITENAME">
                        @error('supportEmail')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="validationCustom01">Telegram поддержки</label>
                        <input wire:model="supportTelegram" type="text" class="form-control @error('supportTelegram') is-invalid @enderror" id="validationCustom01" placeholder="telegram">
                        @error('supportTelegram')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <label>Глобальный процент</label>
                        <div class="custom-control custom-switch">
                            <input wire:model="isGlobalPercent" type="checkbox" class="custom-control-input" id="is_global_percent">
                            <label class="custom-control-label" for="is_global_percent">{{ $isGlobalPercent ? 'Включено' : 'Выключено' }}</label>
                        </div>
                    </div>
                </div>
                @if($isGlobalPercent)
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                            <label for="validationCustom01zzz">Глобальный процент зазора</label>
                            <input wire:model="globalPercent" type="text" class="form-control @error('globalPercent') is-invalid @enderror" id="validationCustom01zzz" placeholder="5">
                            @error('globalPercent')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                @endif
                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label for="validationCustom01244">Telegram chat ID</label>
                        <input wire:model="telegramChatId" type="text" class="form-control @error('telegramChatId') is-invalid @enderror" id="validationCustom01244" placeholder="pass">
                        @error('telegramChatId')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label for="validationCustom0124">Новый пароль админа</label>
                        <input wire:model="newPassword" type="text" class="form-control @error('newPassword') is-invalid @enderror" id="validationCustom0124" placeholder="pass">
                        @error('newPassword')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <button wire:click.prevent="save" class="btn btn-primary" type="button">Сохранить</button>
            </form>
        </div>
    </div>
</div>
