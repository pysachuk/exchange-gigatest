<div>
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <h4>Список валют</h4>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <input wire:model="search" type="text" class="form-control" id="search"  placeholder="Search...">
                        </div>
                    </div>
                    <div class="col-4 mt-1">
                        <button wire:click="showCreateModal" class="btn btn-success">Добавить</button>
                    </div>
                </div>

            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Иконка</th>
                        <th scope="col">Название</th>
                        <th scope="col">type</th>
                        <th scope="col">Описание</th>
                        <th scope="col">Кошелек</th>
                        <th scope="col">Цена в USD</th>
                        <th scope="col">MIN</th>
                        <th scope="col">MAX</th>
                        <th scope="col">Управление</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($coins as $coin)
                        <tr>
                            <th scope="row">{{ $coin->id }}</th>
                            <td><img src="/{{ $coin->icon }}" width="30" alt=""></td>
                            <td>{{ $coin->name }}</td>
                            <td>{{ $coin->type }}</td>
                            <td>
                                RU:{{ $coin->translate('ru')->description ?? '' }}
                                <br>
                                EN:{{ $coin->translate('en')->description ?? '' }}
                                <br>
                                PL:{{ $coin->translate('pl')->description ?? '' }}
                            </td>
                            <td>{{ $coin->wallet }}</td>
                            <td>{{ $coin->usd_price }}</td>
                            <td>{{ $coin->min }}</td>
                            <td>{{ $coin->max }}</td>
                            <td class="text-center">
                                <button wire:click="editCoin({{ $coin->id }})" class="btn btn-primary btn-sm">
                                    <i class="fas fa-edit"></i>
                                </button>
                                <button wire:click="removeConfirmation({{ $coin->id }})" class="btn btn-danger btn-sm">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="8" class="text-center">Пусто</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            {{ $coins->links() }}
        </div>
    </div>
    <div class="modal" @if ($showCreateModal) style="display:block" @endif>
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Добавить валюту</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" wire:click="closeCreateModal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            @if($errors->any())
                                <div class="row">
                                    <div class="col-12">
                                        <ul class="list-group bg-danger">
                                            @foreach ($errors->all() as $error)
                                                <li class="list-group-item">{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                            @endif
                            <div class="row">
                                <div class="form-group">
                                    <h4>coin_id</h4>
                                    <label for="coinId"></label>
                                    <input wire:model="newCoin.coin_id" type="text" class="form-control @error('newCoin.coin_id') is-invalid @enderror" id="coinId" placeholder="Coin Id">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="exampleFormControlFile9991">Иконка</label>
                                    <input wire:model="newCoin.icon" type="file" class="form-control-file" id="exampleFormControlFile9991">
                                    @if(isset($this->newCoin['icon']))
                                        <br>
                                        <img src="{{ $this->newCoin['icon']->temporaryUrl() }}" alt="" width="100" height="100">
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <h4>Название</h4>
                                    <label for="name"></label>
                                    <input wire:model="newCoin.name" type="text" class="form-control @error('newCoin.name') is-invalid @enderror" id="name" placeholder="Coin Id">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <h4>Тип</h4>
                                    <select wire:model="newCoin.type" class="custom-select @error('newCoin.type') is-invalid @enderror" id="inputGroupSelect0123">
                                        <option value="coin">Криптовалюта</option>
                                        <option value="card">Карта</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <h4>Описание RU</h4>
                                    <label for="description_ru"></label>
                                    <input wire:model="newCoin.ru.description" type="text" class="form-control @error('newCoin.ru.description') is-invalid @enderror" id="description_ru" placeholder="Описание RU">
                                </div>
                            </div>
                                <div class="row">
                                    <div class="form-group">
                                        <h4>Описание PL</h4>
                                        <label for="description_pl"></label>
                                        <input wire:model="newCoin.pl.description" type="text" class="form-control @error('newCoin.pl.description') is-invalid @enderror" id="description_pl" placeholder="Описание PL">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <h4>Описание EN</h4>
                                        <label for="description_en"></label>
                                        <input wire:model="newCoin.en.description" type="text" class="form-control @error('newCoin.en.description') is-invalid @enderror" id="description_en" placeholder="Описание EN">
                                    </div>
                                </div>
                            <div class="row">
                                <div class="form-group">
                                    <h4>Кошелек</h4>
                                    <label for="wallet"></label>
                                    <input wire:model="newCoin.wallet" type="text" class="form-control @error('newCoin.wallet') is-invalid @enderror" id="wallet" placeholder="Кошелек">
                                </div>
                            </div>

{{--                            <div class="row">--}}
{{--                                <div class="form-group col-6">--}}
{{--                                    <div class="custom-control custom-switch">--}}
{{--                                        <input wire:model="newFop.is_active" checked type="checkbox" class="custom-control-input" id="customSwitch1">--}}
{{--                                        <label class="custom-control-label" for="customSwitch1">Активний</label>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" wire:click="createCoin">Добавить</button>
                    <button type="button" class="btn btn-secondary" wire:click="closeCreateModal" data-dismiss="modal">Закрыть
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" @if ($showEditModal) style="display:block" @endif>
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Редактировать</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" wire:click="closeEditModal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            @if($errors->any())
                                <div class="row">
                                    <div class="col-12">
                                        <ul class="list-group bg-danger">
                                            @foreach ($errors->all() as $error)
                                                <li class="list-group-item">{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                            @endif
                            <div class="row">
                                <div class="form-group">
                                    <h4>Название</h4>
                                    <label for="name"></label>
                                    <input wire:model="editCoin.name" type="text" class="form-control @error('editCoin.name') is-invalid @enderror" id="name" placeholder="Название">
                                </div>
                            </div>
                                <div class="row">
                                    <div class="form-group">
                                        <h4>Описание RU</h4>
                                        <label for="editCoin_description_ru"></label>
                                        <input wire:model="editCoin.ru.description" type="text" class="form-control @error('editCoin.ru.description') is-invalid @enderror" id="editCoin_description_ru" placeholder="Описание RU">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <h4>Описание PL</h4>
                                        <label for="editCoin_description_pl"></label>
                                        <input wire:model="editCoin.pl.description" type="text" class="form-control @error('editCoin.pl.description') is-invalid @enderror" id="editCoin_description_pl" placeholder="Описание PL">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <h4>Описание EN</h4>
                                        <label for="editCoin_description_en"></label>
                                        <input wire:model="editCoin.en.description" type="text" class="form-control @error('editCoin.en.description') is-invalid @enderror" id="editCoin_description_en" placeholder="Описание EN">
                                    </div>
                                </div>
                            <div class="row">
                                <div class="form-group">
                                    <h4>Кошелек</h4>
                                    <label for="wallet"></label>
                                    <input wire:model="editCoin.wallet" type="text" class="form-control @error('editCoin.wallet') is-invalid @enderror" id="wallet" placeholder="Кошелек">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <h4>Min</h4>
                                    <label for="min"></label>
                                    <input wire:model="editCoin.min" type="text" class="form-control @error('editCoin.min') is-invalid @enderror" id="min" placeholder="Min">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <h4>Max</h4>
                                    <label for="max"></label>
                                    <input wire:model="editCoin.max" type="text" class="form-control @error('editCoin.max') is-invalid @enderror" id="min" placeholder="Max">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" wire:click="saveEditCoin">Сохранить</button>
                    <button type="button" class="btn btn-secondary" wire:click="closeEditModal" data-dismiss="modal">Закрыть
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
