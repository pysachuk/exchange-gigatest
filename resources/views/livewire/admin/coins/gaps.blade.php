<div>
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <div class="card-title">
                            <h4>Зазоры при обмене</h4>
                        </div>
                    </div>
                    <div class="col-2 offset-4">
                        <div class="row">
                            <button class="btn btn-success btn-sm" wire:click="showCreateModal">Добавить зазор</button>
                        </div>
                    </div>
                </div>




            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">№</th>
                        <th scope="col">Отдает</th>
                        <th scope="col">Получает</th>
                        <th scope="col">Процент (%)</th>
                        <th scope="col">Редактировать</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($gaps as $gap)
                        <tr>
                            <th scope="row">{{ $gap->id }}</th>
                            <td>{{ $gap->coinFrom->name }}</td>
                            <td>{{ $gap->coinTo->name }}</td>
                            <td>{{ $gap->percent }} %</td>
                            <td class="text-center">
                                <button wire:click="showEditModal({{ $gap->id }})" class="btn btn-info btn-sm">Редактировать</button>
                                <button wire:click="deleteConfirmation({{ $gap->id }})" class="btn btn-danger btn-sm">Удалить</button>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4" class="text-center">Пусто</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            {{ $gaps->links() }}
        </div>
    </div>
    <div class="modal" @if ($showCreateModal) style="display:block" @endif>
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Добавить зазор</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" wire:click="closeCreateModal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            @if($errors->any())
                                <div class="row">
                                    <div class="col-12">
                                        <ul class="list-group bg-danger">
                                            @foreach ($errors->all() as $error)
                                                <li class="list-group-item">{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                            @endif
                                <div class="row">
                                    <div class="form-group">
                                        <h4>Отдает</h4>
                                        <select wire:model="newGap.coin_from_id" class="custom-select @error('newGap.coin_from_id') is-invalid @enderror" id="inputGroupSelect0123">
                                            @forelse($availableCoins as $coin)
                                                <option value="{{ $coin->id }}">{{ $coin->name }}</option>
                                            @empty
                                                <option value="0">ПУСТО</option>
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <h4>Получает</h4>
                                        <select wire:model="newGap.coin_to_id" class="custom-select @error('newGap.coin_to_id') is-invalid @enderror" id="inputGroupSelect0123">
                                            @forelse($availableCoins as $coin)
                                                <option value="{{ $coin->id }}">{{ $coin->name }}</option>
                                            @empty
                                                <option value="0">ПУСТО</option>
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                            <div class="row">
                                <div class="form-group">
                                    <h4>Процент зазора</h4>
                                    <label for="name"></label>
                                    <input wire:model="newGap.percent" type="text" class="form-control @error('newCoin.percent') is-invalid @enderror" id="name" placeholder="Процент зазора">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" wire:click="saveNewGap">Добавить</button>
                    <button type="button" class="btn btn-secondary" wire:click="closeCreateModal" data-dismiss="modal">Закрыть
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" @if ($showEditModal) style="display:block" @endif>
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Редактировать зазор</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" wire:click="closeEditModal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @if($this->editGap)
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                @if($errors->any())
                                    <div class="row">
                                        <div class="col-12">
                                            <ul class="list-group bg-danger">
                                                @foreach ($errors->all() as $error)
                                                    <li class="list-group-item">{{$error}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                @endif
{{--                                <div class="row">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <h4>Отдает</h4>--}}
{{--                                        <select wire:model="editGap.coin_from_id" class="custom-select @error('editGap.coin_from_id') is-invalid @enderror" id="inputGroupSelect0123">--}}
{{--                                            @forelse($availableCoins as $coin)--}}
{{--                                                <option value="{{ $coin->id }}">{{ $coin->name }}</option>--}}
{{--                                            @empty--}}
{{--                                                <option value="0">ПУСТО</option>--}}
{{--                                            @endforelse--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="row">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <h4>Получает</h4>--}}
{{--                                        <select wire:model="editGap.coin_to_id" class="custom-select @error('editGap.coin_to_id') is-invalid @enderror" id="inputGroupSelect0123">--}}
{{--                                            @forelse($availableCoins as $coin)--}}
{{--                                                <option value="{{ $coin->id }}">{{ $coin->name }}</option>--}}
{{--                                            @empty--}}
{{--                                                <option value="0">ПУСТО</option>--}}
{{--                                            @endforelse--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div class="row">
                                    <div class="form-group">
                                        <h4>Процент зазора</h4>
                                        <label for="percent"></label>
                                        <input wire:model="editGap.percent" type="text" class="form-control @error('editGap.percent') is-invalid @enderror" id="percent" placeholder="Процент зазора">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" wire:click="saveEditGap">Сохранить</button>
                        <button type="button" class="btn btn-secondary" wire:click="closeEditModal" data-dismiss="modal">Закрыть
                        </button>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
