<div>
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <h4>Список валют</h4>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <input wire:model="search" type="text" class="form-control" id="search"  placeholder="Search...">
                        </div>
                    </div>
                </div>

            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">id</th>
                            <th scope="col">name</th>
                            <th scope="col">coin_id</th>
                            <th scope="col">symbol</th>

                        </tr>
                    </thead>
                    <tbody>
                        @forelse($coins as $coin)
                        <tr>
                            <th scope="row">{{ $coin->id }}</th>
                            <td>{{ $coin->name }}</td>
                            <td>{{ $coin->coin_id }}</td>
                            <td>{{ $coin->symbol }}</td>
                        </tr>
                        @empty
                            <tr>
                                <td colspan="4">Пусто</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            {{ $coins->links() }}
        </div>
    </div>
</div>
