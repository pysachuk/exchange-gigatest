 <div class="text-page">
            <h2 class="text-header text-center">{{ __('exchange.request') }} № {{ $exchangeApplication->id }}
                @if(in_array($exchangeApplication->status, ['inspection', 'payed']))
                    <div class="spinner-border text-primary" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                @endif
            </h2>


            <div id="exchange_proccess" class="info">

                <div class="exchange_proccess">
                    <table class="table">
                        <tbody><tr class="pm">
                            <td>{{ $exchangeApplication->fromCoin->name }}</td>
                            <td>{{ $exchangeApplication->toCoin->name }}</td>
                        </tr>
                        <tr class="sum">
                            <td class="longtext">{{ $exchangeApplication->amount_from }} {{ $exchangeApplication->fromCoin->symbol}}</td>
                            <td class="longtext">{{ $exchangeApplication->amount_to }} {{ $exchangeApplication->toCoin->symbol}}</td>
                        </tr>
                        <tr class="date">
                            <td>{{ __('exchange.created_date') }}:</td>
                            <td>{{ $exchangeApplication->created_at->format('d.m.Y H:i') }}</td>
                        </tr>
                        @if($exchangeApplication->status === 'new')
                            <tr class="date">
                                <td>{{ __('exchange.expiration_date') }}:</td>
                                <td>{{ $exchangeApplication->created_at->addMinutes(30)->format('d.m.Y H:i') }}</td>
                            </tr>
                        @endif
                        <tr>
                            <td>{{ __('exchange.rec_det') }}:</td>
                            <td><span class="longtext">{{ $exchangeApplication->wallet_to }}</span></td>
                        </tr>
                        @if($exchangeApplication->status === 'new')
                            <tr>
                                <td>{{ __('exchange.time_to_pay') }}:</td>
                                <td><span id="exchange_timer" class="text-nowrap timer"></span></td>
                            </tr>
                        @endif
                        <tr>
                            <td style="margin-top: 10px;">{{ __('exchange.status') }}:</td>
                            <td>
                                {{ $exchangeApplication->status_name }}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    @if($exchangeApplication->status === 'new')
                        <p class="text-center">{{ __('exchange.pay') }} {{ $exchangeApplication->amount_from }} {{ $exchangeApplication->fromCoin->symbol}}
                            {{ __('exchange.to') }}:<br>
                            <span class="longtext">{{ $exchangeApplication->fromCoin->wallet}}</span>
                            @if($exchangeApplication->fromCoin->walletQrCode)
                                <br>
                                <img width="200" src="{{ $exchangeApplication->fromCoin->walletQrCode }}" alt="">
                            @endif
                        </p>
                        <br>
                        <form action="" method="post">
                            <button wire:click.prevent="setStatusInspection" type="button" name="paid" class="btn-1" value="1">
                                {{ __('exchange.im_payed') }}</button>
                            <button wire:click.prevent="cancelOrder" type="button" name="cancel" class="btn-1" value="1">{{ __('exchange.cancel') }}</button>
                        </form>
                    @endif

                </div>
                <!--/.exchange_proccess-->

            </div>
            <!--/#exchange_proccess-->

</div>
@section('js')
    <script src="/js/jquery.countdown.js"></script>
    <script type="text/javascript">
        $(".timer")
            .countdown("{{ $exchangeApplication->created_at->addMinutes(config('app.exchange_time'))->format('Y/m/d H:i:s') }}", function(event) {
                $(this).text(
                    event.strftime('%M:%S')
                );
            }).on('finish.countdown', function (){
                window.livewire.emit('timeFinish', {{ $exchangeApplication->id }})
        });
    </script>
@endsection
