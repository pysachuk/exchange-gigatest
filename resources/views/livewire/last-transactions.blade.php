<div>
    <section id="last_transactions" data-num_show="30">
        <div class="text-page">
            <div class="last_trans__header">
                <h2 class="text-header">Последние транзакции</h2>
            </div>
            <div class="last_trans__list">
                <div class="container-2">
                    <div class="table-lt table-lt-header">
                        <div class="tr">
                            <div class="td">Время</div>
                            <div class="td">Операция</div>
                            <div class="td">Количество</div>
                        </div>
                    </div>
                    <div class="table-lt table-lt-body">
                        <div class="tr" data-time="1660070667">
                            <div class="td"><label>Время</label><span class="date">12 секунд назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/ripple.svg" class="coin protip" data-pt-title="Ripple" alt="">XRP</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/trueusd.svg" class="coin protip" data-pt-title="TrueUSD" alt="">TUSD</span></div></div>
                            <div class="td"><label>Количество</label>9 275</div>
                        </div>
                        <div class="tr" data-time="1660070652">
                            <div class="td"><label>Время</label><span class="date">27 секунд назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/dash.svg" class="coin protip" data-pt-title="Dash" alt="">DASH</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/visamastercard.svg" class="coin protip" data-pt-title="Visa MasterCard" alt="">UAH</span></div></div>
                            <div class="td"><label>Количество</label>125.52</div>
                        </div>
                        <div class="tr" data-time="1660070650">
                            <div class="td"><label>Время</label><span class="date">29 секунд назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/tether.svg" class="coin protip" data-pt-title="Tether ERC-20" alt="">USDT</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/litecoin.svg" class="coin protip" data-pt-title="Litecoin" alt="">LTC</span></div></div>
                            <div class="td"><label>Количество</label>690</div>
                        </div>
                        <div class="tr" data-time="1660070648">
                            <div class="td"><label>Время</label><span class="date">31 секунда назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/litecoin.svg" class="coin protip" data-pt-title="Litecoin" alt="">LTC</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/bitcoincash.svg" class="coin protip" data-pt-title="Bitcoin Cash" alt="">BCH</span></div></div>
                            <div class="td"><label>Количество</label>81.65</div>
                        </div>
                        <div class="tr" data-time="1660070641">
                            <div class="td"><label>Время</label><span class="date">38 секунд назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/ukrsibbank.svg" class="coin protip" data-pt-title="UkrSibbank" alt="">RUB</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/payeer.svg" class="coin protip" data-pt-title="Payeer" alt="">USD</span></div></div>
                            <div class="td"><label>Количество</label>94 859</div>
                        </div>
                        <div class="tr" data-time="1660070594">
                            <div class="td"><label>Время</label><span class="date">1 минута назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/eos.svg" class="coin protip" data-pt-title="EOS" alt="">EOS</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/litecoin.svg" class="coin protip" data-pt-title="Litecoin" alt="">LTC</span></div></div>
                            <div class="td"><label>Количество</label>2 106.56</div>
                        </div>
                        <div class="tr" data-time="1660070593">
                            <div class="td"><label>Время</label><span class="date">1 минута назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/perfectmoney.svg" class="coin protip" data-pt-title="Perfect Money" alt="">USD</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/tetheromni.svg" class="coin protip" data-pt-title="Tether Omni" alt="">USDT</span></div></div>
                            <div class="td"><label>Количество</label>4 287</div>
                        </div>
                        <div class="tr" data-time="1660070532">
                            <div class="td"><label>Время</label><span class="date">2 минуты назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/zcash.svg" class="coin protip" data-pt-title="Zcash" alt="">ZEC</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/acash.svg" class="coin protip" data-pt-title="AdvCash" alt="">RUB</span></div></div>
                            <div class="td"><label>Количество</label>17</div>
                        </div>
                        <div class="tr" data-time="1660070528">
                            <div class="td"><label>Время</label><span class="date">2 минуты назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/vtb.svg" class="coin protip" data-pt-title="VTB" alt="">RUB</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/binance.svg" class="coin protip" data-pt-title="Binance Coin" alt="">BNB</span></div></div>
                            <div class="td"><label>Количество</label>302 510</div>
                        </div>
                        <div class="tr" data-time="1660070511">
                            <div class="td"><label>Время</label><span class="date">2 минуты назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/acash.svg" class="coin protip" data-pt-title="AdvCash" alt="">RUB</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/oschadbank.svg" class="coin protip" data-pt-title="Oshchadbank" alt="">RUB</span></div></div>
                            <div class="td"><label>Количество</label>322 980</div>
                        </div>
                        <div class="tr" data-time="1660070501">
                            <div class="td"><label>Время</label><span class="date">2 минуты назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/acash.svg" class="coin protip" data-pt-title="AdvCash" alt="">KZT</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/ethereum.svg" class="coin protip" data-pt-title="Ethereum" alt="">ETH</span></div></div>
                            <div class="td"><label>Количество</label>905 576</div>
                        </div>
                        <div class="tr" data-time="1660070372">
                            <div class="td"><label>Время</label><span class="date">5 минут назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/perfectmoney.svg" class="coin protip" data-pt-title="Perfect Money" alt="">USD</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/cardano.svg" class="coin protip" data-pt-title="Cardano" alt="">ADA</span></div></div>
                            <div class="td"><label>Количество</label>980</div>
                        </div>
                        <div class="tr" data-time="1660070296">
                            <div class="td"><label>Время</label><span class="date">6 минут назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/visamastercard.svg" class="coin protip" data-pt-title="Visa MasterCard" alt="">UAH</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/visamastercard.svg" class="coin protip" data-pt-title="Visa MasterCard" alt="">EUR</span></div></div>
                            <div class="td"><label>Количество</label>249 157</div>
                        </div>
                        <div class="tr" data-time="1660070195">
                            <div class="td"><label>Время</label><span class="date">8 минут назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/payeer.svg" class="coin protip" data-pt-title="Payeer" alt="">USD</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/bitcoincash.svg" class="coin protip" data-pt-title="Bitcoin Cash" alt="">BCH</span></div></div>
                            <div class="td"><label>Количество</label>6 000</div>
                        </div>
                        <div class="tr" data-time="1660070182">
                            <div class="td"><label>Время</label><span class="date">8 минут назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/cardano.svg" class="coin protip" data-pt-title="Cardano" alt="">ADA</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/nem.svg" class="coin protip" data-pt-title="NEM" alt="">XEM</span></div></div>
                            <div class="td"><label>Количество</label>3 369.76</div>
                        </div>
                        <div class="tr" data-time="1660070146">
                            <div class="td"><label>Время</label><span class="date">8 минут назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/visamastercard.svg" class="coin protip" data-pt-title="Visa MasterCard" alt="">UAH</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/bitcoincash.svg" class="coin protip" data-pt-title="Bitcoin Cash" alt="">BCH</span></div></div>
                            <div class="td"><label>Количество</label>220 767</div>
                        </div>
                        <div class="tr" data-time="1660070102">
                            <div class="td"><label>Время</label><span class="date">9 минут назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/stellar.svg" class="coin protip" data-pt-title="Stellar" alt="">XLM</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/binance.svg" class="coin protip" data-pt-title="Binance Coin" alt="">BNB</span></div></div>
                            <div class="td"><label>Количество</label>20 755</div>
                        </div>
                        <div class="tr" data-time="1660070067">
                            <div class="td"><label>Время</label><span class="date">10 минут назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/visamastercard.svg" class="coin protip" data-pt-title="Visa MasterCard" alt="">EUR</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/tether.svg" class="coin protip" data-pt-title="Tether ERC-20" alt="">USDT</span></div></div>
                            <div class="td"><label>Количество</label>657</div>
                        </div>
                        <div class="tr" data-time="1660069837">
                            <div class="td"><label>Время</label><span class="date">14 минут назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/ukrsibbank.svg" class="coin protip" data-pt-title="UkrSibbank" alt="">RUB</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/visamastercard.svg" class="coin protip" data-pt-title="Visa MasterCard" alt="">USD</span></div></div>
                            <div class="td"><label>Количество</label>99 085</div>
                        </div>
                        <div class="tr" data-time="1660069817">
                            <div class="td"><label>Время</label><span class="date">14 минут назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/acash.svg" class="coin protip" data-pt-title="AdvCash" alt="">EUR</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/chainlink.svg" class="coin protip" data-pt-title="Chainlink" alt="">LINK</span></div></div>
                            <div class="td"><label>Количество</label>739.8</div>
                        </div>
                        <div class="tr" data-time="1660069809">
                            <div class="td"><label>Время</label><span class="date">14 минут назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/payeer.svg" class="coin protip" data-pt-title="Payeer" alt="">EUR</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/payeer.svg" class="coin protip" data-pt-title="Payeer" alt="">RUB</span></div></div>
                            <div class="td"><label>Количество</label>4 915</div>
                        </div>
                        <div class="tr" data-time="1660069747">
                            <div class="td"><label>Время</label><span class="date">15 минут назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/payeer.svg" class="coin protip" data-pt-title="Payeer" alt="">RUB</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/eos.svg" class="coin protip" data-pt-title="EOS" alt="">EOS</span></div></div>
                            <div class="td"><label>Количество</label>298 887</div>
                        </div>
                        <div class="tr" data-time="1660069695">
                            <div class="td"><label>Время</label><span class="date">16 минут назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/vtb.svg" class="coin protip" data-pt-title="VTB" alt="">RUB</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/chainlink.svg" class="coin protip" data-pt-title="Chainlink" alt="">LINK</span></div></div>
                            <div class="td"><label>Количество</label>344 113</div>
                        </div>
                        <div class="tr" data-time="1660069691">
                            <div class="td"><label>Время</label><span class="date">16 минут назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/ukrsibbank.svg" class="coin protip" data-pt-title="UkrSibbank" alt="">RUB</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/oschadbank.svg" class="coin protip" data-pt-title="Oshchadbank" alt="">RUB</span></div></div>
                            <div class="td"><label>Количество</label>411 076</div>
                        </div>
                        <div class="tr" data-time="1660069625">
                            <div class="td"><label>Время</label><span class="date">17 минут назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/bittorrent.svg" class="coin protip" data-pt-title="BitTorrent" alt="">BTT</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/tron.svg" class="coin protip" data-pt-title="TRON" alt="">TRX</span></div></div>
                            <div class="td"><label>Количество</label>1 792 221.82</div>
                        </div>
                        <div class="tr" data-time="1660069613">
                            <div class="td"><label>Время</label><span class="date">17 минут назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/acash.svg" class="coin protip" data-pt-title="AdvCash" alt="">KZT</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/tether.svg" class="coin protip" data-pt-title="Tether ERC-20" alt="">USDT</span></div></div>
                            <div class="td"><label>Количество</label>3 031 299</div>
                        </div>
                        <div class="tr" data-time="1660069565">
                            <div class="td"><label>Время</label><span class="date">18 минут назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/stellar.svg" class="coin protip" data-pt-title="Stellar" alt="">XLM</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/bittorrent.svg" class="coin protip" data-pt-title="BitTorrent" alt="">BTT</span></div></div>
                            <div class="td"><label>Количество</label>20 657</div>
                        </div>
                        <div class="tr" data-time="1660069563">
                            <div class="td"><label>Время</label><span class="date">18 минут назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/payeer.svg" class="coin protip" data-pt-title="Payeer" alt="">EUR</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/acash.svg" class="coin protip" data-pt-title="AdvCash" alt="">UAH</span></div></div>
                            <div class="td"><label>Количество</label>2 247.84</div>
                        </div>
                        <div class="tr" data-time="1660069498">
                            <div class="td"><label>Время</label><span class="date">19 минут назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/ethereum.svg" class="coin protip" data-pt-title="Ethereum" alt="">ETH</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/chainlink.svg" class="coin protip" data-pt-title="Chainlink" alt="">LINK</span></div></div>
                            <div class="td"><label>Количество</label>5.8</div>
                        </div>
                        <div class="tr" data-time="1660069488">
                            <div class="td"><label>Время</label><span class="date">19 минут назад</span></div>
                            <div class="td"><div class="exch"><span class="nw"><img src="img/pm2/raiffeisen.svg" class="coin protip" data-pt-title="Raiffeisenbank" alt="">RUB</span><span class="icon-arrow-2"></span><span class="nw"><img src="img/pm2/usdcoin.svg" class="coin protip" data-pt-title="USD Coin" alt="">USDC</span></div></div>
                            <div class="td"><label>Количество</label>124 747</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
