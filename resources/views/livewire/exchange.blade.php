<div>
{{--    @if($errors->any())--}}
{{--        <div class="row">--}}
{{--            <div class="col-12">--}}
{{--                <ul class="list-group bg-danger">--}}
{{--                    @foreach ($errors->all() as $error)--}}
{{--                        <li class="list-group-item text-danger">{{$error}}</li>--}}
{{--                    @endforeach--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--    @endif--}}
    <form id="trade">
        <div class="overlay"></div>
        <div class="trade_in">

            <div class="trade-part part-1">
                <h2 class="text-header">{{ __('site.give') }}</h2>
                <div class="pm_select send">
                    <div class="btn">
                        <div class="pm_select_img"><img src="{{ $coinFrom->icon }}" alt="pm"></div>
                        <span>{{ $coinFrom->name }}</span>
                        <svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7 10l5 5 5-5H7z" fill="#000"></path></svg>
                    </div>
                    <div class="dropdown">
                        <div class="dropdown_inner">
                            @forelse($availableCoinsFrom as $coin)
                                <div wire:click="selectCoinFrom({{ $coin->id }})" class="option" data-id="9" data-cur="BTC" id="send_option_9">
                                    <div class="pm_select_img"><img src="{{ $coin->icon }}" alt="pm"></div><span>{{ $coin->name }}</span>
                                    <div class="pm_childs">
                                    </div>
                                </div>
                            @empty
                            @endforelse
                        </div>
                    </div>
                </div>
                <div class="currency_select">
                    <a class="pm_select_child active" href="#" data-child_id="9" data-cur="BTC">{{ $coinFrom->symbol }}</a>
                </div>
                <div class="amount_input">
                    <div class="input-1 ">
                        <input wire:model="fromData.amountFrom" type="text" inputmode="numeric" name="amount_send" id="amount_send" class="dirty amountFrom @error('fromData.amountFrom') is-invalid @enderror" maxlength="20">
                        @error('fromData.amountFrom')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <div class="addons">{{ $coinFrom->symbol }}</div>
                    </div>
                    <div class="input-1 ">
                        <input wire:model="fromData.amountFromUsd" type="text" inputmode="numeric" name="amount_send_usd" id="amount_send_usd" class="dirty amountFrom @error('fromData.amountFromUsd') is-invalid @enderror" maxlength="20">
                        @error('fromData.amountFromUsd')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <div class="addons">$</div>
                    </div>
                </div>
                <aside>
                    <div class="wrapper">
                        <p class="range">
                            <span>Min: <span class="send_min">{{ $coinFrom->min }} {{ $coinFrom->symbol }}</span></span>
                            <span>Max: <span class="send_max">{{ $coinFrom->max }} {{ $coinFrom->symbol }}</span></span>
                            <br>
                            <span>1 {{ $coinFrom->symbol }} = {{ $coinFrom->usd_price }} USD</span>
                        </p>
                        <p class="rate_info"></p>
                    </div>
                </aside>
            </div>

            <div class="trade-part part-2">
                <h4 class="text-header-2">{{ __('exchange.requisites_sender') }}</h4>
                <div class="form-group">
                    <div class="field">
                        <input wire:model="fromData.email" class="dirty @error('fromData.email') is-invalid @enderror" name="email">
                        <div class="placeholder">Email</div>
                        @error('fromData.email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
{{--                <div class="form-group" id="field_account_send_2" style="display: none">--}}
{{--                    <div class="field">--}}
{{--                        <input type="text" name="account_send_2" value="">--}}
{{--                        <div class="placeholder"></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="form-group" id="field_account_send" style="display: none">--}}
{{--                    <div class="field">--}}
{{--                        <input type="text" name="account_send" value="">--}}
{{--                        <div class="placeholder"></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>

            <div class="trade-part__separator"></div>

            <div class="trade-part part-3">
                <h2 class="text-header">{{ __('site.receive') }}</h2>
                <div class="pm_select get">
                    <div class="btn">
                        <div class="pm_select_img"><img src="{{ $coinTo->icon }}" alt="pm"></div>
                        <span>{{ $coinTo->name }}</span>
                        <svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7 10l5 5 5-5H7z" fill="#000"></path></svg>
                    </div>
                    <div class="dropdown">
                        <div class="dropdown_inner">
                            @forelse($availableCoinsTo as $coin)
                                <div wire:click="selectCoinTo({{ $coin->id }})" class="option" data-id="9" data-cur="BTC" id="send_option_9">
                                    <div class="pm_select_img"><img src="{{ $coin->icon }}" alt="pm"></div><span>{{ $coin->name }}</span>
                                    <div class="pm_childs">
                                    </div>
                                </div>
                            @empty
                            @endforelse
                        </div>
                    </div>
                </div>
                <div class="currency_select">
                    <a class="pm_select_child active" href="#" data-child_id="3" data-cur="RUB">{{ $coinTo->symbol }}</a>
                </div>
                <div class="amount_input">
                    <div class="input-1">
                        <input wire:model="toData.amountTo" type="text" inputmode="numeric" name="amount_recive" id="amount_get " class="dirty amountTo" maxlength="20">
                        <div class="addons">{{ $coinTo->symbol }}</div>
                    </div>
                </div>
                <aside class="vertical">
                    <p class="get_reserves">{{ __('site.reserves') }}: <span>{{ $coinTo->max }} {{ $coinTo->symbol }}</span></p>
                </aside>
                <div class="exchange_comment">
                    {{ $coinFrom->name }} - {{ $coinFrom->description }}
                </div>
            </div>
            <div class="trade-part part-4">
                <h4 class="text-header-2">{{ __('exchange.requisites_recipment') }}</h4>
                <div class="form-group" id="field_account_recive" style="">
                    <div class="field">
                        <input wire:model="toData.addressTo" class="@error('toData.addressTo') is-invalid @enderror" type="text" name="account_recive">
                        @error('toData.addressTo')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <div class="placeholder">{{ __('site.wallet') }} {{ $coinTo->name }}</div>
                    </div>
                </div>
                <div class="trade_submit">
                    <div class="terms">{{ __('exchange.term') }} <a href="{{ route('terms') }}" target="_blank" rel="noreferrer noopener">{{ __('site.user_agreement') }}</a>
                        {{ __('site.our_resource') }}</div>
                    <button wire:click.prevent="exchange" class="btn-submit"><span> {{ __('site.exchange') }}</span></button>
                </div>
            </div>
        </div>
    </form>
    <!--/#trade-->

</div>
@section('js')
    <script>
        $(document).find('.pm_select .btn').click(function () {
            let ps = $(this).closest('.pm_select');
            let d = ps.find('.dropdown');
            if (ps.hasClass('is-active')) {
                ps.removeClass('is-active');
                d.hide()
            } else {
                let pa = $('.pm_select.is-active');
                if (pa.length) {
                    pa.removeClass('is-active').find('.dropdown').hide()
                }
                ps.addClass('is-active');
                d.show()
            }
        });
    </script>
    <script>
        $('.amountFrom').on('input', function() {
            this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');
        });

        $('.amountTo').on('input', function() {
            this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');
        });
    </script>
@endsection
{{--    @section('some_content')--}}
{{--        @include('livewire.last-transactions')--}}
{{--    @endsection--}}
