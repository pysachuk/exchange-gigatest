/*! jQuery v3.4.1 | (c) JS Foundation and other contributors | jquery.org/license */
!function (e, t) {
    "use strict";
    "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function (e) {
        if (!e.document) throw new Error("jQuery requires a window with a document");
        return t(e)
    } : t(e)
}("undefined" != typeof window ? window : this, function (C, e) {
    "use strict";
    var t = [], E = C.document, r = Object.getPrototypeOf, s = t.slice, g = t.concat, u = t.push, i = t.indexOf, n = {},
        o = n.toString, v = n.hasOwnProperty, a = v.toString, l = a.call(Object), y = {}, m = function (e) {
            return "function" == typeof e && "number" != typeof e.nodeType
        }, x = function (e) {
            return null != e && e === e.window
        }, c = {type: !0, src: !0, nonce: !0, noModule: !0};

    function b(e, t, n) {
        var r, i, o = (n = n || E).createElement("script");
        if (o.text = e, t) for (r in c) (i = t[r] || t.getAttribute && t.getAttribute(r)) && o.setAttribute(r, i);
        n.head.appendChild(o).parentNode.removeChild(o)
    }

    function w(e) {
        return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? n[o.call(e)] || "object" : typeof e
    }

    var f = "3.4.1", k = function (e, t) {
        return new k.fn.init(e, t)
    }, p = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;

    function d(e) {
        var t = !!e && "length" in e && e.length, n = w(e);
        return !m(e) && !x(e) && ("array" === n || 0 === t || "number" == typeof t && 0 < t && t - 1 in e)
    }

    k.fn = k.prototype = {
        jquery: f, constructor: k, length: 0, toArray: function () {
            return s.call(this)
        }, get: function (e) {
            return null == e ? s.call(this) : e < 0 ? this[e + this.length] : this[e]
        }, pushStack: function (e) {
            var t = k.merge(this.constructor(), e);
            return t.prevObject = this, t
        }, each: function (e) {
            return k.each(this, e)
        }, map: function (n) {
            return this.pushStack(k.map(this, function (e, t) {
                return n.call(e, t, e)
            }))
        }, slice: function () {
            return this.pushStack(s.apply(this, arguments))
        }, first: function () {
            return this.eq(0)
        }, last: function () {
            return this.eq(-1)
        }, eq: function (e) {
            var t = this.length, n = +e + (e < 0 ? t : 0);
            return this.pushStack(0 <= n && n < t ? [this[n]] : [])
        }, end: function () {
            return this.prevObject || this.constructor()
        }, push: u, sort: t.sort, splice: t.splice
    }, k.extend = k.fn.extend = function () {
        var e, t, n, r, i, o, a = arguments[0] || {}, s = 1, u = arguments.length, l = !1;
        for ("boolean" == typeof a && (l = a, a = arguments[s] || {}, s++), "object" == typeof a || m(a) || (a = {}), s === u && (a = this, s--); s < u; s++) if (null != (e = arguments[s])) for (t in e) r = e[t], "__proto__" !== t && a !== r && (l && r && (k.isPlainObject(r) || (i = Array.isArray(r))) ? (n = a[t], o = i && !Array.isArray(n) ? [] : i || k.isPlainObject(n) ? n : {}, i = !1, a[t] = k.extend(l, o, r)) : void 0 !== r && (a[t] = r));
        return a
    }, k.extend({
        expando: "jQuery" + (f + Math.random()).replace(/\D/g, ""), isReady: !0, error: function (e) {
            throw new Error(e)
        }, noop: function () {
        }, isPlainObject: function (e) {
            var t, n;
            return !(!e || "[object Object]" !== o.call(e)) && (!(t = r(e)) || "function" == typeof (n = v.call(t, "constructor") && t.constructor) && a.call(n) === l)
        }, isEmptyObject: function (e) {
            var t;
            for (t in e) return !1;
            return !0
        }, globalEval: function (e, t) {
            b(e, {nonce: t && t.nonce})
        }, each: function (e, t) {
            var n, r = 0;
            if (d(e)) {
                for (n = e.length; r < n; r++) if (!1 === t.call(e[r], r, e[r])) break
            } else for (r in e) if (!1 === t.call(e[r], r, e[r])) break;
            return e
        }, trim: function (e) {
            return null == e ? "" : (e + "").replace(p, "")
        }, makeArray: function (e, t) {
            var n = t || [];
            return null != e && (d(Object(e)) ? k.merge(n, "string" == typeof e ? [e] : e) : u.call(n, e)), n
        }, inArray: function (e, t, n) {
            return null == t ? -1 : i.call(t, e, n)
        }, merge: function (e, t) {
            for (var n = +t.length, r = 0, i = e.length; r < n; r++) e[i++] = t[r];
            return e.length = i, e
        }, grep: function (e, t, n) {
            for (var r = [], i = 0, o = e.length, a = !n; i < o; i++) !t(e[i], i) !== a && r.push(e[i]);
            return r
        }, map: function (e, t, n) {
            var r, i, o = 0, a = [];
            if (d(e)) for (r = e.length; o < r; o++) null != (i = t(e[o], o, n)) && a.push(i); else for (o in e) null != (i = t(e[o], o, n)) && a.push(i);
            return g.apply([], a)
        }, guid: 1, support: y
    }), "function" == typeof Symbol && (k.fn[Symbol.iterator] = t[Symbol.iterator]), k.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (e, t) {
        n["[object " + t + "]"] = t.toLowerCase()
    });
    var h = function (n) {
        var e, d, b, o, i, h, f, g, w, u, l, T, C, a, E, v, s, c, y, k = "sizzle" + 1 * new Date, m = n.document, S = 0,
            r = 0, p = ue(), x = ue(), N = ue(), A = ue(), D = function (e, t) {
                return e === t && (l = !0), 0
            }, j = {}.hasOwnProperty, t = [], q = t.pop, L = t.push, H = t.push, O = t.slice, P = function (e, t) {
                for (var n = 0, r = e.length; n < r; n++) if (e[n] === t) return n;
                return -1
            },
            R = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            M = "[\\x20\\t\\r\\n\\f]", I = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
            W = "\\[" + M + "*(" + I + ")(?:" + M + "*([*^$|!~]?=)" + M + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + I + "))|)" + M + "*\\]",
            $ = ":(" + I + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + W + ")*)|.*)\\)|)",
            F = new RegExp(M + "+", "g"), B = new RegExp("^" + M + "+|((?:^|[^\\\\])(?:\\\\.)*)" + M + "+$", "g"),
            _ = new RegExp("^" + M + "*," + M + "*"), z = new RegExp("^" + M + "*([>+~]|" + M + ")" + M + "*"),
            U = new RegExp(M + "|>"), X = new RegExp($), V = new RegExp("^" + I + "$"), G = {
                ID: new RegExp("^#(" + I + ")"),
                CLASS: new RegExp("^\\.(" + I + ")"),
                TAG: new RegExp("^(" + I + "|[*])"),
                ATTR: new RegExp("^" + W),
                PSEUDO: new RegExp("^" + $),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + M + "*(even|odd|(([+-]|)(\\d*)n|)" + M + "*(?:([+-]|)" + M + "*(\\d+)|))" + M + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + R + ")$", "i"),
                needsContext: new RegExp("^" + M + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + M + "*((?:-\\d)?\\d*)" + M + "*\\)|)(?=[^-]|$)", "i")
            }, Y = /HTML$/i, Q = /^(?:input|select|textarea|button)$/i, J = /^h\d$/i, K = /^[^{]+\{\s*\[native \w/,
            Z = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, ee = /[+~]/,
            te = new RegExp("\\\\([\\da-f]{1,6}" + M + "?|(" + M + ")|.)", "ig"), ne = function (e, t, n) {
                var r = "0x" + t - 65536;
                return r != r || n ? t : r < 0 ? String.fromCharCode(r + 65536) : String.fromCharCode(r >> 10 | 55296, 1023 & r | 56320)
            }, re = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g, ie = function (e, t) {
                return t ? "\0" === e ? "\ufffd" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e
            }, oe = function () {
                T()
            }, ae = be(function (e) {
                return !0 === e.disabled && "fieldset" === e.nodeName.toLowerCase()
            }, {dir: "parentNode", next: "legend"});
        try {
            H.apply(t = O.call(m.childNodes), m.childNodes), t[m.childNodes.length].nodeType
        } catch (e) {
            H = {
                apply: t.length ? function (e, t) {
                    L.apply(e, O.call(t))
                } : function (e, t) {
                    var n = e.length, r = 0;
                    while (e[n++] = t[r++]) ;
                    e.length = n - 1
                }
            }
        }

        function se(t, e, n, r) {
            var i, o, a, s, u, l, c, f = e && e.ownerDocument, p = e ? e.nodeType : 9;
            if (n = n || [], "string" != typeof t || !t || 1 !== p && 9 !== p && 11 !== p) return n;
            if (!r && ((e ? e.ownerDocument || e : m) !== C && T(e), e = e || C, E)) {
                if (11 !== p && (u = Z.exec(t))) if (i = u[1]) {
                    if (9 === p) {
                        if (!(a = e.getElementById(i))) return n;
                        if (a.id === i) return n.push(a), n
                    } else if (f && (a = f.getElementById(i)) && y(e, a) && a.id === i) return n.push(a), n
                } else {
                    if (u[2]) return H.apply(n, e.getElementsByTagName(t)), n;
                    if ((i = u[3]) && d.getElementsByClassName && e.getElementsByClassName) return H.apply(n, e.getElementsByClassName(i)), n
                }
                if (d.qsa && !A[t + " "] && (!v || !v.test(t)) && (1 !== p || "object" !== e.nodeName.toLowerCase())) {
                    if (c = t, f = e, 1 === p && U.test(t)) {
                        (s = e.getAttribute("id")) ? s = s.replace(re, ie) : e.setAttribute("id", s = k), o = (l = h(t)).length;
                        while (o--) l[o] = "#" + s + " " + xe(l[o]);
                        c = l.join(","), f = ee.test(t) && ye(e.parentNode) || e
                    }
                    try {
                        return H.apply(n, f.querySelectorAll(c)), n
                    } catch (e) {
                        A(t, !0)
                    } finally {
                        s === k && e.removeAttribute("id")
                    }
                }
            }
            return g(t.replace(B, "$1"), e, n, r)
        }

        function ue() {
            var r = [];
            return function e(t, n) {
                return r.push(t + " ") > b.cacheLength && delete e[r.shift()], e[t + " "] = n
            }
        }

        function le(e) {
            return e[k] = !0, e
        }

        function ce(e) {
            var t = C.createElement("fieldset");
            try {
                return !!e(t)
            } catch (e) {
                return !1
            } finally {
                t.parentNode && t.parentNode.removeChild(t), t = null
            }
        }

        function fe(e, t) {
            var n = e.split("|"), r = n.length;
            while (r--) b.attrHandle[n[r]] = t
        }

        function pe(e, t) {
            var n = t && e, r = n && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
            if (r) return r;
            if (n) while (n = n.nextSibling) if (n === t) return -1;
            return e ? 1 : -1
        }

        function de(t) {
            return function (e) {
                return "input" === e.nodeName.toLowerCase() && e.type === t
            }
        }

        function he(n) {
            return function (e) {
                var t = e.nodeName.toLowerCase();
                return ("input" === t || "button" === t) && e.type === n
            }
        }

        function ge(t) {
            return function (e) {
                return "form" in e ? e.parentNode && !1 === e.disabled ? "label" in e ? "label" in e.parentNode ? e.parentNode.disabled === t : e.disabled === t : e.isDisabled === t || e.isDisabled !== !t && ae(e) === t : e.disabled === t : "label" in e && e.disabled === t
            }
        }

        function ve(a) {
            return le(function (o) {
                return o = +o, le(function (e, t) {
                    var n, r = a([], e.length, o), i = r.length;
                    while (i--) e[n = r[i]] && (e[n] = !(t[n] = e[n]))
                })
            })
        }

        function ye(e) {
            return e && "undefined" != typeof e.getElementsByTagName && e
        }

        for (e in d = se.support = {}, i = se.isXML = function (e) {
            var t = e.namespaceURI, n = (e.ownerDocument || e).documentElement;
            return !Y.test(t || n && n.nodeName || "HTML")
        }, T = se.setDocument = function (e) {
            var t, n, r = e ? e.ownerDocument || e : m;
            return r !== C && 9 === r.nodeType && r.documentElement && (a = (C = r).documentElement, E = !i(C), m !== C && (n = C.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", oe, !1) : n.attachEvent && n.attachEvent("onunload", oe)), d.attributes = ce(function (e) {
                return e.className = "i", !e.getAttribute("className")
            }), d.getElementsByTagName = ce(function (e) {
                return e.appendChild(C.createComment("")), !e.getElementsByTagName("*").length
            }), d.getElementsByClassName = K.test(C.getElementsByClassName), d.getById = ce(function (e) {
                return a.appendChild(e).id = k, !C.getElementsByName || !C.getElementsByName(k).length
            }), d.getById ? (b.filter.ID = function (e) {
                var t = e.replace(te, ne);
                return function (e) {
                    return e.getAttribute("id") === t
                }
            }, b.find.ID = function (e, t) {
                if ("undefined" != typeof t.getElementById && E) {
                    var n = t.getElementById(e);
                    return n ? [n] : []
                }
            }) : (b.filter.ID = function (e) {
                var n = e.replace(te, ne);
                return function (e) {
                    var t = "undefined" != typeof e.getAttributeNode && e.getAttributeNode("id");
                    return t && t.value === n
                }
            }, b.find.ID = function (e, t) {
                if ("undefined" != typeof t.getElementById && E) {
                    var n, r, i, o = t.getElementById(e);
                    if (o) {
                        if ((n = o.getAttributeNode("id")) && n.value === e) return [o];
                        i = t.getElementsByName(e), r = 0;
                        while (o = i[r++]) if ((n = o.getAttributeNode("id")) && n.value === e) return [o]
                    }
                    return []
                }
            }), b.find.TAG = d.getElementsByTagName ? function (e, t) {
                return "undefined" != typeof t.getElementsByTagName ? t.getElementsByTagName(e) : d.qsa ? t.querySelectorAll(e) : void 0
            } : function (e, t) {
                var n, r = [], i = 0, o = t.getElementsByTagName(e);
                if ("*" === e) {
                    while (n = o[i++]) 1 === n.nodeType && r.push(n);
                    return r
                }
                return o
            }, b.find.CLASS = d.getElementsByClassName && function (e, t) {
                if ("undefined" != typeof t.getElementsByClassName && E) return t.getElementsByClassName(e)
            }, s = [], v = [], (d.qsa = K.test(C.querySelectorAll)) && (ce(function (e) {
                a.appendChild(e).innerHTML = "<a id='" + k + "'></a><select id='" + k + "-\r\\' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && v.push("[*^$]=" + M + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || v.push("\\[" + M + "*(?:value|" + R + ")"), e.querySelectorAll("[id~=" + k + "-]").length || v.push("~="), e.querySelectorAll(":checked").length || v.push(":checked"), e.querySelectorAll("a#" + k + "+*").length || v.push(".#.+[+~]")
            }), ce(function (e) {
                e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                var t = C.createElement("input");
                t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && v.push("name" + M + "*[*^$|!~]?="), 2 !== e.querySelectorAll(":enabled").length && v.push(":enabled", ":disabled"), a.appendChild(e).disabled = !0, 2 !== e.querySelectorAll(":disabled").length && v.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), v.push(",.*:")
            })), (d.matchesSelector = K.test(c = a.matches || a.webkitMatchesSelector || a.mozMatchesSelector || a.oMatchesSelector || a.msMatchesSelector)) && ce(function (e) {
                d.disconnectedMatch = c.call(e, "*"), c.call(e, "[s!='']:x"), s.push("!=", $)
            }), v = v.length && new RegExp(v.join("|")), s = s.length && new RegExp(s.join("|")), t = K.test(a.compareDocumentPosition), y = t || K.test(a.contains) ? function (e, t) {
                var n = 9 === e.nodeType ? e.documentElement : e, r = t && t.parentNode;
                return e === r || !(!r || 1 !== r.nodeType || !(n.contains ? n.contains(r) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(r)))
            } : function (e, t) {
                if (t) while (t = t.parentNode) if (t === e) return !0;
                return !1
            }, D = t ? function (e, t) {
                if (e === t) return l = !0, 0;
                var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
                return n || (1 & (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1) || !d.sortDetached && t.compareDocumentPosition(e) === n ? e === C || e.ownerDocument === m && y(m, e) ? -1 : t === C || t.ownerDocument === m && y(m, t) ? 1 : u ? P(u, e) - P(u, t) : 0 : 4 & n ? -1 : 1)
            } : function (e, t) {
                if (e === t) return l = !0, 0;
                var n, r = 0, i = e.parentNode, o = t.parentNode, a = [e], s = [t];
                if (!i || !o) return e === C ? -1 : t === C ? 1 : i ? -1 : o ? 1 : u ? P(u, e) - P(u, t) : 0;
                if (i === o) return pe(e, t);
                n = e;
                while (n = n.parentNode) a.unshift(n);
                n = t;
                while (n = n.parentNode) s.unshift(n);
                while (a[r] === s[r]) r++;
                return r ? pe(a[r], s[r]) : a[r] === m ? -1 : s[r] === m ? 1 : 0
            }), C
        }, se.matches = function (e, t) {
            return se(e, null, null, t)
        }, se.matchesSelector = function (e, t) {
            if ((e.ownerDocument || e) !== C && T(e), d.matchesSelector && E && !A[t + " "] && (!s || !s.test(t)) && (!v || !v.test(t))) try {
                var n = c.call(e, t);
                if (n || d.disconnectedMatch || e.document && 11 !== e.document.nodeType) return n
            } catch (e) {
                A(t, !0)
            }
            return 0 < se(t, C, null, [e]).length
        }, se.contains = function (e, t) {
            return (e.ownerDocument || e) !== C && T(e), y(e, t)
        }, se.attr = function (e, t) {
            (e.ownerDocument || e) !== C && T(e);
            var n = b.attrHandle[t.toLowerCase()],
                r = n && j.call(b.attrHandle, t.toLowerCase()) ? n(e, t, !E) : void 0;
            return void 0 !== r ? r : d.attributes || !E ? e.getAttribute(t) : (r = e.getAttributeNode(t)) && r.specified ? r.value : null
        }, se.escape = function (e) {
            return (e + "").replace(re, ie)
        }, se.error = function (e) {
            throw new Error("Syntax error, unrecognized expression: " + e)
        }, se.uniqueSort = function (e) {
            var t, n = [], r = 0, i = 0;
            if (l = !d.detectDuplicates, u = !d.sortStable && e.slice(0), e.sort(D), l) {
                while (t = e[i++]) t === e[i] && (r = n.push(i));
                while (r--) e.splice(n[r], 1)
            }
            return u = null, e
        }, o = se.getText = function (e) {
            var t, n = "", r = 0, i = e.nodeType;
            if (i) {
                if (1 === i || 9 === i || 11 === i) {
                    if ("string" == typeof e.textContent) return e.textContent;
                    for (e = e.firstChild; e; e = e.nextSibling) n += o(e)
                } else if (3 === i || 4 === i) return e.nodeValue
            } else while (t = e[r++]) n += o(t);
            return n
        }, (b = se.selectors = {
            cacheLength: 50,
            createPseudo: le,
            match: G,
            attrHandle: {},
            find: {},
            relative: {
                ">": {dir: "parentNode", first: !0},
                " ": {dir: "parentNode"},
                "+": {dir: "previousSibling", first: !0},
                "~": {dir: "previousSibling"}
            },
            preFilter: {
                ATTR: function (e) {
                    return e[1] = e[1].replace(te, ne), e[3] = (e[3] || e[4] || e[5] || "").replace(te, ne), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                }, CHILD: function (e) {
                    return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || se.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && se.error(e[0]), e
                }, PSEUDO: function (e) {
                    var t, n = !e[6] && e[2];
                    return G.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && X.test(n) && (t = h(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
                }
            },
            filter: {
                TAG: function (e) {
                    var t = e.replace(te, ne).toLowerCase();
                    return "*" === e ? function () {
                        return !0
                    } : function (e) {
                        return e.nodeName && e.nodeName.toLowerCase() === t
                    }
                }, CLASS: function (e) {
                    var t = p[e + " "];
                    return t || (t = new RegExp("(^|" + M + ")" + e + "(" + M + "|$)")) && p(e, function (e) {
                        return t.test("string" == typeof e.className && e.className || "undefined" != typeof e.getAttribute && e.getAttribute("class") || "")
                    })
                }, ATTR: function (n, r, i) {
                    return function (e) {
                        var t = se.attr(e, n);
                        return null == t ? "!=" === r : !r || (t += "", "=" === r ? t === i : "!=" === r ? t !== i : "^=" === r ? i && 0 === t.indexOf(i) : "*=" === r ? i && -1 < t.indexOf(i) : "$=" === r ? i && t.slice(-i.length) === i : "~=" === r ? -1 < (" " + t.replace(F, " ") + " ").indexOf(i) : "|=" === r && (t === i || t.slice(0, i.length + 1) === i + "-"))
                    }
                }, CHILD: function (h, e, t, g, v) {
                    var y = "nth" !== h.slice(0, 3), m = "last" !== h.slice(-4), x = "of-type" === e;
                    return 1 === g && 0 === v ? function (e) {
                        return !!e.parentNode
                    } : function (e, t, n) {
                        var r, i, o, a, s, u, l = y !== m ? "nextSibling" : "previousSibling", c = e.parentNode,
                            f = x && e.nodeName.toLowerCase(), p = !n && !x, d = !1;
                        if (c) {
                            if (y) {
                                while (l) {
                                    a = e;
                                    while (a = a[l]) if (x ? a.nodeName.toLowerCase() === f : 1 === a.nodeType) return !1;
                                    u = l = "only" === h && !u && "nextSibling"
                                }
                                return !0
                            }
                            if (u = [m ? c.firstChild : c.lastChild], m && p) {
                                d = (s = (r = (i = (o = (a = c)[k] || (a[k] = {}))[a.uniqueID] || (o[a.uniqueID] = {}))[h] || [])[0] === S && r[1]) && r[2], a = s && c.childNodes[s];
                                while (a = ++s && a && a[l] || (d = s = 0) || u.pop()) if (1 === a.nodeType && ++d && a === e) {
                                    i[h] = [S, s, d];
                                    break
                                }
                            } else if (p && (d = s = (r = (i = (o = (a = e)[k] || (a[k] = {}))[a.uniqueID] || (o[a.uniqueID] = {}))[h] || [])[0] === S && r[1]), !1 === d) while (a = ++s && a && a[l] || (d = s = 0) || u.pop()) if ((x ? a.nodeName.toLowerCase() === f : 1 === a.nodeType) && ++d && (p && ((i = (o = a[k] || (a[k] = {}))[a.uniqueID] || (o[a.uniqueID] = {}))[h] = [S, d]), a === e)) break;
                            return (d -= v) === g || d % g == 0 && 0 <= d / g
                        }
                    }
                }, PSEUDO: function (e, o) {
                    var t, a = b.pseudos[e] || b.setFilters[e.toLowerCase()] || se.error("unsupported pseudo: " + e);
                    return a[k] ? a(o) : 1 < a.length ? (t = [e, e, "", o], b.setFilters.hasOwnProperty(e.toLowerCase()) ? le(function (e, t) {
                        var n, r = a(e, o), i = r.length;
                        while (i--) e[n = P(e, r[i])] = !(t[n] = r[i])
                    }) : function (e) {
                        return a(e, 0, t)
                    }) : a
                }
            },
            pseudos: {
                not: le(function (e) {
                    var r = [], i = [], s = f(e.replace(B, "$1"));
                    return s[k] ? le(function (e, t, n, r) {
                        var i, o = s(e, null, r, []), a = e.length;
                        while (a--) (i = o[a]) && (e[a] = !(t[a] = i))
                    }) : function (e, t, n) {
                        return r[0] = e, s(r, null, n, i), r[0] = null, !i.pop()
                    }
                }), has: le(function (t) {
                    return function (e) {
                        return 0 < se(t, e).length
                    }
                }), contains: le(function (t) {
                    return t = t.replace(te, ne), function (e) {
                        return -1 < (e.textContent || o(e)).indexOf(t)
                    }
                }), lang: le(function (n) {
                    return V.test(n || "") || se.error("unsupported lang: " + n), n = n.replace(te, ne).toLowerCase(), function (e) {
                        var t;
                        do {
                            if (t = E ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return (t = t.toLowerCase()) === n || 0 === t.indexOf(n + "-")
                        } while ((e = e.parentNode) && 1 === e.nodeType);
                        return !1
                    }
                }), target: function (e) {
                    var t = n.location && n.location.hash;
                    return t && t.slice(1) === e.id
                }, root: function (e) {
                    return e === a
                }, focus: function (e) {
                    return e === C.activeElement && (!C.hasFocus || C.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                }, enabled: ge(!1), disabled: ge(!0), checked: function (e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && !!e.checked || "option" === t && !!e.selected
                }, selected: function (e) {
                    return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected
                }, empty: function (e) {
                    for (e = e.firstChild; e; e = e.nextSibling) if (e.nodeType < 6) return !1;
                    return !0
                }, parent: function (e) {
                    return !b.pseudos.empty(e)
                }, header: function (e) {
                    return J.test(e.nodeName)
                }, input: function (e) {
                    return Q.test(e.nodeName)
                }, button: function (e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && "button" === e.type || "button" === t
                }, text: function (e) {
                    var t;
                    return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                }, first: ve(function () {
                    return [0]
                }), last: ve(function (e, t) {
                    return [t - 1]
                }), eq: ve(function (e, t, n) {
                    return [n < 0 ? n + t : n]
                }), even: ve(function (e, t) {
                    for (var n = 0; n < t; n += 2) e.push(n);
                    return e
                }), odd: ve(function (e, t) {
                    for (var n = 1; n < t; n += 2) e.push(n);
                    return e
                }), lt: ve(function (e, t, n) {
                    for (var r = n < 0 ? n + t : t < n ? t : n; 0 <= --r;) e.push(r);
                    return e
                }), gt: ve(function (e, t, n) {
                    for (var r = n < 0 ? n + t : n; ++r < t;) e.push(r);
                    return e
                })
            }
        }).pseudos.nth = b.pseudos.eq, {
            radio: !0,
            checkbox: !0,
            file: !0,
            password: !0,
            image: !0
        }) b.pseudos[e] = de(e);
        for (e in {submit: !0, reset: !0}) b.pseudos[e] = he(e);

        function me() {
        }

        function xe(e) {
            for (var t = 0, n = e.length, r = ""; t < n; t++) r += e[t].value;
            return r
        }

        function be(s, e, t) {
            var u = e.dir, l = e.next, c = l || u, f = t && "parentNode" === c, p = r++;
            return e.first ? function (e, t, n) {
                while (e = e[u]) if (1 === e.nodeType || f) return s(e, t, n);
                return !1
            } : function (e, t, n) {
                var r, i, o, a = [S, p];
                if (n) {
                    while (e = e[u]) if ((1 === e.nodeType || f) && s(e, t, n)) return !0
                } else while (e = e[u]) if (1 === e.nodeType || f) if (i = (o = e[k] || (e[k] = {}))[e.uniqueID] || (o[e.uniqueID] = {}), l && l === e.nodeName.toLowerCase()) e = e[u] || e; else {
                    if ((r = i[c]) && r[0] === S && r[1] === p) return a[2] = r[2];
                    if ((i[c] = a)[2] = s(e, t, n)) return !0
                }
                return !1
            }
        }

        function we(i) {
            return 1 < i.length ? function (e, t, n) {
                var r = i.length;
                while (r--) if (!i[r](e, t, n)) return !1;
                return !0
            } : i[0]
        }

        function Te(e, t, n, r, i) {
            for (var o, a = [], s = 0, u = e.length, l = null != t; s < u; s++) (o = e[s]) && (n && !n(o, r, i) || (a.push(o), l && t.push(s)));
            return a
        }

        function Ce(d, h, g, v, y, e) {
            return v && !v[k] && (v = Ce(v)), y && !y[k] && (y = Ce(y, e)), le(function (e, t, n, r) {
                var i, o, a, s = [], u = [], l = t.length, c = e || function (e, t, n) {
                        for (var r = 0, i = t.length; r < i; r++) se(e, t[r], n);
                        return n
                    }(h || "*", n.nodeType ? [n] : n, []), f = !d || !e && h ? c : Te(c, s, d, n, r),
                    p = g ? y || (e ? d : l || v) ? [] : t : f;
                if (g && g(f, p, n, r), v) {
                    i = Te(p, u), v(i, [], n, r), o = i.length;
                    while (o--) (a = i[o]) && (p[u[o]] = !(f[u[o]] = a))
                }
                if (e) {
                    if (y || d) {
                        if (y) {
                            i = [], o = p.length;
                            while (o--) (a = p[o]) && i.push(f[o] = a);
                            y(null, p = [], i, r)
                        }
                        o = p.length;
                        while (o--) (a = p[o]) && -1 < (i = y ? P(e, a) : s[o]) && (e[i] = !(t[i] = a))
                    }
                } else p = Te(p === t ? p.splice(l, p.length) : p), y ? y(null, t, p, r) : H.apply(t, p)
            })
        }

        function Ee(e) {
            for (var i, t, n, r = e.length, o = b.relative[e[0].type], a = o || b.relative[" "], s = o ? 1 : 0, u = be(function (e) {
                return e === i
            }, a, !0), l = be(function (e) {
                return -1 < P(i, e)
            }, a, !0), c = [function (e, t, n) {
                var r = !o && (n || t !== w) || ((i = t).nodeType ? u(e, t, n) : l(e, t, n));
                return i = null, r
            }]; s < r; s++) if (t = b.relative[e[s].type]) c = [be(we(c), t)]; else {
                if ((t = b.filter[e[s].type].apply(null, e[s].matches))[k]) {
                    for (n = ++s; n < r; n++) if (b.relative[e[n].type]) break;
                    return Ce(1 < s && we(c), 1 < s && xe(e.slice(0, s - 1).concat({value: " " === e[s - 2].type ? "*" : ""})).replace(B, "$1"), t, s < n && Ee(e.slice(s, n)), n < r && Ee(e = e.slice(n)), n < r && xe(e))
                }
                c.push(t)
            }
            return we(c)
        }

        return me.prototype = b.filters = b.pseudos, b.setFilters = new me, h = se.tokenize = function (e, t) {
            var n, r, i, o, a, s, u, l = x[e + " "];
            if (l) return t ? 0 : l.slice(0);
            a = e, s = [], u = b.preFilter;
            while (a) {
                for (o in n && !(r = _.exec(a)) || (r && (a = a.slice(r[0].length) || a), s.push(i = [])), n = !1, (r = z.exec(a)) && (n = r.shift(), i.push({
                    value: n,
                    type: r[0].replace(B, " ")
                }), a = a.slice(n.length)), b.filter) !(r = G[o].exec(a)) || u[o] && !(r = u[o](r)) || (n = r.shift(), i.push({
                    value: n,
                    type: o,
                    matches: r
                }), a = a.slice(n.length));
                if (!n) break
            }
            return t ? a.length : a ? se.error(e) : x(e, s).slice(0)
        }, f = se.compile = function (e, t) {
            var n, v, y, m, x, r, i = [], o = [], a = N[e + " "];
            if (!a) {
                t || (t = h(e)), n = t.length;
                while (n--) (a = Ee(t[n]))[k] ? i.push(a) : o.push(a);
                (a = N(e, (v = o, m = 0 < (y = i).length, x = 0 < v.length, r = function (e, t, n, r, i) {
                    var o, a, s, u = 0, l = "0", c = e && [], f = [], p = w, d = e || x && b.find.TAG("*", i),
                        h = S += null == p ? 1 : Math.random() || .1, g = d.length;
                    for (i && (w = t === C || t || i); l !== g && null != (o = d[l]); l++) {
                        if (x && o) {
                            a = 0, t || o.ownerDocument === C || (T(o), n = !E);
                            while (s = v[a++]) if (s(o, t || C, n)) {
                                r.push(o);
                                break
                            }
                            i && (S = h)
                        }
                        m && ((o = !s && o) && u--, e && c.push(o))
                    }
                    if (u += l, m && l !== u) {
                        a = 0;
                        while (s = y[a++]) s(c, f, t, n);
                        if (e) {
                            if (0 < u) while (l--) c[l] || f[l] || (f[l] = q.call(r));
                            f = Te(f)
                        }
                        H.apply(r, f), i && !e && 0 < f.length && 1 < u + y.length && se.uniqueSort(r)
                    }
                    return i && (S = h, w = p), c
                }, m ? le(r) : r))).selector = e
            }
            return a
        }, g = se.select = function (e, t, n, r) {
            var i, o, a, s, u, l = "function" == typeof e && e, c = !r && h(e = l.selector || e);
            if (n = n || [], 1 === c.length) {
                if (2 < (o = c[0] = c[0].slice(0)).length && "ID" === (a = o[0]).type && 9 === t.nodeType && E && b.relative[o[1].type]) {
                    if (!(t = (b.find.ID(a.matches[0].replace(te, ne), t) || [])[0])) return n;
                    l && (t = t.parentNode), e = e.slice(o.shift().value.length)
                }
                i = G.needsContext.test(e) ? 0 : o.length;
                while (i--) {
                    if (a = o[i], b.relative[s = a.type]) break;
                    if ((u = b.find[s]) && (r = u(a.matches[0].replace(te, ne), ee.test(o[0].type) && ye(t.parentNode) || t))) {
                        if (o.splice(i, 1), !(e = r.length && xe(o))) return H.apply(n, r), n;
                        break
                    }
                }
            }
            return (l || f(e, c))(r, t, !E, n, !t || ee.test(e) && ye(t.parentNode) || t), n
        }, d.sortStable = k.split("").sort(D).join("") === k, d.detectDuplicates = !!l, T(), d.sortDetached = ce(function (e) {
            return 1 & e.compareDocumentPosition(C.createElement("fieldset"))
        }), ce(function (e) {
            return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
        }) || fe("type|href|height|width", function (e, t, n) {
            if (!n) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
        }), d.attributes && ce(function (e) {
            return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
        }) || fe("value", function (e, t, n) {
            if (!n && "input" === e.nodeName.toLowerCase()) return e.defaultValue
        }), ce(function (e) {
            return null == e.getAttribute("disabled")
        }) || fe(R, function (e, t, n) {
            var r;
            if (!n) return !0 === e[t] ? t.toLowerCase() : (r = e.getAttributeNode(t)) && r.specified ? r.value : null
        }), se
    }(C);
    k.find = h, k.expr = h.selectors, k.expr[":"] = k.expr.pseudos, k.uniqueSort = k.unique = h.uniqueSort, k.text = h.getText, k.isXMLDoc = h.isXML, k.contains = h.contains, k.escapeSelector = h.escape;
    var T = function (e, t, n) {
        var r = [], i = void 0 !== n;
        while ((e = e[t]) && 9 !== e.nodeType) if (1 === e.nodeType) {
            if (i && k(e).is(n)) break;
            r.push(e)
        }
        return r
    }, S = function (e, t) {
        for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
        return n
    }, N = k.expr.match.needsContext;

    function A(e, t) {
        return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
    }

    var D = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;

    function j(e, n, r) {
        return m(n) ? k.grep(e, function (e, t) {
            return !!n.call(e, t, e) !== r
        }) : n.nodeType ? k.grep(e, function (e) {
            return e === n !== r
        }) : "string" != typeof n ? k.grep(e, function (e) {
            return -1 < i.call(n, e) !== r
        }) : k.filter(n, e, r)
    }

    k.filter = function (e, t, n) {
        var r = t[0];
        return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === r.nodeType ? k.find.matchesSelector(r, e) ? [r] : [] : k.find.matches(e, k.grep(t, function (e) {
            return 1 === e.nodeType
        }))
    }, k.fn.extend({
        find: function (e) {
            var t, n, r = this.length, i = this;
            if ("string" != typeof e) return this.pushStack(k(e).filter(function () {
                for (t = 0; t < r; t++) if (k.contains(i[t], this)) return !0
            }));
            for (n = this.pushStack([]), t = 0; t < r; t++) k.find(e, i[t], n);
            return 1 < r ? k.uniqueSort(n) : n
        }, filter: function (e) {
            return this.pushStack(j(this, e || [], !1))
        }, not: function (e) {
            return this.pushStack(j(this, e || [], !0))
        }, is: function (e) {
            return !!j(this, "string" == typeof e && N.test(e) ? k(e) : e || [], !1).length
        }
    });
    var q, L = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
    (k.fn.init = function (e, t, n) {
        var r, i;
        if (!e) return this;
        if (n = n || q, "string" == typeof e) {
            if (!(r = "<" === e[0] && ">" === e[e.length - 1] && 3 <= e.length ? [null, e, null] : L.exec(e)) || !r[1] && t) return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);
            if (r[1]) {
                if (t = t instanceof k ? t[0] : t, k.merge(this, k.parseHTML(r[1], t && t.nodeType ? t.ownerDocument || t : E, !0)), D.test(r[1]) && k.isPlainObject(t)) for (r in t) m(this[r]) ? this[r](t[r]) : this.attr(r, t[r]);
                return this
            }
            return (i = E.getElementById(r[2])) && (this[0] = i, this.length = 1), this
        }
        return e.nodeType ? (this[0] = e, this.length = 1, this) : m(e) ? void 0 !== n.ready ? n.ready(e) : e(k) : k.makeArray(e, this)
    }).prototype = k.fn, q = k(E);
    var H = /^(?:parents|prev(?:Until|All))/, O = {children: !0, contents: !0, next: !0, prev: !0};

    function P(e, t) {
        while ((e = e[t]) && 1 !== e.nodeType) ;
        return e
    }

    k.fn.extend({
        has: function (e) {
            var t = k(e, this), n = t.length;
            return this.filter(function () {
                for (var e = 0; e < n; e++) if (k.contains(this, t[e])) return !0
            })
        }, closest: function (e, t) {
            var n, r = 0, i = this.length, o = [], a = "string" != typeof e && k(e);
            if (!N.test(e)) for (; r < i; r++) for (n = this[r]; n && n !== t; n = n.parentNode) if (n.nodeType < 11 && (a ? -1 < a.index(n) : 1 === n.nodeType && k.find.matchesSelector(n, e))) {
                o.push(n);
                break
            }
            return this.pushStack(1 < o.length ? k.uniqueSort(o) : o)
        }, index: function (e) {
            return e ? "string" == typeof e ? i.call(k(e), this[0]) : i.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        }, add: function (e, t) {
            return this.pushStack(k.uniqueSort(k.merge(this.get(), k(e, t))))
        }, addBack: function (e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }
    }), k.each({
        parent: function (e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null
        }, parents: function (e) {
            return T(e, "parentNode")
        }, parentsUntil: function (e, t, n) {
            return T(e, "parentNode", n)
        }, next: function (e) {
            return P(e, "nextSibling")
        }, prev: function (e) {
            return P(e, "previousSibling")
        }, nextAll: function (e) {
            return T(e, "nextSibling")
        }, prevAll: function (e) {
            return T(e, "previousSibling")
        }, nextUntil: function (e, t, n) {
            return T(e, "nextSibling", n)
        }, prevUntil: function (e, t, n) {
            return T(e, "previousSibling", n)
        }, siblings: function (e) {
            return S((e.parentNode || {}).firstChild, e)
        }, children: function (e) {
            return S(e.firstChild)
        }, contents: function (e) {
            return "undefined" != typeof e.contentDocument ? e.contentDocument : (A(e, "template") && (e = e.content || e), k.merge([], e.childNodes))
        }
    }, function (r, i) {
        k.fn[r] = function (e, t) {
            var n = k.map(this, i, e);
            return "Until" !== r.slice(-5) && (t = e), t && "string" == typeof t && (n = k.filter(t, n)), 1 < this.length && (O[r] || k.uniqueSort(n), H.test(r) && n.reverse()), this.pushStack(n)
        }
    });
    var R = /[^\x20\t\r\n\f]+/g;

    function M(e) {
        return e
    }

    function I(e) {
        throw e
    }

    function W(e, t, n, r) {
        var i;
        try {
            e && m(i = e.promise) ? i.call(e).done(t).fail(n) : e && m(i = e.then) ? i.call(e, t, n) : t.apply(void 0, [e].slice(r))
        } catch (e) {
            n.apply(void 0, [e])
        }
    }

    k.Callbacks = function (r) {
        var e, n;
        r = "string" == typeof r ? (e = r, n = {}, k.each(e.match(R) || [], function (e, t) {
            n[t] = !0
        }), n) : k.extend({}, r);
        var i, t, o, a, s = [], u = [], l = -1, c = function () {
            for (a = a || r.once, o = i = !0; u.length; l = -1) {
                t = u.shift();
                while (++l < s.length) !1 === s[l].apply(t[0], t[1]) && r.stopOnFalse && (l = s.length, t = !1)
            }
            r.memory || (t = !1), i = !1, a && (s = t ? [] : "")
        }, f = {
            add: function () {
                return s && (t && !i && (l = s.length - 1, u.push(t)), function n(e) {
                    k.each(e, function (e, t) {
                        m(t) ? r.unique && f.has(t) || s.push(t) : t && t.length && "string" !== w(t) && n(t)
                    })
                }(arguments), t && !i && c()), this
            }, remove: function () {
                return k.each(arguments, function (e, t) {
                    var n;
                    while (-1 < (n = k.inArray(t, s, n))) s.splice(n, 1), n <= l && l--
                }), this
            }, has: function (e) {
                return e ? -1 < k.inArray(e, s) : 0 < s.length
            }, empty: function () {
                return s && (s = []), this
            }, disable: function () {
                return a = u = [], s = t = "", this
            }, disabled: function () {
                return !s
            }, lock: function () {
                return a = u = [], t || i || (s = t = ""), this
            }, locked: function () {
                return !!a
            }, fireWith: function (e, t) {
                return a || (t = [e, (t = t || []).slice ? t.slice() : t], u.push(t), i || c()), this
            }, fire: function () {
                return f.fireWith(this, arguments), this
            }, fired: function () {
                return !!o
            }
        };
        return f
    }, k.extend({
        Deferred: function (e) {
            var o = [["notify", "progress", k.Callbacks("memory"), k.Callbacks("memory"), 2], ["resolve", "done", k.Callbacks("once memory"), k.Callbacks("once memory"), 0, "resolved"], ["reject", "fail", k.Callbacks("once memory"), k.Callbacks("once memory"), 1, "rejected"]],
                i = "pending", a = {
                    state: function () {
                        return i
                    }, always: function () {
                        return s.done(arguments).fail(arguments), this
                    }, "catch": function (e) {
                        return a.then(null, e)
                    }, pipe: function () {
                        var i = arguments;
                        return k.Deferred(function (r) {
                            k.each(o, function (e, t) {
                                var n = m(i[t[4]]) && i[t[4]];
                                s[t[1]](function () {
                                    var e = n && n.apply(this, arguments);
                                    e && m(e.promise) ? e.promise().progress(r.notify).done(r.resolve).fail(r.reject) : r[t[0] + "With"](this, n ? [e] : arguments)
                                })
                            }), i = null
                        }).promise()
                    }, then: function (t, n, r) {
                        var u = 0;

                        function l(i, o, a, s) {
                            return function () {
                                var n = this, r = arguments, e = function () {
                                    var e, t;
                                    if (!(i < u)) {
                                        if ((e = a.apply(n, r)) === o.promise()) throw new TypeError("Thenable self-resolution");
                                        t = e && ("object" == typeof e || "function" == typeof e) && e.then, m(t) ? s ? t.call(e, l(u, o, M, s), l(u, o, I, s)) : (u++, t.call(e, l(u, o, M, s), l(u, o, I, s), l(u, o, M, o.notifyWith))) : (a !== M && (n = void 0, r = [e]), (s || o.resolveWith)(n, r))
                                    }
                                }, t = s ? e : function () {
                                    try {
                                        e()
                                    } catch (e) {
                                        k.Deferred.exceptionHook && k.Deferred.exceptionHook(e, t.stackTrace), u <= i + 1 && (a !== I && (n = void 0, r = [e]), o.rejectWith(n, r))
                                    }
                                };
                                i ? t() : (k.Deferred.getStackHook && (t.stackTrace = k.Deferred.getStackHook()), C.setTimeout(t))
                            }
                        }

                        return k.Deferred(function (e) {
                            o[0][3].add(l(0, e, m(r) ? r : M, e.notifyWith)), o[1][3].add(l(0, e, m(t) ? t : M)), o[2][3].add(l(0, e, m(n) ? n : I))
                        }).promise()
                    }, promise: function (e) {
                        return null != e ? k.extend(e, a) : a
                    }
                }, s = {};
            return k.each(o, function (e, t) {
                var n = t[2], r = t[5];
                a[t[1]] = n.add, r && n.add(function () {
                    i = r
                }, o[3 - e][2].disable, o[3 - e][3].disable, o[0][2].lock, o[0][3].lock), n.add(t[3].fire), s[t[0]] = function () {
                    return s[t[0] + "With"](this === s ? void 0 : this, arguments), this
                }, s[t[0] + "With"] = n.fireWith
            }), a.promise(s), e && e.call(s, s), s
        }, when: function (e) {
            var n = arguments.length, t = n, r = Array(t), i = s.call(arguments), o = k.Deferred(), a = function (t) {
                return function (e) {
                    r[t] = this, i[t] = 1 < arguments.length ? s.call(arguments) : e, --n || o.resolveWith(r, i)
                }
            };
            if (n <= 1 && (W(e, o.done(a(t)).resolve, o.reject, !n), "pending" === o.state() || m(i[t] && i[t].then))) return o.then();
            while (t--) W(i[t], a(t), o.reject);
            return o.promise()
        }
    });
    var $ = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    k.Deferred.exceptionHook = function (e, t) {
        C.console && C.console.warn && e && $.test(e.name) && C.console.warn("jQuery.Deferred exception: " + e.message, e.stack, t)
    }, k.readyException = function (e) {
        C.setTimeout(function () {
            throw e
        })
    };
    var F = k.Deferred();

    function B() {
        E.removeEventListener("DOMContentLoaded", B), C.removeEventListener("load", B), k.ready()
    }

    k.fn.ready = function (e) {
        return F.then(e)["catch"](function (e) {
            k.readyException(e)
        }), this
    }, k.extend({
        isReady: !1, readyWait: 1, ready: function (e) {
            (!0 === e ? --k.readyWait : k.isReady) || (k.isReady = !0) !== e && 0 < --k.readyWait || F.resolveWith(E, [k])
        }
    }), k.ready.then = F.then, "complete" === E.readyState || "loading" !== E.readyState && !E.documentElement.doScroll ? C.setTimeout(k.ready) : (E.addEventListener("DOMContentLoaded", B), C.addEventListener("load", B));
    var _ = function (e, t, n, r, i, o, a) {
        var s = 0, u = e.length, l = null == n;
        if ("object" === w(n)) for (s in i = !0, n) _(e, t, s, n[s], !0, o, a); else if (void 0 !== r && (i = !0, m(r) || (a = !0), l && (a ? (t.call(e, r), t = null) : (l = t, t = function (e, t, n) {
            return l.call(k(e), n)
        })), t)) for (; s < u; s++) t(e[s], n, a ? r : r.call(e[s], s, t(e[s], n)));
        return i ? e : l ? t.call(e) : u ? t(e[0], n) : o
    }, z = /^-ms-/, U = /-([a-z])/g;

    function X(e, t) {
        return t.toUpperCase()
    }

    function V(e) {
        return e.replace(z, "ms-").replace(U, X)
    }

    var G = function (e) {
        return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType
    };

    function Y() {
        this.expando = k.expando + Y.uid++
    }

    Y.uid = 1, Y.prototype = {
        cache: function (e) {
            var t = e[this.expando];
            return t || (t = {}, G(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, {
                value: t,
                configurable: !0
            }))), t
        }, set: function (e, t, n) {
            var r, i = this.cache(e);
            if ("string" == typeof t) i[V(t)] = n; else for (r in t) i[V(r)] = t[r];
            return i
        }, get: function (e, t) {
            return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][V(t)]
        }, access: function (e, t, n) {
            return void 0 === t || t && "string" == typeof t && void 0 === n ? this.get(e, t) : (this.set(e, t, n), void 0 !== n ? n : t)
        }, remove: function (e, t) {
            var n, r = e[this.expando];
            if (void 0 !== r) {
                if (void 0 !== t) {
                    n = (t = Array.isArray(t) ? t.map(V) : (t = V(t)) in r ? [t] : t.match(R) || []).length;
                    while (n--) delete r[t[n]]
                }
                (void 0 === t || k.isEmptyObject(r)) && (e.nodeType ? e[this.expando] = void 0 : delete e[this.expando])
            }
        }, hasData: function (e) {
            var t = e[this.expando];
            return void 0 !== t && !k.isEmptyObject(t)
        }
    };
    var Q = new Y, J = new Y, K = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, Z = /[A-Z]/g;

    function ee(e, t, n) {
        var r, i;
        if (void 0 === n && 1 === e.nodeType) if (r = "data-" + t.replace(Z, "-$&").toLowerCase(), "string" == typeof (n = e.getAttribute(r))) {
            try {
                n = "true" === (i = n) || "false" !== i && ("null" === i ? null : i === +i + "" ? +i : K.test(i) ? JSON.parse(i) : i)
            } catch (e) {
            }
            J.set(e, t, n)
        } else n = void 0;
        return n
    }

    k.extend({
        hasData: function (e) {
            return J.hasData(e) || Q.hasData(e)
        }, data: function (e, t, n) {
            return J.access(e, t, n)
        }, removeData: function (e, t) {
            J.remove(e, t)
        }, _data: function (e, t, n) {
            return Q.access(e, t, n)
        }, _removeData: function (e, t) {
            Q.remove(e, t)
        }
    }), k.fn.extend({
        data: function (n, e) {
            var t, r, i, o = this[0], a = o && o.attributes;
            if (void 0 === n) {
                if (this.length && (i = J.get(o), 1 === o.nodeType && !Q.get(o, "hasDataAttrs"))) {
                    t = a.length;
                    while (t--) a[t] && 0 === (r = a[t].name).indexOf("data-") && (r = V(r.slice(5)), ee(o, r, i[r]));
                    Q.set(o, "hasDataAttrs", !0)
                }
                return i
            }
            return "object" == typeof n ? this.each(function () {
                J.set(this, n)
            }) : _(this, function (e) {
                var t;
                if (o && void 0 === e) return void 0 !== (t = J.get(o, n)) ? t : void 0 !== (t = ee(o, n)) ? t : void 0;
                this.each(function () {
                    J.set(this, n, e)
                })
            }, null, e, 1 < arguments.length, null, !0)
        }, removeData: function (e) {
            return this.each(function () {
                J.remove(this, e)
            })
        }
    }), k.extend({
        queue: function (e, t, n) {
            var r;
            if (e) return t = (t || "fx") + "queue", r = Q.get(e, t), n && (!r || Array.isArray(n) ? r = Q.access(e, t, k.makeArray(n)) : r.push(n)), r || []
        }, dequeue: function (e, t) {
            t = t || "fx";
            var n = k.queue(e, t), r = n.length, i = n.shift(), o = k._queueHooks(e, t);
            "inprogress" === i && (i = n.shift(), r--), i && ("fx" === t && n.unshift("inprogress"), delete o.stop, i.call(e, function () {
                k.dequeue(e, t)
            }, o)), !r && o && o.empty.fire()
        }, _queueHooks: function (e, t) {
            var n = t + "queueHooks";
            return Q.get(e, n) || Q.access(e, n, {
                empty: k.Callbacks("once memory").add(function () {
                    Q.remove(e, [t + "queue", n])
                })
            })
        }
    }), k.fn.extend({
        queue: function (t, n) {
            var e = 2;
            return "string" != typeof t && (n = t, t = "fx", e--), arguments.length < e ? k.queue(this[0], t) : void 0 === n ? this : this.each(function () {
                var e = k.queue(this, t, n);
                k._queueHooks(this, t), "fx" === t && "inprogress" !== e[0] && k.dequeue(this, t)
            })
        }, dequeue: function (e) {
            return this.each(function () {
                k.dequeue(this, e)
            })
        }, clearQueue: function (e) {
            return this.queue(e || "fx", [])
        }, promise: function (e, t) {
            var n, r = 1, i = k.Deferred(), o = this, a = this.length, s = function () {
                --r || i.resolveWith(o, [o])
            };
            "string" != typeof e && (t = e, e = void 0), e = e || "fx";
            while (a--) (n = Q.get(o[a], e + "queueHooks")) && n.empty && (r++, n.empty.add(s));
            return s(), i.promise(t)
        }
    });
    var te = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, ne = new RegExp("^(?:([+-])=|)(" + te + ")([a-z%]*)$", "i"),
        re = ["Top", "Right", "Bottom", "Left"], ie = E.documentElement, oe = function (e) {
            return k.contains(e.ownerDocument, e)
        }, ae = {composed: !0};
    ie.getRootNode && (oe = function (e) {
        return k.contains(e.ownerDocument, e) || e.getRootNode(ae) === e.ownerDocument
    });
    var se = function (e, t) {
        return "none" === (e = t || e).style.display || "" === e.style.display && oe(e) && "none" === k.css(e, "display")
    }, ue = function (e, t, n, r) {
        var i, o, a = {};
        for (o in t) a[o] = e.style[o], e.style[o] = t[o];
        for (o in i = n.apply(e, r || []), t) e.style[o] = a[o];
        return i
    };

    function le(e, t, n, r) {
        var i, o, a = 20, s = r ? function () {
                return r.cur()
            } : function () {
                return k.css(e, t, "")
            }, u = s(), l = n && n[3] || (k.cssNumber[t] ? "" : "px"),
            c = e.nodeType && (k.cssNumber[t] || "px" !== l && +u) && ne.exec(k.css(e, t));
        if (c && c[3] !== l) {
            u /= 2, l = l || c[3], c = +u || 1;
            while (a--) k.style(e, t, c + l), (1 - o) * (1 - (o = s() / u || .5)) <= 0 && (a = 0), c /= o;
            c *= 2, k.style(e, t, c + l), n = n || []
        }
        return n && (c = +c || +u || 0, i = n[1] ? c + (n[1] + 1) * n[2] : +n[2], r && (r.unit = l, r.start = c, r.end = i)), i
    }

    var ce = {};

    function fe(e, t) {
        for (var n, r, i, o, a, s, u, l = [], c = 0, f = e.length; c < f; c++) (r = e[c]).style && (n = r.style.display, t ? ("none" === n && (l[c] = Q.get(r, "display") || null, l[c] || (r.style.display = "")), "" === r.style.display && se(r) && (l[c] = (u = a = o = void 0, a = (i = r).ownerDocument, s = i.nodeName, (u = ce[s]) || (o = a.body.appendChild(a.createElement(s)), u = k.css(o, "display"), o.parentNode.removeChild(o), "none" === u && (u = "block"), ce[s] = u)))) : "none" !== n && (l[c] = "none", Q.set(r, "display", n)));
        for (c = 0; c < f; c++) null != l[c] && (e[c].style.display = l[c]);
        return e
    }

    k.fn.extend({
        show: function () {
            return fe(this, !0)
        }, hide: function () {
            return fe(this)
        }, toggle: function (e) {
            return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function () {
                se(this) ? k(this).show() : k(this).hide()
            })
        }
    });
    var pe = /^(?:checkbox|radio)$/i, de = /<([a-z][^\/\0>\x20\t\r\n\f]*)/i, he = /^$|^module$|\/(?:java|ecma)script/i,
        ge = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            thead: [1, "<table>", "</table>"],
            col: [2, "<table><colgroup>", "</colgroup></table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: [0, "", ""]
        };

    function ve(e, t) {
        var n;
        return n = "undefined" != typeof e.getElementsByTagName ? e.getElementsByTagName(t || "*") : "undefined" != typeof e.querySelectorAll ? e.querySelectorAll(t || "*") : [], void 0 === t || t && A(e, t) ? k.merge([e], n) : n
    }

    function ye(e, t) {
        for (var n = 0, r = e.length; n < r; n++) Q.set(e[n], "globalEval", !t || Q.get(t[n], "globalEval"))
    }

    ge.optgroup = ge.option, ge.tbody = ge.tfoot = ge.colgroup = ge.caption = ge.thead, ge.th = ge.td;
    var me, xe, be = /<|&#?\w+;/;

    function we(e, t, n, r, i) {
        for (var o, a, s, u, l, c, f = t.createDocumentFragment(), p = [], d = 0, h = e.length; d < h; d++) if ((o = e[d]) || 0 === o) if ("object" === w(o)) k.merge(p, o.nodeType ? [o] : o); else if (be.test(o)) {
            a = a || f.appendChild(t.createElement("div")), s = (de.exec(o) || ["", ""])[1].toLowerCase(), u = ge[s] || ge._default, a.innerHTML = u[1] + k.htmlPrefilter(o) + u[2], c = u[0];
            while (c--) a = a.lastChild;
            k.merge(p, a.childNodes), (a = f.firstChild).textContent = ""
        } else p.push(t.createTextNode(o));
        f.textContent = "", d = 0;
        while (o = p[d++]) if (r && -1 < k.inArray(o, r)) i && i.push(o); else if (l = oe(o), a = ve(f.appendChild(o), "script"), l && ye(a), n) {
            c = 0;
            while (o = a[c++]) he.test(o.type || "") && n.push(o)
        }
        return f
    }

    me = E.createDocumentFragment().appendChild(E.createElement("div")), (xe = E.createElement("input")).setAttribute("type", "radio"), xe.setAttribute("checked", "checked"), xe.setAttribute("name", "t"), me.appendChild(xe), y.checkClone = me.cloneNode(!0).cloneNode(!0).lastChild.checked, me.innerHTML = "<textarea>x</textarea>", y.noCloneChecked = !!me.cloneNode(!0).lastChild.defaultValue;
    var Te = /^key/, Ce = /^(?:mouse|pointer|contextmenu|drag|drop)|click/, Ee = /^([^.]*)(?:\.(.+)|)/;

    function ke() {
        return !0
    }

    function Se() {
        return !1
    }

    function Ne(e, t) {
        return e === function () {
            try {
                return E.activeElement
            } catch (e) {
            }
        }() == ("focus" === t)
    }

    function Ae(e, t, n, r, i, o) {
        var a, s;
        if ("object" == typeof t) {
            for (s in "string" != typeof n && (r = r || n, n = void 0), t) Ae(e, s, n, r, t[s], o);
            return e
        }
        if (null == r && null == i ? (i = n, r = n = void 0) : null == i && ("string" == typeof n ? (i = r, r = void 0) : (i = r, r = n, n = void 0)), !1 === i) i = Se; else if (!i) return e;
        return 1 === o && (a = i, (i = function (e) {
            return k().off(e), a.apply(this, arguments)
        }).guid = a.guid || (a.guid = k.guid++)), e.each(function () {
            k.event.add(this, t, i, r, n)
        })
    }

    function De(e, i, o) {
        o ? (Q.set(e, i, !1), k.event.add(e, i, {
            namespace: !1, handler: function (e) {
                var t, n, r = Q.get(this, i);
                if (1 & e.isTrigger && this[i]) {
                    if (r.length) (k.event.special[i] || {}).delegateType && e.stopPropagation(); else if (r = s.call(arguments), Q.set(this, i, r), t = o(this, i), this[i](), r !== (n = Q.get(this, i)) || t ? Q.set(this, i, !1) : n = {}, r !== n) return e.stopImmediatePropagation(), e.preventDefault(), n.value
                } else r.length && (Q.set(this, i, {value: k.event.trigger(k.extend(r[0], k.Event.prototype), r.slice(1), this)}), e.stopImmediatePropagation())
            }
        })) : void 0 === Q.get(e, i) && k.event.add(e, i, ke)
    }

    k.event = {
        global: {}, add: function (t, e, n, r, i) {
            var o, a, s, u, l, c, f, p, d, h, g, v = Q.get(t);
            if (v) {
                n.handler && (n = (o = n).handler, i = o.selector), i && k.find.matchesSelector(ie, i), n.guid || (n.guid = k.guid++), (u = v.events) || (u = v.events = {}), (a = v.handle) || (a = v.handle = function (e) {
                    return "undefined" != typeof k && k.event.triggered !== e.type ? k.event.dispatch.apply(t, arguments) : void 0
                }), l = (e = (e || "").match(R) || [""]).length;
                while (l--) d = g = (s = Ee.exec(e[l]) || [])[1], h = (s[2] || "").split(".").sort(), d && (f = k.event.special[d] || {}, d = (i ? f.delegateType : f.bindType) || d, f = k.event.special[d] || {}, c = k.extend({
                    type: d,
                    origType: g,
                    data: r,
                    handler: n,
                    guid: n.guid,
                    selector: i,
                    needsContext: i && k.expr.match.needsContext.test(i),
                    namespace: h.join(".")
                }, o), (p = u[d]) || ((p = u[d] = []).delegateCount = 0, f.setup && !1 !== f.setup.call(t, r, h, a) || t.addEventListener && t.addEventListener(d, a)), f.add && (f.add.call(t, c), c.handler.guid || (c.handler.guid = n.guid)), i ? p.splice(p.delegateCount++, 0, c) : p.push(c), k.event.global[d] = !0)
            }
        }, remove: function (e, t, n, r, i) {
            var o, a, s, u, l, c, f, p, d, h, g, v = Q.hasData(e) && Q.get(e);
            if (v && (u = v.events)) {
                l = (t = (t || "").match(R) || [""]).length;
                while (l--) if (d = g = (s = Ee.exec(t[l]) || [])[1], h = (s[2] || "").split(".").sort(), d) {
                    f = k.event.special[d] || {}, p = u[d = (r ? f.delegateType : f.bindType) || d] || [], s = s[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), a = o = p.length;
                    while (o--) c = p[o], !i && g !== c.origType || n && n.guid !== c.guid || s && !s.test(c.namespace) || r && r !== c.selector && ("**" !== r || !c.selector) || (p.splice(o, 1), c.selector && p.delegateCount--, f.remove && f.remove.call(e, c));
                    a && !p.length && (f.teardown && !1 !== f.teardown.call(e, h, v.handle) || k.removeEvent(e, d, v.handle), delete u[d])
                } else for (d in u) k.event.remove(e, d + t[l], n, r, !0);
                k.isEmptyObject(u) && Q.remove(e, "handle events")
            }
        }, dispatch: function (e) {
            var t, n, r, i, o, a, s = k.event.fix(e), u = new Array(arguments.length),
                l = (Q.get(this, "events") || {})[s.type] || [], c = k.event.special[s.type] || {};
            for (u[0] = s, t = 1; t < arguments.length; t++) u[t] = arguments[t];
            if (s.delegateTarget = this, !c.preDispatch || !1 !== c.preDispatch.call(this, s)) {
                a = k.event.handlers.call(this, s, l), t = 0;
                while ((i = a[t++]) && !s.isPropagationStopped()) {
                    s.currentTarget = i.elem, n = 0;
                    while ((o = i.handlers[n++]) && !s.isImmediatePropagationStopped()) s.rnamespace && !1 !== o.namespace && !s.rnamespace.test(o.namespace) || (s.handleObj = o, s.data = o.data, void 0 !== (r = ((k.event.special[o.origType] || {}).handle || o.handler).apply(i.elem, u)) && !1 === (s.result = r) && (s.preventDefault(), s.stopPropagation()))
                }
                return c.postDispatch && c.postDispatch.call(this, s), s.result
            }
        }, handlers: function (e, t) {
            var n, r, i, o, a, s = [], u = t.delegateCount, l = e.target;
            if (u && l.nodeType && !("click" === e.type && 1 <= e.button)) for (; l !== this; l = l.parentNode || this) if (1 === l.nodeType && ("click" !== e.type || !0 !== l.disabled)) {
                for (o = [], a = {}, n = 0; n < u; n++) void 0 === a[i = (r = t[n]).selector + " "] && (a[i] = r.needsContext ? -1 < k(i, this).index(l) : k.find(i, this, null, [l]).length), a[i] && o.push(r);
                o.length && s.push({elem: l, handlers: o})
            }
            return l = this, u < t.length && s.push({elem: l, handlers: t.slice(u)}), s
        }, addProp: function (t, e) {
            Object.defineProperty(k.Event.prototype, t, {
                enumerable: !0, configurable: !0, get: m(e) ? function () {
                    if (this.originalEvent) return e(this.originalEvent)
                } : function () {
                    if (this.originalEvent) return this.originalEvent[t]
                }, set: function (e) {
                    Object.defineProperty(this, t, {enumerable: !0, configurable: !0, writable: !0, value: e})
                }
            })
        }, fix: function (e) {
            return e[k.expando] ? e : new k.Event(e)
        }, special: {
            load: {noBubble: !0}, click: {
                setup: function (e) {
                    var t = this || e;
                    return pe.test(t.type) && t.click && A(t, "input") && De(t, "click", ke), !1
                }, trigger: function (e) {
                    var t = this || e;
                    return pe.test(t.type) && t.click && A(t, "input") && De(t, "click"), !0
                }, _default: function (e) {
                    var t = e.target;
                    return pe.test(t.type) && t.click && A(t, "input") && Q.get(t, "click") || A(t, "a")
                }
            }, beforeunload: {
                postDispatch: function (e) {
                    void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
                }
            }
        }
    }, k.removeEvent = function (e, t, n) {
        e.removeEventListener && e.removeEventListener(t, n)
    }, k.Event = function (e, t) {
        if (!(this instanceof k.Event)) return new k.Event(e, t);
        e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && !1 === e.returnValue ? ke : Se, this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target, this.currentTarget = e.currentTarget, this.relatedTarget = e.relatedTarget) : this.type = e, t && k.extend(this, t), this.timeStamp = e && e.timeStamp || Date.now(), this[k.expando] = !0
    }, k.Event.prototype = {
        constructor: k.Event,
        isDefaultPrevented: Se,
        isPropagationStopped: Se,
        isImmediatePropagationStopped: Se,
        isSimulated: !1,
        preventDefault: function () {
            var e = this.originalEvent;
            this.isDefaultPrevented = ke, e && !this.isSimulated && e.preventDefault()
        },
        stopPropagation: function () {
            var e = this.originalEvent;
            this.isPropagationStopped = ke, e && !this.isSimulated && e.stopPropagation()
        },
        stopImmediatePropagation: function () {
            var e = this.originalEvent;
            this.isImmediatePropagationStopped = ke, e && !this.isSimulated && e.stopImmediatePropagation(), this.stopPropagation()
        }
    }, k.each({
        altKey: !0,
        bubbles: !0,
        cancelable: !0,
        changedTouches: !0,
        ctrlKey: !0,
        detail: !0,
        eventPhase: !0,
        metaKey: !0,
        pageX: !0,
        pageY: !0,
        shiftKey: !0,
        view: !0,
        "char": !0,
        code: !0,
        charCode: !0,
        key: !0,
        keyCode: !0,
        button: !0,
        buttons: !0,
        clientX: !0,
        clientY: !0,
        offsetX: !0,
        offsetY: !0,
        pointerId: !0,
        pointerType: !0,
        screenX: !0,
        screenY: !0,
        targetTouches: !0,
        toElement: !0,
        touches: !0,
        which: function (e) {
            var t = e.button;
            return null == e.which && Te.test(e.type) ? null != e.charCode ? e.charCode : e.keyCode : !e.which && void 0 !== t && Ce.test(e.type) ? 1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0 : e.which
        }
    }, k.event.addProp), k.each({focus: "focusin", blur: "focusout"}, function (e, t) {
        k.event.special[e] = {
            setup: function () {
                return De(this, e, Ne), !1
            }, trigger: function () {
                return De(this, e), !0
            }, delegateType: t
        }
    }), k.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function (e, i) {
        k.event.special[e] = {
            delegateType: i, bindType: i, handle: function (e) {
                var t, n = e.relatedTarget, r = e.handleObj;
                return n && (n === this || k.contains(this, n)) || (e.type = r.origType, t = r.handler.apply(this, arguments), e.type = i), t
            }
        }
    }), k.fn.extend({
        on: function (e, t, n, r) {
            return Ae(this, e, t, n, r)
        }, one: function (e, t, n, r) {
            return Ae(this, e, t, n, r, 1)
        }, off: function (e, t, n) {
            var r, i;
            if (e && e.preventDefault && e.handleObj) return r = e.handleObj, k(e.delegateTarget).off(r.namespace ? r.origType + "." + r.namespace : r.origType, r.selector, r.handler), this;
            if ("object" == typeof e) {
                for (i in e) this.off(i, t, e[i]);
                return this
            }
            return !1 !== t && "function" != typeof t || (n = t, t = void 0), !1 === n && (n = Se), this.each(function () {
                k.event.remove(this, e, n, t)
            })
        }
    });
    var je = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
        qe = /<script|<style|<link/i, Le = /checked\s*(?:[^=]|=\s*.checked.)/i,
        He = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

    function Oe(e, t) {
        return A(e, "table") && A(11 !== t.nodeType ? t : t.firstChild, "tr") && k(e).children("tbody")[0] || e
    }

    function Pe(e) {
        return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e
    }

    function Re(e) {
        return "true/" === (e.type || "").slice(0, 5) ? e.type = e.type.slice(5) : e.removeAttribute("type"), e
    }

    function Me(e, t) {
        var n, r, i, o, a, s, u, l;
        if (1 === t.nodeType) {
            if (Q.hasData(e) && (o = Q.access(e), a = Q.set(t, o), l = o.events)) for (i in delete a.handle, a.events = {}, l) for (n = 0, r = l[i].length; n < r; n++) k.event.add(t, i, l[i][n]);
            J.hasData(e) && (s = J.access(e), u = k.extend({}, s), J.set(t, u))
        }
    }

    function Ie(n, r, i, o) {
        r = g.apply([], r);
        var e, t, a, s, u, l, c = 0, f = n.length, p = f - 1, d = r[0], h = m(d);
        if (h || 1 < f && "string" == typeof d && !y.checkClone && Le.test(d)) return n.each(function (e) {
            var t = n.eq(e);
            h && (r[0] = d.call(this, e, t.html())), Ie(t, r, i, o)
        });
        if (f && (t = (e = we(r, n[0].ownerDocument, !1, n, o)).firstChild, 1 === e.childNodes.length && (e = t), t || o)) {
            for (s = (a = k.map(ve(e, "script"), Pe)).length; c < f; c++) u = e, c !== p && (u = k.clone(u, !0, !0), s && k.merge(a, ve(u, "script"))), i.call(n[c], u, c);
            if (s) for (l = a[a.length - 1].ownerDocument, k.map(a, Re), c = 0; c < s; c++) u = a[c], he.test(u.type || "") && !Q.access(u, "globalEval") && k.contains(l, u) && (u.src && "module" !== (u.type || "").toLowerCase() ? k._evalUrl && !u.noModule && k._evalUrl(u.src, {nonce: u.nonce || u.getAttribute("nonce")}) : b(u.textContent.replace(He, ""), u, l))
        }
        return n
    }

    function We(e, t, n) {
        for (var r, i = t ? k.filter(t, e) : e, o = 0; null != (r = i[o]); o++) n || 1 !== r.nodeType || k.cleanData(ve(r)), r.parentNode && (n && oe(r) && ye(ve(r, "script")), r.parentNode.removeChild(r));
        return e
    }

    k.extend({
        htmlPrefilter: function (e) {
            return e.replace(je, "<$1></$2>")
        }, clone: function (e, t, n) {
            var r, i, o, a, s, u, l, c = e.cloneNode(!0), f = oe(e);
            if (!(y.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || k.isXMLDoc(e))) for (a = ve(c), r = 0, i = (o = ve(e)).length; r < i; r++) s = o[r], u = a[r], void 0, "input" === (l = u.nodeName.toLowerCase()) && pe.test(s.type) ? u.checked = s.checked : "input" !== l && "textarea" !== l || (u.defaultValue = s.defaultValue);
            if (t) if (n) for (o = o || ve(e), a = a || ve(c), r = 0, i = o.length; r < i; r++) Me(o[r], a[r]); else Me(e, c);
            return 0 < (a = ve(c, "script")).length && ye(a, !f && ve(e, "script")), c
        }, cleanData: function (e) {
            for (var t, n, r, i = k.event.special, o = 0; void 0 !== (n = e[o]); o++) if (G(n)) {
                if (t = n[Q.expando]) {
                    if (t.events) for (r in t.events) i[r] ? k.event.remove(n, r) : k.removeEvent(n, r, t.handle);
                    n[Q.expando] = void 0
                }
                n[J.expando] && (n[J.expando] = void 0)
            }
        }
    }), k.fn.extend({
        detach: function (e) {
            return We(this, e, !0)
        }, remove: function (e) {
            return We(this, e)
        }, text: function (e) {
            return _(this, function (e) {
                return void 0 === e ? k.text(this) : this.empty().each(function () {
                    1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e)
                })
            }, null, e, arguments.length)
        }, append: function () {
            return Ie(this, arguments, function (e) {
                1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || Oe(this, e).appendChild(e)
            })
        }, prepend: function () {
            return Ie(this, arguments, function (e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = Oe(this, e);
                    t.insertBefore(e, t.firstChild)
                }
            })
        }, before: function () {
            return Ie(this, arguments, function (e) {
                this.parentNode && this.parentNode.insertBefore(e, this)
            })
        }, after: function () {
            return Ie(this, arguments, function (e) {
                this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
            })
        }, empty: function () {
            for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (k.cleanData(ve(e, !1)), e.textContent = "");
            return this
        }, clone: function (e, t) {
            return e = null != e && e, t = null == t ? e : t, this.map(function () {
                return k.clone(this, e, t)
            })
        }, html: function (e) {
            return _(this, function (e) {
                var t = this[0] || {}, n = 0, r = this.length;
                if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
                if ("string" == typeof e && !qe.test(e) && !ge[(de.exec(e) || ["", ""])[1].toLowerCase()]) {
                    e = k.htmlPrefilter(e);
                    try {
                        for (; n < r; n++) 1 === (t = this[n] || {}).nodeType && (k.cleanData(ve(t, !1)), t.innerHTML = e);
                        t = 0
                    } catch (e) {
                    }
                }
                t && this.empty().append(e)
            }, null, e, arguments.length)
        }, replaceWith: function () {
            var n = [];
            return Ie(this, arguments, function (e) {
                var t = this.parentNode;
                k.inArray(this, n) < 0 && (k.cleanData(ve(this)), t && t.replaceChild(e, this))
            }, n)
        }
    }), k.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function (e, a) {
        k.fn[e] = function (e) {
            for (var t, n = [], r = k(e), i = r.length - 1, o = 0; o <= i; o++) t = o === i ? this : this.clone(!0), k(r[o])[a](t), u.apply(n, t.get());
            return this.pushStack(n)
        }
    });
    var $e = new RegExp("^(" + te + ")(?!px)[a-z%]+$", "i"), Fe = function (e) {
        var t = e.ownerDocument.defaultView;
        return t && t.opener || (t = C), t.getComputedStyle(e)
    }, Be = new RegExp(re.join("|"), "i");

    function _e(e, t, n) {
        var r, i, o, a, s = e.style;
        return (n = n || Fe(e)) && ("" !== (a = n.getPropertyValue(t) || n[t]) || oe(e) || (a = k.style(e, t)), !y.pixelBoxStyles() && $e.test(a) && Be.test(t) && (r = s.width, i = s.minWidth, o = s.maxWidth, s.minWidth = s.maxWidth = s.width = a, a = n.width, s.width = r, s.minWidth = i, s.maxWidth = o)), void 0 !== a ? a + "" : a
    }

    function ze(e, t) {
        return {
            get: function () {
                if (!e()) return (this.get = t).apply(this, arguments);
                delete this.get
            }
        }
    }

    !function () {
        function e() {
            if (u) {
                s.style.cssText = "position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0", u.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%", ie.appendChild(s).appendChild(u);
                var e = C.getComputedStyle(u);
                n = "1%" !== e.top, a = 12 === t(e.marginLeft), u.style.right = "60%", o = 36 === t(e.right), r = 36 === t(e.width), u.style.position = "absolute", i = 12 === t(u.offsetWidth / 3), ie.removeChild(s), u = null
            }
        }

        function t(e) {
            return Math.round(parseFloat(e))
        }

        var n, r, i, o, a, s = E.createElement("div"), u = E.createElement("div");
        u.style && (u.style.backgroundClip = "content-box", u.cloneNode(!0).style.backgroundClip = "", y.clearCloneStyle = "content-box" === u.style.backgroundClip, k.extend(y, {
            boxSizingReliable: function () {
                return e(), r
            }, pixelBoxStyles: function () {
                return e(), o
            }, pixelPosition: function () {
                return e(), n
            }, reliableMarginLeft: function () {
                return e(), a
            }, scrollboxSize: function () {
                return e(), i
            }
        }))
    }();
    var Ue = ["Webkit", "Moz", "ms"], Xe = E.createElement("div").style, Ve = {};

    function Ge(e) {
        var t = k.cssProps[e] || Ve[e];
        return t || (e in Xe ? e : Ve[e] = function (e) {
            var t = e[0].toUpperCase() + e.slice(1), n = Ue.length;
            while (n--) if ((e = Ue[n] + t) in Xe) return e
        }(e) || e)
    }

    var Ye = /^(none|table(?!-c[ea]).+)/, Qe = /^--/,
        Je = {position: "absolute", visibility: "hidden", display: "block"},
        Ke = {letterSpacing: "0", fontWeight: "400"};

    function Ze(e, t, n) {
        var r = ne.exec(t);
        return r ? Math.max(0, r[2] - (n || 0)) + (r[3] || "px") : t
    }

    function et(e, t, n, r, i, o) {
        var a = "width" === t ? 1 : 0, s = 0, u = 0;
        if (n === (r ? "border" : "content")) return 0;
        for (; a < 4; a += 2) "margin" === n && (u += k.css(e, n + re[a], !0, i)), r ? ("content" === n && (u -= k.css(e, "padding" + re[a], !0, i)), "margin" !== n && (u -= k.css(e, "border" + re[a] + "Width", !0, i))) : (u += k.css(e, "padding" + re[a], !0, i), "padding" !== n ? u += k.css(e, "border" + re[a] + "Width", !0, i) : s += k.css(e, "border" + re[a] + "Width", !0, i));
        return !r && 0 <= o && (u += Math.max(0, Math.ceil(e["offset" + t[0].toUpperCase() + t.slice(1)] - o - u - s - .5)) || 0), u
    }

    function tt(e, t, n) {
        var r = Fe(e), i = (!y.boxSizingReliable() || n) && "border-box" === k.css(e, "boxSizing", !1, r), o = i,
            a = _e(e, t, r), s = "offset" + t[0].toUpperCase() + t.slice(1);
        if ($e.test(a)) {
            if (!n) return a;
            a = "auto"
        }
        return (!y.boxSizingReliable() && i || "auto" === a || !parseFloat(a) && "inline" === k.css(e, "display", !1, r)) && e.getClientRects().length && (i = "border-box" === k.css(e, "boxSizing", !1, r), (o = s in e) && (a = e[s])), (a = parseFloat(a) || 0) + et(e, t, n || (i ? "border" : "content"), o, r, a) + "px"
    }

    function nt(e, t, n, r, i) {
        return new nt.prototype.init(e, t, n, r, i)
    }

    k.extend({
        cssHooks: {
            opacity: {
                get: function (e, t) {
                    if (t) {
                        var n = _e(e, "opacity");
                        return "" === n ? "1" : n
                    }
                }
            }
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            gridArea: !0,
            gridColumn: !0,
            gridColumnEnd: !0,
            gridColumnStart: !0,
            gridRow: !0,
            gridRowEnd: !0,
            gridRowStart: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {},
        style: function (e, t, n, r) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var i, o, a, s = V(t), u = Qe.test(t), l = e.style;
                if (u || (t = Ge(s)), a = k.cssHooks[t] || k.cssHooks[s], void 0 === n) return a && "get" in a && void 0 !== (i = a.get(e, !1, r)) ? i : l[t];
                "string" === (o = typeof n) && (i = ne.exec(n)) && i[1] && (n = le(e, t, i), o = "number"), null != n && n == n && ("number" !== o || u || (n += i && i[3] || (k.cssNumber[s] ? "" : "px")), y.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (l[t] = "inherit"), a && "set" in a && void 0 === (n = a.set(e, n, r)) || (u ? l.setProperty(t, n) : l[t] = n))
            }
        },
        css: function (e, t, n, r) {
            var i, o, a, s = V(t);
            return Qe.test(t) || (t = Ge(s)), (a = k.cssHooks[t] || k.cssHooks[s]) && "get" in a && (i = a.get(e, !0, n)), void 0 === i && (i = _e(e, t, r)), "normal" === i && t in Ke && (i = Ke[t]), "" === n || n ? (o = parseFloat(i), !0 === n || isFinite(o) ? o || 0 : i) : i
        }
    }), k.each(["height", "width"], function (e, u) {
        k.cssHooks[u] = {
            get: function (e, t, n) {
                if (t) return !Ye.test(k.css(e, "display")) || e.getClientRects().length && e.getBoundingClientRect().width ? tt(e, u, n) : ue(e, Je, function () {
                    return tt(e, u, n)
                })
            }, set: function (e, t, n) {
                var r, i = Fe(e), o = !y.scrollboxSize() && "absolute" === i.position,
                    a = (o || n) && "border-box" === k.css(e, "boxSizing", !1, i), s = n ? et(e, u, n, a, i) : 0;
                return a && o && (s -= Math.ceil(e["offset" + u[0].toUpperCase() + u.slice(1)] - parseFloat(i[u]) - et(e, u, "border", !1, i) - .5)), s && (r = ne.exec(t)) && "px" !== (r[3] || "px") && (e.style[u] = t, t = k.css(e, u)), Ze(0, t, s)
            }
        }
    }), k.cssHooks.marginLeft = ze(y.reliableMarginLeft, function (e, t) {
        if (t) return (parseFloat(_e(e, "marginLeft")) || e.getBoundingClientRect().left - ue(e, {marginLeft: 0}, function () {
            return e.getBoundingClientRect().left
        })) + "px"
    }), k.each({margin: "", padding: "", border: "Width"}, function (i, o) {
        k.cssHooks[i + o] = {
            expand: function (e) {
                for (var t = 0, n = {}, r = "string" == typeof e ? e.split(" ") : [e]; t < 4; t++) n[i + re[t] + o] = r[t] || r[t - 2] || r[0];
                return n
            }
        }, "margin" !== i && (k.cssHooks[i + o].set = Ze)
    }), k.fn.extend({
        css: function (e, t) {
            return _(this, function (e, t, n) {
                var r, i, o = {}, a = 0;
                if (Array.isArray(t)) {
                    for (r = Fe(e), i = t.length; a < i; a++) o[t[a]] = k.css(e, t[a], !1, r);
                    return o
                }
                return void 0 !== n ? k.style(e, t, n) : k.css(e, t)
            }, e, t, 1 < arguments.length)
        }
    }), ((k.Tween = nt).prototype = {
        constructor: nt, init: function (e, t, n, r, i, o) {
            this.elem = e, this.prop = n, this.easing = i || k.easing._default, this.options = t, this.start = this.now = this.cur(), this.end = r, this.unit = o || (k.cssNumber[n] ? "" : "px")
        }, cur: function () {
            var e = nt.propHooks[this.prop];
            return e && e.get ? e.get(this) : nt.propHooks._default.get(this)
        }, run: function (e) {
            var t, n = nt.propHooks[this.prop];
            return this.options.duration ? this.pos = t = k.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : nt.propHooks._default.set(this), this
        }
    }).init.prototype = nt.prototype, (nt.propHooks = {
        _default: {
            get: function (e) {
                var t;
                return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = k.css(e.elem, e.prop, "")) && "auto" !== t ? t : 0
            }, set: function (e) {
                k.fx.step[e.prop] ? k.fx.step[e.prop](e) : 1 !== e.elem.nodeType || !k.cssHooks[e.prop] && null == e.elem.style[Ge(e.prop)] ? e.elem[e.prop] = e.now : k.style(e.elem, e.prop, e.now + e.unit)
            }
        }
    }).scrollTop = nt.propHooks.scrollLeft = {
        set: function (e) {
            e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
        }
    }, k.easing = {
        linear: function (e) {
            return e
        }, swing: function (e) {
            return .5 - Math.cos(e * Math.PI) / 2
        }, _default: "swing"
    }, k.fx = nt.prototype.init, k.fx.step = {};
    var rt, it, ot, at, st = /^(?:toggle|show|hide)$/, ut = /queueHooks$/;

    function lt() {
        it && (!1 === E.hidden && C.requestAnimationFrame ? C.requestAnimationFrame(lt) : C.setTimeout(lt, k.fx.interval), k.fx.tick())
    }

    function ct() {
        return C.setTimeout(function () {
            rt = void 0
        }), rt = Date.now()
    }

    function ft(e, t) {
        var n, r = 0, i = {height: e};
        for (t = t ? 1 : 0; r < 4; r += 2 - t) i["margin" + (n = re[r])] = i["padding" + n] = e;
        return t && (i.opacity = i.width = e), i
    }

    function pt(e, t, n) {
        for (var r, i = (dt.tweeners[t] || []).concat(dt.tweeners["*"]), o = 0, a = i.length; o < a; o++) if (r = i[o].call(n, t, e)) return r
    }

    function dt(o, e, t) {
        var n, a, r = 0, i = dt.prefilters.length, s = k.Deferred().always(function () {
            delete u.elem
        }), u = function () {
            if (a) return !1;
            for (var e = rt || ct(), t = Math.max(0, l.startTime + l.duration - e), n = 1 - (t / l.duration || 0), r = 0, i = l.tweens.length; r < i; r++) l.tweens[r].run(n);
            return s.notifyWith(o, [l, n, t]), n < 1 && i ? t : (i || s.notifyWith(o, [l, 1, 0]), s.resolveWith(o, [l]), !1)
        }, l = s.promise({
            elem: o,
            props: k.extend({}, e),
            opts: k.extend(!0, {specialEasing: {}, easing: k.easing._default}, t),
            originalProperties: e,
            originalOptions: t,
            startTime: rt || ct(),
            duration: t.duration,
            tweens: [],
            createTween: function (e, t) {
                var n = k.Tween(o, l.opts, e, t, l.opts.specialEasing[e] || l.opts.easing);
                return l.tweens.push(n), n
            },
            stop: function (e) {
                var t = 0, n = e ? l.tweens.length : 0;
                if (a) return this;
                for (a = !0; t < n; t++) l.tweens[t].run(1);
                return e ? (s.notifyWith(o, [l, 1, 0]), s.resolveWith(o, [l, e])) : s.rejectWith(o, [l, e]), this
            }
        }), c = l.props;
        for (!function (e, t) {
            var n, r, i, o, a;
            for (n in e) if (i = t[r = V(n)], o = e[n], Array.isArray(o) && (i = o[1], o = e[n] = o[0]), n !== r && (e[r] = o, delete e[n]), (a = k.cssHooks[r]) && "expand" in a) for (n in o = a.expand(o), delete e[r], o) n in e || (e[n] = o[n], t[n] = i); else t[r] = i
        }(c, l.opts.specialEasing); r < i; r++) if (n = dt.prefilters[r].call(l, o, c, l.opts)) return m(n.stop) && (k._queueHooks(l.elem, l.opts.queue).stop = n.stop.bind(n)), n;
        return k.map(c, pt, l), m(l.opts.start) && l.opts.start.call(o, l), l.progress(l.opts.progress).done(l.opts.done, l.opts.complete).fail(l.opts.fail).always(l.opts.always), k.fx.timer(k.extend(u, {
            elem: o,
            anim: l,
            queue: l.opts.queue
        })), l
    }

    k.Animation = k.extend(dt, {
        tweeners: {
            "*": [function (e, t) {
                var n = this.createTween(e, t);
                return le(n.elem, e, ne.exec(t), n), n
            }]
        }, tweener: function (e, t) {
            m(e) ? (t = e, e = ["*"]) : e = e.match(R);
            for (var n, r = 0, i = e.length; r < i; r++) n = e[r], dt.tweeners[n] = dt.tweeners[n] || [], dt.tweeners[n].unshift(t)
        }, prefilters: [function (e, t, n) {
            var r, i, o, a, s, u, l, c, f = "width" in t || "height" in t, p = this, d = {}, h = e.style,
                g = e.nodeType && se(e), v = Q.get(e, "fxshow");
            for (r in n.queue || (null == (a = k._queueHooks(e, "fx")).unqueued && (a.unqueued = 0, s = a.empty.fire, a.empty.fire = function () {
                a.unqueued || s()
            }), a.unqueued++, p.always(function () {
                p.always(function () {
                    a.unqueued--, k.queue(e, "fx").length || a.empty.fire()
                })
            })), t) if (i = t[r], st.test(i)) {
                if (delete t[r], o = o || "toggle" === i, i === (g ? "hide" : "show")) {
                    if ("show" !== i || !v || void 0 === v[r]) continue;
                    g = !0
                }
                d[r] = v && v[r] || k.style(e, r)
            }
            if ((u = !k.isEmptyObject(t)) || !k.isEmptyObject(d)) for (r in f && 1 === e.nodeType && (n.overflow = [h.overflow, h.overflowX, h.overflowY], null == (l = v && v.display) && (l = Q.get(e, "display")), "none" === (c = k.css(e, "display")) && (l ? c = l : (fe([e], !0), l = e.style.display || l, c = k.css(e, "display"), fe([e]))), ("inline" === c || "inline-block" === c && null != l) && "none" === k.css(e, "float") && (u || (p.done(function () {
                h.display = l
            }), null == l && (c = h.display, l = "none" === c ? "" : c)), h.display = "inline-block")), n.overflow && (h.overflow = "hidden", p.always(function () {
                h.overflow = n.overflow[0], h.overflowX = n.overflow[1], h.overflowY = n.overflow[2]
            })), u = !1, d) u || (v ? "hidden" in v && (g = v.hidden) : v = Q.access(e, "fxshow", {display: l}), o && (v.hidden = !g), g && fe([e], !0), p.done(function () {
                for (r in g || fe([e]), Q.remove(e, "fxshow"), d) k.style(e, r, d[r])
            })), u = pt(g ? v[r] : 0, r, p), r in v || (v[r] = u.start, g && (u.end = u.start, u.start = 0))
        }], prefilter: function (e, t) {
            t ? dt.prefilters.unshift(e) : dt.prefilters.push(e)
        }
    }), k.speed = function (e, t, n) {
        var r = e && "object" == typeof e ? k.extend({}, e) : {
            complete: n || !n && t || m(e) && e,
            duration: e,
            easing: n && t || t && !m(t) && t
        };
        return k.fx.off ? r.duration = 0 : "number" != typeof r.duration && (r.duration in k.fx.speeds ? r.duration = k.fx.speeds[r.duration] : r.duration = k.fx.speeds._default), null != r.queue && !0 !== r.queue || (r.queue = "fx"), r.old = r.complete, r.complete = function () {
            m(r.old) && r.old.call(this), r.queue && k.dequeue(this, r.queue)
        }, r
    }, k.fn.extend({
        fadeTo: function (e, t, n, r) {
            return this.filter(se).css("opacity", 0).show().end().animate({opacity: t}, e, n, r)
        }, animate: function (t, e, n, r) {
            var i = k.isEmptyObject(t), o = k.speed(e, n, r), a = function () {
                var e = dt(this, k.extend({}, t), o);
                (i || Q.get(this, "finish")) && e.stop(!0)
            };
            return a.finish = a, i || !1 === o.queue ? this.each(a) : this.queue(o.queue, a)
        }, stop: function (i, e, o) {
            var a = function (e) {
                var t = e.stop;
                delete e.stop, t(o)
            };
            return "string" != typeof i && (o = e, e = i, i = void 0), e && !1 !== i && this.queue(i || "fx", []), this.each(function () {
                var e = !0, t = null != i && i + "queueHooks", n = k.timers, r = Q.get(this);
                if (t) r[t] && r[t].stop && a(r[t]); else for (t in r) r[t] && r[t].stop && ut.test(t) && a(r[t]);
                for (t = n.length; t--;) n[t].elem !== this || null != i && n[t].queue !== i || (n[t].anim.stop(o), e = !1, n.splice(t, 1));
                !e && o || k.dequeue(this, i)
            })
        }, finish: function (a) {
            return !1 !== a && (a = a || "fx"), this.each(function () {
                var e, t = Q.get(this), n = t[a + "queue"], r = t[a + "queueHooks"], i = k.timers, o = n ? n.length : 0;
                for (t.finish = !0, k.queue(this, a, []), r && r.stop && r.stop.call(this, !0), e = i.length; e--;) i[e].elem === this && i[e].queue === a && (i[e].anim.stop(!0), i.splice(e, 1));
                for (e = 0; e < o; e++) n[e] && n[e].finish && n[e].finish.call(this);
                delete t.finish
            })
        }
    }), k.each(["toggle", "show", "hide"], function (e, r) {
        var i = k.fn[r];
        k.fn[r] = function (e, t, n) {
            return null == e || "boolean" == typeof e ? i.apply(this, arguments) : this.animate(ft(r, !0), e, t, n)
        }
    }), k.each({
        slideDown: ft("show"),
        slideUp: ft("hide"),
        slideToggle: ft("toggle"),
        fadeIn: {opacity: "show"},
        fadeOut: {opacity: "hide"},
        fadeToggle: {opacity: "toggle"}
    }, function (e, r) {
        k.fn[e] = function (e, t, n) {
            return this.animate(r, e, t, n)
        }
    }), k.timers = [], k.fx.tick = function () {
        var e, t = 0, n = k.timers;
        for (rt = Date.now(); t < n.length; t++) (e = n[t])() || n[t] !== e || n.splice(t--, 1);
        n.length || k.fx.stop(), rt = void 0
    }, k.fx.timer = function (e) {
        k.timers.push(e), k.fx.start()
    }, k.fx.interval = 13, k.fx.start = function () {
        it || (it = !0, lt())
    }, k.fx.stop = function () {
        it = null
    }, k.fx.speeds = {slow: 600, fast: 200, _default: 400}, k.fn.delay = function (r, e) {
        return r = k.fx && k.fx.speeds[r] || r, e = e || "fx", this.queue(e, function (e, t) {
            var n = C.setTimeout(e, r);
            t.stop = function () {
                C.clearTimeout(n)
            }
        })
    }, ot = E.createElement("input"), at = E.createElement("select").appendChild(E.createElement("option")), ot.type = "checkbox", y.checkOn = "" !== ot.value, y.optSelected = at.selected, (ot = E.createElement("input")).value = "t", ot.type = "radio", y.radioValue = "t" === ot.value;
    var ht, gt = k.expr.attrHandle;
    k.fn.extend({
        attr: function (e, t) {
            return _(this, k.attr, e, t, 1 < arguments.length)
        }, removeAttr: function (e) {
            return this.each(function () {
                k.removeAttr(this, e)
            })
        }
    }), k.extend({
        attr: function (e, t, n) {
            var r, i, o = e.nodeType;
            if (3 !== o && 8 !== o && 2 !== o) return "undefined" == typeof e.getAttribute ? k.prop(e, t, n) : (1 === o && k.isXMLDoc(e) || (i = k.attrHooks[t.toLowerCase()] || (k.expr.match.bool.test(t) ? ht : void 0)), void 0 !== n ? null === n ? void k.removeAttr(e, t) : i && "set" in i && void 0 !== (r = i.set(e, n, t)) ? r : (e.setAttribute(t, n + ""), n) : i && "get" in i && null !== (r = i.get(e, t)) ? r : null == (r = k.find.attr(e, t)) ? void 0 : r)
        }, attrHooks: {
            type: {
                set: function (e, t) {
                    if (!y.radioValue && "radio" === t && A(e, "input")) {
                        var n = e.value;
                        return e.setAttribute("type", t), n && (e.value = n), t
                    }
                }
            }
        }, removeAttr: function (e, t) {
            var n, r = 0, i = t && t.match(R);
            if (i && 1 === e.nodeType) while (n = i[r++]) e.removeAttribute(n)
        }
    }), ht = {
        set: function (e, t, n) {
            return !1 === t ? k.removeAttr(e, n) : e.setAttribute(n, n), n
        }
    }, k.each(k.expr.match.bool.source.match(/\w+/g), function (e, t) {
        var a = gt[t] || k.find.attr;
        gt[t] = function (e, t, n) {
            var r, i, o = t.toLowerCase();
            return n || (i = gt[o], gt[o] = r, r = null != a(e, t, n) ? o : null, gt[o] = i), r
        }
    });
    var vt = /^(?:input|select|textarea|button)$/i, yt = /^(?:a|area)$/i;

    function mt(e) {
        return (e.match(R) || []).join(" ")
    }

    function xt(e) {
        return e.getAttribute && e.getAttribute("class") || ""
    }

    function bt(e) {
        return Array.isArray(e) ? e : "string" == typeof e && e.match(R) || []
    }

    k.fn.extend({
        prop: function (e, t) {
            return _(this, k.prop, e, t, 1 < arguments.length)
        }, removeProp: function (e) {
            return this.each(function () {
                delete this[k.propFix[e] || e]
            })
        }
    }), k.extend({
        prop: function (e, t, n) {
            var r, i, o = e.nodeType;
            if (3 !== o && 8 !== o && 2 !== o) return 1 === o && k.isXMLDoc(e) || (t = k.propFix[t] || t, i = k.propHooks[t]), void 0 !== n ? i && "set" in i && void 0 !== (r = i.set(e, n, t)) ? r : e[t] = n : i && "get" in i && null !== (r = i.get(e, t)) ? r : e[t]
        }, propHooks: {
            tabIndex: {
                get: function (e) {
                    var t = k.find.attr(e, "tabindex");
                    return t ? parseInt(t, 10) : vt.test(e.nodeName) || yt.test(e.nodeName) && e.href ? 0 : -1
                }
            }
        }, propFix: {"for": "htmlFor", "class": "className"}
    }), y.optSelected || (k.propHooks.selected = {
        get: function (e) {
            var t = e.parentNode;
            return t && t.parentNode && t.parentNode.selectedIndex, null
        }, set: function (e) {
            var t = e.parentNode;
            t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex)
        }
    }), k.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
        k.propFix[this.toLowerCase()] = this
    }), k.fn.extend({
        addClass: function (t) {
            var e, n, r, i, o, a, s, u = 0;
            if (m(t)) return this.each(function (e) {
                k(this).addClass(t.call(this, e, xt(this)))
            });
            if ((e = bt(t)).length) while (n = this[u++]) if (i = xt(n), r = 1 === n.nodeType && " " + mt(i) + " ") {
                a = 0;
                while (o = e[a++]) r.indexOf(" " + o + " ") < 0 && (r += o + " ");
                i !== (s = mt(r)) && n.setAttribute("class", s)
            }
            return this
        }, removeClass: function (t) {
            var e, n, r, i, o, a, s, u = 0;
            if (m(t)) return this.each(function (e) {
                k(this).removeClass(t.call(this, e, xt(this)))
            });
            if (!arguments.length) return this.attr("class", "");
            if ((e = bt(t)).length) while (n = this[u++]) if (i = xt(n), r = 1 === n.nodeType && " " + mt(i) + " ") {
                a = 0;
                while (o = e[a++]) while (-1 < r.indexOf(" " + o + " ")) r = r.replace(" " + o + " ", " ");
                i !== (s = mt(r)) && n.setAttribute("class", s)
            }
            return this
        }, toggleClass: function (i, t) {
            var o = typeof i, a = "string" === o || Array.isArray(i);
            return "boolean" == typeof t && a ? t ? this.addClass(i) : this.removeClass(i) : m(i) ? this.each(function (e) {
                k(this).toggleClass(i.call(this, e, xt(this), t), t)
            }) : this.each(function () {
                var e, t, n, r;
                if (a) {
                    t = 0, n = k(this), r = bt(i);
                    while (e = r[t++]) n.hasClass(e) ? n.removeClass(e) : n.addClass(e)
                } else void 0 !== i && "boolean" !== o || ((e = xt(this)) && Q.set(this, "__className__", e), this.setAttribute && this.setAttribute("class", e || !1 === i ? "" : Q.get(this, "__className__") || ""))
            })
        }, hasClass: function (e) {
            var t, n, r = 0;
            t = " " + e + " ";
            while (n = this[r++]) if (1 === n.nodeType && -1 < (" " + mt(xt(n)) + " ").indexOf(t)) return !0;
            return !1
        }
    });
    var wt = /\r/g;
    k.fn.extend({
        val: function (n) {
            var r, e, i, t = this[0];
            return arguments.length ? (i = m(n), this.each(function (e) {
                var t;
                1 === this.nodeType && (null == (t = i ? n.call(this, e, k(this).val()) : n) ? t = "" : "number" == typeof t ? t += "" : Array.isArray(t) && (t = k.map(t, function (e) {
                    return null == e ? "" : e + ""
                })), (r = k.valHooks[this.type] || k.valHooks[this.nodeName.toLowerCase()]) && "set" in r && void 0 !== r.set(this, t, "value") || (this.value = t))
            })) : t ? (r = k.valHooks[t.type] || k.valHooks[t.nodeName.toLowerCase()]) && "get" in r && void 0 !== (e = r.get(t, "value")) ? e : "string" == typeof (e = t.value) ? e.replace(wt, "") : null == e ? "" : e : void 0
        }
    }), k.extend({
        valHooks: {
            option: {
                get: function (e) {
                    var t = k.find.attr(e, "value");
                    return null != t ? t : mt(k.text(e))
                }
            }, select: {
                get: function (e) {
                    var t, n, r, i = e.options, o = e.selectedIndex, a = "select-one" === e.type, s = a ? null : [],
                        u = a ? o + 1 : i.length;
                    for (r = o < 0 ? u : a ? o : 0; r < u; r++) if (((n = i[r]).selected || r === o) && !n.disabled && (!n.parentNode.disabled || !A(n.parentNode, "optgroup"))) {
                        if (t = k(n).val(), a) return t;
                        s.push(t)
                    }
                    return s
                }, set: function (e, t) {
                    var n, r, i = e.options, o = k.makeArray(t), a = i.length;
                    while (a--) ((r = i[a]).selected = -1 < k.inArray(k.valHooks.option.get(r), o)) && (n = !0);
                    return n || (e.selectedIndex = -1), o
                }
            }
        }
    }), k.each(["radio", "checkbox"], function () {
        k.valHooks[this] = {
            set: function (e, t) {
                if (Array.isArray(t)) return e.checked = -1 < k.inArray(k(e).val(), t)
            }
        }, y.checkOn || (k.valHooks[this].get = function (e) {
            return null === e.getAttribute("value") ? "on" : e.value
        })
    }), y.focusin = "onfocusin" in C;
    var Tt = /^(?:focusinfocus|focusoutblur)$/, Ct = function (e) {
        e.stopPropagation()
    };
    k.extend(k.event, {
        trigger: function (e, t, n, r) {
            var i, o, a, s, u, l, c, f, p = [n || E], d = v.call(e, "type") ? e.type : e,
                h = v.call(e, "namespace") ? e.namespace.split(".") : [];
            if (o = f = a = n = n || E, 3 !== n.nodeType && 8 !== n.nodeType && !Tt.test(d + k.event.triggered) && (-1 < d.indexOf(".") && (d = (h = d.split(".")).shift(), h.sort()), u = d.indexOf(":") < 0 && "on" + d, (e = e[k.expando] ? e : new k.Event(d, "object" == typeof e && e)).isTrigger = r ? 2 : 3, e.namespace = h.join("."), e.rnamespace = e.namespace ? new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = void 0, e.target || (e.target = n), t = null == t ? [e] : k.makeArray(t, [e]), c = k.event.special[d] || {}, r || !c.trigger || !1 !== c.trigger.apply(n, t))) {
                if (!r && !c.noBubble && !x(n)) {
                    for (s = c.delegateType || d, Tt.test(s + d) || (o = o.parentNode); o; o = o.parentNode) p.push(o), a = o;
                    a === (n.ownerDocument || E) && p.push(a.defaultView || a.parentWindow || C)
                }
                i = 0;
                while ((o = p[i++]) && !e.isPropagationStopped()) f = o, e.type = 1 < i ? s : c.bindType || d, (l = (Q.get(o, "events") || {})[e.type] && Q.get(o, "handle")) && l.apply(o, t), (l = u && o[u]) && l.apply && G(o) && (e.result = l.apply(o, t), !1 === e.result && e.preventDefault());
                return e.type = d, r || e.isDefaultPrevented() || c._default && !1 !== c._default.apply(p.pop(), t) || !G(n) || u && m(n[d]) && !x(n) && ((a = n[u]) && (n[u] = null), k.event.triggered = d, e.isPropagationStopped() && f.addEventListener(d, Ct), n[d](), e.isPropagationStopped() && f.removeEventListener(d, Ct), k.event.triggered = void 0, a && (n[u] = a)), e.result
            }
        }, simulate: function (e, t, n) {
            var r = k.extend(new k.Event, n, {type: e, isSimulated: !0});
            k.event.trigger(r, null, t)
        }
    }), k.fn.extend({
        trigger: function (e, t) {
            return this.each(function () {
                k.event.trigger(e, t, this)
            })
        }, triggerHandler: function (e, t) {
            var n = this[0];
            if (n) return k.event.trigger(e, t, n, !0)
        }
    }), y.focusin || k.each({focus: "focusin", blur: "focusout"}, function (n, r) {
        var i = function (e) {
            k.event.simulate(r, e.target, k.event.fix(e))
        };
        k.event.special[r] = {
            setup: function () {
                var e = this.ownerDocument || this, t = Q.access(e, r);
                t || e.addEventListener(n, i, !0), Q.access(e, r, (t || 0) + 1)
            }, teardown: function () {
                var e = this.ownerDocument || this, t = Q.access(e, r) - 1;
                t ? Q.access(e, r, t) : (e.removeEventListener(n, i, !0), Q.remove(e, r))
            }
        }
    });
    var Et = C.location, kt = Date.now(), St = /\?/;
    k.parseXML = function (e) {
        var t;
        if (!e || "string" != typeof e) return null;
        try {
            t = (new C.DOMParser).parseFromString(e, "text/xml")
        } catch (e) {
            t = void 0
        }
        return t && !t.getElementsByTagName("parsererror").length || k.error("Invalid XML: " + e), t
    };
    var Nt = /\[\]$/, At = /\r?\n/g, Dt = /^(?:submit|button|image|reset|file)$/i,
        jt = /^(?:input|select|textarea|keygen)/i;

    function qt(n, e, r, i) {
        var t;
        if (Array.isArray(e)) k.each(e, function (e, t) {
            r || Nt.test(n) ? i(n, t) : qt(n + "[" + ("object" == typeof t && null != t ? e : "") + "]", t, r, i)
        }); else if (r || "object" !== w(e)) i(n, e); else for (t in e) qt(n + "[" + t + "]", e[t], r, i)
    }

    k.param = function (e, t) {
        var n, r = [], i = function (e, t) {
            var n = m(t) ? t() : t;
            r[r.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == n ? "" : n)
        };
        if (null == e) return "";
        if (Array.isArray(e) || e.jquery && !k.isPlainObject(e)) k.each(e, function () {
            i(this.name, this.value)
        }); else for (n in e) qt(n, e[n], t, i);
        return r.join("&")
    }, k.fn.extend({
        serialize: function () {
            return k.param(this.serializeArray())
        }, serializeArray: function () {
            return this.map(function () {
                var e = k.prop(this, "elements");
                return e ? k.makeArray(e) : this
            }).filter(function () {
                var e = this.type;
                return this.name && !k(this).is(":disabled") && jt.test(this.nodeName) && !Dt.test(e) && (this.checked || !pe.test(e))
            }).map(function (e, t) {
                var n = k(this).val();
                return null == n ? null : Array.isArray(n) ? k.map(n, function (e) {
                    return {name: t.name, value: e.replace(At, "\r\n")}
                }) : {name: t.name, value: n.replace(At, "\r\n")}
            }).get()
        }
    });
    var Lt = /%20/g, Ht = /#.*$/, Ot = /([?&])_=[^&]*/, Pt = /^(.*?):[ \t]*([^\r\n]*)$/gm, Rt = /^(?:GET|HEAD)$/,
        Mt = /^\/\//, It = {}, Wt = {}, $t = "*/".concat("*"), Ft = E.createElement("a");

    function Bt(o) {
        return function (e, t) {
            "string" != typeof e && (t = e, e = "*");
            var n, r = 0, i = e.toLowerCase().match(R) || [];
            if (m(t)) while (n = i[r++]) "+" === n[0] ? (n = n.slice(1) || "*", (o[n] = o[n] || []).unshift(t)) : (o[n] = o[n] || []).push(t)
        }
    }

    function _t(t, i, o, a) {
        var s = {}, u = t === Wt;

        function l(e) {
            var r;
            return s[e] = !0, k.each(t[e] || [], function (e, t) {
                var n = t(i, o, a);
                return "string" != typeof n || u || s[n] ? u ? !(r = n) : void 0 : (i.dataTypes.unshift(n), l(n), !1)
            }), r
        }

        return l(i.dataTypes[0]) || !s["*"] && l("*")
    }

    function zt(e, t) {
        var n, r, i = k.ajaxSettings.flatOptions || {};
        for (n in t) void 0 !== t[n] && ((i[n] ? e : r || (r = {}))[n] = t[n]);
        return r && k.extend(!0, e, r), e
    }

    Ft.href = Et.href, k.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: Et.href,
            type: "GET",
            isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(Et.protocol),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": $t,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {xml: /\bxml\b/, html: /\bhtml/, json: /\bjson\b/},
            responseFields: {xml: "responseXML", text: "responseText", json: "responseJSON"},
            converters: {"* text": String, "text html": !0, "text json": JSON.parse, "text xml": k.parseXML},
            flatOptions: {url: !0, context: !0}
        },
        ajaxSetup: function (e, t) {
            return t ? zt(zt(e, k.ajaxSettings), t) : zt(k.ajaxSettings, e)
        },
        ajaxPrefilter: Bt(It),
        ajaxTransport: Bt(Wt),
        ajax: function (e, t) {
            "object" == typeof e && (t = e, e = void 0), t = t || {};
            var c, f, p, n, d, r, h, g, i, o, v = k.ajaxSetup({}, t), y = v.context || v,
                m = v.context && (y.nodeType || y.jquery) ? k(y) : k.event, x = k.Deferred(),
                b = k.Callbacks("once memory"), w = v.statusCode || {}, a = {}, s = {}, u = "canceled", T = {
                    readyState: 0, getResponseHeader: function (e) {
                        var t;
                        if (h) {
                            if (!n) {
                                n = {};
                                while (t = Pt.exec(p)) n[t[1].toLowerCase() + " "] = (n[t[1].toLowerCase() + " "] || []).concat(t[2])
                            }
                            t = n[e.toLowerCase() + " "]
                        }
                        return null == t ? null : t.join(", ")
                    }, getAllResponseHeaders: function () {
                        return h ? p : null
                    }, setRequestHeader: function (e, t) {
                        return null == h && (e = s[e.toLowerCase()] = s[e.toLowerCase()] || e, a[e] = t), this
                    }, overrideMimeType: function (e) {
                        return null == h && (v.mimeType = e), this
                    }, statusCode: function (e) {
                        var t;
                        if (e) if (h) T.always(e[T.status]); else for (t in e) w[t] = [w[t], e[t]];
                        return this
                    }, abort: function (e) {
                        var t = e || u;
                        return c && c.abort(t), l(0, t), this
                    }
                };
            if (x.promise(T), v.url = ((e || v.url || Et.href) + "").replace(Mt, Et.protocol + "//"), v.type = t.method || t.type || v.method || v.type, v.dataTypes = (v.dataType || "*").toLowerCase().match(R) || [""], null == v.crossDomain) {
                r = E.createElement("a");
                try {
                    r.href = v.url, r.href = r.href, v.crossDomain = Ft.protocol + "//" + Ft.host != r.protocol + "//" + r.host
                } catch (e) {
                    v.crossDomain = !0
                }
            }
            if (v.data && v.processData && "string" != typeof v.data && (v.data = k.param(v.data, v.traditional)), _t(It, v, t, T), h) return T;
            for (i in (g = k.event && v.global) && 0 == k.active++ && k.event.trigger("ajaxStart"), v.type = v.type.toUpperCase(), v.hasContent = !Rt.test(v.type), f = v.url.replace(Ht, ""), v.hasContent ? v.data && v.processData && 0 === (v.contentType || "").indexOf("application/x-www-form-urlencoded") && (v.data = v.data.replace(Lt, "+")) : (o = v.url.slice(f.length), v.data && (v.processData || "string" == typeof v.data) && (f += (St.test(f) ? "&" : "?") + v.data, delete v.data), !1 === v.cache && (f = f.replace(Ot, "$1"), o = (St.test(f) ? "&" : "?") + "_=" + kt++ + o), v.url = f + o), v.ifModified && (k.lastModified[f] && T.setRequestHeader("If-Modified-Since", k.lastModified[f]), k.etag[f] && T.setRequestHeader("If-None-Match", k.etag[f])), (v.data && v.hasContent && !1 !== v.contentType || t.contentType) && T.setRequestHeader("Content-Type", v.contentType), T.setRequestHeader("Accept", v.dataTypes[0] && v.accepts[v.dataTypes[0]] ? v.accepts[v.dataTypes[0]] + ("*" !== v.dataTypes[0] ? ", " + $t + "; q=0.01" : "") : v.accepts["*"]), v.headers) T.setRequestHeader(i, v.headers[i]);
            if (v.beforeSend && (!1 === v.beforeSend.call(y, T, v) || h)) return T.abort();
            if (u = "abort", b.add(v.complete), T.done(v.success), T.fail(v.error), c = _t(Wt, v, t, T)) {
                if (T.readyState = 1, g && m.trigger("ajaxSend", [T, v]), h) return T;
                v.async && 0 < v.timeout && (d = C.setTimeout(function () {
                    T.abort("timeout")
                }, v.timeout));
                try {
                    h = !1, c.send(a, l)
                } catch (e) {
                    if (h) throw e;
                    l(-1, e)
                }
            } else l(-1, "No Transport");

            function l(e, t, n, r) {
                var i, o, a, s, u, l = t;
                h || (h = !0, d && C.clearTimeout(d), c = void 0, p = r || "", T.readyState = 0 < e ? 4 : 0, i = 200 <= e && e < 300 || 304 === e, n && (s = function (e, t, n) {
                    var r, i, o, a, s = e.contents, u = e.dataTypes;
                    while ("*" === u[0]) u.shift(), void 0 === r && (r = e.mimeType || t.getResponseHeader("Content-Type"));
                    if (r) for (i in s) if (s[i] && s[i].test(r)) {
                        u.unshift(i);
                        break
                    }
                    if (u[0] in n) o = u[0]; else {
                        for (i in n) {
                            if (!u[0] || e.converters[i + " " + u[0]]) {
                                o = i;
                                break
                            }
                            a || (a = i)
                        }
                        o = o || a
                    }
                    if (o) return o !== u[0] && u.unshift(o), n[o]
                }(v, T, n)), s = function (e, t, n, r) {
                    var i, o, a, s, u, l = {}, c = e.dataTypes.slice();
                    if (c[1]) for (a in e.converters) l[a.toLowerCase()] = e.converters[a];
                    o = c.shift();
                    while (o) if (e.responseFields[o] && (n[e.responseFields[o]] = t), !u && r && e.dataFilter && (t = e.dataFilter(t, e.dataType)), u = o, o = c.shift()) if ("*" === o) o = u; else if ("*" !== u && u !== o) {
                        if (!(a = l[u + " " + o] || l["* " + o])) for (i in l) if ((s = i.split(" "))[1] === o && (a = l[u + " " + s[0]] || l["* " + s[0]])) {
                            !0 === a ? a = l[i] : !0 !== l[i] && (o = s[0], c.unshift(s[1]));
                            break
                        }
                        if (!0 !== a) if (a && e["throws"]) t = a(t); else try {
                            t = a(t)
                        } catch (e) {
                            return {state: "parsererror", error: a ? e : "No conversion from " + u + " to " + o}
                        }
                    }
                    return {state: "success", data: t}
                }(v, s, T, i), i ? (v.ifModified && ((u = T.getResponseHeader("Last-Modified")) && (k.lastModified[f] = u), (u = T.getResponseHeader("etag")) && (k.etag[f] = u)), 204 === e || "HEAD" === v.type ? l = "nocontent" : 304 === e ? l = "notmodified" : (l = s.state, o = s.data, i = !(a = s.error))) : (a = l, !e && l || (l = "error", e < 0 && (e = 0))), T.status = e, T.statusText = (t || l) + "", i ? x.resolveWith(y, [o, l, T]) : x.rejectWith(y, [T, l, a]), T.statusCode(w), w = void 0, g && m.trigger(i ? "ajaxSuccess" : "ajaxError", [T, v, i ? o : a]), b.fireWith(y, [T, l]), g && (m.trigger("ajaxComplete", [T, v]), --k.active || k.event.trigger("ajaxStop")))
            }

            return T
        },
        getJSON: function (e, t, n) {
            return k.get(e, t, n, "json")
        },
        getScript: function (e, t) {
            return k.get(e, void 0, t, "script")
        }
    }), k.each(["get", "post"], function (e, i) {
        k[i] = function (e, t, n, r) {
            return m(t) && (r = r || n, n = t, t = void 0), k.ajax(k.extend({
                url: e,
                type: i,
                dataType: r,
                data: t,
                success: n
            }, k.isPlainObject(e) && e))
        }
    }), k._evalUrl = function (e, t) {
        return k.ajax({
            url: e,
            type: "GET",
            dataType: "script",
            cache: !0,
            async: !1,
            global: !1,
            converters: {
                "text script": function () {
                }
            },
            dataFilter: function (e) {
                k.globalEval(e, t)
            }
        })
    }, k.fn.extend({
        wrapAll: function (e) {
            var t;
            return this[0] && (m(e) && (e = e.call(this[0])), t = k(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function () {
                var e = this;
                while (e.firstElementChild) e = e.firstElementChild;
                return e
            }).append(this)), this
        }, wrapInner: function (n) {
            return m(n) ? this.each(function (e) {
                k(this).wrapInner(n.call(this, e))
            }) : this.each(function () {
                var e = k(this), t = e.contents();
                t.length ? t.wrapAll(n) : e.append(n)
            })
        }, wrap: function (t) {
            var n = m(t);
            return this.each(function (e) {
                k(this).wrapAll(n ? t.call(this, e) : t)
            })
        }, unwrap: function (e) {
            return this.parent(e).not("body").each(function () {
                k(this).replaceWith(this.childNodes)
            }), this
        }
    }), k.expr.pseudos.hidden = function (e) {
        return !k.expr.pseudos.visible(e)
    }, k.expr.pseudos.visible = function (e) {
        return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length)
    }, k.ajaxSettings.xhr = function () {
        try {
            return new C.XMLHttpRequest
        } catch (e) {
        }
    };
    var Ut = {0: 200, 1223: 204}, Xt = k.ajaxSettings.xhr();
    y.cors = !!Xt && "withCredentials" in Xt, y.ajax = Xt = !!Xt, k.ajaxTransport(function (i) {
        var o, a;
        if (y.cors || Xt && !i.crossDomain) return {
            send: function (e, t) {
                var n, r = i.xhr();
                if (r.open(i.type, i.url, i.async, i.username, i.password), i.xhrFields) for (n in i.xhrFields) r[n] = i.xhrFields[n];
                for (n in i.mimeType && r.overrideMimeType && r.overrideMimeType(i.mimeType), i.crossDomain || e["X-Requested-With"] || (e["X-Requested-With"] = "XMLHttpRequest"), e) r.setRequestHeader(n, e[n]);
                o = function (e) {
                    return function () {
                        o && (o = a = r.onload = r.onerror = r.onabort = r.ontimeout = r.onreadystatechange = null, "abort" === e ? r.abort() : "error" === e ? "number" != typeof r.status ? t(0, "error") : t(r.status, r.statusText) : t(Ut[r.status] || r.status, r.statusText, "text" !== (r.responseType || "text") || "string" != typeof r.responseText ? {binary: r.response} : {text: r.responseText}, r.getAllResponseHeaders()))
                    }
                }, r.onload = o(), a = r.onerror = r.ontimeout = o("error"), void 0 !== r.onabort ? r.onabort = a : r.onreadystatechange = function () {
                    4 === r.readyState && C.setTimeout(function () {
                        o && a()
                    })
                }, o = o("abort");
                try {
                    r.send(i.hasContent && i.data || null)
                } catch (e) {
                    if (o) throw e
                }
            }, abort: function () {
                o && o()
            }
        }
    }), k.ajaxPrefilter(function (e) {
        e.crossDomain && (e.contents.script = !1)
    }), k.ajaxSetup({
        accepts: {script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},
        contents: {script: /\b(?:java|ecma)script\b/},
        converters: {
            "text script": function (e) {
                return k.globalEval(e), e
            }
        }
    }), k.ajaxPrefilter("script", function (e) {
        void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET")
    }), k.ajaxTransport("script", function (n) {
        var r, i;
        if (n.crossDomain || n.scriptAttrs) return {
            send: function (e, t) {
                r = k("<script>").attr(n.scriptAttrs || {}).prop({
                    charset: n.scriptCharset,
                    src: n.url
                }).on("load error", i = function (e) {
                    r.remove(), i = null, e && t("error" === e.type ? 404 : 200, e.type)
                }), E.head.appendChild(r[0])
            }, abort: function () {
                i && i()
            }
        }
    });
    var Vt, Gt = [], Yt = /(=)\?(?=&|$)|\?\?/;
    k.ajaxSetup({
        jsonp: "callback", jsonpCallback: function () {
            var e = Gt.pop() || k.expando + "_" + kt++;
            return this[e] = !0, e
        }
    }), k.ajaxPrefilter("json jsonp", function (e, t, n) {
        var r, i, o,
            a = !1 !== e.jsonp && (Yt.test(e.url) ? "url" : "string" == typeof e.data && 0 === (e.contentType || "").indexOf("application/x-www-form-urlencoded") && Yt.test(e.data) && "data");
        if (a || "jsonp" === e.dataTypes[0]) return r = e.jsonpCallback = m(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, a ? e[a] = e[a].replace(Yt, "$1" + r) : !1 !== e.jsonp && (e.url += (St.test(e.url) ? "&" : "?") + e.jsonp + "=" + r), e.converters["script json"] = function () {
            return o || k.error(r + " was not called"), o[0]
        }, e.dataTypes[0] = "json", i = C[r], C[r] = function () {
            o = arguments
        }, n.always(function () {
            void 0 === i ? k(C).removeProp(r) : C[r] = i, e[r] && (e.jsonpCallback = t.jsonpCallback, Gt.push(r)), o && m(i) && i(o[0]), o = i = void 0
        }), "script"
    }), y.createHTMLDocument = ((Vt = E.implementation.createHTMLDocument("").body).innerHTML = "<form></form><form></form>", 2 === Vt.childNodes.length), k.parseHTML = function (e, t, n) {
        return "string" != typeof e ? [] : ("boolean" == typeof t && (n = t, t = !1), t || (y.createHTMLDocument ? ((r = (t = E.implementation.createHTMLDocument("")).createElement("base")).href = E.location.href, t.head.appendChild(r)) : t = E), o = !n && [], (i = D.exec(e)) ? [t.createElement(i[1])] : (i = we([e], t, o), o && o.length && k(o).remove(), k.merge([], i.childNodes)));
        var r, i, o
    }, k.fn.load = function (e, t, n) {
        var r, i, o, a = this, s = e.indexOf(" ");
        return -1 < s && (r = mt(e.slice(s)), e = e.slice(0, s)), m(t) ? (n = t, t = void 0) : t && "object" == typeof t && (i = "POST"), 0 < a.length && k.ajax({
            url: e,
            type: i || "GET",
            dataType: "html",
            data: t
        }).done(function (e) {
            o = arguments, a.html(r ? k("<div>").append(k.parseHTML(e)).find(r) : e)
        }).always(n && function (e, t) {
            a.each(function () {
                n.apply(this, o || [e.responseText, t, e])
            })
        }), this
    }, k.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (e, t) {
        k.fn[t] = function (e) {
            return this.on(t, e)
        }
    }), k.expr.pseudos.animated = function (t) {
        return k.grep(k.timers, function (e) {
            return t === e.elem
        }).length
    }, k.offset = {
        setOffset: function (e, t, n) {
            var r, i, o, a, s, u, l = k.css(e, "position"), c = k(e), f = {};
            "static" === l && (e.style.position = "relative"), s = c.offset(), o = k.css(e, "top"), u = k.css(e, "left"), ("absolute" === l || "fixed" === l) && -1 < (o + u).indexOf("auto") ? (a = (r = c.position()).top, i = r.left) : (a = parseFloat(o) || 0, i = parseFloat(u) || 0), m(t) && (t = t.call(e, n, k.extend({}, s))), null != t.top && (f.top = t.top - s.top + a), null != t.left && (f.left = t.left - s.left + i), "using" in t ? t.using.call(e, f) : c.css(f)
        }
    }, k.fn.extend({
        offset: function (t) {
            if (arguments.length) return void 0 === t ? this : this.each(function (e) {
                k.offset.setOffset(this, t, e)
            });
            var e, n, r = this[0];
            return r ? r.getClientRects().length ? (e = r.getBoundingClientRect(), n = r.ownerDocument.defaultView, {
                top: e.top + n.pageYOffset,
                left: e.left + n.pageXOffset
            }) : {top: 0, left: 0} : void 0
        }, position: function () {
            if (this[0]) {
                var e, t, n, r = this[0], i = {top: 0, left: 0};
                if ("fixed" === k.css(r, "position")) t = r.getBoundingClientRect(); else {
                    t = this.offset(), n = r.ownerDocument, e = r.offsetParent || n.documentElement;
                    while (e && (e === n.body || e === n.documentElement) && "static" === k.css(e, "position")) e = e.parentNode;
                    e && e !== r && 1 === e.nodeType && ((i = k(e).offset()).top += k.css(e, "borderTopWidth", !0), i.left += k.css(e, "borderLeftWidth", !0))
                }
                return {
                    top: t.top - i.top - k.css(r, "marginTop", !0),
                    left: t.left - i.left - k.css(r, "marginLeft", !0)
                }
            }
        }, offsetParent: function () {
            return this.map(function () {
                var e = this.offsetParent;
                while (e && "static" === k.css(e, "position")) e = e.offsetParent;
                return e || ie
            })
        }
    }), k.each({scrollLeft: "pageXOffset", scrollTop: "pageYOffset"}, function (t, i) {
        var o = "pageYOffset" === i;
        k.fn[t] = function (e) {
            return _(this, function (e, t, n) {
                var r;
                if (x(e) ? r = e : 9 === e.nodeType && (r = e.defaultView), void 0 === n) return r ? r[i] : e[t];
                r ? r.scrollTo(o ? r.pageXOffset : n, o ? n : r.pageYOffset) : e[t] = n
            }, t, e, arguments.length)
        }
    }), k.each(["top", "left"], function (e, n) {
        k.cssHooks[n] = ze(y.pixelPosition, function (e, t) {
            if (t) return t = _e(e, n), $e.test(t) ? k(e).position()[n] + "px" : t
        })
    }), k.each({Height: "height", Width: "width"}, function (a, s) {
        k.each({padding: "inner" + a, content: s, "": "outer" + a}, function (r, o) {
            k.fn[o] = function (e, t) {
                var n = arguments.length && (r || "boolean" != typeof e),
                    i = r || (!0 === e || !0 === t ? "margin" : "border");
                return _(this, function (e, t, n) {
                    var r;
                    return x(e) ? 0 === o.indexOf("outer") ? e["inner" + a] : e.document.documentElement["client" + a] : 9 === e.nodeType ? (r = e.documentElement, Math.max(e.body["scroll" + a], r["scroll" + a], e.body["offset" + a], r["offset" + a], r["client" + a])) : void 0 === n ? k.css(e, t, i) : k.style(e, t, n, i)
                }, s, n ? e : void 0, n)
            }
        })
    }), k.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function (e, n) {
        k.fn[n] = function (e, t) {
            return 0 < arguments.length ? this.on(n, null, e, t) : this.trigger(n)
        }
    }), k.fn.extend({
        hover: function (e, t) {
            return this.mouseenter(e).mouseleave(t || e)
        }
    }), k.fn.extend({
        bind: function (e, t, n) {
            return this.on(e, null, t, n)
        }, unbind: function (e, t) {
            return this.off(e, null, t)
        }, delegate: function (e, t, n, r) {
            return this.on(t, e, n, r)
        }, undelegate: function (e, t, n) {
            return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
        }
    }), k.proxy = function (e, t) {
        var n, r, i;
        if ("string" == typeof t && (n = e[t], t = e, e = n), m(e)) return r = s.call(arguments, 2), (i = function () {
            return e.apply(t || this, r.concat(s.call(arguments)))
        }).guid = e.guid = e.guid || k.guid++, i
    }, k.holdReady = function (e) {
        e ? k.readyWait++ : k.ready(!0)
    }, k.isArray = Array.isArray, k.parseJSON = JSON.parse, k.nodeName = A, k.isFunction = m, k.isWindow = x, k.camelCase = V, k.type = w, k.now = Date.now, k.isNumeric = function (e) {
        var t = k.type(e);
        return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e))
    }, "function" == typeof define && define.amd && define("jquery", [], function () {
        return k
    });
    var Qt = C.jQuery, Jt = C.$;
    return k.noConflict = function (e) {
        return C.$ === k && (C.$ = Jt), e && C.jQuery === k && (C.jQuery = Qt), k
    }, e || (C.jQuery = C.$ = k), k
});/*!
  * Bootstrap v4.4.1 (https://getbootstrap.com/)
  * Copyright 2011-2019 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
  */
!function (e, t) {
    "object" == typeof exports && "undefined" != typeof module ? t(exports, require("jquery")) : "function" == typeof define && define.amd ? define(["exports", "jquery"], t) : t((e = e || self).bootstrap = {}, e.jQuery)
}(this, function (e, p) {
    "use strict";

    function i(e, t) {
        for (var n = 0; n < t.length; n++) {
            var i = t[n];
            i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
        }
    }

    function s(e, t, n) {
        return t && i(e.prototype, t), n && i(e, n), e
    }

    function t(t, e) {
        var n = Object.keys(t);
        if (Object.getOwnPropertySymbols) {
            var i = Object.getOwnPropertySymbols(t);
            e && (i = i.filter(function (e) {
                return Object.getOwnPropertyDescriptor(t, e).enumerable
            })), n.push.apply(n, i)
        }
        return n
    }

    function l(o) {
        for (var e = 1; e < arguments.length; e++) {
            var r = null != arguments[e] ? arguments[e] : {};
            e % 2 ? t(Object(r), !0).forEach(function (e) {
                var t, n, i;
                t = o, i = r[n = e], n in t ? Object.defineProperty(t, n, {
                    value: i,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : t[n] = i
            }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(o, Object.getOwnPropertyDescriptors(r)) : t(Object(r)).forEach(function (e) {
                Object.defineProperty(o, e, Object.getOwnPropertyDescriptor(r, e))
            })
        }
        return o
    }

    p = p && p.hasOwnProperty("default") ? p.default : p;
    var n = "transitionend";

    function o(e) {
        var t = this, n = !1;
        return p(this).one(m.TRANSITION_END, function () {
            n = !0
        }), setTimeout(function () {
            n || m.triggerTransitionEnd(t)
        }, e), this
    }

    var m = {
        TRANSITION_END: "bsTransitionEnd", getUID: function (e) {
            for (; e += ~~(1e6 * Math.random()), document.getElementById(e);) ;
            return e
        }, getSelectorFromElement: function (e) {
            var t = e.getAttribute("data-target");
            if (!t || "#" === t) {
                var n = e.getAttribute("href");
                t = n && "#" !== n ? n.trim() : ""
            }
            try {
                return document.querySelector(t) ? t : null
            } catch (e) {
                return null
            }
        }, getTransitionDurationFromElement: function (e) {
            if (!e) return 0;
            var t = p(e).css("transition-duration"), n = p(e).css("transition-delay"), i = parseFloat(t),
                o = parseFloat(n);
            return i || o ? (t = t.split(",")[0], n = n.split(",")[0], 1e3 * (parseFloat(t) + parseFloat(n))) : 0
        }, reflow: function (e) {
            return e.offsetHeight
        }, triggerTransitionEnd: function (e) {
            p(e).trigger(n)
        }, supportsTransitionEnd: function () {
            return Boolean(n)
        }, isElement: function (e) {
            return (e[0] || e).nodeType
        }, typeCheckConfig: function (e, t, n) {
            for (var i in n) if (Object.prototype.hasOwnProperty.call(n, i)) {
                var o = n[i], r = t[i],
                    s = r && m.isElement(r) ? "element" : (a = r, {}.toString.call(a).match(/\s([a-z]+)/i)[1].toLowerCase());
                if (!new RegExp(o).test(s)) throw new Error(e.toUpperCase() + ': Option "' + i + '" provided type "' + s + '" but expected type "' + o + '".')
            }
            var a
        }, findShadowRoot: function (e) {
            if (!document.documentElement.attachShadow) return null;
            if ("function" != typeof e.getRootNode) return e instanceof ShadowRoot ? e : e.parentNode ? m.findShadowRoot(e.parentNode) : null;
            var t = e.getRootNode();
            return t instanceof ShadowRoot ? t : null
        }, jQueryDetection: function () {
            if ("undefined" == typeof p) throw new TypeError("Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript.");
            var e = p.fn.jquery.split(" ")[0].split(".");
            if (e[0] < 2 && e[1] < 9 || 1 === e[0] && 9 === e[1] && e[2] < 1 || 4 <= e[0]) throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0")
        }
    };
    m.jQueryDetection(), p.fn.emulateTransitionEnd = o, p.event.special[m.TRANSITION_END] = {
        bindType: n,
        delegateType: n,
        handle: function (e) {
            if (p(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
        }
    };
    var r = "alert", a = "bs.alert", c = "." + a, h = p.fn[r],
        u = {CLOSE: "close" + c, CLOSED: "closed" + c, CLICK_DATA_API: "click" + c + ".data-api"}, f = "alert",
        d = "fade", g = "show", _ = function () {
            function i(e) {
                this._element = e
            }

            var e = i.prototype;
            return e.close = function (e) {
                var t = this._element;
                e && (t = this._getRootElement(e)), this._triggerCloseEvent(t).isDefaultPrevented() || this._removeElement(t)
            }, e.dispose = function () {
                p.removeData(this._element, a), this._element = null
            }, e._getRootElement = function (e) {
                var t = m.getSelectorFromElement(e), n = !1;
                return t && (n = document.querySelector(t)), n = n || p(e).closest("." + f)[0]
            }, e._triggerCloseEvent = function (e) {
                var t = p.Event(u.CLOSE);
                return p(e).trigger(t), t
            }, e._removeElement = function (t) {
                var n = this;
                if (p(t).removeClass(g), p(t).hasClass(d)) {
                    var e = m.getTransitionDurationFromElement(t);
                    p(t).one(m.TRANSITION_END, function (e) {
                        return n._destroyElement(t, e)
                    }).emulateTransitionEnd(e)
                } else this._destroyElement(t)
            }, e._destroyElement = function (e) {
                p(e).detach().trigger(u.CLOSED).remove()
            }, i._jQueryInterface = function (n) {
                return this.each(function () {
                    var e = p(this), t = e.data(a);
                    t || (t = new i(this), e.data(a, t)), "close" === n && t[n](this)
                })
            }, i._handleDismiss = function (t) {
                return function (e) {
                    e && e.preventDefault(), t.close(this)
                }
            }, s(i, null, [{
                key: "VERSION", get: function () {
                    return "4.4.1"
                }
            }]), i
        }();
    p(document).on(u.CLICK_DATA_API, '[data-dismiss="alert"]', _._handleDismiss(new _)), p.fn[r] = _._jQueryInterface, p.fn[r].Constructor = _, p.fn[r].noConflict = function () {
        return p.fn[r] = h, _._jQueryInterface
    };
    var v = "button", y = "bs.button", E = "." + y, b = ".data-api", w = p.fn[v], T = "active", C = "btn", S = "focus",
        D = '[data-toggle^="button"]', I = '[data-toggle="buttons"]', A = '[data-toggle="button"]',
        O = '[data-toggle="buttons"] .btn', N = 'input:not([type="hidden"])', k = ".active", L = ".btn", P = {
            CLICK_DATA_API: "click" + E + b,
            FOCUS_BLUR_DATA_API: "focus" + E + b + " blur" + E + b,
            LOAD_DATA_API: "load" + E + b
        }, x = function () {
            function n(e) {
                this._element = e
            }

            var e = n.prototype;
            return e.toggle = function () {
                var e = !0, t = !0, n = p(this._element).closest(I)[0];
                if (n) {
                    var i = this._element.querySelector(N);
                    if (i) {
                        if ("radio" === i.type) if (i.checked && this._element.classList.contains(T)) e = !1; else {
                            var o = n.querySelector(k);
                            o && p(o).removeClass(T)
                        } else "checkbox" === i.type ? "LABEL" === this._element.tagName && i.checked === this._element.classList.contains(T) && (e = !1) : e = !1;
                        e && (i.checked = !this._element.classList.contains(T), p(i).trigger("change")), i.focus(), t = !1
                    }
                }
                this._element.hasAttribute("disabled") || this._element.classList.contains("disabled") || (t && this._element.setAttribute("aria-pressed", !this._element.classList.contains(T)), e && p(this._element).toggleClass(T))
            }, e.dispose = function () {
                p.removeData(this._element, y), this._element = null
            }, n._jQueryInterface = function (t) {
                return this.each(function () {
                    var e = p(this).data(y);
                    e || (e = new n(this), p(this).data(y, e)), "toggle" === t && e[t]()
                })
            }, s(n, null, [{
                key: "VERSION", get: function () {
                    return "4.4.1"
                }
            }]), n
        }();
    p(document).on(P.CLICK_DATA_API, D, function (e) {
        var t = e.target;
        if (p(t).hasClass(C) || (t = p(t).closest(L)[0]), !t || t.hasAttribute("disabled") || t.classList.contains("disabled")) e.preventDefault(); else {
            var n = t.querySelector(N);
            if (n && (n.hasAttribute("disabled") || n.classList.contains("disabled"))) return void e.preventDefault();
            x._jQueryInterface.call(p(t), "toggle")
        }
    }).on(P.FOCUS_BLUR_DATA_API, D, function (e) {
        var t = p(e.target).closest(L)[0];
        p(t).toggleClass(S, /^focus(in)?$/.test(e.type))
    }), p(window).on(P.LOAD_DATA_API, function () {
        for (var e = [].slice.call(document.querySelectorAll(O)), t = 0, n = e.length; t < n; t++) {
            var i = e[t], o = i.querySelector(N);
            o.checked || o.hasAttribute("checked") ? i.classList.add(T) : i.classList.remove(T)
        }
        for (var r = 0, s = (e = [].slice.call(document.querySelectorAll(A))).length; r < s; r++) {
            var a = e[r];
            "true" === a.getAttribute("aria-pressed") ? a.classList.add(T) : a.classList.remove(T)
        }
    }), p.fn[v] = x._jQueryInterface, p.fn[v].Constructor = x, p.fn[v].noConflict = function () {
        return p.fn[v] = w, x._jQueryInterface
    };
    var j = "carousel", H = "bs.carousel", R = "." + H, F = ".data-api", M = p.fn[j],
        W = {interval: 5e3, keyboard: !0, slide: !1, pause: "hover", wrap: !0, touch: !0}, U = {
            interval: "(number|boolean)",
            keyboard: "boolean",
            slide: "(boolean|string)",
            pause: "(string|boolean)",
            wrap: "boolean",
            touch: "boolean"
        }, B = "next", q = "prev", K = "left", Q = "right", V = {
            SLIDE: "slide" + R,
            SLID: "slid" + R,
            KEYDOWN: "keydown" + R,
            MOUSEENTER: "mouseenter" + R,
            MOUSELEAVE: "mouseleave" + R,
            TOUCHSTART: "touchstart" + R,
            TOUCHMOVE: "touchmove" + R,
            TOUCHEND: "touchend" + R,
            POINTERDOWN: "pointerdown" + R,
            POINTERUP: "pointerup" + R,
            DRAG_START: "dragstart" + R,
            LOAD_DATA_API: "load" + R + F,
            CLICK_DATA_API: "click" + R + F
        }, Y = "carousel", z = "active", X = "slide", G = "carousel-item-right", $ = "carousel-item-left",
        J = "carousel-item-next", Z = "carousel-item-prev", ee = "pointer-event", te = ".active",
        ne = ".active.carousel-item", ie = ".carousel-item", oe = ".carousel-item img",
        re = ".carousel-item-next, .carousel-item-prev", se = ".carousel-indicators",
        ae = "[data-slide], [data-slide-to]", le = '[data-ride="carousel"]', ce = {TOUCH: "touch", PEN: "pen"},
        he = function () {
            function r(e, t) {
                this._items = null, this._interval = null, this._activeElement = null, this._isPaused = !1, this._isSliding = !1, this.touchTimeout = null, this.touchStartX = 0, this.touchDeltaX = 0, this._config = this._getConfig(t), this._element = e, this._indicatorsElement = this._element.querySelector(se), this._touchSupported = "ontouchstart" in document.documentElement || 0 < navigator.maxTouchPoints, this._pointerEvent = Boolean(window.PointerEvent || window.MSPointerEvent), this._addEventListeners()
            }

            var e = r.prototype;
            return e.next = function () {
                this._isSliding || this._slide(B)
            }, e.nextWhenVisible = function () {
                !document.hidden && p(this._element).is(":visible") && "hidden" !== p(this._element).css("visibility") && this.next()
            }, e.prev = function () {
                this._isSliding || this._slide(q)
            }, e.pause = function (e) {
                e || (this._isPaused = !0), this._element.querySelector(re) && (m.triggerTransitionEnd(this._element), this.cycle(!0)), clearInterval(this._interval), this._interval = null
            }, e.cycle = function (e) {
                e || (this._isPaused = !1), this._interval && (clearInterval(this._interval), this._interval = null), this._config.interval && !this._isPaused && (this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval))
            }, e.to = function (e) {
                var t = this;
                this._activeElement = this._element.querySelector(ne);
                var n = this._getItemIndex(this._activeElement);
                if (!(e > this._items.length - 1 || e < 0)) if (this._isSliding) p(this._element).one(V.SLID, function () {
                    return t.to(e)
                }); else {
                    if (n === e) return this.pause(), void this.cycle();
                    var i = n < e ? B : q;
                    this._slide(i, this._items[e])
                }
            }, e.dispose = function () {
                p(this._element).off(R), p.removeData(this._element, H), this._items = null, this._config = null, this._element = null, this._interval = null, this._isPaused = null, this._isSliding = null, this._activeElement = null, this._indicatorsElement = null
            }, e._getConfig = function (e) {
                return e = l({}, W, {}, e), m.typeCheckConfig(j, e, U), e
            }, e._handleSwipe = function () {
                var e = Math.abs(this.touchDeltaX);
                if (!(e <= 40)) {
                    var t = e / this.touchDeltaX;
                    (this.touchDeltaX = 0) < t && this.prev(), t < 0 && this.next()
                }
            }, e._addEventListeners = function () {
                var t = this;
                this._config.keyboard && p(this._element).on(V.KEYDOWN, function (e) {
                    return t._keydown(e)
                }), "hover" === this._config.pause && p(this._element).on(V.MOUSEENTER, function (e) {
                    return t.pause(e)
                }).on(V.MOUSELEAVE, function (e) {
                    return t.cycle(e)
                }), this._config.touch && this._addTouchEventListeners()
            }, e._addTouchEventListeners = function () {
                var t = this;
                if (this._touchSupported) {
                    var n = function (e) {
                        t._pointerEvent && ce[e.originalEvent.pointerType.toUpperCase()] ? t.touchStartX = e.originalEvent.clientX : t._pointerEvent || (t.touchStartX = e.originalEvent.touches[0].clientX)
                    }, i = function (e) {
                        t._pointerEvent && ce[e.originalEvent.pointerType.toUpperCase()] && (t.touchDeltaX = e.originalEvent.clientX - t.touchStartX), t._handleSwipe(), "hover" === t._config.pause && (t.pause(), t.touchTimeout && clearTimeout(t.touchTimeout), t.touchTimeout = setTimeout(function (e) {
                            return t.cycle(e)
                        }, 500 + t._config.interval))
                    };
                    p(this._element.querySelectorAll(oe)).on(V.DRAG_START, function (e) {
                        return e.preventDefault()
                    }), this._pointerEvent ? (p(this._element).on(V.POINTERDOWN, function (e) {
                        return n(e)
                    }), p(this._element).on(V.POINTERUP, function (e) {
                        return i(e)
                    }), this._element.classList.add(ee)) : (p(this._element).on(V.TOUCHSTART, function (e) {
                        return n(e)
                    }), p(this._element).on(V.TOUCHMOVE, function (e) {
                        return function (e) {
                            e.originalEvent.touches && 1 < e.originalEvent.touches.length ? t.touchDeltaX = 0 : t.touchDeltaX = e.originalEvent.touches[0].clientX - t.touchStartX
                        }(e)
                    }), p(this._element).on(V.TOUCHEND, function (e) {
                        return i(e)
                    }))
                }
            }, e._keydown = function (e) {
                if (!/input|textarea/i.test(e.target.tagName)) switch (e.which) {
                    case 37:
                        e.preventDefault(), this.prev();
                        break;
                    case 39:
                        e.preventDefault(), this.next()
                }
            }, e._getItemIndex = function (e) {
                return this._items = e && e.parentNode ? [].slice.call(e.parentNode.querySelectorAll(ie)) : [], this._items.indexOf(e)
            }, e._getItemByDirection = function (e, t) {
                var n = e === B, i = e === q, o = this._getItemIndex(t), r = this._items.length - 1;
                if ((i && 0 === o || n && o === r) && !this._config.wrap) return t;
                var s = (o + (e === q ? -1 : 1)) % this._items.length;
                return -1 == s ? this._items[this._items.length - 1] : this._items[s]
            }, e._triggerSlideEvent = function (e, t) {
                var n = this._getItemIndex(e), i = this._getItemIndex(this._element.querySelector(ne)),
                    o = p.Event(V.SLIDE, {relatedTarget: e, direction: t, from: i, to: n});
                return p(this._element).trigger(o), o
            }, e._setActiveIndicatorElement = function (e) {
                if (this._indicatorsElement) {
                    var t = [].slice.call(this._indicatorsElement.querySelectorAll(te));
                    p(t).removeClass(z);
                    var n = this._indicatorsElement.children[this._getItemIndex(e)];
                    n && p(n).addClass(z)
                }
            }, e._slide = function (e, t) {
                var n, i, o, r = this, s = this._element.querySelector(ne), a = this._getItemIndex(s),
                    l = t || s && this._getItemByDirection(e, s), c = this._getItemIndex(l),
                    h = Boolean(this._interval);
                if (o = e === B ? (n = $, i = J, K) : (n = G, i = Z, Q), l && p(l).hasClass(z)) this._isSliding = !1; else if (!this._triggerSlideEvent(l, o).isDefaultPrevented() && s && l) {
                    this._isSliding = !0, h && this.pause(), this._setActiveIndicatorElement(l);
                    var u = p.Event(V.SLID, {relatedTarget: l, direction: o, from: a, to: c});
                    if (p(this._element).hasClass(X)) {
                        p(l).addClass(i), m.reflow(l), p(s).addClass(n), p(l).addClass(n);
                        var f = parseInt(l.getAttribute("data-interval"), 10);
                        f ? (this._config.defaultInterval = this._config.defaultInterval || this._config.interval, this._config.interval = f) : this._config.interval = this._config.defaultInterval || this._config.interval;
                        var d = m.getTransitionDurationFromElement(s);
                        p(s).one(m.TRANSITION_END, function () {
                            p(l).removeClass(n + " " + i).addClass(z), p(s).removeClass(z + " " + i + " " + n), r._isSliding = !1, setTimeout(function () {
                                return p(r._element).trigger(u)
                            }, 0)
                        }).emulateTransitionEnd(d)
                    } else p(s).removeClass(z), p(l).addClass(z), this._isSliding = !1, p(this._element).trigger(u);
                    h && this.cycle()
                }
            }, r._jQueryInterface = function (i) {
                return this.each(function () {
                    var e = p(this).data(H), t = l({}, W, {}, p(this).data());
                    "object" == typeof i && (t = l({}, t, {}, i));
                    var n = "string" == typeof i ? i : t.slide;
                    if (e || (e = new r(this, t), p(this).data(H, e)), "number" == typeof i) e.to(i); else if ("string" == typeof n) {
                        if ("undefined" == typeof e[n]) throw new TypeError('No method named "' + n + '"');
                        e[n]()
                    } else t.interval && t.ride && (e.pause(), e.cycle())
                })
            }, r._dataApiClickHandler = function (e) {
                var t = m.getSelectorFromElement(this);
                if (t) {
                    var n = p(t)[0];
                    if (n && p(n).hasClass(Y)) {
                        var i = l({}, p(n).data(), {}, p(this).data()), o = this.getAttribute("data-slide-to");
                        o && (i.interval = !1), r._jQueryInterface.call(p(n), i), o && p(n).data(H).to(o), e.preventDefault()
                    }
                }
            }, s(r, null, [{
                key: "VERSION", get: function () {
                    return "4.4.1"
                }
            }, {
                key: "Default", get: function () {
                    return W
                }
            }]), r
        }();
    p(document).on(V.CLICK_DATA_API, ae, he._dataApiClickHandler), p(window).on(V.LOAD_DATA_API, function () {
        for (var e = [].slice.call(document.querySelectorAll(le)), t = 0, n = e.length; t < n; t++) {
            var i = p(e[t]);
            he._jQueryInterface.call(i, i.data())
        }
    }), p.fn[j] = he._jQueryInterface, p.fn[j].Constructor = he, p.fn[j].noConflict = function () {
        return p.fn[j] = M, he._jQueryInterface
    };
    var ue = "collapse", fe = "bs.collapse", de = "." + fe, pe = p.fn[ue], me = {toggle: !0, parent: ""},
        ge = {toggle: "boolean", parent: "(string|element)"}, _e = {
            SHOW: "show" + de,
            SHOWN: "shown" + de,
            HIDE: "hide" + de,
            HIDDEN: "hidden" + de,
            CLICK_DATA_API: "click" + de + ".data-api"
        }, ve = "show", ye = "collapse", Ee = "collapsing", be = "collapsed", we = "width", Te = "height",
        Ce = ".show, .collapsing", Se = '[data-toggle="collapse"]', De = function () {
            function a(t, e) {
                this._isTransitioning = !1, this._element = t, this._config = this._getConfig(e), this._triggerArray = [].slice.call(document.querySelectorAll('[data-toggle="collapse"][href="#' + t.id + '"],[data-toggle="collapse"][data-target="#' + t.id + '"]'));
                for (var n = [].slice.call(document.querySelectorAll(Se)), i = 0, o = n.length; i < o; i++) {
                    var r = n[i], s = m.getSelectorFromElement(r),
                        a = [].slice.call(document.querySelectorAll(s)).filter(function (e) {
                            return e === t
                        });
                    null !== s && 0 < a.length && (this._selector = s, this._triggerArray.push(r))
                }
                this._parent = this._config.parent ? this._getParent() : null, this._config.parent || this._addAriaAndCollapsedClass(this._element, this._triggerArray), this._config.toggle && this.toggle()
            }

            var e = a.prototype;
            return e.toggle = function () {
                p(this._element).hasClass(ve) ? this.hide() : this.show()
            }, e.show = function () {
                var e, t, n = this;
                if (!this._isTransitioning && !p(this._element).hasClass(ve) && (this._parent && 0 === (e = [].slice.call(this._parent.querySelectorAll(Ce)).filter(function (e) {
                    return "string" == typeof n._config.parent ? e.getAttribute("data-parent") === n._config.parent : e.classList.contains(ye)
                })).length && (e = null), !(e && (t = p(e).not(this._selector).data(fe)) && t._isTransitioning))) {
                    var i = p.Event(_e.SHOW);
                    if (p(this._element).trigger(i), !i.isDefaultPrevented()) {
                        e && (a._jQueryInterface.call(p(e).not(this._selector), "hide"), t || p(e).data(fe, null));
                        var o = this._getDimension();
                        p(this._element).removeClass(ye).addClass(Ee), this._element.style[o] = 0, this._triggerArray.length && p(this._triggerArray).removeClass(be).attr("aria-expanded", !0), this.setTransitioning(!0);
                        var r = "scroll" + (o[0].toUpperCase() + o.slice(1)),
                            s = m.getTransitionDurationFromElement(this._element);
                        p(this._element).one(m.TRANSITION_END, function () {
                            p(n._element).removeClass(Ee).addClass(ye).addClass(ve), n._element.style[o] = "", n.setTransitioning(!1), p(n._element).trigger(_e.SHOWN)
                        }).emulateTransitionEnd(s), this._element.style[o] = this._element[r] + "px"
                    }
                }
            }, e.hide = function () {
                var e = this;
                if (!this._isTransitioning && p(this._element).hasClass(ve)) {
                    var t = p.Event(_e.HIDE);
                    if (p(this._element).trigger(t), !t.isDefaultPrevented()) {
                        var n = this._getDimension();
                        this._element.style[n] = this._element.getBoundingClientRect()[n] + "px", m.reflow(this._element), p(this._element).addClass(Ee).removeClass(ye).removeClass(ve);
                        var i = this._triggerArray.length;
                        if (0 < i) for (var o = 0; o < i; o++) {
                            var r = this._triggerArray[o], s = m.getSelectorFromElement(r);
                            if (null !== s) p([].slice.call(document.querySelectorAll(s))).hasClass(ve) || p(r).addClass(be).attr("aria-expanded", !1)
                        }
                        this.setTransitioning(!0);
                        this._element.style[n] = "";
                        var a = m.getTransitionDurationFromElement(this._element);
                        p(this._element).one(m.TRANSITION_END, function () {
                            e.setTransitioning(!1), p(e._element).removeClass(Ee).addClass(ye).trigger(_e.HIDDEN)
                        }).emulateTransitionEnd(a)
                    }
                }
            }, e.setTransitioning = function (e) {
                this._isTransitioning = e
            }, e.dispose = function () {
                p.removeData(this._element, fe), this._config = null, this._parent = null, this._element = null, this._triggerArray = null, this._isTransitioning = null
            }, e._getConfig = function (e) {
                return (e = l({}, me, {}, e)).toggle = Boolean(e.toggle), m.typeCheckConfig(ue, e, ge), e
            }, e._getDimension = function () {
                return p(this._element).hasClass(we) ? we : Te
            }, e._getParent = function () {
                var e, n = this;
                m.isElement(this._config.parent) ? (e = this._config.parent, "undefined" != typeof this._config.parent.jquery && (e = this._config.parent[0])) : e = document.querySelector(this._config.parent);
                var t = '[data-toggle="collapse"][data-parent="' + this._config.parent + '"]',
                    i = [].slice.call(e.querySelectorAll(t));
                return p(i).each(function (e, t) {
                    n._addAriaAndCollapsedClass(a._getTargetFromElement(t), [t])
                }), e
            }, e._addAriaAndCollapsedClass = function (e, t) {
                var n = p(e).hasClass(ve);
                t.length && p(t).toggleClass(be, !n).attr("aria-expanded", n)
            }, a._getTargetFromElement = function (e) {
                var t = m.getSelectorFromElement(e);
                return t ? document.querySelector(t) : null
            }, a._jQueryInterface = function (i) {
                return this.each(function () {
                    var e = p(this), t = e.data(fe), n = l({}, me, {}, e.data(), {}, "object" == typeof i && i ? i : {});
                    if (!t && n.toggle && /show|hide/.test(i) && (n.toggle = !1), t || (t = new a(this, n), e.data(fe, t)), "string" == typeof i) {
                        if ("undefined" == typeof t[i]) throw new TypeError('No method named "' + i + '"');
                        t[i]()
                    }
                })
            }, s(a, null, [{
                key: "VERSION", get: function () {
                    return "4.4.1"
                }
            }, {
                key: "Default", get: function () {
                    return me
                }
            }]), a
        }();
    p(document).on(_e.CLICK_DATA_API, Se, function (e) {
        "A" === e.currentTarget.tagName && e.preventDefault();
        var n = p(this), t = m.getSelectorFromElement(this), i = [].slice.call(document.querySelectorAll(t));
        p(i).each(function () {
            var e = p(this), t = e.data(fe) ? "toggle" : n.data();
            De._jQueryInterface.call(e, t)
        })
    }), p.fn[ue] = De._jQueryInterface, p.fn[ue].Constructor = De, p.fn[ue].noConflict = function () {
        return p.fn[ue] = pe, De._jQueryInterface
    };
    var Ie = "undefined" != typeof window && "undefined" != typeof document && "undefined" != typeof navigator,
        Ae = function () {
            for (var e = ["Edge", "Trident", "Firefox"], t = 0; t < e.length; t += 1) if (Ie && 0 <= navigator.userAgent.indexOf(e[t])) return 1;
            return 0
        }();
    var Oe = Ie && window.Promise ? function (e) {
        var t = !1;
        return function () {
            t || (t = !0, window.Promise.resolve().then(function () {
                t = !1, e()
            }))
        }
    } : function (e) {
        var t = !1;
        return function () {
            t || (t = !0, setTimeout(function () {
                t = !1, e()
            }, Ae))
        }
    };

    function Ne(e) {
        return e && "[object Function]" === {}.toString.call(e)
    }

    function ke(e, t) {
        if (1 !== e.nodeType) return [];
        var n = e.ownerDocument.defaultView.getComputedStyle(e, null);
        return t ? n[t] : n
    }

    function Le(e) {
        return "HTML" === e.nodeName ? e : e.parentNode || e.host
    }

    function Pe(e) {
        if (!e) return document.body;
        switch (e.nodeName) {
            case"HTML":
            case"BODY":
                return e.ownerDocument.body;
            case"#document":
                return e.body
        }
        var t = ke(e), n = t.overflow, i = t.overflowX, o = t.overflowY;
        return /(auto|scroll|overlay)/.test(n + o + i) ? e : Pe(Le(e))
    }

    function xe(e) {
        return e && e.referenceNode ? e.referenceNode : e
    }

    var je = Ie && !(!window.MSInputMethodContext || !document.documentMode),
        He = Ie && /MSIE 10/.test(navigator.userAgent);

    function Re(e) {
        return 11 === e ? je : 10 === e ? He : je || He
    }

    function Fe(e) {
        if (!e) return document.documentElement;
        for (var t = Re(10) ? document.body : null, n = e.offsetParent || null; n === t && e.nextElementSibling;) n = (e = e.nextElementSibling).offsetParent;
        var i = n && n.nodeName;
        return i && "BODY" !== i && "HTML" !== i ? -1 !== ["TH", "TD", "TABLE"].indexOf(n.nodeName) && "static" === ke(n, "position") ? Fe(n) : n : e ? e.ownerDocument.documentElement : document.documentElement
    }

    function Me(e) {
        return null !== e.parentNode ? Me(e.parentNode) : e
    }

    function We(e, t) {
        if (!(e && e.nodeType && t && t.nodeType)) return document.documentElement;
        var n = e.compareDocumentPosition(t) & Node.DOCUMENT_POSITION_FOLLOWING, i = n ? e : t, o = n ? t : e,
            r = document.createRange();
        r.setStart(i, 0), r.setEnd(o, 0);
        var s = r.commonAncestorContainer;
        if (e !== s && t !== s || i.contains(o)) return function (e) {
            var t = e.nodeName;
            return "BODY" !== t && ("HTML" === t || Fe(e.firstElementChild) === e)
        }(s) ? s : Fe(s);
        var a = Me(e);
        return a.host ? We(a.host, t) : We(e, Me(t).host)
    }

    function Ue(e, t) {
        var n = "top" === (1 < arguments.length && void 0 !== t ? t : "top") ? "scrollTop" : "scrollLeft",
            i = e.nodeName;
        if ("BODY" !== i && "HTML" !== i) return e[n];
        var o = e.ownerDocument.documentElement;
        return (e.ownerDocument.scrollingElement || o)[n]
    }

    function Be(e, t) {
        var n = "x" === t ? "Left" : "Top", i = "Left" == n ? "Right" : "Bottom";
        return parseFloat(e["border" + n + "Width"], 10) + parseFloat(e["border" + i + "Width"], 10)
    }

    function qe(e, t, n, i) {
        return Math.max(t["offset" + e], t["scroll" + e], n["client" + e], n["offset" + e], n["scroll" + e], Re(10) ? parseInt(n["offset" + e]) + parseInt(i["margin" + ("Height" === e ? "Top" : "Left")]) + parseInt(i["margin" + ("Height" === e ? "Bottom" : "Right")]) : 0)
    }

    function Ke(e) {
        var t = e.body, n = e.documentElement, i = Re(10) && getComputedStyle(n);
        return {height: qe("Height", t, n, i), width: qe("Width", t, n, i)}
    }

    var Qe = function (e, t, n) {
        return t && Ve(e.prototype, t), n && Ve(e, n), e
    };

    function Ve(e, t) {
        for (var n = 0; n < t.length; n++) {
            var i = t[n];
            i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
        }
    }

    function Ye(e, t, n) {
        return t in e ? Object.defineProperty(e, t, {
            value: n,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : e[t] = n, e
    }

    var ze = Object.assign || function (e) {
        for (var t = 1; t < arguments.length; t++) {
            var n = arguments[t];
            for (var i in n) Object.prototype.hasOwnProperty.call(n, i) && (e[i] = n[i])
        }
        return e
    };

    function Xe(e) {
        return ze({}, e, {right: e.left + e.width, bottom: e.top + e.height})
    }

    function Ge(e) {
        var t = {};
        try {
            if (Re(10)) {
                t = e.getBoundingClientRect();
                var n = Ue(e, "top"), i = Ue(e, "left");
                t.top += n, t.left += i, t.bottom += n, t.right += i
            } else t = e.getBoundingClientRect()
        } catch (e) {
        }
        var o = {left: t.left, top: t.top, width: t.right - t.left, height: t.bottom - t.top},
            r = "HTML" === e.nodeName ? Ke(e.ownerDocument) : {}, s = r.width || e.clientWidth || o.width,
            a = r.height || e.clientHeight || o.height, l = e.offsetWidth - s, c = e.offsetHeight - a;
        if (l || c) {
            var h = ke(e);
            l -= Be(h, "x"), c -= Be(h, "y"), o.width -= l, o.height -= c
        }
        return Xe(o)
    }

    function $e(e, t, n) {
        var i = 2 < arguments.length && void 0 !== n && n, o = Re(10), r = "HTML" === t.nodeName, s = Ge(e), a = Ge(t),
            l = Pe(e), c = ke(t), h = parseFloat(c.borderTopWidth, 10), u = parseFloat(c.borderLeftWidth, 10);
        i && r && (a.top = Math.max(a.top, 0), a.left = Math.max(a.left, 0));
        var f = Xe({top: s.top - a.top - h, left: s.left - a.left - u, width: s.width, height: s.height});
        if (f.marginTop = 0, f.marginLeft = 0, !o && r) {
            var d = parseFloat(c.marginTop, 10), p = parseFloat(c.marginLeft, 10);
            f.top -= h - d, f.bottom -= h - d, f.left -= u - p, f.right -= u - p, f.marginTop = d, f.marginLeft = p
        }
        return (o && !i ? t.contains(l) : t === l && "BODY" !== l.nodeName) && (f = function (e, t, n) {
            var i = 2 < arguments.length && void 0 !== n && n, o = Ue(t, "top"), r = Ue(t, "left"), s = i ? -1 : 1;
            return e.top += o * s, e.bottom += o * s, e.left += r * s, e.right += r * s, e
        }(f, t)), f
    }

    function Je(e) {
        if (!e || !e.parentElement || Re()) return document.documentElement;
        for (var t = e.parentElement; t && "none" === ke(t, "transform");) t = t.parentElement;
        return t || document.documentElement
    }

    function Ze(e, t, n, i, o) {
        var r = 4 < arguments.length && void 0 !== o && o, s = {top: 0, left: 0}, a = r ? Je(e) : We(e, xe(t));
        if ("viewport" === i) s = function (e, t) {
            var n = 1 < arguments.length && void 0 !== t && t, i = e.ownerDocument.documentElement, o = $e(e, i),
                r = Math.max(i.clientWidth, window.innerWidth || 0),
                s = Math.max(i.clientHeight, window.innerHeight || 0), a = n ? 0 : Ue(i), l = n ? 0 : Ue(i, "left");
            return Xe({top: a - o.top + o.marginTop, left: l - o.left + o.marginLeft, width: r, height: s})
        }(a, r); else {
            var l = void 0;
            "scrollParent" === i ? "BODY" === (l = Pe(Le(t))).nodeName && (l = e.ownerDocument.documentElement) : l = "window" === i ? e.ownerDocument.documentElement : i;
            var c = $e(l, a, r);
            if ("HTML" !== l.nodeName || function e(t) {
                var n = t.nodeName;
                if ("BODY" === n || "HTML" === n) return !1;
                if ("fixed" === ke(t, "position")) return !0;
                var i = Le(t);
                return !!i && e(i)
            }(a)) s = c; else {
                var h = Ke(e.ownerDocument), u = h.height, f = h.width;
                s.top += c.top - c.marginTop, s.bottom = u + c.top, s.left += c.left - c.marginLeft, s.right = f + c.left
            }
        }
        var d = "number" == typeof (n = n || 0);
        return s.left += d ? n : n.left || 0, s.top += d ? n : n.top || 0, s.right -= d ? n : n.right || 0, s.bottom -= d ? n : n.bottom || 0, s
    }

    function et(e, t, i, n, o, r) {
        var s = 5 < arguments.length && void 0 !== r ? r : 0;
        if (-1 === e.indexOf("auto")) return e;
        var a = Ze(i, n, s, o), l = {
            top: {width: a.width, height: t.top - a.top},
            right: {width: a.right - t.right, height: a.height},
            bottom: {width: a.width, height: a.bottom - t.bottom},
            left: {width: t.left - a.left, height: a.height}
        }, c = Object.keys(l).map(function (e) {
            return ze({key: e}, l[e], {
                area: function (e) {
                    return e.width * e.height
                }(l[e])
            })
        }).sort(function (e, t) {
            return t.area - e.area
        }), h = c.filter(function (e) {
            var t = e.width, n = e.height;
            return t >= i.clientWidth && n >= i.clientHeight
        }), u = 0 < h.length ? h[0].key : c[0].key, f = e.split("-")[1];
        return u + (f ? "-" + f : "")
    }

    function tt(e, t, n, i) {
        var o = 3 < arguments.length && void 0 !== i ? i : null;
        return $e(n, o ? Je(t) : We(t, xe(n)), o)
    }

    function nt(e) {
        var t = e.ownerDocument.defaultView.getComputedStyle(e),
            n = parseFloat(t.marginTop || 0) + parseFloat(t.marginBottom || 0),
            i = parseFloat(t.marginLeft || 0) + parseFloat(t.marginRight || 0);
        return {width: e.offsetWidth + i, height: e.offsetHeight + n}
    }

    function it(e) {
        var t = {left: "right", right: "left", bottom: "top", top: "bottom"};
        return e.replace(/left|right|bottom|top/g, function (e) {
            return t[e]
        })
    }

    function ot(e, t, n) {
        n = n.split("-")[0];
        var i = nt(e), o = {width: i.width, height: i.height}, r = -1 !== ["right", "left"].indexOf(n),
            s = r ? "top" : "left", a = r ? "left" : "top", l = r ? "height" : "width", c = r ? "width" : "height";
        return o[s] = t[s] + t[l] / 2 - i[l] / 2, o[a] = n === a ? t[a] - i[c] : t[it(a)], o
    }

    function rt(e, t) {
        return Array.prototype.find ? e.find(t) : e.filter(t)[0]
    }

    function st(e, n, t) {
        return (void 0 === t ? e : e.slice(0, function (e, t, n) {
            if (Array.prototype.findIndex) return e.findIndex(function (e) {
                return e[t] === n
            });
            var i = rt(e, function (e) {
                return e[t] === n
            });
            return e.indexOf(i)
        }(e, "name", t))).forEach(function (e) {
            e.function && console.warn("`modifier.function` is deprecated, use `modifier.fn`!");
            var t = e.function || e.fn;
            e.enabled && Ne(t) && (n.offsets.popper = Xe(n.offsets.popper), n.offsets.reference = Xe(n.offsets.reference), n = t(n, e))
        }), n
    }

    function at(e, n) {
        return e.some(function (e) {
            var t = e.name;
            return e.enabled && t === n
        })
    }

    function lt(e) {
        for (var t = [!1, "ms", "Webkit", "Moz", "O"], n = e.charAt(0).toUpperCase() + e.slice(1), i = 0; i < t.length; i++) {
            var o = t[i], r = o ? "" + o + n : e;
            if ("undefined" != typeof document.body.style[r]) return r
        }
        return null
    }

    function ct(e) {
        var t = e.ownerDocument;
        return t ? t.defaultView : window
    }

    function ht(e, t, n, i) {
        n.updateBound = i, ct(e).addEventListener("resize", n.updateBound, {passive: !0});
        var o = Pe(e);
        return function e(t, n, i, o) {
            var r = "BODY" === t.nodeName, s = r ? t.ownerDocument.defaultView : t;
            s.addEventListener(n, i, {passive: !0}), r || e(Pe(s.parentNode), n, i, o), o.push(s)
        }(o, "scroll", n.updateBound, n.scrollParents), n.scrollElement = o, n.eventsEnabled = !0, n
    }

    function ut() {
        this.state.eventsEnabled && (cancelAnimationFrame(this.scheduleUpdate), this.state = function (e, t) {
            return ct(e).removeEventListener("resize", t.updateBound), t.scrollParents.forEach(function (e) {
                e.removeEventListener("scroll", t.updateBound)
            }), t.updateBound = null, t.scrollParents = [], t.scrollElement = null, t.eventsEnabled = !1, t
        }(this.reference, this.state))
    }

    function ft(e) {
        return "" !== e && !isNaN(parseFloat(e)) && isFinite(e)
    }

    function dt(n, i) {
        Object.keys(i).forEach(function (e) {
            var t = "";
            -1 !== ["width", "height", "top", "right", "bottom", "left"].indexOf(e) && ft(i[e]) && (t = "px"), n.style[e] = i[e] + t
        })
    }

    function pt(e, t) {
        function n(e) {
            return e
        }

        var i = e.offsets, o = i.popper, r = i.reference, s = Math.round, a = Math.floor, l = s(r.width),
            c = s(o.width), h = -1 !== ["left", "right"].indexOf(e.placement), u = -1 !== e.placement.indexOf("-"),
            f = t ? h || u || l % 2 == c % 2 ? s : a : n, d = t ? s : n;
        return {
            left: f(l % 2 == 1 && c % 2 == 1 && !u && t ? o.left - 1 : o.left),
            top: d(o.top),
            bottom: d(o.bottom),
            right: f(o.right)
        }
    }

    var mt = Ie && /Firefox/i.test(navigator.userAgent);

    function gt(e, t, n) {
        var i = rt(e, function (e) {
            return e.name === t
        }), o = !!i && e.some(function (e) {
            return e.name === n && e.enabled && e.order < i.order
        });
        if (!o) {
            var r = "`" + t + "`", s = "`" + n + "`";
            console.warn(s + " modifier is required by " + r + " modifier in order to work, be sure to include it before " + r + "!")
        }
        return o
    }

    var _t = ["auto-start", "auto", "auto-end", "top-start", "top", "top-end", "right-start", "right", "right-end", "bottom-end", "bottom", "bottom-start", "left-end", "left", "left-start"],
        vt = _t.slice(3);

    function yt(e, t) {
        var n = 1 < arguments.length && void 0 !== t && t, i = vt.indexOf(e),
            o = vt.slice(i + 1).concat(vt.slice(0, i));
        return n ? o.reverse() : o
    }

    var Et = "flip", bt = "clockwise", wt = "counterclockwise";

    function Tt(e, o, r, t) {
        var s = [0, 0], a = -1 !== ["right", "left"].indexOf(t), n = e.split(/(\+|\-)/).map(function (e) {
            return e.trim()
        }), i = n.indexOf(rt(n, function (e) {
            return -1 !== e.search(/,|\s/)
        }));
        n[i] && -1 === n[i].indexOf(",") && console.warn("Offsets separated by white space(s) are deprecated, use a comma (,) instead.");
        var l = /\s*,\s*|\s+/,
            c = -1 !== i ? [n.slice(0, i).concat([n[i].split(l)[0]]), [n[i].split(l)[1]].concat(n.slice(i + 1))] : [n];
        return (c = c.map(function (e, t) {
            var n = (1 === t ? !a : a) ? "height" : "width", i = !1;
            return e.reduce(function (e, t) {
                return "" === e[e.length - 1] && -1 !== ["+", "-"].indexOf(t) ? (e[e.length - 1] = t, i = !0, e) : i ? (e[e.length - 1] += t, i = !1, e) : e.concat(t)
            }, []).map(function (e) {
                return function (e, t, n, i) {
                    var o = e.match(/((?:\-|\+)?\d*\.?\d*)(.*)/), r = +o[1], s = o[2];
                    if (!r) return e;
                    if (0 !== s.indexOf("%")) return "vh" !== s && "vw" !== s ? r : ("vh" === s ? Math.max(document.documentElement.clientHeight, window.innerHeight || 0) : Math.max(document.documentElement.clientWidth, window.innerWidth || 0)) / 100 * r;
                    var a = void 0;
                    switch (s) {
                        case"%p":
                            a = n;
                            break;
                        case"%":
                        case"%r":
                        default:
                            a = i
                    }
                    return Xe(a)[t] / 100 * r
                }(e, n, o, r)
            })
        })).forEach(function (n, i) {
            n.forEach(function (e, t) {
                ft(e) && (s[i] += e * ("-" === n[t - 1] ? -1 : 1))
            })
        }), s
    }

    var Ct = {
        placement: "bottom", positionFixed: !1, eventsEnabled: !0, removeOnDestroy: !1, onCreate: function () {
        }, onUpdate: function () {
        }, modifiers: {
            shift: {
                order: 100, enabled: !0, fn: function (e) {
                    var t = e.placement, n = t.split("-")[0], i = t.split("-")[1];
                    if (i) {
                        var o = e.offsets, r = o.reference, s = o.popper, a = -1 !== ["bottom", "top"].indexOf(n),
                            l = a ? "left" : "top", c = a ? "width" : "height",
                            h = {start: Ye({}, l, r[l]), end: Ye({}, l, r[l] + r[c] - s[c])};
                        e.offsets.popper = ze({}, s, h[i])
                    }
                    return e
                }
            }, offset: {
                order: 200, enabled: !0, fn: function (e, t) {
                    var n = t.offset, i = e.placement, o = e.offsets, r = o.popper, s = o.reference,
                        a = i.split("-")[0], l = void 0;
                    return l = ft(+n) ? [+n, 0] : Tt(n, r, s, a), "left" === a ? (r.top += l[0], r.left -= l[1]) : "right" === a ? (r.top += l[0], r.left += l[1]) : "top" === a ? (r.left += l[0], r.top -= l[1]) : "bottom" === a && (r.left += l[0], r.top += l[1]), e.popper = r, e
                }, offset: 0
            }, preventOverflow: {
                order: 300, enabled: !0, fn: function (e, i) {
                    var t = i.boundariesElement || Fe(e.instance.popper);
                    e.instance.reference === t && (t = Fe(t));
                    var n = lt("transform"), o = e.instance.popper.style, r = o.top, s = o.left, a = o[n];
                    o.top = "", o.left = "", o[n] = "";
                    var l = Ze(e.instance.popper, e.instance.reference, i.padding, t, e.positionFixed);
                    o.top = r, o.left = s, o[n] = a, i.boundaries = l;
                    var c = i.priority, h = e.offsets.popper, u = {
                        primary: function (e) {
                            var t = h[e];
                            return h[e] < l[e] && !i.escapeWithReference && (t = Math.max(h[e], l[e])), Ye({}, e, t)
                        }, secondary: function (e) {
                            var t = "right" === e ? "left" : "top", n = h[t];
                            return h[e] > l[e] && !i.escapeWithReference && (n = Math.min(h[t], l[e] - ("right" === e ? h.width : h.height))), Ye({}, t, n)
                        }
                    };
                    return c.forEach(function (e) {
                        var t = -1 !== ["left", "top"].indexOf(e) ? "primary" : "secondary";
                        h = ze({}, h, u[t](e))
                    }), e.offsets.popper = h, e
                }, priority: ["left", "right", "top", "bottom"], padding: 5, boundariesElement: "scrollParent"
            }, keepTogether: {
                order: 400, enabled: !0, fn: function (e) {
                    var t = e.offsets, n = t.popper, i = t.reference, o = e.placement.split("-")[0], r = Math.floor,
                        s = -1 !== ["top", "bottom"].indexOf(o), a = s ? "right" : "bottom", l = s ? "left" : "top",
                        c = s ? "width" : "height";
                    return n[a] < r(i[l]) && (e.offsets.popper[l] = r(i[l]) - n[c]), n[l] > r(i[a]) && (e.offsets.popper[l] = r(i[a])), e
                }
            }, arrow: {
                order: 500, enabled: !0, fn: function (e, t) {
                    var n;
                    if (!gt(e.instance.modifiers, "arrow", "keepTogether")) return e;
                    var i = t.element;
                    if ("string" == typeof i) {
                        if (!(i = e.instance.popper.querySelector(i))) return e
                    } else if (!e.instance.popper.contains(i)) return console.warn("WARNING: `arrow.element` must be child of its popper element!"), e;
                    var o = e.placement.split("-")[0], r = e.offsets, s = r.popper, a = r.reference,
                        l = -1 !== ["left", "right"].indexOf(o), c = l ? "height" : "width", h = l ? "Top" : "Left",
                        u = h.toLowerCase(), f = l ? "left" : "top", d = l ? "bottom" : "right", p = nt(i)[c];
                    a[d] - p < s[u] && (e.offsets.popper[u] -= s[u] - (a[d] - p)), a[u] + p > s[d] && (e.offsets.popper[u] += a[u] + p - s[d]), e.offsets.popper = Xe(e.offsets.popper);
                    var m = a[u] + a[c] / 2 - p / 2, g = ke(e.instance.popper), _ = parseFloat(g["margin" + h], 10),
                        v = parseFloat(g["border" + h + "Width"], 10), y = m - e.offsets.popper[u] - _ - v;
                    return y = Math.max(Math.min(s[c] - p, y), 0), e.arrowElement = i, e.offsets.arrow = (Ye(n = {}, u, Math.round(y)), Ye(n, f, ""), n), e
                }, element: "[x-arrow]"
            }, flip: {
                order: 600,
                enabled: !0,
                fn: function (m, g) {
                    if (at(m.instance.modifiers, "inner")) return m;
                    if (m.flipped && m.placement === m.originalPlacement) return m;
                    var _ = Ze(m.instance.popper, m.instance.reference, g.padding, g.boundariesElement, m.positionFixed),
                        v = m.placement.split("-")[0], y = it(v), E = m.placement.split("-")[1] || "", b = [];
                    switch (g.behavior) {
                        case Et:
                            b = [v, y];
                            break;
                        case bt:
                            b = yt(v);
                            break;
                        case wt:
                            b = yt(v, !0);
                            break;
                        default:
                            b = g.behavior
                    }
                    return b.forEach(function (e, t) {
                        if (v !== e || b.length === t + 1) return m;
                        v = m.placement.split("-")[0], y = it(v);
                        var n = m.offsets.popper, i = m.offsets.reference, o = Math.floor,
                            r = "left" === v && o(n.right) > o(i.left) || "right" === v && o(n.left) < o(i.right) || "top" === v && o(n.bottom) > o(i.top) || "bottom" === v && o(n.top) < o(i.bottom),
                            s = o(n.left) < o(_.left), a = o(n.right) > o(_.right), l = o(n.top) < o(_.top),
                            c = o(n.bottom) > o(_.bottom),
                            h = "left" === v && s || "right" === v && a || "top" === v && l || "bottom" === v && c,
                            u = -1 !== ["top", "bottom"].indexOf(v),
                            f = !!g.flipVariations && (u && "start" === E && s || u && "end" === E && a || !u && "start" === E && l || !u && "end" === E && c),
                            d = !!g.flipVariationsByContent && (u && "start" === E && a || u && "end" === E && s || !u && "start" === E && c || !u && "end" === E && l),
                            p = f || d;
                        (r || h || p) && (m.flipped = !0, (r || h) && (v = b[t + 1]), p && (E = function (e) {
                            return "end" === e ? "start" : "start" === e ? "end" : e
                        }(E)), m.placement = v + (E ? "-" + E : ""), m.offsets.popper = ze({}, m.offsets.popper, ot(m.instance.popper, m.offsets.reference, m.placement)), m = st(m.instance.modifiers, m, "flip"))
                    }), m
                },
                behavior: "flip",
                padding: 5,
                boundariesElement: "viewport",
                flipVariations: !1,
                flipVariationsByContent: !1
            }, inner: {
                order: 700, enabled: !1, fn: function (e) {
                    var t = e.placement, n = t.split("-")[0], i = e.offsets, o = i.popper, r = i.reference,
                        s = -1 !== ["left", "right"].indexOf(n), a = -1 === ["top", "left"].indexOf(n);
                    return o[s ? "left" : "top"] = r[n] - (a ? o[s ? "width" : "height"] : 0), e.placement = it(t), e.offsets.popper = Xe(o), e
                }
            }, hide: {
                order: 800, enabled: !0, fn: function (e) {
                    if (!gt(e.instance.modifiers, "hide", "preventOverflow")) return e;
                    var t = e.offsets.reference, n = rt(e.instance.modifiers, function (e) {
                        return "preventOverflow" === e.name
                    }).boundaries;
                    if (t.bottom < n.top || t.left > n.right || t.top > n.bottom || t.right < n.left) {
                        if (!0 === e.hide) return e;
                        e.hide = !0, e.attributes["x-out-of-boundaries"] = ""
                    } else {
                        if (!1 === e.hide) return e;
                        e.hide = !1, e.attributes["x-out-of-boundaries"] = !1
                    }
                    return e
                }
            }, computeStyle: {
                order: 850, enabled: !0, fn: function (e, t) {
                    var n = t.x, i = t.y, o = e.offsets.popper, r = rt(e.instance.modifiers, function (e) {
                        return "applyStyle" === e.name
                    }).gpuAcceleration;
                    void 0 !== r && console.warn("WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!");
                    var s = void 0 !== r ? r : t.gpuAcceleration, a = Fe(e.instance.popper), l = Ge(a),
                        c = {position: o.position}, h = pt(e, window.devicePixelRatio < 2 || !mt),
                        u = "bottom" === n ? "top" : "bottom", f = "right" === i ? "left" : "right",
                        d = lt("transform"), p = void 0, m = void 0;
                    if (m = "bottom" == u ? "HTML" === a.nodeName ? -a.clientHeight + h.bottom : -l.height + h.bottom : h.top, p = "right" == f ? "HTML" === a.nodeName ? -a.clientWidth + h.right : -l.width + h.right : h.left, s && d) c[d] = "translate3d(" + p + "px, " + m + "px, 0)", c[u] = 0, c[f] = 0, c.willChange = "transform"; else {
                        var g = "bottom" == u ? -1 : 1, _ = "right" == f ? -1 : 1;
                        c[u] = m * g, c[f] = p * _, c.willChange = u + ", " + f
                    }
                    var v = {"x-placement": e.placement};
                    return e.attributes = ze({}, v, e.attributes), e.styles = ze({}, c, e.styles), e.arrowStyles = ze({}, e.offsets.arrow, e.arrowStyles), e
                }, gpuAcceleration: !0, x: "bottom", y: "right"
            }, applyStyle: {
                order: 900, enabled: !0, fn: function (e) {
                    return dt(e.instance.popper, e.styles), function (t, n) {
                        Object.keys(n).forEach(function (e) {
                            !1 !== n[e] ? t.setAttribute(e, n[e]) : t.removeAttribute(e)
                        })
                    }(e.instance.popper, e.attributes), e.arrowElement && Object.keys(e.arrowStyles).length && dt(e.arrowElement, e.arrowStyles), e
                }, onLoad: function (e, t, n, i, o) {
                    var r = tt(o, t, e, n.positionFixed),
                        s = et(n.placement, r, t, e, n.modifiers.flip.boundariesElement, n.modifiers.flip.padding);
                    return t.setAttribute("x-placement", s), dt(t, {position: n.positionFixed ? "fixed" : "absolute"}), n
                }, gpuAcceleration: void 0
            }
        }
    }, St = (Qe(Dt, [{
        key: "update", value: function () {
            return function () {
                if (!this.state.isDestroyed) {
                    var e = {instance: this, styles: {}, arrowStyles: {}, attributes: {}, flipped: !1, offsets: {}};
                    e.offsets.reference = tt(this.state, this.popper, this.reference, this.options.positionFixed), e.placement = et(this.options.placement, e.offsets.reference, this.popper, this.reference, this.options.modifiers.flip.boundariesElement, this.options.modifiers.flip.padding), e.originalPlacement = e.placement, e.positionFixed = this.options.positionFixed, e.offsets.popper = ot(this.popper, e.offsets.reference, e.placement), e.offsets.popper.position = this.options.positionFixed ? "fixed" : "absolute", e = st(this.modifiers, e), this.state.isCreated ? this.options.onUpdate(e) : (this.state.isCreated = !0, this.options.onCreate(e))
                }
            }.call(this)
        }
    }, {
        key: "destroy", value: function () {
            return function () {
                return this.state.isDestroyed = !0, at(this.modifiers, "applyStyle") && (this.popper.removeAttribute("x-placement"), this.popper.style.position = "", this.popper.style.top = "", this.popper.style.left = "", this.popper.style.right = "", this.popper.style.bottom = "", this.popper.style.willChange = "", this.popper.style[lt("transform")] = ""), this.disableEventListeners(), this.options.removeOnDestroy && this.popper.parentNode.removeChild(this.popper), this
            }.call(this)
        }
    }, {
        key: "enableEventListeners", value: function () {
            return function () {
                this.state.eventsEnabled || (this.state = ht(this.reference, this.options, this.state, this.scheduleUpdate))
            }.call(this)
        }
    }, {
        key: "disableEventListeners", value: function () {
            return ut.call(this)
        }
    }]), Dt);

    function Dt(e, t) {
        var n = this, i = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {};
        !function (e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }(this, Dt), this.scheduleUpdate = function () {
            return requestAnimationFrame(n.update)
        }, this.update = Oe(this.update.bind(this)), this.options = ze({}, Dt.Defaults, i), this.state = {
            isDestroyed: !1,
            isCreated: !1,
            scrollParents: []
        }, this.reference = e && e.jquery ? e[0] : e, this.popper = t && t.jquery ? t[0] : t, this.options.modifiers = {}, Object.keys(ze({}, Dt.Defaults.modifiers, i.modifiers)).forEach(function (e) {
            n.options.modifiers[e] = ze({}, Dt.Defaults.modifiers[e] || {}, i.modifiers ? i.modifiers[e] : {})
        }), this.modifiers = Object.keys(this.options.modifiers).map(function (e) {
            return ze({name: e}, n.options.modifiers[e])
        }).sort(function (e, t) {
            return e.order - t.order
        }), this.modifiers.forEach(function (e) {
            e.enabled && Ne(e.onLoad) && e.onLoad(n.reference, n.popper, n.options, e, n.state)
        }), this.update();
        var o = this.options.eventsEnabled;
        o && this.enableEventListeners(), this.state.eventsEnabled = o
    }

    St.Utils = ("undefined" != typeof window ? window : global).PopperUtils, St.placements = _t, St.Defaults = Ct;
    var It = "dropdown", At = "bs.dropdown", Ot = "." + At, Nt = ".data-api", kt = p.fn[It],
        Lt = new RegExp("38|40|27"), Pt = {
            HIDE: "hide" + Ot,
            HIDDEN: "hidden" + Ot,
            SHOW: "show" + Ot,
            SHOWN: "shown" + Ot,
            CLICK: "click" + Ot,
            CLICK_DATA_API: "click" + Ot + Nt,
            KEYDOWN_DATA_API: "keydown" + Ot + Nt,
            KEYUP_DATA_API: "keyup" + Ot + Nt
        }, xt = "disabled", jt = "show", Ht = "dropup", Rt = "dropright", Ft = "dropleft", Mt = "dropdown-menu-right",
        Wt = "position-static", Ut = '[data-toggle="dropdown"]', Bt = ".dropdown form", qt = ".dropdown-menu",
        Kt = ".navbar-nav", Qt = ".dropdown-menu .dropdown-item:not(.disabled):not(:disabled)", Vt = "top-start",
        Yt = "top-end", zt = "bottom-start", Xt = "bottom-end", Gt = "right-start", $t = "left-start", Jt = {
            offset: 0,
            flip: !0,
            boundary: "scrollParent",
            reference: "toggle",
            display: "dynamic",
            popperConfig: null
        }, Zt = {
            offset: "(number|string|function)",
            flip: "boolean",
            boundary: "(string|element)",
            reference: "(string|element)",
            display: "string",
            popperConfig: "(null|object)"
        }, en = function () {
            function c(e, t) {
                this._element = e, this._popper = null, this._config = this._getConfig(t), this._menu = this._getMenuElement(), this._inNavbar = this._detectNavbar(), this._addEventListeners()
            }

            var e = c.prototype;
            return e.toggle = function () {
                if (!this._element.disabled && !p(this._element).hasClass(xt)) {
                    var e = p(this._menu).hasClass(jt);
                    c._clearMenus(), e || this.show(!0)
                }
            }, e.show = function (e) {
                if (void 0 === e && (e = !1), !(this._element.disabled || p(this._element).hasClass(xt) || p(this._menu).hasClass(jt))) {
                    var t = {relatedTarget: this._element}, n = p.Event(Pt.SHOW, t),
                        i = c._getParentFromElement(this._element);
                    if (p(i).trigger(n), !n.isDefaultPrevented()) {
                        if (!this._inNavbar && e) {
                            if ("undefined" == typeof St) throw new TypeError("Bootstrap's dropdowns require Popper.js (https://popper.js.org/)");
                            var o = this._element;
                            "parent" === this._config.reference ? o = i : m.isElement(this._config.reference) && (o = this._config.reference, "undefined" != typeof this._config.reference.jquery && (o = this._config.reference[0])), "scrollParent" !== this._config.boundary && p(i).addClass(Wt), this._popper = new St(o, this._menu, this._getPopperConfig())
                        }
                        "ontouchstart" in document.documentElement && 0 === p(i).closest(Kt).length && p(document.body).children().on("mouseover", null, p.noop), this._element.focus(), this._element.setAttribute("aria-expanded", !0), p(this._menu).toggleClass(jt), p(i).toggleClass(jt).trigger(p.Event(Pt.SHOWN, t))
                    }
                }
            }, e.hide = function () {
                if (!this._element.disabled && !p(this._element).hasClass(xt) && p(this._menu).hasClass(jt)) {
                    var e = {relatedTarget: this._element}, t = p.Event(Pt.HIDE, e),
                        n = c._getParentFromElement(this._element);
                    p(n).trigger(t), t.isDefaultPrevented() || (this._popper && this._popper.destroy(), p(this._menu).toggleClass(jt), p(n).toggleClass(jt).trigger(p.Event(Pt.HIDDEN, e)))
                }
            }, e.dispose = function () {
                p.removeData(this._element, At), p(this._element).off(Ot), this._element = null, (this._menu = null) !== this._popper && (this._popper.destroy(), this._popper = null)
            }, e.update = function () {
                this._inNavbar = this._detectNavbar(), null !== this._popper && this._popper.scheduleUpdate()
            }, e._addEventListeners = function () {
                var t = this;
                p(this._element).on(Pt.CLICK, function (e) {
                    e.preventDefault(), e.stopPropagation(), t.toggle()
                })
            }, e._getConfig = function (e) {
                return e = l({}, this.constructor.Default, {}, p(this._element).data(), {}, e), m.typeCheckConfig(It, e, this.constructor.DefaultType), e
            }, e._getMenuElement = function () {
                if (!this._menu) {
                    var e = c._getParentFromElement(this._element);
                    e && (this._menu = e.querySelector(qt))
                }
                return this._menu
            }, e._getPlacement = function () {
                var e = p(this._element.parentNode), t = zt;
                return e.hasClass(Ht) ? (t = Vt, p(this._menu).hasClass(Mt) && (t = Yt)) : e.hasClass(Rt) ? t = Gt : e.hasClass(Ft) ? t = $t : p(this._menu).hasClass(Mt) && (t = Xt), t
            }, e._detectNavbar = function () {
                return 0 < p(this._element).closest(".navbar").length
            }, e._getOffset = function () {
                var t = this, e = {};
                return "function" == typeof this._config.offset ? e.fn = function (e) {
                    return e.offsets = l({}, e.offsets, {}, t._config.offset(e.offsets, t._element) || {}), e
                } : e.offset = this._config.offset, e
            }, e._getPopperConfig = function () {
                var e = {
                    placement: this._getPlacement(),
                    modifiers: {
                        offset: this._getOffset(),
                        flip: {enabled: this._config.flip},
                        preventOverflow: {boundariesElement: this._config.boundary}
                    }
                };
                return "static" === this._config.display && (e.modifiers.applyStyle = {enabled: !1}), l({}, e, {}, this._config.popperConfig)
            }, c._jQueryInterface = function (t) {
                return this.each(function () {
                    var e = p(this).data(At);
                    if (e || (e = new c(this, "object" == typeof t ? t : null), p(this).data(At, e)), "string" == typeof t) {
                        if ("undefined" == typeof e[t]) throw new TypeError('No method named "' + t + '"');
                        e[t]()
                    }
                })
            }, c._clearMenus = function (e) {
                if (!e || 3 !== e.which && ("keyup" !== e.type || 9 === e.which)) for (var t = [].slice.call(document.querySelectorAll(Ut)), n = 0, i = t.length; n < i; n++) {
                    var o = c._getParentFromElement(t[n]), r = p(t[n]).data(At), s = {relatedTarget: t[n]};
                    if (e && "click" === e.type && (s.clickEvent = e), r) {
                        var a = r._menu;
                        if (p(o).hasClass(jt) && !(e && ("click" === e.type && /input|textarea/i.test(e.target.tagName) || "keyup" === e.type && 9 === e.which) && p.contains(o, e.target))) {
                            var l = p.Event(Pt.HIDE, s);
                            p(o).trigger(l), l.isDefaultPrevented() || ("ontouchstart" in document.documentElement && p(document.body).children().off("mouseover", null, p.noop), t[n].setAttribute("aria-expanded", "false"), r._popper && r._popper.destroy(), p(a).removeClass(jt), p(o).removeClass(jt).trigger(p.Event(Pt.HIDDEN, s)))
                        }
                    }
                }
            }, c._getParentFromElement = function (e) {
                var t, n = m.getSelectorFromElement(e);
                return n && (t = document.querySelector(n)), t || e.parentNode
            }, c._dataApiKeydownHandler = function (e) {
                if ((/input|textarea/i.test(e.target.tagName) ? !(32 === e.which || 27 !== e.which && (40 !== e.which && 38 !== e.which || p(e.target).closest(qt).length)) : Lt.test(e.which)) && (e.preventDefault(), e.stopPropagation(), !this.disabled && !p(this).hasClass(xt))) {
                    var t = c._getParentFromElement(this), n = p(t).hasClass(jt);
                    if (n || 27 !== e.which) if (n && (!n || 27 !== e.which && 32 !== e.which)) {
                        var i = [].slice.call(t.querySelectorAll(Qt)).filter(function (e) {
                            return p(e).is(":visible")
                        });
                        if (0 !== i.length) {
                            var o = i.indexOf(e.target);
                            38 === e.which && 0 < o && o--, 40 === e.which && o < i.length - 1 && o++, o < 0 && (o = 0), i[o].focus()
                        }
                    } else {
                        if (27 === e.which) {
                            var r = t.querySelector(Ut);
                            p(r).trigger("focus")
                        }
                        p(this).trigger("click")
                    }
                }
            }, s(c, null, [{
                key: "VERSION", get: function () {
                    return "4.4.1"
                }
            }, {
                key: "Default", get: function () {
                    return Jt
                }
            }, {
                key: "DefaultType", get: function () {
                    return Zt
                }
            }]), c
        }();
    p(document).on(Pt.KEYDOWN_DATA_API, Ut, en._dataApiKeydownHandler).on(Pt.KEYDOWN_DATA_API, qt, en._dataApiKeydownHandler).on(Pt.CLICK_DATA_API + " " + Pt.KEYUP_DATA_API, en._clearMenus).on(Pt.CLICK_DATA_API, Ut, function (e) {
        e.preventDefault(), e.stopPropagation(), en._jQueryInterface.call(p(this), "toggle")
    }).on(Pt.CLICK_DATA_API, Bt, function (e) {
        e.stopPropagation()
    }), p.fn[It] = en._jQueryInterface, p.fn[It].Constructor = en, p.fn[It].noConflict = function () {
        return p.fn[It] = kt, en._jQueryInterface
    };
    var tn = "modal", nn = "bs.modal", on = "." + nn, rn = p.fn[tn],
        sn = {backdrop: !0, keyboard: !0, focus: !0, show: !0},
        an = {backdrop: "(boolean|string)", keyboard: "boolean", focus: "boolean", show: "boolean"}, ln = {
            HIDE: "hide" + on,
            HIDE_PREVENTED: "hidePrevented" + on,
            HIDDEN: "hidden" + on,
            SHOW: "show" + on,
            SHOWN: "shown" + on,
            FOCUSIN: "focusin" + on,
            RESIZE: "resize" + on,
            CLICK_DISMISS: "click.dismiss" + on,
            KEYDOWN_DISMISS: "keydown.dismiss" + on,
            MOUSEUP_DISMISS: "mouseup.dismiss" + on,
            MOUSEDOWN_DISMISS: "mousedown.dismiss" + on,
            CLICK_DATA_API: "click" + on + ".data-api"
        }, cn = "modal-dialog-scrollable", hn = "modal-scrollbar-measure", un = "modal-backdrop", fn = "modal-open",
        dn = "fade", pn = "show", mn = "modal-static", gn = ".modal-dialog", _n = ".modal-body",
        vn = '[data-toggle="modal"]', yn = '[data-dismiss="modal"]',
        En = ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top", bn = ".sticky-top", wn = function () {
            function o(e, t) {
                this._config = this._getConfig(t), this._element = e, this._dialog = e.querySelector(gn), this._backdrop = null, this._isShown = !1, this._isBodyOverflowing = !1, this._ignoreBackdropClick = !1, this._isTransitioning = !1, this._scrollbarWidth = 0
            }

            var e = o.prototype;
            return e.toggle = function (e) {
                return this._isShown ? this.hide() : this.show(e)
            }, e.show = function (e) {
                var t = this;
                if (!this._isShown && !this._isTransitioning) {
                    p(this._element).hasClass(dn) && (this._isTransitioning = !0);
                    var n = p.Event(ln.SHOW, {relatedTarget: e});
                    p(this._element).trigger(n), this._isShown || n.isDefaultPrevented() || (this._isShown = !0, this._checkScrollbar(), this._setScrollbar(), this._adjustDialog(), this._setEscapeEvent(), this._setResizeEvent(), p(this._element).on(ln.CLICK_DISMISS, yn, function (e) {
                        return t.hide(e)
                    }), p(this._dialog).on(ln.MOUSEDOWN_DISMISS, function () {
                        p(t._element).one(ln.MOUSEUP_DISMISS, function (e) {
                            p(e.target).is(t._element) && (t._ignoreBackdropClick = !0)
                        })
                    }), this._showBackdrop(function () {
                        return t._showElement(e)
                    }))
                }
            }, e.hide = function (e) {
                var t = this;
                if (e && e.preventDefault(), this._isShown && !this._isTransitioning) {
                    var n = p.Event(ln.HIDE);
                    if (p(this._element).trigger(n), this._isShown && !n.isDefaultPrevented()) {
                        this._isShown = !1;
                        var i = p(this._element).hasClass(dn);
                        if (i && (this._isTransitioning = !0), this._setEscapeEvent(), this._setResizeEvent(), p(document).off(ln.FOCUSIN), p(this._element).removeClass(pn), p(this._element).off(ln.CLICK_DISMISS), p(this._dialog).off(ln.MOUSEDOWN_DISMISS), i) {
                            var o = m.getTransitionDurationFromElement(this._element);
                            p(this._element).one(m.TRANSITION_END, function (e) {
                                return t._hideModal(e)
                            }).emulateTransitionEnd(o)
                        } else this._hideModal()
                    }
                }
            }, e.dispose = function () {
                [window, this._element, this._dialog].forEach(function (e) {
                    return p(e).off(on)
                }), p(document).off(ln.FOCUSIN), p.removeData(this._element, nn), this._config = null, this._element = null, this._dialog = null, this._backdrop = null, this._isShown = null, this._isBodyOverflowing = null, this._ignoreBackdropClick = null, this._isTransitioning = null, this._scrollbarWidth = null
            }, e.handleUpdate = function () {
                this._adjustDialog()
            }, e._getConfig = function (e) {
                return e = l({}, sn, {}, e), m.typeCheckConfig(tn, e, an), e
            }, e._triggerBackdropTransition = function () {
                var e = this;
                if ("static" === this._config.backdrop) {
                    var t = p.Event(ln.HIDE_PREVENTED);
                    if (p(this._element).trigger(t), t.defaultPrevented) return;
                    this._element.classList.add(mn);
                    var n = m.getTransitionDurationFromElement(this._element);
                    p(this._element).one(m.TRANSITION_END, function () {
                        e._element.classList.remove(mn)
                    }).emulateTransitionEnd(n), this._element.focus()
                } else this.hide()
            }, e._showElement = function (e) {
                var t = this, n = p(this._element).hasClass(dn), i = this._dialog ? this._dialog.querySelector(_n) : null;
                this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE || document.body.appendChild(this._element), this._element.style.display = "block", this._element.removeAttribute("aria-hidden"), this._element.setAttribute("aria-modal", !0), p(this._dialog).hasClass(cn) && i ? i.scrollTop = 0 : this._element.scrollTop = 0, n && m.reflow(this._element), p(this._element).addClass(pn), this._config.focus && this._enforceFocus();

                function o() {
                    t._config.focus && t._element.focus(), t._isTransitioning = !1, p(t._element).trigger(r)
                }

                var r = p.Event(ln.SHOWN, {relatedTarget: e});
                if (n) {
                    var s = m.getTransitionDurationFromElement(this._dialog);
                    p(this._dialog).one(m.TRANSITION_END, o).emulateTransitionEnd(s)
                } else o()
            }, e._enforceFocus = function () {
                var t = this;
                p(document).off(ln.FOCUSIN).on(ln.FOCUSIN, function (e) {
                    document !== e.target && t._element !== e.target && 0 === p(t._element).has(e.target).length && t._element.focus()
                })
            }, e._setEscapeEvent = function () {
                var t = this;
                this._isShown && this._config.keyboard ? p(this._element).on(ln.KEYDOWN_DISMISS, function (e) {
                    27 === e.which && t._triggerBackdropTransition()
                }) : this._isShown || p(this._element).off(ln.KEYDOWN_DISMISS)
            }, e._setResizeEvent = function () {
                var t = this;
                this._isShown ? p(window).on(ln.RESIZE, function (e) {
                    return t.handleUpdate(e)
                }) : p(window).off(ln.RESIZE)
            }, e._hideModal = function () {
                var e = this;
                this._element.style.display = "none", this._element.setAttribute("aria-hidden", !0), this._element.removeAttribute("aria-modal"), this._isTransitioning = !1, this._showBackdrop(function () {
                    p(document.body).removeClass(fn), e._resetAdjustments(), e._resetScrollbar(), p(e._element).trigger(ln.HIDDEN)
                })
            }, e._removeBackdrop = function () {
                this._backdrop && (p(this._backdrop).remove(), this._backdrop = null)
            }, e._showBackdrop = function (e) {
                var t = this, n = p(this._element).hasClass(dn) ? dn : "";
                if (this._isShown && this._config.backdrop) {
                    if (this._backdrop = document.createElement("div"), this._backdrop.className = un, n && this._backdrop.classList.add(n), p(this._backdrop).appendTo(document.body), p(this._element).on(ln.CLICK_DISMISS, function (e) {
                        t._ignoreBackdropClick ? t._ignoreBackdropClick = !1 : e.target === e.currentTarget && t._triggerBackdropTransition()
                    }), n && m.reflow(this._backdrop), p(this._backdrop).addClass(pn), !e) return;
                    if (!n) return void e();
                    var i = m.getTransitionDurationFromElement(this._backdrop);
                    p(this._backdrop).one(m.TRANSITION_END, e).emulateTransitionEnd(i)
                } else if (!this._isShown && this._backdrop) {
                    p(this._backdrop).removeClass(pn);
                    var o = function () {
                        t._removeBackdrop(), e && e()
                    };
                    if (p(this._element).hasClass(dn)) {
                        var r = m.getTransitionDurationFromElement(this._backdrop);
                        p(this._backdrop).one(m.TRANSITION_END, o).emulateTransitionEnd(r)
                    } else o()
                } else e && e()
            }, e._adjustDialog = function () {
                var e = this._element.scrollHeight > document.documentElement.clientHeight;
                !this._isBodyOverflowing && e && (this._element.style.paddingLeft = this._scrollbarWidth + "px"), this._isBodyOverflowing && !e && (this._element.style.paddingRight = this._scrollbarWidth + "px")
            }, e._resetAdjustments = function () {
                this._element.style.paddingLeft = "", this._element.style.paddingRight = ""
            }, e._checkScrollbar = function () {
                var e = document.body.getBoundingClientRect();
                this._isBodyOverflowing = e.left + e.right < window.innerWidth, this._scrollbarWidth = this._getScrollbarWidth()
            }, e._setScrollbar = function () {
                var o = this;
                if (this._isBodyOverflowing) {
                    var e = [].slice.call(document.querySelectorAll(En)), t = [].slice.call(document.querySelectorAll(bn));
                    p(e).each(function (e, t) {
                        var n = t.style.paddingRight, i = p(t).css("padding-right");
                        p(t).data("padding-right", n).css("padding-right", parseFloat(i) + o._scrollbarWidth + "px")
                    }), p(t).each(function (e, t) {
                        var n = t.style.marginRight, i = p(t).css("margin-right");
                        p(t).data("margin-right", n).css("margin-right", parseFloat(i) - o._scrollbarWidth + "px")
                    });
                    var n = document.body.style.paddingRight, i = p(document.body).css("padding-right");
                    p(document.body).data("padding-right", n).css("padding-right", parseFloat(i) + this._scrollbarWidth + "px")
                }
                p(document.body).addClass(fn)
            }, e._resetScrollbar = function () {
                var e = [].slice.call(document.querySelectorAll(En));
                p(e).each(function (e, t) {
                    var n = p(t).data("padding-right");
                    p(t).removeData("padding-right"), t.style.paddingRight = n || ""
                });
                var t = [].slice.call(document.querySelectorAll("" + bn));
                p(t).each(function (e, t) {
                    var n = p(t).data("margin-right");
                    "undefined" != typeof n && p(t).css("margin-right", n).removeData("margin-right")
                });
                var n = p(document.body).data("padding-right");
                p(document.body).removeData("padding-right"), document.body.style.paddingRight = n || ""
            }, e._getScrollbarWidth = function () {
                var e = document.createElement("div");
                e.className = hn, document.body.appendChild(e);
                var t = e.getBoundingClientRect().width - e.clientWidth;
                return document.body.removeChild(e), t
            }, o._jQueryInterface = function (n, i) {
                return this.each(function () {
                    var e = p(this).data(nn), t = l({}, sn, {}, p(this).data(), {}, "object" == typeof n && n ? n : {});
                    if (e || (e = new o(this, t), p(this).data(nn, e)), "string" == typeof n) {
                        if ("undefined" == typeof e[n]) throw new TypeError('No method named "' + n + '"');
                        e[n](i)
                    } else t.show && e.show(i)
                })
            }, s(o, null, [{
                key: "VERSION", get: function () {
                    return "4.4.1"
                }
            }, {
                key: "Default", get: function () {
                    return sn
                }
            }]), o
        }();
    p(document).on(ln.CLICK_DATA_API, vn, function (e) {
        var t, n = this, i = m.getSelectorFromElement(this);
        i && (t = document.querySelector(i));
        var o = p(t).data(nn) ? "toggle" : l({}, p(t).data(), {}, p(this).data());
        "A" !== this.tagName && "AREA" !== this.tagName || e.preventDefault();
        var r = p(t).one(ln.SHOW, function (e) {
            e.isDefaultPrevented() || r.one(ln.HIDDEN, function () {
                p(n).is(":visible") && n.focus()
            })
        });
        wn._jQueryInterface.call(p(t), o, this)
    }), p.fn[tn] = wn._jQueryInterface, p.fn[tn].Constructor = wn, p.fn[tn].noConflict = function () {
        return p.fn[tn] = rn, wn._jQueryInterface
    };
    var Tn = ["background", "cite", "href", "itemtype", "longdesc", "poster", "src", "xlink:href"], Cn = {
            "*": ["class", "dir", "id", "lang", "role", /^aria-[\w-]*$/i],
            a: ["target", "href", "title", "rel"],
            area: [],
            b: [],
            br: [],
            col: [],
            code: [],
            div: [],
            em: [],
            hr: [],
            h1: [],
            h2: [],
            h3: [],
            h4: [],
            h5: [],
            h6: [],
            i: [],
            img: ["src", "alt", "title", "width", "height"],
            li: [],
            ol: [],
            p: [],
            pre: [],
            s: [],
            small: [],
            span: [],
            sub: [],
            sup: [],
            strong: [],
            u: [],
            ul: []
        }, Sn = /^(?:(?:https?|mailto|ftp|tel|file):|[^&:/?#]*(?:[/?#]|$))/gi,
        Dn = /^data:(?:image\/(?:bmp|gif|jpeg|jpg|png|tiff|webp)|video\/(?:mpeg|mp4|ogg|webm)|audio\/(?:mp3|oga|ogg|opus));base64,[a-z0-9+/]+=*$/i;

    function In(e, r, t) {
        if (0 === e.length) return e;
        if (t && "function" == typeof t) return t(e);
        for (var n = (new window.DOMParser).parseFromString(e, "text/html"), s = Object.keys(r), a = [].slice.call(n.body.querySelectorAll("*")), i = function (e) {
            var t = a[e], n = t.nodeName.toLowerCase();
            if (-1 === s.indexOf(t.nodeName.toLowerCase())) return t.parentNode.removeChild(t), "continue";
            var i = [].slice.call(t.attributes), o = [].concat(r["*"] || [], r[n] || []);
            i.forEach(function (e) {
                !function (e, t) {
                    var n = e.nodeName.toLowerCase();
                    if (-1 !== t.indexOf(n)) return -1 === Tn.indexOf(n) || Boolean(e.nodeValue.match(Sn) || e.nodeValue.match(Dn));
                    for (var i = t.filter(function (e) {
                        return e instanceof RegExp
                    }), o = 0, r = i.length; o < r; o++) if (n.match(i[o])) return !0;
                    return !1
                }(e, o) && t.removeAttribute(e.nodeName)
            })
        }, o = 0, l = a.length; o < l; o++) i(o);
        return n.body.innerHTML
    }

    var An = "tooltip", On = "bs.tooltip", Nn = "." + On, kn = p.fn[An], Ln = "bs-tooltip",
        Pn = new RegExp("(^|\\s)" + Ln + "\\S+", "g"), xn = ["sanitize", "whiteList", "sanitizeFn"], jn = {
            animation: "boolean",
            template: "string",
            title: "(string|element|function)",
            trigger: "string",
            delay: "(number|object)",
            html: "boolean",
            selector: "(string|boolean)",
            placement: "(string|function)",
            offset: "(number|string|function)",
            container: "(string|element|boolean)",
            fallbackPlacement: "(string|array)",
            boundary: "(string|element)",
            sanitize: "boolean",
            sanitizeFn: "(null|function)",
            whiteList: "object",
            popperConfig: "(null|object)"
        }, Hn = {AUTO: "auto", TOP: "top", RIGHT: "right", BOTTOM: "bottom", LEFT: "left"}, Rn = {
            animation: !0,
            template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
            trigger: "hover focus",
            title: "",
            delay: 0,
            html: !1,
            selector: !1,
            placement: "top",
            offset: 0,
            container: !1,
            fallbackPlacement: "flip",
            boundary: "scrollParent",
            sanitize: !0,
            sanitizeFn: null,
            whiteList: Cn,
            popperConfig: null
        }, Fn = "show", Mn = "out", Wn = {
            HIDE: "hide" + Nn,
            HIDDEN: "hidden" + Nn,
            SHOW: "show" + Nn,
            SHOWN: "shown" + Nn,
            INSERTED: "inserted" + Nn,
            CLICK: "click" + Nn,
            FOCUSIN: "focusin" + Nn,
            FOCUSOUT: "focusout" + Nn,
            MOUSEENTER: "mouseenter" + Nn,
            MOUSELEAVE: "mouseleave" + Nn
        }, Un = "fade", Bn = "show", qn = ".tooltip-inner", Kn = ".arrow", Qn = "hover", Vn = "focus", Yn = "click",
        zn = "manual", Xn = function () {
            function i(e, t) {
                if ("undefined" == typeof St) throw new TypeError("Bootstrap's tooltips require Popper.js (https://popper.js.org/)");
                this._isEnabled = !0, this._timeout = 0, this._hoverState = "", this._activeTrigger = {}, this._popper = null, this.element = e, this.config = this._getConfig(t), this.tip = null, this._setListeners()
            }

            var e = i.prototype;
            return e.enable = function () {
                this._isEnabled = !0
            }, e.disable = function () {
                this._isEnabled = !1
            }, e.toggleEnabled = function () {
                this._isEnabled = !this._isEnabled
            }, e.toggle = function (e) {
                if (this._isEnabled) if (e) {
                    var t = this.constructor.DATA_KEY, n = p(e.currentTarget).data(t);
                    n || (n = new this.constructor(e.currentTarget, this._getDelegateConfig()), p(e.currentTarget).data(t, n)), n._activeTrigger.click = !n._activeTrigger.click, n._isWithActiveTrigger() ? n._enter(null, n) : n._leave(null, n)
                } else {
                    if (p(this.getTipElement()).hasClass(Bn)) return void this._leave(null, this);
                    this._enter(null, this)
                }
            }, e.dispose = function () {
                clearTimeout(this._timeout), p.removeData(this.element, this.constructor.DATA_KEY), p(this.element).off(this.constructor.EVENT_KEY), p(this.element).closest(".modal").off("hide.bs.modal", this._hideModalHandler), this.tip && p(this.tip).remove(), this._isEnabled = null, this._timeout = null, this._hoverState = null, this._activeTrigger = null, this._popper && this._popper.destroy(), this._popper = null, this.element = null, this.config = null, this.tip = null
            }, e.show = function () {
                var t = this;
                if ("none" === p(this.element).css("display")) throw new Error("Please use show on visible elements");
                var e = p.Event(this.constructor.Event.SHOW);
                if (this.isWithContent() && this._isEnabled) {
                    p(this.element).trigger(e);
                    var n = m.findShadowRoot(this.element),
                        i = p.contains(null !== n ? n : this.element.ownerDocument.documentElement, this.element);
                    if (e.isDefaultPrevented() || !i) return;
                    var o = this.getTipElement(), r = m.getUID(this.constructor.NAME);
                    o.setAttribute("id", r), this.element.setAttribute("aria-describedby", r), this.setContent(), this.config.animation && p(o).addClass(Un);
                    var s = "function" == typeof this.config.placement ? this.config.placement.call(this, o, this.element) : this.config.placement,
                        a = this._getAttachment(s);
                    this.addAttachmentClass(a);
                    var l = this._getContainer();
                    p(o).data(this.constructor.DATA_KEY, this), p.contains(this.element.ownerDocument.documentElement, this.tip) || p(o).appendTo(l), p(this.element).trigger(this.constructor.Event.INSERTED), this._popper = new St(this.element, o, this._getPopperConfig(a)), p(o).addClass(Bn), "ontouchstart" in document.documentElement && p(document.body).children().on("mouseover", null, p.noop);
                    var c = function () {
                        t.config.animation && t._fixTransition();
                        var e = t._hoverState;
                        t._hoverState = null, p(t.element).trigger(t.constructor.Event.SHOWN), e === Mn && t._leave(null, t)
                    };
                    if (p(this.tip).hasClass(Un)) {
                        var h = m.getTransitionDurationFromElement(this.tip);
                        p(this.tip).one(m.TRANSITION_END, c).emulateTransitionEnd(h)
                    } else c()
                }
            }, e.hide = function (e) {
                function t() {
                    n._hoverState !== Fn && i.parentNode && i.parentNode.removeChild(i), n._cleanTipClass(), n.element.removeAttribute("aria-describedby"), p(n.element).trigger(n.constructor.Event.HIDDEN), null !== n._popper && n._popper.destroy(), e && e()
                }

                var n = this, i = this.getTipElement(), o = p.Event(this.constructor.Event.HIDE);
                if (p(this.element).trigger(o), !o.isDefaultPrevented()) {
                    if (p(i).removeClass(Bn), "ontouchstart" in document.documentElement && p(document.body).children().off("mouseover", null, p.noop), this._activeTrigger[Yn] = !1, this._activeTrigger[Vn] = !1, this._activeTrigger[Qn] = !1, p(this.tip).hasClass(Un)) {
                        var r = m.getTransitionDurationFromElement(i);
                        p(i).one(m.TRANSITION_END, t).emulateTransitionEnd(r)
                    } else t();
                    this._hoverState = ""
                }
            }, e.update = function () {
                null !== this._popper && this._popper.scheduleUpdate()
            }, e.isWithContent = function () {
                return Boolean(this.getTitle())
            }, e.addAttachmentClass = function (e) {
                p(this.getTipElement()).addClass(Ln + "-" + e)
            }, e.getTipElement = function () {
                return this.tip = this.tip || p(this.config.template)[0], this.tip
            }, e.setContent = function () {
                var e = this.getTipElement();
                this.setElementContent(p(e.querySelectorAll(qn)), this.getTitle()), p(e).removeClass(Un + " " + Bn)
            }, e.setElementContent = function (e, t) {
                "object" != typeof t || !t.nodeType && !t.jquery ? this.config.html ? (this.config.sanitize && (t = In(t, this.config.whiteList, this.config.sanitizeFn)), e.html(t)) : e.text(t) : this.config.html ? p(t).parent().is(e) || e.empty().append(t) : e.text(p(t).text())
            }, e.getTitle = function () {
                var e = this.element.getAttribute("data-original-title");
                return e = e || ("function" == typeof this.config.title ? this.config.title.call(this.element) : this.config.title)
            }, e._getPopperConfig = function (e) {
                var t = this;
                return l({}, {
                    placement: e,
                    modifiers: {
                        offset: this._getOffset(),
                        flip: {behavior: this.config.fallbackPlacement},
                        arrow: {element: Kn},
                        preventOverflow: {boundariesElement: this.config.boundary}
                    },
                    onCreate: function (e) {
                        e.originalPlacement !== e.placement && t._handlePopperPlacementChange(e)
                    },
                    onUpdate: function (e) {
                        return t._handlePopperPlacementChange(e)
                    }
                }, {}, this.config.popperConfig)
            }, e._getOffset = function () {
                var t = this, e = {};
                return "function" == typeof this.config.offset ? e.fn = function (e) {
                    return e.offsets = l({}, e.offsets, {}, t.config.offset(e.offsets, t.element) || {}), e
                } : e.offset = this.config.offset, e
            }, e._getContainer = function () {
                return !1 === this.config.container ? document.body : m.isElement(this.config.container) ? p(this.config.container) : p(document).find(this.config.container)
            }, e._getAttachment = function (e) {
                return Hn[e.toUpperCase()]
            }, e._setListeners = function () {
                var i = this;
                this.config.trigger.split(" ").forEach(function (e) {
                    if ("click" === e) p(i.element).on(i.constructor.Event.CLICK, i.config.selector, function (e) {
                        return i.toggle(e)
                    }); else if (e !== zn) {
                        var t = e === Qn ? i.constructor.Event.MOUSEENTER : i.constructor.Event.FOCUSIN,
                            n = e === Qn ? i.constructor.Event.MOUSELEAVE : i.constructor.Event.FOCUSOUT;
                        p(i.element).on(t, i.config.selector, function (e) {
                            return i._enter(e)
                        }).on(n, i.config.selector, function (e) {
                            return i._leave(e)
                        })
                    }
                }), this._hideModalHandler = function () {
                    i.element && i.hide()
                }, p(this.element).closest(".modal").on("hide.bs.modal", this._hideModalHandler), this.config.selector ? this.config = l({}, this.config, {
                    trigger: "manual",
                    selector: ""
                }) : this._fixTitle()
            }, e._fixTitle = function () {
                var e = typeof this.element.getAttribute("data-original-title");
                !this.element.getAttribute("title") && "string" == e || (this.element.setAttribute("data-original-title", this.element.getAttribute("title") || ""), this.element.setAttribute("title", ""))
            }, e._enter = function (e, t) {
                var n = this.constructor.DATA_KEY;
                (t = t || p(e.currentTarget).data(n)) || (t = new this.constructor(e.currentTarget, this._getDelegateConfig()), p(e.currentTarget).data(n, t)), e && (t._activeTrigger["focusin" === e.type ? Vn : Qn] = !0), p(t.getTipElement()).hasClass(Bn) || t._hoverState === Fn ? t._hoverState = Fn : (clearTimeout(t._timeout), t._hoverState = Fn, t.config.delay && t.config.delay.show ? t._timeout = setTimeout(function () {
                    t._hoverState === Fn && t.show()
                }, t.config.delay.show) : t.show())
            }, e._leave = function (e, t) {
                var n = this.constructor.DATA_KEY;
                (t = t || p(e.currentTarget).data(n)) || (t = new this.constructor(e.currentTarget, this._getDelegateConfig()), p(e.currentTarget).data(n, t)), e && (t._activeTrigger["focusout" === e.type ? Vn : Qn] = !1), t._isWithActiveTrigger() || (clearTimeout(t._timeout), t._hoverState = Mn, t.config.delay && t.config.delay.hide ? t._timeout = setTimeout(function () {
                    t._hoverState === Mn && t.hide()
                }, t.config.delay.hide) : t.hide())
            }, e._isWithActiveTrigger = function () {
                for (var e in this._activeTrigger) if (this._activeTrigger[e]) return !0;
                return !1
            }, e._getConfig = function (e) {
                var t = p(this.element).data();
                return Object.keys(t).forEach(function (e) {
                    -1 !== xn.indexOf(e) && delete t[e]
                }), "number" == typeof (e = l({}, this.constructor.Default, {}, t, {}, "object" == typeof e && e ? e : {})).delay && (e.delay = {
                    show: e.delay,
                    hide: e.delay
                }), "number" == typeof e.title && (e.title = e.title.toString()), "number" == typeof e.content && (e.content = e.content.toString()), m.typeCheckConfig(An, e, this.constructor.DefaultType), e.sanitize && (e.template = In(e.template, e.whiteList, e.sanitizeFn)), e
            }, e._getDelegateConfig = function () {
                var e = {};
                if (this.config) for (var t in this.config) this.constructor.Default[t] !== this.config[t] && (e[t] = this.config[t]);
                return e
            }, e._cleanTipClass = function () {
                var e = p(this.getTipElement()), t = e.attr("class").match(Pn);
                null !== t && t.length && e.removeClass(t.join(""))
            }, e._handlePopperPlacementChange = function (e) {
                var t = e.instance;
                this.tip = t.popper, this._cleanTipClass(), this.addAttachmentClass(this._getAttachment(e.placement))
            }, e._fixTransition = function () {
                var e = this.getTipElement(), t = this.config.animation;
                null === e.getAttribute("x-placement") && (p(e).removeClass(Un), this.config.animation = !1, this.hide(), this.show(), this.config.animation = t)
            }, i._jQueryInterface = function (n) {
                return this.each(function () {
                    var e = p(this).data(On), t = "object" == typeof n && n;
                    if ((e || !/dispose|hide/.test(n)) && (e || (e = new i(this, t), p(this).data(On, e)), "string" == typeof n)) {
                        if ("undefined" == typeof e[n]) throw new TypeError('No method named "' + n + '"');
                        e[n]()
                    }
                })
            }, s(i, null, [{
                key: "VERSION", get: function () {
                    return "4.4.1"
                }
            }, {
                key: "Default", get: function () {
                    return Rn
                }
            }, {
                key: "NAME", get: function () {
                    return An
                }
            }, {
                key: "DATA_KEY", get: function () {
                    return On
                }
            }, {
                key: "Event", get: function () {
                    return Wn
                }
            }, {
                key: "EVENT_KEY", get: function () {
                    return Nn
                }
            }, {
                key: "DefaultType", get: function () {
                    return jn
                }
            }]), i
        }();
    p.fn[An] = Xn._jQueryInterface, p.fn[An].Constructor = Xn, p.fn[An].noConflict = function () {
        return p.fn[An] = kn, Xn._jQueryInterface
    };
    var Gn = "popover", $n = "bs.popover", Jn = "." + $n, Zn = p.fn[Gn], ei = "bs-popover",
        ti = new RegExp("(^|\\s)" + ei + "\\S+", "g"), ni = l({}, Xn.Default, {
            placement: "right",
            trigger: "click",
            content: "",
            template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
        }), ii = l({}, Xn.DefaultType, {content: "(string|element|function)"}), oi = "fade", ri = "show",
        si = ".popover-header", ai = ".popover-body", li = {
            HIDE: "hide" + Jn,
            HIDDEN: "hidden" + Jn,
            SHOW: "show" + Jn,
            SHOWN: "shown" + Jn,
            INSERTED: "inserted" + Jn,
            CLICK: "click" + Jn,
            FOCUSIN: "focusin" + Jn,
            FOCUSOUT: "focusout" + Jn,
            MOUSEENTER: "mouseenter" + Jn,
            MOUSELEAVE: "mouseleave" + Jn
        }, ci = function (e) {
            function i() {
                return e.apply(this, arguments) || this
            }

            !function (e, t) {
                e.prototype = Object.create(t.prototype), (e.prototype.constructor = e).__proto__ = t
            }(i, e);
            var t = i.prototype;
            return t.isWithContent = function () {
                return this.getTitle() || this._getContent()
            }, t.addAttachmentClass = function (e) {
                p(this.getTipElement()).addClass(ei + "-" + e)
            }, t.getTipElement = function () {
                return this.tip = this.tip || p(this.config.template)[0], this.tip
            }, t.setContent = function () {
                var e = p(this.getTipElement());
                this.setElementContent(e.find(si), this.getTitle());
                var t = this._getContent();
                "function" == typeof t && (t = t.call(this.element)), this.setElementContent(e.find(ai), t), e.removeClass(oi + " " + ri)
            }, t._getContent = function () {
                return this.element.getAttribute("data-content") || this.config.content
            }, t._cleanTipClass = function () {
                var e = p(this.getTipElement()), t = e.attr("class").match(ti);
                null !== t && 0 < t.length && e.removeClass(t.join(""))
            }, i._jQueryInterface = function (n) {
                return this.each(function () {
                    var e = p(this).data($n), t = "object" == typeof n ? n : null;
                    if ((e || !/dispose|hide/.test(n)) && (e || (e = new i(this, t), p(this).data($n, e)), "string" == typeof n)) {
                        if ("undefined" == typeof e[n]) throw new TypeError('No method named "' + n + '"');
                        e[n]()
                    }
                })
            }, s(i, null, [{
                key: "VERSION", get: function () {
                    return "4.4.1"
                }
            }, {
                key: "Default", get: function () {
                    return ni
                }
            }, {
                key: "NAME", get: function () {
                    return Gn
                }
            }, {
                key: "DATA_KEY", get: function () {
                    return $n
                }
            }, {
                key: "Event", get: function () {
                    return li
                }
            }, {
                key: "EVENT_KEY", get: function () {
                    return Jn
                }
            }, {
                key: "DefaultType", get: function () {
                    return ii
                }
            }]), i
        }(Xn);
    p.fn[Gn] = ci._jQueryInterface, p.fn[Gn].Constructor = ci, p.fn[Gn].noConflict = function () {
        return p.fn[Gn] = Zn, ci._jQueryInterface
    };
    var hi = "scrollspy", ui = "bs.scrollspy", fi = "." + ui, di = p.fn[hi],
        pi = {offset: 10, method: "auto", target: ""},
        mi = {offset: "number", method: "string", target: "(string|element)"},
        gi = {ACTIVATE: "activate" + fi, SCROLL: "scroll" + fi, LOAD_DATA_API: "load" + fi + ".data-api"},
        _i = "dropdown-item", vi = "active", yi = '[data-spy="scroll"]', Ei = ".nav, .list-group", bi = ".nav-link",
        wi = ".nav-item", Ti = ".list-group-item", Ci = ".dropdown", Si = ".dropdown-item", Di = ".dropdown-toggle",
        Ii = "offset", Ai = "position", Oi = function () {
            function n(e, t) {
                var n = this;
                this._element = e, this._scrollElement = "BODY" === e.tagName ? window : e, this._config = this._getConfig(t), this._selector = this._config.target + " " + bi + "," + this._config.target + " " + Ti + "," + this._config.target + " " + Si, this._offsets = [], this._targets = [], this._activeTarget = null, this._scrollHeight = 0, p(this._scrollElement).on(gi.SCROLL, function (e) {
                    return n._process(e)
                }), this.refresh(), this._process()
            }

            var e = n.prototype;
            return e.refresh = function () {
                var t = this, e = this._scrollElement === this._scrollElement.window ? Ii : Ai,
                    o = "auto" === this._config.method ? e : this._config.method, r = o === Ai ? this._getScrollTop() : 0;
                this._offsets = [], this._targets = [], this._scrollHeight = this._getScrollHeight(), [].slice.call(document.querySelectorAll(this._selector)).map(function (e) {
                    var t, n = m.getSelectorFromElement(e);
                    if (n && (t = document.querySelector(n)), t) {
                        var i = t.getBoundingClientRect();
                        if (i.width || i.height) return [p(t)[o]().top + r, n]
                    }
                    return null
                }).filter(function (e) {
                    return e
                }).sort(function (e, t) {
                    return e[0] - t[0]
                }).forEach(function (e) {
                    t._offsets.push(e[0]), t._targets.push(e[1])
                })
            }, e.dispose = function () {
                p.removeData(this._element, ui), p(this._scrollElement).off(fi), this._element = null, this._scrollElement = null, this._config = null, this._selector = null, this._offsets = null, this._targets = null, this._activeTarget = null, this._scrollHeight = null
            }, e._getConfig = function (e) {
                if ("string" != typeof (e = l({}, pi, {}, "object" == typeof e && e ? e : {})).target) {
                    var t = p(e.target).attr("id");
                    t || (t = m.getUID(hi), p(e.target).attr("id", t)), e.target = "#" + t
                }
                return m.typeCheckConfig(hi, e, mi), e
            }, e._getScrollTop = function () {
                return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop
            }, e._getScrollHeight = function () {
                return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight)
            }, e._getOffsetHeight = function () {
                return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height
            }, e._process = function () {
                var e = this._getScrollTop() + this._config.offset, t = this._getScrollHeight(),
                    n = this._config.offset + t - this._getOffsetHeight();
                if (this._scrollHeight !== t && this.refresh(), n <= e) {
                    var i = this._targets[this._targets.length - 1];
                    this._activeTarget !== i && this._activate(i)
                } else {
                    if (this._activeTarget && e < this._offsets[0] && 0 < this._offsets[0]) return this._activeTarget = null, void this._clear();
                    for (var o = this._offsets.length; o--;) {
                        this._activeTarget !== this._targets[o] && e >= this._offsets[o] && ("undefined" == typeof this._offsets[o + 1] || e < this._offsets[o + 1]) && this._activate(this._targets[o])
                    }
                }
            }, e._activate = function (t) {
                this._activeTarget = t, this._clear();
                var e = this._selector.split(",").map(function (e) {
                    return e + '[data-target="' + t + '"],' + e + '[href="' + t + '"]'
                }), n = p([].slice.call(document.querySelectorAll(e.join(","))));
                n.hasClass(_i) ? (n.closest(Ci).find(Di).addClass(vi), n.addClass(vi)) : (n.addClass(vi), n.parents(Ei).prev(bi + ", " + Ti).addClass(vi), n.parents(Ei).prev(wi).children(bi).addClass(vi)), p(this._scrollElement).trigger(gi.ACTIVATE, {relatedTarget: t})
            }, e._clear = function () {
                [].slice.call(document.querySelectorAll(this._selector)).filter(function (e) {
                    return e.classList.contains(vi)
                }).forEach(function (e) {
                    return e.classList.remove(vi)
                })
            }, n._jQueryInterface = function (t) {
                return this.each(function () {
                    var e = p(this).data(ui);
                    if (e || (e = new n(this, "object" == typeof t && t), p(this).data(ui, e)), "string" == typeof t) {
                        if ("undefined" == typeof e[t]) throw new TypeError('No method named "' + t + '"');
                        e[t]()
                    }
                })
            }, s(n, null, [{
                key: "VERSION", get: function () {
                    return "4.4.1"
                }
            }, {
                key: "Default", get: function () {
                    return pi
                }
            }]), n
        }();
    p(window).on(gi.LOAD_DATA_API, function () {
        for (var e = [].slice.call(document.querySelectorAll(yi)), t = e.length; t--;) {
            var n = p(e[t]);
            Oi._jQueryInterface.call(n, n.data())
        }
    }), p.fn[hi] = Oi._jQueryInterface, p.fn[hi].Constructor = Oi, p.fn[hi].noConflict = function () {
        return p.fn[hi] = di, Oi._jQueryInterface
    };
    var Ni = "bs.tab", ki = "." + Ni, Li = p.fn.tab, Pi = {
            HIDE: "hide" + ki,
            HIDDEN: "hidden" + ki,
            SHOW: "show" + ki,
            SHOWN: "shown" + ki,
            CLICK_DATA_API: "click" + ki + ".data-api"
        }, xi = "dropdown-menu", ji = "active", Hi = "disabled", Ri = "fade", Fi = "show", Mi = ".dropdown",
        Wi = ".nav, .list-group", Ui = ".active", Bi = "> li > .active",
        qi = '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]', Ki = ".dropdown-toggle",
        Qi = "> .dropdown-menu .active", Vi = function () {
            function i(e) {
                this._element = e
            }

            var e = i.prototype;
            return e.show = function () {
                var n = this;
                if (!(this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && p(this._element).hasClass(ji) || p(this._element).hasClass(Hi))) {
                    var e, i, t = p(this._element).closest(Wi)[0], o = m.getSelectorFromElement(this._element);
                    if (t) {
                        var r = "UL" === t.nodeName || "OL" === t.nodeName ? Bi : Ui;
                        i = (i = p.makeArray(p(t).find(r)))[i.length - 1]
                    }
                    var s = p.Event(Pi.HIDE, {relatedTarget: this._element}), a = p.Event(Pi.SHOW, {relatedTarget: i});
                    if (i && p(i).trigger(s), p(this._element).trigger(a), !a.isDefaultPrevented() && !s.isDefaultPrevented()) {
                        o && (e = document.querySelector(o)), this._activate(this._element, t);
                        var l = function () {
                            var e = p.Event(Pi.HIDDEN, {relatedTarget: n._element}),
                                t = p.Event(Pi.SHOWN, {relatedTarget: i});
                            p(i).trigger(e), p(n._element).trigger(t)
                        };
                        e ? this._activate(e, e.parentNode, l) : l()
                    }
                }
            }, e.dispose = function () {
                p.removeData(this._element, Ni), this._element = null
            }, e._activate = function (e, t, n) {
                function i() {
                    return o._transitionComplete(e, r, n)
                }

                var o = this, r = (!t || "UL" !== t.nodeName && "OL" !== t.nodeName ? p(t).children(Ui) : p(t).find(Bi))[0],
                    s = n && r && p(r).hasClass(Ri);
                if (r && s) {
                    var a = m.getTransitionDurationFromElement(r);
                    p(r).removeClass(Fi).one(m.TRANSITION_END, i).emulateTransitionEnd(a)
                } else i()
            }, e._transitionComplete = function (e, t, n) {
                if (t) {
                    p(t).removeClass(ji);
                    var i = p(t.parentNode).find(Qi)[0];
                    i && p(i).removeClass(ji), "tab" === t.getAttribute("role") && t.setAttribute("aria-selected", !1)
                }
                if (p(e).addClass(ji), "tab" === e.getAttribute("role") && e.setAttribute("aria-selected", !0), m.reflow(e), e.classList.contains(Ri) && e.classList.add(Fi), e.parentNode && p(e.parentNode).hasClass(xi)) {
                    var o = p(e).closest(Mi)[0];
                    if (o) {
                        var r = [].slice.call(o.querySelectorAll(Ki));
                        p(r).addClass(ji)
                    }
                    e.setAttribute("aria-expanded", !0)
                }
                n && n()
            }, i._jQueryInterface = function (n) {
                return this.each(function () {
                    var e = p(this), t = e.data(Ni);
                    if (t || (t = new i(this), e.data(Ni, t)), "string" == typeof n) {
                        if ("undefined" == typeof t[n]) throw new TypeError('No method named "' + n + '"');
                        t[n]()
                    }
                })
            }, s(i, null, [{
                key: "VERSION", get: function () {
                    return "4.4.1"
                }
            }]), i
        }();
    p(document).on(Pi.CLICK_DATA_API, qi, function (e) {
        e.preventDefault(), Vi._jQueryInterface.call(p(this), "show")
    }), p.fn.tab = Vi._jQueryInterface, p.fn.tab.Constructor = Vi, p.fn.tab.noConflict = function () {
        return p.fn.tab = Li, Vi._jQueryInterface
    };
    var Yi = "toast", zi = "bs.toast", Xi = "." + zi, Gi = p.fn[Yi], $i = {
            CLICK_DISMISS: "click.dismiss" + Xi,
            HIDE: "hide" + Xi,
            HIDDEN: "hidden" + Xi,
            SHOW: "show" + Xi,
            SHOWN: "shown" + Xi
        }, Ji = "fade", Zi = "hide", eo = "show", to = "showing",
        no = {animation: "boolean", autohide: "boolean", delay: "number"},
        io = {animation: !0, autohide: !0, delay: 500}, oo = '[data-dismiss="toast"]', ro = function () {
            function i(e, t) {
                this._element = e, this._config = this._getConfig(t), this._timeout = null, this._setListeners()
            }

            var e = i.prototype;
            return e.show = function () {
                var e = this, t = p.Event($i.SHOW);
                if (p(this._element).trigger(t), !t.isDefaultPrevented()) {
                    this._config.animation && this._element.classList.add(Ji);
                    var n = function () {
                        e._element.classList.remove(to), e._element.classList.add(eo), p(e._element).trigger($i.SHOWN), e._config.autohide && (e._timeout = setTimeout(function () {
                            e.hide()
                        }, e._config.delay))
                    };
                    if (this._element.classList.remove(Zi), m.reflow(this._element), this._element.classList.add(to), this._config.animation) {
                        var i = m.getTransitionDurationFromElement(this._element);
                        p(this._element).one(m.TRANSITION_END, n).emulateTransitionEnd(i)
                    } else n()
                }
            }, e.hide = function () {
                if (this._element.classList.contains(eo)) {
                    var e = p.Event($i.HIDE);
                    p(this._element).trigger(e), e.isDefaultPrevented() || this._close()
                }
            }, e.dispose = function () {
                clearTimeout(this._timeout), this._timeout = null, this._element.classList.contains(eo) && this._element.classList.remove(eo), p(this._element).off($i.CLICK_DISMISS), p.removeData(this._element, zi), this._element = null, this._config = null
            }, e._getConfig = function (e) {
                return e = l({}, io, {}, p(this._element).data(), {}, "object" == typeof e && e ? e : {}), m.typeCheckConfig(Yi, e, this.constructor.DefaultType), e
            }, e._setListeners = function () {
                var e = this;
                p(this._element).on($i.CLICK_DISMISS, oo, function () {
                    return e.hide()
                })
            }, e._close = function () {
                function e() {
                    t._element.classList.add(Zi), p(t._element).trigger($i.HIDDEN)
                }

                var t = this;
                if (this._element.classList.remove(eo), this._config.animation) {
                    var n = m.getTransitionDurationFromElement(this._element);
                    p(this._element).one(m.TRANSITION_END, e).emulateTransitionEnd(n)
                } else e()
            }, i._jQueryInterface = function (n) {
                return this.each(function () {
                    var e = p(this), t = e.data(zi);
                    if (t || (t = new i(this, "object" == typeof n && n), e.data(zi, t)), "string" == typeof n) {
                        if ("undefined" == typeof t[n]) throw new TypeError('No method named "' + n + '"');
                        t[n](this)
                    }
                })
            }, s(i, null, [{
                key: "VERSION", get: function () {
                    return "4.4.1"
                }
            }, {
                key: "DefaultType", get: function () {
                    return no
                }
            }, {
                key: "Default", get: function () {
                    return io
                }
            }]), i
        }();
    p.fn[Yi] = ro._jQueryInterface, p.fn[Yi].Constructor = ro, p.fn[Yi].noConflict = function () {
        return p.fn[Yi] = Gi, ro._jQueryInterface
    }, e.Alert = _, e.Button = x, e.Carousel = he, e.Collapse = De, e.Dropdown = en, e.Modal = wn, e.Popover = ci, e.Scrollspy = Oi, e.Tab = Vi, e.Toast = ro, e.Tooltip = Xn, e.Util = m, Object.defineProperty(e, "__esModule", {value: !0})
});/*! jQuery Validation Plugin - v1.19.1 - 6/15/2019
 * https://jqueryvalidation.org/
 * Copyright (c) 2019 Jörn Zaefferer; Licensed MIT */
!function (a) {
    "function" == typeof define && define.amd ? define(["jquery"], a) : "object" == typeof module && module.exports ? module.exports = a(require("jquery")) : a(jQuery)
}(function (a) {
    a.extend(a.fn, {
        validate: function (b) {
            if (!this.length) return void (b && b.debug && window.console && console.warn("Nothing selected, can't validate, returning nothing."));
            var c = a.data(this[0], "validator");
            return c ? c : (this.attr("novalidate", "novalidate"), c = new a.validator(b, this[0]), a.data(this[0], "validator", c), c.settings.onsubmit && (this.on("click.validate", ":submit", function (b) {
                c.submitButton = b.currentTarget, a(this).hasClass("cancel") && (c.cancelSubmit = !0), void 0 !== a(this).attr("formnovalidate") && (c.cancelSubmit = !0)
            }), this.on("submit.validate", function (b) {
                function d() {
                    var d, e;
                    return c.submitButton && (c.settings.submitHandler || c.formSubmitted) && (d = a("<input type='hidden'/>").attr("name", c.submitButton.name).val(a(c.submitButton).val()).appendTo(c.currentForm)), !(c.settings.submitHandler && !c.settings.debug) || (e = c.settings.submitHandler.call(c, c.currentForm, b), d && d.remove(), void 0 !== e && e)
                }

                return c.settings.debug && b.preventDefault(), c.cancelSubmit ? (c.cancelSubmit = !1, d()) : c.form() ? c.pendingRequest ? (c.formSubmitted = !0, !1) : d() : (c.focusInvalid(), !1)
            })), c)
        }, valid: function () {
            var b, c, d;
            return a(this[0]).is("form") ? b = this.validate().form() : (d = [], b = !0, c = a(this[0].form).validate(), this.each(function () {
                b = c.element(this) && b, b || (d = d.concat(c.errorList))
            }), c.errorList = d), b
        }, rules: function (b, c) {
            var d, e, f, g, h, i, j = this[0],
                k = "undefined" != typeof this.attr("contenteditable") && "false" !== this.attr("contenteditable");
            if (null != j && (!j.form && k && (j.form = this.closest("form")[0], j.name = this.attr("name")), null != j.form)) {
                if (b) switch (d = a.data(j.form, "validator").settings, e = d.rules, f = a.validator.staticRules(j), b) {
                    case"add":
                        a.extend(f, a.validator.normalizeRule(c)), delete f.messages, e[j.name] = f, c.messages && (d.messages[j.name] = a.extend(d.messages[j.name], c.messages));
                        break;
                    case"remove":
                        return c ? (i = {}, a.each(c.split(/\s/), function (a, b) {
                            i[b] = f[b], delete f[b]
                        }), i) : (delete e[j.name], f)
                }
                return g = a.validator.normalizeRules(a.extend({}, a.validator.classRules(j), a.validator.attributeRules(j), a.validator.dataRules(j), a.validator.staticRules(j)), j), g.required && (h = g.required, delete g.required, g = a.extend({required: h}, g)), g.remote && (h = g.remote, delete g.remote, g = a.extend(g, {remote: h})), g
            }
        }
    }), a.extend(a.expr.pseudos || a.expr[":"], {
        blank: function (b) {
            return !a.trim("" + a(b).val())
        }, filled: function (b) {
            var c = a(b).val();
            return null !== c && !!a.trim("" + c)
        }, unchecked: function (b) {
            return !a(b).prop("checked")
        }
    }), a.validator = function (b, c) {
        this.settings = a.extend(!0, {}, a.validator.defaults, b), this.currentForm = c, this.init()
    }, a.validator.format = function (b, c) {
        return 1 === arguments.length ? function () {
            var c = a.makeArray(arguments);
            return c.unshift(b), a.validator.format.apply(this, c)
        } : void 0 === c ? b : (arguments.length > 2 && c.constructor !== Array && (c = a.makeArray(arguments).slice(1)), c.constructor !== Array && (c = [c]), a.each(c, function (a, c) {
            b = b.replace(new RegExp("\\{" + a + "\\}", "g"), function () {
                return c
            })
        }), b)
    }, a.extend(a.validator, {
        defaults: {
            messages: {},
            groups: {},
            rules: {},
            errorClass: "error",
            pendingClass: "pending",
            validClass: "valid",
            errorElement: "label",
            focusCleanup: !1,
            focusInvalid: !0,
            errorContainer: a([]),
            errorLabelContainer: a([]),
            onsubmit: !0,
            ignore: ":hidden",
            ignoreTitle: !1,
            onfocusin: function (a) {
                this.lastActive = a, this.settings.focusCleanup && (this.settings.unhighlight && this.settings.unhighlight.call(this, a, this.settings.errorClass, this.settings.validClass), this.hideThese(this.errorsFor(a)))
            },
            onfocusout: function (a) {
                this.checkable(a) || !(a.name in this.submitted) && this.optional(a) || this.element(a)
            },
            onkeyup: function (b, c) {
                var d = [16, 17, 18, 20, 35, 36, 37, 38, 39, 40, 45, 144, 225];
                9 === c.which && "" === this.elementValue(b) || a.inArray(c.keyCode, d) !== -1 || (b.name in this.submitted || b.name in this.invalid) && this.element(b)
            },
            onclick: function (a) {
                a.name in this.submitted ? this.element(a) : a.parentNode.name in this.submitted && this.element(a.parentNode)
            },
            highlight: function (b, c, d) {
                "radio" === b.type ? this.findByName(b.name).addClass(c).removeClass(d) : a(b).addClass(c).removeClass(d)
            },
            unhighlight: function (b, c, d) {
                "radio" === b.type ? this.findByName(b.name).removeClass(c).addClass(d) : a(b).removeClass(c).addClass(d)
            }
        },
        setDefaults: function (b) {
            a.extend(a.validator.defaults, b)
        },
        messages: {
            required: "This field is required.",
            remote: "Please fix this field.",
            email: "Please enter a valid email address.",
            url: "Please enter a valid URL.",
            date: "Please enter a valid date.",
            dateISO: "Please enter a valid date (ISO).",
            number: "Please enter a valid number.",
            digits: "Please enter only digits.",
            equalTo: "Please enter the same value again.",
            maxlength: a.validator.format("Please enter no more than {0} characters."),
            minlength: a.validator.format("Please enter at least {0} characters."),
            rangelength: a.validator.format("Please enter a value between {0} and {1} characters long."),
            range: a.validator.format("Please enter a value between {0} and {1}."),
            max: a.validator.format("Please enter a value less than or equal to {0}."),
            min: a.validator.format("Please enter a value greater than or equal to {0}."),
            step: a.validator.format("Please enter a multiple of {0}.")
        },
        autoCreateRanges: !1,
        prototype: {
            init: function () {
                function b(b) {
                    var c = "undefined" != typeof a(this).attr("contenteditable") && "false" !== a(this).attr("contenteditable");
                    if (!this.form && c && (this.form = a(this).closest("form")[0], this.name = a(this).attr("name")), d === this.form) {
                        var e = a.data(this.form, "validator"), f = "on" + b.type.replace(/^validate/, ""),
                            g = e.settings;
                        g[f] && !a(this).is(g.ignore) && g[f].call(e, this, b)
                    }
                }

                this.labelContainer = a(this.settings.errorLabelContainer), this.errorContext = this.labelContainer.length && this.labelContainer || a(this.currentForm), this.containers = a(this.settings.errorContainer).add(this.settings.errorLabelContainer), this.submitted = {}, this.valueCache = {}, this.pendingRequest = 0, this.pending = {}, this.invalid = {}, this.reset();
                var c, d = this.currentForm, e = this.groups = {};
                a.each(this.settings.groups, function (b, c) {
                    "string" == typeof c && (c = c.split(/\s/)), a.each(c, function (a, c) {
                        e[c] = b
                    })
                }), c = this.settings.rules, a.each(c, function (b, d) {
                    c[b] = a.validator.normalizeRule(d)
                }), a(this.currentForm).on("focusin.validate focusout.validate keyup.validate", ":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'], [type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], [type='radio'], [type='checkbox'], [contenteditable], [type='button']", b).on("click.validate", "select, option, [type='radio'], [type='checkbox']", b), this.settings.invalidHandler && a(this.currentForm).on("invalid-form.validate", this.settings.invalidHandler)
            }, form: function () {
                return this.checkForm(), a.extend(this.submitted, this.errorMap), this.invalid = a.extend({}, this.errorMap), this.valid() || a(this.currentForm).triggerHandler("invalid-form", [this]), this.showErrors(), this.valid()
            }, checkForm: function () {
                this.prepareForm();
                for (var a = 0, b = this.currentElements = this.elements(); b[a]; a++) this.check(b[a]);
                return this.valid()
            }, element: function (b) {
                var c, d, e = this.clean(b), f = this.validationTargetFor(e), g = this, h = !0;
                return void 0 === f ? delete this.invalid[e.name] : (this.prepareElement(f), this.currentElements = a(f), d = this.groups[f.name], d && a.each(this.groups, function (a, b) {
                    b === d && a !== f.name && (e = g.validationTargetFor(g.clean(g.findByName(a))), e && e.name in g.invalid && (g.currentElements.push(e), h = g.check(e) && h))
                }), c = this.check(f) !== !1, h = h && c, c ? this.invalid[f.name] = !1 : this.invalid[f.name] = !0, this.numberOfInvalids() || (this.toHide = this.toHide.add(this.containers)), this.showErrors(), a(b).attr("aria-invalid", !c)), h
            }, showErrors: function (b) {
                if (b) {
                    var c = this;
                    a.extend(this.errorMap, b), this.errorList = a.map(this.errorMap, function (a, b) {
                        return {message: a, element: c.findByName(b)[0]}
                    }), this.successList = a.grep(this.successList, function (a) {
                        return !(a.name in b)
                    })
                }
                this.settings.showErrors ? this.settings.showErrors.call(this, this.errorMap, this.errorList) : this.defaultShowErrors()
            }, resetForm: function () {
                a.fn.resetForm && a(this.currentForm).resetForm(), this.invalid = {}, this.submitted = {}, this.prepareForm(), this.hideErrors();
                var b = this.elements().removeData("previousValue").removeAttr("aria-invalid");
                this.resetElements(b)
            }, resetElements: function (a) {
                var b;
                if (this.settings.unhighlight) for (b = 0; a[b]; b++) this.settings.unhighlight.call(this, a[b], this.settings.errorClass, ""), this.findByName(a[b].name).removeClass(this.settings.validClass); else a.removeClass(this.settings.errorClass).removeClass(this.settings.validClass)
            }, numberOfInvalids: function () {
                return this.objectLength(this.invalid)
            }, objectLength: function (a) {
                var b, c = 0;
                for (b in a) void 0 !== a[b] && null !== a[b] && a[b] !== !1 && c++;
                return c
            }, hideErrors: function () {
                this.hideThese(this.toHide)
            }, hideThese: function (a) {
                a.not(this.containers).text(""), this.addWrapper(a).hide()
            }, valid: function () {
                return 0 === this.size()
            }, size: function () {
                return this.errorList.length
            }, focusInvalid: function () {
                if (this.settings.focusInvalid) try {
                    a(this.findLastActive() || this.errorList.length && this.errorList[0].element || []).filter(":visible").trigger("focus").trigger("focusin")
                } catch (b) {
                }
            }, findLastActive: function () {
                var b = this.lastActive;
                return b && 1 === a.grep(this.errorList, function (a) {
                    return a.element.name === b.name
                }).length && b
            }, elements: function () {
                var b = this, c = {};
                return a(this.currentForm).find("input, select, textarea, [contenteditable]").not(":submit, :reset, :image, :disabled").not(this.settings.ignore).filter(function () {
                    var d = this.name || a(this).attr("name"),
                        e = "undefined" != typeof a(this).attr("contenteditable") && "false" !== a(this).attr("contenteditable");
                    return !d && b.settings.debug && window.console && console.error("%o has no name assigned", this), e && (this.form = a(this).closest("form")[0], this.name = d), this.form === b.currentForm && (!(d in c || !b.objectLength(a(this).rules())) && (c[d] = !0, !0))
                })
            }, clean: function (b) {
                return a(b)[0]
            }, errors: function () {
                var b = this.settings.errorClass.split(" ").join(".");
                return a(this.settings.errorElement + "." + b, this.errorContext)
            }, resetInternals: function () {
                this.successList = [], this.errorList = [], this.errorMap = {}, this.toShow = a([]), this.toHide = a([])
            }, reset: function () {
                this.resetInternals(), this.currentElements = a([])
            }, prepareForm: function () {
                this.reset(), this.toHide = this.errors().add(this.containers)
            }, prepareElement: function (a) {
                this.reset(), this.toHide = this.errorsFor(a)
            }, elementValue: function (b) {
                var c, d, e = a(b), f = b.type,
                    g = "undefined" != typeof e.attr("contenteditable") && "false" !== e.attr("contenteditable");
                return "radio" === f || "checkbox" === f ? this.findByName(b.name).filter(":checked").val() : "number" === f && "undefined" != typeof b.validity ? b.validity.badInput ? "NaN" : e.val() : (c = g ? e.text() : e.val(), "file" === f ? "C:\\fakepath\\" === c.substr(0, 12) ? c.substr(12) : (d = c.lastIndexOf("/"), d >= 0 ? c.substr(d + 1) : (d = c.lastIndexOf("\\"), d >= 0 ? c.substr(d + 1) : c)) : "string" == typeof c ? c.replace(/\r/g, "") : c)
            }, check: function (b) {
                b = this.validationTargetFor(this.clean(b));
                var c, d, e, f, g = a(b).rules(), h = a.map(g, function (a, b) {
                    return b
                }).length, i = !1, j = this.elementValue(b);
                "function" == typeof g.normalizer ? f = g.normalizer : "function" == typeof this.settings.normalizer && (f = this.settings.normalizer), f && (j = f.call(b, j), delete g.normalizer);
                for (d in g) {
                    e = {method: d, parameters: g[d]};
                    try {
                        if (c = a.validator.methods[d].call(this, j, b, e.parameters), "dependency-mismatch" === c && 1 === h) {
                            i = !0;
                            continue
                        }
                        if (i = !1, "pending" === c) return void (this.toHide = this.toHide.not(this.errorsFor(b)));
                        if (!c) return this.formatAndAdd(b, e), !1
                    } catch (k) {
                        throw this.settings.debug && window.console && console.log("Exception occurred when checking element " + b.id + ", check the '" + e.method + "' method.", k), k instanceof TypeError && (k.message += ".  Exception occurred when checking element " + b.id + ", check the '" + e.method + "' method."), k
                    }
                }
                if (!i) return this.objectLength(g) && this.successList.push(b), !0
            }, customDataMessage: function (b, c) {
                return a(b).data("msg" + c.charAt(0).toUpperCase() + c.substring(1).toLowerCase()) || a(b).data("msg")
            }, customMessage: function (a, b) {
                var c = this.settings.messages[a];
                return c && (c.constructor === String ? c : c[b])
            }, findDefined: function () {
                for (var a = 0; a < arguments.length; a++) if (void 0 !== arguments[a]) return arguments[a]
            }, defaultMessage: function (b, c) {
                "string" == typeof c && (c = {method: c});
                var d = this.findDefined(this.customMessage(b.name, c.method), this.customDataMessage(b, c.method), !this.settings.ignoreTitle && b.title || void 0, a.validator.messages[c.method], "<strong>Warning: No message defined for " + b.name + "</strong>"),
                    e = /\$?\{(\d+)\}/g;
                return "function" == typeof d ? d = d.call(this, c.parameters, b) : e.test(d) && (d = a.validator.format(d.replace(e, "{$1}"), c.parameters)), d
            }, formatAndAdd: function (a, b) {
                var c = this.defaultMessage(a, b);
                this.errorList.push({
                    message: c,
                    element: a,
                    method: b.method
                }), this.errorMap[a.name] = c, this.submitted[a.name] = c
            }, addWrapper: function (a) {
                return this.settings.wrapper && (a = a.add(a.parent(this.settings.wrapper))), a
            }, defaultShowErrors: function () {
                var a, b, c;
                for (a = 0; this.errorList[a]; a++) c = this.errorList[a], this.settings.highlight && this.settings.highlight.call(this, c.element, this.settings.errorClass, this.settings.validClass), this.showLabel(c.element, c.message);
                if (this.errorList.length && (this.toShow = this.toShow.add(this.containers)), this.settings.success) for (a = 0; this.successList[a]; a++) this.showLabel(this.successList[a]);
                if (this.settings.unhighlight) for (a = 0, b = this.validElements(); b[a]; a++) this.settings.unhighlight.call(this, b[a], this.settings.errorClass, this.settings.validClass);
                this.toHide = this.toHide.not(this.toShow), this.hideErrors(), this.addWrapper(this.toShow).show()
            }, validElements: function () {
                return this.currentElements.not(this.invalidElements())
            }, invalidElements: function () {
                return a(this.errorList).map(function () {
                    return this.element
                })
            }, showLabel: function (b, c) {
                var d, e, f, g, h = this.errorsFor(b), i = this.idOrName(b), j = a(b).attr("aria-describedby");
                h.length ? (h.removeClass(this.settings.validClass).addClass(this.settings.errorClass), h.html(c)) : (h = a("<" + this.settings.errorElement + ">").attr("id", i + "-error").addClass(this.settings.errorClass).html(c || ""), d = h, this.settings.wrapper && (d = h.hide().show().wrap("<" + this.settings.wrapper + "/>").parent()), this.labelContainer.length ? this.labelContainer.append(d) : this.settings.errorPlacement ? this.settings.errorPlacement.call(this, d, a(b)) : d.insertAfter(b), h.is("label") ? h.attr("for", i) : 0 === h.parents("label[for='" + this.escapeCssMeta(i) + "']").length && (f = h.attr("id"), j ? j.match(new RegExp("\\b" + this.escapeCssMeta(f) + "\\b")) || (j += " " + f) : j = f, a(b).attr("aria-describedby", j), e = this.groups[b.name], e && (g = this, a.each(g.groups, function (b, c) {
                    c === e && a("[name='" + g.escapeCssMeta(b) + "']", g.currentForm).attr("aria-describedby", h.attr("id"))
                })))), !c && this.settings.success && (h.text(""), "string" == typeof this.settings.success ? h.addClass(this.settings.success) : this.settings.success(h, b)), this.toShow = this.toShow.add(h)
            }, errorsFor: function (b) {
                var c = this.escapeCssMeta(this.idOrName(b)), d = a(b).attr("aria-describedby"),
                    e = "label[for='" + c + "'], label[for='" + c + "'] *";
                return d && (e = e + ", #" + this.escapeCssMeta(d).replace(/\s+/g, ", #")), this.errors().filter(e)
            }, escapeCssMeta: function (a) {
                return a.replace(/([\\!"#$%&'()*+,.\/:;<=>?@\[\]^`{|}~])/g, "\\$1")
            }, idOrName: function (a) {
                return this.groups[a.name] || (this.checkable(a) ? a.name : a.id || a.name)
            }, validationTargetFor: function (b) {
                return this.checkable(b) && (b = this.findByName(b.name)), a(b).not(this.settings.ignore)[0]
            }, checkable: function (a) {
                return /radio|checkbox/i.test(a.type)
            }, findByName: function (b) {
                return a(this.currentForm).find("[name='" + this.escapeCssMeta(b) + "']")
            }, getLength: function (b, c) {
                switch (c.nodeName.toLowerCase()) {
                    case"select":
                        return a("option:selected", c).length;
                    case"input":
                        if (this.checkable(c)) return this.findByName(c.name).filter(":checked").length
                }
                return b.length
            }, depend: function (a, b) {
                return !this.dependTypes[typeof a] || this.dependTypes[typeof a](a, b)
            }, dependTypes: {
                "boolean": function (a) {
                    return a
                }, string: function (b, c) {
                    return !!a(b, c.form).length
                }, "function": function (a, b) {
                    return a(b)
                }
            }, optional: function (b) {
                var c = this.elementValue(b);
                return !a.validator.methods.required.call(this, c, b) && "dependency-mismatch"
            }, startRequest: function (b) {
                this.pending[b.name] || (this.pendingRequest++, a(b).addClass(this.settings.pendingClass), this.pending[b.name] = !0)
            }, stopRequest: function (b, c) {
                this.pendingRequest--, this.pendingRequest < 0 && (this.pendingRequest = 0), delete this.pending[b.name], a(b).removeClass(this.settings.pendingClass), c && 0 === this.pendingRequest && this.formSubmitted && this.form() ? (a(this.currentForm).submit(), this.submitButton && a("input:hidden[name='" + this.submitButton.name + "']", this.currentForm).remove(), this.formSubmitted = !1) : !c && 0 === this.pendingRequest && this.formSubmitted && (a(this.currentForm).triggerHandler("invalid-form", [this]), this.formSubmitted = !1)
            }, previousValue: function (b, c) {
                return c = "string" == typeof c && c || "remote", a.data(b, "previousValue") || a.data(b, "previousValue", {
                    old: null,
                    valid: !0,
                    message: this.defaultMessage(b, {method: c})
                })
            }, destroy: function () {
                this.resetForm(), a(this.currentForm).off(".validate").removeData("validator").find(".validate-equalTo-blur").off(".validate-equalTo").removeClass("validate-equalTo-blur").find(".validate-lessThan-blur").off(".validate-lessThan").removeClass("validate-lessThan-blur").find(".validate-lessThanEqual-blur").off(".validate-lessThanEqual").removeClass("validate-lessThanEqual-blur").find(".validate-greaterThanEqual-blur").off(".validate-greaterThanEqual").removeClass("validate-greaterThanEqual-blur").find(".validate-greaterThan-blur").off(".validate-greaterThan").removeClass("validate-greaterThan-blur")
            }
        },
        classRuleSettings: {
            required: {required: !0},
            email: {email: !0},
            url: {url: !0},
            date: {date: !0},
            dateISO: {dateISO: !0},
            number: {number: !0},
            digits: {digits: !0},
            creditcard: {creditcard: !0}
        },
        addClassRules: function (b, c) {
            b.constructor === String ? this.classRuleSettings[b] = c : a.extend(this.classRuleSettings, b)
        },
        classRules: function (b) {
            var c = {}, d = a(b).attr("class");
            return d && a.each(d.split(" "), function () {
                this in a.validator.classRuleSettings && a.extend(c, a.validator.classRuleSettings[this])
            }), c
        },
        normalizeAttributeRule: function (a, b, c, d) {
            /min|max|step/.test(c) && (null === b || /number|range|text/.test(b)) && (d = Number(d), isNaN(d) && (d = void 0)), d || 0 === d ? a[c] = d : b === c && "range" !== b && (a[c] = !0)
        },
        attributeRules: function (b) {
            var c, d, e = {}, f = a(b), g = b.getAttribute("type");
            for (c in a.validator.methods) "required" === c ? (d = b.getAttribute(c), "" === d && (d = !0), d = !!d) : d = f.attr(c), this.normalizeAttributeRule(e, g, c, d);
            return e.maxlength && /-1|2147483647|524288/.test(e.maxlength) && delete e.maxlength, e
        },
        dataRules: function (b) {
            var c, d, e = {}, f = a(b), g = b.getAttribute("type");
            for (c in a.validator.methods) d = f.data("rule" + c.charAt(0).toUpperCase() + c.substring(1).toLowerCase()), "" === d && (d = !0), this.normalizeAttributeRule(e, g, c, d);
            return e
        },
        staticRules: function (b) {
            var c = {}, d = a.data(b.form, "validator");
            return d.settings.rules && (c = a.validator.normalizeRule(d.settings.rules[b.name]) || {}), c
        },
        normalizeRules: function (b, c) {
            return a.each(b, function (d, e) {
                if (e === !1) return void delete b[d];
                if (e.param || e.depends) {
                    var f = !0;
                    switch (typeof e.depends) {
                        case"string":
                            f = !!a(e.depends, c.form).length;
                            break;
                        case"function":
                            f = e.depends.call(c, c)
                    }
                    f ? b[d] = void 0 === e.param || e.param : (a.data(c.form, "validator").resetElements(a(c)), delete b[d])
                }
            }), a.each(b, function (d, e) {
                b[d] = a.isFunction(e) && "normalizer" !== d ? e(c) : e
            }), a.each(["minlength", "maxlength"], function () {
                b[this] && (b[this] = Number(b[this]))
            }), a.each(["rangelength", "range"], function () {
                var c;
                b[this] && (a.isArray(b[this]) ? b[this] = [Number(b[this][0]), Number(b[this][1])] : "string" == typeof b[this] && (c = b[this].replace(/[\[\]]/g, "").split(/[\s,]+/), b[this] = [Number(c[0]), Number(c[1])]))
            }), a.validator.autoCreateRanges && (null != b.min && null != b.max && (b.range = [b.min, b.max], delete b.min, delete b.max), null != b.minlength && null != b.maxlength && (b.rangelength = [b.minlength, b.maxlength], delete b.minlength, delete b.maxlength)), b
        },
        normalizeRule: function (b) {
            if ("string" == typeof b) {
                var c = {};
                a.each(b.split(/\s/), function () {
                    c[this] = !0
                }), b = c
            }
            return b
        },
        addMethod: function (b, c, d) {
            a.validator.methods[b] = c, a.validator.messages[b] = void 0 !== d ? d : a.validator.messages[b], c.length < 3 && a.validator.addClassRules(b, a.validator.normalizeRule(b))
        },
        methods: {
            required: function (b, c, d) {
                if (!this.depend(d, c)) return "dependency-mismatch";
                if ("select" === c.nodeName.toLowerCase()) {
                    var e = a(c).val();
                    return e && e.length > 0
                }
                return this.checkable(c) ? this.getLength(b, c) > 0 : void 0 !== b && null !== b && b.length > 0
            }, email: function (a, b) {
                return this.optional(b) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(a)
            }, url: function (a, b) {
                return this.optional(b) || /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[\/?#]\S*)?$/i.test(a)
            }, date: function () {
                var a = !1;
                return function (b, c) {
                    return a || (a = !0, this.settings.debug && window.console && console.warn("The `date` method is deprecated and will be removed in version '2.0.0'.\nPlease don't use it, since it relies on the Date constructor, which\nbehaves very differently across browsers and locales. Use `dateISO`\ninstead or one of the locale specific methods in `localizations/`\nand `additional-methods.js`.")), this.optional(c) || !/Invalid|NaN/.test(new Date(b).toString())
                }
            }(), dateISO: function (a, b) {
                return this.optional(b) || /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(a)
            }, number: function (a, b) {
                return this.optional(b) || /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(a)
            }, digits: function (a, b) {
                return this.optional(b) || /^\d+$/.test(a)
            }, minlength: function (b, c, d) {
                var e = a.isArray(b) ? b.length : this.getLength(b, c);
                return this.optional(c) || e >= d
            }, maxlength: function (b, c, d) {
                var e = a.isArray(b) ? b.length : this.getLength(b, c);
                return this.optional(c) || e <= d
            }, rangelength: function (b, c, d) {
                var e = a.isArray(b) ? b.length : this.getLength(b, c);
                return this.optional(c) || e >= d[0] && e <= d[1]
            }, min: function (a, b, c) {
                return this.optional(b) || a >= c
            }, max: function (a, b, c) {
                return this.optional(b) || a <= c
            }, range: function (a, b, c) {
                return this.optional(b) || a >= c[0] && a <= c[1]
            }, step: function (b, c, d) {
                var e, f = a(c).attr("type"), g = "Step attribute on input type " + f + " is not supported.",
                    h = ["text", "number", "range"], i = new RegExp("\\b" + f + "\\b"), j = f && !i.test(h.join()),
                    k = function (a) {
                        var b = ("" + a).match(/(?:\.(\d+))?$/);
                        return b && b[1] ? b[1].length : 0
                    }, l = function (a) {
                        return Math.round(a * Math.pow(10, e))
                    }, m = !0;
                if (j) throw new Error(g);
                return e = k(d), (k(b) > e || l(b) % l(d) !== 0) && (m = !1), this.optional(c) || m
            }, equalTo: function (b, c, d) {
                var e = a(d);
                return this.settings.onfocusout && e.not(".validate-equalTo-blur").length && e.addClass("validate-equalTo-blur").on("blur.validate-equalTo", function () {
                    a(c).valid()
                }), b === e.val()
            }, remote: function (b, c, d, e) {
                if (this.optional(c)) return "dependency-mismatch";
                e = "string" == typeof e && e || "remote";
                var f, g, h, i = this.previousValue(c, e);
                return this.settings.messages[c.name] || (this.settings.messages[c.name] = {}), i.originalMessage = i.originalMessage || this.settings.messages[c.name][e], this.settings.messages[c.name][e] = i.message, d = "string" == typeof d && {url: d} || d, h = a.param(a.extend({data: b}, d.data)), i.old === h ? i.valid : (i.old = h, f = this, this.startRequest(c), g = {}, g[c.name] = b, a.ajax(a.extend(!0, {
                    mode: "abort",
                    port: "validate" + c.name,
                    dataType: "json",
                    data: g,
                    context: f.currentForm,
                    success: function (a) {
                        var d, g, h, j = a === !0 || "true" === a;
                        f.settings.messages[c.name][e] = i.originalMessage, j ? (h = f.formSubmitted, f.resetInternals(), f.toHide = f.errorsFor(c), f.formSubmitted = h, f.successList.push(c), f.invalid[c.name] = !1, f.showErrors()) : (d = {}, g = a || f.defaultMessage(c, {
                            method: e,
                            parameters: b
                        }), d[c.name] = i.message = g, f.invalid[c.name] = !0, f.showErrors(d)), i.valid = j, f.stopRequest(c, j)
                    }
                }, d)), "pending")
            }
        }
    });
    var b, c = {};
    return a.ajaxPrefilter ? a.ajaxPrefilter(function (a, b, d) {
        var e = a.port;
        "abort" === a.mode && (c[e] && c[e].abort(), c[e] = d)
    }) : (b = a.ajax, a.ajax = function (d) {
        var e = ("mode" in d ? d : a.ajaxSettings).mode, f = ("port" in d ? d : a.ajaxSettings).port;
        return "abort" === e ? (c[f] && c[f].abort(), c[f] = b.apply(this, arguments), c[f]) : b.apply(this, arguments)
    }), a
});/*! jQuery Validation Plugin - v1.19.1 - 6/15/2019
 * https://jqueryvalidation.org/
 * Copyright (c) 2019 Jörn Zaefferer; Licensed MIT */
!function (a) {
    "function" == typeof define && define.amd ? define(["jquery", "./jquery.validate.min"], a) : "object" == typeof module && module.exports ? module.exports = a(require("jquery")) : a(jQuery)
}(function (a) {
    return function () {
        function b(a) {
            return a.replace(/<.[^<>]*?>/g, " ").replace(/&nbsp;|&#160;/gi, " ").replace(/[.(),;:!?%#$'\"_+=\/\-“”’]*/g, "")
        }

        a.validator.addMethod("maxWords", function (a, c, d) {
            return this.optional(c) || b(a).match(/\b\w+\b/g).length <= d
        }, a.validator.format("Please enter {0} words or less.")), a.validator.addMethod("minWords", function (a, c, d) {
            return this.optional(c) || b(a).match(/\b\w+\b/g).length >= d
        }, a.validator.format("Please enter at least {0} words.")), a.validator.addMethod("rangeWords", function (a, c, d) {
            var e = b(a), f = /\b\w+\b/g;
            return this.optional(c) || e.match(f).length >= d[0] && e.match(f).length <= d[1]
        }, a.validator.format("Please enter between {0} and {1} words."))
    }(), a.validator.addMethod("abaRoutingNumber", function (a) {
        var b = 0, c = a.split(""), d = c.length;
        if (9 !== d) return !1;
        for (var e = 0; e < d; e += 3) b += 3 * parseInt(c[e], 10) + 7 * parseInt(c[e + 1], 10) + parseInt(c[e + 2], 10);
        return 0 !== b && b % 10 === 0
    }, "Please enter a valid routing number."), a.validator.addMethod("accept", function (b, c, d) {
        var e, f, g, h = "string" == typeof d ? d.replace(/\s/g, "") : "image/*", i = this.optional(c);
        if (i) return i;
        if ("file" === a(c).attr("type") && (h = h.replace(/[\-\[\]\/\{\}\(\)\+\?\.\\\^\$\|]/g, "\\$&").replace(/,/g, "|").replace(/\/\*/g, "/.*"), c.files && c.files.length)) for (g = new RegExp(".?(" + h + ")$", "i"), e = 0; e < c.files.length; e++) if (f = c.files[e], !f.type.match(g)) return !1;
        return !0
    }, a.validator.format("Please enter a value with a valid mimetype.")), a.validator.addMethod("alphanumeric", function (a, b) {
        return this.optional(b) || /^\w+$/i.test(a)
    }, "Letters, numbers, and underscores only please"), a.validator.addMethod("bankaccountNL", function (a, b) {
        if (this.optional(b)) return !0;
        if (!/^[0-9]{9}|([0-9]{2} ){3}[0-9]{3}$/.test(a)) return !1;
        var c, d, e, f = a.replace(/ /g, ""), g = 0, h = f.length;
        for (c = 0; c < h; c++) d = h - c, e = f.substring(c, c + 1), g += d * e;
        return g % 11 === 0
    }, "Please specify a valid bank account number"), a.validator.addMethod("bankorgiroaccountNL", function (b, c) {
        return this.optional(c) || a.validator.methods.bankaccountNL.call(this, b, c) || a.validator.methods.giroaccountNL.call(this, b, c)
    }, "Please specify a valid bank or giro account number"), a.validator.addMethod("bic", function (a, b) {
        return this.optional(b) || /^([A-Z]{6}[A-Z2-9][A-NP-Z1-9])(X{3}|[A-WY-Z0-9][A-Z0-9]{2})?$/.test(a.toUpperCase())
    }, "Please specify a valid BIC code"), a.validator.addMethod("cifES", function (a, b) {
        "use strict";

        function c(a) {
            return a % 2 === 0
        }

        if (this.optional(b)) return !0;
        var d, e, f, g, h = new RegExp(/^([ABCDEFGHJKLMNPQRSUVW])(\d{7})([0-9A-J])$/gi), i = a.substring(0, 1),
            j = a.substring(1, 8), k = a.substring(8, 9), l = 0, m = 0, n = 0;
        if (9 !== a.length || !h.test(a)) return !1;
        for (d = 0; d < j.length; d++) e = parseInt(j[d], 10), c(d) ? (e *= 2, n += e < 10 ? e : e - 9) : m += e;
        return l = m + n, f = (10 - l.toString().substr(-1)).toString(), f = parseInt(f, 10) > 9 ? "0" : f, g = "JABCDEFGHI".substr(f, 1).toString(), i.match(/[ABEH]/) ? k === f : i.match(/[KPQS]/) ? k === g : k === f || k === g
    }, "Please specify a valid CIF number."), a.validator.addMethod("cnhBR", function (a) {
        if (a = a.replace(/([~!@#$%^&*()_+=`{}\[\]\-|\\:;'<>,.\/? ])+/g, ""), 11 !== a.length) return !1;
        var b, c, d, e, f, g, h = 0, i = 0;
        if (b = a.charAt(0), new Array(12).join(b) === a) return !1;
        for (e = 0, f = 9, g = 0; e < 9; ++e, --f) h += +(a.charAt(e) * f);
        for (c = h % 11, c >= 10 && (c = 0, i = 2), h = 0, e = 0, f = 1, g = 0; e < 9; ++e, ++f) h += +(a.charAt(e) * f);
        return d = h % 11, d >= 10 ? d = 0 : d -= i, String(c).concat(d) === a.substr(-2)
    }, "Please specify a valid CNH number"), a.validator.addMethod("cnpjBR", function (a, b) {
        "use strict";
        if (this.optional(b)) return !0;
        if (a = a.replace(/[^\d]+/g, ""), 14 !== a.length) return !1;
        if ("00000000000000" === a || "11111111111111" === a || "22222222222222" === a || "33333333333333" === a || "44444444444444" === a || "55555555555555" === a || "66666666666666" === a || "77777777777777" === a || "88888888888888" === a || "99999999999999" === a) return !1;
        for (var c = a.length - 2, d = a.substring(0, c), e = a.substring(c), f = 0, g = c - 7, h = c; h >= 1; h--) f += d.charAt(c - h) * g--, g < 2 && (g = 9);
        var i = f % 11 < 2 ? 0 : 11 - f % 11;
        if (i !== parseInt(e.charAt(0), 10)) return !1;
        c += 1, d = a.substring(0, c), f = 0, g = c - 7;
        for (var j = c; j >= 1; j--) f += d.charAt(c - j) * g--, g < 2 && (g = 9);
        return i = f % 11 < 2 ? 0 : 11 - f % 11, i === parseInt(e.charAt(1), 10)
    }, "Please specify a CNPJ value number"), a.validator.addMethod("cpfBR", function (a, b) {
        "use strict";
        if (this.optional(b)) return !0;
        if (a = a.replace(/([~!@#$%^&*()_+=`{}\[\]\-|\\:;'<>,.\/? ])+/g, ""), 11 !== a.length) return !1;
        var c, d, e, f, g = 0;
        if (c = parseInt(a.substring(9, 10), 10), d = parseInt(a.substring(10, 11), 10), e = function (a, b) {
            var c = 10 * a % 11;
            return 10 !== c && 11 !== c || (c = 0), c === b
        }, "" === a || "00000000000" === a || "11111111111" === a || "22222222222" === a || "33333333333" === a || "44444444444" === a || "55555555555" === a || "66666666666" === a || "77777777777" === a || "88888888888" === a || "99999999999" === a) return !1;
        for (f = 1; f <= 9; f++) g += parseInt(a.substring(f - 1, f), 10) * (11 - f);
        if (e(g, c)) {
            for (g = 0, f = 1; f <= 10; f++) g += parseInt(a.substring(f - 1, f), 10) * (12 - f);
            return e(g, d)
        }
        return !1
    }, "Please specify a valid CPF number"), a.validator.addMethod("creditcard", function (a, b) {
        if (this.optional(b)) return "dependency-mismatch";
        if (/[^0-9 \-]+/.test(a)) return !1;
        var c, d, e = 0, f = 0, g = !1;
        if (a = a.replace(/\D/g, ""), a.length < 13 || a.length > 19) return !1;
        for (c = a.length - 1; c >= 0; c--) d = a.charAt(c), f = parseInt(d, 10), g && (f *= 2) > 9 && (f -= 9), e += f, g = !g;
        return e % 10 === 0
    }, "Please enter a valid credit card number."), a.validator.addMethod("creditcardtypes", function (a, b, c) {
        if (/[^0-9\-]+/.test(a)) return !1;
        a = a.replace(/\D/g, "");
        var d = 0;
        return c.mastercard && (d |= 1), c.visa && (d |= 2), c.amex && (d |= 4), c.dinersclub && (d |= 8), c.enroute && (d |= 16), c.discover && (d |= 32), c.jcb && (d |= 64), c.unknown && (d |= 128), c.all && (d = 255), 1 & d && (/^(5[12345])/.test(a) || /^(2[234567])/.test(a)) ? 16 === a.length : 2 & d && /^(4)/.test(a) ? 16 === a.length : 4 & d && /^(3[47])/.test(a) ? 15 === a.length : 8 & d && /^(3(0[012345]|[68]))/.test(a) ? 14 === a.length : 16 & d && /^(2(014|149))/.test(a) ? 15 === a.length : 32 & d && /^(6011)/.test(a) ? 16 === a.length : 64 & d && /^(3)/.test(a) ? 16 === a.length : 64 & d && /^(2131|1800)/.test(a) ? 15 === a.length : !!(128 & d)
    }, "Please enter a valid credit card number."), a.validator.addMethod("currency", function (a, b, c) {
        var d, e = "string" == typeof c, f = e ? c : c[0], g = !!e || c[1];
        return f = f.replace(/,/g, ""), f = g ? f + "]" : f + "]?", d = "^[" + f + "([1-9]{1}[0-9]{0,2}(\\,[0-9]{3})*(\\.[0-9]{0,2})?|[1-9]{1}[0-9]{0,}(\\.[0-9]{0,2})?|0(\\.[0-9]{0,2})?|(\\.[0-9]{1,2})?)$", d = new RegExp(d), this.optional(b) || d.test(a)
    }, "Please specify a valid currency"), a.validator.addMethod("dateFA", function (a, b) {
        return this.optional(b) || /^[1-4]\d{3}\/((0?[1-6]\/((3[0-1])|([1-2][0-9])|(0?[1-9])))|((1[0-2]|(0?[7-9]))\/(30|([1-2][0-9])|(0?[1-9]))))$/.test(a)
    }, a.validator.messages.date), a.validator.addMethod("dateITA", function (a, b) {
        var c, d, e, f, g, h = !1, i = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
        return i.test(a) ? (c = a.split("/"), d = parseInt(c[0], 10), e = parseInt(c[1], 10), f = parseInt(c[2], 10), g = new Date(Date.UTC(f, e - 1, d, 12, 0, 0, 0)), h = g.getUTCFullYear() === f && g.getUTCMonth() === e - 1 && g.getUTCDate() === d) : h = !1, this.optional(b) || h
    }, a.validator.messages.date), a.validator.addMethod("dateNL", function (a, b) {
        return this.optional(b) || /^(0?[1-9]|[12]\d|3[01])[\.\/\-](0?[1-9]|1[012])[\.\/\-]([12]\d)?(\d\d)$/.test(a)
    }, a.validator.messages.date), a.validator.addMethod("extension", function (a, b, c) {
        return c = "string" == typeof c ? c.replace(/,/g, "|") : "png|jpe?g|gif", this.optional(b) || a.match(new RegExp("\\.(" + c + ")$", "i"))
    }, a.validator.format("Please enter a value with a valid extension.")), a.validator.addMethod("giroaccountNL", function (a, b) {
        return this.optional(b) || /^[0-9]{1,7}$/.test(a)
    }, "Please specify a valid giro account number"), a.validator.addMethod("greaterThan", function (b, c, d) {
        var e = a(d);
        return this.settings.onfocusout && e.not(".validate-greaterThan-blur").length && e.addClass("validate-greaterThan-blur").on("blur.validate-greaterThan", function () {
            a(c).valid()
        }), b > e.val()
    }, "Please enter a greater value."), a.validator.addMethod("greaterThanEqual", function (b, c, d) {
        var e = a(d);
        return this.settings.onfocusout && e.not(".validate-greaterThanEqual-blur").length && e.addClass("validate-greaterThanEqual-blur").on("blur.validate-greaterThanEqual", function () {
            a(c).valid()
        }), b >= e.val()
    }, "Please enter a greater value."), a.validator.addMethod("iban", function (a, b) {
        if (this.optional(b)) return !0;
        var c, d, e, f, g, h, i, j, k, l = a.replace(/ /g, "").toUpperCase(), m = "", n = !0, o = "", p = "", q = 5;
        if (l.length < q) return !1;
        if (c = l.substring(0, 2), h = {
            AL: "\\d{8}[\\dA-Z]{16}",
            AD: "\\d{8}[\\dA-Z]{12}",
            AT: "\\d{16}",
            AZ: "[\\dA-Z]{4}\\d{20}",
            BE: "\\d{12}",
            BH: "[A-Z]{4}[\\dA-Z]{14}",
            BA: "\\d{16}",
            BR: "\\d{23}[A-Z][\\dA-Z]",
            BG: "[A-Z]{4}\\d{6}[\\dA-Z]{8}",
            CR: "\\d{17}",
            HR: "\\d{17}",
            CY: "\\d{8}[\\dA-Z]{16}",
            CZ: "\\d{20}",
            DK: "\\d{14}",
            DO: "[A-Z]{4}\\d{20}",
            EE: "\\d{16}",
            FO: "\\d{14}",
            FI: "\\d{14}",
            FR: "\\d{10}[\\dA-Z]{11}\\d{2}",
            GE: "[\\dA-Z]{2}\\d{16}",
            DE: "\\d{18}",
            GI: "[A-Z]{4}[\\dA-Z]{15}",
            GR: "\\d{7}[\\dA-Z]{16}",
            GL: "\\d{14}",
            GT: "[\\dA-Z]{4}[\\dA-Z]{20}",
            HU: "\\d{24}",
            IS: "\\d{22}",
            IE: "[\\dA-Z]{4}\\d{14}",
            IL: "\\d{19}",
            IT: "[A-Z]\\d{10}[\\dA-Z]{12}",
            KZ: "\\d{3}[\\dA-Z]{13}",
            KW: "[A-Z]{4}[\\dA-Z]{22}",
            LV: "[A-Z]{4}[\\dA-Z]{13}",
            LB: "\\d{4}[\\dA-Z]{20}",
            LI: "\\d{5}[\\dA-Z]{12}",
            LT: "\\d{16}",
            LU: "\\d{3}[\\dA-Z]{13}",
            MK: "\\d{3}[\\dA-Z]{10}\\d{2}",
            MT: "[A-Z]{4}\\d{5}[\\dA-Z]{18}",
            MR: "\\d{23}",
            MU: "[A-Z]{4}\\d{19}[A-Z]{3}",
            MC: "\\d{10}[\\dA-Z]{11}\\d{2}",
            MD: "[\\dA-Z]{2}\\d{18}",
            ME: "\\d{18}",
            NL: "[A-Z]{4}\\d{10}",
            NO: "\\d{11}",
            PK: "[\\dA-Z]{4}\\d{16}",
            PS: "[\\dA-Z]{4}\\d{21}",
            PL: "\\d{24}",
            PT: "\\d{21}",
            RO: "[A-Z]{4}[\\dA-Z]{16}",
            SM: "[A-Z]\\d{10}[\\dA-Z]{12}",
            SA: "\\d{2}[\\dA-Z]{18}",
            RS: "\\d{18}",
            SK: "\\d{20}",
            SI: "\\d{15}",
            ES: "\\d{20}",
            SE: "\\d{20}",
            CH: "\\d{5}[\\dA-Z]{12}",
            TN: "\\d{20}",
            TR: "\\d{5}[\\dA-Z]{17}",
            AE: "\\d{3}\\d{16}",
            GB: "[A-Z]{4}\\d{14}",
            VG: "[\\dA-Z]{4}\\d{16}"
        }, g = h[c], "undefined" != typeof g && (i = new RegExp("^[A-Z]{2}\\d{2}" + g + "$", ""), !i.test(l))) return !1;
        for (d = l.substring(4, l.length) + l.substring(0, 4), j = 0; j < d.length; j++) e = d.charAt(j), "0" !== e && (n = !1), n || (m += "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(e));
        for (k = 0; k < m.length; k++) f = m.charAt(k), p = "" + o + f, o = p % 97;
        return 1 === o
    }, "Please specify a valid IBAN"), a.validator.addMethod("integer", function (a, b) {
        return this.optional(b) || /^-?\d+$/.test(a)
    }, "A positive or negative non-decimal number please"), a.validator.addMethod("ipv4", function (a, b) {
        return this.optional(b) || /^(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)$/i.test(a)
    }, "Please enter a valid IP v4 address."), a.validator.addMethod("ipv6", function (a, b) {
        return this.optional(b) || /^((([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}:[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){5}:([0-9A-Fa-f]{1,4}:)?[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){4}:([0-9A-Fa-f]{1,4}:){0,2}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){3}:([0-9A-Fa-f]{1,4}:){0,3}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){2}:([0-9A-Fa-f]{1,4}:){0,4}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|(([0-9A-Fa-f]{1,4}:){0,5}:((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|(::([0-9A-Fa-f]{1,4}:){0,5}((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|([0-9A-Fa-f]{1,4}::([0-9A-Fa-f]{1,4}:){0,5}[0-9A-Fa-f]{1,4})|(::([0-9A-Fa-f]{1,4}:){0,6}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){1,7}:))$/i.test(a)
    }, "Please enter a valid IP v6 address."), a.validator.addMethod("lessThan", function (b, c, d) {
        var e = a(d);
        return this.settings.onfocusout && e.not(".validate-lessThan-blur").length && e.addClass("validate-lessThan-blur").on("blur.validate-lessThan", function () {
            a(c).valid()
        }), b < e.val()
    }, "Please enter a lesser value."), a.validator.addMethod("lessThanEqual", function (b, c, d) {
        var e = a(d);
        return this.settings.onfocusout && e.not(".validate-lessThanEqual-blur").length && e.addClass("validate-lessThanEqual-blur").on("blur.validate-lessThanEqual", function () {
            a(c).valid()
        }), b <= e.val()
    }, "Please enter a lesser value."), a.validator.addMethod("lettersonly", function (a, b) {
        return this.optional(b) || /^[a-z]+$/i.test(a)
    }, "Letters only please"), a.validator.addMethod("letterswithbasicpunc", function (a, b) {
        return this.optional(b) || /^[a-z\-.,()'"\s]+$/i.test(a)
    }, "Letters or punctuation only please"), a.validator.addMethod("maxfiles", function (b, c, d) {
        return !!this.optional(c) || !("file" === a(c).attr("type") && c.files && c.files.length > d)
    }, a.validator.format("Please select no more than {0} files.")), a.validator.addMethod("maxsize", function (b, c, d) {
        if (this.optional(c)) return !0;
        if ("file" === a(c).attr("type") && c.files && c.files.length) for (var e = 0; e < c.files.length; e++) if (c.files[e].size > d) return !1;
        return !0
    }, a.validator.format("File size must not exceed {0} bytes each.")), a.validator.addMethod("maxsizetotal", function (b, c, d) {
        if (this.optional(c)) return !0;
        if ("file" === a(c).attr("type") && c.files && c.files.length) for (var e = 0, f = 0; f < c.files.length; f++) if (e += c.files[f].size, e > d) return !1;
        return !0
    }, a.validator.format("Total size of all files must not exceed {0} bytes.")), a.validator.addMethod("mobileNL", function (a, b) {
        return this.optional(b) || /^((\+|00(\s|\s?\-\s?)?)31(\s|\s?\-\s?)?(\(0\)[\-\s]?)?|0)6((\s|\s?\-\s?)?[0-9]){8}$/.test(a)
    }, "Please specify a valid mobile number"), a.validator.addMethod("mobileRU", function (a, b) {
        var c = a.replace(/\(|\)|\s+|-/g, "");
        return this.optional(b) || c.length > 9 && /^((\+7|7|8)+([0-9]){10})$/.test(c)
    }, "Please specify a valid mobile number"), a.validator.addMethod("mobileUK", function (a, b) {
        return a = a.replace(/\(|\)|\s+|-/g, ""), this.optional(b) || a.length > 9 && a.match(/^(?:(?:(?:00\s?|\+)44\s?|0)7(?:[1345789]\d{2}|624)\s?\d{3}\s?\d{3})$/)
    }, "Please specify a valid mobile number"), a.validator.addMethod("netmask", function (a, b) {
        return this.optional(b) || /^(254|252|248|240|224|192|128)\.0\.0\.0|255\.(254|252|248|240|224|192|128|0)\.0\.0|255\.255\.(254|252|248|240|224|192|128|0)\.0|255\.255\.255\.(254|252|248|240|224|192|128|0)/i.test(a)
    }, "Please enter a valid netmask."), a.validator.addMethod("nieES", function (a, b) {
        "use strict";
        if (this.optional(b)) return !0;
        var c, d = new RegExp(/^[MXYZ]{1}[0-9]{7,8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/gi), e = "TRWAGMYFPDXBNJZSQVHLCKET",
            f = a.substr(a.length - 1).toUpperCase();
        return a = a.toString().toUpperCase(), !(a.length > 10 || a.length < 9 || !d.test(a)) && (a = a.replace(/^[X]/, "0").replace(/^[Y]/, "1").replace(/^[Z]/, "2"), c = 9 === a.length ? a.substr(0, 8) : a.substr(0, 9), e.charAt(parseInt(c, 10) % 23) === f)
    }, "Please specify a valid NIE number."), a.validator.addMethod("nifES", function (a, b) {
        "use strict";
        return !!this.optional(b) || (a = a.toUpperCase(), !!a.match("((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)") && (/^[0-9]{8}[A-Z]{1}$/.test(a) ? "TRWAGMYFPDXBNJZSQVHLCKE".charAt(a.substring(8, 0) % 23) === a.charAt(8) : !!/^[KLM]{1}/.test(a) && a[8] === "TRWAGMYFPDXBNJZSQVHLCKE".charAt(a.substring(8, 1) % 23)))
    }, "Please specify a valid NIF number."), a.validator.addMethod("nipPL", function (a) {
        "use strict";
        if (a = a.replace(/[^0-9]/g, ""), 10 !== a.length) return !1;
        for (var b = [6, 5, 7, 2, 3, 4, 5, 6, 7], c = 0, d = 0; d < 9; d++) c += b[d] * a[d];
        var e = c % 11, f = 10 === e ? 0 : e;
        return f === parseInt(a[9], 10)
    }, "Please specify a valid NIP number."), a.validator.addMethod("nisBR", function (a) {
        var b, c, d, e, f, g = 0;
        if (a = a.replace(/([~!@#$%^&*()_+=`{}\[\]\-|\\:;'<>,.\/? ])+/g, ""), 11 !== a.length) return !1;
        for (c = parseInt(a.substring(10, 11), 10), b = parseInt(a.substring(0, 10), 10), e = 2; e < 12; e++) f = e, 10 === e && (f = 2), 11 === e && (f = 3), g += b % 10 * f, b = parseInt(b / 10, 10);
        return d = g % 11, d = d > 1 ? 11 - d : 0, c === d
    }, "Please specify a valid NIS/PIS number"), a.validator.addMethod("notEqualTo", function (b, c, d) {
        return this.optional(c) || !a.validator.methods.equalTo.call(this, b, c, d)
    }, "Please enter a different value, values must not be the same."), a.validator.addMethod("nowhitespace", function (a, b) {
        return this.optional(b) || /^\S+$/i.test(a)
    }, "No white space please"), a.validator.addMethod("pattern", function (a, b, c) {
        return !!this.optional(b) || ("string" == typeof c && (c = new RegExp("^(?:" + c + ")$")), c.test(a))
    }, "Invalid format."), a.validator.addMethod("phoneNL", function (a, b) {
        return this.optional(b) || /^((\+|00(\s|\s?\-\s?)?)31(\s|\s?\-\s?)?(\(0\)[\-\s]?)?|0)[1-9]((\s|\s?\-\s?)?[0-9]){8}$/.test(a)
    }, "Please specify a valid phone number."), a.validator.addMethod("phonePL", function (a, b) {
        a = a.replace(/\s+/g, "");
        var c = /^(?:(?:(?:\+|00)?48)|(?:\(\+?48\)))?(?:1[2-8]|2[2-69]|3[2-49]|4[1-68]|5[0-9]|6[0-35-9]|[7-8][1-9]|9[145])\d{7}$/;
        return this.optional(b) || c.test(a)
    }, "Please specify a valid phone number"), a.validator.addMethod("phonesUK", function (a, b) {
        return a = a.replace(/\(|\)|\s+|-/g, ""), this.optional(b) || a.length > 9 && a.match(/^(?:(?:(?:00\s?|\+)44\s?|0)(?:1\d{8,9}|[23]\d{9}|7(?:[1345789]\d{8}|624\d{6})))$/)
    }, "Please specify a valid uk phone number"), a.validator.addMethod("phoneUK", function (a, b) {
        return a = a.replace(/\(|\)|\s+|-/g, ""), this.optional(b) || a.length > 9 && a.match(/^(?:(?:(?:00\s?|\+)44\s?)|(?:\(?0))(?:\d{2}\)?\s?\d{4}\s?\d{4}|\d{3}\)?\s?\d{3}\s?\d{3,4}|\d{4}\)?\s?(?:\d{5}|\d{3}\s?\d{3})|\d{5}\)?\s?\d{4,5})$/)
    }, "Please specify a valid phone number"), a.validator.addMethod("phoneUS", function (a, b) {
        return a = a.replace(/\s+/g, ""), this.optional(b) || a.length > 9 && a.match(/^(\+?1-?)?(\([2-9]([02-9]\d|1[02-9])\)|[2-9]([02-9]\d|1[02-9]))-?[2-9]\d{2}-?\d{4}$/)
    }, "Please specify a valid phone number"), a.validator.addMethod("postalcodeBR", function (a, b) {
        return this.optional(b) || /^\d{2}.\d{3}-\d{3}?$|^\d{5}-?\d{3}?$/.test(a)
    }, "Informe um CEP válido."), a.validator.addMethod("postalCodeCA", function (a, b) {
        return this.optional(b) || /^[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ] *\d[ABCEGHJKLMNPRSTVWXYZ]\d$/i.test(a)
    }, "Please specify a valid postal code"), a.validator.addMethod("postalcodeIT", function (a, b) {
        return this.optional(b) || /^\d{5}$/.test(a)
    }, "Please specify a valid postal code"), a.validator.addMethod("postalcodeNL", function (a, b) {
        return this.optional(b) || /^[1-9][0-9]{3}\s?[a-zA-Z]{2}$/.test(a)
    }, "Please specify a valid postal code"), a.validator.addMethod("postcodeUK", function (a, b) {
        return this.optional(b) || /^((([A-PR-UWYZ][0-9])|([A-PR-UWYZ][0-9][0-9])|([A-PR-UWYZ][A-HK-Y][0-9])|([A-PR-UWYZ][A-HK-Y][0-9][0-9])|([A-PR-UWYZ][0-9][A-HJKSTUW])|([A-PR-UWYZ][A-HK-Y][0-9][ABEHMNPRVWXY]))\s?([0-9][ABD-HJLNP-UW-Z]{2})|(GIR)\s?(0AA))$/i.test(a)
    }, "Please specify a valid UK postcode"), a.validator.addMethod("require_from_group", function (b, c, d) {
        var e = a(d[1], c.form), f = e.eq(0),
            g = f.data("valid_req_grp") ? f.data("valid_req_grp") : a.extend({}, this), h = e.filter(function () {
                return g.elementValue(this)
            }).length >= d[0];
        return f.data("valid_req_grp", g), a(c).data("being_validated") || (e.data("being_validated", !0), e.each(function () {
            g.element(this)
        }), e.data("being_validated", !1)), h
    }, a.validator.format("Please fill at least {0} of these fields.")), a.validator.addMethod("skip_or_fill_minimum", function (b, c, d) {
        var e = a(d[1], c.form), f = e.eq(0), g = f.data("valid_skip") ? f.data("valid_skip") : a.extend({}, this),
            h = e.filter(function () {
                return g.elementValue(this)
            }).length, i = 0 === h || h >= d[0];
        return f.data("valid_skip", g), a(c).data("being_validated") || (e.data("being_validated", !0), e.each(function () {
            g.element(this)
        }), e.data("being_validated", !1)), i
    }, a.validator.format("Please either skip these fields or fill at least {0} of them.")), a.validator.addMethod("stateUS", function (a, b, c) {
        var d, e = "undefined" == typeof c, f = !e && "undefined" != typeof c.caseSensitive && c.caseSensitive,
            g = !e && "undefined" != typeof c.includeTerritories && c.includeTerritories,
            h = !e && "undefined" != typeof c.includeMilitary && c.includeMilitary;
        return d = g || h ? g && h ? "^(A[AEKLPRSZ]|C[AOT]|D[CE]|FL|G[AU]|HI|I[ADLN]|K[SY]|LA|M[ADEINOPST]|N[CDEHJMVY]|O[HKR]|P[AR]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY])$" : g ? "^(A[KLRSZ]|C[AOT]|D[CE]|FL|G[AU]|HI|I[ADLN]|K[SY]|LA|M[ADEINOPST]|N[CDEHJMVY]|O[HKR]|P[AR]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY])$" : "^(A[AEKLPRZ]|C[AOT]|D[CE]|FL|GA|HI|I[ADLN]|K[SY]|LA|M[ADEINOST]|N[CDEHJMVY]|O[HKR]|PA|RI|S[CD]|T[NX]|UT|V[AT]|W[AIVY])$" : "^(A[KLRZ]|C[AOT]|D[CE]|FL|GA|HI|I[ADLN]|K[SY]|LA|M[ADEINOST]|N[CDEHJMVY]|O[HKR]|PA|RI|S[CD]|T[NX]|UT|V[AT]|W[AIVY])$", d = f ? new RegExp(d) : new RegExp(d, "i"), this.optional(b) || d.test(a)
    }, "Please specify a valid state"), a.validator.addMethod("strippedminlength", function (b, c, d) {
        return a(b).text().length >= d
    }, a.validator.format("Please enter at least {0} characters")), a.validator.addMethod("time", function (a, b) {
        return this.optional(b) || /^([01]\d|2[0-3]|[0-9])(:[0-5]\d){1,2}$/.test(a)
    }, "Please enter a valid time, between 00:00 and 23:59"), a.validator.addMethod("time12h", function (a, b) {
        return this.optional(b) || /^((0?[1-9]|1[012])(:[0-5]\d){1,2}(\ ?[AP]M))$/i.test(a)
    }, "Please enter a valid time in 12-hour am/pm format"), a.validator.addMethod("url2", function (a, b) {
        return this.optional(b) || /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)*(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(a)
    }, a.validator.messages.url), a.validator.addMethod("vinUS", function (a) {
        if (17 !== a.length) return !1;
        var b, c, d, e, f, g,
            h = ["A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"],
            i = [1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 7, 9, 2, 3, 4, 5, 6, 7, 8, 9],
            j = [8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2], k = 0;
        for (b = 0; b < 17; b++) {
            if (e = j[b], d = a.slice(b, b + 1), 8 === b && (g = d), isNaN(d)) {
                for (c = 0; c < h.length; c++) if (d.toUpperCase() === h[c]) {
                    d = i[c], d *= e, isNaN(g) && 8 === c && (g = h[c]);
                    break
                }
            } else d *= e;
            k += d
        }
        return f = k % 11, 10 === f && (f = "X"), f === g
    }, "The specified vehicle identification number (VIN) is invalid."), a.validator.addMethod("zipcodeUS", function (a, b) {
        return this.optional(b) || /^\d{5}(-\d{4})?$/.test(a)
    }, "The specified US ZIP Code is invalid"), a.validator.addMethod("ziprange", function (a, b) {
        return this.optional(b) || /^90[2-5]\d\{2\}-\d{4}$/.test(a)
    }, "Your ZIP-code must be in the range 902xx-xxxx to 905xx-xxxx"), a
});
!function (e, t) {
    "object" == typeof exports && "undefined" != typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define(t) : e.moment = t()
}(this, function () {
    "use strict";
    var e, i;

    function c() {
        return e.apply(null, arguments)
    }

    function o(e) {
        return e instanceof Array || "[object Array]" === Object.prototype.toString.call(e)
    }

    function u(e) {
        return null != e && "[object Object]" === Object.prototype.toString.call(e)
    }

    function l(e) {
        return void 0 === e
    }

    function h(e) {
        return "number" == typeof e || "[object Number]" === Object.prototype.toString.call(e)
    }

    function d(e) {
        return e instanceof Date || "[object Date]" === Object.prototype.toString.call(e)
    }

    function f(e, t) {
        var n, s = [];
        for (n = 0; n < e.length; ++n) s.push(t(e[n], n));
        return s
    }

    function m(e, t) {
        return Object.prototype.hasOwnProperty.call(e, t)
    }

    function _(e, t) {
        for (var n in t) m(t, n) && (e[n] = t[n]);
        return m(t, "toString") && (e.toString = t.toString), m(t, "valueOf") && (e.valueOf = t.valueOf), e
    }

    function y(e, t, n, s) {
        return Tt(e, t, n, s, !0).utc()
    }

    function g(e) {
        return null == e._pf && (e._pf = {
            empty: !1,
            unusedTokens: [],
            unusedInput: [],
            overflow: -2,
            charsLeftOver: 0,
            nullInput: !1,
            invalidMonth: null,
            invalidFormat: !1,
            userInvalidated: !1,
            iso: !1,
            parsedDateParts: [],
            meridiem: null,
            rfc2822: !1,
            weekdayMismatch: !1
        }), e._pf
    }

    function v(e) {
        if (null == e._isValid) {
            var t = g(e), n = i.call(t.parsedDateParts, function (e) {
                    return null != e
                }),
                s = !isNaN(e._d.getTime()) && t.overflow < 0 && !t.empty && !t.invalidMonth && !t.invalidWeekday && !t.weekdayMismatch && !t.nullInput && !t.invalidFormat && !t.userInvalidated && (!t.meridiem || t.meridiem && n);
            if (e._strict && (s = s && 0 === t.charsLeftOver && 0 === t.unusedTokens.length && void 0 === t.bigHour), null != Object.isFrozen && Object.isFrozen(e)) return s;
            e._isValid = s
        }
        return e._isValid
    }

    function p(e) {
        var t = y(NaN);
        return null != e ? _(g(t), e) : g(t).userInvalidated = !0, t
    }

    i = Array.prototype.some ? Array.prototype.some : function (e) {
        for (var t = Object(this), n = t.length >>> 0, s = 0; s < n; s++) if (s in t && e.call(this, t[s], s, t)) return !0;
        return !1
    };
    var r = c.momentProperties = [];

    function w(e, t) {
        var n, s, i;
        if (l(t._isAMomentObject) || (e._isAMomentObject = t._isAMomentObject), l(t._i) || (e._i = t._i), l(t._f) || (e._f = t._f), l(t._l) || (e._l = t._l), l(t._strict) || (e._strict = t._strict), l(t._tzm) || (e._tzm = t._tzm), l(t._isUTC) || (e._isUTC = t._isUTC), l(t._offset) || (e._offset = t._offset), l(t._pf) || (e._pf = g(t)), l(t._locale) || (e._locale = t._locale), 0 < r.length) for (n = 0; n < r.length; n++) l(i = t[s = r[n]]) || (e[s] = i);
        return e
    }

    var t = !1;

    function M(e) {
        w(this, e), this._d = new Date(null != e._d ? e._d.getTime() : NaN), this.isValid() || (this._d = new Date(NaN)), !1 === t && (t = !0, c.updateOffset(this), t = !1)
    }

    function k(e) {
        return e instanceof M || null != e && null != e._isAMomentObject
    }

    function S(e) {
        return e < 0 ? Math.ceil(e) || 0 : Math.floor(e)
    }

    function D(e) {
        var t = +e, n = 0;
        return 0 !== t && isFinite(t) && (n = S(t)), n
    }

    function a(e, t, n) {
        var s, i = Math.min(e.length, t.length), r = Math.abs(e.length - t.length), a = 0;
        for (s = 0; s < i; s++) (n && e[s] !== t[s] || !n && D(e[s]) !== D(t[s])) && a++;
        return a + r
    }

    function Y(e) {
        !1 === c.suppressDeprecationWarnings && "undefined" != typeof console && console.warn && console.warn("Deprecation warning: " + e)
    }

    function n(i, r) {
        var a = !0;
        return _(function () {
            if (null != c.deprecationHandler && c.deprecationHandler(null, i), a) {
                for (var e, t = [], n = 0; n < arguments.length; n++) {
                    if (e = "", "object" == typeof arguments[n]) {
                        for (var s in e += "\n[" + n + "] ", arguments[0]) e += s + ": " + arguments[0][s] + ", ";
                        e = e.slice(0, -2)
                    } else e = arguments[n];
                    t.push(e)
                }
                Y(i + "\nArguments: " + Array.prototype.slice.call(t).join("") + "\n" + (new Error).stack), a = !1
            }
            return r.apply(this, arguments)
        }, r)
    }

    var s, O = {};

    function T(e, t) {
        null != c.deprecationHandler && c.deprecationHandler(e, t), O[e] || (Y(t), O[e] = !0)
    }

    function b(e) {
        return e instanceof Function || "[object Function]" === Object.prototype.toString.call(e)
    }

    function x(e, t) {
        var n, s = _({}, e);
        for (n in t) m(t, n) && (u(e[n]) && u(t[n]) ? (s[n] = {}, _(s[n], e[n]), _(s[n], t[n])) : null != t[n] ? s[n] = t[n] : delete s[n]);
        for (n in e) m(e, n) && !m(t, n) && u(e[n]) && (s[n] = _({}, s[n]));
        return s
    }

    function P(e) {
        null != e && this.set(e)
    }

    c.suppressDeprecationWarnings = !1, c.deprecationHandler = null, s = Object.keys ? Object.keys : function (e) {
        var t, n = [];
        for (t in e) m(e, t) && n.push(t);
        return n
    };
    var W = {};

    function C(e, t) {
        var n = e.toLowerCase();
        W[n] = W[n + "s"] = W[t] = e
    }

    function H(e) {
        return "string" == typeof e ? W[e] || W[e.toLowerCase()] : void 0
    }

    function R(e) {
        var t, n, s = {};
        for (n in e) m(e, n) && (t = H(n)) && (s[t] = e[n]);
        return s
    }

    var U = {};

    function F(e, t) {
        U[e] = t
    }

    function L(e, t, n) {
        var s = "" + Math.abs(e), i = t - s.length;
        return (0 <= e ? n ? "+" : "" : "-") + Math.pow(10, Math.max(0, i)).toString().substr(1) + s
    }

    var N = /(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g,
        G = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g, V = {}, E = {};

    function I(e, t, n, s) {
        var i = s;
        "string" == typeof s && (i = function () {
            return this[s]()
        }), e && (E[e] = i), t && (E[t[0]] = function () {
            return L(i.apply(this, arguments), t[1], t[2])
        }), n && (E[n] = function () {
            return this.localeData().ordinal(i.apply(this, arguments), e)
        })
    }

    function A(e, t) {
        return e.isValid() ? (t = j(t, e.localeData()), V[t] = V[t] || function (s) {
            var e, i, t, r = s.match(N);
            for (e = 0, i = r.length; e < i; e++) E[r[e]] ? r[e] = E[r[e]] : r[e] = (t = r[e]).match(/\[[\s\S]/) ? t.replace(/^\[|\]$/g, "") : t.replace(/\\/g, "");
            return function (e) {
                var t, n = "";
                for (t = 0; t < i; t++) n += b(r[t]) ? r[t].call(e, s) : r[t];
                return n
            }
        }(t), V[t](e)) : e.localeData().invalidDate()
    }

    function j(e, t) {
        var n = 5;

        function s(e) {
            return t.longDateFormat(e) || e
        }

        for (G.lastIndex = 0; 0 <= n && G.test(e);) e = e.replace(G, s), G.lastIndex = 0, n -= 1;
        return e
    }

    var Z = /\d/, z = /\d\d/, $ = /\d{3}/, q = /\d{4}/, J = /[+-]?\d{6}/, B = /\d\d?/, Q = /\d\d\d\d?/,
        X = /\d\d\d\d\d\d?/, K = /\d{1,3}/, ee = /\d{1,4}/, te = /[+-]?\d{1,6}/, ne = /\d+/, se = /[+-]?\d+/,
        ie = /Z|[+-]\d\d:?\d\d/gi, re = /Z|[+-]\d\d(?::?\d\d)?/gi,
        ae = /[0-9]{0,256}['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFF07\uFF10-\uFFEF]{1,256}|[\u0600-\u06FF\/]{1,256}(\s*?[\u0600-\u06FF]{1,256}){1,2}/i,
        oe = {};

    function ue(e, n, s) {
        oe[e] = b(n) ? n : function (e, t) {
            return e && s ? s : n
        }
    }

    function le(e, t) {
        return m(oe, e) ? oe[e](t._strict, t._locale) : new RegExp(he(e.replace("\\", "").replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function (e, t, n, s, i) {
            return t || n || s || i
        })))
    }

    function he(e) {
        return e.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&")
    }

    var de = {};

    function ce(e, n) {
        var t, s = n;
        for ("string" == typeof e && (e = [e]), h(n) && (s = function (e, t) {
            t[n] = D(e)
        }), t = 0; t < e.length; t++) de[e[t]] = s
    }

    function fe(e, i) {
        ce(e, function (e, t, n, s) {
            n._w = n._w || {}, i(e, n._w, n, s)
        })
    }

    var me = 0, _e = 1, ye = 2, ge = 3, ve = 4, pe = 5, we = 6, Me = 7, ke = 8;

    function Se(e) {
        return De(e) ? 366 : 365
    }

    function De(e) {
        return e % 4 == 0 && e % 100 != 0 || e % 400 == 0
    }

    I("Y", 0, 0, function () {
        var e = this.year();
        return e <= 9999 ? "" + e : "+" + e
    }), I(0, ["YY", 2], 0, function () {
        return this.year() % 100
    }), I(0, ["YYYY", 4], 0, "year"), I(0, ["YYYYY", 5], 0, "year"), I(0, ["YYYYYY", 6, !0], 0, "year"), C("year", "y"), F("year", 1), ue("Y", se), ue("YY", B, z), ue("YYYY", ee, q), ue("YYYYY", te, J), ue("YYYYYY", te, J), ce(["YYYYY", "YYYYYY"], me), ce("YYYY", function (e, t) {
        t[me] = 2 === e.length ? c.parseTwoDigitYear(e) : D(e)
    }), ce("YY", function (e, t) {
        t[me] = c.parseTwoDigitYear(e)
    }), ce("Y", function (e, t) {
        t[me] = parseInt(e, 10)
    }), c.parseTwoDigitYear = function (e) {
        return D(e) + (68 < D(e) ? 1900 : 2e3)
    };
    var Ye, Oe = Te("FullYear", !0);

    function Te(t, n) {
        return function (e) {
            return null != e ? (xe(this, t, e), c.updateOffset(this, n), this) : be(this, t)
        }
    }

    function be(e, t) {
        return e.isValid() ? e._d["get" + (e._isUTC ? "UTC" : "") + t]() : NaN
    }

    function xe(e, t, n) {
        e.isValid() && !isNaN(n) && ("FullYear" === t && De(e.year()) && 1 === e.month() && 29 === e.date() ? e._d["set" + (e._isUTC ? "UTC" : "") + t](n, e.month(), Pe(n, e.month())) : e._d["set" + (e._isUTC ? "UTC" : "") + t](n))
    }

    function Pe(e, t) {
        if (isNaN(e) || isNaN(t)) return NaN;
        var n, s = (t % (n = 12) + n) % n;
        return e += (t - s) / 12, 1 === s ? De(e) ? 29 : 28 : 31 - s % 7 % 2
    }

    Ye = Array.prototype.indexOf ? Array.prototype.indexOf : function (e) {
        var t;
        for (t = 0; t < this.length; ++t) if (this[t] === e) return t;
        return -1
    }, I("M", ["MM", 2], "Mo", function () {
        return this.month() + 1
    }), I("MMM", 0, 0, function (e) {
        return this.localeData().monthsShort(this, e)
    }), I("MMMM", 0, 0, function (e) {
        return this.localeData().months(this, e)
    }), C("month", "M"), F("month", 8), ue("M", B), ue("MM", B, z), ue("MMM", function (e, t) {
        return t.monthsShortRegex(e)
    }), ue("MMMM", function (e, t) {
        return t.monthsRegex(e)
    }), ce(["M", "MM"], function (e, t) {
        t[_e] = D(e) - 1
    }), ce(["MMM", "MMMM"], function (e, t, n, s) {
        var i = n._locale.monthsParse(e, s, n._strict);
        null != i ? t[_e] = i : g(n).invalidMonth = e
    });
    var We = /D[oD]?(\[[^\[\]]*\]|\s)+MMMM?/,
        Ce = "January_February_March_April_May_June_July_August_September_October_November_December".split("_");
    var He = "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_");

    function Re(e, t) {
        var n;
        if (!e.isValid()) return e;
        if ("string" == typeof t) if (/^\d+$/.test(t)) t = D(t); else if (!h(t = e.localeData().monthsParse(t))) return e;
        return n = Math.min(e.date(), Pe(e.year(), t)), e._d["set" + (e._isUTC ? "UTC" : "") + "Month"](t, n), e
    }

    function Ue(e) {
        return null != e ? (Re(this, e), c.updateOffset(this, !0), this) : be(this, "Month")
    }

    var Fe = ae;
    var Le = ae;

    function Ne() {
        function e(e, t) {
            return t.length - e.length
        }

        var t, n, s = [], i = [], r = [];
        for (t = 0; t < 12; t++) n = y([2e3, t]), s.push(this.monthsShort(n, "")), i.push(this.months(n, "")), r.push(this.months(n, "")), r.push(this.monthsShort(n, ""));
        for (s.sort(e), i.sort(e), r.sort(e), t = 0; t < 12; t++) s[t] = he(s[t]), i[t] = he(i[t]);
        for (t = 0; t < 24; t++) r[t] = he(r[t]);
        this._monthsRegex = new RegExp("^(" + r.join("|") + ")", "i"), this._monthsShortRegex = this._monthsRegex, this._monthsStrictRegex = new RegExp("^(" + i.join("|") + ")", "i"), this._monthsShortStrictRegex = new RegExp("^(" + s.join("|") + ")", "i")
    }

    function Ge(e) {
        var t;
        if (e < 100 && 0 <= e) {
            var n = Array.prototype.slice.call(arguments);
            n[0] = e + 400, t = new Date(Date.UTC.apply(null, n)), isFinite(t.getUTCFullYear()) && t.setUTCFullYear(e)
        } else t = new Date(Date.UTC.apply(null, arguments));
        return t
    }

    function Ve(e, t, n) {
        var s = 7 + t - n;
        return -((7 + Ge(e, 0, s).getUTCDay() - t) % 7) + s - 1
    }

    function Ee(e, t, n, s, i) {
        var r, a, o = 1 + 7 * (t - 1) + (7 + n - s) % 7 + Ve(e, s, i);
        return a = o <= 0 ? Se(r = e - 1) + o : o > Se(e) ? (r = e + 1, o - Se(e)) : (r = e, o), {year: r, dayOfYear: a}
    }

    function Ie(e, t, n) {
        var s, i, r = Ve(e.year(), t, n), a = Math.floor((e.dayOfYear() - r - 1) / 7) + 1;
        return a < 1 ? s = a + Ae(i = e.year() - 1, t, n) : a > Ae(e.year(), t, n) ? (s = a - Ae(e.year(), t, n), i = e.year() + 1) : (i = e.year(), s = a), {
            week: s,
            year: i
        }
    }

    function Ae(e, t, n) {
        var s = Ve(e, t, n), i = Ve(e + 1, t, n);
        return (Se(e) - s + i) / 7
    }

    I("w", ["ww", 2], "wo", "week"), I("W", ["WW", 2], "Wo", "isoWeek"), C("week", "w"), C("isoWeek", "W"), F("week", 5), F("isoWeek", 5), ue("w", B), ue("ww", B, z), ue("W", B), ue("WW", B, z), fe(["w", "ww", "W", "WW"], function (e, t, n, s) {
        t[s.substr(0, 1)] = D(e)
    });

    function je(e, t) {
        return e.slice(t, 7).concat(e.slice(0, t))
    }

    I("d", 0, "do", "day"), I("dd", 0, 0, function (e) {
        return this.localeData().weekdaysMin(this, e)
    }), I("ddd", 0, 0, function (e) {
        return this.localeData().weekdaysShort(this, e)
    }), I("dddd", 0, 0, function (e) {
        return this.localeData().weekdays(this, e)
    }), I("e", 0, 0, "weekday"), I("E", 0, 0, "isoWeekday"), C("day", "d"), C("weekday", "e"), C("isoWeekday", "E"), F("day", 11), F("weekday", 11), F("isoWeekday", 11), ue("d", B), ue("e", B), ue("E", B), ue("dd", function (e, t) {
        return t.weekdaysMinRegex(e)
    }), ue("ddd", function (e, t) {
        return t.weekdaysShortRegex(e)
    }), ue("dddd", function (e, t) {
        return t.weekdaysRegex(e)
    }), fe(["dd", "ddd", "dddd"], function (e, t, n, s) {
        var i = n._locale.weekdaysParse(e, s, n._strict);
        null != i ? t.d = i : g(n).invalidWeekday = e
    }), fe(["d", "e", "E"], function (e, t, n, s) {
        t[s] = D(e)
    });
    var Ze = "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_");
    var ze = "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_");
    var $e = "Su_Mo_Tu_We_Th_Fr_Sa".split("_");
    var qe = ae;
    var Je = ae;
    var Be = ae;

    function Qe() {
        function e(e, t) {
            return t.length - e.length
        }

        var t, n, s, i, r, a = [], o = [], u = [], l = [];
        for (t = 0; t < 7; t++) n = y([2e3, 1]).day(t), s = this.weekdaysMin(n, ""), i = this.weekdaysShort(n, ""), r = this.weekdays(n, ""), a.push(s), o.push(i), u.push(r), l.push(s), l.push(i), l.push(r);
        for (a.sort(e), o.sort(e), u.sort(e), l.sort(e), t = 0; t < 7; t++) o[t] = he(o[t]), u[t] = he(u[t]), l[t] = he(l[t]);
        this._weekdaysRegex = new RegExp("^(" + l.join("|") + ")", "i"), this._weekdaysShortRegex = this._weekdaysRegex, this._weekdaysMinRegex = this._weekdaysRegex, this._weekdaysStrictRegex = new RegExp("^(" + u.join("|") + ")", "i"), this._weekdaysShortStrictRegex = new RegExp("^(" + o.join("|") + ")", "i"), this._weekdaysMinStrictRegex = new RegExp("^(" + a.join("|") + ")", "i")
    }

    function Xe() {
        return this.hours() % 12 || 12
    }

    function Ke(e, t) {
        I(e, 0, 0, function () {
            return this.localeData().meridiem(this.hours(), this.minutes(), t)
        })
    }

    function et(e, t) {
        return t._meridiemParse
    }

    I("H", ["HH", 2], 0, "hour"), I("h", ["hh", 2], 0, Xe), I("k", ["kk", 2], 0, function () {
        return this.hours() || 24
    }), I("hmm", 0, 0, function () {
        return "" + Xe.apply(this) + L(this.minutes(), 2)
    }), I("hmmss", 0, 0, function () {
        return "" + Xe.apply(this) + L(this.minutes(), 2) + L(this.seconds(), 2)
    }), I("Hmm", 0, 0, function () {
        return "" + this.hours() + L(this.minutes(), 2)
    }), I("Hmmss", 0, 0, function () {
        return "" + this.hours() + L(this.minutes(), 2) + L(this.seconds(), 2)
    }), Ke("a", !0), Ke("A", !1), C("hour", "h"), F("hour", 13), ue("a", et), ue("A", et), ue("H", B), ue("h", B), ue("k", B), ue("HH", B, z), ue("hh", B, z), ue("kk", B, z), ue("hmm", Q), ue("hmmss", X), ue("Hmm", Q), ue("Hmmss", X), ce(["H", "HH"], ge), ce(["k", "kk"], function (e, t, n) {
        var s = D(e);
        t[ge] = 24 === s ? 0 : s
    }), ce(["a", "A"], function (e, t, n) {
        n._isPm = n._locale.isPM(e), n._meridiem = e
    }), ce(["h", "hh"], function (e, t, n) {
        t[ge] = D(e), g(n).bigHour = !0
    }), ce("hmm", function (e, t, n) {
        var s = e.length - 2;
        t[ge] = D(e.substr(0, s)), t[ve] = D(e.substr(s)), g(n).bigHour = !0
    }), ce("hmmss", function (e, t, n) {
        var s = e.length - 4, i = e.length - 2;
        t[ge] = D(e.substr(0, s)), t[ve] = D(e.substr(s, 2)), t[pe] = D(e.substr(i)), g(n).bigHour = !0
    }), ce("Hmm", function (e, t, n) {
        var s = e.length - 2;
        t[ge] = D(e.substr(0, s)), t[ve] = D(e.substr(s))
    }), ce("Hmmss", function (e, t, n) {
        var s = e.length - 4, i = e.length - 2;
        t[ge] = D(e.substr(0, s)), t[ve] = D(e.substr(s, 2)), t[pe] = D(e.substr(i))
    });
    var tt, nt = Te("Hours", !0), st = {
        calendar: {
            sameDay: "[Today at] LT",
            nextDay: "[Tomorrow at] LT",
            nextWeek: "dddd [at] LT",
            lastDay: "[Yesterday at] LT",
            lastWeek: "[Last] dddd [at] LT",
            sameElse: "L"
        },
        longDateFormat: {
            LTS: "h:mm:ss A",
            LT: "h:mm A",
            L: "MM/DD/YYYY",
            LL: "MMMM D, YYYY",
            LLL: "MMMM D, YYYY h:mm A",
            LLLL: "dddd, MMMM D, YYYY h:mm A"
        },
        invalidDate: "Invalid date",
        ordinal: "%d",
        dayOfMonthOrdinalParse: /\d{1,2}/,
        relativeTime: {
            future: "in %s",
            past: "%s ago",
            s: "a few seconds",
            ss: "%d seconds",
            m: "a minute",
            mm: "%d minutes",
            h: "an hour",
            hh: "%d hours",
            d: "a day",
            dd: "%d days",
            M: "a month",
            MM: "%d months",
            y: "a year",
            yy: "%d years"
        },
        months: Ce,
        monthsShort: He,
        week: {dow: 0, doy: 6},
        weekdays: Ze,
        weekdaysMin: $e,
        weekdaysShort: ze,
        meridiemParse: /[ap]\.?m?\.?/i
    }, it = {}, rt = {};

    function at(e) {
        return e ? e.toLowerCase().replace("_", "-") : e
    }

    function ot(e) {
        var t = null;
        if (!it[e] && "undefined" != typeof module && module && module.exports) try {
            t = tt._abbr, require("./locale/" + e), ut(t)
        } catch (e) {
        }
        return it[e]
    }

    function ut(e, t) {
        var n;
        return e && ((n = l(t) ? ht(e) : lt(e, t)) ? tt = n : "undefined" != typeof console && console.warn && console.warn("Locale " + e + " not found. Did you forget to load it?")), tt._abbr
    }

    function lt(e, t) {
        if (null === t) return delete it[e], null;
        var n, s = st;
        if (t.abbr = e, null != it[e]) T("defineLocaleOverride", "use moment.updateLocale(localeName, config) to change an existing locale. moment.defineLocale(localeName, config) should only be used for creating a new locale See http://momentjs.com/guides/#/warnings/define-locale/ for more info."), s = it[e]._config; else if (null != t.parentLocale) if (null != it[t.parentLocale]) s = it[t.parentLocale]._config; else {
            if (null == (n = ot(t.parentLocale))) return rt[t.parentLocale] || (rt[t.parentLocale] = []), rt[t.parentLocale].push({
                name: e,
                config: t
            }), null;
            s = n._config
        }
        return it[e] = new P(x(s, t)), rt[e] && rt[e].forEach(function (e) {
            lt(e.name, e.config)
        }), ut(e), it[e]
    }

    function ht(e) {
        var t;
        if (e && e._locale && e._locale._abbr && (e = e._locale._abbr), !e) return tt;
        if (!o(e)) {
            if (t = ot(e)) return t;
            e = [e]
        }
        return function (e) {
            for (var t, n, s, i, r = 0; r < e.length;) {
                for (t = (i = at(e[r]).split("-")).length, n = (n = at(e[r + 1])) ? n.split("-") : null; 0 < t;) {
                    if (s = ot(i.slice(0, t).join("-"))) return s;
                    if (n && n.length >= t && a(i, n, !0) >= t - 1) break;
                    t--
                }
                r++
            }
            return tt
        }(e)
    }

    function dt(e) {
        var t, n = e._a;
        return n && -2 === g(e).overflow && (t = n[_e] < 0 || 11 < n[_e] ? _e : n[ye] < 1 || n[ye] > Pe(n[me], n[_e]) ? ye : n[ge] < 0 || 24 < n[ge] || 24 === n[ge] && (0 !== n[ve] || 0 !== n[pe] || 0 !== n[we]) ? ge : n[ve] < 0 || 59 < n[ve] ? ve : n[pe] < 0 || 59 < n[pe] ? pe : n[we] < 0 || 999 < n[we] ? we : -1, g(e)._overflowDayOfYear && (t < me || ye < t) && (t = ye), g(e)._overflowWeeks && -1 === t && (t = Me), g(e)._overflowWeekday && -1 === t && (t = ke), g(e).overflow = t), e
    }

    function ct(e, t, n) {
        return null != e ? e : null != t ? t : n
    }

    function ft(e) {
        var t, n, s, i, r, a = [];
        if (!e._d) {
            var o, u;
            for (o = e, u = new Date(c.now()), s = o._useUTC ? [u.getUTCFullYear(), u.getUTCMonth(), u.getUTCDate()] : [u.getFullYear(), u.getMonth(), u.getDate()], e._w && null == e._a[ye] && null == e._a[_e] && function (e) {
                var t, n, s, i, r, a, o, u;
                if (null != (t = e._w).GG || null != t.W || null != t.E) r = 1, a = 4, n = ct(t.GG, e._a[me], Ie(bt(), 1, 4).year), s = ct(t.W, 1), ((i = ct(t.E, 1)) < 1 || 7 < i) && (u = !0); else {
                    r = e._locale._week.dow, a = e._locale._week.doy;
                    var l = Ie(bt(), r, a);
                    n = ct(t.gg, e._a[me], l.year), s = ct(t.w, l.week), null != t.d ? ((i = t.d) < 0 || 6 < i) && (u = !0) : null != t.e ? (i = t.e + r, (t.e < 0 || 6 < t.e) && (u = !0)) : i = r
                }
                s < 1 || s > Ae(n, r, a) ? g(e)._overflowWeeks = !0 : null != u ? g(e)._overflowWeekday = !0 : (o = Ee(n, s, i, r, a), e._a[me] = o.year, e._dayOfYear = o.dayOfYear)
            }(e), null != e._dayOfYear && (r = ct(e._a[me], s[me]), (e._dayOfYear > Se(r) || 0 === e._dayOfYear) && (g(e)._overflowDayOfYear = !0), n = Ge(r, 0, e._dayOfYear), e._a[_e] = n.getUTCMonth(), e._a[ye] = n.getUTCDate()), t = 0; t < 3 && null == e._a[t]; ++t) e._a[t] = a[t] = s[t];
            for (; t < 7; t++) e._a[t] = a[t] = null == e._a[t] ? 2 === t ? 1 : 0 : e._a[t];
            24 === e._a[ge] && 0 === e._a[ve] && 0 === e._a[pe] && 0 === e._a[we] && (e._nextDay = !0, e._a[ge] = 0), e._d = (e._useUTC ? Ge : function (e, t, n, s, i, r, a) {
                var o;
                return e < 100 && 0 <= e ? (o = new Date(e + 400, t, n, s, i, r, a), isFinite(o.getFullYear()) && o.setFullYear(e)) : o = new Date(e, t, n, s, i, r, a), o
            }).apply(null, a), i = e._useUTC ? e._d.getUTCDay() : e._d.getDay(), null != e._tzm && e._d.setUTCMinutes(e._d.getUTCMinutes() - e._tzm), e._nextDay && (e._a[ge] = 24), e._w && void 0 !== e._w.d && e._w.d !== i && (g(e).weekdayMismatch = !0)
        }
    }

    var mt = /^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/,
        _t = /^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/,
        yt = /Z|[+-]\d\d(?::?\d\d)?/,
        gt = [["YYYYYY-MM-DD", /[+-]\d{6}-\d\d-\d\d/], ["YYYY-MM-DD", /\d{4}-\d\d-\d\d/], ["GGGG-[W]WW-E", /\d{4}-W\d\d-\d/], ["GGGG-[W]WW", /\d{4}-W\d\d/, !1], ["YYYY-DDD", /\d{4}-\d{3}/], ["YYYY-MM", /\d{4}-\d\d/, !1], ["YYYYYYMMDD", /[+-]\d{10}/], ["YYYYMMDD", /\d{8}/], ["GGGG[W]WWE", /\d{4}W\d{3}/], ["GGGG[W]WW", /\d{4}W\d{2}/, !1], ["YYYYDDD", /\d{7}/]],
        vt = [["HH:mm:ss.SSSS", /\d\d:\d\d:\d\d\.\d+/], ["HH:mm:ss,SSSS", /\d\d:\d\d:\d\d,\d+/], ["HH:mm:ss", /\d\d:\d\d:\d\d/], ["HH:mm", /\d\d:\d\d/], ["HHmmss.SSSS", /\d\d\d\d\d\d\.\d+/], ["HHmmss,SSSS", /\d\d\d\d\d\d,\d+/], ["HHmmss", /\d\d\d\d\d\d/], ["HHmm", /\d\d\d\d/], ["HH", /\d\d/]],
        pt = /^\/?Date\((\-?\d+)/i;

    function wt(e) {
        var t, n, s, i, r, a, o = e._i, u = mt.exec(o) || _t.exec(o);
        if (u) {
            for (g(e).iso = !0, t = 0, n = gt.length; t < n; t++) if (gt[t][1].exec(u[1])) {
                i = gt[t][0], s = !1 !== gt[t][2];
                break
            }
            if (null == i) return void (e._isValid = !1);
            if (u[3]) {
                for (t = 0, n = vt.length; t < n; t++) if (vt[t][1].exec(u[3])) {
                    r = (u[2] || " ") + vt[t][0];
                    break
                }
                if (null == r) return void (e._isValid = !1)
            }
            if (!s && null != r) return void (e._isValid = !1);
            if (u[4]) {
                if (!yt.exec(u[4])) return void (e._isValid = !1);
                a = "Z"
            }
            e._f = i + (r || "") + (a || ""), Yt(e)
        } else e._isValid = !1
    }

    var Mt = /^(?:(Mon|Tue|Wed|Thu|Fri|Sat|Sun),?\s)?(\d{1,2})\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s(\d{2,4})\s(\d\d):(\d\d)(?::(\d\d))?\s(?:(UT|GMT|[ECMP][SD]T)|([Zz])|([+-]\d{4}))$/;

    function kt(e, t, n, s, i, r) {
        var a = [function (e) {
            var t = parseInt(e, 10);
            {
                if (t <= 49) return 2e3 + t;
                if (t <= 999) return 1900 + t
            }
            return t
        }(e), He.indexOf(t), parseInt(n, 10), parseInt(s, 10), parseInt(i, 10)];
        return r && a.push(parseInt(r, 10)), a
    }

    var St = {UT: 0, GMT: 0, EDT: -240, EST: -300, CDT: -300, CST: -360, MDT: -360, MST: -420, PDT: -420, PST: -480};

    function Dt(e) {
        var t, n, s,
            i = Mt.exec(e._i.replace(/\([^)]*\)|[\n\t]/g, " ").replace(/(\s\s+)/g, " ").replace(/^\s\s*/, "").replace(/\s\s*$/, ""));
        if (i) {
            var r = kt(i[4], i[3], i[2], i[5], i[6], i[7]);
            if (t = i[1], n = r, s = e, t && ze.indexOf(t) !== new Date(n[0], n[1], n[2]).getDay() && (g(s).weekdayMismatch = !0, !(s._isValid = !1))) return;
            e._a = r, e._tzm = function (e, t, n) {
                if (e) return St[e];
                if (t) return 0;
                var s = parseInt(n, 10), i = s % 100;
                return (s - i) / 100 * 60 + i
            }(i[8], i[9], i[10]), e._d = Ge.apply(null, e._a), e._d.setUTCMinutes(e._d.getUTCMinutes() - e._tzm), g(e).rfc2822 = !0
        } else e._isValid = !1
    }

    function Yt(e) {
        if (e._f !== c.ISO_8601) if (e._f !== c.RFC_2822) {
            e._a = [], g(e).empty = !0;
            var t, n, s, i, r, a, o, u, l = "" + e._i, h = l.length, d = 0;
            for (s = j(e._f, e._locale).match(N) || [], t = 0; t < s.length; t++) i = s[t], (n = (l.match(le(i, e)) || [])[0]) && (0 < (r = l.substr(0, l.indexOf(n))).length && g(e).unusedInput.push(r), l = l.slice(l.indexOf(n) + n.length), d += n.length), E[i] ? (n ? g(e).empty = !1 : g(e).unusedTokens.push(i), a = i, u = e, null != (o = n) && m(de, a) && de[a](o, u._a, u, a)) : e._strict && !n && g(e).unusedTokens.push(i);
            g(e).charsLeftOver = h - d, 0 < l.length && g(e).unusedInput.push(l), e._a[ge] <= 12 && !0 === g(e).bigHour && 0 < e._a[ge] && (g(e).bigHour = void 0), g(e).parsedDateParts = e._a.slice(0), g(e).meridiem = e._meridiem, e._a[ge] = function (e, t, n) {
                var s;
                if (null == n) return t;
                return null != e.meridiemHour ? e.meridiemHour(t, n) : (null != e.isPM && ((s = e.isPM(n)) && t < 12 && (t += 12), s || 12 !== t || (t = 0)), t)
            }(e._locale, e._a[ge], e._meridiem), ft(e), dt(e)
        } else Dt(e); else wt(e)
    }

    function Ot(e) {
        var t, n, s, i, r = e._i, a = e._f;
        return e._locale = e._locale || ht(e._l), null === r || void 0 === a && "" === r ? p({nullInput: !0}) : ("string" == typeof r && (e._i = r = e._locale.preparse(r)), k(r) ? new M(dt(r)) : (d(r) ? e._d = r : o(a) ? function (e) {
            var t, n, s, i, r;
            if (0 === e._f.length) return g(e).invalidFormat = !0, e._d = new Date(NaN);
            for (i = 0; i < e._f.length; i++) r = 0, t = w({}, e), null != e._useUTC && (t._useUTC = e._useUTC), t._f = e._f[i], Yt(t), v(t) && (r += g(t).charsLeftOver, r += 10 * g(t).unusedTokens.length, g(t).score = r, (null == s || r < s) && (s = r, n = t));
            _(e, n || t)
        }(e) : a ? Yt(e) : l(n = (t = e)._i) ? t._d = new Date(c.now()) : d(n) ? t._d = new Date(n.valueOf()) : "string" == typeof n ? (s = t, null === (i = pt.exec(s._i)) ? (wt(s), !1 === s._isValid && (delete s._isValid, Dt(s), !1 === s._isValid && (delete s._isValid, c.createFromInputFallback(s)))) : s._d = new Date(+i[1])) : o(n) ? (t._a = f(n.slice(0), function (e) {
            return parseInt(e, 10)
        }), ft(t)) : u(n) ? function (e) {
            if (!e._d) {
                var t = R(e._i);
                e._a = f([t.year, t.month, t.day || t.date, t.hour, t.minute, t.second, t.millisecond], function (e) {
                    return e && parseInt(e, 10)
                }), ft(e)
            }
        }(t) : h(n) ? t._d = new Date(n) : c.createFromInputFallback(t), v(e) || (e._d = null), e))
    }

    function Tt(e, t, n, s, i) {
        var r, a = {};
        return !0 !== n && !1 !== n || (s = n, n = void 0), (u(e) && function (e) {
            if (Object.getOwnPropertyNames) return 0 === Object.getOwnPropertyNames(e).length;
            var t;
            for (t in e) if (e.hasOwnProperty(t)) return !1;
            return !0
        }(e) || o(e) && 0 === e.length) && (e = void 0), a._isAMomentObject = !0, a._useUTC = a._isUTC = i, a._l = n, a._i = e, a._f = t, a._strict = s, (r = new M(dt(Ot(a))))._nextDay && (r.add(1, "d"), r._nextDay = void 0), r
    }

    function bt(e, t, n, s) {
        return Tt(e, t, n, s, !1)
    }

    c.createFromInputFallback = n("value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), which is not reliable across all browsers and versions. Non RFC2822/ISO date formats are discouraged and will be removed in an upcoming major release. Please refer to http://momentjs.com/guides/#/warnings/js-date/ for more info.", function (e) {
        e._d = new Date(e._i + (e._useUTC ? " UTC" : ""))
    }), c.ISO_8601 = function () {
    }, c.RFC_2822 = function () {
    };
    var xt = n("moment().min is deprecated, use moment.max instead. http://momentjs.com/guides/#/warnings/min-max/", function () {
            var e = bt.apply(null, arguments);
            return this.isValid() && e.isValid() ? e < this ? this : e : p()
        }),
        Pt = n("moment().max is deprecated, use moment.min instead. http://momentjs.com/guides/#/warnings/min-max/", function () {
            var e = bt.apply(null, arguments);
            return this.isValid() && e.isValid() ? this < e ? this : e : p()
        });

    function Wt(e, t) {
        var n, s;
        if (1 === t.length && o(t[0]) && (t = t[0]), !t.length) return bt();
        for (n = t[0], s = 1; s < t.length; ++s) t[s].isValid() && !t[s][e](n) || (n = t[s]);
        return n
    }

    var Ct = ["year", "quarter", "month", "week", "day", "hour", "minute", "second", "millisecond"];

    function Ht(e) {
        var t = R(e), n = t.year || 0, s = t.quarter || 0, i = t.month || 0, r = t.week || t.isoWeek || 0,
            a = t.day || 0, o = t.hour || 0, u = t.minute || 0, l = t.second || 0, h = t.millisecond || 0;
        this._isValid = function (e) {
            for (var t in e) if (-1 === Ye.call(Ct, t) || null != e[t] && isNaN(e[t])) return !1;
            for (var n = !1, s = 0; s < Ct.length; ++s) if (e[Ct[s]]) {
                if (n) return !1;
                parseFloat(e[Ct[s]]) !== D(e[Ct[s]]) && (n = !0)
            }
            return !0
        }(t), this._milliseconds = +h + 1e3 * l + 6e4 * u + 1e3 * o * 60 * 60, this._days = +a + 7 * r, this._months = +i + 3 * s + 12 * n, this._data = {}, this._locale = ht(), this._bubble()
    }

    function Rt(e) {
        return e instanceof Ht
    }

    function Ut(e) {
        return e < 0 ? -1 * Math.round(-1 * e) : Math.round(e)
    }

    function Ft(e, n) {
        I(e, 0, 0, function () {
            var e = this.utcOffset(), t = "+";
            return e < 0 && (e = -e, t = "-"), t + L(~~(e / 60), 2) + n + L(~~e % 60, 2)
        })
    }

    Ft("Z", ":"), Ft("ZZ", ""), ue("Z", re), ue("ZZ", re), ce(["Z", "ZZ"], function (e, t, n) {
        n._useUTC = !0, n._tzm = Nt(re, e)
    });
    var Lt = /([\+\-]|\d\d)/gi;

    function Nt(e, t) {
        var n = (t || "").match(e);
        if (null === n) return null;
        var s = ((n[n.length - 1] || []) + "").match(Lt) || ["-", 0, 0], i = 60 * s[1] + D(s[2]);
        return 0 === i ? 0 : "+" === s[0] ? i : -i
    }

    function Gt(e, t) {
        var n, s;
        return t._isUTC ? (n = t.clone(), s = (k(e) || d(e) ? e.valueOf() : bt(e).valueOf()) - n.valueOf(), n._d.setTime(n._d.valueOf() + s), c.updateOffset(n, !1), n) : bt(e).local()
    }

    function Vt(e) {
        return 15 * -Math.round(e._d.getTimezoneOffset() / 15)
    }

    function Et() {
        return !!this.isValid() && (this._isUTC && 0 === this._offset)
    }

    c.updateOffset = function () {
    };
    var It = /^(\-|\+)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)(\.\d*)?)?$/,
        At = /^(-|\+)?P(?:([-+]?[0-9,.]*)Y)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)W)?(?:([-+]?[0-9,.]*)D)?(?:T(?:([-+]?[0-9,.]*)H)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)S)?)?$/;

    function jt(e, t) {
        var n, s, i, r = e, a = null;
        return Rt(e) ? r = {
            ms: e._milliseconds,
            d: e._days,
            M: e._months
        } : h(e) ? (r = {}, t ? r[t] = e : r.milliseconds = e) : (a = It.exec(e)) ? (n = "-" === a[1] ? -1 : 1, r = {
            y: 0,
            d: D(a[ye]) * n,
            h: D(a[ge]) * n,
            m: D(a[ve]) * n,
            s: D(a[pe]) * n,
            ms: D(Ut(1e3 * a[we])) * n
        }) : (a = At.exec(e)) ? (n = "-" === a[1] ? -1 : 1, r = {
            y: Zt(a[2], n),
            M: Zt(a[3], n),
            w: Zt(a[4], n),
            d: Zt(a[5], n),
            h: Zt(a[6], n),
            m: Zt(a[7], n),
            s: Zt(a[8], n)
        }) : null == r ? r = {} : "object" == typeof r && ("from" in r || "to" in r) && (i = function (e, t) {
            var n;
            if (!e.isValid() || !t.isValid()) return {milliseconds: 0, months: 0};
            t = Gt(t, e), e.isBefore(t) ? n = zt(e, t) : ((n = zt(t, e)).milliseconds = -n.milliseconds, n.months = -n.months);
            return n
        }(bt(r.from), bt(r.to)), (r = {}).ms = i.milliseconds, r.M = i.months), s = new Ht(r), Rt(e) && m(e, "_locale") && (s._locale = e._locale), s
    }

    function Zt(e, t) {
        var n = e && parseFloat(e.replace(",", "."));
        return (isNaN(n) ? 0 : n) * t
    }

    function zt(e, t) {
        var n = {};
        return n.months = t.month() - e.month() + 12 * (t.year() - e.year()), e.clone().add(n.months, "M").isAfter(t) && --n.months, n.milliseconds = +t - +e.clone().add(n.months, "M"), n
    }

    function $t(s, i) {
        return function (e, t) {
            var n;
            return null === t || isNaN(+t) || (T(i, "moment()." + i + "(period, number) is deprecated. Please use moment()." + i + "(number, period). See http://momentjs.com/guides/#/warnings/add-inverted-param/ for more info."), n = e, e = t, t = n), qt(this, jt(e = "string" == typeof e ? +e : e, t), s), this
        }
    }

    function qt(e, t, n, s) {
        var i = t._milliseconds, r = Ut(t._days), a = Ut(t._months);
        e.isValid() && (s = null == s || s, a && Re(e, be(e, "Month") + a * n), r && xe(e, "Date", be(e, "Date") + r * n), i && e._d.setTime(e._d.valueOf() + i * n), s && c.updateOffset(e, r || a))
    }

    jt.fn = Ht.prototype, jt.invalid = function () {
        return jt(NaN)
    };
    var Jt = $t(1, "add"), Bt = $t(-1, "subtract");

    function Qt(e, t) {
        var n = 12 * (t.year() - e.year()) + (t.month() - e.month()), s = e.clone().add(n, "months");
        return -(n + (t - s < 0 ? (t - s) / (s - e.clone().add(n - 1, "months")) : (t - s) / (e.clone().add(n + 1, "months") - s))) || 0
    }

    function Xt(e) {
        var t;
        return void 0 === e ? this._locale._abbr : (null != (t = ht(e)) && (this._locale = t), this)
    }

    c.defaultFormat = "YYYY-MM-DDTHH:mm:ssZ", c.defaultFormatUtc = "YYYY-MM-DDTHH:mm:ss[Z]";
    var Kt = n("moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.", function (e) {
        return void 0 === e ? this.localeData() : this.locale(e)
    });

    function en() {
        return this._locale
    }

    var tn = 126227808e5;

    function nn(e, t) {
        return (e % t + t) % t
    }

    function sn(e, t, n) {
        return e < 100 && 0 <= e ? new Date(e + 400, t, n) - tn : new Date(e, t, n).valueOf()
    }

    function rn(e, t, n) {
        return e < 100 && 0 <= e ? Date.UTC(e + 400, t, n) - tn : Date.UTC(e, t, n)
    }

    function an(e, t) {
        I(0, [e, e.length], 0, t)
    }

    function on(e, t, n, s, i) {
        var r;
        return null == e ? Ie(this, s, i).year : ((r = Ae(e, s, i)) < t && (t = r), function (e, t, n, s, i) {
            var r = Ee(e, t, n, s, i), a = Ge(r.year, 0, r.dayOfYear);
            return this.year(a.getUTCFullYear()), this.month(a.getUTCMonth()), this.date(a.getUTCDate()), this
        }.call(this, e, t, n, s, i))
    }

    I(0, ["gg", 2], 0, function () {
        return this.weekYear() % 100
    }), I(0, ["GG", 2], 0, function () {
        return this.isoWeekYear() % 100
    }), an("gggg", "weekYear"), an("ggggg", "weekYear"), an("GGGG", "isoWeekYear"), an("GGGGG", "isoWeekYear"), C("weekYear", "gg"), C("isoWeekYear", "GG"), F("weekYear", 1), F("isoWeekYear", 1), ue("G", se), ue("g", se), ue("GG", B, z), ue("gg", B, z), ue("GGGG", ee, q), ue("gggg", ee, q), ue("GGGGG", te, J), ue("ggggg", te, J), fe(["gggg", "ggggg", "GGGG", "GGGGG"], function (e, t, n, s) {
        t[s.substr(0, 2)] = D(e)
    }), fe(["gg", "GG"], function (e, t, n, s) {
        t[s] = c.parseTwoDigitYear(e)
    }), I("Q", 0, "Qo", "quarter"), C("quarter", "Q"), F("quarter", 7), ue("Q", Z), ce("Q", function (e, t) {
        t[_e] = 3 * (D(e) - 1)
    }), I("D", ["DD", 2], "Do", "date"), C("date", "D"), F("date", 9), ue("D", B), ue("DD", B, z), ue("Do", function (e, t) {
        return e ? t._dayOfMonthOrdinalParse || t._ordinalParse : t._dayOfMonthOrdinalParseLenient
    }), ce(["D", "DD"], ye), ce("Do", function (e, t) {
        t[ye] = D(e.match(B)[0])
    });
    var un = Te("Date", !0);
    I("DDD", ["DDDD", 3], "DDDo", "dayOfYear"), C("dayOfYear", "DDD"), F("dayOfYear", 4), ue("DDD", K), ue("DDDD", $), ce(["DDD", "DDDD"], function (e, t, n) {
        n._dayOfYear = D(e)
    }), I("m", ["mm", 2], 0, "minute"), C("minute", "m"), F("minute", 14), ue("m", B), ue("mm", B, z), ce(["m", "mm"], ve);
    var ln = Te("Minutes", !1);
    I("s", ["ss", 2], 0, "second"), C("second", "s"), F("second", 15), ue("s", B), ue("ss", B, z), ce(["s", "ss"], pe);
    var hn, dn = Te("Seconds", !1);
    for (I("S", 0, 0, function () {
        return ~~(this.millisecond() / 100)
    }), I(0, ["SS", 2], 0, function () {
        return ~~(this.millisecond() / 10)
    }), I(0, ["SSS", 3], 0, "millisecond"), I(0, ["SSSS", 4], 0, function () {
        return 10 * this.millisecond()
    }), I(0, ["SSSSS", 5], 0, function () {
        return 100 * this.millisecond()
    }), I(0, ["SSSSSS", 6], 0, function () {
        return 1e3 * this.millisecond()
    }), I(0, ["SSSSSSS", 7], 0, function () {
        return 1e4 * this.millisecond()
    }), I(0, ["SSSSSSSS", 8], 0, function () {
        return 1e5 * this.millisecond()
    }), I(0, ["SSSSSSSSS", 9], 0, function () {
        return 1e6 * this.millisecond()
    }), C("millisecond", "ms"), F("millisecond", 16), ue("S", K, Z), ue("SS", K, z), ue("SSS", K, $), hn = "SSSS"; hn.length <= 9; hn += "S") ue(hn, ne);

    function cn(e, t) {
        t[we] = D(1e3 * ("0." + e))
    }

    for (hn = "S"; hn.length <= 9; hn += "S") ce(hn, cn);
    var fn = Te("Milliseconds", !1);
    I("z", 0, 0, "zoneAbbr"), I("zz", 0, 0, "zoneName");
    var mn = M.prototype;

    function _n(e) {
        return e
    }

    mn.add = Jt, mn.calendar = function (e, t) {
        var n = e || bt(), s = Gt(n, this).startOf("day"), i = c.calendarFormat(this, s) || "sameElse",
            r = t && (b(t[i]) ? t[i].call(this, n) : t[i]);
        return this.format(r || this.localeData().calendar(i, this, bt(n)))
    }, mn.clone = function () {
        return new M(this)
    }, mn.diff = function (e, t, n) {
        var s, i, r;
        if (!this.isValid()) return NaN;
        if (!(s = Gt(e, this)).isValid()) return NaN;
        switch (i = 6e4 * (s.utcOffset() - this.utcOffset()), t = H(t)) {
            case"year":
                r = Qt(this, s) / 12;
                break;
            case"month":
                r = Qt(this, s);
                break;
            case"quarter":
                r = Qt(this, s) / 3;
                break;
            case"second":
                r = (this - s) / 1e3;
                break;
            case"minute":
                r = (this - s) / 6e4;
                break;
            case"hour":
                r = (this - s) / 36e5;
                break;
            case"day":
                r = (this - s - i) / 864e5;
                break;
            case"week":
                r = (this - s - i) / 6048e5;
                break;
            default:
                r = this - s
        }
        return n ? r : S(r)
    }, mn.endOf = function (e) {
        var t;
        if (void 0 === (e = H(e)) || "millisecond" === e || !this.isValid()) return this;
        var n = this._isUTC ? rn : sn;
        switch (e) {
            case"year":
                t = n(this.year() + 1, 0, 1) - 1;
                break;
            case"quarter":
                t = n(this.year(), this.month() - this.month() % 3 + 3, 1) - 1;
                break;
            case"month":
                t = n(this.year(), this.month() + 1, 1) - 1;
                break;
            case"week":
                t = n(this.year(), this.month(), this.date() - this.weekday() + 7) - 1;
                break;
            case"isoWeek":
                t = n(this.year(), this.month(), this.date() - (this.isoWeekday() - 1) + 7) - 1;
                break;
            case"day":
            case"date":
                t = n(this.year(), this.month(), this.date() + 1) - 1;
                break;
            case"hour":
                t = this._d.valueOf(), t += 36e5 - nn(t + (this._isUTC ? 0 : 6e4 * this.utcOffset()), 36e5) - 1;
                break;
            case"minute":
                t = this._d.valueOf(), t += 6e4 - nn(t, 6e4) - 1;
                break;
            case"second":
                t = this._d.valueOf(), t += 1e3 - nn(t, 1e3) - 1;
                break
        }
        return this._d.setTime(t), c.updateOffset(this, !0), this
    }, mn.format = function (e) {
        e || (e = this.isUtc() ? c.defaultFormatUtc : c.defaultFormat);
        var t = A(this, e);
        return this.localeData().postformat(t)
    }, mn.from = function (e, t) {
        return this.isValid() && (k(e) && e.isValid() || bt(e).isValid()) ? jt({
            to: this,
            from: e
        }).locale(this.locale()).humanize(!t) : this.localeData().invalidDate()
    }, mn.fromNow = function (e) {
        return this.from(bt(), e)
    }, mn.to = function (e, t) {
        return this.isValid() && (k(e) && e.isValid() || bt(e).isValid()) ? jt({
            from: this,
            to: e
        }).locale(this.locale()).humanize(!t) : this.localeData().invalidDate()
    }, mn.toNow = function (e) {
        return this.to(bt(), e)
    }, mn.get = function (e) {
        return b(this[e = H(e)]) ? this[e]() : this
    }, mn.invalidAt = function () {
        return g(this).overflow
    }, mn.isAfter = function (e, t) {
        var n = k(e) ? e : bt(e);
        return !(!this.isValid() || !n.isValid()) && ("millisecond" === (t = H(t) || "millisecond") ? this.valueOf() > n.valueOf() : n.valueOf() < this.clone().startOf(t).valueOf())
    }, mn.isBefore = function (e, t) {
        var n = k(e) ? e : bt(e);
        return !(!this.isValid() || !n.isValid()) && ("millisecond" === (t = H(t) || "millisecond") ? this.valueOf() < n.valueOf() : this.clone().endOf(t).valueOf() < n.valueOf())
    }, mn.isBetween = function (e, t, n, s) {
        var i = k(e) ? e : bt(e), r = k(t) ? t : bt(t);
        return !!(this.isValid() && i.isValid() && r.isValid()) && ("(" === (s = s || "()")[0] ? this.isAfter(i, n) : !this.isBefore(i, n)) && (")" === s[1] ? this.isBefore(r, n) : !this.isAfter(r, n))
    }, mn.isSame = function (e, t) {
        var n, s = k(e) ? e : bt(e);
        return !(!this.isValid() || !s.isValid()) && ("millisecond" === (t = H(t) || "millisecond") ? this.valueOf() === s.valueOf() : (n = s.valueOf(), this.clone().startOf(t).valueOf() <= n && n <= this.clone().endOf(t).valueOf()))
    }, mn.isSameOrAfter = function (e, t) {
        return this.isSame(e, t) || this.isAfter(e, t)
    }, mn.isSameOrBefore = function (e, t) {
        return this.isSame(e, t) || this.isBefore(e, t)
    }, mn.isValid = function () {
        return v(this)
    }, mn.lang = Kt, mn.locale = Xt, mn.localeData = en, mn.max = Pt, mn.min = xt, mn.parsingFlags = function () {
        return _({}, g(this))
    }, mn.set = function (e, t) {
        if ("object" == typeof e) for (var n = function (e) {
            var t = [];
            for (var n in e) t.push({unit: n, priority: U[n]});
            return t.sort(function (e, t) {
                return e.priority - t.priority
            }), t
        }(e = R(e)), s = 0; s < n.length; s++) this[n[s].unit](e[n[s].unit]); else if (b(this[e = H(e)])) return this[e](t);
        return this
    }, mn.startOf = function (e) {
        var t;
        if (void 0 === (e = H(e)) || "millisecond" === e || !this.isValid()) return this;
        var n = this._isUTC ? rn : sn;
        switch (e) {
            case"year":
                t = n(this.year(), 0, 1);
                break;
            case"quarter":
                t = n(this.year(), this.month() - this.month() % 3, 1);
                break;
            case"month":
                t = n(this.year(), this.month(), 1);
                break;
            case"week":
                t = n(this.year(), this.month(), this.date() - this.weekday());
                break;
            case"isoWeek":
                t = n(this.year(), this.month(), this.date() - (this.isoWeekday() - 1));
                break;
            case"day":
            case"date":
                t = n(this.year(), this.month(), this.date());
                break;
            case"hour":
                t = this._d.valueOf(), t -= nn(t + (this._isUTC ? 0 : 6e4 * this.utcOffset()), 36e5);
                break;
            case"minute":
                t = this._d.valueOf(), t -= nn(t, 6e4);
                break;
            case"second":
                t = this._d.valueOf(), t -= nn(t, 1e3);
                break
        }
        return this._d.setTime(t), c.updateOffset(this, !0), this
    }, mn.subtract = Bt, mn.toArray = function () {
        var e = this;
        return [e.year(), e.month(), e.date(), e.hour(), e.minute(), e.second(), e.millisecond()]
    }, mn.toObject = function () {
        var e = this;
        return {
            years: e.year(),
            months: e.month(),
            date: e.date(),
            hours: e.hours(),
            minutes: e.minutes(),
            seconds: e.seconds(),
            milliseconds: e.milliseconds()
        }
    }, mn.toDate = function () {
        return new Date(this.valueOf())
    }, mn.toISOString = function (e) {
        if (!this.isValid()) return null;
        var t = !0 !== e, n = t ? this.clone().utc() : this;
        return n.year() < 0 || 9999 < n.year() ? A(n, t ? "YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]" : "YYYYYY-MM-DD[T]HH:mm:ss.SSSZ") : b(Date.prototype.toISOString) ? t ? this.toDate().toISOString() : new Date(this.valueOf() + 60 * this.utcOffset() * 1e3).toISOString().replace("Z", A(n, "Z")) : A(n, t ? "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]" : "YYYY-MM-DD[T]HH:mm:ss.SSSZ")
    }, mn.inspect = function () {
        if (!this.isValid()) return "moment.invalid(/* " + this._i + " */)";
        var e = "moment", t = "";
        this.isLocal() || (e = 0 === this.utcOffset() ? "moment.utc" : "moment.parseZone", t = "Z");
        var n = "[" + e + '("]', s = 0 <= this.year() && this.year() <= 9999 ? "YYYY" : "YYYYYY", i = t + '[")]';
        return this.format(n + s + "-MM-DD[T]HH:mm:ss.SSS" + i)
    }, mn.toJSON = function () {
        return this.isValid() ? this.toISOString() : null
    }, mn.toString = function () {
        return this.clone().locale("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")
    }, mn.unix = function () {
        return Math.floor(this.valueOf() / 1e3)
    }, mn.valueOf = function () {
        return this._d.valueOf() - 6e4 * (this._offset || 0)
    }, mn.creationData = function () {
        return {input: this._i, format: this._f, locale: this._locale, isUTC: this._isUTC, strict: this._strict}
    }, mn.year = Oe, mn.isLeapYear = function () {
        return De(this.year())
    }, mn.weekYear = function (e) {
        return on.call(this, e, this.week(), this.weekday(), this.localeData()._week.dow, this.localeData()._week.doy)
    }, mn.isoWeekYear = function (e) {
        return on.call(this, e, this.isoWeek(), this.isoWeekday(), 1, 4)
    }, mn.quarter = mn.quarters = function (e) {
        return null == e ? Math.ceil((this.month() + 1) / 3) : this.month(3 * (e - 1) + this.month() % 3)
    }, mn.month = Ue, mn.daysInMonth = function () {
        return Pe(this.year(), this.month())
    }, mn.week = mn.weeks = function (e) {
        var t = this.localeData().week(this);
        return null == e ? t : this.add(7 * (e - t), "d")
    }, mn.isoWeek = mn.isoWeeks = function (e) {
        var t = Ie(this, 1, 4).week;
        return null == e ? t : this.add(7 * (e - t), "d")
    }, mn.weeksInYear = function () {
        var e = this.localeData()._week;
        return Ae(this.year(), e.dow, e.doy)
    }, mn.isoWeeksInYear = function () {
        return Ae(this.year(), 1, 4)
    }, mn.date = un, mn.day = mn.days = function (e) {
        if (!this.isValid()) return null != e ? this : NaN;
        var t, n, s = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
        return null != e ? (t = e, n = this.localeData(), e = "string" != typeof t ? t : isNaN(t) ? "number" == typeof (t = n.weekdaysParse(t)) ? t : null : parseInt(t, 10), this.add(e - s, "d")) : s
    }, mn.weekday = function (e) {
        if (!this.isValid()) return null != e ? this : NaN;
        var t = (this.day() + 7 - this.localeData()._week.dow) % 7;
        return null == e ? t : this.add(e - t, "d")
    }, mn.isoWeekday = function (e) {
        if (!this.isValid()) return null != e ? this : NaN;
        if (null == e) return this.day() || 7;
        var t, n,
            s = (t = e, n = this.localeData(), "string" == typeof t ? n.weekdaysParse(t) % 7 || 7 : isNaN(t) ? null : t);
        return this.day(this.day() % 7 ? s : s - 7)
    }, mn.dayOfYear = function (e) {
        var t = Math.round((this.clone().startOf("day") - this.clone().startOf("year")) / 864e5) + 1;
        return null == e ? t : this.add(e - t, "d")
    }, mn.hour = mn.hours = nt, mn.minute = mn.minutes = ln, mn.second = mn.seconds = dn, mn.millisecond = mn.milliseconds = fn, mn.utcOffset = function (e, t, n) {
        var s, i = this._offset || 0;
        if (!this.isValid()) return null != e ? this : NaN;
        if (null == e) return this._isUTC ? i : Vt(this);
        if ("string" == typeof e) {
            if (null === (e = Nt(re, e))) return this
        } else Math.abs(e) < 16 && !n && (e *= 60);
        return !this._isUTC && t && (s = Vt(this)), this._offset = e, this._isUTC = !0, null != s && this.add(s, "m"), i !== e && (!t || this._changeInProgress ? qt(this, jt(e - i, "m"), 1, !1) : this._changeInProgress || (this._changeInProgress = !0, c.updateOffset(this, !0), this._changeInProgress = null)), this
    }, mn.utc = function (e) {
        return this.utcOffset(0, e)
    }, mn.local = function (e) {
        return this._isUTC && (this.utcOffset(0, e), this._isUTC = !1, e && this.subtract(Vt(this), "m")), this
    }, mn.parseZone = function () {
        if (null != this._tzm) this.utcOffset(this._tzm, !1, !0); else if ("string" == typeof this._i) {
            var e = Nt(ie, this._i);
            null != e ? this.utcOffset(e) : this.utcOffset(0, !0)
        }
        return this
    }, mn.hasAlignedHourOffset = function (e) {
        return !!this.isValid() && (e = e ? bt(e).utcOffset() : 0, (this.utcOffset() - e) % 60 == 0)
    }, mn.isDST = function () {
        return this.utcOffset() > this.clone().month(0).utcOffset() || this.utcOffset() > this.clone().month(5).utcOffset()
    }, mn.isLocal = function () {
        return !!this.isValid() && !this._isUTC
    }, mn.isUtcOffset = function () {
        return !!this.isValid() && this._isUTC
    }, mn.isUtc = Et, mn.isUTC = Et, mn.zoneAbbr = function () {
        return this._isUTC ? "UTC" : ""
    }, mn.zoneName = function () {
        return this._isUTC ? "Coordinated Universal Time" : ""
    }, mn.dates = n("dates accessor is deprecated. Use date instead.", un), mn.months = n("months accessor is deprecated. Use month instead", Ue), mn.years = n("years accessor is deprecated. Use year instead", Oe), mn.zone = n("moment().zone is deprecated, use moment().utcOffset instead. http://momentjs.com/guides/#/warnings/zone/", function (e, t) {
        return null != e ? ("string" != typeof e && (e = -e), this.utcOffset(e, t), this) : -this.utcOffset()
    }), mn.isDSTShifted = n("isDSTShifted is deprecated. See http://momentjs.com/guides/#/warnings/dst-shifted/ for more information", function () {
        if (!l(this._isDSTShifted)) return this._isDSTShifted;
        var e = {};
        if (w(e, this), (e = Ot(e))._a) {
            var t = e._isUTC ? y(e._a) : bt(e._a);
            this._isDSTShifted = this.isValid() && 0 < a(e._a, t.toArray())
        } else this._isDSTShifted = !1;
        return this._isDSTShifted
    });
    var yn = P.prototype;

    function gn(e, t, n, s) {
        var i = ht(), r = y().set(s, t);
        return i[n](r, e)
    }

    function vn(e, t, n) {
        if (h(e) && (t = e, e = void 0), e = e || "", null != t) return gn(e, t, n, "month");
        var s, i = [];
        for (s = 0; s < 12; s++) i[s] = gn(e, s, n, "month");
        return i
    }

    function pn(e, t, n, s) {
        t = ("boolean" == typeof e ? h(t) && (n = t, t = void 0) : (t = e, e = !1, h(n = t) && (n = t, t = void 0)), t || "");
        var i, r = ht(), a = e ? r._week.dow : 0;
        if (null != n) return gn(t, (n + a) % 7, s, "day");
        var o = [];
        for (i = 0; i < 7; i++) o[i] = gn(t, (i + a) % 7, s, "day");
        return o
    }

    yn.calendar = function (e, t, n) {
        var s = this._calendar[e] || this._calendar.sameElse;
        return b(s) ? s.call(t, n) : s
    }, yn.longDateFormat = function (e) {
        var t = this._longDateFormat[e], n = this._longDateFormat[e.toUpperCase()];
        return t || !n ? t : (this._longDateFormat[e] = n.replace(/MMMM|MM|DD|dddd/g, function (e) {
            return e.slice(1)
        }), this._longDateFormat[e])
    }, yn.invalidDate = function () {
        return this._invalidDate
    }, yn.ordinal = function (e) {
        return this._ordinal.replace("%d", e)
    }, yn.preparse = _n, yn.postformat = _n, yn.relativeTime = function (e, t, n, s) {
        var i = this._relativeTime[n];
        return b(i) ? i(e, t, n, s) : i.replace(/%d/i, e)
    }, yn.pastFuture = function (e, t) {
        var n = this._relativeTime[0 < e ? "future" : "past"];
        return b(n) ? n(t) : n.replace(/%s/i, t)
    }, yn.set = function (e) {
        var t, n;
        for (n in e) b(t = e[n]) ? this[n] = t : this["_" + n] = t;
        this._config = e, this._dayOfMonthOrdinalParseLenient = new RegExp((this._dayOfMonthOrdinalParse.source || this._ordinalParse.source) + "|" + /\d{1,2}/.source)
    }, yn.months = function (e, t) {
        return e ? o(this._months) ? this._months[e.month()] : this._months[(this._months.isFormat || We).test(t) ? "format" : "standalone"][e.month()] : o(this._months) ? this._months : this._months.standalone
    }, yn.monthsShort = function (e, t) {
        return e ? o(this._monthsShort) ? this._monthsShort[e.month()] : this._monthsShort[We.test(t) ? "format" : "standalone"][e.month()] : o(this._monthsShort) ? this._monthsShort : this._monthsShort.standalone
    }, yn.monthsParse = function (e, t, n) {
        var s, i, r;
        if (this._monthsParseExact) return function (e, t, n) {
            var s, i, r, a = e.toLocaleLowerCase();
            if (!this._monthsParse) for (this._monthsParse = [], this._longMonthsParse = [], this._shortMonthsParse = [], s = 0; s < 12; ++s) r = y([2e3, s]), this._shortMonthsParse[s] = this.monthsShort(r, "").toLocaleLowerCase(), this._longMonthsParse[s] = this.months(r, "").toLocaleLowerCase();
            return n ? "MMM" === t ? -1 !== (i = Ye.call(this._shortMonthsParse, a)) ? i : null : -1 !== (i = Ye.call(this._longMonthsParse, a)) ? i : null : "MMM" === t ? -1 !== (i = Ye.call(this._shortMonthsParse, a)) ? i : -1 !== (i = Ye.call(this._longMonthsParse, a)) ? i : null : -1 !== (i = Ye.call(this._longMonthsParse, a)) ? i : -1 !== (i = Ye.call(this._shortMonthsParse, a)) ? i : null
        }.call(this, e, t, n);
        for (this._monthsParse || (this._monthsParse = [], this._longMonthsParse = [], this._shortMonthsParse = []), s = 0; s < 12; s++) {
            if (i = y([2e3, s]), n && !this._longMonthsParse[s] && (this._longMonthsParse[s] = new RegExp("^" + this.months(i, "").replace(".", "") + "$", "i"), this._shortMonthsParse[s] = new RegExp("^" + this.monthsShort(i, "").replace(".", "") + "$", "i")), n || this._monthsParse[s] || (r = "^" + this.months(i, "") + "|^" + this.monthsShort(i, ""), this._monthsParse[s] = new RegExp(r.replace(".", ""), "i")), n && "MMMM" === t && this._longMonthsParse[s].test(e)) return s;
            if (n && "MMM" === t && this._shortMonthsParse[s].test(e)) return s;
            if (!n && this._monthsParse[s].test(e)) return s
        }
    }, yn.monthsRegex = function (e) {
        return this._monthsParseExact ? (m(this, "_monthsRegex") || Ne.call(this), e ? this._monthsStrictRegex : this._monthsRegex) : (m(this, "_monthsRegex") || (this._monthsRegex = Le), this._monthsStrictRegex && e ? this._monthsStrictRegex : this._monthsRegex)
    }, yn.monthsShortRegex = function (e) {
        return this._monthsParseExact ? (m(this, "_monthsRegex") || Ne.call(this), e ? this._monthsShortStrictRegex : this._monthsShortRegex) : (m(this, "_monthsShortRegex") || (this._monthsShortRegex = Fe), this._monthsShortStrictRegex && e ? this._monthsShortStrictRegex : this._monthsShortRegex)
    }, yn.week = function (e) {
        return Ie(e, this._week.dow, this._week.doy).week
    }, yn.firstDayOfYear = function () {
        return this._week.doy
    }, yn.firstDayOfWeek = function () {
        return this._week.dow
    }, yn.weekdays = function (e, t) {
        var n = o(this._weekdays) ? this._weekdays : this._weekdays[e && !0 !== e && this._weekdays.isFormat.test(t) ? "format" : "standalone"];
        return !0 === e ? je(n, this._week.dow) : e ? n[e.day()] : n
    }, yn.weekdaysMin = function (e) {
        return !0 === e ? je(this._weekdaysMin, this._week.dow) : e ? this._weekdaysMin[e.day()] : this._weekdaysMin
    }, yn.weekdaysShort = function (e) {
        return !0 === e ? je(this._weekdaysShort, this._week.dow) : e ? this._weekdaysShort[e.day()] : this._weekdaysShort
    }, yn.weekdaysParse = function (e, t, n) {
        var s, i, r;
        if (this._weekdaysParseExact) return function (e, t, n) {
            var s, i, r, a = e.toLocaleLowerCase();
            if (!this._weekdaysParse) for (this._weekdaysParse = [], this._shortWeekdaysParse = [], this._minWeekdaysParse = [], s = 0; s < 7; ++s) r = y([2e3, 1]).day(s), this._minWeekdaysParse[s] = this.weekdaysMin(r, "").toLocaleLowerCase(), this._shortWeekdaysParse[s] = this.weekdaysShort(r, "").toLocaleLowerCase(), this._weekdaysParse[s] = this.weekdays(r, "").toLocaleLowerCase();
            return n ? "dddd" === t ? -1 !== (i = Ye.call(this._weekdaysParse, a)) ? i : null : "ddd" === t ? -1 !== (i = Ye.call(this._shortWeekdaysParse, a)) ? i : null : -1 !== (i = Ye.call(this._minWeekdaysParse, a)) ? i : null : "dddd" === t ? -1 !== (i = Ye.call(this._weekdaysParse, a)) ? i : -1 !== (i = Ye.call(this._shortWeekdaysParse, a)) ? i : -1 !== (i = Ye.call(this._minWeekdaysParse, a)) ? i : null : "ddd" === t ? -1 !== (i = Ye.call(this._shortWeekdaysParse, a)) ? i : -1 !== (i = Ye.call(this._weekdaysParse, a)) ? i : -1 !== (i = Ye.call(this._minWeekdaysParse, a)) ? i : null : -1 !== (i = Ye.call(this._minWeekdaysParse, a)) ? i : -1 !== (i = Ye.call(this._weekdaysParse, a)) ? i : -1 !== (i = Ye.call(this._shortWeekdaysParse, a)) ? i : null
        }.call(this, e, t, n);
        for (this._weekdaysParse || (this._weekdaysParse = [], this._minWeekdaysParse = [], this._shortWeekdaysParse = [], this._fullWeekdaysParse = []), s = 0; s < 7; s++) {
            if (i = y([2e3, 1]).day(s), n && !this._fullWeekdaysParse[s] && (this._fullWeekdaysParse[s] = new RegExp("^" + this.weekdays(i, "").replace(".", "\\.?") + "$", "i"), this._shortWeekdaysParse[s] = new RegExp("^" + this.weekdaysShort(i, "").replace(".", "\\.?") + "$", "i"), this._minWeekdaysParse[s] = new RegExp("^" + this.weekdaysMin(i, "").replace(".", "\\.?") + "$", "i")), this._weekdaysParse[s] || (r = "^" + this.weekdays(i, "") + "|^" + this.weekdaysShort(i, "") + "|^" + this.weekdaysMin(i, ""), this._weekdaysParse[s] = new RegExp(r.replace(".", ""), "i")), n && "dddd" === t && this._fullWeekdaysParse[s].test(e)) return s;
            if (n && "ddd" === t && this._shortWeekdaysParse[s].test(e)) return s;
            if (n && "dd" === t && this._minWeekdaysParse[s].test(e)) return s;
            if (!n && this._weekdaysParse[s].test(e)) return s
        }
    }, yn.weekdaysRegex = function (e) {
        return this._weekdaysParseExact ? (m(this, "_weekdaysRegex") || Qe.call(this), e ? this._weekdaysStrictRegex : this._weekdaysRegex) : (m(this, "_weekdaysRegex") || (this._weekdaysRegex = qe), this._weekdaysStrictRegex && e ? this._weekdaysStrictRegex : this._weekdaysRegex)
    }, yn.weekdaysShortRegex = function (e) {
        return this._weekdaysParseExact ? (m(this, "_weekdaysRegex") || Qe.call(this), e ? this._weekdaysShortStrictRegex : this._weekdaysShortRegex) : (m(this, "_weekdaysShortRegex") || (this._weekdaysShortRegex = Je), this._weekdaysShortStrictRegex && e ? this._weekdaysShortStrictRegex : this._weekdaysShortRegex)
    }, yn.weekdaysMinRegex = function (e) {
        return this._weekdaysParseExact ? (m(this, "_weekdaysRegex") || Qe.call(this), e ? this._weekdaysMinStrictRegex : this._weekdaysMinRegex) : (m(this, "_weekdaysMinRegex") || (this._weekdaysMinRegex = Be), this._weekdaysMinStrictRegex && e ? this._weekdaysMinStrictRegex : this._weekdaysMinRegex)
    }, yn.isPM = function (e) {
        return "p" === (e + "").toLowerCase().charAt(0)
    }, yn.meridiem = function (e, t, n) {
        return 11 < e ? n ? "pm" : "PM" : n ? "am" : "AM"
    }, ut("en", {
        dayOfMonthOrdinalParse: /\d{1,2}(th|st|nd|rd)/, ordinal: function (e) {
            var t = e % 10;
            return e + (1 === D(e % 100 / 10) ? "th" : 1 === t ? "st" : 2 === t ? "nd" : 3 === t ? "rd" : "th")
        }
    }), c.lang = n("moment.lang is deprecated. Use moment.locale instead.", ut), c.langData = n("moment.langData is deprecated. Use moment.localeData instead.", ht);
    var wn = Math.abs;

    function Mn(e, t, n, s) {
        var i = jt(t, n);
        return e._milliseconds += s * i._milliseconds, e._days += s * i._days, e._months += s * i._months, e._bubble()
    }

    function kn(e) {
        return e < 0 ? Math.floor(e) : Math.ceil(e)
    }

    function Sn(e) {
        return 4800 * e / 146097
    }

    function Dn(e) {
        return 146097 * e / 4800
    }

    function Yn(e) {
        return function () {
            return this.as(e)
        }
    }

    var On = Yn("ms"), Tn = Yn("s"), bn = Yn("m"), xn = Yn("h"), Pn = Yn("d"), Wn = Yn("w"), Cn = Yn("M"), Hn = Yn("Q"),
        Rn = Yn("y");

    function Un(e) {
        return function () {
            return this.isValid() ? this._data[e] : NaN
        }
    }

    var Fn = Un("milliseconds"), Ln = Un("seconds"), Nn = Un("minutes"), Gn = Un("hours"), Vn = Un("days"),
        En = Un("months"), In = Un("years");
    var An = Math.round, jn = {ss: 44, s: 45, m: 45, h: 22, d: 26, M: 11};
    var Zn = Math.abs;

    function zn(e) {
        return (0 < e) - (e < 0) || +e
    }

    function $n() {
        if (!this.isValid()) return this.localeData().invalidDate();
        var e, t, n = Zn(this._milliseconds) / 1e3, s = Zn(this._days), i = Zn(this._months);
        t = S((e = S(n / 60)) / 60), n %= 60, e %= 60;
        var r = S(i / 12), a = i %= 12, o = s, u = t, l = e, h = n ? n.toFixed(3).replace(/\.?0+$/, "") : "",
            d = this.asSeconds();
        if (!d) return "P0D";
        var c = d < 0 ? "-" : "", f = zn(this._months) !== zn(d) ? "-" : "", m = zn(this._days) !== zn(d) ? "-" : "",
            _ = zn(this._milliseconds) !== zn(d) ? "-" : "";
        return c + "P" + (r ? f + r + "Y" : "") + (a ? f + a + "M" : "") + (o ? m + o + "D" : "") + (u || l || h ? "T" : "") + (u ? _ + u + "H" : "") + (l ? _ + l + "M" : "") + (h ? _ + h + "S" : "")
    }

    var qn = Ht.prototype;
    return qn.isValid = function () {
        return this._isValid
    }, qn.abs = function () {
        var e = this._data;
        return this._milliseconds = wn(this._milliseconds), this._days = wn(this._days), this._months = wn(this._months), e.milliseconds = wn(e.milliseconds), e.seconds = wn(e.seconds), e.minutes = wn(e.minutes), e.hours = wn(e.hours), e.months = wn(e.months), e.years = wn(e.years), this
    }, qn.add = function (e, t) {
        return Mn(this, e, t, 1)
    }, qn.subtract = function (e, t) {
        return Mn(this, e, t, -1)
    }, qn.as = function (e) {
        if (!this.isValid()) return NaN;
        var t, n, s = this._milliseconds;
        if ("month" === (e = H(e)) || "quarter" === e || "year" === e) switch (t = this._days + s / 864e5, n = this._months + Sn(t), e) {
            case"month":
                return n;
            case"quarter":
                return n / 3;
            case"year":
                return n / 12
        } else switch (t = this._days + Math.round(Dn(this._months)), e) {
            case"week":
                return t / 7 + s / 6048e5;
            case"day":
                return t + s / 864e5;
            case"hour":
                return 24 * t + s / 36e5;
            case"minute":
                return 1440 * t + s / 6e4;
            case"second":
                return 86400 * t + s / 1e3;
            case"millisecond":
                return Math.floor(864e5 * t) + s;
            default:
                throw new Error("Unknown unit " + e)
        }
    }, qn.asMilliseconds = On, qn.asSeconds = Tn, qn.asMinutes = bn, qn.asHours = xn, qn.asDays = Pn, qn.asWeeks = Wn, qn.asMonths = Cn, qn.asQuarters = Hn, qn.asYears = Rn, qn.valueOf = function () {
        return this.isValid() ? this._milliseconds + 864e5 * this._days + this._months % 12 * 2592e6 + 31536e6 * D(this._months / 12) : NaN
    }, qn._bubble = function () {
        var e, t, n, s, i, r = this._milliseconds, a = this._days, o = this._months, u = this._data;
        return 0 <= r && 0 <= a && 0 <= o || r <= 0 && a <= 0 && o <= 0 || (r += 864e5 * kn(Dn(o) + a), o = a = 0), u.milliseconds = r % 1e3, e = S(r / 1e3), u.seconds = e % 60, t = S(e / 60), u.minutes = t % 60, n = S(t / 60), u.hours = n % 24, o += i = S(Sn(a += S(n / 24))), a -= kn(Dn(i)), s = S(o / 12), o %= 12, u.days = a, u.months = o, u.years = s, this
    }, qn.clone = function () {
        return jt(this)
    }, qn.get = function (e) {
        return e = H(e), this.isValid() ? this[e + "s"]() : NaN
    }, qn.milliseconds = Fn, qn.seconds = Ln, qn.minutes = Nn, qn.hours = Gn, qn.days = Vn, qn.weeks = function () {
        return S(this.days() / 7)
    }, qn.months = En, qn.years = In, qn.humanize = function (e) {
        if (!this.isValid()) return this.localeData().invalidDate();
        var t, n, s, i, r, a, o, u, l, h, d, c = this.localeData(),
            f = (n = !e, s = c, i = jt(t = this).abs(), r = An(i.as("s")), a = An(i.as("m")), o = An(i.as("h")), u = An(i.as("d")), l = An(i.as("M")), h = An(i.as("y")), (d = r <= jn.ss && ["s", r] || r < jn.s && ["ss", r] || a <= 1 && ["m"] || a < jn.m && ["mm", a] || o <= 1 && ["h"] || o < jn.h && ["hh", o] || u <= 1 && ["d"] || u < jn.d && ["dd", u] || l <= 1 && ["M"] || l < jn.M && ["MM", l] || h <= 1 && ["y"] || ["yy", h])[2] = n, d[3] = 0 < +t, d[4] = s, function (e, t, n, s, i) {
                return i.relativeTime(t || 1, !!n, e, s)
            }.apply(null, d));
        return e && (f = c.pastFuture(+this, f)), c.postformat(f)
    }, qn.toISOString = $n, qn.toString = $n, qn.toJSON = $n, qn.locale = Xt, qn.localeData = en, qn.toIsoString = n("toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)", $n), qn.lang = Kt, I("X", 0, 0, "unix"), I("x", 0, 0, "valueOf"), ue("x", se), ue("X", /[+-]?\d+(\.\d{1,3})?/), ce("X", function (e, t, n) {
        n._d = new Date(1e3 * parseFloat(e, 10))
    }), ce("x", function (e, t, n) {
        n._d = new Date(D(e))
    }), c.version = "2.24.0", e = bt, c.fn = mn, c.min = function () {
        return Wt("isBefore", [].slice.call(arguments, 0))
    }, c.max = function () {
        return Wt("isAfter", [].slice.call(arguments, 0))
    }, c.now = function () {
        return Date.now ? Date.now() : +new Date
    }, c.utc = y, c.unix = function (e) {
        return bt(1e3 * e)
    }, c.months = function (e, t) {
        return vn(e, t, "months")
    }, c.isDate = d, c.locale = ut, c.invalid = p, c.duration = jt, c.isMoment = k, c.weekdays = function (e, t, n) {
        return pn(e, t, n, "weekdays")
    }, c.parseZone = function () {
        return bt.apply(null, arguments).parseZone()
    }, c.localeData = ht, c.isDuration = Rt, c.monthsShort = function (e, t) {
        return vn(e, t, "monthsShort")
    }, c.weekdaysMin = function (e, t, n) {
        return pn(e, t, n, "weekdaysMin")
    }, c.defineLocale = lt, c.updateLocale = function (e, t) {
        if (null != t) {
            var n, s, i = st;
            null != (s = ot(e)) && (i = s._config), (n = new P(t = x(i, t))).parentLocale = it[e], it[e] = n, ut(e)
        } else null != it[e] && (null != it[e].parentLocale ? it[e] = it[e].parentLocale : null != it[e] && delete it[e]);
        return it[e]
    }, c.locales = function () {
        return s(it)
    }, c.weekdaysShort = function (e, t, n) {
        return pn(e, t, n, "weekdaysShort")
    }, c.normalizeUnits = H, c.relativeTimeRounding = function (e) {
        return void 0 === e ? An : "function" == typeof e && (An = e, !0)
    }, c.relativeTimeThreshold = function (e, t) {
        return void 0 !== jn[e] && (void 0 === t ? jn[e] : (jn[e] = t, "s" === e && (jn.ss = t - 1), !0))
    }, c.calendarFormat = function (e, t) {
        var n = e.diff(t, "days", !0);
        return n < -6 ? "sameElse" : n < -1 ? "lastWeek" : n < 0 ? "lastDay" : n < 1 ? "sameDay" : n < 2 ? "nextDay" : n < 7 ? "nextWeek" : "sameElse"
    }, c.prototype = mn, c.HTML5_FMT = {
        DATETIME_LOCAL: "YYYY-MM-DDTHH:mm",
        DATETIME_LOCAL_SECONDS: "YYYY-MM-DDTHH:mm:ss",
        DATETIME_LOCAL_MS: "YYYY-MM-DDTHH:mm:ss.SSS",
        DATE: "YYYY-MM-DD",
        TIME: "HH:mm",
        TIME_SECONDS: "HH:mm:ss",
        TIME_MS: "HH:mm:ss.SSS",
        WEEK: "GGGG-[W]WW",
        MONTH: "YYYY-MM"
    }, c
});/*@preserve
 * Tempus Dominus Bootstrap4 v5.1.2 (https://tempusdominus.github.io/bootstrap-4/)
 * Copyright 2016-2018 Jonathan Peterson
 * Licensed under MIT (https://github.com/tempusdominus/bootstrap-3/blob/master/LICENSE)
 */
if ("undefined" == typeof jQuery) throw new Error("Tempus Dominus Bootstrap4's requires jQuery. jQuery must be included before Tempus Dominus Bootstrap4's JavaScript.");
if (+function (a) {
    var b = a.fn.jquery.split(" ")[0].split(".");
    if (b[0] < 2 && b[1] < 9 || 1 === b[0] && 9 === b[1] && b[2] < 1 || b[0] >= 4) throw new Error("Tempus Dominus Bootstrap4's requires at least jQuery v3.0.0 but less than v4.0.0")
}(jQuery), "undefined" == typeof moment) throw new Error("Tempus Dominus Bootstrap4's requires moment.js. Moment.js must be included before Tempus Dominus Bootstrap4's JavaScript.");
var version = moment.version.split(".");
if (version[0] <= 2 && version[1] < 17 || version[0] >= 3) throw new Error("Tempus Dominus Bootstrap4's requires at least moment.js v2.17.0 but less than v3.0.0");
+function () {
    function a(a, b) {
        if (!a) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return !b || "object" != typeof b && "function" != typeof b ? a : b
    }

    function b(a, b) {
        if ("function" != typeof b && null !== b) throw new TypeError("Super expression must either be null or a function, not " + typeof b);
        a.prototype = Object.create(b && b.prototype, {
            constructor: {
                value: a,
                enumerable: !1,
                writable: !0,
                configurable: !0
            }
        }), b && (Object.setPrototypeOf ? Object.setPrototypeOf(a, b) : a.__proto__ = b)
    }

    function c(a, b) {
        if (!(a instanceof b)) throw new TypeError("Cannot call a class as a function")
    }

    var d = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (a) {
        return typeof a
    } : function (a) {
        return a && "function" == typeof Symbol && a.constructor === Symbol && a !== Symbol.prototype ? "symbol" : typeof a
    }, e = function () {
        function a(a, b) {
            for (var c = 0; c < b.length; c++) {
                var d = b[c];
                d.enumerable = d.enumerable || !1, d.configurable = !0, "value" in d && (d.writable = !0), Object.defineProperty(a, d.key, d)
            }
        }

        return function (b, c, d) {
            return c && a(b.prototype, c), d && a(b, d), b
        }
    }(), f = function (a, b) {
        var d = "datetimepicker", f = "" + d, g = "." + f, h = ".data-api",
            i = {DATA_TOGGLE: '[data-toggle="' + f + '"]'}, j = {INPUT: d + "-input"}, k = {
                CHANGE: "change" + g,
                BLUR: "blur" + g,
                KEYUP: "keyup" + g,
                KEYDOWN: "keydown" + g,
                FOCUS: "focus" + g,
                CLICK_DATA_API: "click" + g + h,
                UPDATE: "update" + g,
                ERROR: "error" + g,
                HIDE: "hide" + g,
                SHOW: "show" + g
            }, l = [{CLASS_NAME: "days", NAV_FUNCTION: "M", NAV_STEP: 1}, {
                CLASS_NAME: "months",
                NAV_FUNCTION: "y",
                NAV_STEP: 1
            }, {CLASS_NAME: "years", NAV_FUNCTION: "y", NAV_STEP: 10}, {
                CLASS_NAME: "decades",
                NAV_FUNCTION: "y",
                NAV_STEP: 100
            }], m = {
                up: 38,
                38: "up",
                down: 40,
                40: "down",
                left: 37,
                37: "left",
                right: 39,
                39: "right",
                tab: 9,
                9: "tab",
                escape: 27,
                27: "escape",
                enter: 13,
                13: "enter",
                pageUp: 33,
                33: "pageUp",
                pageDown: 34,
                34: "pageDown",
                shift: 16,
                16: "shift",
                control: 17,
                17: "control",
                space: 32,
                32: "space",
                t: 84,
                84: "t",
                delete: 46,
                46: "delete"
            }, n = ["times", "days", "months", "years", "decades"], o = {}, p = {}, q = {
                timeZone: "",
                format: !1,
                dayViewHeaderFormat: "MMMM YYYY",
                extraFormats: !1,
                stepping: 1,
                minDate: !1,
                maxDate: !1,
                useCurrent: !0,
                collapse: !0,
                locale: b.locale(),
                defaultDate: !1,
                disabledDates: !1,
                enabledDates: !1,
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down",
                    previous: "fa fa-chevron-left",
                    next: "fa fa-chevron-right",
                    today: "fa fa-calendar-check-o",
                    clear: "fa fa-delete",
                    close: "fa fa-times"
                },
                tooltips: {
                    today: "Go to today",
                    clear: "Clear selection",
                    close: "Close the picker",
                    selectMonth: "Select Month",
                    prevMonth: "Previous Month",
                    nextMonth: "Next Month",
                    selectYear: "Select Year",
                    prevYear: "Previous Year",
                    nextYear: "Next Year",
                    selectDecade: "Select Decade",
                    prevDecade: "Previous Decade",
                    nextDecade: "Next Decade",
                    prevCentury: "Previous Century",
                    nextCentury: "Next Century",
                    pickHour: "Pick Hour",
                    incrementHour: "Increment Hour",
                    decrementHour: "Decrement Hour",
                    pickMinute: "Pick Minute",
                    incrementMinute: "Increment Minute",
                    decrementMinute: "Decrement Minute",
                    pickSecond: "Pick Second",
                    incrementSecond: "Increment Second",
                    decrementSecond: "Decrement Second",
                    togglePeriod: "Toggle Period",
                    selectTime: "Select Time",
                    selectDate: "Select Date"
                },
                useStrict: !1,
                sideBySide: !1,
                daysOfWeekDisabled: !1,
                calendarWeeks: !1,
                viewMode: "days",
                toolbarPlacement: "default",
                buttons: {showToday: !1, showClear: !1, showClose: !1},
                widgetPositioning: {horizontal: "auto", vertical: "auto"},
                widgetParent: null,
                ignoreReadonly: !1,
                keepOpen: !1,
                focusOnShow: !0,
                inline: !1,
                keepInvalid: !1,
                keyBinds: {
                    up: function () {
                        if (!this.widget) return !1;
                        var a = this._dates[0] || this.getMoment();
                        return this.widget.find(".datepicker").is(":visible") ? this.date(a.clone().subtract(7, "d")) : this.date(a.clone().add(this.stepping(), "m")), !0
                    }, down: function () {
                        if (!this.widget) return this.show(), !1;
                        var a = this._dates[0] || this.getMoment();
                        return this.widget.find(".datepicker").is(":visible") ? this.date(a.clone().add(7, "d")) : this.date(a.clone().subtract(this.stepping(), "m")), !0
                    }, "control up": function () {
                        if (!this.widget) return !1;
                        var a = this._dates[0] || this.getMoment();
                        return this.widget.find(".datepicker").is(":visible") ? this.date(a.clone().subtract(1, "y")) : this.date(a.clone().add(1, "h")), !0
                    }, "control down": function () {
                        if (!this.widget) return !1;
                        var a = this._dates[0] || this.getMoment();
                        return this.widget.find(".datepicker").is(":visible") ? this.date(a.clone().add(1, "y")) : this.date(a.clone().subtract(1, "h")), !0
                    }, left: function () {
                        if (!this.widget) return !1;
                        var a = this._dates[0] || this.getMoment();
                        return this.widget.find(".datepicker").is(":visible") && this.date(a.clone().subtract(1, "d")), !0
                    }, right: function () {
                        if (!this.widget) return !1;
                        var a = this._dates[0] || this.getMoment();
                        return this.widget.find(".datepicker").is(":visible") && this.date(a.clone().add(1, "d")), !0
                    }, pageUp: function () {
                        if (!this.widget) return !1;
                        var a = this._dates[0] || this.getMoment();
                        return this.widget.find(".datepicker").is(":visible") && this.date(a.clone().subtract(1, "M")), !0
                    }, pageDown: function () {
                        if (!this.widget) return !1;
                        var a = this._dates[0] || this.getMoment();
                        return this.widget.find(".datepicker").is(":visible") && this.date(a.clone().add(1, "M")), !0
                    }, enter: function () {
                        return !!this.widget && (this.hide(), !0)
                    }, escape: function () {
                        return !!this.widget && (this.hide(), !0)
                    }, "control space": function () {
                        return !!this.widget && (this.widget.find(".timepicker").is(":visible") && this.widget.find('.btn[data-action="togglePeriod"]').click(), !0)
                    }, t: function () {
                        return !!this.widget && (this.date(this.getMoment()), !0)
                    }, delete: function () {
                        return !!this.widget && (this.clear(), !0)
                    }
                },
                debug: !1,
                allowInputToggle: !1,
                disabledTimeIntervals: !1,
                disabledHours: !1,
                enabledHours: !1,
                viewDate: !1,
                allowMultidate: !1,
                multidateSeparator: ","
            }, r = function () {
                function r(a, b) {
                    c(this, r), this._options = this._getOptions(b), this._element = a, this._dates = [], this._datesFormatted = [], this._viewDate = null, this.unset = !0, this.component = !1, this.widget = !1, this.use24Hours = null, this.actualFormat = null, this.parseFormats = null, this.currentViewMode = null, this.MinViewModeNumber = 0, this._int()
                }

                return r.prototype._int = function () {
                    var b = this._element.data("target-input");
                    this._element.is("input") ? this.input = this._element : void 0 !== b && ("nearest" === b ? this.input = this._element.find("input") : this.input = a(b)), this._dates = [], this._dates[0] = this.getMoment(), this._viewDate = this.getMoment().clone(), a.extend(!0, this._options, this._dataToOptions()), this.options(this._options), this._initFormatting(), void 0 !== this.input && this.input.is("input") && 0 !== this.input.val().trim().length ? this._setValue(this._parseInputDate(this.input.val().trim()), 0) : this._options.defaultDate && void 0 !== this.input && void 0 === this.input.attr("placeholder") && this._setValue(this._options.defaultDate, 0), this._options.inline && this.show()
                }, r.prototype._update = function () {
                    this.widget && (this._fillDate(), this._fillTime())
                }, r.prototype._setValue = function (a, b) {
                    var c = this.unset ? null : this._dates[b], d = "";
                    if (!a) return this._options.allowMultidate && 1 !== this._dates.length ? (d = this._element.data("date") + ",", d = d.replace(c.format(this.actualFormat) + ",", "").replace(",,", "").replace(/,\s*$/, ""), this._dates.splice(b, 1), this._datesFormatted.splice(b, 1)) : (this.unset = !0, this._dates = [], this._datesFormatted = []), void 0 !== this.input && (this.input.val(d), this.input.trigger("input")), this._element.data("date", d), this._notifyEvent({
                        type: r.Event.CHANGE,
                        date: !1,
                        oldDate: c
                    }), void this._update();
                    if (a = a.clone().locale(this._options.locale), this._hasTimeZone() && a.tz(this._options.timeZone), 1 !== this._options.stepping && a.minutes(Math.round(a.minutes() / this._options.stepping) * this._options.stepping).seconds(0), this._isValid(a)) {
                        if (this._dates[b] = a, this._datesFormatted[b] = a.format("YYYY-MM-DD"), this._viewDate = a.clone(), this._options.allowMultidate && this._dates.length > 1) {
                            for (var e = 0; e < this._dates.length; e++) d += "" + this._dates[e].format(this.actualFormat) + this._options.multidateSeparator;
                            d = d.replace(/,\s*$/, "")
                        } else d = this._dates[b].format(this.actualFormat);
                        void 0 !== this.input && (this.input.val(d), this.input.trigger("input")), this._element.data("date", d), this.unset = !1, this._update(), this._notifyEvent({
                            type: r.Event.CHANGE,
                            date: this._dates[b].clone(),
                            oldDate: c
                        })
                    } else this._options.keepInvalid ? this._notifyEvent({
                        type: r.Event.CHANGE,
                        date: a,
                        oldDate: c
                    }) : void 0 !== this.input && (this.input.val("" + (this.unset ? "" : this._dates[b].format(this.actualFormat))), this.input.trigger("input")), this._notifyEvent({
                        type: r.Event.ERROR,
                        date: a,
                        oldDate: c
                    })
                }, r.prototype._change = function (b) {
                    var c = a(b.target).val().trim(), d = c ? this._parseInputDate(c) : null;
                    return this._setValue(d), b.stopImmediatePropagation(), !1
                }, r.prototype._getOptions = function (b) {
                    return b = a.extend(!0, {}, q, b)
                }, r.prototype._hasTimeZone = function () {
                    return void 0 !== b.tz && void 0 !== this._options.timeZone && null !== this._options.timeZone && "" !== this._options.timeZone
                }, r.prototype._isEnabled = function (a) {
                    if ("string" != typeof a || a.length > 1) throw new TypeError("isEnabled expects a single character string parameter");
                    switch (a) {
                        case"y":
                            return this.actualFormat.indexOf("Y") !== -1;
                        case"M":
                            return this.actualFormat.indexOf("M") !== -1;
                        case"d":
                            return this.actualFormat.toLowerCase().indexOf("d") !== -1;
                        case"h":
                        case"H":
                            return this.actualFormat.toLowerCase().indexOf("h") !== -1;
                        case"m":
                            return this.actualFormat.indexOf("m") !== -1;
                        case"s":
                            return this.actualFormat.indexOf("s") !== -1;
                        case"a":
                        case"A":
                            return this.actualFormat.toLowerCase().indexOf("a") !== -1;
                        default:
                            return !1
                    }
                }, r.prototype._hasTime = function () {
                    return this._isEnabled("h") || this._isEnabled("m") || this._isEnabled("s")
                }, r.prototype._hasDate = function () {
                    return this._isEnabled("y") || this._isEnabled("M") || this._isEnabled("d")
                }, r.prototype._dataToOptions = function () {
                    var b = this._element.data(), c = {};
                    return b.dateOptions && b.dateOptions instanceof Object && (c = a.extend(!0, c, b.dateOptions)), a.each(this._options, function (a) {
                        var d = "date" + a.charAt(0).toUpperCase() + a.slice(1);
                        void 0 !== b[d] ? c[a] = b[d] : delete c[a]
                    }), c
                }, r.prototype._notifyEvent = function (a) {
                    a.type === r.Event.CHANGE && (a.date && a.date.isSame(a.oldDate)) || !a.date && !a.oldDate || this._element.trigger(a)
                }, r.prototype._viewUpdate = function (a) {
                    "y" === a && (a = "YYYY"), this._notifyEvent({
                        type: r.Event.UPDATE,
                        change: a,
                        viewDate: this._viewDate.clone()
                    })
                }, r.prototype._showMode = function (a) {
                    this.widget && (a && (this.currentViewMode = Math.max(this.MinViewModeNumber, Math.min(3, this.currentViewMode + a))), this.widget.find(".datepicker > div").hide().filter(".datepicker-" + l[this.currentViewMode].CLASS_NAME).show())
                }, r.prototype._isInDisabledDates = function (a) {
                    return this._options.disabledDates[a.format("YYYY-MM-DD")] === !0
                }, r.prototype._isInEnabledDates = function (a) {
                    return this._options.enabledDates[a.format("YYYY-MM-DD")] === !0
                }, r.prototype._isInDisabledHours = function (a) {
                    return this._options.disabledHours[a.format("H")] === !0
                }, r.prototype._isInEnabledHours = function (a) {
                    return this._options.enabledHours[a.format("H")] === !0
                }, r.prototype._isValid = function (b, c) {
                    if (!b.isValid()) return !1;
                    if (this._options.disabledDates && "d" === c && this._isInDisabledDates(b)) return !1;
                    if (this._options.enabledDates && "d" === c && !this._isInEnabledDates(b)) return !1;
                    if (this._options.minDate && b.isBefore(this._options.minDate, c)) return !1;
                    if (this._options.maxDate && b.isAfter(this._options.maxDate, c)) return !1;
                    if (this._options.daysOfWeekDisabled && "d" === c && this._options.daysOfWeekDisabled.indexOf(b.day()) !== -1) return !1;
                    if (this._options.disabledHours && ("h" === c || "m" === c || "s" === c) && this._isInDisabledHours(b)) return !1;
                    if (this._options.enabledHours && ("h" === c || "m" === c || "s" === c) && !this._isInEnabledHours(b)) return !1;
                    if (this._options.disabledTimeIntervals && ("h" === c || "m" === c || "s" === c)) {
                        var d = !1;
                        if (a.each(this._options.disabledTimeIntervals, function () {
                            if (b.isBetween(this[0], this[1])) return d = !0, !1
                        }), d) return !1
                    }
                    return !0
                }, r.prototype._parseInputDate = function (a) {
                    return void 0 === this._options.parseInputDate ? b.isMoment(a) || (a = this.getMoment(a)) : a = this._options.parseInputDate(a), a
                }, r.prototype._keydown = function (a) {
                    var b = null, c = void 0, d = void 0, e = void 0, f = void 0, g = [], h = {}, i = a.which, j = "p";
                    o[i] = j;
                    for (c in o) o.hasOwnProperty(c) && o[c] === j && (g.push(c), parseInt(c, 10) !== i && (h[c] = !0));
                    for (c in this._options.keyBinds) if (this._options.keyBinds.hasOwnProperty(c) && "function" == typeof this._options.keyBinds[c] && (e = c.split(" "), e.length === g.length && m[i] === e[e.length - 1])) {
                        for (f = !0, d = e.length - 2; d >= 0; d--) if (!(m[e[d]] in h)) {
                            f = !1;
                            break
                        }
                        if (f) {
                            b = this._options.keyBinds[c];
                            break
                        }
                    }
                    b && b.call(this) && (a.stopPropagation(), a.preventDefault())
                }, r.prototype._keyup = function (a) {
                    o[a.which] = "r", p[a.which] && (p[a.which] = !1, a.stopPropagation(), a.preventDefault())
                }, r.prototype._indexGivenDates = function (b) {
                    var c = {}, d = this;
                    return a.each(b, function () {
                        var a = d._parseInputDate(this);
                        a.isValid() && (c[a.format("YYYY-MM-DD")] = !0)
                    }), !!Object.keys(c).length && c
                }, r.prototype._indexGivenHours = function (b) {
                    var c = {};
                    return a.each(b, function () {
                        c[this] = !0
                    }), !!Object.keys(c).length && c
                }, r.prototype._initFormatting = function () {
                    var a = this._options.format || "L LT", b = this;
                    this.actualFormat = a.replace(/(\[[^\[]*])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g, function (a) {
                        return b._dates[0].localeData().longDateFormat(a) || a
                    }), this.parseFormats = this._options.extraFormats ? this._options.extraFormats.slice() : [], this.parseFormats.indexOf(a) < 0 && this.parseFormats.indexOf(this.actualFormat) < 0 && this.parseFormats.push(this.actualFormat), this.use24Hours = this.actualFormat.toLowerCase().indexOf("a") < 1 && this.actualFormat.replace(/\[.*?]/g, "").indexOf("h") < 1, this._isEnabled("y") && (this.MinViewModeNumber = 2), this._isEnabled("M") && (this.MinViewModeNumber = 1), this._isEnabled("d") && (this.MinViewModeNumber = 0), this.currentViewMode = Math.max(this.MinViewModeNumber, this.currentViewMode), this.unset || this._setValue(this._dates[0], 0)
                }, r.prototype._getLastPickedDate = function () {
                    return this._dates[this._getLastPickedDateIndex()]
                }, r.prototype._getLastPickedDateIndex = function () {
                    return this._dates.length - 1
                }, r.prototype.getMoment = function (a) {
                    var c = void 0;
                    return c = void 0 === a || null === a ? b() : this._hasTimeZone() ? b.tz(a, this.parseFormats, this._options.locale, this._options.useStrict, this._options.timeZone) : b(a, this.parseFormats, this._options.locale, this._options.useStrict), this._hasTimeZone() && c.tz(this._options.timeZone), c
                }, r.prototype.toggle = function () {
                    return this.widget ? this.hide() : this.show()
                }, r.prototype.ignoreReadonly = function (a) {
                    if (0 === arguments.length) return this._options.ignoreReadonly;
                    if ("boolean" != typeof a) throw new TypeError("ignoreReadonly () expects a boolean parameter");
                    this._options.ignoreReadonly = a
                }, r.prototype.options = function (b) {
                    if (0 === arguments.length) return a.extend(!0, {}, this._options);
                    if (!(b instanceof Object)) throw new TypeError("options() this.options parameter should be an object");
                    a.extend(!0, this._options, b);
                    var c = this;
                    a.each(this._options, function (a, b) {
                        void 0 !== c[a] && c[a](b)
                    })
                }, r.prototype.date = function (a, c) {
                    if (c = c || 0, 0 === arguments.length) return this.unset ? null : this._options.allowMultidate ? this._dates.join(this._options.multidateSeparator) : this._dates[c].clone();
                    if (!(null === a || "string" == typeof a || b.isMoment(a) || a instanceof Date)) throw new TypeError("date() parameter must be one of [null, string, moment or Date]");
                    this._setValue(null === a ? null : this._parseInputDate(a), c)
                }, r.prototype.format = function (a) {
                    if (0 === arguments.length) return this._options.format;
                    if ("string" != typeof a && ("boolean" != typeof a || a !== !1)) throw new TypeError("format() expects a string or boolean:false parameter " + a);
                    this._options.format = a, this.actualFormat && this._initFormatting()
                }, r.prototype.timeZone = function (a) {
                    if (0 === arguments.length) return this._options.timeZone;
                    if ("string" != typeof a) throw new TypeError("newZone() expects a string parameter");
                    this._options.timeZone = a
                }, r.prototype.dayViewHeaderFormat = function (a) {
                    if (0 === arguments.length) return this._options.dayViewHeaderFormat;
                    if ("string" != typeof a) throw new TypeError("dayViewHeaderFormat() expects a string parameter");
                    this._options.dayViewHeaderFormat = a
                }, r.prototype.extraFormats = function (a) {
                    if (0 === arguments.length) return this._options.extraFormats;
                    if (a !== !1 && !(a instanceof Array)) throw new TypeError("extraFormats() expects an array or false parameter");
                    this._options.extraFormats = a, this.parseFormats && this._initFormatting()
                }, r.prototype.disabledDates = function (b) {
                    if (0 === arguments.length) return this._options.disabledDates ? a.extend({}, this._options.disabledDates) : this._options.disabledDates;
                    if (!b) return this._options.disabledDates = !1, this._update(), !0;
                    if (!(b instanceof Array)) throw new TypeError("disabledDates() expects an array parameter");
                    this._options.disabledDates = this._indexGivenDates(b), this._options.enabledDates = !1, this._update()
                }, r.prototype.enabledDates = function (b) {
                    if (0 === arguments.length) return this._options.enabledDates ? a.extend({}, this._options.enabledDates) : this._options.enabledDates;
                    if (!b) return this._options.enabledDates = !1, this._update(), !0;
                    if (!(b instanceof Array)) throw new TypeError("enabledDates() expects an array parameter");
                    this._options.enabledDates = this._indexGivenDates(b), this._options.disabledDates = !1, this._update()
                }, r.prototype.daysOfWeekDisabled = function (a) {
                    if (0 === arguments.length) return this._options.daysOfWeekDisabled.splice(0);
                    if ("boolean" == typeof a && !a) return this._options.daysOfWeekDisabled = !1, this._update(), !0;
                    if (!(a instanceof Array)) throw new TypeError("daysOfWeekDisabled() expects an array parameter");
                    if (this._options.daysOfWeekDisabled = a.reduce(function (a, b) {
                        return b = parseInt(b, 10), b > 6 || b < 0 || isNaN(b) ? a : (a.indexOf(b) === -1 && a.push(b), a)
                    }, []).sort(), this._options.useCurrent && !this._options.keepInvalid) for (var b = 0; b < this._dates.length; b++) {
                        for (var c = 0; !this._isValid(this._dates[b], "d");) {
                            if (this._dates[b].add(1, "d"), 31 === c) throw"Tried 31 times to find a valid date";
                            c++
                        }
                        this._setValue(this._dates[b], b)
                    }
                    this._update()
                }, r.prototype.maxDate = function (a) {
                    if (0 === arguments.length) return this._options.maxDate ? this._options.maxDate.clone() : this._options.maxDate;
                    if ("boolean" == typeof a && a === !1) return this._options.maxDate = !1, this._update(), !0;
                    "string" == typeof a && ("now" !== a && "moment" !== a || (a = this.getMoment()));
                    var b = this._parseInputDate(a);
                    if (!b.isValid()) throw new TypeError("maxDate() Could not parse date parameter: " + a);
                    if (this._options.minDate && b.isBefore(this._options.minDate)) throw new TypeError("maxDate() date parameter is before this.options.minDate: " + b.format(this.actualFormat));
                    this._options.maxDate = b;
                    for (var c = 0; c < this._dates.length; c++) this._options.useCurrent && !this._options.keepInvalid && this._dates[c].isAfter(a) && this._setValue(this._options.maxDate, c);
                    this._viewDate.isAfter(b) && (this._viewDate = b.clone().subtract(this._options.stepping, "m")), this._update()
                }, r.prototype.minDate = function (a) {
                    if (0 === arguments.length) return this._options.minDate ? this._options.minDate.clone() : this._options.minDate;
                    if ("boolean" == typeof a && a === !1) return this._options.minDate = !1, this._update(), !0;
                    "string" == typeof a && ("now" !== a && "moment" !== a || (a = this.getMoment()));
                    var b = this._parseInputDate(a);
                    if (!b.isValid()) throw new TypeError("minDate() Could not parse date parameter: " + a);
                    if (this._options.maxDate && b.isAfter(this._options.maxDate)) throw new TypeError("minDate() date parameter is after this.options.maxDate: " + b.format(this.actualFormat));
                    this._options.minDate = b;
                    for (var c = 0; c < this._dates.length; c++) this._options.useCurrent && !this._options.keepInvalid && this._dates[c].isBefore(a) && this._setValue(this._options.minDate, c);
                    this._viewDate.isBefore(b) && (this._viewDate = b.clone().add(this._options.stepping, "m")), this._update()
                }, r.prototype.defaultDate = function (a) {
                    if (0 === arguments.length) return this._options.defaultDate ? this._options.defaultDate.clone() : this._options.defaultDate;
                    if (!a) return this._options.defaultDate = !1, !0;
                    "string" == typeof a && (a = "now" === a || "moment" === a ? this.getMoment() : this.getMoment(a));
                    var b = this._parseInputDate(a);
                    if (!b.isValid()) throw new TypeError("defaultDate() Could not parse date parameter: " + a);
                    if (!this._isValid(b)) throw new TypeError("defaultDate() date passed is invalid according to component setup validations");
                    this._options.defaultDate = b, (this._options.defaultDate && this._options.inline || void 0 !== this.input && "" === this.input.val().trim()) && this._setValue(this._options.defaultDate, 0)
                }, r.prototype.locale = function (a) {
                    if (0 === arguments.length) return this._options.locale;
                    if (!b.localeData(a)) throw new TypeError("locale() locale " + a + " is not loaded from moment locales!");
                    this._options.locale = a;
                    for (var c = 0; c < this._dates.length; c++) this._dates[c].locale(this._options.locale);
                    this._viewDate.locale(this._options.locale), this.actualFormat && this._initFormatting(), this.widget && (this.hide(), this.show())
                }, r.prototype.stepping = function (a) {
                    return 0 === arguments.length ? this._options.stepping : (a = parseInt(a, 10), (isNaN(a) || a < 1) && (a = 1), void (this._options.stepping = a))
                }, r.prototype.useCurrent = function (a) {
                    var b = ["year", "month", "day", "hour", "minute"];
                    if (0 === arguments.length) return this._options.useCurrent;
                    if ("boolean" != typeof a && "string" != typeof a) throw new TypeError("useCurrent() expects a boolean or string parameter");
                    if ("string" == typeof a && b.indexOf(a.toLowerCase()) === -1) throw new TypeError("useCurrent() expects a string parameter of " + b.join(", "));
                    this._options.useCurrent = a
                }, r.prototype.collapse = function (a) {
                    if (0 === arguments.length) return this._options.collapse;
                    if ("boolean" != typeof a) throw new TypeError("collapse() expects a boolean parameter");
                    return this._options.collapse === a || (this._options.collapse = a, void (this.widget && (this.hide(), this.show())))
                }, r.prototype.icons = function (b) {
                    if (0 === arguments.length) return a.extend({}, this._options.icons);
                    if (!(b instanceof Object)) throw new TypeError("icons() expects parameter to be an Object");
                    a.extend(this._options.icons, b), this.widget && (this.hide(), this.show())
                }, r.prototype.tooltips = function (b) {
                    if (0 === arguments.length) return a.extend({}, this._options.tooltips);
                    if (!(b instanceof Object)) throw new TypeError("tooltips() expects parameter to be an Object");
                    a.extend(this._options.tooltips, b), this.widget && (this.hide(), this.show())
                }, r.prototype.useStrict = function (a) {
                    if (0 === arguments.length) return this._options.useStrict;
                    if ("boolean" != typeof a) throw new TypeError("useStrict() expects a boolean parameter");
                    this._options.useStrict = a
                }, r.prototype.sideBySide = function (a) {
                    if (0 === arguments.length) return this._options.sideBySide;
                    if ("boolean" != typeof a) throw new TypeError("sideBySide() expects a boolean parameter");
                    this._options.sideBySide = a, this.widget && (this.hide(), this.show())
                }, r.prototype.viewMode = function (a) {
                    if (0 === arguments.length) return this._options.viewMode;
                    if ("string" != typeof a) throw new TypeError("viewMode() expects a string parameter");
                    if (r.ViewModes.indexOf(a) === -1) throw new TypeError("viewMode() parameter must be one of (" + r.ViewModes.join(", ") + ") value");
                    this._options.viewMode = a, this.currentViewMode = Math.max(r.ViewModes.indexOf(a) - 1, this.MinViewModeNumber), this._showMode()
                }, r.prototype.calendarWeeks = function (a) {
                    if (0 === arguments.length) return this._options.calendarWeeks;
                    if ("boolean" != typeof a) throw new TypeError("calendarWeeks() expects parameter to be a boolean value");
                    this._options.calendarWeeks = a, this._update()
                }, r.prototype.buttons = function (b) {
                    if (0 === arguments.length) return a.extend({}, this._options.buttons);
                    if (!(b instanceof Object)) throw new TypeError("buttons() expects parameter to be an Object");
                    if (a.extend(this._options.buttons, b), "boolean" != typeof this._options.buttons.showToday) throw new TypeError("buttons.showToday expects a boolean parameter");
                    if ("boolean" != typeof this._options.buttons.showClear) throw new TypeError("buttons.showClear expects a boolean parameter");
                    if ("boolean" != typeof this._options.buttons.showClose) throw new TypeError("buttons.showClose expects a boolean parameter");
                    this.widget && (this.hide(), this.show())
                }, r.prototype.keepOpen = function (a) {
                    if (0 === arguments.length) return this._options.keepOpen;
                    if ("boolean" != typeof a) throw new TypeError("keepOpen() expects a boolean parameter");
                    this._options.keepOpen = a
                }, r.prototype.focusOnShow = function (a) {
                    if (0 === arguments.length) return this._options.focusOnShow;
                    if ("boolean" != typeof a) throw new TypeError("focusOnShow() expects a boolean parameter");
                    this._options.focusOnShow = a
                }, r.prototype.inline = function (a) {
                    if (0 === arguments.length) return this._options.inline;
                    if ("boolean" != typeof a) throw new TypeError("inline() expects a boolean parameter");
                    this._options.inline = a
                }, r.prototype.clear = function () {
                    this._setValue(null)
                }, r.prototype.keyBinds = function (a) {
                    return 0 === arguments.length ? this._options.keyBinds : void (this._options.keyBinds = a)
                }, r.prototype.debug = function (a) {
                    if ("boolean" != typeof a) throw new TypeError("debug() expects a boolean parameter");
                    this._options.debug = a
                }, r.prototype.allowInputToggle = function (a) {
                    if (0 === arguments.length) return this._options.allowInputToggle;
                    if ("boolean" != typeof a) throw new TypeError("allowInputToggle() expects a boolean parameter");
                    this._options.allowInputToggle = a
                }, r.prototype.keepInvalid = function (a) {
                    if (0 === arguments.length) return this._options.keepInvalid;
                    if ("boolean" != typeof a) throw new TypeError("keepInvalid() expects a boolean parameter");
                    this._options.keepInvalid = a
                }, r.prototype.datepickerInput = function (a) {
                    if (0 === arguments.length) return this._options.datepickerInput;
                    if ("string" != typeof a) throw new TypeError("datepickerInput() expects a string parameter");
                    this._options.datepickerInput = a
                }, r.prototype.parseInputDate = function (a) {
                    if (0 === arguments.length) return this._options.parseInputDate;
                    if ("function" != typeof a) throw new TypeError("parseInputDate() should be as function");
                    this._options.parseInputDate = a
                }, r.prototype.disabledTimeIntervals = function (b) {
                    if (0 === arguments.length) return this._options.disabledTimeIntervals ? a.extend({}, this._options.disabledTimeIntervals) : this._options.disabledTimeIntervals;
                    if (!b) return this._options.disabledTimeIntervals = !1, this._update(), !0;
                    if (!(b instanceof Array)) throw new TypeError("disabledTimeIntervals() expects an array parameter");
                    this._options.disabledTimeIntervals = b, this._update()
                }, r.prototype.disabledHours = function (b) {
                    if (0 === arguments.length) return this._options.disabledHours ? a.extend({}, this._options.disabledHours) : this._options.disabledHours;
                    if (!b) return this._options.disabledHours = !1, this._update(), !0;
                    if (!(b instanceof Array)) throw new TypeError("disabledHours() expects an array parameter");
                    if (this._options.disabledHours = this._indexGivenHours(b), this._options.enabledHours = !1, this._options.useCurrent && !this._options.keepInvalid) for (var c = 0; c < this._dates.length; c++) {
                        for (var d = 0; !this._isValid(this._dates[c], "h");) {
                            if (this._dates[c].add(1, "h"), 24 === d) throw"Tried 24 times to find a valid date";
                            d++
                        }
                        this._setValue(this._dates[c], c)
                    }
                    this._update()
                }, r.prototype.enabledHours = function (b) {
                    if (0 === arguments.length) return this._options.enabledHours ? a.extend({}, this._options.enabledHours) : this._options.enabledHours;
                    if (!b) return this._options.enabledHours = !1, this._update(), !0;
                    if (!(b instanceof Array)) throw new TypeError("enabledHours() expects an array parameter");
                    if (this._options.enabledHours = this._indexGivenHours(b), this._options.disabledHours = !1, this._options.useCurrent && !this._options.keepInvalid) for (var c = 0; c < this._dates.length; c++) {
                        for (var d = 0; !this._isValid(this._dates[c], "h");) {
                            if (this._dates[c].add(1, "h"), 24 === d) throw"Tried 24 times to find a valid date";
                            d++
                        }
                        this._setValue(this._dates[c], c)
                    }
                    this._update()
                }, r.prototype.viewDate = function (a) {
                    if (0 === arguments.length) return this._viewDate.clone();
                    if (!a) return this._viewDate = (this._dates[0] || this.getMoment()).clone(), !0;
                    if (!("string" == typeof a || b.isMoment(a) || a instanceof Date)) throw new TypeError("viewDate() parameter must be one of [string, moment or Date]");
                    this._viewDate = this._parseInputDate(a), this._viewUpdate()
                }, r.prototype.allowMultidate = function (a) {
                    if ("boolean" != typeof a) throw new TypeError("allowMultidate() expects a boolean parameter");
                    this._options.allowMultidate = a
                }, r.prototype.multidateSeparator = function (a) {
                    if (0 === arguments.length) return this._options.multidateSeparator;
                    if ("string" != typeof a || a.length > 1) throw new TypeError("multidateSeparator expects a single character string parameter");
                    this._options.multidateSeparator = a
                }, e(r, null, [{
                    key: "NAME", get: function () {
                        return d
                    }
                }, {
                    key: "DATA_KEY", get: function () {
                        return f
                    }
                }, {
                    key: "EVENT_KEY", get: function () {
                        return g
                    }
                }, {
                    key: "DATA_API_KEY", get: function () {
                        return h
                    }
                }, {
                    key: "DatePickerModes", get: function () {
                        return l
                    }
                }, {
                    key: "ViewModes", get: function () {
                        return n
                    }
                }, {
                    key: "Event", get: function () {
                        return k
                    }
                }, {
                    key: "Selector", get: function () {
                        return i
                    }
                }, {
                    key: "Default", get: function () {
                        return q
                    }, set: function (a) {
                        q = a
                    }
                }, {
                    key: "ClassName", get: function () {
                        return j
                    }
                }]), r
            }();
        return r
    }(jQuery, moment);
    (function (e) {
        var g = e.fn[f.NAME], h = ["top", "bottom", "auto"], i = ["left", "right", "auto"],
            j = ["default", "top", "bottom"], k = function (a) {
                var b = a.data("target"), c = void 0;
                return b || (b = a.attr("href") || "", b = /^#[a-z]/i.test(b) ? b : null), c = e(b), 0 === c.length ? c : (c.data(f.DATA_KEY) || e.extend({}, c.data(), e(this).data()), c)
            }, l = function (g) {
                function k(b, d) {
                    c(this, k);
                    var e = a(this, g.call(this, b, d));
                    return e._init(), e
                }

                return b(k, g), k.prototype._init = function () {
                    if (this._element.hasClass("input-group")) {
                        var a = this._element.find(".datepickerbutton");
                        0 === a.length ? this.component = this._element.find('[data-toggle="datetimepicker"]') : this.component = a
                    }
                }, k.prototype._getDatePickerTemplate = function () {
                    var a = e("<thead>").append(e("<tr>").append(e("<th>").addClass("prev").attr("data-action", "previous").append(e("<span>").addClass(this._options.icons.previous))).append(e("<th>").addClass("picker-switch").attr("data-action", "pickerSwitch").attr("colspan", "" + (this._options.calendarWeeks ? "6" : "5"))).append(e("<th>").addClass("next").attr("data-action", "next").append(e("<span>").addClass(this._options.icons.next)))),
                        b = e("<tbody>").append(e("<tr>").append(e("<td>").attr("colspan", "" + (this._options.calendarWeeks ? "8" : "7"))));
                    return [e("<div>").addClass("datepicker-days").append(e("<table>").addClass("table table-sm").append(a).append(e("<tbody>"))), e("<div>").addClass("datepicker-months").append(e("<table>").addClass("table-condensed").append(a.clone()).append(b.clone())), e("<div>").addClass("datepicker-years").append(e("<table>").addClass("table-condensed").append(a.clone()).append(b.clone())), e("<div>").addClass("datepicker-decades").append(e("<table>").addClass("table-condensed").append(a.clone()).append(b.clone()))]
                }, k.prototype._getTimePickerMainTemplate = function () {
                    var a = e("<tr>"), b = e("<tr>"), c = e("<tr>");
                    return this._isEnabled("h") && (a.append(e("<td>").append(e("<a>").attr({
                        href: "#",
                        tabindex: "-1",
                        title: this._options.tooltips.incrementHour
                    }).addClass("btn").attr("data-action", "incrementHours").append(e("<span>").addClass(this._options.icons.up)))), b.append(e("<td>").append(e("<span>").addClass("timepicker-hour").attr({
                        "data-time-component": "hours",
                        title: this._options.tooltips.pickHour
                    }).attr("data-action", "showHours"))), c.append(e("<td>").append(e("<a>").attr({
                        href: "#",
                        tabindex: "-1",
                        title: this._options.tooltips.decrementHour
                    }).addClass("btn").attr("data-action", "decrementHours").append(e("<span>").addClass(this._options.icons.down))))), this._isEnabled("m") && (this._isEnabled("h") && (a.append(e("<td>").addClass("separator")), b.append(e("<td>").addClass("separator").html(":")), c.append(e("<td>").addClass("separator"))), a.append(e("<td>").append(e("<a>").attr({
                        href: "#",
                        tabindex: "-1",
                        title: this._options.tooltips.incrementMinute
                    }).addClass("btn").attr("data-action", "incrementMinutes").append(e("<span>").addClass(this._options.icons.up)))), b.append(e("<td>").append(e("<span>").addClass("timepicker-minute").attr({
                        "data-time-component": "minutes",
                        title: this._options.tooltips.pickMinute
                    }).attr("data-action", "showMinutes"))), c.append(e("<td>").append(e("<a>").attr({
                        href: "#",
                        tabindex: "-1",
                        title: this._options.tooltips.decrementMinute
                    }).addClass("btn").attr("data-action", "decrementMinutes").append(e("<span>").addClass(this._options.icons.down))))), this._isEnabled("s") && (this._isEnabled("m") && (a.append(e("<td>").addClass("separator")), b.append(e("<td>").addClass("separator").html(":")), c.append(e("<td>").addClass("separator"))), a.append(e("<td>").append(e("<a>").attr({
                        href: "#",
                        tabindex: "-1",
                        title: this._options.tooltips.incrementSecond
                    }).addClass("btn").attr("data-action", "incrementSeconds").append(e("<span>").addClass(this._options.icons.up)))), b.append(e("<td>").append(e("<span>").addClass("timepicker-second").attr({
                        "data-time-component": "seconds",
                        title: this._options.tooltips.pickSecond
                    }).attr("data-action", "showSeconds"))), c.append(e("<td>").append(e("<a>").attr({
                        href: "#",
                        tabindex: "-1",
                        title: this._options.tooltips.decrementSecond
                    }).addClass("btn").attr("data-action", "decrementSeconds").append(e("<span>").addClass(this._options.icons.down))))), this.use24Hours || (a.append(e("<td>").addClass("separator")), b.append(e("<td>").append(e("<button>").addClass("btn btn-primary").attr({
                        "data-action": "togglePeriod",
                        tabindex: "-1",
                        title: this._options.tooltips.togglePeriod
                    }))), c.append(e("<td>").addClass("separator"))), e("<div>").addClass("timepicker-picker").append(e("<table>").addClass("table-condensed").append([a, b, c]))
                }, k.prototype._getTimePickerTemplate = function () {
                    var a = e("<div>").addClass("timepicker-hours").append(e("<table>").addClass("table-condensed")),
                        b = e("<div>").addClass("timepicker-minutes").append(e("<table>").addClass("table-condensed")),
                        c = e("<div>").addClass("timepicker-seconds").append(e("<table>").addClass("table-condensed")),
                        d = [this._getTimePickerMainTemplate()];
                    return this._isEnabled("h") && d.push(a), this._isEnabled("m") && d.push(b), this._isEnabled("s") && d.push(c), d
                }, k.prototype._getToolbar = function () {
                    var a = [];
                    if (this._options.buttons.showToday && a.push(e("<td>").append(e("<a>").attr({
                        href: "#",
                        tabindex: "-1",
                        "data-action": "today",
                        title: this._options.tooltips.today
                    }).append(e("<span>").addClass(this._options.icons.today)))), !this._options.sideBySide && this._hasDate() && this._hasTime()) {
                        var b = void 0, c = void 0;
                        "times" === this._options.viewMode ? (b = this._options.tooltips.selectDate, c = this._options.icons.date) : (b = this._options.tooltips.selectTime, c = this._options.icons.time), a.push(e("<td>").append(e("<a>").attr({
                            href: "#",
                            tabindex: "-1",
                            "data-action": "togglePicker",
                            title: b
                        }).append(e("<span>").addClass(c))))
                    }
                    return this._options.buttons.showClear && a.push(e("<td>").append(e("<a>").attr({
                        href: "#",
                        tabindex: "-1",
                        "data-action": "clear",
                        title: this._options.tooltips.clear
                    }).append(e("<span>").addClass(this._options.icons.clear)))), this._options.buttons.showClose && a.push(e("<td>").append(e("<a>").attr({
                        href: "#",
                        tabindex: "-1",
                        "data-action": "close",
                        title: this._options.tooltips.close
                    }).append(e("<span>").addClass(this._options.icons.close)))), 0 === a.length ? "" : e("<table>").addClass("table-condensed").append(e("<tbody>").append(e("<tr>").append(a)))
                }, k.prototype._getTemplate = function () {
                    var a = e("<div>").addClass("bootstrap-datetimepicker-widget dropdown-menu"),
                        b = e("<div>").addClass("datepicker").append(this._getDatePickerTemplate()),
                        c = e("<div>").addClass("timepicker").append(this._getTimePickerTemplate()),
                        d = e("<ul>").addClass("list-unstyled"),
                        f = e("<li>").addClass("picker-switch" + (this._options.collapse ? " accordion-toggle" : "")).append(this._getToolbar());
                    return this._options.inline && a.removeClass("dropdown-menu"), this.use24Hours && a.addClass("usetwentyfour"), this._isEnabled("s") && !this.use24Hours && a.addClass("wider"), this._options.sideBySide && this._hasDate() && this._hasTime() ? (a.addClass("timepicker-sbs"), "top" === this._options.toolbarPlacement && a.append(f), a.append(e("<div>").addClass("row").append(b.addClass("col-md-6")).append(c.addClass("col-md-6"))), "bottom" !== this._options.toolbarPlacement && "default" !== this._options.toolbarPlacement || a.append(f), a) : ("top" === this._options.toolbarPlacement && d.append(f), this._hasDate() && d.append(e("<li>").addClass(this._options.collapse && this._hasTime() ? "collapse" : "").addClass(this._options.collapse && this._hasTime() && "times" === this._options.viewMode ? "" : "show").append(b)), "default" === this._options.toolbarPlacement && d.append(f), this._hasTime() && d.append(e("<li>").addClass(this._options.collapse && this._hasDate() ? "collapse" : "").addClass(this._options.collapse && this._hasDate() && "times" === this._options.viewMode ? "show" : "").append(c)), "bottom" === this._options.toolbarPlacement && d.append(f), a.append(d))
                }, k.prototype._place = function (a) {
                    var b = a && a.data && a.data.picker || this, c = b._options.widgetPositioning.vertical,
                        d = b._options.widgetPositioning.horizontal, f = void 0,
                        g = (b.component && b.component.length ? b.component : b._element).position(),
                        h = (b.component && b.component.length ? b.component : b._element).offset();
                    if (b._options.widgetParent) f = b._options.widgetParent.append(b.widget); else if (b._element.is("input")) f = b._element.after(b.widget).parent(); else {
                        if (b._options.inline) return void (f = b._element.append(b.widget));
                        f = b._element, b._element.children().first().after(b.widget)
                    }
                    if ("auto" === c && (c = h.top + 1.5 * b.widget.height() >= e(window).height() + e(window).scrollTop() && b.widget.height() + b._element.outerHeight() < h.top ? "top" : "bottom"), "auto" === d && (d = f.width() < h.left + b.widget.outerWidth() / 2 && h.left + b.widget.outerWidth() > e(window).width() ? "right" : "left"), "top" === c ? b.widget.addClass("top").removeClass("bottom") : b.widget.addClass("bottom").removeClass("top"), "right" === d ? b.widget.addClass("float-right") : b.widget.removeClass("float-right"), "relative" !== f.css("position") && (f = f.parents().filter(function () {
                        return "relative" === e(this).css("position")
                    }).first()), 0 === f.length) throw new Error("datetimepicker component should be placed within a relative positioned container");
                    b.widget.css({
                        top: "top" === c ? "auto" : g.top + b._element.outerHeight() + "px",
                        bottom: "top" === c ? f.outerHeight() - (f === b._element ? 0 : g.top) + "px" : "auto",
                        left: "left" === d ? (f === b._element ? 0 : g.left) + "px" : "auto",
                        right: "left" === d ? "auto" : f.outerWidth() - b._element.outerWidth() - (f === b._element ? 0 : g.left) + "px"
                    })
                }, k.prototype._fillDow = function () {
                    var a = e("<tr>"), b = this._viewDate.clone().startOf("w").startOf("d");
                    for (this._options.calendarWeeks === !0 && a.append(e("<th>").addClass("cw").text("#")); b.isBefore(this._viewDate.clone().endOf("w"));) a.append(e("<th>").addClass("dow").text(b.format("dd"))), b.add(1, "d");
                    this.widget.find(".datepicker-days thead").append(a)
                }, k.prototype._fillMonths = function () {
                    for (var a = [], b = this._viewDate.clone().startOf("y").startOf("d"); b.isSame(this._viewDate, "y");) a.push(e("<span>").attr("data-action", "selectMonth").addClass("month").text(b.format("MMM"))), b.add(1, "M");
                    this.widget.find(".datepicker-months td").empty().append(a)
                }, k.prototype._updateMonths = function () {
                    var a = this.widget.find(".datepicker-months"), b = a.find("th"), c = a.find("tbody").find("span"),
                        d = this;
                    b.eq(0).find("span").attr("title", this._options.tooltips.prevYear), b.eq(1).attr("title", this._options.tooltips.selectYear), b.eq(2).find("span").attr("title", this._options.tooltips.nextYear), a.find(".disabled").removeClass("disabled"), this._isValid(this._viewDate.clone().subtract(1, "y"), "y") || b.eq(0).addClass("disabled"), b.eq(1).text(this._viewDate.year()), this._isValid(this._viewDate.clone().add(1, "y"), "y") || b.eq(2).addClass("disabled"), c.removeClass("active"), this._getLastPickedDate().isSame(this._viewDate, "y") && !this.unset && c.eq(this._getLastPickedDate().month()).addClass("active"), c.each(function (a) {
                        d._isValid(d._viewDate.clone().month(a), "M") || e(this).addClass("disabled")
                    })
                }, k.prototype._getStartEndYear = function (a, b) {
                    var c = a / 10, d = Math.floor(b / a) * a, e = d + 9 * c, f = Math.floor(b / c) * c;
                    return [d, e, f]
                }, k.prototype._updateYears = function () {
                    var a = this.widget.find(".datepicker-years"), b = a.find("th"),
                        c = this._getStartEndYear(10, this._viewDate.year()), d = this._viewDate.clone().year(c[0]),
                        e = this._viewDate.clone().year(c[1]), f = "";
                    for (b.eq(0).find("span").attr("title", this._options.tooltips.prevDecade), b.eq(1).attr("title", this._options.tooltips.selectDecade), b.eq(2).find("span").attr("title", this._options.tooltips.nextDecade), a.find(".disabled").removeClass("disabled"), this._options.minDate && this._options.minDate.isAfter(d, "y") && b.eq(0).addClass("disabled"), b.eq(1).text(d.year() + "-" + e.year()), this._options.maxDate && this._options.maxDate.isBefore(e, "y") && b.eq(2).addClass("disabled"), f += '<span data-action="selectYear" class="year old' + (this._isValid(d, "y") ? "" : " disabled") + '">' + (d.year() - 1) + "</span>"; !d.isAfter(e, "y");) f += '<span data-action="selectYear" class="year' + (d.isSame(this._getLastPickedDate(), "y") && !this.unset ? " active" : "") + (this._isValid(d, "y") ? "" : " disabled") + '">' + d.year() + "</span>", d.add(1, "y");
                    f += '<span data-action="selectYear" class="year old' + (this._isValid(d, "y") ? "" : " disabled") + '">' + d.year() + "</span>", a.find("td").html(f)
                }, k.prototype._updateDecades = function () {
                    var a = this.widget.find(".datepicker-decades"), b = a.find("th"),
                        c = this._getStartEndYear(100, this._viewDate.year()), d = this._viewDate.clone().year(c[0]),
                        e = this._viewDate.clone().year(c[1]), f = !1, g = !1, h = void 0, i = "";
                    for (b.eq(0).find("span").attr("title", this._options.tooltips.prevCentury), b.eq(2).find("span").attr("title", this._options.tooltips.nextCentury), a.find(".disabled").removeClass("disabled"), (0 === d.year() || this._options.minDate && this._options.minDate.isAfter(d, "y")) && b.eq(0).addClass("disabled"), b.eq(1).text(d.year() + "-" + e.year()), this._options.maxDate && this._options.maxDate.isBefore(e, "y") && b.eq(2).addClass("disabled"), i += d.year() - 10 < 0 ? "<span>&nbsp;</span>" : '<span data-action="selectDecade" class="decade old" data-selection="' + (d.year() + 6) + '">' + (d.year() - 10) + "</span>"; !d.isAfter(e, "y");) h = d.year() + 11, f = this._options.minDate && this._options.minDate.isAfter(d, "y") && this._options.minDate.year() <= h, g = this._options.maxDate && this._options.maxDate.isAfter(d, "y") && this._options.maxDate.year() <= h, i += '<span data-action="selectDecade" class="decade' + (this._getLastPickedDate().isAfter(d) && this._getLastPickedDate().year() <= h ? " active" : "") + (this._isValid(d, "y") || f || g ? "" : " disabled") + '" data-selection="' + (d.year() + 6) + '">' + d.year() + "</span>", d.add(10, "y");
                    i += '<span data-action="selectDecade" class="decade old" data-selection="' + (d.year() + 6) + '">' + d.year() + "</span>", a.find("td").html(i)
                }, k.prototype._fillDate = function () {
                    var a = this.widget.find(".datepicker-days"), b = a.find("th"), c = [], d = void 0, f = void 0,
                        g = void 0, h = void 0;
                    if (this._hasDate()) {
                        for (b.eq(0).find("span").attr("title", this._options.tooltips.prevMonth), b.eq(1).attr("title", this._options.tooltips.selectMonth), b.eq(2).find("span").attr("title", this._options.tooltips.nextMonth), a.find(".disabled").removeClass("disabled"), b.eq(1).text(this._viewDate.format(this._options.dayViewHeaderFormat)), this._isValid(this._viewDate.clone().subtract(1, "M"), "M") || b.eq(0).addClass("disabled"), this._isValid(this._viewDate.clone().add(1, "M"), "M") || b.eq(2).addClass("disabled"), d = this._viewDate.clone().startOf("M").startOf("w").startOf("d"), h = 0; h < 42; h++) {
                            if (0 === d.weekday() && (f = e("<tr>"), this._options.calendarWeeks && f.append('<td class="cw">' + d.week() + "</td>"), c.push(f)), g = "", d.isBefore(this._viewDate, "M") && (g += " old"), d.isAfter(this._viewDate, "M") && (g += " new"), this._options.allowMultidate) {
                                var i = this._datesFormatted.indexOf(d.format("YYYY-MM-DD"));
                                i !== -1 && d.isSame(this._datesFormatted[i], "d") && !this.unset && (g += " active")
                            } else d.isSame(this._getLastPickedDate(), "d") && !this.unset && (g += " active");
                            this._isValid(d, "d") || (g += " disabled"), d.isSame(this.getMoment(), "d") && (g += " today"), 0 !== d.day() && 6 !== d.day() || (g += " weekend"), f.append('<td data-action="selectDay" data-day="' + d.format("L") + '" class="day' + g + '">' + d.date() + "</td>"), d.add(1, "d")
                        }
                        a.find("tbody").empty().append(c), this._updateMonths(), this._updateYears(), this._updateDecades()
                    }
                }, k.prototype._fillHours = function () {
                    var a = this.widget.find(".timepicker-hours table"), b = this._viewDate.clone().startOf("d"), c = [],
                        d = e("<tr>");
                    for (this._viewDate.hour() > 11 && !this.use24Hours && b.hour(12); b.isSame(this._viewDate, "d") && (this.use24Hours || this._viewDate.hour() < 12 && b.hour() < 12 || this._viewDate.hour() > 11);) b.hour() % 4 === 0 && (d = e("<tr>"), c.push(d)), d.append('<td data-action="selectHour" class="hour' + (this._isValid(b, "h") ? "" : " disabled") + '">' + b.format(this.use24Hours ? "HH" : "hh") + "</td>"), b.add(1, "h");
                    a.empty().append(c)
                }, k.prototype._fillMinutes = function () {
                    for (var a = this.widget.find(".timepicker-minutes table"), b = this._viewDate.clone().startOf("h"), c = [], d = 1 === this._options.stepping ? 5 : this._options.stepping, f = e("<tr>"); this._viewDate.isSame(b, "h");) b.minute() % (4 * d) === 0 && (f = e("<tr>"), c.push(f)), f.append('<td data-action="selectMinute" class="minute' + (this._isValid(b, "m") ? "" : " disabled") + '">' + b.format("mm") + "</td>"), b.add(d, "m");
                    a.empty().append(c)
                }, k.prototype._fillSeconds = function () {
                    for (var a = this.widget.find(".timepicker-seconds table"), b = this._viewDate.clone().startOf("m"), c = [], d = e("<tr>"); this._viewDate.isSame(b, "m");) b.second() % 20 === 0 && (d = e("<tr>"), c.push(d)), d.append('<td data-action="selectSecond" class="second' + (this._isValid(b, "s") ? "" : " disabled") + '">' + b.format("ss") + "</td>"), b.add(5, "s");
                    a.empty().append(c)
                }, k.prototype._fillTime = function () {
                    var a = void 0, b = void 0, c = this.widget.find(".timepicker span[data-time-component]");
                    this.use24Hours || (a = this.widget.find(".timepicker [data-action=togglePeriod]"), b = this._getLastPickedDate().clone().add(this._getLastPickedDate().hours() >= 12 ? -12 : 12, "h"), a.text(this._getLastPickedDate().format("A")), this._isValid(b, "h") ? a.removeClass("disabled") : a.addClass("disabled")), c.filter("[data-time-component=hours]").text(this._getLastPickedDate().format("" + (this.use24Hours ? "HH" : "hh"))), c.filter("[data-time-component=minutes]").text(this._getLastPickedDate().format("mm")), c.filter("[data-time-component=seconds]").text(this._getLastPickedDate().format("ss")), this._fillHours(), this._fillMinutes(), this._fillSeconds()
                }, k.prototype._doAction = function (a, b) {
                    var c = this._getLastPickedDate();
                    if (e(a.currentTarget).is(".disabled")) return !1;
                    switch (b = b || e(a.currentTarget).data("action")) {
                        case"next":
                            var d = f.DatePickerModes[this.currentViewMode].NAV_FUNCTION;
                            this._viewDate.add(f.DatePickerModes[this.currentViewMode].NAV_STEP, d), this._fillDate(), this._viewUpdate(d);
                            break;
                        case"previous":
                            var g = f.DatePickerModes[this.currentViewMode].NAV_FUNCTION;
                            this._viewDate.subtract(f.DatePickerModes[this.currentViewMode].NAV_STEP, g), this._fillDate(), this._viewUpdate(g);
                            break;
                        case"pickerSwitch":
                            this._showMode(1);
                            break;
                        case"selectMonth":
                            var h = e(a.target).closest("tbody").find("span").index(e(a.target));
                            this._viewDate.month(h), this.currentViewMode === this.MinViewModeNumber ? (this._setValue(c.clone().year(this._viewDate.year()).month(this._viewDate.month()), this._getLastPickedDateIndex()), this._options.inline || this.hide()) : (this._showMode(-1), this._fillDate()), this._viewUpdate("M");
                            break;
                        case"selectYear":
                            var i = parseInt(e(a.target).text(), 10) || 0;
                            this._viewDate.year(i), this.currentViewMode === this.MinViewModeNumber ? (this._setValue(c.clone().year(this._viewDate.year()), this._getLastPickedDateIndex()), this._options.inline || this.hide()) : (this._showMode(-1), this._fillDate()), this._viewUpdate("YYYY");
                            break;
                        case"selectDecade":
                            var j = parseInt(e(a.target).data("selection"), 10) || 0;
                            this._viewDate.year(j), this.currentViewMode === this.MinViewModeNumber ? (this._setValue(c.clone().year(this._viewDate.year()), this._getLastPickedDateIndex()), this._options.inline || this.hide()) : (this._showMode(-1), this._fillDate()), this._viewUpdate("YYYY");
                            break;
                        case"selectDay":
                            var k = this._viewDate.clone();
                            e(a.target).is(".old") && k.subtract(1, "M"), e(a.target).is(".new") && k.add(1, "M");
                            var l = k.date(parseInt(e(a.target).text(), 10)), m = 0;
                            this._options.allowMultidate ? (m = this._datesFormatted.indexOf(l.format("YYYY-MM-DD")), m !== -1 ? this._setValue(null, m) : this._setValue(l, this._getLastPickedDateIndex() + 1)) : this._setValue(l, this._getLastPickedDateIndex()), this._hasTime() || this._options.keepOpen || this._options.inline || this._options.allowMultidate || this.hide();
                            break;
                        case"incrementHours":
                            var n = c.clone().add(1, "h");
                            this._isValid(n, "h") && this._setValue(n, this._getLastPickedDateIndex());
                            break;
                        case"incrementMinutes":
                            var o = c.clone().add(this._options.stepping, "m");
                            this._isValid(o, "m") && this._setValue(o, this._getLastPickedDateIndex());
                            break;
                        case"incrementSeconds":
                            var p = c.clone().add(1, "s");
                            this._isValid(p, "s") && this._setValue(p, this._getLastPickedDateIndex());
                            break;
                        case"decrementHours":
                            var q = c.clone().subtract(1, "h");
                            this._isValid(q, "h") && this._setValue(q, this._getLastPickedDateIndex());
                            break;
                        case"decrementMinutes":
                            var r = c.clone().subtract(this._options.stepping, "m");
                            this._isValid(r, "m") && this._setValue(r, this._getLastPickedDateIndex());
                            break;
                        case"decrementSeconds":
                            var s = c.clone().subtract(1, "s");
                            this._isValid(s, "s") && this._setValue(s, this._getLastPickedDateIndex());
                            break;
                        case"togglePeriod":
                            this._setValue(c.clone().add(c.hours() >= 12 ? -12 : 12, "h"), this._getLastPickedDateIndex());
                            break;
                        case"togglePicker":
                            var t = e(a.target), u = t.closest("a"), v = t.closest("ul"), w = v.find(".show"),
                                x = v.find(".collapse:not(.show)"), y = t.is("span") ? t : t.find("span"), z = void 0;
                            if (w && w.length) {
                                if (z = w.data("collapse"), z && z.transitioning) return !0;
                                w.collapse ? (w.collapse("hide"), x.collapse("show")) : (w.removeClass("show"), x.addClass("show")), y.toggleClass(this._options.icons.time + " " + this._options.icons.date), y.hasClass(this._options.icons.date) ? u.attr("title", this._options.tooltips.selectDate) : u.attr("title", this._options.tooltips.selectTime)
                            }
                            break;
                        case"showPicker":
                            this.widget.find(".timepicker > div:not(.timepicker-picker)").hide(), this.widget.find(".timepicker .timepicker-picker").show();
                            break;
                        case"showHours":
                            this.widget.find(".timepicker .timepicker-picker").hide(), this.widget.find(".timepicker .timepicker-hours").show();
                            break;
                        case"showMinutes":
                            this.widget.find(".timepicker .timepicker-picker").hide(), this.widget.find(".timepicker .timepicker-minutes").show();
                            break;
                        case"showSeconds":
                            this.widget.find(".timepicker .timepicker-picker").hide(), this.widget.find(".timepicker .timepicker-seconds").show();
                            break;
                        case"selectHour":
                            var A = parseInt(e(a.target).text(), 10);
                            this.use24Hours || (c.hours() >= 12 ? 12 !== A && (A += 12) : 12 === A && (A = 0)), this._setValue(c.clone().hours(A), this._getLastPickedDateIndex()), this._isEnabled("a") || this._isEnabled("m") || this._options.keepOpen || this._options.inline ? this._doAction(a, "showPicker") : this.hide();
                            break;
                        case"selectMinute":
                            this._setValue(c.clone().minutes(parseInt(e(a.target).text(), 10)), this._getLastPickedDateIndex()), this._isEnabled("a") || this._isEnabled("s") || this._options.keepOpen || this._options.inline ? this._doAction(a, "showPicker") : this.hide();
                            break;
                        case"selectSecond":
                            this._setValue(c.clone().seconds(parseInt(e(a.target).text(), 10)), this._getLastPickedDateIndex()), this._isEnabled("a") || this._options.keepOpen || this._options.inline ? this._doAction(a, "showPicker") : this.hide();
                            break;
                        case"clear":
                            this.clear();
                            break;
                        case"close":
                            this.hide();
                            break;
                        case"today":
                            var B = this.getMoment();
                            this._isValid(B, "d") && this._setValue(B, this._getLastPickedDateIndex())
                    }
                    return !1
                }, k.prototype.hide = function () {
                    var a = !1;
                    this.widget && (this.widget.find(".collapse").each(function () {
                        var b = e(this).data("collapse");
                        return !b || !b.transitioning || (a = !0, !1)
                    }), a || (this.component && this.component.hasClass("btn") && this.component.toggleClass("active"), this.widget.hide(), e(window).off("resize", this._place()), this.widget.off("click", "[data-action]"), this.widget.off("mousedown", !1), this.widget.remove(), this.widget = !1, this._notifyEvent({
                        type: f.Event.HIDE,
                        date: this._getLastPickedDate().clone()
                    }), void 0 !== this.input && this.input.blur(), this._viewDate = this._getLastPickedDate().clone()))
                }, k.prototype.show = function () {
                    var a = void 0, b = {
                        year: function (a) {
                            return a.month(0).date(1).hours(0).seconds(0).minutes(0)
                        }, month: function (a) {
                            return a.date(1).hours(0).seconds(0).minutes(0)
                        }, day: function (a) {
                            return a.hours(0).seconds(0).minutes(0)
                        }, hour: function (a) {
                            return a.seconds(0).minutes(0)
                        }, minute: function (a) {
                            return a.seconds(0)
                        }
                    };
                    if (void 0 !== this.input) {
                        if (this.input.prop("disabled") || !this._options.ignoreReadonly && this.input.prop("readonly") || this.widget) return;
                        void 0 !== this.input.val() && 0 !== this.input.val().trim().length ? this._setValue(this._parseInputDate(this.input.val().trim()), 0) : this.unset && this._options.useCurrent && (a = this.getMoment(), "string" == typeof this._options.useCurrent && (a = b[this._options.useCurrent](a)), this._setValue(a, 0))
                    } else this.unset && this._options.useCurrent && (a = this.getMoment(), "string" == typeof this._options.useCurrent && (a = b[this._options.useCurrent](a)), this._setValue(a, 0));
                    this.widget = this._getTemplate(), this._fillDow(), this._fillMonths(), this.widget.find(".timepicker-hours").hide(), this.widget.find(".timepicker-minutes").hide(), this.widget.find(".timepicker-seconds").hide(), this._update(), this._showMode(), e(window).on("resize", {picker: this}, this._place), this.widget.on("click", "[data-action]", e.proxy(this._doAction, this)), this.widget.on("mousedown", !1), this.component && this.component.hasClass("btn") && this.component.toggleClass("active"), this._place(), this.widget.show(), void 0 !== this.input && this._options.focusOnShow && !this.input.is(":focus") && this.input.focus(), this._notifyEvent({type: f.Event.SHOW})
                }, k.prototype.destroy = function () {
                    this.hide(), this._element.removeData(f.DATA_KEY), this._element.removeData("date")
                }, k.prototype.disable = function () {
                    this.hide(), this.component && this.component.hasClass("btn") && this.component.addClass("disabled"), void 0 !== this.input && this.input.prop("disabled", !0)
                }, k.prototype.enable = function () {
                    this.component && this.component.hasClass("btn") && this.component.removeClass("disabled"), void 0 !== this.input && this.input.prop("disabled", !1)
                }, k.prototype.toolbarPlacement = function (a) {
                    if (0 === arguments.length) return this._options.toolbarPlacement;
                    if ("string" != typeof a) throw new TypeError("toolbarPlacement() expects a string parameter");
                    if (j.indexOf(a) === -1) throw new TypeError("toolbarPlacement() parameter must be one of (" + j.join(", ") + ") value");
                    this._options.toolbarPlacement = a, this.widget && (this.hide(), this.show())
                }, k.prototype.widgetPositioning = function (a) {
                    if (0 === arguments.length) return e.extend({}, this._options.widgetPositioning);
                    if ("[object Object]" !== {}.toString.call(a)) throw new TypeError("widgetPositioning() expects an object variable");
                    if (a.horizontal) {
                        if ("string" != typeof a.horizontal) throw new TypeError("widgetPositioning() horizontal variable must be a string");
                        if (a.horizontal = a.horizontal.toLowerCase(), i.indexOf(a.horizontal) === -1) throw new TypeError("widgetPositioning() expects horizontal parameter to be one of (" + i.join(", ") + ")");
                        this._options.widgetPositioning.horizontal = a.horizontal
                    }
                    if (a.vertical) {
                        if ("string" != typeof a.vertical) throw new TypeError("widgetPositioning() vertical variable must be a string");
                        if (a.vertical = a.vertical.toLowerCase(), h.indexOf(a.vertical) === -1) throw new TypeError("widgetPositioning() expects vertical parameter to be one of (" + h.join(", ") + ")");
                        this._options.widgetPositioning.vertical = a.vertical
                    }
                    this._update()
                }, k.prototype.widgetParent = function (a) {
                    if (0 === arguments.length) return this._options.widgetParent;
                    if ("string" == typeof a && (a = e(a)), null !== a && "string" != typeof a && !(a instanceof e)) throw new TypeError("widgetParent() expects a string or a jQuery object parameter");
                    this._options.widgetParent = a, this.widget && (this.hide(), this.show())
                }, k._jQueryHandleThis = function (a, b, c) {
                    var g = e(a).data(f.DATA_KEY);
                    if ("object" === ("undefined" == typeof b ? "undefined" : d(b)) && e.extend({}, f.Default, b), g || (g = new k(e(a), b), e(a).data(f.DATA_KEY, g)), "string" == typeof b) {
                        if (void 0 === g[b]) throw new Error('No method named "' + b + '"');
                        return void 0 === c ? g[b]() : g[b](c)
                    }
                }, k._jQueryInterface = function (a, b) {
                    return 1 === this.length ? k._jQueryHandleThis(this[0], a, b) : this.each(function () {
                        k._jQueryHandleThis(this, a, b)
                    })
                }, k
            }(f);
        return e(document).on(f.Event.CLICK_DATA_API, f.Selector.DATA_TOGGLE, function () {
            var a = k(e(this));
            0 !== a.length && l._jQueryInterface.call(a, "toggle")
        }).on(f.Event.CHANGE, "." + f.ClassName.INPUT, function (a) {
            var b = k(e(this));
            0 !== b.length && l._jQueryInterface.call(b, "_change", a)
        }).on(f.Event.BLUR, "." + f.ClassName.INPUT, function (a) {
            var b = k(e(this)), c = b.data(f.DATA_KEY);
            0 !== b.length && (c._options.debug || window.debug || l._jQueryInterface.call(b, "hide", a))
        }).on(f.Event.KEYDOWN, "." + f.ClassName.INPUT, function (a) {
            var b = k(e(this));
            0 !== b.length && l._jQueryInterface.call(b, "_keydown", a)
        }).on(f.Event.KEYUP, "." + f.ClassName.INPUT, function (a) {
            var b = k(e(this));
            0 !== b.length && l._jQueryInterface.call(b, "_keyup", a)
        }).on(f.Event.FOCUS, "." + f.ClassName.INPUT, function (a) {
            var b = k(e(this)), c = b.data(f.DATA_KEY);
            0 !== b.length && c._options.allowInputToggle && l._jQueryInterface.call(b, "show", a)
        }), e.fn[f.NAME] = l._jQueryInterface, e.fn[f.NAME].Constructor = l, e.fn[f.NAME].noConflict = function () {
            return e.fn[f.NAME] = g, l._jQueryInterface
        }, l
    })(jQuery)
}();
!function (t, e) {
    "object" == typeof exports && "undefined" != typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define(e) : (t = t || self).Sweetalert2 = e()
}(this, function () {
    "use strict";

    function r(t) {
        return (r = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (t) {
            return typeof t
        } : function (t) {
            return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
        })(t)
    }

    function a(t, e) {
        if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
    }

    function o(t, e) {
        for (var n = 0; n < e.length; n++) {
            var o = e[n];
            o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(t, o.key, o)
        }
    }

    function s(t, e, n) {
        return e && o(t.prototype, e), n && o(t, n), t
    }

    function u() {
        return (u = Object.assign || function (t) {
            for (var e = 1; e < arguments.length; e++) {
                var n, o = arguments[e];
                for (n in o) Object.prototype.hasOwnProperty.call(o, n) && (t[n] = o[n])
            }
            return t
        }).apply(this, arguments)
    }

    function c(t) {
        return (c = Object.setPrototypeOf ? Object.getPrototypeOf : function (t) {
            return t.__proto__ || Object.getPrototypeOf(t)
        })(t)
    }

    function l(t, e) {
        return (l = Object.setPrototypeOf || function (t, e) {
            return t.__proto__ = e, t
        })(t, e)
    }

    function d() {
        if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
        if (Reflect.construct.sham) return !1;
        if ("function" == typeof Proxy) return !0;
        try {
            return Date.prototype.toString.call(Reflect.construct(Date, [], function () {
            })), !0
        } catch (t) {
            return !1
        }
    }

    function i(t, e, n) {
        return (i = d() ? Reflect.construct : function (t, e, n) {
            var o = [null];
            o.push.apply(o, e);
            o = new (Function.bind.apply(t, o));
            return n && l(o, n.prototype), o
        }).apply(null, arguments)
    }

    function p(t, e) {
        return !e || "object" != typeof e && "function" != typeof e ? function (t) {
            if (void 0 === t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return t
        }(t) : e
    }

    function f(t, e, n) {
        return (f = "undefined" != typeof Reflect && Reflect.get ? Reflect.get : function (t, e, n) {
            t = function (t, e) {
                for (; !Object.prototype.hasOwnProperty.call(t, e) && null !== (t = c(t));) ;
                return t
            }(t, e);
            if (t) {
                e = Object.getOwnPropertyDescriptor(t, e);
                return e.get ? e.get.call(n) : e.value
            }
        })(t, e, n || t)
    }

    function m(t) {
        return t.charAt(0).toUpperCase() + t.slice(1)
    }

    function h(e) {
        return Object.keys(e).map(function (t) {
            return e[t]
        })
    }

    function g(t) {
        return Array.prototype.slice.call(t)
    }

    function v(t, e) {
        e = '"'.concat(t, '" is deprecated and will be removed in the next major release. Please use "').concat(e, '" instead.'), -1 === Y.indexOf(e) && (Y.push(e), W(e))
    }

    function b(t) {
        return t && "function" == typeof t.toPromise
    }

    function y(t) {
        return b(t) ? t.toPromise() : Promise.resolve(t)
    }

    function w(t) {
        return t && Promise.resolve(t) === t
    }

    function C(t) {
        return t instanceof Element || "object" === r(t = t) && t.jquery
    }

    function k() {
        return document.body.querySelector(".".concat($.container))
    }

    function e(t) {
        var e = k();
        return e ? e.querySelector(t) : null
    }

    function t(t) {
        return e(".".concat(t))
    }

    function A() {
        return t($.popup)
    }

    function x() {
        return t($.icon)
    }

    function B() {
        return t($.title)
    }

    function P() {
        return t($.content)
    }

    function E() {
        return t($["html-container"])
    }

    function O() {
        return t($.image)
    }

    function n() {
        return t($["progress-steps"])
    }

    function S() {
        return t($["validation-message"])
    }

    function T() {
        return e(".".concat($.actions, " .").concat($.confirm))
    }

    function L() {
        return e(".".concat($.actions, " .").concat($.deny))
    }

    function D() {
        return e(".".concat($.loader))
    }

    function q() {
        return e(".".concat($.actions, " .").concat($.cancel))
    }

    function j() {
        return t($.actions)
    }

    function M() {
        return t($.header)
    }

    function I() {
        return t($.footer)
    }

    function H() {
        return t($["timer-progress-bar"])
    }

    function V() {
        return t($.close)
    }

    function R() {
        var t = g(A().querySelectorAll('[tabindex]:not([tabindex="-1"]):not([tabindex="0"])')).sort(function (t, e) {
                return t = parseInt(t.getAttribute("tabindex")), (e = parseInt(e.getAttribute("tabindex"))) < t ? 1 : t < e ? -1 : 0
            }),
            e = g(A().querySelectorAll('\n  a[href],\n  area[href],\n  input:not([disabled]),\n  select:not([disabled]),\n  textarea:not([disabled]),\n  button:not([disabled]),\n  iframe,\n  object,\n  embed,\n  [tabindex="0"],\n  [contenteditable],\n  audio[controls],\n  video[controls],\n  summary\n')).filter(function (t) {
                return "-1" !== t.getAttribute("tabindex")
            });
        return function (t) {
            for (var e = [], n = 0; n < t.length; n++) -1 === e.indexOf(t[n]) && e.push(t[n]);
            return e
        }(t.concat(e)).filter(function (t) {
            return wt(t)
        })
    }

    function N() {
        return !G() && !document.body.classList.contains($["no-backdrop"])
    }

    function U(e, t) {
        e.textContent = "", t && (t = (new DOMParser).parseFromString(t, "text/html"), g(t.querySelector("head").childNodes).forEach(function (t) {
            e.appendChild(t)
        }), g(t.querySelector("body").childNodes).forEach(function (t) {
            e.appendChild(t)
        }))
    }

    function _(t, e) {
        if (e) {
            for (var n = e.split(/\s+/), o = 0; o < n.length; o++) if (!t.classList.contains(n[o])) return;
            return 1
        }
    }

    function F(t, e, n) {
        var o, i;
        if (i = e, g((o = t).classList).forEach(function (t) {
            -1 === h($).indexOf(t) && -1 === h(X).indexOf(t) && -1 === h(i.showClass).indexOf(t) && o.classList.remove(t)
        }), e.customClass && e.customClass[n]) {
            if ("string" != typeof e.customClass[n] && !e.customClass[n].forEach) return W("Invalid type of customClass.".concat(n, '! Expected string or iterable object, got "').concat(r(e.customClass[n]), '"'));
            vt(t, e.customClass[n])
        }
    }

    var z = "SweetAlert2:", W = function (t) {
            console.warn("".concat(z, " ").concat("object" === r(t) ? t.join(" ") : t))
        }, K = function (t) {
            console.error("".concat(z, " ").concat(t))
        }, Y = [], Z = function (t) {
            return "function" == typeof t ? t() : t
        }, Q = Object.freeze({cancel: "cancel", backdrop: "backdrop", close: "close", esc: "esc", timer: "timer"}),
        J = function (t) {
            var e, n = {};
            for (e in t) n[t[e]] = "swal2-" + t[e];
            return n
        },
        $ = J(["container", "shown", "height-auto", "iosfix", "popup", "modal", "no-backdrop", "no-transition", "toast", "toast-shown", "toast-column", "show", "hide", "close", "title", "header", "content", "html-container", "actions", "confirm", "deny", "cancel", "footer", "icon", "icon-content", "image", "input", "file", "range", "select", "radio", "checkbox", "label", "textarea", "inputerror", "input-label", "validation-message", "progress-steps", "active-progress-step", "progress-step", "progress-step-line", "loader", "loading", "styled", "top", "top-start", "top-end", "top-left", "top-right", "center", "center-start", "center-end", "center-left", "center-right", "bottom", "bottom-start", "bottom-end", "bottom-left", "bottom-right", "grow-row", "grow-column", "grow-fullscreen", "rtl", "timer-progress-bar", "timer-progress-bar-container", "scrollbar-measure", "icon-success", "icon-warning", "icon-info", "icon-question", "icon-error"]),
        X = J(["success", "warning", "info", "question", "error"]), G = function () {
            return document.body.classList.contains($["toast-shown"])
        }, tt = {previousBodyPadding: null};

    function et(t, e) {
        if (!e) return null;
        switch (e) {
            case"select":
            case"textarea":
            case"file":
                return yt(t, $[e]);
            case"checkbox":
                return t.querySelector(".".concat($.checkbox, " input"));
            case"radio":
                return t.querySelector(".".concat($.radio, " input:checked")) || t.querySelector(".".concat($.radio, " input:first-child"));
            case"range":
                return t.querySelector(".".concat($.range, " input"));
            default:
                return yt(t, $.input)
        }
    }

    function nt(t) {
        var e;
        t.focus(), "file" !== t.type && (e = t.value, t.value = "", t.value = e)
    }

    function ot(t, e, n) {
        t && e && ("string" == typeof e && (e = e.split(/\s+/).filter(Boolean)), e.forEach(function (e) {
            t.forEach ? t.forEach(function (t) {
                n ? t.classList.add(e) : t.classList.remove(e)
            }) : n ? t.classList.add(e) : t.classList.remove(e)
        }))
    }

    function it(t, e, n) {
        n === "".concat(parseInt(n)) && (n = parseInt(n)), n || 0 === parseInt(n) ? t.style[e] = "number" == typeof n ? "".concat(n, "px") : n : t.style.removeProperty(e)
    }

    function rt(t) {
        var e = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : "flex";
        t.style.display = e
    }

    function at(t) {
        t.style.display = "none"
    }

    function st(t, e, n, o) {
        (e = t.querySelector(e)) && (e.style[n] = o)
    }

    function ut(t, e, n) {
        e ? rt(t, n) : at(t)
    }

    function ct(t) {
        return !!(t.scrollHeight > t.clientHeight)
    }

    function lt(t) {
        var e = window.getComputedStyle(t), t = parseFloat(e.getPropertyValue("animation-duration") || "0"),
            e = parseFloat(e.getPropertyValue("transition-duration") || "0");
        return 0 < t || 0 < e
    }

    function dt(t) {
        var e = 1 < arguments.length && void 0 !== arguments[1] && arguments[1], n = H();
        wt(n) && (e && (n.style.transition = "none", n.style.width = "100%"), setTimeout(function () {
            n.style.transition = "width ".concat(t / 1e3, "s linear"), n.style.width = "0%"
        }, 10))
    }

    function pt() {
        return "undefined" == typeof window || "undefined" == typeof document
    }

    function ft(t) {
        Mn.isVisible() && gt !== t.target.value && Mn.resetValidationMessage(), gt = t.target.value
    }

    function mt(t, e) {
        t instanceof HTMLElement ? e.appendChild(t) : "object" === r(t) ? At(t, e) : t && U(e, t)
    }

    function ht(t, e) {
        var n, o, i, r, a = j(), s = D(), u = T(), c = L(), l = q();
        e.showConfirmButton || e.showDenyButton || e.showCancelButton || at(a), F(a, e, "actions"), Pt(u, "confirm", e), Pt(c, "deny", e), Pt(l, "cancel", e), n = u, o = c, i = l, (r = e).buttonsStyling ? (vt([n, o, i], $.styled), r.confirmButtonColor && (n.style.backgroundColor = r.confirmButtonColor), r.denyButtonColor && (o.style.backgroundColor = r.denyButtonColor), r.cancelButtonColor && (i.style.backgroundColor = r.cancelButtonColor)) : bt([n, o, i], $.styled), e.reverseButtons && (a.insertBefore(l, s), a.insertBefore(c, s), a.insertBefore(u, s)), U(s, e.loaderHtml), F(s, e, "loader")
    }

    var gt, vt = function (t, e) {
            ot(t, e, !0)
        }, bt = function (t, e) {
            ot(t, e, !1)
        }, yt = function (t, e) {
            for (var n = 0; n < t.childNodes.length; n++) if (_(t.childNodes[n], e)) return t.childNodes[n]
        }, wt = function (t) {
            return !(!t || !(t.offsetWidth || t.offsetHeight || t.getClientRects().length))
        },
        Ct = '\n <div aria-labelledby="'.concat($.title, '" aria-describedby="').concat($.content, '" class="').concat($.popup, '" tabindex="-1">\n   <div class="').concat($.header, '">\n     <ul class="').concat($["progress-steps"], '"></ul>\n     <div class="').concat($.icon, '"></div>\n     <img class="').concat($.image, '" />\n     <h2 class="').concat($.title, '" id="').concat($.title, '"></h2>\n     <button type="button" class="').concat($.close, '"></button>\n   </div>\n   <div class="').concat($.content, '">\n     <div id="').concat($.content, '" class="').concat($["html-container"], '"></div>\n     <input class="').concat($.input, '" />\n     <input type="file" class="').concat($.file, '" />\n     <div class="').concat($.range, '">\n       <input type="range" />\n       <output></output>\n     </div>\n     <select class="').concat($.select, '"></select>\n     <div class="').concat($.radio, '"></div>\n     <label for="').concat($.checkbox, '" class="').concat($.checkbox, '">\n       <input type="checkbox" />\n       <span class="').concat($.label, '"></span>\n     </label>\n     <textarea class="').concat($.textarea, '"></textarea>\n     <div class="').concat($["validation-message"], '" id="').concat($["validation-message"], '"></div>\n   </div>\n   <div class="').concat($.actions, '">\n     <div class="').concat($.loader, '"></div>\n     <button type="button" class="').concat($.confirm, '"></button>\n     <button type="button" class="').concat($.deny, '"></button>\n     <button type="button" class="').concat($.cancel, '"></button>\n   </div>\n   <div class="').concat($.footer, '"></div>\n   <div class="').concat($["timer-progress-bar-container"], '">\n     <div class="').concat($["timer-progress-bar"], '"></div>\n   </div>\n </div>\n').replace(/(^|\n)\s*/g, ""),
        kt = function (t) {
            var e, n, o, i, r,
                a = !!(i = k()) && (i.parentNode.removeChild(i), bt([document.documentElement, document.body], [$["no-backdrop"], $["toast-shown"], $["has-column"]]), !0);
            pt() ? K("SweetAlert2 requires document to initialize") : ((r = document.createElement("div")).className = $.container, a && vt(r, $["no-transition"]), U(r, Ct), (i = "string" == typeof (e = t.target) ? document.querySelector(e) : e).appendChild(r), a = t, (e = A()).setAttribute("role", a.toast ? "alert" : "dialog"), e.setAttribute("aria-live", a.toast ? "polite" : "assertive"), a.toast || e.setAttribute("aria-modal", "true"), r = i, "rtl" === window.getComputedStyle(r).direction && vt(k(), $.rtl), t = P(), a = yt(t, $.input), e = yt(t, $.file), n = t.querySelector(".".concat($.range, " input")), o = t.querySelector(".".concat($.range, " output")), i = yt(t, $.select), r = t.querySelector(".".concat($.checkbox, " input")), t = yt(t, $.textarea), a.oninput = ft, e.onchange = ft, i.onchange = ft, r.onchange = ft, t.oninput = ft, n.oninput = function (t) {
                ft(t), o.value = n.value
            }, n.onchange = function (t) {
                ft(t), n.nextSibling.value = n.value
            })
        }, At = function (t, e) {
            t.jquery ? xt(e, t) : U(e, t.toString())
        }, xt = function (t, e) {
            if (t.textContent = "", 0 in e) for (var n = 0; n in e; n++) t.appendChild(e[n].cloneNode(!0)); else t.appendChild(e.cloneNode(!0))
        }, Bt = function () {
            if (pt()) return !1;
            var t, e = document.createElement("div"), n = {
                WebkitAnimation: "webkitAnimationEnd",
                OAnimation: "oAnimationEnd oanimationend",
                animation: "animationend"
            };
            for (t in n) if (Object.prototype.hasOwnProperty.call(n, t) && void 0 !== e.style[t]) return n[t];
            return !1
        }();

    function Pt(t, e, n) {
        ut(t, n["show".concat(m(e), "Button")], "inline-block"), U(t, n["".concat(e, "ButtonText")]), t.setAttribute("aria-label", n["".concat(e, "ButtonAriaLabel")]), t.className = $[e], F(t, n, "".concat(e, "Button")), vt(t, n["".concat(e, "ButtonClass")])
    }

    function Et(t, e) {
        var n, o, i = k();
        i && (o = i, "string" == typeof (n = e.backdrop) ? o.style.background = n : n || vt([document.documentElement, document.body], $["no-backdrop"]), !e.backdrop && e.allowOutsideClick && W('"allowOutsideClick" parameter requires `backdrop` parameter to be set to `true`'), o = i, (n = e.position) in $ ? vt(o, $[n]) : (W('The "position" parameter is not valid, defaulting to "center"'), vt(o, $.center)), n = i, !(o = e.grow) || "string" != typeof o || (o = "grow-".concat(o)) in $ && vt(n, $[o]), F(i, e, "container"), (e = document.body.getAttribute("data-swal2-queue-step")) && (i.setAttribute("data-queue-step", e), document.body.removeAttribute("data-swal2-queue-step")))
    }

    function Ot(t, e) {
        t.placeholder && !e.inputPlaceholder || (t.placeholder = e.inputPlaceholder)
    }

    function St(t, e, n) {
        var o, i;
        n.inputLabel && (t.id = $.input, o = document.createElement("label"), i = $["input-label"], o.setAttribute("for", t.id), o.className = i, vt(o, n.customClass.inputLabel), o.innerText = n.inputLabel, e.insertAdjacentElement("beforebegin", o))
    }

    var Tt = {promise: new WeakMap, innerParams: new WeakMap, domCache: new WeakMap},
        Lt = ["input", "file", "range", "select", "radio", "checkbox", "textarea"], Dt = function (t) {
            if (!It[t.input]) return K('Unexpected type of input! Expected "text", "email", "password", "number", "tel", "select", "radio", "checkbox", "textarea", "file" or "url", got "'.concat(t.input, '"'));
            var e = Mt(t.input), n = It[t.input](e, t);
            rt(n), setTimeout(function () {
                nt(n)
            })
        }, qt = function (t, e) {
            var n = et(P(), t);
            if (n) for (var o in !function (t) {
                for (var e = 0; e < t.attributes.length; e++) {
                    var n = t.attributes[e].name;
                    -1 === ["type", "value", "style"].indexOf(n) && t.removeAttribute(n)
                }
            }(n), e) "range" === t && "placeholder" === o || n.setAttribute(o, e[o])
        }, jt = function (t) {
            var e = Mt(t.input);
            t.customClass && vt(e, t.customClass.input)
        }, Mt = function (t) {
            t = $[t] || $.input;
            return yt(P(), t)
        }, It = {};
    It.text = It.email = It.password = It.number = It.tel = It.url = function (t, e) {
        return "string" == typeof e.inputValue || "number" == typeof e.inputValue ? t.value = e.inputValue : w(e.inputValue) || W('Unexpected type of inputValue! Expected "string", "number" or "Promise", got "'.concat(r(e.inputValue), '"')), St(t, t, e), Ot(t, e), t.type = e.input, t
    }, It.file = function (t, e) {
        return St(t, t, e), Ot(t, e), t
    }, It.range = function (t, e) {
        var n = t.querySelector("input"), o = t.querySelector("output");
        return n.value = e.inputValue, n.type = e.input, o.value = e.inputValue, St(n, t, e), t
    }, It.select = function (t, e) {
        var n;
        return t.textContent = "", e.inputPlaceholder && (n = document.createElement("option"), U(n, e.inputPlaceholder), n.value = "", n.disabled = !0, n.selected = !0, t.appendChild(n)), St(t, t, e), t
    }, It.radio = function (t) {
        return t.textContent = "", t
    }, It.checkbox = function (t, e) {
        var n = et(P(), "checkbox");
        n.value = 1, n.id = $.checkbox, n.checked = Boolean(e.inputValue);
        n = t.querySelector("span");
        return U(n, e.inputPlaceholder), t
    }, It.textarea = function (e, t) {
        e.value = t.inputValue, Ot(e, t), St(e, e, t);

        function n(t) {
            return parseInt(window.getComputedStyle(t).paddingLeft) + parseInt(window.getComputedStyle(t).paddingRight)
        }

        var o;
        return "MutationObserver" in window && (o = parseInt(window.getComputedStyle(A()).width), new MutationObserver(function () {
            var t = e.offsetWidth + n(A()) + n(P());
            A().style.width = o < t ? "".concat(t, "px") : null
        }).observe(e, {attributes: !0, attributeFilter: ["style"]})), e
    };

    function Ht(t, e) {
        var o, i, r, n = E();
        F(n, e, "htmlContainer"), e.html ? (mt(e.html, n), rt(n, "block")) : e.text ? (n.textContent = e.text, rt(n, "block")) : at(n), t = t, o = e, i = P(), t = Tt.innerParams.get(t), r = !t || o.input !== t.input, Lt.forEach(function (t) {
            var e = $[t], n = yt(i, e);
            qt(t, o.inputAttributes), n.className = e, r && at(n)
        }), o.input && (r && Dt(o), jt(o)), F(P(), e, "content")
    }

    function Vt() {
        return k() && k().getAttribute("data-queue-step")
    }

    function Rt(t, o) {
        var i = n();
        if (!o.progressSteps || 0 === o.progressSteps.length) return at(i), 0;
        rt(i), i.textContent = "";
        var r = parseInt(void 0 === o.currentProgressStep ? Vt() : o.currentProgressStep);
        r >= o.progressSteps.length && W("Invalid currentProgressStep parameter, it should be less than progressSteps.length (currentProgressStep like JS arrays starts from 0)"), o.progressSteps.forEach(function (t, e) {
            var n, t = (n = t, t = document.createElement("li"), vt(t, $["progress-step"]), U(t, n), t);
            i.appendChild(t), e === r && vt(t, $["active-progress-step"]), e !== o.progressSteps.length - 1 && (t = o, e = document.createElement("li"), vt(e, $["progress-step-line"]), t.progressStepsDistance && (e.style.width = t.progressStepsDistance), e = e, i.appendChild(e))
        })
    }

    function Nt(t, e) {
        var n, o = M();
        F(o, e, "header"), Rt(0, e), n = t, o = e, t = Tt.innerParams.get(n), n = x(), t && o.icon === t.icon ? (Wt(n, o), Ft(n, o)) : o.icon || o.iconHtml ? o.icon && -1 === Object.keys(X).indexOf(o.icon) ? (K('Unknown icon! Expected "success", "error", "warning", "info" or "question", got "'.concat(o.icon, '"')), at(n)) : (rt(n), Wt(n, o), Ft(n, o), vt(n, o.showClass.icon)) : at(n), function (t) {
            var e = O();
            if (!t.imageUrl) return at(e);
            rt(e, ""), e.setAttribute("src", t.imageUrl), e.setAttribute("alt", t.imageAlt), it(e, "width", t.imageWidth), it(e, "height", t.imageHeight), e.className = $.image, F(e, t, "image")
        }(e), o = e, n = B(), ut(n, o.title || o.titleText), o.title && mt(o.title, n), o.titleText && (n.innerText = o.titleText), F(n, o, "title"), o = e, e = V(), U(e, o.closeButtonHtml), F(e, o, "closeButton"), ut(e, o.showCloseButton), e.setAttribute("aria-label", o.closeButtonAriaLabel)
    }

    function Ut(t, e) {
        var n, o, i;
        i = e, n = k(), o = A(), i.toast ? (it(n, "width", i.width), o.style.width = "100%") : it(o, "width", i.width), it(o, "padding", i.padding), i.background && (o.style.background = i.background), Qt(o, i), Et(0, e), Nt(t, e), Ht(t, e), ht(0, e), i = e, t = I(), ut(t, i.footer), i.footer && mt(i.footer, t), F(t, i, "footer"), "function" == typeof e.didRender ? e.didRender(A()) : "function" == typeof e.onRender && e.onRender(A())
    }

    function _t() {
        return T() && T().click()
    }

    var Ft = function (t, e) {
        for (var n in X) e.icon !== n && bt(t, X[n]);
        vt(t, X[e.icon]), Kt(t, e), zt(), F(t, e, "icon")
    }, zt = function () {
        for (var t = A(), e = window.getComputedStyle(t).getPropertyValue("background-color"), n = t.querySelectorAll("[class^=swal2-success-circular-line], .swal2-success-fix"), o = 0; o < n.length; o++) n[o].style.backgroundColor = e
    }, Wt = function (t, e) {
        t.textContent = "", e.iconHtml ? U(t, Yt(e.iconHtml)) : "success" === e.icon ? U(t, '\n      <div class="swal2-success-circular-line-left"></div>\n      <span class="swal2-success-line-tip"></span> <span class="swal2-success-line-long"></span>\n      <div class="swal2-success-ring"></div> <div class="swal2-success-fix"></div>\n      <div class="swal2-success-circular-line-right"></div>\n    ') : "error" === e.icon ? U(t, '\n      <span class="swal2-x-mark">\n        <span class="swal2-x-mark-line-left"></span>\n        <span class="swal2-x-mark-line-right"></span>\n      </span>\n    ') : U(t, Yt({
            question: "?",
            warning: "!",
            info: "i"
        }[e.icon]))
    }, Kt = function (t, e) {
        if (e.iconColor) {
            t.style.color = e.iconColor, t.style.borderColor = e.iconColor;
            for (var n = 0, o = [".swal2-success-line-tip", ".swal2-success-line-long", ".swal2-x-mark-line-left", ".swal2-x-mark-line-right"]; n < o.length; n++) st(t, o[n], "backgroundColor", e.iconColor);
            st(t, ".swal2-success-ring", "borderColor", e.iconColor)
        }
    }, Yt = function (t) {
        return '<div class="'.concat($["icon-content"], '">').concat(t, "</div>")
    }, Zt = [], Qt = function (t, e) {
        t.className = "".concat($.popup, " ").concat(wt(t) ? e.showClass.popup : ""), e.toast ? (vt([document.documentElement, document.body], $["toast-shown"]), vt(t, $.toast)) : vt(t, $.modal), F(t, e, "popup"), "string" == typeof e.customClass && vt(t, e.customClass), e.icon && vt(t, $["icon-".concat(e.icon)])
    };

    function Jt(t) {
        var e = A();
        e || Mn.fire(), e = A();
        var n = j(), o = D();
        !t && wt(T()) && (t = T()), rt(n), t && (at(t), o.setAttribute("data-button-to-replace", t.className)), o.parentNode.insertBefore(o, t), vt([e, n], $.loading), rt(o), e.setAttribute("data-loading", !0), e.setAttribute("aria-busy", !0), e.focus()
    }

    function $t() {
        return new Promise(function (t) {
            var e = window.scrollX, n = window.scrollY;
            te.restoreFocusTimeout = setTimeout(function () {
                te.previousActiveElement && te.previousActiveElement.focus ? (te.previousActiveElement.focus(), te.previousActiveElement = null) : document.body && document.body.focus(), t()
            }, 100), void 0 !== e && void 0 !== n && window.scrollTo(e, n)
        })
    }

    function Xt() {
        if (te.timeout) return function () {
            var t = H(), e = parseInt(window.getComputedStyle(t).width);
            t.style.removeProperty("transition"), t.style.width = "100%";
            var n = parseInt(window.getComputedStyle(t).width), n = parseInt(e / n * 100);
            t.style.removeProperty("transition"), t.style.width = "".concat(n, "%")
        }(), te.timeout.stop()
    }

    function Gt() {
        if (te.timeout) {
            var t = te.timeout.start();
            return dt(t), t
        }
    }

    var te = {}, ee = !1, ne = {};

    function oe(t) {
        for (var e = t.target; e && e !== document; e = e.parentNode) for (var n in ne) {
            var o = e.getAttribute(n);
            if (o) return void ne[n].fire({template: o})
        }
    }

    function ie(t) {
        return Object.prototype.hasOwnProperty.call(se, t)
    }

    function re(t) {
        return ce[t]
    }

    function ae(t) {
        for (var e in t) ie(o = e) || W('Unknown parameter "'.concat(o, '"')), t.toast && (n = e, -1 !== le.indexOf(n) && W('The parameter "'.concat(n, '" is incompatible with toasts'))), re(n = e) && v(n, re(n));
        var n, o
    }

    var se = {
            title: "",
            titleText: "",
            text: "",
            html: "",
            footer: "",
            icon: void 0,
            iconColor: void 0,
            iconHtml: void 0,
            template: void 0,
            toast: !1,
            animation: !0,
            showClass: {popup: "swal2-show", backdrop: "swal2-backdrop-show", icon: "swal2-icon-show"},
            hideClass: {popup: "swal2-hide", backdrop: "swal2-backdrop-hide", icon: "swal2-icon-hide"},
            customClass: {},
            target: "body",
            backdrop: !0,
            heightAuto: !0,
            allowOutsideClick: !0,
            allowEscapeKey: !0,
            allowEnterKey: !0,
            stopKeydownPropagation: !0,
            keydownListenerCapture: !1,
            showConfirmButton: !0,
            showDenyButton: !1,
            showCancelButton: !1,
            preConfirm: void 0,
            preDeny: void 0,
            confirmButtonText: "OK",
            confirmButtonAriaLabel: "",
            confirmButtonColor: void 0,
            denyButtonText: "No",
            denyButtonAriaLabel: "",
            denyButtonColor: void 0,
            cancelButtonText: "Cancel",
            cancelButtonAriaLabel: "",
            cancelButtonColor: void 0,
            buttonsStyling: !0,
            reverseButtons: !1,
            focusConfirm: !0,
            focusDeny: !1,
            focusCancel: !1,
            showCloseButton: !1,
            closeButtonHtml: "&times;",
            closeButtonAriaLabel: "Close this dialog",
            loaderHtml: "",
            showLoaderOnConfirm: !1,
            showLoaderOnDeny: !1,
            imageUrl: void 0,
            imageWidth: void 0,
            imageHeight: void 0,
            imageAlt: "",
            timer: void 0,
            timerProgressBar: !1,
            width: void 0,
            padding: void 0,
            background: void 0,
            input: void 0,
            inputPlaceholder: "",
            inputLabel: "",
            inputValue: "",
            inputOptions: {},
            inputAutoTrim: !0,
            inputAttributes: {},
            inputValidator: void 0,
            returnInputValueOnDeny: !1,
            validationMessage: void 0,
            grow: !1,
            position: "center",
            progressSteps: [],
            currentProgressStep: void 0,
            progressStepsDistance: void 0,
            onBeforeOpen: void 0,
            onOpen: void 0,
            willOpen: void 0,
            didOpen: void 0,
            onRender: void 0,
            didRender: void 0,
            onClose: void 0,
            onAfterClose: void 0,
            willClose: void 0,
            didClose: void 0,
            onDestroy: void 0,
            didDestroy: void 0,
            scrollbarPadding: !0
        },
        ue = ["allowEscapeKey", "allowOutsideClick", "background", "buttonsStyling", "cancelButtonAriaLabel", "cancelButtonColor", "cancelButtonText", "closeButtonAriaLabel", "closeButtonHtml", "confirmButtonAriaLabel", "confirmButtonColor", "confirmButtonText", "currentProgressStep", "customClass", "denyButtonAriaLabel", "denyButtonColor", "denyButtonText", "didClose", "didDestroy", "footer", "hideClass", "html", "icon", "iconColor", "iconHtml", "imageAlt", "imageHeight", "imageUrl", "imageWidth", "onAfterClose", "onClose", "onDestroy", "progressSteps", "reverseButtons", "showCancelButton", "showCloseButton", "showConfirmButton", "showDenyButton", "text", "title", "titleText", "willClose"],
        ce = {
            animation: 'showClass" and "hideClass',
            onBeforeOpen: "willOpen",
            onOpen: "didOpen",
            onRender: "didRender",
            onClose: "willClose",
            onAfterClose: "didClose",
            onDestroy: "didDestroy"
        },
        le = ["allowOutsideClick", "allowEnterKey", "backdrop", "focusConfirm", "focusDeny", "focusCancel", "heightAuto", "keydownListenerCapture"],
        de = Object.freeze({
            isValidParameter: ie,
            isUpdatableParameter: function (t) {
                return -1 !== ue.indexOf(t)
            },
            isDeprecatedParameter: re,
            argsToParams: function (n) {
                var o = {};
                return "object" !== r(n[0]) || C(n[0]) ? ["title", "html", "icon"].forEach(function (t, e) {
                    e = n[e];
                    "string" == typeof e || C(e) ? o[t] = e : void 0 !== e && K("Unexpected type of ".concat(t, '! Expected "string" or "Element", got ').concat(r(e)))
                }) : u(o, n[0]), o
            },
            isVisible: function () {
                return wt(A())
            },
            clickConfirm: _t,
            clickDeny: function () {
                return L() && L().click()
            },
            clickCancel: function () {
                return q() && q().click()
            },
            getContainer: k,
            getPopup: A,
            getTitle: B,
            getContent: P,
            getHtmlContainer: E,
            getImage: O,
            getIcon: x,
            getInputLabel: function () {
                return t($["input-label"])
            },
            getCloseButton: V,
            getActions: j,
            getConfirmButton: T,
            getDenyButton: L,
            getCancelButton: q,
            getLoader: D,
            getHeader: M,
            getFooter: I,
            getTimerProgressBar: H,
            getFocusableElements: R,
            getValidationMessage: S,
            isLoading: function () {
                return A().hasAttribute("data-loading")
            },
            fire: function () {
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                return i(this, e)
            },
            mixin: function (r) {
                return function (t) {
                    !function (t, e) {
                        if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function");
                        t.prototype = Object.create(e && e.prototype, {
                            constructor: {
                                value: t,
                                writable: !0,
                                configurable: !0
                            }
                        }), e && l(t, e)
                    }(i, t);
                    var n, o, e = (n = i, o = d(), function () {
                        var t, e = c(n);
                        return p(this, o ? (t = c(this).constructor, Reflect.construct(e, arguments, t)) : e.apply(this, arguments))
                    });

                    function i() {
                        return a(this, i), e.apply(this, arguments)
                    }

                    return s(i, [{
                        key: "_main", value: function (t, e) {
                            return f(c(i.prototype), "_main", this).call(this, t, u({}, e, r))
                        }
                    }]), i
                }(this)
            },
            queue: function (t) {
                var r = this;
                Zt = t;

                function a(t, e) {
                    Zt = [], t(e)
                }

                var s = [];
                return new Promise(function (i) {
                    !function e(n, o) {
                        n < Zt.length ? (document.body.setAttribute("data-swal2-queue-step", n), r.fire(Zt[n]).then(function (t) {
                            void 0 !== t.value ? (s.push(t.value), e(n + 1, o)) : a(i, {dismiss: t.dismiss})
                        })) : a(i, {value: s})
                    }(0)
                })
            },
            getQueueStep: Vt,
            insertQueueStep: function (t, e) {
                return e && e < Zt.length ? Zt.splice(e, 0, t) : Zt.push(t)
            },
            deleteQueueStep: function (t) {
                void 0 !== Zt[t] && Zt.splice(t, 1)
            },
            showLoading: Jt,
            enableLoading: Jt,
            getTimerLeft: function () {
                return te.timeout && te.timeout.getTimerLeft()
            },
            stopTimer: Xt,
            resumeTimer: Gt,
            toggleTimer: function () {
                var t = te.timeout;
                return t && (t.running ? Xt : Gt)()
            },
            increaseTimer: function (t) {
                if (te.timeout) {
                    t = te.timeout.increase(t);
                    return dt(t, !0), t
                }
            },
            isTimerRunning: function () {
                return te.timeout && te.timeout.isRunning()
            },
            bindClickHandler: function () {
                ne[0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : "data-swal-template"] = this, ee || (document.body.addEventListener("click", oe), ee = !0)
            }
        });

    function pe() {
        var t, e;
        Tt.innerParams.get(this) && (t = Tt.domCache.get(this), at(t.loader), (e = t.popup.getElementsByClassName(t.loader.getAttribute("data-button-to-replace"))).length ? rt(e[0], "inline-block") : wt(T()) || wt(L()) || wt(q()) || at(t.actions), bt([t.popup, t.actions], $.loading), t.popup.removeAttribute("aria-busy"), t.popup.removeAttribute("data-loading"), t.confirmButton.disabled = !1, t.denyButton.disabled = !1, t.cancelButton.disabled = !1)
    }

    function fe() {
        null === tt.previousBodyPadding && document.body.scrollHeight > window.innerHeight && (tt.previousBodyPadding = parseInt(window.getComputedStyle(document.body).getPropertyValue("padding-right")), document.body.style.paddingRight = "".concat(tt.previousBodyPadding + function () {
            var t = document.createElement("div");
            t.className = $["scrollbar-measure"], document.body.appendChild(t);
            var e = t.getBoundingClientRect().width - t.clientWidth;
            return document.body.removeChild(t), e
        }(), "px"))
    }

    function me() {
        return !!window.MSInputMethodContext && !!document.documentMode
    }

    function he() {
        var t = k(), e = A();
        t.style.removeProperty("align-items"), e.offsetTop < 0 && (t.style.alignItems = "flex-start")
    }

    var ge = function () {
        navigator.userAgent.match(/(CriOS|FxiOS|EdgiOS|YaBrowser|UCBrowser)/i) || A().scrollHeight > window.innerHeight - 44 && (k().style.paddingBottom = "".concat(44, "px"))
    }, ve = function () {
        var e, t = k();
        t.ontouchstart = function (t) {
            e = be(t)
        }, t.ontouchmove = function (t) {
            e && (t.preventDefault(), t.stopPropagation())
        }
    }, be = function (t) {
        var e = t.target, n = k();
        return !ye(t) && !we(t) && (e === n || !(ct(n) || "INPUT" === e.tagName || ct(P()) && P().contains(e)))
    }, ye = function (t) {
        return t.touches && t.touches.length && "stylus" === t.touches[0].touchType
    }, we = function (t) {
        return t.touches && 1 < t.touches.length
    }, Ce = {swalPromiseResolve: new WeakMap};

    function ke(t, e, n, o) {
        n ? Oe(t, o) : ($t().then(function () {
            return Oe(t, o)
        }), te.keydownTarget.removeEventListener("keydown", te.keydownHandler, {capture: te.keydownListenerCapture}), te.keydownHandlerAdded = !1), e.parentNode && !document.body.getAttribute("data-swal2-queue-step") && e.parentNode.removeChild(e), N() && (null !== tt.previousBodyPadding && (document.body.style.paddingRight = "".concat(tt.previousBodyPadding, "px"), tt.previousBodyPadding = null), _(document.body, $.iosfix) && (e = parseInt(document.body.style.top, 10), bt(document.body, $.iosfix), document.body.style.top = "", document.body.scrollTop = -1 * e), "undefined" != typeof window && me() && window.removeEventListener("resize", he), g(document.body.children).forEach(function (t) {
            t.hasAttribute("data-previous-aria-hidden") ? (t.setAttribute("aria-hidden", t.getAttribute("data-previous-aria-hidden")), t.removeAttribute("data-previous-aria-hidden")) : t.removeAttribute("aria-hidden")
        })), bt([document.documentElement, document.body], [$.shown, $["height-auto"], $["no-backdrop"], $["toast-shown"], $["toast-column"]])
    }

    function Ae(t) {
        var e, n, o, i = A();
        i && (t = xe(t), (e = Tt.innerParams.get(this)) && !_(i, e.hideClass.popup) && (n = Ce.swalPromiseResolve.get(this), bt(i, e.showClass.popup), vt(i, e.hideClass.popup), o = k(), bt(o, e.showClass.backdrop), vt(o, e.hideClass.backdrop), Be(this, i, e), n(t)))
    }

    function xe(t) {
        return void 0 === t ? {isConfirmed: !1, isDenied: !1, isDismissed: !0} : u({
            isConfirmed: !1,
            isDenied: !1,
            isDismissed: !1
        }, t)
    }

    function Be(t, e, n) {
        var o = k(), i = Bt && lt(e), r = n.onClose, a = n.onAfterClose, s = n.willClose, n = n.didClose;
        Pe(e, s, r), i ? Ee(t, e, o, n || a) : ke(t, o, G(), n || a)
    }

    var Pe = function (t, e, n) {
        null !== e && "function" == typeof e ? e(t) : null !== n && "function" == typeof n && n(t)
    }, Ee = function (t, e, n, o) {
        te.swalCloseEventFinishedCallback = ke.bind(null, t, n, G(), o), e.addEventListener(Bt, function (t) {
            t.target === e && (te.swalCloseEventFinishedCallback(), delete te.swalCloseEventFinishedCallback)
        })
    }, Oe = function (t, e) {
        setTimeout(function () {
            "function" == typeof e && e(), t._destroy()
        })
    };

    function Se(t, e, n) {
        var o = Tt.domCache.get(t);
        e.forEach(function (t) {
            o[t].disabled = n
        })
    }

    function Te(t, e) {
        if (!t) return !1;
        if ("radio" === t.type) for (var n = t.parentNode.parentNode.querySelectorAll("input"), o = 0; o < n.length; o++) n[o].disabled = e; else t.disabled = e
    }

    var Le = function () {
        function n(t, e) {
            a(this, n), this.callback = t, this.remaining = e, this.running = !1, this.start()
        }

        return s(n, [{
            key: "start", value: function () {
                return this.running || (this.running = !0, this.started = new Date, this.id = setTimeout(this.callback, this.remaining)), this.remaining
            }
        }, {
            key: "stop", value: function () {
                return this.running && (this.running = !1, clearTimeout(this.id), this.remaining -= new Date - this.started), this.remaining
            }
        }, {
            key: "increase", value: function (t) {
                var e = this.running;
                return e && this.stop(), this.remaining += t, e && this.start(), this.remaining
            }
        }, {
            key: "getTimerLeft", value: function () {
                return this.running && (this.stop(), this.start()), this.remaining
            }
        }, {
            key: "isRunning", value: function () {
                return this.running
            }
        }]), n
    }(), De = {
        email: function (t, e) {
            return /^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9-]{2,24}$/.test(t) ? Promise.resolve() : Promise.resolve(e || "Invalid email address")
        }, url: function (t, e) {
            return /^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-z]{2,63}\b([-a-zA-Z0-9@:%_+.~#?&/=]*)$/.test(t) ? Promise.resolve() : Promise.resolve(e || "Invalid URL")
        }
    };

    function qe(t) {
        var e, n;
        (e = t).inputValidator || Object.keys(De).forEach(function (t) {
            e.input === t && (e.inputValidator = De[t])
        }), t.showLoaderOnConfirm && !t.preConfirm && W("showLoaderOnConfirm is set to true, but preConfirm is not defined.\nshowLoaderOnConfirm should be used together with preConfirm, see usage example:\nhttps://sweetalert2.github.io/#ajax-request"), t.animation = Z(t.animation), (n = t).target && ("string" != typeof n.target || document.querySelector(n.target)) && ("string" == typeof n.target || n.target.appendChild) || (W('Target parameter is not valid, defaulting to "body"'), n.target = "body"), "string" == typeof t.title && (t.title = t.title.split("\n").join("<br />")), kt(t)
    }

    function je(t) {
        var e = k(), n = A();
        "function" == typeof t.willOpen ? t.willOpen(n) : "function" == typeof t.onBeforeOpen && t.onBeforeOpen(n);
        var o = window.getComputedStyle(document.body).overflowY;
        Je(e, n, t), setTimeout(function () {
            Ze(e, n)
        }, 10), N() && (Qe(e, t.scrollbarPadding, o), g(document.body.children).forEach(function (t) {
            t === k() || function (t, e) {
                if ("function" == typeof t.contains) return t.contains(e)
            }(t, k()) || (t.hasAttribute("aria-hidden") && t.setAttribute("data-previous-aria-hidden", t.getAttribute("aria-hidden")), t.setAttribute("aria-hidden", "true"))
        })), G() || te.previousActiveElement || (te.previousActiveElement = document.activeElement), Ye(n, t), bt(e, $["no-transition"])
    }

    function Me(t) {
        var e = A();
        t.target === e && (t = k(), e.removeEventListener(Bt, Me), t.style.overflowY = "auto")
    }

    function Ie(t, e) {
        t.closePopup({isConfirmed: !0, value: e})
    }

    function He(t, e, n) {
        var o = R();
        if (o.length) return (e += n) === o.length ? e = 0 : -1 === e && (e = o.length - 1), o[e].focus();
        A().focus()
    }

    var Ve = ["swal-title", "swal-html", "swal-footer"], Re = function (t) {
            var n = {};
            return g(t.querySelectorAll("swal-param")).forEach(function (t) {
                Ke(t, ["name", "value"]);
                var e = t.getAttribute("name"), t = t.getAttribute("value");
                "boolean" == typeof se[e] && "false" === t && (t = !1), "object" === r(se[e]) && (t = JSON.parse(t)), n[e] = t
            }), n
        }, Ne = function (t) {
            var n = {};
            return g(t.querySelectorAll("swal-button")).forEach(function (t) {
                Ke(t, ["type", "color", "aria-label"]);
                var e = t.getAttribute("type");
                n["".concat(e, "ButtonText")] = t.innerHTML, n["show".concat(m(e), "Button")] = !0, t.hasAttribute("color") && (n["".concat(e, "ButtonColor")] = t.getAttribute("color")), t.hasAttribute("aria-label") && (n["".concat(e, "ButtonAriaLabel")] = t.getAttribute("aria-label"))
            }), n
        }, Ue = function (t) {
            var e = {}, t = t.querySelector("swal-image");
            return t && (Ke(t, ["src", "width", "height", "alt"]), t.hasAttribute("src") && (e.imageUrl = t.getAttribute("src")), t.hasAttribute("width") && (e.imageWidth = t.getAttribute("width")), t.hasAttribute("height") && (e.imageHeight = t.getAttribute("height")), t.hasAttribute("alt") && (e.imageAlt = t.getAttribute("alt"))), e
        }, _e = function (t) {
            var e = {}, t = t.querySelector("swal-icon");
            return t && (Ke(t, ["type", "color"]), t.hasAttribute("type") && (e.icon = t.getAttribute("type")), t.hasAttribute("color") && (e.iconColor = t.getAttribute("color")), e.iconHtml = t.innerHTML), e
        }, Fe = function (t) {
            var n = {}, e = t.querySelector("swal-input");
            e && (Ke(e, ["type", "label", "placeholder", "value"]), n.input = e.getAttribute("type") || "text", e.hasAttribute("label") && (n.inputLabel = e.getAttribute("label")), e.hasAttribute("placeholder") && (n.inputPlaceholder = e.getAttribute("placeholder")), e.hasAttribute("value") && (n.inputValue = e.getAttribute("value")));
            t = t.querySelectorAll("swal-input-option");
            return t.length && (n.inputOptions = {}, g(t).forEach(function (t) {
                Ke(t, ["value"]);
                var e = t.getAttribute("value"), t = t.innerHTML;
                n.inputOptions[e] = t
            })), n
        }, ze = function (t, e) {
            var n, o = {};
            for (n in e) {
                var i = e[n], r = t.querySelector(i);
                r && (Ke(r, []), o[i.replace(/^swal-/, "")] = r.innerHTML)
            }
            return o
        }, We = function (e) {
            var n = Ve.concat(["swal-param", "swal-button", "swal-image", "swal-icon", "swal-input", "swal-input-option"]);
            g(e.querySelectorAll("*")).forEach(function (t) {
                t.parentNode === e && (t = t.tagName.toLowerCase(), -1 === n.indexOf(t) && W("Unrecognized element <".concat(t, ">")))
            })
        }, Ke = function (e, n) {
            g(e.attributes).forEach(function (t) {
                -1 === n.indexOf(t.name) && W(['Unrecognized attribute "'.concat(t.name, '" on <').concat(e.tagName.toLowerCase(), ">."), "".concat(n.length ? "Allowed attributes are: ".concat(n.join(", ")) : "To set the value, use HTML within the element.")])
            })
        }, Ye = function (t, e) {
            "function" == typeof e.didOpen ? setTimeout(function () {
                return e.didOpen(t)
            }) : "function" == typeof e.onOpen && setTimeout(function () {
                return e.onOpen(t)
            })
        }, Ze = function (t, e) {
            Bt && lt(e) ? (t.style.overflowY = "hidden", e.addEventListener(Bt, Me)) : t.style.overflowY = "auto"
        }, Qe = function (t, e, n) {
            var o;
            (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream || "MacIntel" === navigator.platform && 1 < navigator.maxTouchPoints) && !_(document.body, $.iosfix) && (o = document.body.scrollTop, document.body.style.top = "".concat(-1 * o, "px"), vt(document.body, $.iosfix), ve(), ge()), "undefined" != typeof window && me() && (he(), window.addEventListener("resize", he)), e && "hidden" !== n && fe(), setTimeout(function () {
                t.scrollTop = 0
            })
        }, Je = function (t, e, n) {
            vt(t, n.showClass.backdrop), e.style.setProperty("opacity", "0", "important"), rt(e), setTimeout(function () {
                vt(e, n.showClass.popup), e.style.removeProperty("opacity")
            }, 10), vt([document.documentElement, document.body], $.shown), n.heightAuto && n.backdrop && !n.toast && vt([document.documentElement, document.body], $["height-auto"])
        }, $e = function (t) {
            return t.checked ? 1 : 0
        }, Xe = function (t) {
            return t.checked ? t.value : null
        }, Ge = function (t) {
            return t.files.length ? null !== t.getAttribute("multiple") ? t.files : t.files[0] : null
        }, tn = function (e, n) {
            function o(t) {
                return nn[n.input](i, on(t), n)
            }

            var i = P();
            b(n.inputOptions) || w(n.inputOptions) ? (Jt(T()), y(n.inputOptions).then(function (t) {
                e.hideLoading(), o(t)
            })) : "object" === r(n.inputOptions) ? o(n.inputOptions) : K("Unexpected type of inputOptions! Expected object, Map or Promise, got ".concat(r(n.inputOptions)))
        }, en = function (e, n) {
            var o = e.getInput();
            at(o), y(n.inputValue).then(function (t) {
                o.value = "number" === n.input ? parseFloat(t) || 0 : "".concat(t), rt(o), o.focus(), e.hideLoading()
            }).catch(function (t) {
                K("Error in inputValue promise: ".concat(t)), o.value = "", rt(o), o.focus(), e.hideLoading()
            })
        }, nn = {
            select: function (t, e, i) {
                function o(t, e, n) {
                    var o = document.createElement("option");
                    o.value = n, U(o, e), o.selected = rn(n, i.inputValue), t.appendChild(o)
                }

                var r = yt(t, $.select);
                e.forEach(function (t) {
                    var e, n = t[0], t = t[1];
                    Array.isArray(t) ? ((e = document.createElement("optgroup")).label = n, e.disabled = !1, r.appendChild(e), t.forEach(function (t) {
                        return o(e, t[1], t[0])
                    })) : o(r, t, n)
                }), r.focus()
            }, radio: function (t, e, i) {
                var r = yt(t, $.radio);
                e.forEach(function (t) {
                    var e = t[0], n = t[1], o = document.createElement("input"), t = document.createElement("label");
                    o.type = "radio", o.name = $.radio, o.value = e, rn(e, i.inputValue) && (o.checked = !0);
                    e = document.createElement("span");
                    U(e, n), e.className = $.label, t.appendChild(o), t.appendChild(e), r.appendChild(t)
                });
                e = r.querySelectorAll("input");
                e.length && e[0].focus()
            }
        }, on = function n(o) {
            var i = [];
            return "undefined" != typeof Map && o instanceof Map ? o.forEach(function (t, e) {
                "object" === r(t) && (t = n(t)), i.push([e, t])
            }) : Object.keys(o).forEach(function (t) {
                var e = o[t];
                "object" === r(e) && (e = n(e)), i.push([t, e])
            }), i
        }, rn = function (t, e) {
            return e && e.toString() === t.toString()
        }, an = function (t, e, n) {
            var o = function (t, e) {
                var n = t.getInput();
                if (!n) return null;
                switch (e.input) {
                    case"checkbox":
                        return $e(n);
                    case"radio":
                        return Xe(n);
                    case"file":
                        return Ge(n);
                    default:
                        return e.inputAutoTrim ? n.value.trim() : n.value
                }
            }(t, e);
            e.inputValidator ? sn(t, e, o) : t.getInput().checkValidity() ? ("deny" === n ? un : cn)(t, e, o) : (t.enableButtons(), t.showValidationMessage(e.validationMessage))
        }, sn = function (e, n, o) {
            e.disableInput(), Promise.resolve().then(function () {
                return y(n.inputValidator(o, n.validationMessage))
            }).then(function (t) {
                e.enableButtons(), e.enableInput(), t ? e.showValidationMessage(t) : cn(e, n, o)
            })
        }, un = function (e, t, n) {
            t.showLoaderOnDeny && Jt(L()), t.preDeny ? Promise.resolve().then(function () {
                return y(t.preDeny(n, t.validationMessage))
            }).then(function (t) {
                !1 === t ? e.hideLoading() : e.closePopup({isDenied: !0, value: void 0 === t ? n : t})
            }) : e.closePopup({isDenied: !0, value: n})
        }, cn = function (e, t, n) {
            t.showLoaderOnConfirm && Jt(), t.preConfirm ? (e.resetValidationMessage(), Promise.resolve().then(function () {
                return y(t.preConfirm(n, t.validationMessage))
            }).then(function (t) {
                wt(S()) || !1 === t ? e.hideLoading() : Ie(e, void 0 === t ? n : t)
            })) : Ie(e, n)
        }, ln = ["ArrowRight", "ArrowDown", "Right", "Down"], dn = ["ArrowLeft", "ArrowUp", "Left", "Up"],
        pn = ["Escape", "Esc"], fn = function (t, e, n) {
            var o = Tt.innerParams.get(t);
            o.stopKeydownPropagation && e.stopPropagation(), "Enter" === e.key ? mn(t, e, o) : "Tab" === e.key ? hn(e, o) : -1 !== [].concat(ln, dn).indexOf(e.key) ? gn(e.key) : -1 !== pn.indexOf(e.key) && vn(e, o, n)
        }, mn = function (t, e, n) {
            e.isComposing || e.target && t.getInput() && e.target.outerHTML === t.getInput().outerHTML && -1 === ["textarea", "file"].indexOf(n.input) && (_t(), e.preventDefault())
        }, hn = function (t, e) {
            for (var n = t.target, o = R(), i = -1, r = 0; r < o.length; r++) if (n === o[r]) {
                i = r;
                break
            }
            t.shiftKey ? He(0, i, -1) : He(0, i, 1), t.stopPropagation(), t.preventDefault()
        }, gn = function (t) {
            -1 !== [T(), L(), q()].indexOf(document.activeElement) && (t = -1 !== ln.indexOf(t) ? "nextElementSibling" : "previousElementSibling", (t = document.activeElement[t]) && t.focus())
        }, vn = function (t, e, n) {
            Z(e.allowEscapeKey) && (t.preventDefault(), n(Q.esc))
        }, bn = function (e, t, n) {
            t.popup.onclick = function () {
                var t = Tt.innerParams.get(e);
                t.showConfirmButton || t.showDenyButton || t.showCancelButton || t.showCloseButton || t.timer || t.input || n(Q.close)
            }
        }, yn = !1, wn = function (e) {
            e.popup.onmousedown = function () {
                e.container.onmouseup = function (t) {
                    e.container.onmouseup = void 0, t.target === e.container && (yn = !0)
                }
            }
        }, Cn = function (e) {
            e.container.onmousedown = function () {
                e.popup.onmouseup = function (t) {
                    e.popup.onmouseup = void 0, t.target !== e.popup && !e.popup.contains(t.target) || (yn = !0)
                }
            }
        }, kn = function (n, o, i) {
            o.container.onclick = function (t) {
                var e = Tt.innerParams.get(n);
                yn ? yn = !1 : t.target === o.container && Z(e.allowOutsideClick) && i(Q.backdrop)
            }
        };

    function An(t, e) {
        var n = function (t) {
                t = "string" == typeof t.template ? document.querySelector(t.template) : t.template;
                if (!t) return {};
                t = t.content || t;
                return We(t), u(Re(t), Ne(t), Ue(t), _e(t), Fe(t), ze(t, Ve))
            }(t), o = u({}, se.showClass, e.showClass, n.showClass, t.showClass),
            i = u({}, se.hideClass, e.hideClass, n.hideClass, t.hideClass);
        return (n = u({}, se, e, n, t)).showClass = o, n.hideClass = i, !1 === t.animation && (n.showClass = {
            popup: "swal2-noanimation",
            backdrop: "swal2-noanimation"
        }, n.hideClass = {}), n
    }

    function xn(a, s, u) {
        return new Promise(function (t) {
            function e(t) {
                a.closePopup({isDismissed: !0, dismiss: t})
            }

            var n, o, i, r;
            Ce.swalPromiseResolve.set(a, t), s.confirmButton.onclick = function () {
                return e = u, (t = a).disableButtons(), void (e.input ? an(t, e, "confirm") : cn(t, e, !0));
                var t, e
            }, s.denyButton.onclick = function () {
                return e = u, (t = a).disableButtons(), void (e.returnInputValueOnDeny ? an(t, e, "deny") : un(t, e, !1));
                var t, e
            }, s.cancelButton.onclick = function () {
                return t = e, a.disableButtons(), void t(Q.cancel);
                var t
            }, s.closeButton.onclick = function () {
                return e(Q.close)
            }, n = a, r = s, t = e, Tt.innerParams.get(n).toast ? bn(n, r, t) : (wn(r), Cn(r), kn(n, r, t)), o = a, r = u, i = e, (t = te).keydownTarget && t.keydownHandlerAdded && (t.keydownTarget.removeEventListener("keydown", t.keydownHandler, {capture: t.keydownListenerCapture}), t.keydownHandlerAdded = !1), r.toast || (t.keydownHandler = function (t) {
                return fn(o, t, i)
            }, t.keydownTarget = r.keydownListenerCapture ? window : A(), t.keydownListenerCapture = r.keydownListenerCapture, t.keydownTarget.addEventListener("keydown", t.keydownHandler, {capture: t.keydownListenerCapture}), t.keydownHandlerAdded = !0), (u.toast && (u.input || u.footer || u.showCloseButton) ? vt : bt)(document.body, $["toast-column"]), r = a, "select" === (t = u).input || "radio" === t.input ? tn(r, t) : -1 !== ["text", "email", "number", "tel", "textarea"].indexOf(t.input) && (b(t.inputValue) || w(t.inputValue)) && en(r, t), je(u), Pn(te, u, e), En(s, u), setTimeout(function () {
                s.container.scrollTop = 0
            })
        })
    }

    function Bn(t) {
        var e = {
            popup: A(),
            container: k(),
            content: P(),
            actions: j(),
            confirmButton: T(),
            denyButton: L(),
            cancelButton: q(),
            loader: D(),
            closeButton: V(),
            validationMessage: S(),
            progressSteps: n()
        };
        return Tt.domCache.set(t, e), e
    }

    var Pn = function (t, e, n) {
        var o = H();
        at(o), e.timer && (t.timeout = new Le(function () {
            n("timer"), delete t.timeout
        }, e.timer), e.timerProgressBar && (rt(o), setTimeout(function () {
            t.timeout && t.timeout.running && dt(e.timer)
        })))
    }, En = function (t, e) {
        if (!e.toast) return Z(e.allowEnterKey) ? void (On(t, e) || He(0, -1, 1)) : Sn()
    }, On = function (t, e) {
        return e.focusDeny && wt(t.denyButton) ? (t.denyButton.focus(), !0) : e.focusCancel && wt(t.cancelButton) ? (t.cancelButton.focus(), !0) : !(!e.focusConfirm || !wt(t.confirmButton)) && (t.confirmButton.focus(), !0)
    }, Sn = function () {
        document.activeElement && "function" == typeof document.activeElement.blur && document.activeElement.blur()
    };

    function Tn(t) {
        "function" == typeof t.didDestroy ? t.didDestroy() : "function" == typeof t.onDestroy && t.onDestroy()
    }

    function Ln(t) {
        delete t.params, delete te.keydownHandler, delete te.keydownTarget, qn(Tt), qn(Ce)
    }

    var Dn, qn = function (t) {
        for (var e in t) t[e] = new WeakMap
    }, J = Object.freeze({
        hideLoading: pe, disableLoading: pe, getInput: function (t) {
            var e = Tt.innerParams.get(t || this);
            return (t = Tt.domCache.get(t || this)) ? et(t.content, e.input) : null
        }, close: Ae, closePopup: Ae, closeModal: Ae, closeToast: Ae, enableButtons: function () {
            Se(this, ["confirmButton", "denyButton", "cancelButton"], !1)
        }, disableButtons: function () {
            Se(this, ["confirmButton", "denyButton", "cancelButton"], !0)
        }, enableInput: function () {
            return Te(this.getInput(), !1)
        }, disableInput: function () {
            return Te(this.getInput(), !0)
        }, showValidationMessage: function (t) {
            var e = Tt.domCache.get(this), n = Tt.innerParams.get(this);
            U(e.validationMessage, t), e.validationMessage.className = $["validation-message"], n.customClass && n.customClass.validationMessage && vt(e.validationMessage, n.customClass.validationMessage), rt(e.validationMessage), (e = this.getInput()) && (e.setAttribute("aria-invalid", !0), e.setAttribute("aria-describedBy", $["validation-message"]), nt(e), vt(e, $.inputerror))
        }, resetValidationMessage: function () {
            var t = Tt.domCache.get(this);
            t.validationMessage && at(t.validationMessage), (t = this.getInput()) && (t.removeAttribute("aria-invalid"), t.removeAttribute("aria-describedBy"), bt(t, $.inputerror))
        }, getProgressSteps: function () {
            return Tt.domCache.get(this).progressSteps
        }, _main: function (t) {
            var e = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : {};
            return ae(u({}, e, t)), te.currentInstance && te.currentInstance._destroy(), te.currentInstance = this, qe(t = An(t, e)), Object.freeze(t), te.timeout && (te.timeout.stop(), delete te.timeout), clearTimeout(te.restoreFocusTimeout), e = Bn(this), Ut(this, t), Tt.innerParams.set(this, t), xn(this, e, t)
        }, update: function (e) {
            var t = A(), n = Tt.innerParams.get(this);
            if (!t || _(t, n.hideClass.popup)) return W("You're trying to update the closed or closing popup, that won't work. Use the update() method in preConfirm parameter or show a new popup.");
            var o = {};
            Object.keys(e).forEach(function (t) {
                Mn.isUpdatableParameter(t) ? o[t] = e[t] : W('Invalid parameter to update: "'.concat(t, '". Updatable params are listed here: https://github.com/sweetalert2/sweetalert2/blob/master/src/utils/params.js\n\nIf you think this parameter should be updatable, request it here: https://github.com/sweetalert2/sweetalert2/issues/new?template=02_feature_request.md'))
            }), n = u({}, n, o), Ut(this, n), Tt.innerParams.set(this, n), Object.defineProperties(this, {
                params: {
                    value: u({}, this.params, e),
                    writable: !1,
                    enumerable: !0
                }
            })
        }, _destroy: function () {
            var t = Tt.domCache.get(this), e = Tt.innerParams.get(this);
            e && (t.popup && te.swalCloseEventFinishedCallback && (te.swalCloseEventFinishedCallback(), delete te.swalCloseEventFinishedCallback), te.deferDisposalTimer && (clearTimeout(te.deferDisposalTimer), delete te.deferDisposalTimer), Tn(e), Ln(this))
        }
    }), jn = function () {
        function i() {
            if (a(this, i), "undefined" != typeof window) {
                "undefined" == typeof Promise && K("This package requires a Promise library, please include a shim to enable it in this browser (See: https://github.com/sweetalert2/sweetalert2/wiki/Migration-from-SweetAlert-to-SweetAlert2#1-ie-support)"), Dn = this;
                for (var t = arguments.length, e = new Array(t), n = 0; n < t; n++) e[n] = arguments[n];
                var o = Object.freeze(this.constructor.argsToParams(e));
                Object.defineProperties(this, {params: {value: o, writable: !1, enumerable: !0, configurable: !0}});
                o = this._main(this.params);
                Tt.promise.set(this, o)
            }
        }

        return s(i, [{
            key: "then", value: function (t) {
                return Tt.promise.get(this).then(t)
            }
        }, {
            key: "finally", value: function (t) {
                return Tt.promise.get(this).finally(t)
            }
        }]), i
    }();
    u(jn.prototype, J), u(jn, de), Object.keys(J).forEach(function (t) {
        jn[t] = function () {
            if (Dn) return Dn[t].apply(Dn, arguments)
        }
    }), jn.DismissReason = Q, jn.version = "10.15.4";
    var Mn = jn;
    return Mn.default = Mn
}), void 0 !== this && this.Sweetalert2 && (this.swal = this.sweetAlert = this.Swal = this.SweetAlert = this.Sweetalert2);
!function () {
    function t(e, i, s) {
        function o(h, r) {
            if (!i[h]) {
                if (!e[h]) {
                    var a = "function" == typeof require && require;
                    if (!r && a) return a(h, !0);
                    if (n) return n(h, !0);
                    var _ = new Error("Cannot find module '" + h + "'");
                    throw _.code = "MODULE_NOT_FOUND", _
                }
                var f = i[h] = {exports: {}};
                e[h][0].call(f.exports, function (t) {
                    var i = e[h][1][t];
                    return o(i || t)
                }, f, f.exports, t, e, i, s)
            }
            return i[h].exports
        }

        for (var n = "function" == typeof require && require, h = 0; h < s.length; h++) o(s[h]);
        return o
    }

    return t
}()({
    1: [function (t, e, i) {
        t("./src/Plugin")
    }, {"./src/Plugin": 8}], 2: [function (t, e, i) {
        (function (t) {
            !function (s, o) {
                "use strict";
                "function" == typeof define && define.amd ? define(["jquery"], o) : "object" == typeof i ? e.exports = o("undefined" != typeof window ? window.jQuery : "undefined" != typeof t ? t.jQuery : null) : o(s.jQuery)
            }(this, function (t) {
                "use strict";
                var e = function () {
                    return this._Construct()
                };
                return e.prototype = {
                    _Construct: function () {
                        return this._commandList = [], this._isReady = !1, this._timer = setInterval(this._check.bind(this), 10), this
                    }, add: function (t, e, i) {
                        this._commandList.push({cmd: t, el: e, cmdArgs: i})
                    }, isReady: function () {
                        return this._isReady
                    }, _check: function () {
                        t._protipClassInstance && (this._isReady = !0) && (!this._commandList.length || this._run()) && clearInterval(this._timer)
                    }, _run: function () {
                        var t = this._commandList.shift();
                        return t.el[t.cmd].apply(t.el, t.cmdArgs), this._commandList.length && this._run(), !0
                    }
                }, e
            })
        }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {})
    }, {}], 3: [function (t, e, i) {
        (function (s) {
            !function (o, n) {
                "use strict";
                "function" == typeof define && define.amd ? define(["jquery", "./Constants", "./Item"], n) : "object" == typeof i ? e.exports = n("undefined" != typeof window ? window.jQuery : "undefined" != typeof s ? s.jQuery : null, t("./Constants"), t("./Item")) : o.ProtipClass = n(o.jQuery, o.ProtipConstants, o.ProtipItemClass)
            }(this, function (t, e, i) {
                "use strict";
                try {
                    window.MutationObserver._period = 100
                } catch (s) {
                    console.warn("Protip: MutationObserver polyfill haven't been loaded!"), window.MutationObserver = window.MutationObserver || function () {
                        this.disconnect = this.observe = function () {
                        }
                    }
                }
                var o = function (t) {
                    return this._Construct(t)
                };
                return t.extend(!0, o.prototype, {
                    _defaults: {
                        selector: e.DEFAULT_SELECTOR,
                        namespace: e.DEFAULT_NAMESPACE,
                        protipTemplate: e.TEMPLATE_PROTIP,
                        arrowTemplate: e.TEMPLATE_ARROW,
                        iconTemplate: e.TEMPLATE_ICON,
                        observer: !0,
                        offset: 0,
                        forceMinWidth: !0,
                        delayResize: 100,
                        defaults: {
                            trigger: e.TRIGGER_HOVER,
                            title: null,
                            inited: !1,
                            delayIn: 0,
                            delayOut: 0,
                            interactive: !1,
                            gravity: !0,
                            offsetTop: 0,
                            offsetLeft: 0,
                            position: e.POSITION_RIGHT,
                            placement: e.PLACEMENT_OUTSIDE,
                            classes: null,
                            arrow: !0,
                            width: 300,
                            identifier: !1,
                            icon: !1,
                            observer: !1,
                            target: e.SELECTOR_BODY,
                            skin: e.SKIN_DEFAULT,
                            size: e.SIZE_DEFAULT,
                            scheme: e.SCHEME_DEFAULT,
                            animate: !1,
                            autoHide: !1,
                            autoShow: !1,
                            mixin: null
                        }
                    }, _Construct: function (e) {
                        return this.settings = t.extend(!0, {}, this._defaults, e), this._itemInstances = {}, this._observerInstance = void 0, this._visibleBeforeResize = [], this._task = {
                            delayIn: void 0,
                            delayOut: void 0,
                            resize: void 0
                        }, this._fetchElements(), this._bind(), this
                    }, destroy: function () {
                        this._unbind(), t.each(this._itemInstances, t.proxy(function (t) {
                            this.destroyItemInstance(t)
                        }, this)), this._itemInstances = void 0, this.settings = void 0, t._protipClassInstance = void 0
                    }, namespaced: function (t) {
                        return this.settings.namespace + t.charAt(0).toUpperCase() + t.slice(1)
                    }, destroyItemInstance: function (t) {
                        this._itemInstances[t].destroy()
                    }, onItemDestroyed: function (t) {
                        delete this._itemInstances[t]
                    }, createItemInstance: function (t, s) {
                        var o = this._generateId();
                        return this._itemInstances[o] = new i(o, t, this, s), t.data(this.namespaced(e.PROP_IDENTIFIER), o), this._itemInstances[o]
                    }, reloadItemInstance: function (t) {
                        var i = t.data(this.namespaced(e.PROP_IDENTIFIER));
                        this.destroyItemInstance(i), this.createItemInstance(t)
                    }, getItemInstance: function (t, i) {
                        var s = t.data(this.namespaced(e.PROP_IDENTIFIER));
                        return this._isInited(t) ? this._itemInstances[s] : this.createItemInstance(t, i)
                    }, _fetchElements: function () {
                        setTimeout(function () {
                            t(this.settings.selector).each(t.proxy(function (e, i) {
                                this.getItemInstance(t(i))
                            }, this))
                        }.bind(this))
                    }, _generateId: function () {
                        return (new Date).valueOf() + Math.floor(1e4 * Math.random()).toString()
                    }, _isInited: function (t) {
                        return !!t.data(this.namespaced(e.PROP_INITED))
                    }, _hideAll: function (e, i) {
                        t.each(this._itemInstances, t.proxy(function (t, s) {
                            s.isVisible() && this._visibleBeforeResize.push(s) && s.hide(e, i)
                        }, this))
                    }, _showAll: function (t, e) {
                        this._visibleBeforeResize.forEach(function (i) {
                            i.show(t, e)
                        })
                    }, _onAction: function (i) {
                        var s = t(i.currentTarget), o = this.getItemInstance(s);
                        i.type === e.EVENT_CLICK && o.data.trigger === e.TRIGGER_CLICK && i.preventDefault(), o.actionHandler(i.type)
                    }, _onResize: function () {
                        !this._task.resize && this._hideAll(!0, !0), this._task.resize && clearTimeout(this._task.resize), this._task.resize = setTimeout(function () {
                            this._showAll(!0, !0), this._task.resize = void 0, this._visibleBeforeResize = []
                        }.bind(this), this.settings.delayResize)
                    }, _onBodyClick: function (i) {
                        var s = t(i.target), o = s.closest("." + e.SELECTOR_PREFIX + e.SELECTOR_CONTAINER) || !1,
                            n = s.closest(e.DEFAULT_SELECTOR),
                            h = (this._isInited(n) ? this.getItemInstance(n) : !1, this._isInited(o) ? this.getItemInstance(o) : !1);
                        (!h || h && h.data.trigger !== e.TRIGGER_CLICK) && t.each(this._itemInstances, function (t, i) {
                            i.isVisible() && i.data.trigger === e.TRIGGER_CLICK && (!o || i.el.protip.get(0) !== o.get(0)) && (!n || i.el.source.get(0) !== n.get(0)) && i.hide()
                        })
                    }, _onCloseClick: function (i) {
                        var s = t(i.currentTarget).parents("." + e.SELECTOR_PREFIX + e.SELECTOR_CONTAINER).data(this.namespaced(e.PROP_IDENTIFIER));
                        this._itemInstances[s] && this._itemInstances[s].hide()
                    }, _mutationObserverCallback: function (i) {
                        i.forEach(function (i) {
                            for (var s, o = 0; o < i.addedNodes.length; o++) if (s = t(i.addedNodes[o]), !s.hasClass(e.SELECTOR_PREFIX + e.SELECTOR_CONTAINER)) {
                                var n = s.parent().find(this.settings.selector);
                                n.each(function (i, s) {
                                    if (s = t(s), !this._isInited(s)) {
                                        var o = this.getItemInstance(s);
                                        o.data.trigger === e.TRIGGER_STICKY && this.getItemInstance(s).show()
                                    }
                                }.bind(this))
                            }
                            for (var o = 0; o < i.removedNodes.length; o++) {
                                var h = t(i.removedNodes[o]);
                                h.find(this.settings.selector).each(function (e, i) {
                                    this.getItemInstance(t(i)).destroy()
                                }.bind(this)), h.hasClass(this.settings.selector.replace(".", "")) && this.getItemInstance(h).destroy()
                            }
                        }.bind(this))
                    }, _bind: function () {
                        var i = t(e.SELECTOR_BODY);
                        i.on(e.EVENT_CLICK, t.proxy(this._onBodyClick, this)).on(e.EVENT_MOUSEOVER, this.settings.selector, t.proxy(this._onAction, this)).on(e.EVENT_MOUSEOUT, this.settings.selector, t.proxy(this._onAction, this)).on(e.EVENT_CLICK, this.settings.selector, t.proxy(this._onAction, this)).on(e.EVENT_CLICK, e.SELECTOR_CLOSE, t.proxy(this._onCloseClick, this)), t(window).on(e.EVENT_RESIZE, t.proxy(this._onResize, this)), this.settings.observer && (this._observerInstance = new MutationObserver(this._mutationObserverCallback.bind(this)), this._observerInstance.observe(i.get(0), {
                            attributes: !1,
                            childList: !0,
                            characterData: !1,
                            subtree: !0
                        }))
                    }, _unbind: function () {
                        t(e.SELECTOR_BODY).off(e.EVENT_CLICK, t.proxy(this._onBodyClick, this)).off(e.EVENT_MOUSEOVER, this.settings.selector, t.proxy(this._onAction, this)).off(e.EVENT_MOUSEOUT, this.settings.selector, t.proxy(this._onAction, this)).off(e.EVENT_CLICK, this.settings.selector, t.proxy(this._onAction, this)).off(e.EVENT_CLICK, e.SELECTOR_CLOSE, t.proxy(this._onCloseClick, this)), t(window).off(e.EVENT_RESIZE, t.proxy(this._onResize, this)), this.settings.observer && this._observerInstance.disconnect()
                    }
                }), o
            })
        }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {})
    }, {"./Constants": 4, "./Item": 7}], 4: [function (t, e, i) {
        !function (t, s) {
            "function" == typeof define && define.amd ? define([], s) : "object" == typeof i ? e.exports = s() : t.ProtipConstants = s()
        }(this, function () {
            "use strict";
            var t = {
                PLACEMENT_CENTER: "center",
                PLACEMENT_INSIDE: "inside",
                PLACEMENT_OUTSIDE: "outside",
                PLACEMENT_BORDER: "border",
                POSITION_TOP_LEFT: "top-left",
                POSITION_TOP: "top",
                POSITION_TOP_RIGHT: "top-right",
                POSITION_RIGHT_TOP: "right-top",
                POSITION_RIGHT: "right",
                POSITION_RIGHT_BOTTOM: "right-bottom",
                POSITION_BOTTOM_LEFT: "bottom-left",
                POSITION_BOTTOM: "bottom",
                POSITION_BOTTOM_RIGHT: "bottom-right",
                POSITION_LEFT_TOP: "left-top",
                POSITION_LEFT: "left",
                POSITION_LEFT_BOTTOM: "left-bottom",
                POSITION_CORNER_LEFT_TOP: "top-left-corner",
                POSITION_CORNER_RIGHT_TOP: "top-right-corner",
                POSITION_CORNER_LEFT_BOTTOM: "bottom-left-corner",
                POSITION_CORNER_RIGHT_BOTTOM: "bottom-right-corner",
                TRIGGER_CLICK: "click",
                TRIGGER_CLICK2: "click2",
                TRIGGER_HOVER: "hover",
                TRIGGER_STICKY: "sticky",
                PROP_TRIGGER: "trigger",
                PROP_TITLE: "title",
                PROP_STICKY: "sticky",
                PROP_INITED: "inited",
                PROP_DELAY_IN: "delayIn",
                PROP_DELAY_OUT: "delayOut",
                PROP_GRAVITY: "gravity",
                PROP_OFFSET: "offset",
                PROP_OFFSET_TOP: "offsetTop",
                PROP_OFFSET_LEFT: "offsetLeft",
                PROP_POSITION: "position",
                PROP_CLASS: "class",
                PROP_ARROW: "arrow",
                PROP_WIDTH: "width",
                PROP_IDENTIFIER: "identifier",
                PROP_ICON: "icon",
                PROP_AUTOSHOW: "autoShow",
                PROP_TARGET: "target",
                EVENT_MOUSEOVER: "mouseover",
                EVENT_MOUSEOUT: "mouseout",
                EVENT_MOUSEENTER: "mouseenter",
                EVENT_MOUSELEAVE: "mouseleave",
                EVENT_CLICK: "click",
                EVENT_RESIZE: "resize",
                EVENT_PROTIP_SHOW: "protipshow",
                EVENT_PROTIP_HIDE: "protiphide",
                EVENT_PROTIP_READY: "protipready",
                DEFAULT_SELECTOR: ".protip",
                DEFAULT_NAMESPACE: "pt",
                DEFAULT_DELAY_OUT: 100,
                SELECTOR_PREFIX: "protip-",
                SELECTOR_BODY: "body",
                SELECTOR_ARROW: "arrow",
                SELECTOR_CONTAINER: "container",
                SELECTOR_SHOW: "protip-show",
                SELECTOR_CLOSE: ".protip-close",
                SELECTOR_SKIN_PREFIX: "protip-skin-",
                SELECTOR_SIZE_PREFIX: "--size-",
                SELECTOR_SCHEME_PREFIX: "--scheme-",
                SELECTOR_ANIMATE: "animated",
                SELECTOR_TARGET: ".protip-target",
                SELECTOR_MIXIN_PREFIX: "protip-mixin--",
                SELECTOR_OPEN: "protip-open",
                TEMPLATE_PROTIP: '<div class="{classes}" data-pt-identifier="{identifier}" style="{widthType}:{width}px">{arrow}{icon}<div class="protip-content">{content}</div></div>',
                TEMPLATE_ICON: '<i class="icon-{icon}"></i>',
                ATTR_WIDTH: "width",
                ATTR_MAX_WIDTH: "max-width",
                SKIN_DEFAULT: "default",
                SIZE_DEFAULT: "normal",
                SCHEME_DEFAULT: "pro",
                PSEUDO_NEXT: "next",
                PSEUDO_PREV: "prev",
                PSEUDO_THIS: "this"
            };
            return t.TEMPLATE_ARROW = '<span class="' + t.SELECTOR_PREFIX + t.SELECTOR_ARROW + '"></span>', t
        })
    }, {}], 5: [function (t, e, i) {
        (function (s) {
            !function (o, n) {
                "use strict";
                "function" == typeof define && define.amd ? define(["jquery", "./Constants"], n) : "object" == typeof i ? e.exports = n("undefined" != typeof window ? window.jQuery : "undefined" != typeof s ? s.jQuery : null, t("./Constants")) : o.ProtipGravityParser = n(o.jQuery, o.ProtipConstants)
            }(this, function (t, e) {
                "use strict";
                var i = function (t, e) {
                    return this._Construct(t, e)
                };
                return t.extend(!0, i.prototype, {
                    _Construct: function (t, i) {
                        return this._positionsList = [{lvl: 1, key: i, top: 0, left: 0}, {
                            lvl: 3,
                            key: e.POSITION_CORNER_LEFT_TOP,
                            top: 0,
                            left: 0
                        }, {lvl: 2, key: e.POSITION_TOP_LEFT, top: 0, left: 0}, {
                            lvl: 1,
                            key: e.POSITION_TOP,
                            top: 0,
                            left: 0
                        }, {lvl: 2, key: e.POSITION_TOP_RIGHT, top: 0, left: 0}, {
                            lvl: 3,
                            key: e.POSITION_CORNER_RIGHT_TOP,
                            top: 0,
                            left: 0
                        }, {lvl: 2, key: e.POSITION_RIGHT_TOP, top: 0, left: 0}, {
                            lvl: 1,
                            key: e.POSITION_RIGHT,
                            top: 0,
                            left: 0
                        }, {lvl: 2, key: e.POSITION_RIGHT_BOTTOM, top: 0, left: 0}, {
                            lvl: 2,
                            key: e.POSITION_BOTTOM_LEFT,
                            top: 0,
                            left: 0
                        }, {lvl: 1, key: e.POSITION_BOTTOM, top: 0, left: 0}, {
                            lvl: 2,
                            key: e.POSITION_BOTTOM_RIGHT,
                            top: 0,
                            left: 0
                        }, {lvl: 3, key: e.POSITION_CORNER_RIGHT_BOTTOM, top: 0, left: 0}, {
                            lvl: 2,
                            key: e.POSITION_LEFT_TOP,
                            top: 0,
                            left: 0
                        }, {lvl: 1, key: e.POSITION_LEFT, top: 0, left: 0}, {
                            lvl: 2,
                            key: e.POSITION_LEFT_BOTTOM,
                            top: 0,
                            left: 0
                        }, {
                            lvl: 3,
                            key: e.POSITION_CORNER_LEFT_BOTTOM,
                            top: 0,
                            left: 0
                        }], this._input = t, this._finals = [], this._parse(), this._finals
                    }, _parse: function () {
                        if (this._input === !0 || 3 === this._input) this._finals = this._positionsList; else if (isNaN(this._input)) {
                            var t = [], e = !1;
                            this._finals = this._input.split(";").map(function (i) {
                                if (i = i.trim(), "..." === i) e = !0; else if (i) {
                                    var s = i.split(" ").map(function (t) {
                                        return t.trim()
                                    });
                                    return t.push(s[0]), {
                                        lvl: 1,
                                        key: s[0],
                                        left: parseInt(s[1], 10) || 0,
                                        top: parseInt(s[2], 10) || 0
                                    }
                                }
                            }).filter(function (t) {
                                return !!t
                            }), e && this._positionsList.forEach(function (e) {
                                -1 === t.indexOf(e.key) && this._finals.push(e)
                            }.bind(this))
                        } else this._finals = this._positionsList.filter(function (t) {
                            return t.lvl <= this._input
                        }.bind(this))
                    }
                }), i
            })
        }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {})
    }, {"./Constants": 4}], 6: [function (t, e, i) {
        (function (s) {
            !function (o, n) {
                "use strict";
                "function" == typeof define && define.amd ? define(["jquery", "./Constants", "./GravityParser", "./PositionCalculator"], n) : "object" == typeof i ? e.exports = n("undefined" != typeof window ? window.jQuery : "undefined" != typeof s ? s.jQuery : null, t("./Constants"), t("./GravityParser"), t("./PositionCalculator")) : o.ProtipGravityTester = n(o.jQuery, o.ProtipConstants, o.ProtipGravityParser, o.ProtipPositionCalculator)
            }(this, function (t, e, i, s) {
                "use strict";
                var o = function (t) {
                    return this._Construct(t)
                };
                return t.extend(!0, o.prototype, {
                    _Construct: function (t) {
                        this._item = t, this._result = void 0, this._setWindowDimensions(), this._positionList = new i(this._item.data.gravity, this._item.data.position);
                        var e;
                        for (e = 0; e < this._positionList.length && !this._test(this._positionList[e]); e++) ;
                        return this._item.data.position = this._positionList[0].key, this._result || new s(this._item)
                    }, _test: function (t) {
                        this._setProtipMinWidth();
                        var e = new s(this._item, t.key, t);
                        return this._item.el.protip.css(e), this._setProtipDimensions(), this._topOk() && this._rightOk() && this._bottomOk() && this._leftOk() ? (e.position = t.key, this._result = e, !0) : !1
                    }, _topOk: function () {
                        return this._dimensions.offset.top - this._windowDimensions.scrollTop > 0
                    }, _rightOk: function () {
                        return this._dimensions.offset.left + this._dimensions.width < this._windowDimensions.width
                    }, _bottomOk: function () {
                        return this._dimensions.offset.top - this._windowDimensions.scrollTop + this._dimensions.height < this._windowDimensions.height
                    }, _leftOk: function () {
                        return this._dimensions.offset.left > 0
                    }, _setProtipMinWidth: function () {
                        if (this._item.classInstance.settings.forceMinWidth) {
                            this._item.el.protip.css({position: "fixed", left: 0, top: 0, minWidth: 0});
                            var t = this._item.el.protip.outerWidth() + 1;
                            this._item.el.protip.css({position: "", left: "", top: "", minWidth: t + "px"})
                        }
                    }, _setProtipDimensions: function () {
                        this._dimensions = {
                            width: this._item.el.protip.outerWidth() || 0,
                            height: this._item.el.protip.outerHeight() || 0,
                            offset: this._item.el.protip.offset()
                        }
                    }, _setWindowDimensions: function () {
                        var t = window, e = document, i = e.documentElement, s = e.getElementsByTagName("body")[0],
                            o = t.innerWidth || i.clientWidth || s.clientWidth,
                            n = t.innerHeight || i.clientHeight || s.clientHeight;
                        this._windowDimensions = {
                            width: parseInt(o),
                            height: parseInt(n),
                            scrollTop: window.pageYOffset || document.documentElement.scrollTop || document.getElementsByTagName("body")[0].scrollTop || 0
                        }
                    }
                }), o
            })
        }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {})
    }, {"./Constants": 4, "./GravityParser": 5, "./PositionCalculator": 9}], 7: [function (t, e, i) {
        (function (s) {
            !function (o, n) {
                "use strict";
                "function" == typeof define && define.amd ? define(["jquery", "./Constants", "./GravityTester", "./PositionCalculator"], n) : "object" == typeof i ? e.exports = n("undefined" != typeof window ? window.jQuery : "undefined" != typeof s ? s.jQuery : null, t("./Constants"), t("./GravityTester"), t("./PositionCalculator")) : o.ProtipItemClass = n(o.jQuery, o.ProtipConstants, o.ProtipGravityTester, o.ProtipPositionCalculator)
            }(this, function (t, e, i, s) {
                "use strict";

                function o(t, e) {
                    return t.replace(/\{([\w\.]*)}/g, function (t, i) {
                        for (var s = i.split("."), o = e[s.shift()], n = 0, h = s.length; h > n; n++) o = o[s[n]];
                        return "undefined" != typeof o && null !== o ? o : ""
                    })
                }

                var n = function (t, e, i, s) {
                    return this._Construct(t, e, i, s)
                };
                return t.extend(!0, n.prototype, {
                    _Construct: function (t, i, s, o) {
                        return this._override = o || {}, this._override.identifier = t, this.el = {}, this.el.source = i, this.data = {}, this.classInstance = s, this._isVisible = !1, this._task = {
                            delayIn: void 0,
                            delayOut: void 0
                        }, this._fetchData(), this._prepareInternals(), this._appendProtip(), this._initSticky(), this._initAutoShow(), this._bind(), this.el.source.addClass(this.classInstance.settings.selector.replace(".", "")).data(this._namespaced(e.PROP_INITED), !0), setTimeout(function () {
                            this.el.source.trigger(e.EVENT_PROTIP_READY, this)
                        }.bind(this), 10), this
                    }, actionHandler: function (t) {
                        if (this.data.trigger === e.TRIGGER_STICKY) ; else if (t !== e.EVENT_CLICK || this.data.trigger !== e.TRIGGER_CLICK && this.data.trigger !== e.TRIGGER_CLICK2) {
                            if (this.data.trigger !== e.TRIGGER_CLICK && this.data.trigger !== e.TRIGGER_CLICK2) switch (t) {
                                case e.EVENT_MOUSEOUT:
                                    this.hide();
                                    break;
                                case e.EVENT_MOUSEOVER:
                                    this.show()
                            }
                        } else this.toggle()
                    }, destroy: function () {
                        this.hide(!0), this._unbind(), this.el.protip.remove(), this.el.source.data(this._namespaced(e.PROP_INITED), !1).data(this._namespaced(e.PROP_IDENTIFIER), !1).removeData(), this.classInstance.onItemDestroyed(this.data.identifier), t.each(this._task, function (t, e) {
                            clearTimeout(e)
                        })
                    }, isVisible: function () {
                        return this._isVisible
                    }, toggle: function () {
                        this._isVisible ? this.hide() : this.show()
                    }, show: function (t, o) {
                        if (this.data.title) {
                            if (this._task.delayOut && clearTimeout(this._task.delayOut), this._task.delayIn && clearTimeout(this._task.delayIn), this._task.autoHide && clearTimeout(this._task.autoHide), !t && this.data.delayIn) return void (this._task.delayIn = setTimeout(function () {
                                this.show(!0)
                            }.bind(this), this.data.delayIn));
                            this.data.autoHide !== !1 && (this._task.autoHide = setTimeout(function () {
                                this.hide(!0)
                            }.bind(this), this.data.autoHide));
                            var n;
                            this.data.gravity ? (n = new i(this), delete n.position) : n = new s(this), this.el.source.addClass(e.SELECTOR_OPEN), !o && this.el.source.trigger(e.EVENT_PROTIP_SHOW, this), this.el.protip.css(n).addClass(e.SELECTOR_SHOW), this.data.animate && this.el.protip.addClass(e.SELECTOR_ANIMATE).addClass(this.data.animate || this.classInstance.settings.animate), this._isVisible = !0
                        }
                    }, applyPosition: function (t) {
                        this.el.protip.attr("data-" + e.DEFAULT_NAMESPACE + "-" + e.PROP_POSITION, t)
                    }, hide: function (t, i) {
                        return this._task.delayOut && clearTimeout(this._task.delayOut), this._task.delayIn && clearTimeout(this._task.delayIn), this._task.autoHide && clearTimeout(this._task.autoHide), !t && this.data.delayOut ? void (this._task.delayOut = setTimeout(function () {
                            this.hide(!0)
                        }.bind(this), this.data.delayOut)) : (this.el.source.removeClass(e.SELECTOR_OPEN), !i && this.el.source.trigger(e.EVENT_PROTIP_HIDE, this), this.el.protip.removeClass(e.SELECTOR_SHOW).removeClass(e.SELECTOR_ANIMATE).removeClass(this.data.animate), void (this._isVisible = !1))
                    }, getArrowOffset: function () {
                        return {
                            width: this.el.protipArrow.outerWidth() || 0,
                            height: this.el.protipArrow.outerHeight() || 0
                        }
                    }, _fetchData: function () {
                        t.each(this.classInstance.settings.defaults, t.proxy(function (t) {
                            this.data[t] = this.el.source.data(this._namespaced(t))
                        }, this)), this.data = t.extend({}, this.classInstance.settings.defaults, this.data), this.data = t.extend({}, this.data, this._override), t.each(this.data, t.proxy(function (t, e) {
                            this.el.source.data(this._namespaced(t), e)
                        }, this))
                    }, _prepareInternals: function () {
                        this._setTarget(), this._detectTitle(), this._checkInteractive()
                    }, _checkInteractive: function () {
                        this.data.interactive && (this.data.delayOut = this.data.delayOut || e.DEFAULT_DELAY_OUT)
                    }, _initSticky: function () {
                        this.data.trigger === e.TRIGGER_STICKY && this.show()
                    }, _initAutoShow: function () {
                        this.data.autoShow && this.show()
                    }, _appendProtip: function () {
                        this.el.protip = o(this.classInstance.settings.protipTemplate, {
                            classes: this._getClassList(),
                            widthType: this._getWidthType(),
                            width: this._getWidth(),
                            content: this.data.title,
                            icon: this._getIconTemplate(),
                            arrow: this.data.arrow ? e.TEMPLATE_ARROW : "",
                            identifier: this.data.identifier
                        }), this.el.protip = t(this.el.protip), this.el.protipArrow = this.el.protip.find("." + e.SELECTOR_PREFIX + e.SELECTOR_ARROW), this.el.target.append(this.el.protip)
                    }, _getClassList: function () {
                        var t = [], i = this.data.skin, s = this.data.size, o = this.data.scheme;
                        return t.push(e.SELECTOR_PREFIX + e.SELECTOR_CONTAINER), t.push(e.SELECTOR_SKIN_PREFIX + i), t.push(e.SELECTOR_SKIN_PREFIX + i + e.SELECTOR_SIZE_PREFIX + s), t.push(e.SELECTOR_SKIN_PREFIX + i + e.SELECTOR_SCHEME_PREFIX + o), this.data.classes && t.push(this.data.classes), this.data.mixin && t.push(this._parseMixins()), t.join(" ")
                    }, _parseMixins: function () {
                        var t = [];
                        return this.data.mixin && this.data.mixin.split(" ").forEach(function (i) {
                            i && t.push(e.SELECTOR_MIXIN_PREFIX + i)
                        }, this), t.join(" ")
                    }, _getWidthType: function () {
                        return isNaN(this.data.width) ? e.ATTR_WIDTH : e.ATTR_MAX_WIDTH
                    }, _getWidth: function () {
                        return parseInt(this.data.width, 10)
                    }, _getIconTemplate: function () {
                        return this.data.icon ? o(this.classInstance.settings.iconTemplate, {icon: this.data.icon}) : ""
                    }, _detectTitle: function () {
                        if (!this.data.title || "#" !== this.data.title.charAt(0) && "." !== this.data.title.charAt(0)) {
                            if (this.data.title && ":" === this.data.title.charAt(0)) {
                                var i = this.data.title.substring(1);
                                switch (i) {
                                    case e.PSEUDO_NEXT:
                                        this.data.title = this.el.source.next().html();
                                        break;
                                    case e.PSEUDO_PREV:
                                        this.data.title = this.el.source.prev().html();
                                        break;
                                    case e.PSEUDO_THIS:
                                        this.data.title = this.el.source.html()
                                }
                            }
                        } else this.data.titleSource = this.data.titleSource || this.data.title, this.data.title = t(this.data.title).html();
                        this.data.title && this.data.title.indexOf("<a ") + 1 && (this.data.interactive = !0)
                    }, _setTarget: function () {
                        var i = this._getData(e.PROP_TARGET);
                        i = i === !0 ? this.el.source : i === e.SELECTOR_BODY && this.el.source.closest(e.SELECTOR_TARGET).length ? this.el.source.closest(e.SELECTOR_TARGET) : t(i ? i : e.SELECTOR_BODY), "static" === i.css("position") && i.css({position: "relative"}), this.el.target = i
                    }, _getData: function (t) {
                        return this.el.source.data(this._namespaced(t))
                    }, _namespaced: function (t) {
                        return this.classInstance.namespaced(t)
                    }, _onProtipMouseenter: function () {
                        clearTimeout(this._task.delayOut)
                    }, _onProtipMouseleave: function () {
                        this.data.trigger === e.TRIGGER_HOVER && this.hide()
                    }, _bind: function () {
                        this.data.interactive && this.el.protip.on(e.EVENT_MOUSEENTER, t.proxy(this._onProtipMouseenter, this)).on(e.EVENT_MOUSELEAVE, t.proxy(this._onProtipMouseleave, this)), this.data.observer && (this._observerInstance = new MutationObserver(function () {
                            this.classInstance.reloadItemInstance(this.el.source)
                        }.bind(this)), this._observerInstance.observe(this.el.source.get(0), {
                            attributes: !0,
                            childList: !1,
                            characterData: !1,
                            subtree: !1
                        }))
                    }, _unbind: function () {
                        this.data.interactive && this.el.protip.off(e.EVENT_MOUSEENTER, t.proxy(this._onProtipMouseenter, this)).off(e.EVENT_MOUSELEAVE, t.proxy(this._onProtipMouseleave, this)), this.data.observer && this._observerInstance.disconnect()
                    }
                }), n
            })
        }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {})
    }, {"./Constants": 4, "./GravityTester": 6, "./PositionCalculator": 9}], 8: [function (t, e, i) {
        (function (s) {
            !function (o, n) {
                "use strict";
                "function" == typeof define && define.amd ? define(["jquery", "./Class", "./Buffer", "./Constants"], n) : "object" == typeof i ? e.exports = n("undefined" != typeof window ? window.jQuery : "undefined" != typeof s ? s.jQuery : null, t("./Class"), t("./Buffer"), t("./Constants")) : n(o.jQuery, o.ProtipClass, o.ProtipBuffer, o.ProtipContants)
            }(this, function (t, e, i, s) {
                "use strict";
                t = t.extend(t, {
                    _protipClassInstance: void 0, _protipBuffer: new i, protip: function (t) {
                        return this._protipClassInstance || (this._protipClassInstance = new e(t), this.protip.C = s), this._protipClassInstance
                    }
                }), t.fn.extend({
                    protipDestroy: function () {
                        return t._protipBuffer.isReady() ? this.each(function (e, i) {
                            i = t(i), t._protipClassInstance.getItemInstance(i).destroy()
                        }) : this
                    }, protipSet: function (e) {
                        return t._protipBuffer.isReady() ? this.each(function (i, s) {
                            s = t(s), t._protipClassInstance.getItemInstance(s).destroy(), t._protipClassInstance.getItemInstance(s, e)
                        }) : (t._protipBuffer.add("protipSet", this, arguments), this)
                    }, protipShow: function (e) {
                        return t._protipBuffer.isReady() ? this.each(function (i, s) {
                            s = t(s), t._protipClassInstance.getItemInstance(s).destroy(), t._protipClassInstance.getItemInstance(s, e).show(!0)
                        }) : (t._protipBuffer.add("protipShow", this, arguments), this)
                    }, protipHide: function () {
                        return t._protipBuffer.isReady() ? this.each(function (e, i) {
                            t._protipClassInstance.getItemInstance(t(i)).hide(!0)
                        }) : (t._protipBuffer.add("protipHide", this, arguments), this)
                    }, protipToggle: function () {
                        if (t._protipBuffer.isReady()) {
                            var e;
                            return this.each(function (i, s) {
                                e = t._protipClassInstance.getItemInstance(t(s)), e = e.isVisible() ? e.hide(!0) : e.show(!0)
                            }.bind(this))
                        }
                        return t._protipBuffer.add("protipToggle", this, arguments), this
                    }, protipHideInside: function () {
                        return t._protipBuffer.isReady() ? this.each(function (e, i) {
                            t(i).find(t._protipClassInstance.settings.selector).each(function (e, i) {
                                t._protipClassInstance.getItemInstance(t(i)).hide(!0)
                            })
                        }) : (t._protipBuffer.add("protipHideInside", this, arguments), this)
                    }, protipShowInside: function () {
                        return t._protipBuffer.isReady() ? this.each(function (e, i) {
                            t(i).find(t._protipClassInstance.settings.selector).each(function (e, i) {
                                t._protipClassInstance.getItemInstance(t(i)).show(!0)
                            })
                        }) : (t._protipBuffer.add("protipShowInside", this, arguments), this)
                    }, protipToggleInside: function () {
                        if (t._protipBuffer.isReady()) {
                            var e;
                            return this.each(function (i, s) {
                                t(s).find(t._protipClassInstance.settings.selector).each(function (i, s) {
                                    e = t._protipClassInstance.getItemInstance(t(s)), e = e.isVisible() ? e.hide(!0) : e.show(!0)
                                })
                            })
                        }
                        return t._protipBuffer.add("protipToggleInside", this, arguments), this
                    }
                })
            })
        }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {})
    }, {"./Buffer": 2, "./Class": 3, "./Constants": 4}], 9: [function (t, e, i) {
        (function (s) {
            !function (o, n) {
                "use strict";
                "function" == typeof define && define.amd ? define(["jquery", "./Constants"], n) : "object" == typeof i ? e.exports = n("undefined" != typeof window ? window.jQuery : "undefined" != typeof s ? s.jQuery : null, t("./Constants")) : o.ProtipPositionCalculator = n(o.jQuery, o.ProtipConstants)
            }(this, function (t, e) {
                "use strict";
                var i = function (t, e, i) {
                    return this._Construct(t, e, i)
                };
                return t.extend(!0, i.prototype, {
                    _Construct: function (t, e, i) {
                        return this._itemInstance = t, this._protip = this._getProto(this._itemInstance.el.protip), this._source = this._getProto(this._itemInstance.el.source), this._target = this._getProto(this._itemInstance.el.target), this._position = e || this._itemInstance.data.position, this._placement = this._itemInstance.data.placement, this._offset = i || {
                            top: this._itemInstance.data.offsetTop,
                            left: this._itemInstance.data.offsetLeft
                        }, this._getPosition()
                    }, _getProto: function (t) {
                        var e = {el: void 0, width: void 0, height: void 0, offset: void 0};
                        return e.el = t, e.width = t.outerWidth() || 0, e.height = t.outerHeight() || 0, e.offset = t.offset(), e
                    }, _getPosition: function () {
                        this._itemInstance.applyPosition(this._position);
                        var t = {left: 0, top: 0}, i = this._itemInstance.getArrowOffset(),
                            s = this._itemInstance.classInstance.settings.offset, o = document.body.scrollLeft || 0,
                            n = document.body.scrollTop || 0;
                        if (o = window.pageXOffset === o ? 0 : this._target.el.get(0) !== document.body ? 0 : o, n = window.pageYOffset === n ? 0 : this._target.el.get(0) !== document.body ? 0 : n, this._placement !== e.PLACEMENT_CENTER) switch (this._position) {
                            case e.POSITION_TOP:
                                this._offset.top += -1 * (s + i.height), t.left = this._source.offset.left + this._source.width / 2 - this._protip.width / 2 - this._target.offset.left + this._offset.left + o, t.top = this._source.offset.top - this._protip.height - this._target.offset.top + this._offset.top + n, this._placement === e.PLACEMENT_INSIDE && (t.top += this._protip.height), this._placement === e.PLACEMENT_BORDER && (t.top += this._protip.height / 2);
                                break;
                            case e.POSITION_TOP_LEFT:
                                this._offset.top += -1 * (s + i.height), t.left = this._source.offset.left - this._target.offset.left + this._offset.left + o, t.top = this._source.offset.top - this._protip.height - this._target.offset.top + this._offset.top + n, this._placement === e.PLACEMENT_INSIDE && (t.top += this._protip.height), this._placement === e.PLACEMENT_BORDER && (t.top += this._protip.height / 2);
                                break;
                            case e.POSITION_TOP_RIGHT:
                                this._offset.top += -1 * (s + i.height), t.left = this._source.offset.left + this._source.width - this._protip.width - this._target.offset.left + this._offset.left + o, t.top = this._source.offset.top - this._protip.height - this._target.offset.top + this._offset.top + n, this._placement === e.PLACEMENT_INSIDE && (t.top += this._protip.height), this._placement === e.PLACEMENT_BORDER && (t.top += this._protip.height / 2);
                                break;
                            case e.POSITION_RIGHT:
                                this._offset.left += s + i.width, t.left = this._source.offset.left + this._source.width - this._target.offset.left + this._offset.left + o, t.top = this._source.offset.top + this._source.height / 2 - this._protip.height / 2 - this._target.offset.top + this._offset.top + n, this._placement === e.PLACEMENT_INSIDE && (t.left -= this._protip.width), this._placement === e.PLACEMENT_BORDER && (t.left -= this._protip.width / 2);
                                break;
                            case e.POSITION_RIGHT_TOP:
                                this._offset.left += s + i.width, t.left = this._source.offset.left + this._source.width - this._target.offset.left + this._offset.left + o, t.top = this._source.offset.top - this._target.offset.top + this._offset.top + n, this._placement === e.PLACEMENT_INSIDE && (t.left -= this._protip.width), this._placement === e.PLACEMENT_BORDER && (t.left -= this._protip.width / 2);
                                break;
                            case e.POSITION_RIGHT_BOTTOM:
                                this._offset.left += s + i.width, t.left = this._source.offset.left + this._source.width - this._target.offset.left + this._offset.left + o, t.top = this._source.offset.top + this._source.height - this._protip.height - this._target.offset.top + this._offset.top + n, this._placement === e.PLACEMENT_INSIDE && (t.left -= this._protip.width), this._placement === e.PLACEMENT_BORDER && (t.left -= this._protip.width / 2);
                                break;
                            case e.POSITION_BOTTOM:
                                this._offset.top += s + i.height, t.left = this._source.offset.left + this._source.width / 2 - this._protip.width / 2 - this._target.offset.left + this._offset.left + o, t.top = this._source.offset.top + this._source.height - this._target.offset.top + this._offset.top + n, this._placement === e.PLACEMENT_INSIDE && (t.top -= this._protip.height), this._placement === e.PLACEMENT_BORDER && (t.top -= this._protip.height / 2);
                                break;
                            case e.POSITION_BOTTOM_LEFT:
                                this._offset.top += s + i.height, t.left = this._source.offset.left - this._target.offset.left + this._offset.left + o, t.top = this._source.offset.top + this._source.height - this._target.offset.top + this._offset.top + n, this._placement === e.PLACEMENT_INSIDE && (t.top -= this._protip.height), this._placement === e.PLACEMENT_BORDER && (t.top -= this._protip.height / 2);
                                break;
                            case e.POSITION_BOTTOM_RIGHT:
                                this._offset.top += s + i.height, t.left = this._source.offset.left + this._source.width - this._protip.width - this._target.offset.left + this._offset.left + o, t.top = this._source.offset.top + this._source.height - this._target.offset.top + this._offset.top + n, this._placement === e.PLACEMENT_INSIDE && (t.top -= this._protip.height), this._placement === e.PLACEMENT_BORDER && (t.top -= this._protip.height / 2);
                                break;
                            case e.POSITION_LEFT:
                                this._offset.left += -1 * (s + i.width), t.left = this._source.offset.left - this._protip.width - this._target.offset.left + this._offset.left + o, t.top = this._source.offset.top + this._source.height / 2 - this._protip.height / 2 - this._target.offset.top + this._offset.top + n, this._placement === e.PLACEMENT_INSIDE && (t.left += this._protip.width), this._placement === e.PLACEMENT_BORDER && (t.left += this._protip.width / 2);
                                break;
                            case e.POSITION_LEFT_TOP:
                                this._offset.left += -1 * (s + i.width), t.left = this._source.offset.left - this._protip.width - this._target.offset.left + this._offset.left + o, t.top = this._source.offset.top - this._target.offset.top + this._offset.top + n, this._placement === e.PLACEMENT_INSIDE && (t.left += this._protip.width), this._placement === e.PLACEMENT_BORDER && (t.left += this._protip.width / 2);
                                break;
                            case e.POSITION_LEFT_BOTTOM:
                                this._offset.left += -1 * (s + i.width), t.left = this._source.offset.left - this._protip.width - this._target.offset.left + this._offset.left + o, t.top = this._source.offset.top + this._source.height - this._protip.height - this._target.offset.top + this._offset.top + n, this._placement === e.PLACEMENT_INSIDE && (t.left += this._protip.width), this._placement === e.PLACEMENT_BORDER && (t.left += this._protip.width / 2);
                                break;
                            case e.POSITION_CORNER_LEFT_TOP:
                                this._offset.top += -1 * (s + i.height), t.left = this._source.offset.left - this._protip.width - this._target.offset.left + this._offset.left + o, t.top = this._source.offset.top - this._protip.height - this._target.offset.top + this._offset.top + n, this._placement === e.PLACEMENT_INSIDE && (t.left += this._protip.width), this._placement === e.PLACEMENT_INSIDE && (t.top += this._protip.height), this._placement === e.PLACEMENT_BORDER && (t.left += this._protip.width / 2), this._placement === e.PLACEMENT_BORDER && (t.top += this._protip.height / 2);
                                break;
                            case e.POSITION_CORNER_LEFT_BOTTOM:
                                this._offset.top += s + i.height, t.left = this._source.offset.left - this._protip.width - this._target.offset.left + this._offset.left + o, t.top = this._source.offset.top + this._source.height - this._target.offset.top + this._offset.top + n, this._placement === e.PLACEMENT_INSIDE && (t.left += this._protip.width), this._placement === e.PLACEMENT_INSIDE && (t.top -= this._protip.height), this._placement === e.PLACEMENT_BORDER && (t.left += this._protip.width / 2), this._placement === e.PLACEMENT_BORDER && (t.top -= this._protip.height / 2);
                                break;
                            case e.POSITION_CORNER_RIGHT_BOTTOM:
                                this._offset.top += s + i.height, t.left = this._source.offset.left + this._source.width - this._target.offset.left + this._offset.left + o, t.top = this._source.offset.top + this._source.height - this._target.offset.top + this._offset.top + n, this._placement === e.PLACEMENT_INSIDE && (t.left -= this._protip.width), this._placement === e.PLACEMENT_INSIDE && (t.top -= this._protip.height), this._placement === e.PLACEMENT_BORDER && (t.left -= this._protip.width / 2), this._placement === e.PLACEMENT_BORDER && (t.top -= this._protip.height / 2);
                                break;
                            case e.POSITION_CORNER_RIGHT_TOP:
                                this._offset.top += -1 * (s + i.height), t.left = this._source.offset.left + this._source.width - this._target.offset.left + this._offset.left + o, t.top = this._source.offset.top - this._protip.height - this._target.offset.top + this._offset.top + n, this._placement === e.PLACEMENT_INSIDE && (t.left -= this._protip.width), this._placement === e.PLACEMENT_INSIDE && (t.top += this._protip.height), this._placement === e.PLACEMENT_BORDER && (t.left -= this._protip.width / 2), this._placement === e.PLACEMENT_BORDER && (t.top += this._protip.height / 2)
                        } else t.left = this._source.offset.left + this._source.width / 2 - this._protip.width / 2 - this._target.offset.left + this._offset.left + o, t.top = this._source.offset.top + this._source.height / 2 - this._protip.height / 2 - this._target.offset.top + this._offset.top + n;
                        return t.left = t.left + "px", t.top = t.top + "px", t
                    }
                }), i
            })
        }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {})
    }, {"./Constants": 4}]
}, {}, [1]);
!function (e) {
    e(["jquery"], function (e) {
        return function () {
            function t(e, t, n) {
                return g({type: O.error, iconClass: m().iconClasses.error, message: e, optionsOverride: n, title: t})
            }

            function n(t, n) {
                return t || (t = m()), v = e("#" + t.containerId), v.length ? v : (n && (v = d(t)), v)
            }

            function o(e, t, n) {
                return g({type: O.info, iconClass: m().iconClasses.info, message: e, optionsOverride: n, title: t})
            }

            function s(e) {
                C = e
            }

            function i(e, t, n) {
                return g({
                    type: O.success,
                    iconClass: m().iconClasses.success,
                    message: e,
                    optionsOverride: n,
                    title: t
                })
            }

            function a(e, t, n) {
                return g({
                    type: O.warning,
                    iconClass: m().iconClasses.warning,
                    message: e,
                    optionsOverride: n,
                    title: t
                })
            }

            function r(e, t) {
                var o = m();
                v || n(o), u(e, o, t) || l(o)
            }

            function c(t) {
                var o = m();
                return v || n(o), t && 0 === e(":focus", t).length ? void h(t) : void (v.children().length && v.remove())
            }

            function l(t) {
                for (var n = v.children(), o = n.length - 1; o >= 0; o--) u(e(n[o]), t)
            }

            function u(t, n, o) {
                var s = !(!o || !o.force) && o.force;
                return !(!t || !s && 0 !== e(":focus", t).length) && (t[n.hideMethod]({
                    duration: n.hideDuration,
                    easing: n.hideEasing,
                    complete: function () {
                        h(t)
                    }
                }), !0)
            }

            function d(t) {
                return v = e("<div/>").attr("id", t.containerId).addClass(t.positionClass), v.appendTo(e(t.target)), v
            }

            function p() {
                return {
                    tapToDismiss: !0,
                    toastClass: "toast",
                    containerId: "toast-container",
                    debug: !1,
                    showMethod: "fadeIn",
                    showDuration: 300,
                    showEasing: "swing",
                    onShown: void 0,
                    hideMethod: "fadeOut",
                    hideDuration: 1e3,
                    hideEasing: "swing",
                    onHidden: void 0,
                    closeMethod: !1,
                    closeDuration: !1,
                    closeEasing: !1,
                    closeOnHover: !0,
                    extendedTimeOut: 1e3,
                    iconClasses: {
                        error: "toast-error",
                        info: "toast-info",
                        success: "toast-success",
                        warning: "toast-warning"
                    },
                    iconClass: "toast-info",
                    positionClass: "toast-top-right",
                    timeOut: 5e3,
                    titleClass: "toast-title",
                    messageClass: "toast-message",
                    escapeHtml: !1,
                    target: "body",
                    closeHtml: '<button type="button">&times;</button>',
                    closeClass: "toast-close-button",
                    newestOnTop: !0,
                    preventDuplicates: !1,
                    progressBar: !1,
                    progressClass: "toast-progress",
                    rtl: !1
                }
            }

            function f(e) {
                C && C(e)
            }

            function g(t) {
                function o(e) {
                    return null == e && (e = ""), e.replace(/&/g, "&amp;").replace(/"/g, "&quot;").replace(/'/g, "&#39;").replace(/</g, "&lt;").replace(/>/g, "&gt;")
                }

                function s() {
                    c(), u(), d(), p(), g(), C(), l(), i()
                }

                function i() {
                    var e = "";
                    switch (t.iconClass) {
                        case"toast-success":
                        case"toast-info":
                            e = "polite";
                            break;
                        default:
                            e = "assertive"
                    }
                    I.attr("aria-live", e)
                }

                function a() {
                    E.closeOnHover && I.hover(H, D), !E.onclick && E.tapToDismiss && I.click(b), E.closeButton && j && j.click(function (e) {
                        e.stopPropagation ? e.stopPropagation() : void 0 !== e.cancelBubble && e.cancelBubble !== !0 && (e.cancelBubble = !0), E.onCloseClick && E.onCloseClick(e), b(!0)
                    }), E.onclick && I.click(function (e) {
                        E.onclick(e), b()
                    })
                }

                function r() {
                    I.hide(), I[E.showMethod]({
                        duration: E.showDuration,
                        easing: E.showEasing,
                        complete: E.onShown
                    }), E.timeOut > 0 && (k = setTimeout(b, E.timeOut), F.maxHideTime = parseFloat(E.timeOut), F.hideEta = (new Date).getTime() + F.maxHideTime, E.progressBar && (F.intervalId = setInterval(x, 10)))
                }

                function c() {
                    t.iconClass && I.addClass(E.toastClass).addClass(y)
                }

                function l() {
                    E.newestOnTop ? v.prepend(I) : v.append(I)
                }

                function u() {
                    if (t.title) {
                        var e = t.title;
                        E.escapeHtml && (e = o(t.title)), M.append(e).addClass(E.titleClass), I.append(M)
                    }
                }

                function d() {
                    if (t.message) {
                        var e = t.message;
                        E.escapeHtml && (e = o(t.message)), B.append(e).addClass(E.messageClass), I.append(B)
                    }
                }

                function p() {
                    E.closeButton && (j.addClass(E.closeClass).attr("role", "button"), I.prepend(j))
                }

                function g() {
                    E.progressBar && (q.addClass(E.progressClass), I.prepend(q))
                }

                function C() {
                    E.rtl && I.addClass("rtl")
                }

                function O(e, t) {
                    if (e.preventDuplicates) {
                        if (t.message === w) return !0;
                        w = t.message
                    }
                    return !1
                }

                function b(t) {
                    var n = t && E.closeMethod !== !1 ? E.closeMethod : E.hideMethod,
                        o = t && E.closeDuration !== !1 ? E.closeDuration : E.hideDuration,
                        s = t && E.closeEasing !== !1 ? E.closeEasing : E.hideEasing;
                    if (!e(":focus", I).length || t) return clearTimeout(F.intervalId), I[n]({
                        duration: o,
                        easing: s,
                        complete: function () {
                            h(I), clearTimeout(k), E.onHidden && "hidden" !== P.state && E.onHidden(), P.state = "hidden", P.endTime = new Date, f(P)
                        }
                    })
                }

                function D() {
                    (E.timeOut > 0 || E.extendedTimeOut > 0) && (k = setTimeout(b, E.extendedTimeOut), F.maxHideTime = parseFloat(E.extendedTimeOut), F.hideEta = (new Date).getTime() + F.maxHideTime)
                }

                function H() {
                    clearTimeout(k), F.hideEta = 0, I.stop(!0, !0)[E.showMethod]({
                        duration: E.showDuration,
                        easing: E.showEasing
                    })
                }

                function x() {
                    var e = (F.hideEta - (new Date).getTime()) / F.maxHideTime * 100;
                    q.width(e + "%")
                }

                var E = m(), y = t.iconClass || E.iconClass;
                if ("undefined" != typeof t.optionsOverride && (E = e.extend(E, t.optionsOverride), y = t.optionsOverride.iconClass || y), !O(E, t)) {
                    T++, v = n(E, !0);
                    var k = null, I = e("<div/>"), M = e("<div/>"), B = e("<div/>"), q = e("<div/>"),
                        j = e(E.closeHtml), F = {intervalId: null, hideEta: null, maxHideTime: null},
                        P = {toastId: T, state: "visible", startTime: new Date, options: E, map: t};
                    return s(), r(), a(), f(P), E.debug && console && console.log(P), I
                }
            }

            function m() {
                return e.extend({}, p(), b.options)
            }

            function h(e) {
                v || (v = n()), e.is(":visible") || (e.remove(), e = null, 0 === v.children().length && (v.remove(), w = void 0))
            }

            var v, C, w, T = 0, O = {error: "error", info: "info", success: "success", warning: "warning"}, b = {
                clear: r,
                remove: c,
                error: t,
                getContainer: n,
                info: o,
                options: {},
                subscribe: s,
                success: i,
                version: "2.1.4",
                warning: a
            };
            return b
        }()
    })
}("function" == typeof define && define.amd ? define : function (e, t) {
    "undefined" != typeof module && module.exports ? module.exports = t(require("jquery")) : window.toastr = t(window.jQuery)
});/*! tsParticles v1.37.2 by Matteo Bruni */
!function (t, i) {
    if ("object" == typeof exports && "object" == typeof module) module.exports = i(); else if ("function" == typeof define && define.amd) define([], i); else {
        var e = i();
        for (var o in e) ("object" == typeof exports ? exports : t)[o] = e[o]
    }
}(window, (function () {
    return function () {
        "use strict";
        var t, i, e, o, s, n = {}, a = {};

        function r(t) {
            var i = a[t];
            if (void 0 !== i) return i.exports;
            var e = a[t] = {exports: {}};
            return n[t](e, e.exports, r), e.exports
        }

        r.m = n, t = [], r.O = function (i, e, o, s) {
            if (!e) {
                var n = 1 / 0;
                for (d = 0; d < t.length; d++) {
                    e = t[d][0], o = t[d][1], s = t[d][2];
                    for (var a = !0, l = 0; l < e.length; l++) (!1 & s || n >= s) && Object.keys(r.O).every((function (t) {
                        return r.O[t](e[l])
                    })) ? e.splice(l--, 1) : (a = !1, s < n && (n = s));
                    if (a) {
                        t.splice(d--, 1);
                        var c = o();
                        void 0 !== c && (i = c)
                    }
                }
                return i
            }
            s = s || 0;
            for (var d = t.length; d > 0 && t[d - 1][2] > s; d--) t[d] = t[d - 1];
            t[d] = [e, o, s]
        }, r.F = {}, r.E = function (t) {
            Object.keys(r.F).map((function (i) {
                r.F[i](t)
            }))
        }, e = Object.getPrototypeOf ? function (t) {
            return Object.getPrototypeOf(t)
        } : function (t) {
            return t.__proto__
        }, r.t = function (t, o) {
            if (1 & o && (t = this(t)), 8 & o) return t;
            if ("object" == typeof t && t) {
                if (4 & o && t.__esModule) return t;
                if (16 & o && "function" == typeof t.then) return t
            }
            var s = Object.create(null);
            r.r(s);
            var n = {};
            i = i || [null, e({}), e([]), e(e)];
            for (var a = 2 & o && t; "object" == typeof a && !~i.indexOf(a); a = e(a)) Object.getOwnPropertyNames(a).forEach((function (i) {
                n[i] = function () {
                    return t[i]
                }
            }));
            return n.default = function () {
                return t
            }, r.d(s, n), s
        }, r.d = function (t, i) {
            for (var e in i) r.o(i, e) && !r.o(t, e) && Object.defineProperty(t, e, {enumerable: !0, get: i[e]})
        }, r.f = {}, r.e = function (t) {
            return Promise.all(Object.keys(r.f).reduce((function (i, e) {
                return r.f[e](t, i), i
            }), []))
        }, r.u = function (t) {
            return "tsparticles.pathseg.js"
        }, r.g = function () {
            if ("object" == typeof globalThis) return globalThis;
            try {
                return this || new Function("return this")()
            } catch (t) {
                if ("object" == typeof window) return window
            }
        }(), r.o = function (t, i) {
            return Object.prototype.hasOwnProperty.call(t, i)
        }, o = {}, s = "tsparticles:", r.l = function (t, i, e, n) {
            if (o[t]) o[t].push(i); else {
                var a, l;
                if (void 0 !== e) for (var c = document.getElementsByTagName("script"), d = 0; d < c.length; d++) {
                    var h = c[d];
                    if (h.getAttribute("src") == t || h.getAttribute("data-webpack") == s + e) {
                        a = h;
                        break
                    }
                }
                a || (l = !0, (a = document.createElement("script")).charset = "utf-8", a.timeout = 120, r.nc && a.setAttribute("nonce", r.nc), a.setAttribute("data-webpack", s + e), a.src = t), o[t] = [i];
                var u = function (i, e) {
                    a.onerror = a.onload = null, clearTimeout(p);
                    var s = o[t];
                    if (delete o[t], a.parentNode && a.parentNode.removeChild(a), s && s.forEach((function (t) {
                        return t(e)
                    })), i) return i(e)
                }, p = setTimeout(u.bind(null, void 0, {type: "timeout", target: a}), 12e4);
                a.onerror = u.bind(null, a.onerror), a.onload = u.bind(null, a.onload), l && document.head.appendChild
            }
        }, r.r = function (t) {
            "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {value: "Module"}), Object.defineProperty(t, "__esModule", {value: !0})
        }, function () {
            var t;
            r.g.importScripts && (t = r.g.location + "");
            var i = r.g.document;
            if (!t && i && (i.currentScript && (t = i.currentScript.src), !t)) {
                var e = i.getElementsByTagName("script");
                e.length && (t = e[e.length - 1].src)
            }
            if (!t) throw new Error("Automatic publicPath is not supported in this browser");
            t = t.replace(/#.*$/, "").replace(/\?.*$/, "").replace(/\/[^\/]+$/, "/"), r.p = t
        }(), function () {
            var t = {155: 0, 649: 0};
            r.f.j = function (i, e) {
                var o = r.o(t, i) ? t[i] : void 0;
                if (0 !== o) if (o) e.push(o[2]); else {
                    var s = new Promise((function (e, s) {
                        o = t[i] = [e, s]
                    }));
                    e.push(o[2] = s);
                    var n = r.p + r.u(i), a = new Error;
                    r.l(n, (function (e) {
                        if (r.o(t, i) && (0 !== (o = t[i]) && (t[i] = void 0), o)) {
                            var s = e && ("load" === e.type ? "missing" : e.type), n = e && e.target && e.target.src;
                            a.message = "Loading chunk " + i + " failed.\n(" + s + ": " + n + ")", a.name = "ChunkLoadError", a.type = s, a.request = n, o[1](a)
                        }
                    }), "chunk-" + i, i)
                }
            }, r.F.j = function (i) {
                if (!r.o(t, i) || void 0 === t[i]) {
                    t[i] = null;
                    var e = document.createElement("link");
                    r.nc && e.setAttribute("nonce", r.nc), e.rel = "prefetch", e.as = "script", e.href = r.p + r.u(i), document.head.appendChild
                }
            }, r.O.j = function (i) {
                return 0 === t[i]
            };
            var i = function (i, e) {
                var o, s, n = e[0], a = e[1], l = e[2], c = 0;
                if (n.some((function (i) {
                    return 0 !== t[i]
                }))) {
                    for (o in a) r.o(a, o) && (r.m[o] = a[o]);
                    if (l) var d = l(r)
                }
                for (i && i(e); c < n.length; c++) s = n[c], r.o(t, s) && t[s] && t[s][0](), t[n[c]] = 0;
                return r.O(d)
            }, e = window.webpackChunktsparticles = window.webpackChunktsparticles || [];
            e.forEach(i.bind(null, 0)), e.push = i.bind(null, e.push.bind(e))
        }(), r.O(0, [155], (function () {
            r.E(917)
        }), 5);
        var l = {};
        r.r(l), r.d(l, {
            AbsorberClickMode: function () {
                return Ts
            }, AlterType: function () {
                return M
            }, AnimationStatus: function () {
                return P
            }, Circle: function () {
                return ni
            }, CircleWarp: function () {
                return ri
            }, ClickMode: function () {
                return p
            }, CollisionMode: function () {
                return m
            }, Constants: function () {
                return ft
            }, Container: function () {
                return Po
            }, DestroyMode: function () {
                return v
            }, DestroyType: function () {
                return z
            }, DivMode: function () {
                return f
            }, DivType: function () {
                return E
            }, EasingType: function () {
                return R
            }, EmitterClickMode: function () {
                return qs
            }, EmitterShapeType: function () {
                return Fs
            }, ExternalInteractorBase: function () {
                return Ao
            }, GradientType: function () {
                return C
            }, HoverMode: function () {
                return y
            }, InlineArrangement: function () {
                return _s
            }, InteractivityDetect: function () {
                return D
            }, InteractorType: function () {
                return O
            }, Main: function () {
                return To
            }, MoveDirection: function () {
                return c
            }, MoveType: function () {
                return Vs
            }, OrbitType: function () {
                return A
            }, OutMode: function () {
                return b
            }, OutModeDirection: function () {
                return h
            }, Particle: function () {
                return Te
            }, ParticlesInteractorBase: function () {
                return Zo
            }, Point: function () {
                return gi
            }, Rectangle: function () {
                return ai
            }, ResponsiveMode: function () {
                return k
            }, RollMode: function () {
                return g
            }, RotateDirection: function () {
                return d
            }, ShapeType: function () {
                return S
            }, SizeMode: function () {
                return w
            }, StartValueType: function () {
                return T
            }, ThemeMode: function () {
                return x
            }, TiltDirection: function () {
                return u
            }, Type: function () {
                return Bs
            }, Vector: function () {
                return I
            }, alterHsl: function () {
                return oi
            }, animate: function () {
                return Z
            }, areBoundsInside: function () {
                return nt
            }, arrayRandomIndex: function () {
                return et
            }, calcEasing: function () {
                return Q
            }, calculateBounds: function () {
                return at
            }, cancelAnimation: function () {
                return K
            }, circleBounce: function () {
                return pt
            }, circleBounceDataFromParticle: function () {
                return ut
            }, clamp: function () {
                return L
            }, clear: function () {
                return Ut
            }, collisionVelocity: function () {
                return $
            }, colorMix: function () {
                return qt
            }, colorToHsl: function () {
                return gt
            }, colorToRgb: function () {
                return bt
            }, deepExtend: function () {
                return rt
            }, divMode: function () {
                return ht
            }, divModeExecute: function () {
                return ct
            }, drawConnectLine: function () {
                return Qt
            }, drawEllipse: function () {
                return ei
            }, drawGrabLine: function () {
                return Xt
            }, drawLinkLine: function () {
                return Nt
            }, drawLinkTriangle: function () {
                return $t
            }, drawParticle: function () {
                return Yt
            }, drawParticlePlugin: function () {
                return ii
            }, drawPlugin: function () {
                return ti
            }, drawShape: function () {
                return Zt
            }, drawShapeAfterEffect: function () {
                return Kt
            }, getDistance: function () {
                return j
            }, getDistances: function () {
                return G
            }, getHslAnimationFromHsl: function () {
                return Bt
            }, getHslFromAnimation: function () {
                return Vt
            }, getLinkColor: function () {
                return Ft
            }, getLinkRandomColor: function () {
                return _t
            }, getParticleBaseVelocity: function () {
                return N
            }, getParticleDirectionAngle: function () {
                return U
            }, getRandomRgbColor: function () {
                return Dt
            }, getRangeMax: function () {
                return V
            }, getRangeMin: function () {
                return _
            }, getRangeValue: function () {
                return F
            }, getStyleFromHsl: function () {
                return Lt
            }, getStyleFromHsv: function () {
                return Ht
            }, getStyleFromRgb: function () {
                return It
            }, getValue: function () {
                return W
            }, gradient: function () {
                return Jt
            }, hslToHsv: function () {
                return zt
            }, hslToRgb: function () {
                return Pt
            }, hslaToHsva: function () {
                return Ct
            }, hslaToRgba: function () {
                return Mt
            }, hsvToHsl: function () {
                return Ot
            }, hsvToRgb: function () {
                return Tt
            }, hsvaToHsla: function () {
                return St
            }, hsvaToRgba: function () {
                return Et
            }, isDivModeEnabled: function () {
                return lt
            }, isInArray: function () {
                return tt
            }, isPointInside: function () {
                return st
            }, isSsr: function () {
                return Y
            }, itemFromArray: function () {
                return ot
            }, loadFont: function () {
                return it
            }, mix: function () {
                return H
            }, pJSDom: function () {
                return fn
            }, paintBase: function () {
                return jt
            }, particlesJS: function () {
                return vn
            }, randomInRange: function () {
                return q
            }, rectBounce: function () {
                return vt
            }, rgbToHsl: function () {
                return wt
            }, rgbToHsv: function () {
                return Rt
            }, rgbaToHsva: function () {
                return At
            }, setRangeValue: function () {
                return B
            }, singleDivModeExecute: function () {
                return dt
            }, stringToAlpha: function () {
                return xt
            }, stringToRgb: function () {
                return kt
            }, tsParticles: function () {
                return pn
            }
        });
        var c, d, h, u, p, v, f, y, m, b, g, w, x, k, P, M, z, C, O, S, T, E, R, A, D;
        !function (t) {
            t.bottom = "bottom", t.bottomLeft = "bottom-left", t.bottomRight = "bottom-right", t.left = "left", t.none = "none", t.right = "right", t.top = "top", t.topLeft = "top-left", t.topRight = "top-right"
        }(c || (c = {})), function (t) {
            t.clockwise = "clockwise", t.counterClockwise = "counter-clockwise", t.random = "random"
        }(d || (d = {})), function (t) {
            t.bottom = "bottom", t.left = "left", t.right = "right", t.top = "top"
        }(h || (h = {})), function (t) {
            t.clockwise = "clockwise", t.counterClockwise = "counter-clockwise", t.random = "random"
        }(u || (u = {})), function (t) {
            t.attract = "attract", t.bubble = "bubble", t.push = "push", t.remove = "remove", t.repulse = "repulse", t.pause = "pause", t.trail = "trail"
        }(p || (p = {})), function (t) {
            t.none = "none", t.split = "split"
        }(v || (v = {})), function (t) {
            t.bounce = "bounce", t.bubble = "bubble", t.repulse = "repulse"
        }(f || (f = {})), function (t) {
            t.attract = "attract", t.bounce = "bounce", t.bubble = "bubble", t.connect = "connect", t.grab = "grab", t.light = "light", t.repulse = "repulse", t.slow = "slow", t.trail = "trail"
        }(y || (y = {})), function (t) {
            t.absorb = "absorb", t.bounce = "bounce", t.destroy = "destroy"
        }(m || (m = {})), function (t) {
            t.bounce = "bounce", t.bounceHorizontal = "bounce-horizontal", t.bounceVertical = "bounce-vertical", t.none = "none", t.out = "out", t.destroy = "destroy", t.split = "split"
        }(b || (b = {})), function (t) {
            t.both = "both", t.horizontal = "horizontal", t.vertical = "vertical"
        }(g || (g = {})), function (t) {
            t.precise = "precise", t.percent = "percent"
        }(w || (w = {})), function (t) {
            t.any = "any", t.dark = "dark", t.light = "light"
        }(x || (x = {})), function (t) {
            t.screen = "screen", t.canvas = "canvas"
        }(k || (k = {})), function (t) {
            t[t.increasing = 0] = "increasing", t[t.decreasing = 1] = "decreasing"
        }(P || (P = {})), function (t) {
            t.darken = "darken", t.enlighten = "enlighten"
        }(M || (M = {})), function (t) {
            t.none = "none", t.max = "max", t.min = "min"
        }(z || (z = {})), function (t) {
            t.linear = "linear", t.radial = "radial", t.random = "random"
        }(C || (C = {})), function (t) {
            t[t.External = 0] = "External", t[t.Particles = 1] = "Particles"
        }(O || (O = {})), function (t) {
            t.char = "char", t.character = "character", t.circle = "circle", t.edge = "edge", t.image = "image", t.images = "images", t.line = "line", t.polygon = "polygon", t.square = "square", t.star = "star", t.triangle = "triangle"
        }(S || (S = {})), function (t) {
            t.max = "max", t.min = "min", t.random = "random"
        }(T || (T = {})), function (t) {
            t.circle = "circle", t.rectangle = "rectangle"
        }(E || (E = {})), function (t) {
            t.easeOutBack = "ease-out-back", t.easeOutCirc = "ease-out-circ", t.easeOutCubic = "ease-out-cubic", t.easeOutQuad = "ease-out-quad", t.easeOutQuart = "ease-out-quart", t.easeOutQuint = "ease-out-quint", t.easeOutExpo = "ease-out-expo", t.easeOutSine = "ease-out-sine"
        }(R || (R = {})), function (t) {
            t.front = "front", t.back = "back"
        }(A || (A = {})), function (t) {
            t.canvas = "canvas", t.parent = "parent", t.window = "window"
        }(D || (D = {}));

        class I {
            constructor(t, i) {
                let e, o;
                if (void 0 === i) {
                    if ("number" == typeof t) throw new Error("tsParticles - Vector not initialized correctly");
                    const i = t;
                    [e, o] = [i.x, i.y]
                } else [e, o] = [t, i];
                this.x = e, this.y = o
            }

            static clone(t) {
                return I.create(t.x, t.y)
            }

            static create(t, i) {
                return new I(t, i)
            }

            static get origin() {
                return I.create(0, 0)
            }

            get angle() {
                return Math.atan2(this.y, this.x)
            }

            set angle(t) {
                this.updateFromAngle(t, this.length)
            }

            get length() {
                return Math.sqrt(this.x ** 2 + this.y ** 2)
            }

            set length(t) {
                this.updateFromAngle(this.angle, t)
            }

            add(t) {
                return I.create(this.x + t.x, this.y + t.y)
            }

            addTo(t) {
                this.x += t.x, this.y += t.y
            }

            sub(t) {
                return I.create(this.x - t.x, this.y - t.y)
            }

            subFrom(t) {
                this.x -= t.x, this.y -= t.y
            }

            mult(t) {
                return I.create(this.x * t, this.y * t)
            }

            multTo(t) {
                this.x *= t, this.y *= t
            }

            div(t) {
                return I.create(this.x / t, this.y / t)
            }

            divTo(t) {
                this.x /= t, this.y /= t
            }

            distanceTo(t) {
                return this.sub(t).length
            }

            getLengthSq() {
                return this.x ** 2 + this.y ** 2
            }

            distanceToSq(t) {
                return this.sub(t).getLengthSq()
            }

            manhattanDistanceTo(t) {
                return Math.abs(t.x - this.x) + Math.abs(t.y - this.y)
            }

            copy() {
                return I.clone(this)
            }

            setTo(t) {
                this.x = t.x, this.y = t.y
            }

            rotate(t) {
                return I.create(this.x * Math.cos(t) - this.y * Math.sin(t), this.x * Math.sin(t) + this.y * Math.cos(t))
            }

            updateFromAngle(t, i) {
                this.x = Math.cos(t) * i, this.y = Math.sin(t) * i
            }
        }

        function L(t, i, e) {
            return Math.min(Math.max(t, i), e)
        }

        function H(t, i, e, o) {
            return Math.floor((t * e + i * o) / (e + o))
        }

        function q(t) {
            const i = V(t);
            let e = _(t);
            return i === e && (e = 0), Math.random() * (i - e) + e
        }

        function F(t) {
            return "number" == typeof t ? t : q(t)
        }

        function _(t) {
            return "number" == typeof t ? t : t.min
        }

        function V(t) {
            return "number" == typeof t ? t : t.max
        }

        function B(t, i) {
            if (t === i || void 0 === i && "number" == typeof t) return t;
            const e = _(t), o = V(t);
            return void 0 !== i ? {min: Math.min(e, i), max: Math.max(o, i)} : B(e, o)
        }

        function W(t) {
            const i = t.random, {enable: e, minimumValue: o} = "boolean" == typeof i ? {enable: i, minimumValue: 0} : i;
            return F(e ? B(t.value, o) : t.value)
        }

        function G(t, i) {
            const e = t.x - i.x, o = t.y - i.y;
            return {dx: e, dy: o, distance: Math.sqrt(e * e + o * o)}
        }

        function j(t, i) {
            return G(t, i).distance
        }

        function U(t) {
            if ("number" == typeof t) return t * Math.PI / 180;
            switch (t) {
                case c.top:
                    return -Math.PI / 2;
                case c.topRight:
                    return -Math.PI / 4;
                case c.right:
                    return 0;
                case c.bottomRight:
                    return Math.PI / 4;
                case c.bottom:
                    return Math.PI / 2;
                case c.bottomLeft:
                    return 3 * Math.PI / 4;
                case c.left:
                    return Math.PI;
                case c.topLeft:
                    return -3 * Math.PI / 4;
                case c.none:
                default:
                    return Math.random() * Math.PI * 2
            }
        }

        function N(t) {
            const i = I.origin;
            return i.length = 1, i.angle = t, i
        }

        function $(t, i, e, o) {
            return I.create(t.x * (e - o) / (e + o) + 2 * i.x * o / (e + o), t.y)
        }

        function Q(t, i) {
            switch (i) {
                case R.easeOutQuad:
                    return 1 - (1 - t) ** 2;
                case R.easeOutCubic:
                    return 1 - (1 - t) ** 3;
                case R.easeOutQuart:
                    return 1 - (1 - t) ** 4;
                case R.easeOutQuint:
                    return 1 - (1 - t) ** 5;
                case R.easeOutExpo:
                    return 1 === t ? 1 : 1 - Math.pow(2, -10 * t);
                case R.easeOutSine:
                    return Math.sin(t * Math.PI / 2);
                case R.easeOutBack: {
                    const i = 1.70158;
                    return 1 + (i + 1) * Math.pow(t - 1, 3) + i * Math.pow(t - 1, 2)
                }
                case R.easeOutCirc:
                    return Math.sqrt(1 - Math.pow(t - 1, 2));
                default:
                    return t
            }
        }

        function J(t, i, e, o, s, n) {
            const a = {bounced: !1};
            return i.min >= o.min && i.min <= o.max && i.max >= o.min && i.max <= o.max && (t.max >= e.min && t.max <= (e.max + e.min) / 2 && s > 0 || t.min <= e.max && t.min > (e.max + e.min) / 2 && s < 0) && (a.velocity = s * -n, a.bounced = !0), a
        }

        function X(t, i) {
            if (i instanceof Array) {
                for (const e of i) if (t.matches(e)) return !0;
                return !1
            }
            return t.matches(i)
        }

        function Y() {
            return "undefined" == typeof window || !window || void 0 === window.document || !window.document
        }

        function Z() {
            return Y() ? t => setTimeout(t) : t => (window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || window.setTimeout)(t)
        }

        function K() {
            return Y() ? t => clearTimeout(t) : t => (window.cancelAnimationFrame || window.webkitCancelRequestAnimationFrame || window.mozCancelRequestAnimationFrame || window.oCancelRequestAnimationFrame || window.msCancelRequestAnimationFrame || window.clearTimeout)(t)
        }

        function tt(t, i) {
            return t === i || i instanceof Array && i.indexOf(t) > -1
        }

        async function it(t) {
            var i, e;
            try {
                await document.fonts.load(`${null !== (i = t.weight) && void 0 !== i ? i : "400"} 36px '${null !== (e = t.font) && void 0 !== e ? e : "Verdana"}'`)
            } catch (t) {
            }
        }

        function et(t) {
            return Math.floor(Math.random() * t.length)
        }

        function ot(t, i, e = !0) {
            return t[void 0 !== i && e ? i % t.length : et(t)]
        }

        function st(t, i, e, o) {
            return nt(at(t, null != e ? e : 0), i, o)
        }

        function nt(t, i, e) {
            let o = !0;
            return e && e !== h.bottom || (o = t.top < i.height), !o || e && e !== h.left || (o = t.right > 0), !o || e && e !== h.right || (o = t.left < i.width), !o || e && e !== h.top || (o = t.bottom > 0), o
        }

        function at(t, i) {
            return {bottom: t.y + i, left: t.x - i, right: t.x + i, top: t.y - i}
        }

        function rt(t, ...i) {
            for (const e of i) {
                if (null == e) continue;
                if ("object" != typeof e) {
                    t = e;
                    continue
                }
                const i = Array.isArray(e);
                !i || "object" == typeof t && t && Array.isArray(t) ? i || "object" == typeof t && t && !Array.isArray(t) || (t = {}) : t = [];
                for (const i in e) {
                    if ("__proto__" === i) continue;
                    const o = e[i], s = "object" == typeof o, n = t;
                    n[i] = s && Array.isArray(o) ? o.map((t => rt(n[i], t))) : rt(n[i], o)
                }
            }
            return t
        }

        function lt(t, i) {
            return i instanceof Array ? !!i.find((i => i.enable && tt(t, i.mode))) : tt(t, i.mode)
        }

        function ct(t, i, e) {
            if (i instanceof Array) for (const o of i) {
                const i = o.mode;
                o.enable && tt(t, i) && dt(o, e)
            } else {
                const o = i.mode;
                i.enable && tt(t, o) && dt(i, e)
            }
        }

        function dt(t, i) {
            const e = t.selectors;
            if (e instanceof Array) for (const o of e) i(o, t); else i(e, t)
        }

        function ht(t, i) {
            if (i && t) return t instanceof Array ? t.find((t => X(i, t.selectors))) : X(i, t.selectors) ? t : void 0
        }

        function ut(t) {
            return {
                position: t.getPosition(),
                radius: t.getRadius(),
                mass: t.getMass(),
                velocity: t.velocity,
                factor: I.create(W(t.options.bounce.horizontal), W(t.options.bounce.vertical))
            }
        }

        function pt(t, i) {
            const e = t.velocity.x, o = t.velocity.y, s = t.position, n = i.position;
            if (e * (n.x - s.x) + o * (n.y - s.y) >= 0) {
                const e = -Math.atan2(n.y - s.y, n.x - s.x), o = t.mass, a = i.mass, r = t.velocity.rotate(e),
                    l = i.velocity.rotate(e), c = $(r, l, o, a), d = $(l, r, o, a), h = c.rotate(-e), u = d.rotate(-e);
                t.velocity.x = h.x * t.factor.x, t.velocity.y = h.y * t.factor.y, i.velocity.x = u.x * i.factor.x, i.velocity.y = u.y * i.factor.y
            }
        }

        function vt(t, i) {
            const e = at(t.getPosition(), t.getRadius()),
                o = J({min: e.left, max: e.right}, {min: e.top, max: e.bottom}, {
                    min: i.left,
                    max: i.right
                }, {min: i.top, max: i.bottom}, t.velocity.x, W(t.options.bounce.horizontal));
            o.bounced && (void 0 !== o.velocity && (t.velocity.x = o.velocity), void 0 !== o.position && (t.position.x = o.position));
            const s = J({min: e.top, max: e.bottom}, {min: e.left, max: e.right}, {
                min: i.top,
                max: i.bottom
            }, {min: i.left, max: i.right}, t.velocity.y, W(t.options.bounce.vertical));
            s.bounced && (void 0 !== s.velocity && (t.velocity.y = s.velocity), void 0 !== s.position && (t.position.y = s.position))
        }

        class ft {
        }

        function yt(t, i, e) {
            let o = e;
            return o < 0 && (o += 1), o > 1 && (o -= 1), o < 1 / 6 ? t + 6 * (i - t) * o : o < .5 ? i : o < 2 / 3 ? t + (i - t) * (2 / 3 - o) * 6 : t
        }

        function mt(t) {
            if (t.startsWith("rgb")) {
                const i = /rgba?\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*(,\s*([\d.]+)\s*)?\)/i.exec(t);
                return i ? {
                    a: i.length > 4 ? parseFloat(i[5]) : 1,
                    b: parseInt(i[3], 10),
                    g: parseInt(i[2], 10),
                    r: parseInt(i[1], 10)
                } : void 0
            }
            if (t.startsWith("hsl")) {
                const i = /hsla?\(\s*(\d+)\s*,\s*(\d+)%\s*,\s*(\d+)%\s*(,\s*([\d.]+)\s*)?\)/i.exec(t);
                return i ? Mt({
                    a: i.length > 4 ? parseFloat(i[5]) : 1,
                    h: parseInt(i[1], 10),
                    l: parseInt(i[3], 10),
                    s: parseInt(i[2], 10)
                }) : void 0
            }
            if (t.startsWith("hsv")) {
                const i = /hsva?\(\s*(\d+)°\s*,\s*(\d+)%\s*,\s*(\d+)%\s*(,\s*([\d.]+)\s*)?\)/i.exec(t);
                return i ? Et({
                    a: i.length > 4 ? parseFloat(i[5]) : 1,
                    h: parseInt(i[1], 10),
                    s: parseInt(i[2], 10),
                    v: parseInt(i[3], 10)
                }) : void 0
            }
            {
                const i = /^#?([a-f\d])([a-f\d])([a-f\d])([a-f\d])?$/i,
                    e = t.replace(i, ((t, i, e, o, s) => i + i + e + e + o + o + (void 0 !== s ? s + s : ""))),
                    o = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})?$/i.exec(e);
                return o ? {
                    a: void 0 !== o[4] ? parseInt(o[4], 16) / 255 : 1,
                    b: parseInt(o[3], 16),
                    g: parseInt(o[2], 16),
                    r: parseInt(o[1], 16)
                } : void 0
            }
        }

        function bt(t, i, e = !0) {
            var o, s, n;
            if (void 0 === t) return;
            const a = "string" == typeof t ? {value: t} : t;
            let r;
            if ("string" == typeof a.value) r = a.value === ft.randomColorValue ? Dt() : kt(a.value); else if (a.value instanceof Array) {
                r = bt({value: ot(a.value, i, e)})
            } else {
                const t = a.value, i = null !== (o = t.rgb) && void 0 !== o ? o : a.value;
                if (void 0 !== i.r) r = i; else {
                    const i = null !== (s = t.hsl) && void 0 !== s ? s : a.value;
                    if (void 0 !== i.h && void 0 !== i.l) r = Pt(i); else {
                        const i = null !== (n = t.hsv) && void 0 !== n ? n : a.value;
                        void 0 !== i.h && void 0 !== i.v && (r = Tt(i))
                    }
                }
            }
            return r
        }

        function gt(t, i, e = !0) {
            const o = bt(t, i, e);
            return void 0 !== o ? wt(o) : void 0
        }

        function wt(t) {
            const i = t.r / 255, e = t.g / 255, o = t.b / 255, s = Math.max(i, e, o), n = Math.min(i, e, o),
                a = {h: 0, l: (s + n) / 2, s: 0};
            return s != n && (a.s = a.l < .5 ? (s - n) / (s + n) : (s - n) / (2 - s - n), a.h = i === s ? (e - o) / (s - n) : a.h = e === s ? 2 + (o - i) / (s - n) : 4 + (i - e) / (s - n)), a.l *= 100, a.s *= 100, a.h *= 60, a.h < 0 && (a.h += 360), a
        }

        function xt(t) {
            var i;
            return null === (i = mt(t)) || void 0 === i ? void 0 : i.a
        }

        function kt(t) {
            return mt(t)
        }

        function Pt(t) {
            const i = {b: 0, g: 0, r: 0}, e = {h: t.h / 360, l: t.l / 100, s: t.s / 100};
            if (0 === e.s) i.b = e.l, i.g = e.l, i.r = e.l; else {
                const t = e.l < .5 ? e.l * (1 + e.s) : e.l + e.s - e.l * e.s, o = 2 * e.l - t;
                i.r = yt(o, t, e.h + 1 / 3), i.g = yt(o, t, e.h), i.b = yt(o, t, e.h - 1 / 3)
            }
            return i.r = Math.floor(255 * i.r), i.g = Math.floor(255 * i.g), i.b = Math.floor(255 * i.b), i
        }

        function Mt(t) {
            const i = Pt(t);
            return {a: t.a, b: i.b, g: i.g, r: i.r}
        }

        function zt(t) {
            const i = t.l / 100, e = i + t.s / 100 * Math.min(i, 1 - i), o = e ? 2 * (1 - i / e) : 0;
            return {h: t.h, s: 100 * o, v: 100 * e}
        }

        function Ct(t) {
            const i = zt(t);
            return {a: t.a, h: i.h, s: i.s, v: i.v}
        }

        function Ot(t) {
            const i = t.v / 100, e = i * (1 - t.s / 100 / 2), o = 0 === e || 1 === e ? 0 : (i - e) / Math.min(e, 1 - e);
            return {h: t.h, l: 100 * e, s: 100 * o}
        }

        function St(t) {
            const i = Ot(t);
            return {a: t.a, h: i.h, l: i.l, s: i.s}
        }

        function Tt(t) {
            const i = {b: 0, g: 0, r: 0}, e = t.h / 60, o = t.s / 100, s = t.v / 100, n = s * o,
                a = n * (1 - Math.abs(e % 2 - 1));
            let r;
            if (e >= 0 && e <= 1 ? r = {r: n, g: a, b: 0} : e > 1 && e <= 2 ? r = {
                r: a,
                g: n,
                b: 0
            } : e > 2 && e <= 3 ? r = {r: 0, g: n, b: a} : e > 3 && e <= 4 ? r = {
                r: 0,
                g: a,
                b: n
            } : e > 4 && e <= 5 ? r = {r: a, g: 0, b: n} : e > 5 && e <= 6 && (r = {r: n, g: 0, b: a}), r) {
                const t = s - n;
                i.r = Math.floor(255 * (r.r + t)), i.g = Math.floor(255 * (r.g + t)), i.b = Math.floor(255 * (r.b + t))
            }
            return i
        }

        function Et(t) {
            const i = Tt(t);
            return {a: t.a, b: i.b, g: i.g, r: i.r}
        }

        function Rt(t) {
            const i = {r: t.r / 255, g: t.g / 255, b: t.b / 255}, e = Math.max(i.r, i.g, i.b),
                o = e - Math.min(i.r, i.g, i.b);
            let s = 0;
            e === i.r ? s = (i.g - i.b) / o * 60 : e === i.g ? s = 60 * (2 + (i.b - i.r) / o) : e === i.b && (s = 60 * (4 + (i.r - i.g) / o));
            return {h: s, s: 100 * (e ? o / e : 0), v: 100 * e}
        }

        function At(t) {
            const i = Rt(t);
            return {a: t.a, h: i.h, s: i.s, v: i.v}
        }

        function Dt(t) {
            const i = null != t ? t : 0;
            return {b: Math.floor(q(B(i, 256))), g: Math.floor(q(B(i, 256))), r: Math.floor(q(B(i, 256)))}
        }

        function It(t, i) {
            return `rgba(${t.r}, ${t.g}, ${t.b}, ${null != i ? i : 1})`
        }

        function Lt(t, i) {
            return `hsla(${t.h}, ${t.s}%, ${t.l}%, ${null != i ? i : 1})`
        }

        function Ht(t, i) {
            return Lt(Ot(t), i)
        }

        function qt(t, i, e, o) {
            let s = t, n = i;
            return void 0 === s.r && (s = Pt(t)), void 0 === n.r && (n = Pt(i)), {
                b: H(s.b, n.b, e, o),
                g: H(s.g, n.g, e, o),
                r: H(s.r, n.r, e, o)
            }
        }

        function Ft(t, i, e) {
            var o, s;
            if (e === ft.randomColorValue) return Dt();
            if ("mid" !== e) return e;
            {
                const e = null !== (o = t.getFillColor()) && void 0 !== o ? o : t.getStrokeColor(),
                    n = null !== (s = null == i ? void 0 : i.getFillColor()) && void 0 !== s ? s : null == i ? void 0 : i.getStrokeColor();
                if (e && n && i) return qt(e, n, t.getRadius(), i.getRadius());
                {
                    const t = null != e ? e : n;
                    if (t) return Pt(t)
                }
            }
        }

        function _t(t, i, e) {
            const o = "string" == typeof t ? t : t.value;
            return o === ft.randomColorValue ? e ? bt({value: o}) : i ? ft.randomColorValue : ft.midColorValue : bt({value: o})
        }

        function Vt(t) {
            return void 0 !== t ? {h: t.h.value, s: t.s.value, l: t.l.value} : void 0
        }

        function Bt(t, i, e) {
            const o = {h: {enable: !1, value: t.h}, s: {enable: !1, value: t.s}, l: {enable: !1, value: t.l}};
            return i && (Wt(o.h, i.h, e), Wt(o.s, i.s, e), Wt(o.l, i.l, e)), o
        }

        function Wt(t, i, e) {
            if (t.enable = i.enable, t.enable) {
                if (t.velocity = i.speed / 100 * e, i.sync) return;
                t.status = P.increasing, t.velocity *= Math.random(), t.value && (t.value *= Math.random())
            } else t.velocity = 0
        }

        function Gt(t, i, e) {
            t.beginPath(), t.moveTo(i.x, i.y), t.lineTo(e.x, e.y), t.closePath()
        }

        function jt(t, i, e) {
            t.save(), t.fillStyle = null != e ? e : "rgba(0,0,0,0)", t.fillRect(0, 0, i.width, i.height), t.restore()
        }

        function Ut(t, i) {
            t.clearRect(0, 0, i.width, i.height)
        }

        function Nt(t, i, e, o, s, n, a, r, l, c, d, h) {
            let u = !1;
            if (j(e, o) <= s) Gt(t, e, o), u = !0; else if (a) {
                let i, a;
                const r = G(e, {x: o.x - n.width, y: o.y});
                if (r.distance <= s) {
                    const t = e.y - r.dy / r.dx * e.x;
                    i = {x: 0, y: t}, a = {x: n.width, y: t}
                } else {
                    const t = G(e, {x: o.x, y: o.y - n.height});
                    if (t.distance <= s) {
                        const o = -(e.y - t.dy / t.dx * e.x) / (t.dy / t.dx);
                        i = {x: o, y: 0}, a = {x: o, y: n.height}
                    } else {
                        const t = G(e, {x: o.x - n.width, y: o.y - n.height});
                        if (t.distance <= s) {
                            const o = e.y - t.dy / t.dx * e.x;
                            i = {x: -o / (t.dy / t.dx), y: o}, a = {x: i.x + n.width, y: i.y + n.height}
                        }
                    }
                }
                i && a && (Gt(t, e, i), Gt(t, o, a), u = !0)
            }
            if (u) {
                if (t.lineWidth = i, r && (t.globalCompositeOperation = l), t.strokeStyle = It(c, d), h.enable) {
                    const i = bt(h.color);
                    i && (t.shadowBlur = h.blur, t.shadowColor = It(i))
                }
                t.stroke()
            }
        }

        function $t(t, i, e, o, s, n, a, r) {
            !function (t, i, e, o) {
                t.beginPath(), t.moveTo(i.x, i.y), t.lineTo(e.x, e.y), t.lineTo(o.x, o.y), t.closePath()
            }(t, i, e, o), s && (t.globalCompositeOperation = n), t.fillStyle = It(a, r), t.fill()
        }

        function Qt(t, i, e, o, s) {
            t.save(), Gt(t, o, s), t.lineWidth = i, t.strokeStyle = e, t.stroke(), t.restore()
        }

        function Jt(t, i, e, o) {
            const s = Math.floor(e.getRadius() / i.getRadius()), n = i.getFillColor(), a = e.getFillColor();
            if (!n || !a) return;
            const r = i.getPosition(), l = e.getPosition(), c = qt(n, a, i.getRadius(), e.getRadius()),
                d = t.createLinearGradient(r.x, r.y, l.x, l.y);
            return d.addColorStop(0, Lt(n, o)), d.addColorStop(s > 1 ? 1 : s, It(c, o)), d.addColorStop(1, Lt(a, o)), d
        }

        function Xt(t, i, e, o, s, n) {
            t.save(), Gt(t, e, o), t.strokeStyle = It(s, n), t.lineWidth = i, t.stroke(), t.restore()
        }

        function Yt(t, i, e, o, s, n, a, r, l, c, d, h) {
            var u, p, v, f, y, m;
            const b = e.getPosition(), w = e.options.tilt, x = e.options.roll;
            if (i.save(), w.enable || x.enable) {
                const t = x.enable && e.roll, o = w.enable && e.tilt,
                    s = t && (x.mode === g.horizontal || x.mode === g.both),
                    n = t && (x.mode === g.vertical || x.mode === g.both);
                i.setTransform(s ? Math.cos(e.roll.angle) : 1, o ? Math.cos(e.tilt.value) * e.tilt.cosDirection : 0, o ? Math.sin(e.tilt.value) * e.tilt.sinDirection : 0, n ? Math.sin(e.roll.angle) : 1, b.x, b.y)
            } else i.translate(b.x, b.y);
            i.beginPath();
            const k = (null !== (p = null === (u = e.rotate) || void 0 === u ? void 0 : u.value) && void 0 !== p ? p : 0) + (e.options.rotate.path ? e.velocity.angle : 0);
            0 !== k && i.rotate(k), a && (i.globalCompositeOperation = r);
            const P = e.shadowColor;
            if (d.enable && P && (i.shadowBlur = d.blur, i.shadowColor = It(P), i.shadowOffsetX = d.offset.x, i.shadowOffsetY = d.offset.y), h) {
                const t = h.angle.value,
                    e = h.type === C.radial ? i.createRadialGradient(0, 0, 0, 0, 0, l) : i.createLinearGradient(Math.cos(t) * -l, Math.sin(t) * -l, Math.cos(t) * l, Math.sin(t) * l);
                for (const t of h.colors) e.addColorStop(t.stop, Lt({
                    h: t.value.h.value,
                    s: t.value.s.value,
                    l: t.value.l.value
                }, null !== (f = null === (v = t.opacity) || void 0 === v ? void 0 : v.value) && void 0 !== f ? f : c));
                i.fillStyle = e
            } else s && (i.fillStyle = s);
            const M = e.stroke;
            i.lineWidth = null !== (y = e.strokeWidth) && void 0 !== y ? y : 0, n && (i.strokeStyle = n), Zt(t, i, e, l, c, o), (null !== (m = null == M ? void 0 : M.width) && void 0 !== m ? m : 0) > 0 && i.stroke(), e.close && i.closePath(), e.fill && i.fill(), i.restore(), i.save(), w.enable && e.tilt ? i.setTransform(1, Math.cos(e.tilt.value) * e.tilt.cosDirection, Math.sin(e.tilt.value) * e.tilt.sinDirection, 1, b.x, b.y) : i.translate(b.x, b.y), 0 !== k && i.rotate(k), a && (i.globalCompositeOperation = r), Kt(t, i, e, l, c, o), i.restore()
        }

        function Zt(t, i, e, o, s, n) {
            if (!e.shape) return;
            const a = t.drawers.get(e.shape);
            a && a.draw(i, e, o, s, n, t.retina.pixelRatio)
        }

        function Kt(t, i, e, o, s, n) {
            if (!e.shape) return;
            const a = t.drawers.get(e.shape);
            (null == a ? void 0 : a.afterEffect) && a.afterEffect(i, e, o, s, n, t.retina.pixelRatio)
        }

        function ti(t, i, e) {
            i.draw && (t.save(), i.draw(t, e), t.restore())
        }

        function ii(t, i, e, o) {
            void 0 !== i.drawParticle && (t.save(), i.drawParticle(t, e, o), t.restore())
        }

        function ei(t, i, e, o, s, n, a, r, l) {
            const c = i.getPosition();
            if (e && (t.strokeStyle = Lt(e, s)), 0 === n) return;
            t.lineWidth = n;
            const d = a * Math.PI / 180;
            t.beginPath(), t.ellipse(c.x, c.y, o / 2, 2 * o, d, r, l), t.stroke()
        }

        function oi(t, i, e) {
            return {h: t.h, s: t.s, l: t.l + (i === M.darken ? -1 : 1) * e}
        }

        ft.canvasClass = "tsparticles-canvas-el", ft.randomColorValue = "random", ft.midColorValue = "mid", ft.touchEndEvent = "touchend", ft.mouseDownEvent = "mousedown", ft.mouseUpEvent = "mouseup", ft.mouseMoveEvent = "mousemove", ft.touchStartEvent = "touchstart", ft.touchMoveEvent = "touchmove", ft.mouseLeaveEvent = "mouseleave", ft.mouseOutEvent = "mouseout", ft.touchCancelEvent = "touchcancel", ft.resizeEvent = "resize", ft.visibilityChangeEvent = "visibilitychange", ft.noPolygonDataLoaded = "No polygon data loaded.", ft.noPolygonFound = "No polygon found, you need to specify SVG url in config.";

        class si {
            constructor(t, i) {
                this.position = {x: t, y: i}
            }
        }

        class ni extends si {
            constructor(t, i, e) {
                super(t, i), this.radius = e
            }

            contains(t) {
                return j(t, this.position) <= this.radius
            }

            intersects(t) {
                const i = t, e = t, o = this.position, s = t.position, n = Math.abs(s.x - o.x), a = Math.abs(s.y - o.y),
                    r = this.radius;
                if (void 0 !== e.radius) {
                    return r + e.radius > Math.sqrt(n * n + a + a)
                }
                if (void 0 !== i.size) {
                    const t = i.size.width, e = i.size.height, o = Math.pow(n - t, 2) + Math.pow(a - e, 2);
                    return !(n > r + t || a > r + e) && (n <= t || a <= e || o <= r * r)
                }
                return !1
            }
        }

        class ai extends si {
            constructor(t, i, e, o) {
                super(t, i), this.size = {height: o, width: e}
            }

            contains(t) {
                const i = this.size.width, e = this.size.height, o = this.position;
                return t.x >= o.x && t.x <= o.x + i && t.y >= o.y && t.y <= o.y + e
            }

            intersects(t) {
                const i = t, e = t, o = this.size.width, s = this.size.height, n = this.position, a = t.position;
                if (void 0 !== e.radius) return e.intersects(this);
                if (void 0 !== i.size) {
                    const t = i.size, e = t.width, r = t.height;
                    return a.x < n.x + o && a.x + e > n.x && a.y < n.y + s && a.y + r > n.y
                }
                return !1
            }
        }

        class ri extends ni {
            constructor(t, i, e, o) {
                super(t, i, e), this.canvasSize = o, this.canvasSize = {height: o.height, width: o.width}
            }

            contains(t) {
                if (super.contains(t)) return !0;
                const i = {x: t.x - this.canvasSize.width, y: t.y};
                if (super.contains(i)) return !0;
                const e = {x: t.x - this.canvasSize.width, y: t.y - this.canvasSize.height};
                if (super.contains(e)) return !0;
                const o = {x: t.x, y: t.y - this.canvasSize.height};
                return super.contains(o)
            }

            intersects(t) {
                if (super.intersects(t)) return !0;
                const i = t, e = t,
                    o = {x: t.position.x - this.canvasSize.width, y: t.position.y - this.canvasSize.height};
                if (void 0 !== e.radius) {
                    const t = new ni(o.x, o.y, 2 * e.radius);
                    return super.intersects(t)
                }
                if (void 0 !== i.size) {
                    const t = new ai(o.x, o.y, 2 * i.size.width, 2 * i.size.height);
                    return super.intersects(t)
                }
                return !1
            }
        }

        function li(t, i, e, o, s) {
            if (o) {
                let o = {passive: !0};
                "boolean" == typeof s ? o.capture = s : void 0 !== s && (o = s), t.addEventListener(i, e, o)
            } else {
                const o = s;
                t.removeEventListener(i, e, o)
            }
        }

        class ci {
            constructor(t) {
                this.container = t, this.canPush = !0, this.mouseMoveHandler = t => this.mouseTouchMove(t), this.touchStartHandler = t => this.mouseTouchMove(t), this.touchMoveHandler = t => this.mouseTouchMove(t), this.touchEndHandler = () => this.mouseTouchFinish(), this.mouseLeaveHandler = () => this.mouseTouchFinish(), this.touchCancelHandler = () => this.mouseTouchFinish(), this.touchEndClickHandler = t => this.mouseTouchClick(t), this.mouseUpHandler = t => this.mouseTouchClick(t), this.mouseDownHandler = () => this.mouseDown(), this.visibilityChangeHandler = () => this.handleVisibilityChange(), this.themeChangeHandler = t => this.handleThemeChange(t), this.resizeHandler = () => this.handleWindowResize()
            }

            addListeners() {
                this.manageListeners(!0)
            }

            removeListeners() {
                this.manageListeners(!1)
            }

            manageListeners(t) {
                var i;
                const e = this.container, o = e.actualOptions, s = o.interactivity.detectsOn;
                let n = ft.mouseLeaveEvent;
                if (s === D.window) e.interactivity.element = window, n = ft.mouseOutEvent; else if (s === D.parent && e.canvas.element) {
                    const t = e.canvas.element;
                    e.interactivity.element = null !== (i = t.parentElement) && void 0 !== i ? i : t.parentNode
                } else e.interactivity.element = e.canvas.element;
                const a = "undefined" != typeof matchMedia && matchMedia("(prefers-color-scheme: dark)");
                a && li(a, "change", this.themeChangeHandler, t);
                const r = e.interactivity.element;
                if (!r) return;
                const l = r;
                (o.interactivity.events.onHover.enable || o.interactivity.events.onClick.enable) && (li(r, ft.mouseMoveEvent, this.mouseMoveHandler, t), li(r, ft.touchStartEvent, this.touchStartHandler, t), li(r, ft.touchMoveEvent, this.touchMoveHandler, t), o.interactivity.events.onClick.enable ? (li(r, ft.touchEndEvent, this.touchEndClickHandler, t), li(r, ft.mouseUpEvent, this.mouseUpHandler, t), li(r, ft.mouseDownEvent, this.mouseDownHandler, t)) : li(r, ft.touchEndEvent, this.touchEndHandler, t), li(r, n, this.mouseLeaveHandler, t), li(r, ft.touchCancelEvent, this.touchCancelHandler, t)), e.canvas.element && (e.canvas.element.style.pointerEvents = l === e.canvas.element ? "initial" : "none"), o.interactivity.events.resize && ("undefined" != typeof ResizeObserver ? this.resizeObserver && !t ? (e.canvas.element && this.resizeObserver.unobserve(e.canvas.element), this.resizeObserver.disconnect(), delete this.resizeObserver) : !this.resizeObserver && t && e.canvas.element && (this.resizeObserver = new ResizeObserver((t => {
                    t.find((t => t.target === e.canvas.element)) && this.handleWindowResize()
                })), this.resizeObserver.observe(e.canvas.element)) : li(window, ft.resizeEvent, this.resizeHandler, t)), document && li(document, ft.visibilityChangeEvent, this.visibilityChangeHandler, t, !1)
            }

            handleWindowResize() {
                this.resizeTimeout && (clearTimeout(this.resizeTimeout), delete this.resizeTimeout), this.resizeTimeout = setTimeout((() => {
                    var t;
                    return null === (t = this.container.canvas) || void 0 === t ? void 0 : t.windowResize()
                }), 500)
            }

            handleVisibilityChange() {
                const t = this.container, i = t.actualOptions;
                this.mouseTouchFinish(), i.pauseOnBlur && ((null === document || void 0 === document ? void 0 : document.hidden) ? (t.pageHidden = !0, t.pause()) : (t.pageHidden = !1, t.getAnimationStatus() ? t.play(!0) : t.draw(!0)))
            }

            mouseDown() {
                const t = this.container.interactivity;
                if (t) {
                    const i = t.mouse;
                    i.clicking = !0, i.downPosition = i.position
                }
            }

            mouseTouchMove(t) {
                var i, e, o, s, n, a, r;
                const l = this.container, c = l.actualOptions;
                if (void 0 === (null === (i = l.interactivity) || void 0 === i ? void 0 : i.element)) return;
                let d;
                l.interactivity.mouse.inside = !0;
                const h = l.canvas.element;
                if (t.type.startsWith("mouse")) {
                    this.canPush = !0;
                    const i = t;
                    if (l.interactivity.element === window) {
                        if (h) {
                            const t = h.getBoundingClientRect();
                            d = {x: i.clientX - t.left, y: i.clientY - t.top}
                        }
                    } else if (c.interactivity.detectsOn === D.parent) {
                        const t = i.target, s = i.currentTarget, n = l.canvas.element;
                        if (t && s && n) {
                            const e = t.getBoundingClientRect(), o = s.getBoundingClientRect(),
                                a = n.getBoundingClientRect();
                            d = {
                                x: i.offsetX + 2 * e.left - (o.left + a.left),
                                y: i.offsetY + 2 * e.top - (o.top + a.top)
                            }
                        } else d = {
                            x: null !== (e = i.offsetX) && void 0 !== e ? e : i.clientX,
                            y: null !== (o = i.offsetY) && void 0 !== o ? o : i.clientY
                        }
                    } else i.target === l.canvas.element && (d = {
                        x: null !== (s = i.offsetX) && void 0 !== s ? s : i.clientX,
                        y: null !== (n = i.offsetY) && void 0 !== n ? n : i.clientY
                    })
                } else {
                    this.canPush = "touchmove" !== t.type;
                    const i = t, e = i.touches[i.touches.length - 1],
                        o = null == h ? void 0 : h.getBoundingClientRect();
                    d = {
                        x: e.clientX - (null !== (a = null == o ? void 0 : o.left) && void 0 !== a ? a : 0),
                        y: e.clientY - (null !== (r = null == o ? void 0 : o.top) && void 0 !== r ? r : 0)
                    }
                }
                const u = l.retina.pixelRatio;
                d && (d.x *= u, d.y *= u), l.interactivity.mouse.position = d, l.interactivity.status = ft.mouseMoveEvent
            }

            mouseTouchFinish() {
                const t = this.container.interactivity;
                if (void 0 === t) return;
                const i = t.mouse;
                delete i.position, delete i.clickPosition, delete i.downPosition, t.status = ft.mouseLeaveEvent, i.inside = !1, i.clicking = !1
            }

            mouseTouchClick(t) {
                const i = this.container, e = i.actualOptions, o = i.interactivity.mouse;
                o.inside = !0;
                let s = !1;
                const n = o.position;
                if (void 0 !== n && e.interactivity.events.onClick.enable) {
                    for (const [, t] of i.plugins) if (void 0 !== t.clickPositionValid && (s = t.clickPositionValid(n), s)) break;
                    s || this.doMouseTouchClick(t), o.clicking = !1
                }
            }

            doMouseTouchClick(t) {
                const i = this.container, e = i.actualOptions;
                if (this.canPush) {
                    const t = i.interactivity.mouse.position;
                    if (!t) return;
                    i.interactivity.mouse.clickPosition = {
                        x: t.x,
                        y: t.y
                    }, i.interactivity.mouse.clickTime = (new Date).getTime();
                    const o = e.interactivity.events.onClick;
                    if (o.mode instanceof Array) for (const t of o.mode) this.handleClickMode(t); else this.handleClickMode(o.mode)
                }
                "touchend" === t.type && setTimeout((() => this.mouseTouchFinish()), 500)
            }

            handleThemeChange(t) {
                const i = t.matches ? this.container.options.defaultDarkTheme : this.container.options.defaultLightTheme,
                    e = this.container.options.themes.find((t => t.name === i));
                e && e.default.auto && this.container.loadTheme(i)
            }

            handleClickMode(t) {
                const i = this.container, e = i.actualOptions, o = e.interactivity.modes.push.quantity,
                    s = e.interactivity.modes.remove.quantity;
                switch (t) {
                    case p.push:
                        if (o > 0) {
                            const t = ot([void 0, ...e.interactivity.modes.push.groups]),
                                s = void 0 !== t ? i.actualOptions.particles.groups[t] : void 0;
                            i.particles.push(o, i.interactivity.mouse, s, t)
                        }
                        break;
                    case p.remove:
                        i.particles.removeQuantity(s);
                        break;
                    case p.bubble:
                        i.bubble.clicking = !0;
                        break;
                    case p.repulse:
                        i.repulse.clicking = !0, i.repulse.count = 0;
                        for (const t of i.repulse.particles) t.velocity.setTo(t.initialVelocity);
                        i.repulse.particles = [], i.repulse.finish = !1, setTimeout((() => {
                            i.destroyed || (i.repulse.clicking = !1)
                        }), 1e3 * e.interactivity.modes.repulse.duration);
                        break;
                    case p.attract:
                        i.attract.clicking = !0, i.attract.count = 0;
                        for (const t of i.attract.particles) t.velocity.setTo(t.initialVelocity);
                        i.attract.particles = [], i.attract.finish = !1, setTimeout((() => {
                            i.destroyed || (i.attract.clicking = !1)
                        }), 1e3 * e.interactivity.modes.attract.duration);
                        break;
                    case p.pause:
                        i.getAnimationStatus() ? i.pause() : i.play()
                }
                for (const [, e] of i.plugins) e.handleClickMode && e.handleClickMode(t)
            }
        }

        const di = [], hi = new Map, ui = new Map, pi = new Map, vi = new Map, fi = new Map, yi = new Map, mi = new Map;

        class bi {
            static getPlugin(t) {
                return di.find((i => i.id === t))
            }

            static addPlugin(t) {
                bi.getPlugin(t.id) || di.push(t)
            }

            static getAvailablePlugins(t) {
                const i = new Map;
                for (const e of di) e.needsPlugin(t.actualOptions) && i.set(e.id, e.getPlugin(t));
                return i
            }

            static loadOptions(t, i) {
                for (const e of di) e.loadOptions(t, i)
            }

            static getPreset(t) {
                return fi.get(t)
            }

            static addPreset(t, i, e = !1) {
                !e && bi.getPreset(t) || fi.set(t, i)
            }

            static addShapeDrawer(t, i) {
                bi.getShapeDrawer(t) || yi.set(t, i)
            }

            static getShapeDrawer(t) {
                return yi.get(t)
            }

            static getSupportedShapes() {
                return yi.keys()
            }

            static getPathGenerator(t) {
                return mi.get(t)
            }

            static addPathGenerator(t, i) {
                bi.getPathGenerator(t) || mi.set(t, i)
            }

            static getInteractors(t, i = !1) {
                let e = pi.get(t);
                return e && !i || (e = [...hi.values()].map((i => i(t))), pi.set(t, e)), e
            }

            static addInteractor(t, i) {
                hi.set(t, i)
            }

            static getUpdaters(t, i = !1) {
                let e = vi.get(t);
                return e && !i || (e = [...ui.values()].map((i => i(t))), vi.set(t, e)), e
            }

            static addParticleUpdater(t, i) {
                ui.set(t, i)
            }
        }

        class gi {
            constructor(t, i) {
                this.position = t, this.particle = i
            }
        }

        class wi {
            constructor(t, i) {
                this.rectangle = t, this.capacity = i, this.points = [], this.divided = !1
            }

            subdivide() {
                const t = this.rectangle.position.x, i = this.rectangle.position.y, e = this.rectangle.size.width,
                    o = this.rectangle.size.height, s = this.capacity;
                this.northEast = new wi(new ai(t, i, e / 2, o / 2), s), this.northWest = new wi(new ai(t + e / 2, i, e / 2, o / 2), s), this.southEast = new wi(new ai(t, i + o / 2, e / 2, o / 2), s), this.southWest = new wi(new ai(t + e / 2, i + o / 2, e / 2, o / 2), s), this.divided = !0
            }

            insert(t) {
                var i, e, o, s, n;
                return !!this.rectangle.contains(t.position) && (this.points.length < this.capacity ? (this.points.push(t), !0) : (this.divided || this.subdivide(), null !== (n = (null === (i = this.northEast) || void 0 === i ? void 0 : i.insert(t)) || (null === (e = this.northWest) || void 0 === e ? void 0 : e.insert(t)) || (null === (o = this.southEast) || void 0 === o ? void 0 : o.insert(t)) || (null === (s = this.southWest) || void 0 === s ? void 0 : s.insert(t))) && void 0 !== n && n))
            }

            queryCircle(t, i) {
                return this.query(new ni(t.x, t.y, i))
            }

            queryCircleWarp(t, i, e) {
                const o = e, s = e;
                return this.query(new ri(t.x, t.y, i, void 0 !== o.canvas ? o.canvas.size : s))
            }

            queryRectangle(t, i) {
                return this.query(new ai(t.x, t.y, i.width, i.height))
            }

            query(t, i) {
                var e, o, s, n;
                const a = null != i ? i : [];
                if (!t.intersects(this.rectangle)) return [];
                for (const i of this.points) !t.contains(i.position) && j(t.position, i.position) > i.particle.getRadius() || a.push(i.particle);
                return this.divided && (null === (e = this.northEast) || void 0 === e || e.query(t, a), null === (o = this.northWest) || void 0 === o || o.query(t, a), null === (s = this.southEast) || void 0 === s || s.query(t, a), null === (n = this.southWest) || void 0 === n || n.query(t, a)), a
            }
        }

        class xi {
            constructor(t) {
                this.container = t, this.size = {height: 0, width: 0}, this.context = null, this.generatedCanvas = !1
            }

            init() {
                this.resize(), this.initStyle(), this.initCover(), this.initTrail(), this.initBackground(), this.paint()
            }

            loadCanvas(t, i) {
                var e;
                t.className || (t.className = ft.canvasClass), this.generatedCanvas && (null === (e = this.element) || void 0 === e || e.remove()), this.generatedCanvas = null != i ? i : this.generatedCanvas, this.element = t, this.originalStyle = rt({}, this.element.style), this.size.height = t.offsetHeight, this.size.width = t.offsetWidth, this.context = this.element.getContext("2d"), this.container.retina.init(), this.initBackground()
            }

            destroy() {
                var t;
                this.generatedCanvas && (null === (t = this.element) || void 0 === t || t.remove()), this.draw((t => {
                    Ut(t, this.size)
                }))
            }

            paint() {
                const t = this.container.actualOptions;
                this.draw((i => {
                    t.backgroundMask.enable && t.backgroundMask.cover && this.coverColor ? (Ut(i, this.size), this.paintBase(It(this.coverColor, this.coverColor.a))) : this.paintBase()
                }))
            }

            clear() {
                const t = this.container.actualOptions, i = t.particles.move.trail;
                t.backgroundMask.enable ? this.paint() : i.enable && i.length > 0 && this.trailFillColor ? this.paintBase(It(this.trailFillColor, 1 / i.length)) : this.draw((t => {
                    Ut(t, this.size)
                }))
            }

            windowResize() {
                if (!this.element) return;
                const t = this.container;
                this.resize();
                const i = t.updateActualOptions();
                t.particles.setDensity();
                for (const [, i] of t.plugins) void 0 !== i.resize && i.resize();
                i && t.refresh()
            }

            resize() {
                if (!this.element) return;
                const t = this.container, i = t.retina.pixelRatio, e = t.canvas.size, o = e.width, s = e.height;
                e.width = this.element.offsetWidth * i, e.height = this.element.offsetHeight * i, this.element.width = e.width, this.element.height = e.height, this.container.started && (this.resizeFactor = {
                    width: e.width / o,
                    height: e.height / s
                })
            }

            drawConnectLine(t, i) {
                this.draw((e => {
                    var o;
                    const s = this.lineStyle(t, i);
                    if (!s) return;
                    const n = t.getPosition(), a = i.getPosition();
                    Qt(e, null !== (o = t.retina.linksWidth) && void 0 !== o ? o : this.container.retina.linksWidth, s, n, a)
                }))
            }

            drawGrabLine(t, i, e, o) {
                const s = this.container;
                this.draw((n => {
                    var a;
                    const r = t.getPosition();
                    Xt(n, null !== (a = t.retina.linksWidth) && void 0 !== a ? a : s.retina.linksWidth, r, o, i, e)
                }))
            }

            drawParticle(t, i) {
                var e, o, s, n, a, r;
                if (t.spawning || t.destroyed) return;
                const l = t.getFillColor(), c = null !== (e = t.getStrokeColor()) && void 0 !== e ? e : l;
                if (!l && !c) return;
                let [d, h] = this.getPluginParticleColors(t);
                const u = t.options.twinkle.particles, p = u.enable && Math.random() < u.frequency;
                if (!d || !h) {
                    const t = gt(u.color);
                    d || (d = p && void 0 !== t ? t : l || void 0), h || (h = p && void 0 !== t ? t : c || void 0)
                }
                const v = this.container.actualOptions, f = t.options.zIndex, y = (1 - t.zIndexFactor) ** f.opacityRate,
                    m = t.getRadius(),
                    b = p ? u.opacity : null !== (n = null !== (o = t.bubble.opacity) && void 0 !== o ? o : null === (s = t.opacity) || void 0 === s ? void 0 : s.value) && void 0 !== n ? n : 1,
                    g = null !== (r = null === (a = t.stroke) || void 0 === a ? void 0 : a.opacity) && void 0 !== r ? r : b,
                    w = b * y, x = d ? Lt(d, w) : void 0;
                (x || h) && this.draw((e => {
                    const o = (1 - t.zIndexFactor) ** f.sizeRate, s = h ? Lt(h, g * y) : x;
                    if (m <= 0) return;
                    const n = this.container;
                    for (const i of n.particles.updaters) i.beforeDraw && i.beforeDraw(t);
                    Yt(this.container, e, t, i, x, s, v.backgroundMask.enable, v.backgroundMask.composite, m * o, w, t.options.shadow, t.gradient);
                    for (const i of n.particles.updaters) i.afterDraw && i.afterDraw(t)
                }))
            }

            drawPlugin(t, i) {
                this.draw((e => {
                    ti(e, t, i)
                }))
            }

            drawParticlePlugin(t, i, e) {
                this.draw((o => {
                    ii(o, t, i, e)
                }))
            }

            initBackground() {
                const t = this.container.actualOptions.background, i = this.element, e = null == i ? void 0 : i.style;
                if (e) {
                    if (t.color) {
                        const i = bt(t.color);
                        e.backgroundColor = i ? It(i, t.opacity) : ""
                    } else e.backgroundColor = "";
                    e.backgroundImage = t.image || "", e.backgroundPosition = t.position || "", e.backgroundRepeat = t.repeat || "", e.backgroundSize = t.size || ""
                }
            }

            draw(t) {
                if (this.context) return t(this.context)
            }

            initCover() {
                const t = this.container.actualOptions.backgroundMask.cover, i = bt(t.color);
                i && (this.coverColor = {r: i.r, g: i.g, b: i.b, a: t.opacity})
            }

            initTrail() {
                const t = this.container.actualOptions, i = bt(t.particles.move.trail.fillColor);
                if (i) {
                    const e = t.particles.move.trail;
                    this.trailFillColor = {r: i.r, g: i.g, b: i.b, a: 1 / e.length}
                }
            }

            getPluginParticleColors(t) {
                let i, e;
                for (const [, o] of this.container.plugins) if (!i && o.particleFillColor && (i = gt(o.particleFillColor(t))), !e && o.particleStrokeColor && (e = gt(o.particleStrokeColor(t))), i && e) break;
                return [i, e]
            }

            initStyle() {
                const t = this.element, i = this.container.actualOptions;
                if (!t) return;
                const e = this.originalStyle;
                i.fullScreen.enable ? (this.originalStyle = rt({}, t.style), t.style.position = "fixed", t.style.zIndex = i.fullScreen.zIndex.toString(10), t.style.top = "0", t.style.left = "0", t.style.width = "100%", t.style.height = "100%") : e && (t.style.position = e.position, t.style.zIndex = e.zIndex, t.style.top = e.top, t.style.left = e.left, t.style.width = e.width, t.style.height = e.height)
            }

            paintBase(t) {
                this.draw((i => {
                    jt(i, this.size, t)
                }))
            }

            lineStyle(t, i) {
                return this.draw((e => {
                    const o = this.container.actualOptions.interactivity.modes.connect;
                    return Jt(e, t, i, o.links.opacity)
                }))
            }
        }

        class ki {
            constructor() {
                this.value = "#fff"
            }

            static create(t, i) {
                const e = new ki;
                return e.load(t), void 0 !== i && ("string" == typeof i || i instanceof Array ? e.load({value: i}) : e.load(i)), e
            }

            load(t) {
                void 0 !== (null == t ? void 0 : t.value) && (this.value = t.value)
            }
        }

        class Pi {
            constructor() {
                this.blur = 5, this.color = new ki, this.enable = !1, this.color.value = "#00ff00"
            }

            load(t) {
                void 0 !== t && (void 0 !== t.blur && (this.blur = t.blur), this.color = ki.create(this.color, t.color), void 0 !== t.enable && (this.enable = t.enable))
            }
        }

        class Mi {
            constructor() {
                this.enable = !1, this.frequency = 1
            }

            load(t) {
                void 0 !== t && (void 0 !== t.color && (this.color = ki.create(this.color, t.color)), void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.frequency && (this.frequency = t.frequency), void 0 !== t.opacity && (this.opacity = t.opacity))
            }
        }

        class zi {
            constructor() {
                this.blink = !1, this.color = new ki, this.consent = !1, this.distance = 100, this.enable = !1, this.frequency = 1, this.opacity = 1, this.shadow = new Pi, this.triangles = new Mi, this.width = 1, this.warp = !1
            }

            load(t) {
                void 0 !== t && (void 0 !== t.id && (this.id = t.id), void 0 !== t.blink && (this.blink = t.blink), this.color = ki.create(this.color, t.color), void 0 !== t.consent && (this.consent = t.consent), void 0 !== t.distance && (this.distance = t.distance), void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.frequency && (this.frequency = t.frequency), void 0 !== t.opacity && (this.opacity = t.opacity), this.shadow.load(t.shadow), this.triangles.load(t.triangles), void 0 !== t.width && (this.width = t.width), void 0 !== t.warp && (this.warp = t.warp))
            }
        }

        class Ci {
            constructor() {
                this.distance = 200, this.enable = !1, this.rotate = {x: 3e3, y: 3e3}
            }

            get rotateX() {
                return this.rotate.x
            }

            set rotateX(t) {
                this.rotate.x = t
            }

            get rotateY() {
                return this.rotate.y
            }

            set rotateY(t) {
                this.rotate.y = t
            }

            load(t) {
                var i, e, o, s;
                if (!t) return;
                void 0 !== t.distance && (this.distance = t.distance), void 0 !== t.enable && (this.enable = t.enable);
                const n = null !== (e = null === (i = t.rotate) || void 0 === i ? void 0 : i.x) && void 0 !== e ? e : t.rotateX;
                void 0 !== n && (this.rotate.x = n);
                const a = null !== (s = null === (o = t.rotate) || void 0 === o ? void 0 : o.y) && void 0 !== s ? s : t.rotateY;
                void 0 !== a && (this.rotate.y = a)
            }
        }

        class Oi {
            constructor() {
                this.enable = !1, this.length = 10, this.fillColor = new ki, this.fillColor.value = "#000000"
            }

            load(t) {
                void 0 !== t && (void 0 !== t.enable && (this.enable = t.enable), this.fillColor = ki.create(this.fillColor, t.fillColor), void 0 !== t.length && (this.length = t.length))
            }
        }

        class Si {
            constructor() {
                this.enable = !1, this.minimumValue = 0
            }

            load(t) {
                t && (void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.minimumValue && (this.minimumValue = t.minimumValue))
            }
        }

        class Ti {
            constructor() {
                this.random = new Si, this.value = 0
            }

            load(t) {
                t && ("boolean" == typeof t.random ? this.random.enable = t.random : this.random.load(t.random), void 0 !== t.value && (this.value = B(t.value, this.random.enable ? this.random.minimumValue : void 0)))
            }
        }

        class Ei extends Ti {
            constructor() {
                super()
            }
        }

        class Ri {
            constructor() {
                this.clamp = !0, this.delay = new Ei, this.enable = !1, this.options = {}
            }

            load(t) {
                void 0 !== t && (void 0 !== t.clamp && (this.clamp = t.clamp), this.delay.load(t.delay), void 0 !== t.enable && (this.enable = t.enable), this.generator = t.generator, t.options && (this.options = rt(this.options, t.options)))
            }
        }

        class Ai {
            constructor() {
                this.offset = 0, this.value = 90
            }

            load(t) {
                void 0 !== t && (void 0 !== t.offset && (this.offset = t.offset), void 0 !== t.value && (this.value = t.value))
            }
        }

        class Di {
            constructor() {
                this.acceleration = 9.81, this.enable = !1, this.inverse = !1, this.maxSpeed = 50
            }

            load(t) {
                t && (void 0 !== t.acceleration && (this.acceleration = t.acceleration), void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.inverse && (this.inverse = t.inverse), void 0 !== t.maxSpeed && (this.maxSpeed = t.maxSpeed))
            }
        }

        class Ii {
            constructor() {
                this.default = b.out
            }

            load(t) {
                var i, e, o, s;
                t && (void 0 !== t.default && (this.default = t.default), this.bottom = null !== (i = t.bottom) && void 0 !== i ? i : t.default, this.left = null !== (e = t.left) && void 0 !== e ? e : t.default, this.right = null !== (o = t.right) && void 0 !== o ? o : t.default, this.top = null !== (s = t.top) && void 0 !== s ? s : t.default)
            }
        }

        class Li {
            constructor() {
                this.acceleration = 0, this.enable = !1
            }

            load(t) {
                t && (void 0 !== t.acceleration && (this.acceleration = B(t.acceleration)), void 0 !== t.enable && (this.enable = t.enable), this.position = t.position ? rt({}, t.position) : void 0)
            }
        }

        class Hi {
            constructor() {
                this.angle = new Ai, this.attract = new Ci, this.decay = 0, this.distance = {}, this.direction = c.none, this.drift = 0, this.enable = !1, this.gravity = new Di, this.path = new Ri, this.outModes = new Ii, this.random = !1, this.size = !1, this.speed = 2, this.spin = new Li, this.straight = !1, this.trail = new Oi, this.vibrate = !1, this.warp = !1
            }

            get collisions() {
                return !1
            }

            set collisions(t) {
            }

            get bounce() {
                return this.collisions
            }

            set bounce(t) {
                this.collisions = t
            }

            get out_mode() {
                return this.outMode
            }

            set out_mode(t) {
                this.outMode = t
            }

            get outMode() {
                return this.outModes.default
            }

            set outMode(t) {
                this.outModes.default = t
            }

            get noise() {
                return this.path
            }

            set noise(t) {
                this.path = t
            }

            load(t) {
                var i, e, o;
                if (void 0 === t) return;
                void 0 !== t.angle && ("number" == typeof t.angle ? this.angle.value = t.angle : this.angle.load(t.angle)), this.attract.load(t.attract), void 0 !== t.decay && (this.decay = t.decay), void 0 !== t.direction && (this.direction = t.direction), void 0 !== t.distance && (this.distance = "number" == typeof t.distance ? {
                    horizontal: t.distance,
                    vertical: t.distance
                } : rt({}, t.distance)), void 0 !== t.drift && (this.drift = B(t.drift)), void 0 !== t.enable && (this.enable = t.enable), this.gravity.load(t.gravity);
                const s = null !== (i = t.outMode) && void 0 !== i ? i : t.out_mode;
                void 0 === t.outModes && void 0 === s || ("string" == typeof t.outModes || void 0 === t.outModes && void 0 !== s ? this.outModes.load({default: null !== (e = t.outModes) && void 0 !== e ? e : s}) : this.outModes.load(t.outModes)), this.path.load(null !== (o = t.path) && void 0 !== o ? o : t.noise), void 0 !== t.random && (this.random = t.random), void 0 !== t.size && (this.size = t.size), void 0 !== t.speed && (this.speed = B(t.speed)), this.spin.load(t.spin), void 0 !== t.straight && (this.straight = t.straight), this.trail.load(t.trail), void 0 !== t.vibrate && (this.vibrate = t.vibrate), void 0 !== t.warp && (this.warp = t.warp)
            }
        }

        class qi {
            constructor() {
                this.enable = !1, this.area = 800, this.factor = 1e3
            }

            get value_area() {
                return this.area
            }

            set value_area(t) {
                this.area = t
            }

            load(t) {
                var i;
                if (void 0 === t) return;
                void 0 !== t.enable && (this.enable = t.enable);
                const e = null !== (i = t.area) && void 0 !== i ? i : t.value_area;
                void 0 !== e && (this.area = e), void 0 !== t.factor && (this.factor = t.factor)
            }
        }

        class Fi {
            constructor() {
                this.density = new qi, this.limit = 0, this.value = 100
            }

            get max() {
                return this.limit
            }

            set max(t) {
                this.limit = t
            }

            load(t) {
                var i;
                if (void 0 === t) return;
                this.density.load(t.density);
                const e = null !== (i = t.limit) && void 0 !== i ? i : t.max;
                void 0 !== e && (this.limit = e), void 0 !== t.value && (this.value = t.value)
            }
        }

        class _i {
            constructor() {
                this.count = 0, this.enable = !1, this.speed = 1, this.sync = !1
            }

            load(t) {
                t && (void 0 !== t.count && (this.count = t.count), void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.speed && (this.speed = t.speed), void 0 !== t.sync && (this.sync = t.sync))
            }
        }

        class Vi extends _i {
            constructor() {
                super(), this.destroy = z.none, this.enable = !1, this.speed = 2, this.startValue = T.random, this.sync = !1
            }

            get opacity_min() {
                return this.minimumValue
            }

            set opacity_min(t) {
                this.minimumValue = t
            }

            load(t) {
                var i;
                void 0 !== t && (super.load(t), void 0 !== t.destroy && (this.destroy = t.destroy), void 0 !== t.enable && (this.enable = t.enable), this.minimumValue = null !== (i = t.minimumValue) && void 0 !== i ? i : t.opacity_min, void 0 !== t.speed && (this.speed = t.speed), void 0 !== t.startValue && (this.startValue = t.startValue), void 0 !== t.sync && (this.sync = t.sync))
            }
        }

        class Bi extends Ti {
            constructor() {
                super(), this.animation = new Vi, this.random.minimumValue = .1, this.value = 1
            }

            get anim() {
                return this.animation
            }

            set anim(t) {
                this.animation = t
            }

            load(t) {
                var i;
                if (!t) return;
                super.load(t);
                const e = null !== (i = t.animation) && void 0 !== i ? i : t.anim;
                void 0 !== e && (this.animation.load(e), this.value = B(this.value, this.animation.enable ? this.animation.minimumValue : void 0))
            }
        }

        class Wi {
            constructor() {
                this.options = {}, this.type = S.circle
            }

            get image() {
                var t;
                return null !== (t = this.options[S.image]) && void 0 !== t ? t : this.options[S.images]
            }

            set image(t) {
                this.options[S.image] = t, this.options[S.images] = t
            }

            get custom() {
                return this.options
            }

            set custom(t) {
                this.options = t
            }

            get images() {
                return this.image
            }

            set images(t) {
                this.image = t
            }

            get stroke() {
                return []
            }

            set stroke(t) {
            }

            get character() {
                var t;
                return null !== (t = this.options[S.character]) && void 0 !== t ? t : this.options[S.char]
            }

            set character(t) {
                this.options[S.character] = t, this.options[S.char] = t
            }

            get polygon() {
                var t;
                return null !== (t = this.options[S.polygon]) && void 0 !== t ? t : this.options[S.star]
            }

            set polygon(t) {
                this.options[S.polygon] = t, this.options[S.star] = t
            }

            load(t) {
                var i, e, o;
                if (void 0 === t) return;
                const s = null !== (i = t.options) && void 0 !== i ? i : t.custom;
                if (void 0 !== s) for (const t in s) {
                    const i = s[t];
                    void 0 !== i && (this.options[t] = rt(null !== (e = this.options[t]) && void 0 !== e ? e : {}, i))
                }
                this.loadShape(t.character, S.character, S.char, !0), this.loadShape(t.polygon, S.polygon, S.star, !1), this.loadShape(null !== (o = t.image) && void 0 !== o ? o : t.images, S.image, S.images, !0), void 0 !== t.type && (this.type = t.type)
            }

            loadShape(t, i, e, o) {
                var s, n, a, r;
                void 0 !== t && (t instanceof Array ? (this.options[i] instanceof Array || (this.options[i] = [], this.options[e] && !o || (this.options[e] = [])), this.options[i] = rt(null !== (s = this.options[i]) && void 0 !== s ? s : [], t), this.options[e] && !o || (this.options[e] = rt(null !== (n = this.options[e]) && void 0 !== n ? n : [], t))) : (this.options[i] instanceof Array && (this.options[i] = {}, this.options[e] && !o || (this.options[e] = {})), this.options[i] = rt(null !== (a = this.options[i]) && void 0 !== a ? a : {}, t), this.options[e] && !o || (this.options[e] = rt(null !== (r = this.options[e]) && void 0 !== r ? r : {}, t))))
            }
        }

        class Gi extends _i {
            constructor() {
                super(), this.destroy = z.none, this.enable = !1, this.speed = 5, this.startValue = T.random, this.sync = !1
            }

            get size_min() {
                return this.minimumValue
            }

            set size_min(t) {
                this.minimumValue = t
            }

            load(t) {
                var i;
                void 0 !== t && (super.load(t), void 0 !== t.destroy && (this.destroy = t.destroy), void 0 !== t.enable && (this.enable = t.enable), this.minimumValue = null !== (i = t.minimumValue) && void 0 !== i ? i : t.size_min, void 0 !== t.speed && (this.speed = t.speed), void 0 !== t.startValue && (this.startValue = t.startValue), void 0 !== t.sync && (this.sync = t.sync))
            }
        }

        class ji extends Ti {
            constructor() {
                super(), this.animation = new Gi, this.random.minimumValue = 1, this.value = 3
            }

            get anim() {
                return this.animation
            }

            set anim(t) {
                this.animation = t
            }

            load(t) {
                var i;
                if (!t) return;
                super.load(t);
                const e = null !== (i = t.animation) && void 0 !== i ? i : t.anim;
                void 0 !== e && (this.animation.load(e), this.value = B(this.value, this.animation.enable ? this.animation.minimumValue : void 0))
            }
        }

        class Ui {
            constructor() {
                this.enable = !1, this.speed = 0, this.sync = !1
            }

            load(t) {
                void 0 !== t && (void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.speed && (this.speed = t.speed), void 0 !== t.sync && (this.sync = t.sync))
            }
        }

        class Ni extends Ti {
            constructor() {
                super(), this.animation = new Ui, this.direction = d.clockwise, this.path = !1, this.value = 0
            }

            load(t) {
                t && (super.load(t), void 0 !== t.direction && (this.direction = t.direction), this.animation.load(t.animation), void 0 !== t.path && (this.path = t.path))
            }
        }

        class $i {
            constructor() {
                this.blur = 0, this.color = new ki, this.enable = !1, this.offset = {
                    x: 0,
                    y: 0
                }, this.color.value = "#000000"
            }

            load(t) {
                void 0 !== t && (void 0 !== t.blur && (this.blur = t.blur), this.color = ki.create(this.color, t.color), void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.offset && (void 0 !== t.offset.x && (this.offset.x = t.offset.x), void 0 !== t.offset.y && (this.offset.y = t.offset.y)))
            }
        }

        class Qi {
            constructor() {
                this.count = 0, this.enable = !1, this.offset = 0, this.speed = 1, this.sync = !0
            }

            load(t) {
                void 0 !== t && (void 0 !== t.count && (this.count = t.count), void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.offset && (this.offset = B(t.offset)), void 0 !== t.speed && (this.speed = t.speed), void 0 !== t.sync && (this.sync = t.sync))
            }
        }

        class Ji {
            constructor() {
                this.h = new Qi, this.s = new Qi, this.l = new Qi
            }

            load(t) {
                t && (this.h.load(t.h), this.s.load(t.s), this.l.load(t.l))
            }
        }

        class Xi extends ki {
            constructor() {
                super(), this.animation = new Ji
            }

            static create(t, i) {
                const e = new Xi;
                return e.load(t), void 0 !== i && ("string" == typeof i || i instanceof Array ? e.load({value: i}) : e.load(i)), e
            }

            load(t) {
                if (super.load(t), !t) return;
                const i = t.animation;
                void 0 !== i && (void 0 !== i.enable ? this.animation.h.load(i) : this.animation.load(t.animation))
            }
        }

        class Yi {
            constructor() {
                this.width = 0
            }

            load(t) {
                void 0 !== t && (void 0 !== t.color && (this.color = Xi.create(this.color, t.color)), void 0 !== t.width && (this.width = t.width), void 0 !== t.opacity && (this.opacity = t.opacity))
            }
        }

        class Zi extends Ti {
            constructor() {
                super(), this.random.minimumValue = .1, this.value = 1
            }
        }

        class Ki {
            constructor() {
                this.horizontal = new Zi, this.vertical = new Zi
            }

            load(t) {
                t && (this.horizontal.load(t.horizontal), this.vertical.load(t.vertical))
            }
        }

        class te {
            constructor() {
                this.enable = !0, this.retries = 0
            }

            load(t) {
                t && (void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.retries && (this.retries = t.retries))
            }
        }

        class ie {
            constructor() {
                this.bounce = new Ki, this.enable = !1, this.mode = m.bounce, this.overlap = new te
            }

            load(t) {
                void 0 !== t && (this.bounce.load(t.bounce), void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.mode && (this.mode = t.mode), this.overlap.load(t.overlap))
            }
        }

        class ee {
            constructor() {
                this.enable = !1, this.frequency = .05, this.opacity = 1
            }

            load(t) {
                void 0 !== t && (void 0 !== t.color && (this.color = ki.create(this.color, t.color)), void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.frequency && (this.frequency = t.frequency), void 0 !== t.opacity && (this.opacity = t.opacity))
            }
        }

        class oe {
            constructor() {
                this.lines = new ee, this.particles = new ee
            }

            load(t) {
                void 0 !== t && (this.lines.load(t.lines), this.particles.load(t.particles))
            }
        }

        class se extends Ti {
            constructor() {
                super(), this.sync = !1
            }

            load(t) {
                t && (super.load(t), void 0 !== t.sync && (this.sync = t.sync))
            }
        }

        class ne extends Ti {
            constructor() {
                super(), this.random.minimumValue = 1e-4, this.sync = !1
            }

            load(t) {
                void 0 !== t && (super.load(t), void 0 !== t.sync && (this.sync = t.sync))
            }
        }

        class ae {
            constructor() {
                this.count = 0, this.delay = new se, this.duration = new ne
            }

            load(t) {
                void 0 !== t && (void 0 !== t.count && (this.count = t.count), this.delay.load(t.delay), this.duration.load(t.duration))
            }
        }

        class re extends Ti {
            constructor() {
                super(), this.value = 3
            }
        }

        class le extends Ti {
            constructor() {
                super(), this.value = {min: 4, max: 9}
            }
        }

        class ce {
            constructor() {
                this.count = 1, this.factor = new re, this.rate = new le, this.sizeOffset = !0
            }

            load(t) {
                t && (void 0 !== t.count && (this.count = t.count), this.factor.load(t.factor), this.rate.load(t.rate), void 0 !== t.particles && (this.particles = rt({}, t.particles)), void 0 !== t.sizeOffset && (this.sizeOffset = t.sizeOffset))
            }
        }

        class de {
            constructor() {
                this.mode = v.none, this.split = new ce
            }

            load(t) {
                t && (void 0 !== t.mode && (this.mode = t.mode), this.split.load(t.split))
            }
        }

        class he {
            constructor() {
                this.distance = 5, this.enable = !1, this.speed = 50
            }

            load(t) {
                t && (void 0 !== t.distance && (this.distance = B(t.distance)), void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.speed && (this.speed = B(t.speed)))
            }
        }

        class ue {
            constructor() {
                this.enable = !1, this.speed = 0, this.sync = !1
            }

            load(t) {
                void 0 !== t && (void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.speed && (this.speed = t.speed), void 0 !== t.sync && (this.sync = t.sync))
            }
        }

        class pe extends Ti {
            constructor() {
                super(), this.animation = new ue, this.direction = u.clockwise, this.enable = !1, this.value = 0
            }

            load(t) {
                t && (super.load(t), this.animation.load(t.animation), void 0 !== t.direction && (this.direction = t.direction), void 0 !== t.enable && (this.enable = t.enable))
            }
        }

        class ve {
            constructor() {
                this.enable = !1, this.value = 0
            }

            load(t) {
                t && (void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.value && (this.value = t.value))
            }
        }

        class fe {
            constructor() {
                this.darken = new ve, this.enable = !1, this.enlighten = new ve, this.mode = g.vertical, this.speed = 25
            }

            load(t) {
                t && (void 0 !== t.backColor && (this.backColor = ki.create(this.backColor, t.backColor)), this.darken.load(t.darken), void 0 !== t.enable && (this.enable = t.enable), this.enlighten.load(t.enlighten), void 0 !== t.mode && (this.mode = t.mode), void 0 !== t.speed && (this.speed = B(t.speed)))
            }
        }

        class ye extends Ti {
            constructor() {
                super(), this.opacityRate = 1, this.sizeRate = 1, this.velocityRate = 1
            }

            load(t) {
                super.load(t), t && (void 0 !== t.opacityRate && (this.opacityRate = t.opacityRate), void 0 !== t.sizeRate && (this.sizeRate = t.sizeRate), void 0 !== t.velocityRate && (this.velocityRate = t.velocityRate))
            }
        }

        class me extends Ti {
            constructor() {
                super(), this.value = 45, this.random.enable = !1, this.random.minimumValue = 0
            }

            load(t) {
                void 0 !== t && super.load(t)
            }
        }

        class be {
            constructor() {
                this.animation = new _i, this.enable = !1, this.opacity = 1, this.rotation = new me, this.width = 1
            }

            load(t) {
                void 0 !== t && (this.animation.load(t.animation), this.rotation.load(t.rotation), void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.opacity && (this.opacity = t.opacity), void 0 !== t.width && (this.width = t.width), void 0 !== t.radius && (this.radius = t.radius), void 0 !== t.color && (this.color = ki.create(this.color, t.color)))
            }
        }

        class ge extends Ti {
            constructor() {
                super(), this.enabled = !1, this.distance = 1, this.duration = 1, this.factor = 1, this.speed = 1
            }

            load(t) {
                super.load(t), t && (void 0 !== t.enabled && (this.enabled = t.enabled), void 0 !== t.distance && (this.distance = t.distance), void 0 !== t.duration && (this.duration = t.duration), void 0 !== t.factor && (this.factor = t.factor), void 0 !== t.speed && (this.speed = t.speed))
            }
        }

        class we {
            constructor() {
                this.angle = new xe, this.colors = [], this.type = C.random
            }

            load(t) {
                t && (this.angle.load(t.angle), void 0 !== t.colors && (this.colors = t.colors.map((t => {
                    const i = new Pe;
                    return i.load(t), i
                }))), void 0 !== t.type && (this.type = t.type))
            }
        }

        class xe {
            constructor() {
                this.value = 0, this.animation = new Me, this.direction = d.clockwise
            }

            load(t) {
                t && (this.animation.load(t.animation), void 0 !== t.value && (this.value = t.value), void 0 !== t.direction && (this.direction = t.direction))
            }
        }

        class ke {
            constructor() {
                this.value = 0, this.animation = new ze
            }

            load(t) {
                t && (this.animation.load(t.animation), void 0 !== t.value && (this.value = B(t.value)))
            }
        }

        class Pe {
            constructor() {
                this.stop = 0, this.value = new Xi
            }

            load(t) {
                t && (void 0 !== t.stop && (this.stop = t.stop), this.value = Xi.create(this.value, t.value), void 0 !== t.opacity && (this.opacity = new ke, "number" == typeof t.opacity ? this.opacity.value = t.opacity : this.opacity.load(t.opacity)))
            }
        }

        class Me {
            constructor() {
                this.count = 0, this.enable = !1, this.speed = 0, this.sync = !1
            }

            load(t) {
                t && (void 0 !== t.count && (this.count = t.count), void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.speed && (this.speed = t.speed), void 0 !== t.sync && (this.sync = t.sync))
            }
        }

        class ze {
            constructor() {
                this.count = 0, this.enable = !1, this.speed = 0, this.sync = !1, this.startValue = T.random
            }

            load(t) {
                t && (void 0 !== t.count && (this.count = t.count), void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.speed && (this.speed = t.speed), void 0 !== t.sync && (this.sync = t.sync), void 0 !== t.startValue && (this.startValue = t.startValue))
            }
        }

        class Ce {
            constructor() {
                this.bounce = new Ki, this.collisions = new ie, this.color = new Xi, this.destroy = new de, this.gradient = [], this.groups = {}, this.life = new ae, this.links = new zi, this.move = new Hi, this.number = new Fi, this.opacity = new Bi, this.orbit = new be, this.reduceDuplicates = !1, this.repulse = new ge, this.roll = new fe, this.rotate = new Ni, this.shadow = new $i, this.shape = new Wi, this.size = new ji, this.stroke = new Yi, this.tilt = new pe, this.twinkle = new oe, this.wobble = new he, this.zIndex = new ye
            }

            get line_linked() {
                return this.links
            }

            set line_linked(t) {
                this.links = t
            }

            get lineLinked() {
                return this.links
            }

            set lineLinked(t) {
                this.links = t
            }

            load(t) {
                var i, e, o, s, n, a, r, l;
                if (void 0 === t) return;
                this.bounce.load(t.bounce), this.color.load(Xi.create(this.color, t.color)), this.destroy.load(t.destroy), this.life.load(t.life);
                const c = null !== (e = null !== (i = t.links) && void 0 !== i ? i : t.lineLinked) && void 0 !== e ? e : t.line_linked;
                if (void 0 !== c && this.links.load(c), void 0 !== t.groups) for (const i in t.groups) {
                    const e = t.groups[i];
                    void 0 !== e && (this.groups[i] = rt(null !== (o = this.groups[i]) && void 0 !== o ? o : {}, e))
                }
                this.move.load(t.move), this.number.load(t.number), this.opacity.load(t.opacity), this.orbit.load(t.orbit), void 0 !== t.reduceDuplicates && (this.reduceDuplicates = t.reduceDuplicates), this.repulse.load(t.repulse), this.roll.load(t.roll), this.rotate.load(t.rotate), this.shape.load(t.shape), this.size.load(t.size), this.shadow.load(t.shadow), this.tilt.load(t.tilt), this.twinkle.load(t.twinkle), this.wobble.load(t.wobble), this.zIndex.load(t.zIndex);
                const d = null !== (n = null === (s = t.move) || void 0 === s ? void 0 : s.collisions) && void 0 !== n ? n : null === (a = t.move) || void 0 === a ? void 0 : a.bounce;
                void 0 !== d && (this.collisions.enable = d), this.collisions.load(t.collisions);
                const h = null !== (r = t.stroke) && void 0 !== r ? r : null === (l = t.shape) || void 0 === l ? void 0 : l.stroke;
                h && (h instanceof Array ? this.stroke = h.map((t => {
                    const i = new Yi;
                    return i.load(t), i
                })) : (this.stroke instanceof Array && (this.stroke = new Yi), this.stroke.load(h)));
                const u = t.gradient;
                u && (u instanceof Array ? this.gradient = u.map((t => {
                    const i = new we;
                    return i.load(t), i
                })) : (this.gradient instanceof Array && (this.gradient = new we), this.gradient.load(u)))
            }
        }

        class Oe extends I {
            constructor(t, i, e) {
                super(t, i), this.z = void 0 === e ? t.z : e
            }

            static clone(t) {
                return Oe.create(t.x, t.y, t.z)
            }

            static create(t, i, e) {
                return new Oe(t, i, e)
            }

            add(t) {
                return t instanceof Oe ? Oe.create(this.x + t.x, this.y + t.y, this.z + t.z) : super.add(t)
            }

            addTo(t) {
                super.addTo(t), t instanceof Oe && (this.z += t.z)
            }

            sub(t) {
                return t instanceof Oe ? Oe.create(this.x - t.x, this.y - t.y, this.z - t.z) : super.sub(t)
            }

            subFrom(t) {
                super.subFrom(t), t instanceof Oe && (this.z -= t.z)
            }

            mult(t) {
                return Oe.create(this.x * t, this.y * t, this.z * t)
            }

            multTo(t) {
                super.multTo(t), this.z *= t
            }

            div(t) {
                return Oe.create(this.x / t, this.y / t, this.z / t)
            }

            divTo(t) {
                super.divTo(t), this.z /= t
            }

            copy() {
                return Oe.clone(this)
            }

            setTo(t) {
                super.setTo(t), t instanceof Oe && (this.z = t.z)
            }
        }

        const Se = t => {
            (tt(t.outMode, t.checkModes) || tt(t.outMode, t.checkModes)) && (t.coord > t.maxCoord - 2 * t.radius ? t.setCb(-t.radius) : t.coord < 2 * t.radius && t.setCb(t.radius))
        };

        class Te {
            constructor(t, i, e, o, s) {
                var n, a, r, l, c, h, u, p, v;
                this.id = t, this.container = i, this.group = s, this.fill = !0, this.close = !0, this.lastPathTime = 0, this.destroyed = !1, this.unbreakable = !1, this.splitCount = 0, this.misplaced = !1, this.retina = {maxDistance: {}};
                const f = i.retina.pixelRatio, y = i.actualOptions, m = new Ce;
                m.load(y.particles);
                const b = m.shape.type, g = m.reduceDuplicates;
                if (this.shape = b instanceof Array ? ot(b, this.id, g) : b, null == o ? void 0 : o.shape) {
                    if (o.shape.type) {
                        const t = o.shape.type;
                        this.shape = t instanceof Array ? ot(t, this.id, g) : t
                    }
                    const t = new Wi;
                    t.load(o.shape), this.shape && (this.shapeData = this.loadShapeData(t, g))
                } else this.shapeData = this.loadShapeData(m.shape, g);
                void 0 !== o && m.load(o), void 0 !== (null === (n = this.shapeData) || void 0 === n ? void 0 : n.particles) && m.load(null === (a = this.shapeData) || void 0 === a ? void 0 : a.particles), this.fill = null !== (l = null === (r = this.shapeData) || void 0 === r ? void 0 : r.fill) && void 0 !== l ? l : this.fill, this.close = null !== (h = null === (c = this.shapeData) || void 0 === c ? void 0 : c.close) && void 0 !== h ? h : this.close, this.options = m, this.pathDelay = 1e3 * W(this.options.move.path.delay);
                const w = F(this.options.zIndex.value);
                i.retina.initParticle(this);
                const x = this.options.size, k = x.value;
                this.size = {
                    enable: x.animation.enable,
                    value: W(x) * i.retina.pixelRatio,
                    max: V(k) * f,
                    min: _(k) * f,
                    loops: 0,
                    maxLoops: x.animation.count
                };
                const M = x.animation;
                if (M.enable) {
                    switch (this.size.status = P.increasing, M.startValue) {
                        case T.min:
                            this.size.value = this.size.min, this.size.status = P.increasing;
                            break;
                        case T.random:
                            this.size.value = q(this.size) * f, this.size.status = Math.random() >= .5 ? P.increasing : P.decreasing;
                            break;
                        case T.max:
                        default:
                            this.size.value = this.size.max, this.size.status = P.decreasing
                    }
                    this.size.velocity = (null !== (u = this.retina.sizeAnimationSpeed) && void 0 !== u ? u : i.retina.sizeAnimationSpeed) / 100 * i.retina.reduceFactor, M.sync || (this.size.velocity *= Math.random())
                }
                this.direction = U(this.options.move.direction), this.bubble = {inRange: !1}, this.initialVelocity = this.calculateVelocity(), this.velocity = this.initialVelocity.copy(), this.moveDecay = 1 - F(this.options.move.decay), this.position = this.calcPosition(i, e, L(w, 0, i.zLayers)), this.initialPosition = this.position.copy(), this.offset = I.origin;
                const z = i.particles;
                z.needsSort = z.needsSort || z.lastZIndex < this.position.z, z.lastZIndex = this.position.z, this.zIndexFactor = this.position.z / i.zLayers, this.sides = 24;
                let C = i.drawers.get(this.shape);
                C || (C = bi.getShapeDrawer(this.shape), C && i.drawers.set(this.shape, C)), (null == C ? void 0 : C.loadShape) && (null == C || C.loadShape(this));
                const O = null == C ? void 0 : C.getSidesCount;
                if (O && (this.sides = O(this)), this.life = this.loadLife(), this.spawning = this.life.delay > 0, this.options.move.spin.enable) {
                    const t = null !== (p = this.options.move.spin.position) && void 0 !== p ? p : {x: 50, y: 50},
                        e = {x: t.x / 100 * i.canvas.size.width, y: t.y / 100 * i.canvas.size.height},
                        o = j(this.getPosition(), e);
                    this.spin = {
                        center: e,
                        direction: this.velocity.x >= 0 ? d.clockwise : d.counterClockwise,
                        angle: this.velocity.angle,
                        radius: o,
                        acceleration: null !== (v = this.retina.spinAcceleration) && void 0 !== v ? v : F(this.options.move.spin.acceleration)
                    }
                }
                this.shadowColor = bt(this.options.shadow.color);
                for (const t of i.particles.updaters) t.init && t.init(this);
                C && C.particleInit && C.particleInit(i, this);
                for (const [, t] of i.plugins) t.particleCreated && t.particleCreated(this)
            }

            isVisible() {
                return !this.destroyed && !this.spawning && this.isInsideCanvas()
            }

            isInsideCanvas() {
                const t = this.getRadius(), i = this.container.canvas.size;
                return this.position.x >= -t && this.position.y >= -t && this.position.y <= i.height + t && this.position.x <= i.width + t
            }

            draw(t) {
                const i = this.container;
                for (const [, e] of i.plugins) i.canvas.drawParticlePlugin(e, this, t);
                i.canvas.drawParticle(this, t)
            }

            getPosition() {
                return {x: this.position.x + this.offset.x, y: this.position.y + this.offset.y, z: this.position.z}
            }

            getRadius() {
                var t;
                return null !== (t = this.bubble.radius) && void 0 !== t ? t : this.size.value
            }

            getMass() {
                return this.getRadius() ** 2 * Math.PI / 2
            }

            getFillColor() {
                var t, i, e;
                const o = null !== (t = this.bubble.color) && void 0 !== t ? t : Vt(this.color);
                if (o && this.roll && (this.backColor || this.roll.alter)) {
                    if (Math.floor((null !== (e = null === (i = this.roll) || void 0 === i ? void 0 : i.angle) && void 0 !== e ? e : 0) / (Math.PI / 2)) % 2) {
                        if (this.backColor) return this.backColor;
                        if (this.roll.alter) return oi(o, this.roll.alter.type, this.roll.alter.value)
                    }
                }
                return o
            }

            getStrokeColor() {
                var t, i;
                return null !== (i = null !== (t = this.bubble.color) && void 0 !== t ? t : Vt(this.strokeColor)) && void 0 !== i ? i : this.getFillColor()
            }

            destroy(t) {
                if (this.destroyed = !0, this.bubble.inRange = !1, this.unbreakable) return;
                this.destroyed = !0, this.bubble.inRange = !1;
                for (const [, i] of this.container.plugins) i.particleDestroyed && i.particleDestroyed(this, t);
                if (t) return;
                this.options.destroy.mode === v.split && this.split()
            }

            reset() {
                this.opacity && (this.opacity.loops = 0), this.size.loops = 0
            }

            split() {
                const t = this.options.destroy.split;
                if (t.count >= 0 && this.splitCount++ > t.count) return;
                const i = F(t.rate.value);
                for (let t = 0; t < i; t++) this.container.particles.addSplitParticle(this)
            }

            calcPosition(t, i, e, o = 0) {
                var s, n, a, r, l, c;
                for (const [, o] of t.plugins) {
                    const t = void 0 !== o.particlePosition ? o.particlePosition(i, this) : void 0;
                    if (void 0 !== t) return Oe.create(t.x, t.y, e)
                }
                const d = t.canvas.size,
                    h = Oe.create(null !== (s = null == i ? void 0 : i.x) && void 0 !== s ? s : Math.random() * d.width, null !== (n = null == i ? void 0 : i.y) && void 0 !== n ? n : Math.random() * d.height, e),
                    u = this.getRadius(), p = this.options.move.outModes, v = i => {
                        Se({
                            outMode: i,
                            checkModes: [b.bounce, b.bounceHorizontal],
                            coord: h.x,
                            maxCoord: t.canvas.size.width,
                            setCb: t => h.x += t,
                            radius: u
                        })
                    }, f = i => {
                        Se({
                            outMode: i,
                            checkModes: [b.bounce, b.bounceVertical],
                            coord: h.y,
                            maxCoord: t.canvas.size.height,
                            setCb: t => h.y += t,
                            radius: u
                        })
                    };
                return v(null !== (a = p.left) && void 0 !== a ? a : p.default), v(null !== (r = p.right) && void 0 !== r ? r : p.default), f(null !== (l = p.top) && void 0 !== l ? l : p.default), f(null !== (c = p.bottom) && void 0 !== c ? c : p.default), this.checkOverlap(h, o) ? this.calcPosition(t, void 0, e, o + 1) : h
            }

            checkOverlap(t, i = 0) {
                const e = this.options.collisions, o = this.getRadius();
                if (!e.enable) return !1;
                const s = e.overlap;
                if (s.enable) return !1;
                const n = s.retries;
                if (n >= 0 && i > n) throw new Error("Particle is overlapping and can't be placed");
                let a = !1;
                for (const i of this.container.particles.array) if (j(t, i.position) < o + i.getRadius()) {
                    a = !0;
                    break
                }
                return a
            }

            calculateVelocity() {
                const t = N(this.direction).copy(), i = this.options.move, e = Math.PI / 180 * i.angle.value,
                    o = Math.PI / 180 * i.angle.offset, s = {left: o - e / 2, right: o + e / 2};
                return i.straight || (t.angle += q(B(s.left, s.right))), i.random && "number" == typeof i.speed && (t.length *= Math.random()), t
            }

            loadShapeData(t, i) {
                const e = t.options[this.shape];
                if (e) return rt({}, e instanceof Array ? ot(e, this.id, i) : e)
            }

            loadLife() {
                const t = this.container, i = this.options, e = i.life, o = {
                    delay: t.retina.reduceFactor ? F(e.delay.value) * (e.delay.sync ? 1 : Math.random()) / t.retina.reduceFactor * 1e3 : 0,
                    delayTime: 0,
                    duration: t.retina.reduceFactor ? F(e.duration.value) * (e.duration.sync ? 1 : Math.random()) / t.retina.reduceFactor * 1e3 : 0,
                    time: 0,
                    count: i.life.count
                };
                return o.duration <= 0 && (o.duration = -1), o.count <= 0 && (o.count = -1), o
            }
        }

        class Ee {
            constructor(t) {
                this.container = t, this.externalInteractors = [], this.particleInteractors = [], this.init()
            }

            init() {
                const t = bi.getInteractors(this.container, !0);
                for (const i of t) switch (i.type) {
                    case O.External:
                        this.externalInteractors.push(i);
                        break;
                    case O.Particles:
                        this.particleInteractors.push(i)
                }
            }

            externalInteract(t) {
                for (const i of this.externalInteractors) i.isEnabled() && i.interact(t)
            }

            particlesInteract(t, i) {
                for (const i of this.externalInteractors) i.reset(t);
                for (const e of this.particleInteractors) e.isEnabled(t) && e.interact(t, i)
            }
        }

        class Re {
            constructor(t) {
                this.container = t
            }

            move(t, i) {
                t.destroyed || (this.moveParticle(t, i), this.moveParallax(t))
            }

            moveParticle(t, i) {
                var e, o, s, n, a;
                const r = t.options, l = r.move;
                if (!l.enable) return;
                const c = this.container, d = this.getProximitySpeedFactor(t),
                    h = (null !== (e = (n = t.retina).moveSpeed) && void 0 !== e ? e : n.moveSpeed = F(l.speed) * c.retina.pixelRatio) * c.retina.reduceFactor,
                    u = null !== (o = (a = t.retina).moveDrift) && void 0 !== o ? o : a.moveDrift = F(t.options.move.drift) * c.retina.pixelRatio,
                    p = V(r.size.value) * c.retina.pixelRatio,
                    v = h * ((l.size ? t.getRadius() / p : 1) * d * (i.factor || 1) / 2);
                this.applyPath(t, i);
                const f = l.gravity, y = f.enable && f.inverse ? -1 : 1;
                f.enable && v && (t.velocity.y += y * (f.acceleration * i.factor) / (60 * v)), u && v && (t.velocity.x += u * i.factor / (60 * v));
                const m = t.moveDecay;
                1 != m && t.velocity.multTo(m);
                const b = t.velocity.mult(v),
                    g = null !== (s = t.retina.maxSpeed) && void 0 !== s ? s : c.retina.maxSpeed;
                f.enable && f.maxSpeed > 0 && (!f.inverse && b.y >= 0 && b.y >= g || f.inverse && b.y <= 0 && b.y <= -g) && (b.y = y * g, v && (t.velocity.y = b.y / v));
                const w = t.options.zIndex, x = (1 - t.zIndexFactor) ** w.velocityRate;
                l.spin.enable ? this.spin(t, v) : (1 != x && b.multTo(x), t.position.addTo(b), l.vibrate && (t.position.x += Math.sin(t.position.x * Math.cos(t.position.y)), t.position.y += Math.cos(t.position.y * Math.sin(t.position.x)))), function (t) {
                    const i = t.initialPosition, {dx: e, dy: o} = G(i, t.position), s = Math.abs(e), n = Math.abs(o),
                        a = t.retina.maxDistance.horizontal, r = t.retina.maxDistance.vertical;
                    if (a || r) if ((a && s >= a || r && n >= r) && !t.misplaced) t.misplaced = !!a && s > a || !!r && n > r, a && (t.velocity.x = t.velocity.y / 2 - t.velocity.x), r && (t.velocity.y = t.velocity.x / 2 - t.velocity.y); else if ((!a || s < a) && (!r || n < r) && t.misplaced) t.misplaced = !1; else if (t.misplaced) {
                        const e = t.position, o = t.velocity;
                        a && (e.x < i.x && o.x < 0 || e.x > i.x && o.x > 0) && (o.x *= -Math.random()), r && (e.y < i.y && o.y < 0 || e.y > i.y && o.y > 0) && (o.y *= -Math.random())
                    }
                }(t)
            }

            spin(t, i) {
                const e = this.container;
                if (!t.spin) return;
                const o = {
                    x: t.spin.direction === d.clockwise ? Math.cos : Math.sin,
                    y: t.spin.direction === d.clockwise ? Math.sin : Math.cos
                };
                t.position.x = t.spin.center.x + t.spin.radius * o.x(t.spin.angle), t.position.y = t.spin.center.y + t.spin.radius * o.y(t.spin.angle), t.spin.radius += t.spin.acceleration;
                const s = Math.max(e.canvas.size.width, e.canvas.size.height);
                t.spin.radius > s / 2 ? (t.spin.radius = s / 2, t.spin.acceleration *= -1) : t.spin.radius < 0 && (t.spin.radius = 0, t.spin.acceleration *= -1), t.spin.angle += i / 100 * (1 - t.spin.radius / s)
            }

            applyPath(t, i) {
                const e = t.options.move.path;
                if (!e.enable) return;
                const o = this.container;
                if (t.lastPathTime <= t.pathDelay) return void (t.lastPathTime += i.value);
                const s = o.pathGenerator.generate(t);
                t.velocity.addTo(s), e.clamp && (t.velocity.x = L(t.velocity.x, -1, 1), t.velocity.y = L(t.velocity.y, -1, 1)), t.lastPathTime -= t.pathDelay
            }

            moveParallax(t) {
                const i = this.container, e = i.actualOptions;
                if (Y() || !e.interactivity.events.onHover.parallax.enable) return;
                const o = e.interactivity.events.onHover.parallax.force, s = i.interactivity.mouse.position;
                if (!s) return;
                const n = i.canvas.size.width / 2, a = i.canvas.size.height / 2,
                    r = e.interactivity.events.onHover.parallax.smooth, l = t.getRadius() / o, c = (s.x - n) * l,
                    d = (s.y - a) * l;
                t.offset.x += (c - t.offset.x) / r, t.offset.y += (d - t.offset.y) / r
            }

            getProximitySpeedFactor(t) {
                const i = this.container, e = i.actualOptions;
                if (!tt(y.slow, e.interactivity.events.onHover.mode)) return 1;
                const o = this.container.interactivity.mouse.position;
                if (!o) return 1;
                const s = j(o, t.getPosition()), n = i.retina.slowModeRadius;
                if (s > n) return 1;
                return (s / n || 0) / e.interactivity.modes.slow.factor
            }
        }

        class Ae {
            constructor(t) {
                this.container = t, this.nextId = 0, this.array = [], this.zArray = [], this.mover = new Re(t), this.limit = 0, this.needsSort = !1, this.lastZIndex = 0, this.freqs = {
                    links: new Map,
                    triangles: new Map
                }, this.interactionManager = new Ee(t);
                const i = this.container.canvas.size;
                this.linksColors = new Map, this.quadTree = new wi(new ai(-i.width / 4, -i.height / 4, 3 * i.width / 2, 3 * i.height / 2), 4), this.updaters = bi.getUpdaters(t, !0)
            }

            get count() {
                return this.array.length
            }

            init() {
                var t;
                const i = this.container, e = i.actualOptions;
                this.lastZIndex = 0, this.needsSort = !1, this.freqs.links = new Map, this.freqs.triangles = new Map;
                let o = !1;
                this.updaters = bi.getUpdaters(i, !0), this.interactionManager.init();
                for (const [, t] of i.plugins) if (void 0 !== t.particlesInitialization && (o = t.particlesInitialization()), o) break;
                if (this.addManualParticles(), !o) {
                    for (const i in e.particles.groups) {
                        const o = e.particles.groups[i];
                        for (let s = this.count, n = 0; n < (null === (t = o.number) || void 0 === t ? void 0 : t.value) && s < e.particles.number.value; s++, n++) this.addParticle(void 0, o, i)
                    }
                    for (let t = this.count; t < e.particles.number.value; t++) this.addParticle()
                }
                i.pathGenerator.init(i)
            }

            redraw() {
                this.clear(), this.init(), this.draw({value: 0, factor: 0})
            }

            removeAt(t, i = 1, e, o) {
                if (!(t >= 0 && t <= this.count)) return;
                let s = 0;
                for (let n = t; s < i && n < this.count; n++) {
                    const t = this.array[n];
                    if (!t || t.group !== e) continue;
                    t.destroy(o), this.array.splice(n--, 1);
                    const i = this.zArray.indexOf(t);
                    this.zArray.splice(i, 1), s++
                }
            }

            remove(t, i, e) {
                this.removeAt(this.array.indexOf(t), void 0, i, e)
            }

            update(t) {
                const i = this.container, e = [];
                i.pathGenerator.update();
                for (const [, e] of i.plugins) void 0 !== e.update && e.update(t);
                for (const o of this.array) {
                    const s = i.canvas.resizeFactor;
                    s && (o.position.x *= s.width, o.position.y *= s.height), o.bubble.inRange = !1;
                    for (const [, i] of this.container.plugins) {
                        if (o.destroyed) break;
                        i.particleUpdate && i.particleUpdate(o, t)
                    }
                    this.mover.move(o, t), o.destroyed ? e.push(o) : this.quadTree.insert(new gi(o.getPosition(), o))
                }
                for (const t of e) this.remove(t);
                this.interactionManager.externalInteract(t);
                for (const e of i.particles.array) {
                    for (const i of this.updaters) i.update(e, t);
                    e.destroyed || e.spawning || this.interactionManager.particlesInteract(e, t)
                }
                delete i.canvas.resizeFactor
            }

            draw(t) {
                const i = this.container;
                i.canvas.clear();
                const e = this.container.canvas.size;
                this.quadTree = new wi(new ai(-e.width / 4, -e.height / 4, 3 * e.width / 2, 3 * e.height / 2), 4), this.update(t), this.needsSort && (this.zArray.sort(((t, i) => i.position.z - t.position.z || t.id - i.id)), this.lastZIndex = this.zArray[this.zArray.length - 1].position.z, this.needsSort = !1);
                for (const [, e] of i.plugins) i.canvas.drawPlugin(e, t);
                for (const i of this.zArray) i.draw(t)
            }

            clear() {
                this.array = [], this.zArray = []
            }

            push(t, i, e, o) {
                this.pushing = !0;
                for (let s = 0; s < t; s++) this.addParticle(null == i ? void 0 : i.position, e, o);
                this.pushing = !1
            }

            addParticle(t, i, e) {
                const o = this.container, s = o.actualOptions.particles.number.limit * o.density;
                if (s > 0) {
                    const t = this.count + 1 - s;
                    t > 0 && this.removeQuantity(t)
                }
                return this.pushParticle(t, i, e)
            }

            addSplitParticle(t) {
                const i = t.options.destroy.split, e = new Ce;
                e.load(t.options);
                const o = F(i.factor.value);
                e.color.load({value: {hsl: t.getFillColor()}}), "number" == typeof e.size.value ? e.size.value /= o : (e.size.value.min /= o, e.size.value.max /= o), e.load(i.particles);
                const s = i.sizeOffset ? B(-t.size.value, t.size.value) : 0,
                    n = {x: t.position.x + q(s), y: t.position.y + q(s)};
                return this.pushParticle(n, e, t.group, (i => !(i.size.value < .5) && (i.velocity.length = q(B(t.velocity.length, i.velocity.length)), i.splitCount = t.splitCount + 1, i.unbreakable = !0, setTimeout((() => {
                    i.unbreakable = !1
                }), 500), !0)))
            }

            removeQuantity(t, i) {
                this.removeAt(0, t, i)
            }

            getLinkFrequency(t, i) {
                const e = `${Math.min(t.id, i.id)}_${Math.max(t.id, i.id)}`;
                let o = this.freqs.links.get(e);
                return void 0 === o && (o = Math.random(), this.freqs.links.set(e, o)), o
            }

            getTriangleFrequency(t, i, e) {
                let [o, s, n] = [t.id, i.id, e.id];
                o > s && ([s, o] = [o, s]), s > n && ([n, s] = [s, n]), o > n && ([n, o] = [o, n]);
                const a = `${o}_${s}_${n}`;
                let r = this.freqs.triangles.get(a);
                return void 0 === r && (r = Math.random(), this.freqs.triangles.set(a, r)), r
            }

            addManualParticles() {
                const t = this.container, i = t.actualOptions;
                for (const e of i.manualParticles) {
                    const i = e.position ? {
                        x: e.position.x * t.canvas.size.width / 100,
                        y: e.position.y * t.canvas.size.height / 100
                    } : void 0;
                    this.addParticle(i, e.options)
                }
            }

            setDensity() {
                const t = this.container.actualOptions;
                for (const i in t.particles.groups) this.applyDensity(t.particles.groups[i], 0, i);
                this.applyDensity(t.particles, t.manualParticles.length)
            }

            applyDensity(t, i, e) {
                var o;
                if (!(null === (o = t.number.density) || void 0 === o ? void 0 : o.enable)) return;
                const s = t.number, n = this.initDensityFactor(s.density), a = s.value, r = s.limit > 0 ? s.limit : a,
                    l = Math.min(a, r) * n + i,
                    c = Math.min(this.count, this.array.filter((t => t.group === e)).length);
                this.limit = s.limit * n, c < l ? this.push(Math.abs(l - c), void 0, t, e) : c > l && this.removeQuantity(c - l, e)
            }

            initDensityFactor(t) {
                const i = this.container;
                if (!i.canvas.element || !t.enable) return 1;
                const e = i.canvas.element, o = i.retina.pixelRatio;
                return e.width * e.height / (t.factor * o ** 2 * t.area)
            }

            pushParticle(t, i, e, o) {
                try {
                    const s = new Te(this.nextId, this.container, t, i, e);
                    let n = !0;
                    if (o && (n = o(s)), !n) return;
                    return this.array.push(s), this.zArray.push(s), this.nextId++, s
                } catch (t) {
                    return void console.warn(`error adding particle: ${t}`)
                }
            }
        }

        class De {
            constructor(t) {
                this.container = t
            }

            init() {
                const t = this.container, i = t.actualOptions;
                this.pixelRatio = !i.detectRetina || Y() ? 1 : window.devicePixelRatio;
                const e = this.container.actualOptions.motion;
                if (e && (e.disable || e.reduce.value)) if (Y() || "undefined" == typeof matchMedia || !matchMedia) this.reduceFactor = 1; else {
                    const i = matchMedia("(prefers-reduced-motion: reduce)");
                    if (i) {
                        this.handleMotionChange(i);
                        const e = () => {
                            this.handleMotionChange(i), t.refresh().catch((() => {
                            }))
                        };
                        void 0 !== i.addEventListener ? i.addEventListener("change", e) : void 0 !== i.addListener && i.addListener(e)
                    }
                } else this.reduceFactor = 1;
                const o = this.pixelRatio;
                if (t.canvas.element) {
                    const i = t.canvas.element;
                    t.canvas.size.width = i.offsetWidth * o, t.canvas.size.height = i.offsetHeight * o
                }
                const s = i.particles;
                this.attractDistance = s.move.attract.distance * o, this.linksDistance = s.links.distance * o, this.linksWidth = s.links.width * o, this.sizeAnimationSpeed = s.size.animation.speed * o, this.maxSpeed = s.move.gravity.maxSpeed * o, void 0 !== s.orbit.radius && (this.orbitRadius = s.orbit.radius * this.container.retina.pixelRatio);
                const n = i.interactivity.modes;
                this.connectModeDistance = n.connect.distance * o, this.connectModeRadius = n.connect.radius * o, this.grabModeDistance = n.grab.distance * o, this.repulseModeDistance = n.repulse.distance * o, this.bounceModeDistance = n.bounce.distance * o, this.attractModeDistance = n.attract.distance * o, this.slowModeRadius = n.slow.radius * o, this.bubbleModeDistance = n.bubble.distance * o, n.bubble.size && (this.bubbleModeSize = n.bubble.size * o)
            }

            initParticle(t) {
                const i = t.options, e = this.pixelRatio, o = i.move.distance, s = t.retina;
                s.attractDistance = i.move.attract.distance * e, s.linksDistance = i.links.distance * e, s.linksWidth = i.links.width * e, s.moveDrift = F(i.move.drift) * e, s.moveSpeed = F(i.move.speed) * e, s.sizeAnimationSpeed = i.size.animation.speed * e, t.spin && (s.spinAcceleration = F(i.move.spin.acceleration) * e);
                const n = s.maxDistance;
                n.horizontal = void 0 !== o.horizontal ? o.horizontal * e : void 0, n.vertical = void 0 !== o.vertical ? o.vertical * e : void 0, s.maxSpeed = i.move.gravity.maxSpeed * e
            }

            handleMotionChange(t) {
                const i = this.container.actualOptions;
                if (t.matches) {
                    const t = i.motion;
                    this.reduceFactor = t.disable ? 0 : t.reduce.value ? 1 / t.reduce.factor : 1
                } else this.reduceFactor = 1
            }
        }

        class Ie {
            constructor(t) {
                this.container = t
            }

            nextFrame(t) {
                var i;
                try {
                    const e = this.container;
                    if (void 0 !== e.lastFrameTime && t < e.lastFrameTime + 1e3 / e.fpsLimit) return void e.draw(!1);
                    null !== (i = e.lastFrameTime) && void 0 !== i || (e.lastFrameTime = t);
                    const o = t - e.lastFrameTime, s = {value: o, factor: 60 * o / 1e3};
                    if (e.lifeTime += s.value, e.lastFrameTime = t, o > 1e3) return void e.draw(!1);
                    if (e.particles.draw(s), e.duration > 0 && e.lifeTime > e.duration) return void e.destroy();
                    e.getAnimationStatus() && e.draw(!1)
                } catch (t) {
                    console.error("tsParticles error in animation loop", t)
                }
            }
        }

        class Le {
            constructor() {
                this.enable = !1, this.mode = []
            }

            load(t) {
                void 0 !== t && (void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.mode && (this.mode = t.mode))
            }
        }

        class He {
            constructor() {
                this.selectors = [], this.enable = !1, this.mode = [], this.type = E.circle
            }

            get elementId() {
                return this.ids
            }

            set elementId(t) {
                this.ids = t
            }

            get el() {
                return this.elementId
            }

            set el(t) {
                this.elementId = t
            }

            get ids() {
                return this.selectors instanceof Array ? this.selectors.map((t => t.replace("#", ""))) : this.selectors.replace("#", "")
            }

            set ids(t) {
                this.selectors = t instanceof Array ? t.map((t => `#${t}`)) : `#${t}`
            }

            load(t) {
                var i, e;
                if (void 0 === t) return;
                const o = null !== (e = null !== (i = t.ids) && void 0 !== i ? i : t.elementId) && void 0 !== e ? e : t.el;
                void 0 !== o && (this.ids = o), void 0 !== t.selectors && (this.selectors = t.selectors), void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.mode && (this.mode = t.mode), void 0 !== t.type && (this.type = t.type)
            }
        }

        class qe {
            constructor() {
                this.enable = !1, this.force = 2, this.smooth = 10
            }

            load(t) {
                void 0 !== t && (void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.force && (this.force = t.force), void 0 !== t.smooth && (this.smooth = t.smooth))
            }
        }

        class Fe {
            constructor() {
                this.enable = !1, this.mode = [], this.parallax = new qe
            }

            load(t) {
                void 0 !== t && (void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.mode && (this.mode = t.mode), this.parallax.load(t.parallax))
            }
        }

        class _e {
            constructor() {
                this.onClick = new Le, this.onDiv = new He, this.onHover = new Fe, this.resize = !0
            }

            get onclick() {
                return this.onClick
            }

            set onclick(t) {
                this.onClick = t
            }

            get ondiv() {
                return this.onDiv
            }

            set ondiv(t) {
                this.onDiv = t
            }

            get onhover() {
                return this.onHover
            }

            set onhover(t) {
                this.onHover = t
            }

            load(t) {
                var i, e, o;
                if (void 0 === t) return;
                this.onClick.load(null !== (i = t.onClick) && void 0 !== i ? i : t.onclick);
                const s = null !== (e = t.onDiv) && void 0 !== e ? e : t.ondiv;
                void 0 !== s && (s instanceof Array ? this.onDiv = s.map((t => {
                    const i = new He;
                    return i.load(t), i
                })) : (this.onDiv = new He, this.onDiv.load(s))), this.onHover.load(null !== (o = t.onHover) && void 0 !== o ? o : t.onhover), void 0 !== t.resize && (this.resize = t.resize)
            }
        }

        class Ve {
            constructor() {
                this.distance = 200, this.duration = .4, this.mix = !1
            }

            load(t) {
                void 0 !== t && (void 0 !== t.distance && (this.distance = t.distance), void 0 !== t.duration && (this.duration = t.duration), void 0 !== t.mix && (this.mix = t.mix), void 0 !== t.opacity && (this.opacity = t.opacity), void 0 !== t.color && (t.color instanceof Array ? this.color = t.color.map((t => ki.create(void 0, t))) : (this.color instanceof Array && (this.color = new ki), this.color = ki.create(this.color, t.color))), void 0 !== t.size && (this.size = t.size))
            }
        }

        class Be extends Ve {
            constructor() {
                super(), this.selectors = []
            }

            get ids() {
                return this.selectors instanceof Array ? this.selectors.map((t => t.replace("#", ""))) : this.selectors.replace("#", "")
            }

            set ids(t) {
                this.selectors = t instanceof Array ? t.map((t => `#${t}`)) : `#${t}`
            }

            load(t) {
                super.load(t), void 0 !== t && (void 0 !== t.ids && (this.ids = t.ids), void 0 !== t.selectors && (this.selectors = t.selectors))
            }
        }

        class We extends Ve {
            load(t) {
                super.load(t), void 0 !== t && void 0 !== t.divs && (t.divs instanceof Array ? this.divs = t.divs.map((t => {
                    const i = new Be;
                    return i.load(t), i
                })) : ((this.divs instanceof Array || !this.divs) && (this.divs = new Be), this.divs.load(t.divs)))
            }
        }

        class Ge {
            constructor() {
                this.opacity = .5
            }

            load(t) {
                void 0 !== t && void 0 !== t.opacity && (this.opacity = t.opacity)
            }
        }

        class je {
            constructor() {
                this.distance = 80, this.links = new Ge, this.radius = 60
            }

            get line_linked() {
                return this.links
            }

            set line_linked(t) {
                this.links = t
            }

            get lineLinked() {
                return this.links
            }

            set lineLinked(t) {
                this.links = t
            }

            load(t) {
                var i, e;
                void 0 !== t && (void 0 !== t.distance && (this.distance = t.distance), this.links.load(null !== (e = null !== (i = t.links) && void 0 !== i ? i : t.lineLinked) && void 0 !== e ? e : t.line_linked), void 0 !== t.radius && (this.radius = t.radius))
            }
        }

        class Ue {
            constructor() {
                this.blink = !1, this.consent = !1, this.opacity = 1
            }

            load(t) {
                void 0 !== t && (void 0 !== t.blink && (this.blink = t.blink), void 0 !== t.color && (this.color = ki.create(this.color, t.color)), void 0 !== t.consent && (this.consent = t.consent), void 0 !== t.opacity && (this.opacity = t.opacity))
            }
        }

        class Ne {
            constructor() {
                this.distance = 100, this.links = new Ue
            }

            get line_linked() {
                return this.links
            }

            set line_linked(t) {
                this.links = t
            }

            get lineLinked() {
                return this.links
            }

            set lineLinked(t) {
                this.links = t
            }

            load(t) {
                var i, e;
                void 0 !== t && (void 0 !== t.distance && (this.distance = t.distance), this.links.load(null !== (e = null !== (i = t.links) && void 0 !== i ? i : t.lineLinked) && void 0 !== e ? e : t.line_linked))
            }
        }

        class $e {
            constructor() {
                this.quantity = 2
            }

            get particles_nb() {
                return this.quantity
            }

            set particles_nb(t) {
                this.quantity = t
            }

            load(t) {
                var i;
                if (void 0 === t) return;
                const e = null !== (i = t.quantity) && void 0 !== i ? i : t.particles_nb;
                void 0 !== e && (this.quantity = e)
            }
        }

        class Qe {
            constructor() {
                this.default = !0, this.groups = [], this.quantity = 4
            }

            get particles_nb() {
                return this.quantity
            }

            set particles_nb(t) {
                this.quantity = t
            }

            load(t) {
                var i;
                if (void 0 === t) return;
                void 0 !== t.default && (this.default = t.default), void 0 !== t.groups && (this.groups = t.groups.map((t => t))), this.groups.length || (this.default = !0);
                const e = null !== (i = t.quantity) && void 0 !== i ? i : t.particles_nb;
                void 0 !== e && (this.quantity = e)
            }
        }

        class Je {
            constructor() {
                this.distance = 200, this.duration = .4, this.factor = 100, this.speed = 1, this.maxSpeed = 50, this.easing = R.easeOutQuad
            }

            load(t) {
                t && (void 0 !== t.distance && (this.distance = t.distance), void 0 !== t.duration && (this.duration = t.duration), void 0 !== t.easing && (this.easing = t.easing), void 0 !== t.factor && (this.factor = t.factor), void 0 !== t.speed && (this.speed = t.speed), void 0 !== t.maxSpeed && (this.maxSpeed = t.maxSpeed))
            }
        }

        class Xe extends Je {
            constructor() {
                super(), this.selectors = []
            }

            get ids() {
                return this.selectors instanceof Array ? this.selectors.map((t => t.replace("#", ""))) : this.selectors.replace("#", "")
            }

            set ids(t) {
                this.selectors = t instanceof Array ? t.map((() => `#${t}`)) : `#${t}`
            }

            load(t) {
                super.load(t), void 0 !== t && (void 0 !== t.ids && (this.ids = t.ids), void 0 !== t.selectors && (this.selectors = t.selectors))
            }
        }

        class Ye extends Je {
            load(t) {
                super.load(t), void 0 !== (null == t ? void 0 : t.divs) && (t.divs instanceof Array ? this.divs = t.divs.map((t => {
                    const i = new Xe;
                    return i.load(t), i
                })) : ((this.divs instanceof Array || !this.divs) && (this.divs = new Xe), this.divs.load(t.divs)))
            }
        }

        class Ze {
            constructor() {
                this.factor = 3, this.radius = 200
            }

            get active() {
                return !1
            }

            set active(t) {
            }

            load(t) {
                void 0 !== t && (void 0 !== t.factor && (this.factor = t.factor), void 0 !== t.radius && (this.radius = t.radius))
            }
        }

        class Ke {
            constructor() {
                this.delay = 1, this.pauseOnStop = !1, this.quantity = 1
            }

            load(t) {
                void 0 !== t && (void 0 !== t.delay && (this.delay = t.delay), void 0 !== t.quantity && (this.quantity = t.quantity), void 0 !== t.particles && (this.particles = rt({}, t.particles)), void 0 !== t.pauseOnStop && (this.pauseOnStop = t.pauseOnStop))
            }
        }

        class to {
            constructor() {
                this.distance = 200, this.duration = .4, this.easing = R.easeOutQuad, this.factor = 1, this.maxSpeed = 50, this.speed = 1
            }

            load(t) {
                t && (void 0 !== t.distance && (this.distance = t.distance), void 0 !== t.duration && (this.duration = t.duration), void 0 !== t.easing && (this.easing = t.easing), void 0 !== t.factor && (this.factor = t.factor), void 0 !== t.maxSpeed && (this.maxSpeed = t.maxSpeed), void 0 !== t.speed && (this.speed = t.speed))
            }
        }

        class io {
            constructor() {
                this.start = new ki, this.stop = new ki, this.start.value = "#ffffff", this.stop.value = "#000000"
            }

            load(t) {
                void 0 !== t && (this.start = ki.create(this.start, t.start), this.stop = ki.create(this.stop, t.stop))
            }
        }

        class eo {
            constructor() {
                this.gradient = new io, this.radius = 1e3
            }

            load(t) {
                void 0 !== t && (this.gradient.load(t.gradient), void 0 !== t.radius && (this.radius = t.radius))
            }
        }

        class oo {
            constructor() {
                this.color = new ki, this.color.value = "#000000", this.length = 2e3
            }

            load(t) {
                void 0 !== t && (this.color = ki.create(this.color, t.color), void 0 !== t.length && (this.length = t.length))
            }
        }

        class so {
            constructor() {
                this.area = new eo, this.shadow = new oo
            }

            load(t) {
                void 0 !== t && (this.area.load(t.area), this.shadow.load(t.shadow))
            }
        }

        class no {
            constructor() {
                this.distance = 200
            }

            load(t) {
                t && void 0 !== t.distance && (this.distance = t.distance)
            }
        }

        class ao {
            constructor() {
                this.attract = new to, this.bounce = new no, this.bubble = new We, this.connect = new je, this.grab = new Ne, this.light = new so, this.push = new Qe, this.remove = new $e, this.repulse = new Ye, this.slow = new Ze, this.trail = new Ke
            }

            load(t) {
                void 0 !== t && (this.attract.load(t.attract), this.bubble.load(t.bubble), this.connect.load(t.connect), this.grab.load(t.grab), this.light.load(t.light), this.push.load(t.push), this.remove.load(t.remove), this.repulse.load(t.repulse), this.slow.load(t.slow), this.trail.load(t.trail))
            }
        }

        class ro {
            constructor() {
                this.detectsOn = D.window, this.events = new _e, this.modes = new ao
            }

            get detect_on() {
                return this.detectsOn
            }

            set detect_on(t) {
                this.detectsOn = t
            }

            load(t) {
                var i, e, o;
                if (void 0 === t) return;
                const s = null !== (i = t.detectsOn) && void 0 !== i ? i : t.detect_on;
                void 0 !== s && (this.detectsOn = s), this.events.load(t.events), this.modes.load(t.modes), !0 === (null === (o = null === (e = t.modes) || void 0 === e ? void 0 : e.slow) || void 0 === o ? void 0 : o.active) && (this.events.onHover.mode instanceof Array ? this.events.onHover.mode.indexOf(y.slow) < 0 && this.events.onHover.mode.push(y.slow) : this.events.onHover.mode !== y.slow && (this.events.onHover.mode = [this.events.onHover.mode, y.slow]))
            }
        }

        class lo {
            constructor() {
                this.color = new ki, this.opacity = 1
            }

            load(t) {
                void 0 !== t && (void 0 !== t.color && (this.color = ki.create(this.color, t.color)), void 0 !== t.opacity && (this.opacity = t.opacity))
            }
        }

        class co {
            constructor() {
                this.composite = "destination-out", this.cover = new lo, this.enable = !1
            }

            load(t) {
                if (void 0 !== t) {
                    if (void 0 !== t.composite && (this.composite = t.composite), void 0 !== t.cover) {
                        const i = t.cover, e = "string" == typeof t.cover ? {color: t.cover} : t.cover;
                        this.cover.load(void 0 !== i.color ? i : {color: e})
                    }
                    void 0 !== t.enable && (this.enable = t.enable)
                }
            }
        }

        class ho {
            constructor() {
                this.color = new ki, this.color.value = "", this.image = "", this.position = "", this.repeat = "", this.size = "", this.opacity = 1
            }

            load(t) {
                void 0 !== t && (void 0 !== t.color && (this.color = ki.create(this.color, t.color)), void 0 !== t.image && (this.image = t.image), void 0 !== t.position && (this.position = t.position), void 0 !== t.repeat && (this.repeat = t.repeat), void 0 !== t.size && (this.size = t.size), void 0 !== t.opacity && (this.opacity = t.opacity))
            }
        }

        class uo {
            constructor() {
                this.auto = !1, this.mode = x.any, this.value = !1
            }

            load(t) {
                t && (void 0 !== t.auto && (this.auto = t.auto), void 0 !== t.mode && (this.mode = t.mode), void 0 !== t.value && (this.value = t.value))
            }
        }

        class po {
            constructor() {
                this.name = "", this.default = new uo
            }

            load(t) {
                void 0 !== t && (void 0 !== t.name && (this.name = t.name), this.default.load(t.default), void 0 !== t.options && (this.options = rt({}, t.options)))
            }
        }

        class vo {
            constructor() {
                this.enable = !0, this.zIndex = 0
            }

            load(t) {
                t && (void 0 !== t.enable && (this.enable = t.enable), void 0 !== t.zIndex && (this.zIndex = t.zIndex))
            }
        }

        class fo {
            constructor() {
                this.factor = 4, this.value = !0
            }

            load(t) {
                t && (void 0 !== t.factor && (this.factor = t.factor), void 0 !== t.value && (this.value = t.value))
            }
        }

        class yo {
            constructor() {
                this.disable = !1, this.reduce = new fo
            }

            load(t) {
                t && (void 0 !== t.disable && (this.disable = t.disable), this.reduce.load(t.reduce))
            }
        }

        class mo {
            load(t) {
                var i, e;
                t && (void 0 !== t.position && (this.position = {
                    x: null !== (i = t.position.x) && void 0 !== i ? i : 50,
                    y: null !== (e = t.position.y) && void 0 !== e ? e : 50
                }), void 0 !== t.options && (this.options = rt({}, t.options)))
            }
        }

        class bo {
            constructor() {
                this.maxWidth = 1 / 0, this.options = {}, this.mode = k.canvas
            }

            load(t) {
                t && (void 0 !== t.maxWidth && (this.maxWidth = t.maxWidth), void 0 !== t.mode && (t.mode === k.screen ? this.mode = k.screen : this.mode = k.canvas), void 0 !== t.options && (this.options = rt({}, t.options)))
            }
        }

        var go, wo, xo = function (t, i, e, o) {
            if ("a" === e && !o) throw new TypeError("Private accessor was defined without a getter");
            if ("function" == typeof i ? t !== i || !o : !i.has(t)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
            return "m" === e ? o : "a" === e ? o.call(t) : o ? o.value : i.get(t)
        };

        class ko {
            constructor() {
                go.add(this), this.autoPlay = !0, this.background = new ho, this.backgroundMask = new co, this.fullScreen = new vo, this.detectRetina = !0, this.duration = 0, this.fpsLimit = 60, this.interactivity = new ro, this.manualParticles = [], this.motion = new yo, this.particles = new Ce, this.pauseOnBlur = !0, this.pauseOnOutsideViewport = !0, this.responsive = [], this.themes = [], this.zLayers = 100
            }

            get fps_limit() {
                return this.fpsLimit
            }

            set fps_limit(t) {
                this.fpsLimit = t
            }

            get retina_detect() {
                return this.detectRetina
            }

            set retina_detect(t) {
                this.detectRetina = t
            }

            get backgroundMode() {
                return this.fullScreen
            }

            set backgroundMode(t) {
                this.fullScreen.load(t)
            }

            load(t) {
                var i, e, o, s, n;
                if (void 0 === t) return;
                if (void 0 !== t.preset) if (t.preset instanceof Array) for (const i of t.preset) this.importPreset(i); else this.importPreset(t.preset);
                void 0 !== t.autoPlay && (this.autoPlay = t.autoPlay);
                const a = null !== (i = t.detectRetina) && void 0 !== i ? i : t.retina_detect;
                void 0 !== a && (this.detectRetina = a), void 0 !== t.duration && (this.duration = t.duration);
                const r = null !== (e = t.fpsLimit) && void 0 !== e ? e : t.fps_limit;
                void 0 !== r && (this.fpsLimit = r), void 0 !== t.pauseOnBlur && (this.pauseOnBlur = t.pauseOnBlur), void 0 !== t.pauseOnOutsideViewport && (this.pauseOnOutsideViewport = t.pauseOnOutsideViewport), void 0 !== t.zLayers && (this.zLayers = t.zLayers), this.background.load(t.background);
                const l = null !== (o = t.fullScreen) && void 0 !== o ? o : t.backgroundMode;
                if ("boolean" == typeof l ? this.fullScreen.enable = l : this.fullScreen.load(l), this.backgroundMask.load(t.backgroundMask), this.interactivity.load(t.interactivity), void 0 !== t.manualParticles && (this.manualParticles = t.manualParticles.map((t => {
                    const i = new mo;
                    return i.load(t), i
                }))), this.motion.load(t.motion), this.particles.load(t.particles), bi.loadOptions(this, t), void 0 !== t.responsive) for (const i of t.responsive) {
                    const t = new bo;
                    t.load(i), this.responsive.push(t)
                }
                if (this.responsive.sort(((t, i) => t.maxWidth - i.maxWidth)), void 0 !== t.themes) for (const i of t.themes) {
                    const t = new po;
                    t.load(i), this.themes.push(t)
                }
                this.defaultDarkTheme = null === (s = xo(this, go, "m", wo).call(this, x.dark)) || void 0 === s ? void 0 : s.name, this.defaultLightTheme = null === (n = xo(this, go, "m", wo).call(this, x.light)) || void 0 === n ? void 0 : n.name
            }

            setTheme(t) {
                if (t) {
                    const i = this.themes.find((i => i.name === t));
                    i && this.load(i.options)
                } else {
                    const t = "undefined" != typeof matchMedia && matchMedia("(prefers-color-scheme: dark)"),
                        i = t && t.matches, e = xo(this, go, "m", wo).call(this, i ? x.dark : x.light);
                    e && this.load(e.options)
                }
            }

            setResponsive(t, i, e) {
                this.load(e);
                const o = this.responsive.find((e => e.mode === k.screen && screen ? e.maxWidth * i > screen.availWidth : e.maxWidth * i > t));
                return this.load(null == o ? void 0 : o.options), null == o ? void 0 : o.maxWidth
            }

            importPreset(t) {
                this.load(bi.getPreset(t))
            }
        }

        go = new WeakSet, wo = function (t) {
            var i;
            return null !== (i = this.themes.find((i => i.default.value && i.default.mode === t))) && void 0 !== i ? i : this.themes.find((t => t.default.value && t.default.mode === x.any))
        };

        class Po {
            constructor(t, i, ...e) {
                this.id = t, this.fpsLimit = 60, this.duration = 0, this.lifeTime = 0, this.firstStart = !0, this.started = !1, this.destroyed = !1, this.paused = !0, this.lastFrameTime = 0, this.zLayers = 100, this.pageHidden = !1, this._sourceOptions = i, this._initialSourceOptions = i, this.retina = new De(this), this.canvas = new xi(this), this.particles = new Ae(this), this.drawer = new Ie(this), this.presets = e, this.pathGenerator = {
                    generate: () => {
                        const t = I.create(0, 0);
                        return t.length = Math.random(), t.angle = Math.random() * Math.PI * 2, t
                    }, init: () => {
                    }, update: () => {
                    }
                }, this.interactivity = {
                    mouse: {
                        clicking: !1,
                        inside: !1
                    }
                }, this.bubble = {}, this.repulse = {particles: []}, this.attract = {particles: []}, this.plugins = new Map, this.drawers = new Map, this.density = 1, this._options = new ko, this.actualOptions = new ko, this.eventListeners = new ci(this), "undefined" != typeof IntersectionObserver && IntersectionObserver && (this.intersectionObserver = new IntersectionObserver((t => this.intersectionManager(t))))
            }

            get options() {
                return this._options
            }

            get sourceOptions() {
                return this._sourceOptions
            }

            play(t) {
                const i = this.paused || t;
                if (!this.firstStart || this.actualOptions.autoPlay) {
                    if (this.paused && (this.paused = !1), i) for (const [, t] of this.plugins) t.play && t.play();
                    this.draw(i || !1)
                } else this.firstStart = !1
            }

            pause() {
                if (void 0 !== this.drawAnimationFrame && (K()(this.drawAnimationFrame), delete this.drawAnimationFrame), !this.paused) {
                    for (const [, t] of this.plugins) t.pause && t.pause();
                    this.pageHidden || (this.paused = !0)
                }
            }

            draw(t) {
                let i = t;
                this.drawAnimationFrame = Z()((t => {
                    i && (this.lastFrameTime = void 0, i = !1), this.drawer.nextFrame(t)
                }))
            }

            getAnimationStatus() {
                return !this.paused && !this.pageHidden
            }

            setNoise(t, i, e) {
                this.setPath(t, i, e)
            }

            setPath(t, i, e) {
                t && ("function" == typeof t ? (this.pathGenerator.generate = t, i && (this.pathGenerator.init = i), e && (this.pathGenerator.update = e)) : (t.generate && (this.pathGenerator.generate = t.generate), t.init && (this.pathGenerator.init = t.init), t.update && (this.pathGenerator.update = t.update)))
            }

            destroy() {
                this.stop(), this.canvas.destroy();
                for (const [, t] of this.drawers) t.destroy && t.destroy(this);
                for (const t of this.drawers.keys()) this.drawers.delete(t);
                this.destroyed = !0
            }

            exportImg(t) {
                this.exportImage(t)
            }

            exportImage(t, i, e) {
                var o;
                return null === (o = this.canvas.element) || void 0 === o ? void 0 : o.toBlob(t, null != i ? i : "image/png", e)
            }

            exportConfiguration() {
                return JSON.stringify(this.actualOptions, void 0, 2)
            }

            refresh() {
                return this.stop(), this.start()
            }

            reset() {
                return this._options = new ko, this.refresh()
            }

            stop() {
                if (this.started) {
                    this.firstStart = !0, this.started = !1, this.eventListeners.removeListeners(), this.pause(), this.particles.clear(), this.canvas.clear(), this.interactivity.element instanceof HTMLElement && this.intersectionObserver && this.intersectionObserver.observe(this.interactivity.element);
                    for (const [, t] of this.plugins) t.stop && t.stop();
                    for (const t of this.plugins.keys()) this.plugins.delete(t);
                    this.particles.linksColors = new Map, delete this.particles.grabLineColor, delete this.particles.linksColor, this._sourceOptions = this._options
                }
            }

            async loadTheme(t) {
                this.currentTheme = t, await this.refresh()
            }

            async start() {
                if (!this.started) {
                    await this.init(), this.started = !0, this.eventListeners.addListeners(), this.interactivity.element instanceof HTMLElement && this.intersectionObserver && this.intersectionObserver.observe(this.interactivity.element);
                    for (const [, t] of this.plugins) void 0 !== t.startAsync ? await t.startAsync() : void 0 !== t.start && t.start();
                    this.play()
                }
            }

            addClickHandler(t) {
                const i = this.interactivity.element;
                if (!i) return;
                const e = (i, e, o) => {
                    if (this.destroyed) return;
                    const s = this.retina.pixelRatio, n = {x: e.x * s, y: e.y * s},
                        a = this.particles.quadTree.queryCircle(n, o * s);
                    t(i, a)
                };
                let o = !1, s = !1;
                i.addEventListener("click", (t => {
                    if (this.destroyed) return;
                    const i = t, o = {x: i.offsetX || i.clientX, y: i.offsetY || i.clientY};
                    e(t, o, 1)
                })), i.addEventListener("touchstart", (() => {
                    this.destroyed || (o = !0, s = !1)
                })), i.addEventListener("touchmove", (() => {
                    this.destroyed || (s = !0)
                })), i.addEventListener("touchend", (t => {
                    var i, n, a;
                    if (!this.destroyed) {
                        if (o && !s) {
                            const o = t;
                            let s = o.touches[o.touches.length - 1];
                            if (!s && (s = o.changedTouches[o.changedTouches.length - 1], !s)) return;
                            const r = null === (i = this.canvas.element) || void 0 === i ? void 0 : i.getBoundingClientRect(),
                                l = {
                                    x: s.clientX - (null !== (n = null == r ? void 0 : r.left) && void 0 !== n ? n : 0),
                                    y: s.clientY - (null !== (a = null == r ? void 0 : r.top) && void 0 !== a ? a : 0)
                                };
                            e(t, l, Math.max(s.radiusX, s.radiusY))
                        }
                        o = !1, s = !1
                    }
                })), i.addEventListener("touchcancel", (() => {
                    this.destroyed || (o = !1, s = !1)
                }))
            }

            updateActualOptions() {
                this.actualOptions.responsive = [];
                const t = this.actualOptions.setResponsive(this.canvas.size.width, this.retina.pixelRatio, this._options);
                return this.actualOptions.setTheme(this.currentTheme), this.responsiveMaxWidth != t && (this.responsiveMaxWidth = t, !0)
            }

            async init() {
                this._options = new ko;
                for (const t of this.presets) this._options.load(bi.getPreset(t));
                const t = bi.getSupportedShapes();
                for (const i of t) {
                    const t = bi.getShapeDrawer(i);
                    t && this.drawers.set(i, t)
                }
                this._options.load(this._initialSourceOptions), this._options.load(this._sourceOptions), this.actualOptions = new ko, this.actualOptions.load(this._options), this.retina.init(), this.canvas.init(), this.updateActualOptions(), this.canvas.initBackground(), this.canvas.resize(), this.zLayers = this.actualOptions.zLayers, this.duration = F(this.actualOptions.duration), this.lifeTime = 0, this.fpsLimit = this.actualOptions.fpsLimit > 0 ? this.actualOptions.fpsLimit : 60;
                const i = bi.getAvailablePlugins(this);
                for (const [t, e] of i) this.plugins.set(t, e);
                for (const [, t] of this.drawers) t.init && await t.init(this);
                for (const [, t] of this.plugins) t.init ? t.init(this.actualOptions) : void 0 !== t.initAsync && await t.initAsync(this.actualOptions);
                const e = this.actualOptions.particles.move.path;
                if (e.generator) {
                    const t = bi.getPathGenerator(e.generator);
                    t && (t.init && (this.pathGenerator.init = t.init), t.generate && (this.pathGenerator.generate = t.generate), t.update && (this.pathGenerator.update = t.update))
                }
                this.particles.init(), this.particles.setDensity();
                for (const [, t] of this.plugins) void 0 !== t.particlesSetup && t.particlesSetup()
            }

            intersectionManager(t) {
                if (this.actualOptions.pauseOnOutsideViewport) for (const i of t) i.target === this.interactivity.element && (i.isIntersecting ? this.play() : this.pause())
            }
        }

        const Mo = [];

        class zo {
            static dom() {
                return Mo
            }

            static domItem(t) {
                const i = zo.dom(), e = i[t];
                if (e && !e.destroyed) return e;
                i.splice(t, 1)
            }

            static async loadOptions(t) {
                var i, e, o;
                const s = null !== (i = t.tagId) && void 0 !== i ? i : `tsparticles${Math.floor(1e4 * Math.random())}`, {
                    options: n,
                    index: a
                } = t;
                let r = null !== (e = t.element) && void 0 !== e ? e : document.getElementById(s);
                r || (r = document.createElement("div"), r.id = s, null === (o = document.querySelector("body")) || void 0 === o || o.append(r));
                const l = n instanceof Array ? ot(n, a) : n, c = zo.dom(), d = c.findIndex((t => t.id === s));
                if (d >= 0) {
                    const t = zo.domItem(d);
                    t && !t.destroyed && (t.destroy(), c.splice(d, 1))
                }
                let h, u;
                if ("canvas" === r.tagName.toLowerCase()) h = r, u = !1; else {
                    const t = r.getElementsByTagName("canvas");
                    t.length ? (h = t[0], h.className || (h.className = ft.canvasClass), u = !1) : (u = !0, h = document.createElement("canvas"), h.className = ft.canvasClass, h.style.width = "100%", h.style.height = "100%", r.appendChild(h))
                }
                const p = new Po(s, l);
                return d >= 0 ? c.splice(d, 0, p) : c.push(p), p.canvas.loadCanvas(h, u), await p.start(), p
            }

            static async loadRemoteOptions(t) {
                const {url: i, index: e} = t, o = i instanceof Array ? ot(i, e) : i;
                if (!o) return;
                const s = await fetch(o);
                if (!s.ok) return n = s.status, console.error(`Error tsParticles - fetch status: ${n}`), void console.error("Error tsParticles - File config not found");
                var n;
                const a = await s.json();
                return await zo.loadOptions({tagId: t.tagId, element: t.element, index: e, options: a})
            }

            static load(t, i, e) {
                const o = {index: e};
                return "string" == typeof t ? o.tagId = t : o.options = t, "number" == typeof i ? o.index = null != i ? i : o.index : o.options = null != i ? i : o.options, this.loadOptions(o)
            }

            static async set(t, i, e, o) {
                const s = {index: o};
                return "string" == typeof t ? s.tagId = t : s.element = t, i instanceof HTMLElement ? s.element = i : s.options = i, "number" == typeof e ? s.index = e : s.options = null != e ? e : s.options, this.loadOptions(s)
            }

            static async loadJSON(t, i, e) {
                let o, s;
                return "number" == typeof i || void 0 === i ? o = t : (s = t, o = i), await zo.loadRemoteOptions({
                    tagId: s,
                    url: o,
                    index: e
                })
            }

            static async setJSON(t, i, e, o) {
                let s, n, a, r;
                return t instanceof HTMLElement ? (r = t, s = i, a = e) : (n = t, r = i, s = e, a = o), await zo.loadRemoteOptions({
                    tagId: n,
                    url: s,
                    index: a,
                    element: r
                })
            }

            static setOnClickHandler(t) {
                const i = zo.dom();
                if (0 === i.length) throw new Error("Can only set click handlers after calling tsParticles.load() or tsParticles.loadJSON()");
                for (const e of i) e.addClickHandler(t)
            }
        }

        var Co, Oo = function (t, i, e, o, s) {
            if ("m" === o) throw new TypeError("Private method is not writable");
            if ("a" === o && !s) throw new TypeError("Private accessor was defined without a setter");
            if ("function" == typeof i ? t !== i || !s : !i.has(t)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
            return "a" === o ? s.call(t, e) : s ? s.value = e : i.set(t, e), e
        }, So = function (t, i, e, o) {
            if ("a" === e && !o) throw new TypeError("Private accessor was defined without a getter");
            if ("function" == typeof i ? t !== i || !o : !i.has(t)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
            return "m" === e ? o : "a" === e ? o.call(t) : o ? o.value : i.get(t)
        };

        class To {
            constructor() {
                Co.set(this, void 0), Oo(this, Co, !1, "f")
            }

            init() {
                So(this, Co, "f") || Oo(this, Co, !0, "f")
            }

            async loadFromArray(t, i, e) {
                return zo.load(t, i, e)
            }

            async load(t, i) {
                return zo.load(t, i)
            }

            async set(t, i, e) {
                return zo.set(t, i, e)
            }

            async loadJSON(t, i, e) {
                return zo.loadJSON(t, i, e)
            }

            async setJSON(t, i, e, o) {
                return zo.setJSON(t, i, e, o)
            }

            setOnClickHandler(t) {
                zo.setOnClickHandler(t)
            }

            dom() {
                return zo.dom()
            }

            domItem(t) {
                return zo.domItem(t)
            }

            async refresh() {
                for (const t of this.dom()) await t.refresh()
            }

            async addShape(t, i, e, o, s) {
                let n;
                n = "function" == typeof i ? {
                    afterEffect: o,
                    destroy: s,
                    draw: i,
                    init: e
                } : i, bi.addShapeDrawer(t, n), await this.refresh()
            }

            async addPreset(t, i, e = !1) {
                bi.addPreset(t, i, e), await this.refresh()
            }

            async addPlugin(t) {
                bi.addPlugin(t), await this.refresh()
            }

            async addPathGenerator(t, i) {
                bi.addPathGenerator(t, i), await this.refresh()
            }

            async addInteractor(t, i) {
                bi.addInteractor(t, i), await this.refresh()
            }

            async addParticleUpdater(t, i) {
                bi.addParticleUpdater(t, i), await this.refresh()
            }
        }

        Co = new WeakMap;

        class Eo {
            getSidesCount() {
                return 12
            }

            draw(t, i, e) {
                t.arc(0, 0, e, 0, 2 * Math.PI, !1)
            }
        }

        class Ro {
            constructor(t) {
                this.container = t
            }

            init() {
            }

            isEnabled(t) {
                return !t.destroyed
            }

            update(t, i) {
                if (!this.isEnabled(t)) return;
                const e = t.life;
                let o = !1;
                if (t.spawning) {
                    if (e.delayTime += i.value, !(e.delayTime >= t.life.delay)) return;
                    o = !0, t.spawning = !1, e.delayTime = 0, e.time = 0
                }
                if (-1 === e.duration) return;
                if (t.spawning) return;
                if (o ? e.time = 0 : e.time += i.value, e.time < e.duration) return;
                if (e.time = 0, t.life.count > 0 && t.life.count--, 0 === t.life.count) return void t.destroy();
                const s = this.container.canvas.size, n = B(0, s.width), a = B(0, s.width);
                t.position.x = q(n), t.position.y = q(a), t.spawning = !0, e.delayTime = 0, e.time = 0, t.reset();
                const r = t.options.life;
                e.delay = 1e3 * F(r.delay.value), e.duration = 1e3 * F(r.duration.value)
            }
        }

        class Ao {
            constructor(t) {
                this.container = t, this.type = O.External
            }
        }

        class Do extends Ao {
            constructor(t) {
                super(t)
            }

            isEnabled() {
                const t = this.container, i = t.interactivity.mouse, e = t.actualOptions.interactivity.events;
                return !(!e.onHover.enable || !i.position) && tt(y.connect, e.onHover.mode)
            }

            reset() {
            }

            interact() {
                const t = this.container;
                if (t.actualOptions.interactivity.events.onHover.enable && "mousemove" === t.interactivity.status) {
                    const i = t.interactivity.mouse.position;
                    if (!i) return;
                    const e = Math.abs(t.retina.connectModeRadius), o = t.particles.quadTree.queryCircle(i, e);
                    let s = 0;
                    for (const i of o) {
                        const e = i.getPosition();
                        for (const n of o.slice(s + 1)) {
                            const o = n.getPosition(), s = Math.abs(t.retina.connectModeDistance),
                                a = Math.abs(e.x - o.x), r = Math.abs(e.y - o.y);
                            a < s && r < s && t.canvas.drawConnectLine(i, n)
                        }
                        ++s
                    }
                }
            }
        }

        function Io(t, i) {
            var e, o, s, n, a;
            if (!t.opacity) return;
            const r = t.opacity.min, l = t.opacity.max;
            if (!t.destroyed && t.opacity.enable && ((null !== (e = t.opacity.maxLoops) && void 0 !== e ? e : 0) <= 0 || (null !== (o = t.opacity.loops) && void 0 !== o ? o : 0) < (null !== (s = t.opacity.maxLoops) && void 0 !== s ? s : 0))) {
                switch (t.opacity.status) {
                    case P.increasing:
                        t.opacity.value >= l ? (t.opacity.status = P.decreasing, t.opacity.loops || (t.opacity.loops = 0), t.opacity.loops++) : t.opacity.value += (null !== (n = t.opacity.velocity) && void 0 !== n ? n : 0) * i.factor;
                        break;
                    case P.decreasing:
                        t.opacity.value <= r ? (t.opacity.status = P.increasing, t.opacity.loops || (t.opacity.loops = 0), t.opacity.loops++) : t.opacity.value -= (null !== (a = t.opacity.velocity) && void 0 !== a ? a : 0) * i.factor
                }
                !function (t, i, e, o) {
                    switch (t.options.opacity.animation.destroy) {
                        case z.max:
                            i >= o && t.destroy();
                            break;
                        case z.min:
                            i <= e && t.destroy()
                    }
                }(t, t.opacity.value, r, l), t.destroyed || (t.opacity.value = L(t.opacity.value, r, l))
            }
        }

        class Lo {
            constructor(t) {
                this.container = t
            }

            init(t) {
                const i = t.options.opacity;
                t.opacity = {
                    enable: i.animation.enable,
                    max: V(i.value),
                    min: _(i.value),
                    value: F(i.value),
                    loops: 0,
                    maxLoops: i.animation.count
                };
                const e = i.animation;
                if (e.enable) {
                    t.opacity.status = P.increasing;
                    const o = i.value;
                    switch (t.opacity.min = _(o), t.opacity.max = V(o), e.startValue) {
                        case T.min:
                            t.opacity.value = t.opacity.min, t.opacity.status = P.increasing;
                            break;
                        case T.random:
                            t.opacity.value = q(t.opacity), t.opacity.status = Math.random() >= .5 ? P.increasing : P.decreasing;
                            break;
                        case T.max:
                        default:
                            t.opacity.value = t.opacity.max, t.opacity.status = P.decreasing
                    }
                    t.opacity.velocity = e.speed / 100 * this.container.retina.reduceFactor, e.sync || (t.opacity.velocity *= Math.random())
                }
            }

            isEnabled(t) {
                var i, e, o;
                return !t.destroyed && !t.spawning && !!t.opacity && t.opacity.enable && ((null !== (i = t.opacity.maxLoops) && void 0 !== i ? i : 0) <= 0 || (null !== (e = t.opacity.loops) && void 0 !== e ? e : 0) < (null !== (o = t.opacity.maxLoops) && void 0 !== o ? o : 0))
            }

            update(t, i) {
                this.isEnabled(t) && Io(t, i)
            }
        }

        function Ho(t) {
            return new Promise(((i, e) => {
                if (!t) return void e("Error tsParticles - No image.src");
                const o = {source: t, type: t.substr(t.length - 3)}, s = new Image;
                s.addEventListener("load", (() => {
                    o.element = s, i(o)
                })), s.addEventListener("error", (() => {
                    e(`Error tsParticles - loading image: ${t}`)
                })), s.src = t
            }))
        }

        async function qo(t) {
            if (!t) throw new Error("Error tsParticles - No image.src");
            const i = {source: t, type: t.substr(t.length - 3)};
            if ("svg" !== i.type) return Ho(t);
            const e = await fetch(i.source);
            if (!e.ok) throw new Error("Error tsParticles - Image not found");
            return i.svgData = await e.text(), i
        }

        var Fo, _o, Vo = function (t, i, e, o, s) {
            if ("m" === o) throw new TypeError("Private method is not writable");
            if ("a" === o && !s) throw new TypeError("Private accessor was defined without a setter");
            if ("function" == typeof i ? t !== i || !s : !i.has(t)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
            return "a" === o ? s.call(t, e) : s ? s.value = e : i.set(t, e), e
        }, Bo = function (t, i, e, o) {
            if ("a" === e && !o) throw new TypeError("Private accessor was defined without a getter");
            if ("function" == typeof i ? t !== i || !o : !i.has(t)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
            return "m" === e ? o : "a" === e ? o.call(t) : o ? o.value : i.get(t)
        };

        class Wo {
            constructor() {
                Fo.set(this, void 0), Vo(this, Fo, [], "f")
            }

            getSidesCount() {
                return 12
            }

            getImages(t) {
                const i = Bo(this, Fo, "f").find((i => i.id === t.id));
                return i || (Bo(this, Fo, "f").push({id: t.id, images: []}), this.getImages(t))
            }

            addImage(t, i) {
                const e = this.getImages(t);
                null == e || e.images.push(i)
            }

            async init(t) {
                await this.loadImagesFromParticlesOptions(t, t.actualOptions.particles), await this.loadImagesFromParticlesOptions(t, t.actualOptions.interactivity.modes.trail.particles);
                for (const i of t.actualOptions.manualParticles) await this.loadImagesFromParticlesOptions(t, i.options);
                const i = t.actualOptions;
                if (i.emitters) if (i.emitters instanceof Array) for (const e of i.emitters) await this.loadImagesFromParticlesOptions(t, e.particles); else await this.loadImagesFromParticlesOptions(t, i.emitters.particles);
                const e = i.interactivity.modes.emitters;
                if (e) if (e instanceof Array) for (const i of e) await this.loadImagesFromParticlesOptions(t, i.particles); else await this.loadImagesFromParticlesOptions(t, e.particles)
            }

            destroy() {
                Vo(this, Fo, [], "f")
            }

            async loadImagesFromParticlesOptions(t, i) {
                var e, o, s;
                const n = null == i ? void 0 : i.shape;
                if (!(null == n ? void 0 : n.type) || !n.options || !tt(S.image, n.type) && !tt(S.images, n.type)) return;
                const a = Bo(this, Fo, "f").findIndex((i => i.id === t.id));
                a >= 0 && Bo(this, Fo, "f").splice(a, 1);
                const r = null !== (e = n.options[S.images]) && void 0 !== e ? e : n.options[S.image];
                if (r instanceof Array) for (const i of r) await this.loadImageShape(t, i); else await this.loadImageShape(t, r);
                if (null == i ? void 0 : i.groups) for (const e in i.groups) {
                    const o = i.groups[e];
                    await this.loadImagesFromParticlesOptions(t, o)
                }
                (null === (s = null === (o = null == i ? void 0 : i.destroy) || void 0 === o ? void 0 : o.split) || void 0 === s ? void 0 : s.particles) && await this.loadImagesFromParticlesOptions(t, null == i ? void 0 : i.destroy.split.particles)
            }

            async loadImageShape(t, i) {
                try {
                    const e = i.replaceColor ? qo : Ho, o = await e(i.src);
                    o && this.addImage(t, o)
                } catch (t) {
                    console.warn(`tsParticles error - ${i.src} not found`)
                }
            }

            draw(t, i, e, o) {
                var s, n;
                if (!t) return;
                const a = i.image, r = null === (s = null == a ? void 0 : a.data) || void 0 === s ? void 0 : s.element;
                if (!r) return;
                const l = null !== (n = null == a ? void 0 : a.ratio) && void 0 !== n ? n : 1, c = {x: -e, y: -e};
                (null == a ? void 0 : a.data.svgData) && (null == a ? void 0 : a.replaceColor) || (t.globalAlpha = o), t.drawImage(r, c.x, c.y, 2 * e, 2 * e / l), (null == a ? void 0 : a.data.svgData) && (null == a ? void 0 : a.replaceColor) || (t.globalAlpha = 1)
            }

            loadShape(t) {
                var i, e, o, s, n, a, r;
                if ("image" !== t.shape && "images" !== t.shape) return;
                const l = this.getImages(t.container).images, c = t.shapeData,
                    d = null !== (i = l.find((t => t.source === c.src))) && void 0 !== i ? i : l[0],
                    h = t.getFillColor();
                let u;
                if (!d) return;
                if (void 0 !== d.svgData && c.replaceColor && h) {
                    const i = function (t, i, e) {
                            const {svgData: o} = t;
                            if (!o) return "";
                            if (o.includes("fill")) {
                                const t = /(#(?:[0-9a-f]{2}){2,4}|(#[0-9a-f]{3})|(rgb|hsl)a?\((-?\d+%?[,\s]+){2,3}\s*[\d.]+%?\))|currentcolor/gi;
                                return o.replace(t, (() => Lt(i, e)))
                            }
                            const s = o.indexOf(">");
                            return `${o.substring(0, s)} fill="${Lt(i, e)}"${o.substring(s)}`
                        }(d, h, null !== (o = null === (e = t.opacity) || void 0 === e ? void 0 : e.value) && void 0 !== o ? o : 1),
                        n = new Blob([i], {type: "image/svg+xml"}), a = URL || window.URL || window.webkitURL || window,
                        r = a.createObjectURL(n), l = new Image;
                    u = {
                        data: Object.assign(Object.assign({}, d), {svgData: i}),
                        ratio: c.width / c.height,
                        replaceColor: null !== (s = c.replaceColor) && void 0 !== s ? s : c.replace_color,
                        source: c.src
                    }, l.addEventListener("load", (() => {
                        const i = t.image;
                        i && (i.loaded = !0, d.element = l), a.revokeObjectURL(r)
                    })), l.addEventListener("error", (() => {
                        a.revokeObjectURL(r), Ho(c.src).then((i => {
                            const e = t.image;
                            e && (d.element = null == i ? void 0 : i.element, e.loaded = !0)
                        }))
                    })), l.src = r
                } else u = {
                    data: d,
                    loaded: !0,
                    ratio: c.width / c.height,
                    replaceColor: null !== (n = c.replaceColor) && void 0 !== n ? n : c.replace_color,
                    source: c.src
                };
                u.ratio || (u.ratio = 1);
                const p = {
                    image: u,
                    fill: null !== (a = c.fill) && void 0 !== a ? a : t.fill,
                    close: null !== (r = c.close) && void 0 !== r ? r : t.close
                };
                t.image = p.image, t.fill = p.fill, t.close = p.close
            }
        }

        Fo = new WeakMap;

        class Go {
            getSidesCount(t) {
                var i, e;
                const o = t.shapeData;
                return null !== (e = null !== (i = null == o ? void 0 : o.sides) && void 0 !== i ? i : null == o ? void 0 : o.nb_sides) && void 0 !== e ? e : 5
            }

            draw(t, i, e) {
                const o = this.getCenter(i, e), s = this.getSidesData(i, e),
                    n = s.count.numerator * s.count.denominator, a = s.count.numerator / s.count.denominator,
                    r = 180 * (a - 2) / a, l = Math.PI - Math.PI * r / 180;
                if (t) {
                    t.beginPath(), t.translate(o.x, o.y), t.moveTo(0, 0);
                    for (let i = 0; i < n; i++) t.lineTo(s.length, 0), t.translate(s.length, 0), t.rotate(l)
                }
            }
        }

        class jo extends Go {
            getSidesData(t, i) {
                var e, o;
                const s = t.shapeData,
                    n = null !== (o = null !== (e = null == s ? void 0 : s.sides) && void 0 !== e ? e : null == s ? void 0 : s.nb_sides) && void 0 !== o ? o : 5;
                return {count: {denominator: 1, numerator: n}, length: 2.66 * i / (n / 3)}
            }

            getCenter(t, i) {
                return {x: -i / (this.getSidesCount(t) / 3.5), y: -i / .76}
            }
        }

        class Uo extends Go {
            getSidesCount() {
                return 3
            }

            getSidesData(t, i) {
                return {count: {denominator: 2, numerator: 3}, length: 2 * i}
            }

            getCenter(t, i) {
                return {x: -i, y: i / 1.66}
            }
        }

        async function No(t) {
            await async function (t) {
                await t.addShape("polygon", new jo)
            }(t), await async function (t) {
                await t.addShape("triangle", new Uo)
            }(t)
        }

        function $o(t, i, e, o) {
            if (i >= e) {
                return L(t + (i - e) * o, t, i)
            }
            if (i < e) {
                return L(t - (e - i) * o, i, t)
            }
        }

        !function (t) {
            t.color = "color", t.opacity = "opacity", t.size = "size"
        }(_o || (_o = {}));

        class Qo extends Ao {
            constructor(t) {
                super(t)
            }

            isEnabled() {
                const t = this.container, i = t.actualOptions, e = t.interactivity.mouse, o = i.interactivity.events,
                    s = o.onDiv, n = lt(f.bubble, s);
                if (!(n || o.onHover.enable && e.position || o.onClick.enable && e.clickPosition)) return !1;
                const a = o.onHover.mode, r = o.onClick.mode;
                return tt(y.bubble, a) || tt(p.bubble, r) || n
            }

            reset(t, i) {
                t.bubble.inRange && !i || (delete t.bubble.div, delete t.bubble.opacity, delete t.bubble.radius, delete t.bubble.color)
            }

            interact() {
                const t = this.container.actualOptions.interactivity.events, i = t.onHover, e = t.onClick, o = i.enable,
                    s = i.mode, n = e.enable, a = e.mode, r = t.onDiv;
                o && tt(y.bubble, s) ? this.hoverBubble() : n && tt(p.bubble, a) ? this.clickBubble() : ct(f.bubble, r, ((t, i) => this.singleSelectorHover(t, i)))
            }

            singleSelectorHover(t, i) {
                const e = this.container, o = document.querySelectorAll(t);
                o.length && o.forEach((t => {
                    const o = t, s = e.retina.pixelRatio,
                        n = {x: (o.offsetLeft + o.offsetWidth / 2) * s, y: (o.offsetTop + o.offsetHeight / 2) * s},
                        a = o.offsetWidth / 2 * s,
                        r = i.type === E.circle ? new ni(n.x, n.y, a) : new ai(o.offsetLeft * s, o.offsetTop * s, o.offsetWidth * s, o.offsetHeight * s),
                        l = e.particles.quadTree.query(r);
                    for (const t of l) {
                        if (!r.contains(t.getPosition())) continue;
                        t.bubble.inRange = !0;
                        const i = ht(e.actualOptions.interactivity.modes.bubble.divs, o);
                        t.bubble.div && t.bubble.div === o || (this.reset(t, !0), t.bubble.div = o), this.hoverBubbleSize(t, 1, i), this.hoverBubbleOpacity(t, 1, i), this.hoverBubbleColor(t, 1, i)
                    }
                }))
            }

            process(t, i, e, o) {
                const s = this.container, n = o.bubbleObj.optValue;
                if (void 0 === n) return;
                const a = s.actualOptions.interactivity.modes.bubble.duration, r = s.retina.bubbleModeDistance,
                    l = o.particlesObj.optValue, c = o.bubbleObj.value, d = o.particlesObj.value || 0, h = o.type;
                if (n !== l) if (s.bubble.durationEnd) c && (h === _o.size && delete t.bubble.radius, h === _o.opacity && delete t.bubble.opacity); else if (i <= r) {
                    if ((null != c ? c : d) !== n) {
                        const i = d - e * (d - n) / a;
                        h === _o.size && (t.bubble.radius = i), h === _o.opacity && (t.bubble.opacity = i)
                    }
                } else h === _o.size && delete t.bubble.radius, h === _o.opacity && delete t.bubble.opacity
            }

            clickBubble() {
                var t, i;
                const e = this.container, o = e.actualOptions, s = e.interactivity.mouse.clickPosition;
                if (!s) return;
                const n = e.retina.bubbleModeDistance, a = e.particles.quadTree.queryCircle(s, n);
                for (const n of a) {
                    if (!e.bubble.clicking) continue;
                    n.bubble.inRange = !e.bubble.durationEnd;
                    const a = j(n.getPosition(), s),
                        r = ((new Date).getTime() - (e.interactivity.mouse.clickTime || 0)) / 1e3;
                    r > o.interactivity.modes.bubble.duration && (e.bubble.durationEnd = !0), r > 2 * o.interactivity.modes.bubble.duration && (e.bubble.clicking = !1, e.bubble.durationEnd = !1);
                    const l = {
                        bubbleObj: {optValue: e.retina.bubbleModeSize, value: n.bubble.radius},
                        particlesObj: {optValue: V(n.options.size.value) * e.retina.pixelRatio, value: n.size.value},
                        type: _o.size
                    };
                    this.process(n, a, r, l);
                    const c = {
                        bubbleObj: {optValue: o.interactivity.modes.bubble.opacity, value: n.bubble.opacity},
                        particlesObj: {
                            optValue: V(n.options.opacity.value),
                            value: null !== (i = null === (t = n.opacity) || void 0 === t ? void 0 : t.value) && void 0 !== i ? i : 1
                        },
                        type: _o.opacity
                    };
                    this.process(n, a, r, c), e.bubble.durationEnd ? delete n.bubble.color : a <= e.retina.bubbleModeDistance ? this.hoverBubbleColor(n, a) : delete n.bubble.color
                }
            }

            hoverBubble() {
                const t = this.container, i = t.interactivity.mouse.position;
                if (void 0 === i) return;
                const e = t.retina.bubbleModeDistance, o = t.particles.quadTree.queryCircle(i, e);
                for (const s of o) {
                    s.bubble.inRange = !0;
                    const o = j(s.getPosition(), i), n = 1 - o / e;
                    o <= e ? n >= 0 && t.interactivity.status === ft.mouseMoveEvent && (this.hoverBubbleSize(s, n), this.hoverBubbleOpacity(s, n), this.hoverBubbleColor(s, n)) : this.reset(s), t.interactivity.status === ft.mouseLeaveEvent && this.reset(s)
                }
            }

            hoverBubbleSize(t, i, e) {
                const o = this.container,
                    s = (null == e ? void 0 : e.size) ? e.size * o.retina.pixelRatio : o.retina.bubbleModeSize;
                if (void 0 === s) return;
                const n = V(t.options.size.value) * o.retina.pixelRatio, a = $o(t.size.value, s, n, i);
                void 0 !== a && (t.bubble.radius = a)
            }

            hoverBubbleOpacity(t, i, e) {
                var o, s, n;
                const a = this.container.actualOptions,
                    r = null !== (o = null == e ? void 0 : e.opacity) && void 0 !== o ? o : a.interactivity.modes.bubble.opacity;
                if (!r) return;
                const l = t.options.opacity.value,
                    c = $o(null !== (n = null === (s = t.opacity) || void 0 === s ? void 0 : s.value) && void 0 !== n ? n : 1, r, V(l), i);
                void 0 !== c && (t.bubble.opacity = c)
            }

            hoverBubbleColor(t, i, e) {
                const o = this.container.actualOptions, s = null != e ? e : o.interactivity.modes.bubble;
                if (!t.bubble.finalColor) {
                    const i = s.color;
                    if (!i) return;
                    const e = i instanceof Array ? ot(i) : i;
                    t.bubble.finalColor = gt(e)
                }
                if (t.bubble.finalColor) if (s.mix) {
                    t.bubble.color = void 0;
                    const e = t.getFillColor();
                    t.bubble.color = e ? wt(qt(e, t.bubble.finalColor, 1 - i, i)) : t.bubble.finalColor
                } else t.bubble.color = t.bubble.finalColor
            }
        }

        class Jo extends Ao {
            constructor(t) {
                super(t)
            }

            isEnabled() {
                const t = this.container, i = t.actualOptions, e = t.interactivity.mouse, o = i.interactivity.events;
                if (!(e.position && o.onHover.enable || e.clickPosition && o.onClick.enable)) return !1;
                const s = o.onHover.mode, n = o.onClick.mode;
                return tt(y.attract, s) || tt(p.attract, n)
            }

            reset() {
            }

            interact() {
                const t = this.container, i = t.actualOptions, e = t.interactivity.status === ft.mouseMoveEvent,
                    o = i.interactivity.events, s = o.onHover.enable, n = o.onHover.mode, a = o.onClick.enable,
                    r = o.onClick.mode;
                e && s && tt(y.attract, n) ? this.hoverAttract() : a && tt(p.attract, r) && this.clickAttract()
            }

            hoverAttract() {
                const t = this.container, i = t.interactivity.mouse.position;
                if (!i) return;
                const e = t.retina.attractModeDistance;
                this.processAttract(i, e, new ni(i.x, i.y, e))
            }

            processAttract(t, i, e) {
                const o = this.container, s = o.actualOptions.interactivity.modes.attract,
                    n = o.particles.quadTree.query(e);
                for (const e of n) {
                    const {dx: o, dy: n, distance: a} = G(e.position, t), r = s.speed * s.factor,
                        l = L(Q(1 - a / i, s.easing) * r, 0, s.maxSpeed),
                        c = I.create(0 === a ? r : o / a * l, 0 === a ? r : n / a * l);
                    e.position.subFrom(c)
                }
            }

            clickAttract() {
                const t = this.container;
                if (t.attract.finish || (t.attract.count || (t.attract.count = 0), t.attract.count++, t.attract.count === t.particles.count && (t.attract.finish = !0)), t.attract.clicking) {
                    const i = t.interactivity.mouse.clickPosition;
                    if (!i) return;
                    const e = t.retina.attractModeDistance;
                    this.processAttract(i, e, new ni(i.x, i.y, e))
                } else !1 === t.attract.clicking && (t.attract.particles = [])
            }
        }

        class Xo extends Ao {
            constructor(t) {
                super(t)
            }

            isEnabled() {
                const t = this.container, i = t.interactivity.mouse, e = t.actualOptions.interactivity.events;
                return e.onHover.enable && !!i.position && tt(y.grab, e.onHover.mode)
            }

            reset() {
            }

            interact() {
                var t;
                const i = this.container, e = i.actualOptions, o = e.interactivity;
                if (o.events.onHover.enable && i.interactivity.status === ft.mouseMoveEvent) {
                    const s = i.interactivity.mouse.position;
                    if (!s) return;
                    const n = i.retina.grabModeDistance, a = i.particles.quadTree.queryCircle(s, n);
                    for (const r of a) {
                        const a = j(r.getPosition(), s);
                        if (a <= n) {
                            const l = o.modes.grab.links, c = l.opacity, d = c - a * c / n;
                            if (d <= 0) continue;
                            const h = null !== (t = l.color) && void 0 !== t ? t : r.options.links.color;
                            if (!i.particles.grabLineColor) {
                                const t = e.interactivity.modes.grab.links;
                                i.particles.grabLineColor = _t(h, t.blink, t.consent)
                            }
                            const u = Ft(r, void 0, i.particles.grabLineColor);
                            if (!u) return;
                            i.canvas.drawGrabLine(r, u, d, s)
                        }
                    }
                }
            }
        }

        class Yo {
            getSidesCount(t) {
                var i, e;
                const o = t.shapeData;
                return null !== (e = null !== (i = null == o ? void 0 : o.sides) && void 0 !== i ? i : null == o ? void 0 : o.nb_sides) && void 0 !== e ? e : 5
            }

            draw(t, i, e) {
                var o;
                const s = i.shapeData, n = this.getSidesCount(i),
                    a = null !== (o = null == s ? void 0 : s.inset) && void 0 !== o ? o : 2;
                t.moveTo(0, 0 - e);
                for (let i = 0; i < n; i++) t.rotate(Math.PI / n), t.lineTo(0, 0 - e * a), t.rotate(Math.PI / n), t.lineTo(0, 0 - e)
            }
        }

        class Zo {
            constructor(t) {
                this.container = t, this.type = O.Particles
            }
        }

        class Ko extends Zo {
            constructor(t) {
                super(t)
            }

            interact(t) {
                var i;
                const e = this.container,
                    o = null !== (i = t.retina.attractDistance) && void 0 !== i ? i : e.retina.attractDistance,
                    s = t.getPosition(), n = e.particles.quadTree.queryCircle(s, o);
                for (const i of n) {
                    if (t === i || !i.options.move.attract.enable || i.destroyed || i.spawning) continue;
                    const e = i.getPosition(), {dx: o, dy: n} = G(s, e), a = t.options.move.attract.rotate,
                        r = o / (1e3 * a.x), l = n / (1e3 * a.y), c = i.size.value / t.size.value, d = 1 / c;
                    t.velocity.x -= r * c, t.velocity.y -= l * c, i.velocity.x += r * d, i.velocity.y += l * d
                }
            }

            isEnabled(t) {
                return t.options.move.attract.enable
            }

            reset() {
            }
        }

        const ts = Math.sqrt(2);

        class is {
            getSidesCount() {
                return 4
            }

            draw(t, i, e) {
                t.rect(-e / ts, -e / ts, 2 * e / ts, 2 * e / ts)
            }
        }

        function es(t, i, e, o, s) {
            var n;
            const a = i;
            if (!a || !a.enable) return;
            const r = q(e.offset), l = (null !== (n = i.velocity) && void 0 !== n ? n : 0) * t.factor + 3.6 * r;
            s && a.status !== P.increasing ? (a.value -= l, a.value < 0 && (a.status = P.increasing, a.value += a.value)) : (a.value += l, s && a.value > o && (a.status = P.decreasing, a.value -= a.value % o)), a.value > o && (a.value %= o)
        }

        class os {
            constructor(t) {
                this.container = t
            }

            init(t) {
                var i, e;
                const o = this.container;
                t.stroke = t.options.stroke instanceof Array ? ot(t.options.stroke, t.id, t.options.reduceDuplicates) : t.options.stroke, t.strokeWidth = t.stroke.width * o.retina.pixelRatio;
                const s = null !== (i = gt(t.stroke.color)) && void 0 !== i ? i : t.getFillColor();
                s && (t.strokeColor = Bt(s, null === (e = t.stroke.color) || void 0 === e ? void 0 : e.animation, o.retina.reduceFactor))
            }

            isEnabled(t) {
                var i, e, o, s;
                const n = null === (i = t.stroke) || void 0 === i ? void 0 : i.color;
                return !t.destroyed && !t.spawning && !!n && (void 0 !== (null === (e = t.strokeColor) || void 0 === e ? void 0 : e.h.value) && n.animation.h.enable || void 0 !== (null === (o = t.strokeColor) || void 0 === o ? void 0 : o.s.value) && n.animation.s.enable || void 0 !== (null === (s = t.strokeColor) || void 0 === s ? void 0 : s.l.value) && n.animation.l.enable)
            }

            update(t, i) {
                this.isEnabled(t) && function (t, i) {
                    var e, o, s, n, a, r, l, c, d, h;
                    if (!(null === (e = t.stroke) || void 0 === e ? void 0 : e.color)) return;
                    const u = t.stroke.color.animation,
                        p = null !== (s = null === (o = t.strokeColor) || void 0 === o ? void 0 : o.h) && void 0 !== s ? s : null === (n = t.color) || void 0 === n ? void 0 : n.h;
                    p && es(i, p, u.h, 360, !1);
                    const v = null !== (r = null === (a = t.strokeColor) || void 0 === a ? void 0 : a.s) && void 0 !== r ? r : null === (l = t.color) || void 0 === l ? void 0 : l.s;
                    v && es(i, v, u.s, 100, !0);
                    const f = null !== (d = null === (c = t.strokeColor) || void 0 === c ? void 0 : c.l) && void 0 !== d ? d : null === (h = t.color) || void 0 === h ? void 0 : h.l;
                    f && es(i, f, u.l, 100, !0)
                }(t, i)
            }
        }

        function ss(t, i, e, o, s) {
            var n;
            const a = i;
            if (!a || !e.enable) return;
            const r = q(e.offset), l = (null !== (n = i.velocity) && void 0 !== n ? n : 0) * t.factor + 3.6 * r;
            s && a.status !== P.increasing ? (a.value -= l, a.value < 0 && (a.status = P.increasing, a.value += a.value)) : (a.value += l, s && a.value > o && (a.status = P.decreasing, a.value -= a.value % o)), a.value > o && (a.value %= o)
        }

        class ns {
            constructor(t) {
                this.container = t
            }

            init(t) {
                const i = gt(t.options.color, t.id, t.options.reduceDuplicates);
                i && (t.color = Bt(i, t.options.color.animation, this.container.retina.reduceFactor))
            }

            isEnabled(t) {
                var i, e, o;
                const s = t.options.color.animation;
                return !t.destroyed && !t.spawning && (void 0 !== (null === (i = t.color) || void 0 === i ? void 0 : i.h.value) && s.h.enable || void 0 !== (null === (e = t.color) || void 0 === e ? void 0 : e.s.value) && s.s.enable || void 0 !== (null === (o = t.color) || void 0 === o ? void 0 : o.l.value) && s.l.enable)
            }

            update(t, i) {
                !function (t, i) {
                    var e, o, s;
                    const n = t.options.color.animation;
                    void 0 !== (null === (e = t.color) || void 0 === e ? void 0 : e.h) && ss(i, t.color.h, n.h, 360, !1), void 0 !== (null === (o = t.color) || void 0 === o ? void 0 : o.s) && ss(i, t.color.s, n.s, 100, !0), void 0 !== (null === (s = t.color) || void 0 === s ? void 0 : s.l) && ss(i, t.color.l, n.l, 100, !0)
                }(t, i)
            }
        }

        function as(t, i) {
            pt(ut(t), ut(i))
        }

        class rs extends Zo {
            constructor(t) {
                super(t)
            }

            isEnabled(t) {
                return t.options.collisions.enable
            }

            reset() {
            }

            interact(t) {
                const i = this.container, e = t.getPosition(), o = t.getRadius(),
                    s = i.particles.quadTree.queryCircle(e, 2 * o);
                for (const i of s) {
                    if (t === i || !i.options.collisions.enable || t.options.collisions.mode !== i.options.collisions.mode || i.destroyed || i.spawning) continue;
                    const s = i.getPosition();
                    if (Math.round(e.z) !== Math.round(s.z)) continue;
                    j(e, s) <= o + i.getRadius() && this.resolveCollision(t, i)
                }
            }

            resolveCollision(t, i) {
                switch (t.options.collisions.mode) {
                    case m.absorb:
                        this.absorb(t, i);
                        break;
                    case m.bounce:
                        as(t, i);
                        break;
                    case m.destroy:
                        !function (t, i) {
                            t.unbreakable || i.unbreakable || as(t, i), void 0 === t.getRadius() && void 0 !== i.getRadius() ? t.destroy() : void 0 !== t.getRadius() && void 0 === i.getRadius() ? i.destroy() : void 0 !== t.getRadius() && void 0 !== i.getRadius() && (t.getRadius() >= i.getRadius() ? i.destroy() : t.destroy())
                        }(t, i)
                }
            }

            absorb(t, i) {
                const e = this.container, o = e.fpsLimit / 1e3;
                if (void 0 === t.getRadius() && void 0 !== i.getRadius()) t.destroy(); else if (void 0 !== t.getRadius() && void 0 === i.getRadius()) i.destroy(); else if (void 0 !== t.getRadius() && void 0 !== i.getRadius()) if (t.getRadius() >= i.getRadius()) {
                    const s = L(t.getRadius() / i.getRadius(), 0, i.getRadius()) * o;
                    t.size.value += s, i.size.value -= s, i.getRadius() <= e.retina.pixelRatio && (i.size.value = 0, i.destroy())
                } else {
                    const s = L(i.getRadius() / t.getRadius(), 0, t.getRadius()) * o;
                    t.size.value -= s, i.size.value += s, t.getRadius() <= e.retina.pixelRatio && (t.size.value = 0, t.destroy())
                }
            }
        }

        class ls {
            constructor(t) {
                this.container = t
            }

            init(t) {
                const i = t.options.rotate;
                t.rotate = {enable: i.animation.enable, value: F(i.value) * Math.PI / 180};
                let e = i.direction;
                if (e === d.random) {
                    e = Math.floor(2 * Math.random()) > 0 ? d.counterClockwise : d.clockwise
                }
                switch (e) {
                    case d.counterClockwise:
                    case"counterClockwise":
                        t.rotate.status = P.decreasing;
                        break;
                    case d.clockwise:
                        t.rotate.status = P.increasing
                }
                const o = t.options.rotate.animation;
                o.enable && (t.rotate.velocity = o.speed / 360 * this.container.retina.reduceFactor, o.sync || (t.rotate.velocity *= Math.random()))
            }

            isEnabled(t) {
                const i = t.options.rotate, e = i.animation;
                return !t.destroyed && !t.spawning && !i.path && e.enable
            }

            update(t, i) {
                this.isEnabled(t) && function (t, i) {
                    var e;
                    const o = t.rotate;
                    if (!o) return;
                    const s = t.options.rotate.animation,
                        n = (null !== (e = o.velocity) && void 0 !== e ? e : 0) * i.factor, a = 2 * Math.PI;
                    if (s.enable) switch (o.status) {
                        case P.increasing:
                            o.value += n, o.value > a && (o.value -= a);
                            break;
                        case P.decreasing:
                        default:
                            o.value -= n, o.value < 0 && (o.value += a)
                    }
                }(t, i)
            }
        }

        class cs {
            constructor(t) {
                this.container = t
            }

            init() {
            }

            isEnabled(t) {
                return !t.destroyed && !t.spawning
            }

            update(t, i) {
                var e, o, s, n;
                const a = t.options.move.outModes;
                this.updateOutMode(t, i, null !== (e = a.bottom) && void 0 !== e ? e : a.default, h.bottom), this.updateOutMode(t, i, null !== (o = a.left) && void 0 !== o ? o : a.default, h.left), this.updateOutMode(t, i, null !== (s = a.right) && void 0 !== s ? s : a.default, h.right), this.updateOutMode(t, i, null !== (n = a.top) && void 0 !== n ? n : a.default, h.top)
            }

            updateOutMode(t, i, e, o) {
                switch (e) {
                    case b.bounce:
                    case b.bounceVertical:
                    case b.bounceHorizontal:
                    case"bounceVertical":
                    case"bounceHorizontal":
                    case b.split:
                        this.bounce(t, i, o, e);
                        break;
                    case b.destroy:
                        this.destroy(t, o);
                        break;
                    case b.out:
                        this.out(t, o);
                        break;
                    case b.none:
                    default:
                        this.none(t, o)
                }
            }

            destroy(t, i) {
                const e = this.container;
                st(t.position, e.canvas.size, t.getRadius(), i) || e.particles.remove(t, void 0, !0)
            }

            out(t, i) {
                const e = this.container;
                if (st(t.position, e.canvas.size, t.getRadius(), i)) return;
                const o = t.options.move.warp, s = e.canvas.size, n = {
                    bottom: s.height + t.getRadius() + t.offset.y,
                    left: -t.getRadius() - t.offset.x,
                    right: s.width + t.getRadius() + t.offset.x,
                    top: -t.getRadius() - t.offset.y
                }, a = t.getRadius(), r = at(t.position, a);
                i === h.right && r.left > s.width + t.offset.x ? (t.position.x = n.left, t.initialPosition.x = t.position.x, o || (t.position.y = Math.random() * s.height, t.initialPosition.y = t.position.y)) : i === h.left && r.right < -t.offset.x && (t.position.x = n.right, t.initialPosition.x = t.position.x, o || (t.position.y = Math.random() * s.height, t.initialPosition.y = t.position.y)), i === h.bottom && r.top > s.height + t.offset.y ? (o || (t.position.x = Math.random() * s.width, t.initialPosition.x = t.position.x), t.position.y = n.top, t.initialPosition.y = t.position.y) : i === h.top && r.bottom < -t.offset.y && (o || (t.position.x = Math.random() * s.width, t.initialPosition.x = t.position.x), t.position.y = n.bottom, t.initialPosition.y = t.position.y)
            }

            bounce(t, i, e, o) {
                const s = this.container;
                let n = !1;
                for (const [, o] of s.plugins) if (void 0 !== o.particleBounce && (n = o.particleBounce(t, i, e)), n) break;
                if (n) return;
                const a = t.getPosition(), r = t.offset, l = t.getRadius(), c = at(a, l), d = s.canvas.size;
                !function (t) {
                    if (t.outMode !== b.bounce && t.outMode !== b.bounceHorizontal && "bounceHorizontal" !== t.outMode && t.outMode !== b.split) return;
                    const i = t.particle.velocity.x;
                    let e = !1;
                    if (t.direction === h.right && t.bounds.right >= t.canvasSize.width && i > 0 || t.direction === h.left && t.bounds.left <= 0 && i < 0) {
                        const i = F(t.particle.options.bounce.horizontal.value);
                        t.particle.velocity.x *= -i, e = !0
                    }
                    if (!e) return;
                    const o = t.offset.x + t.size;
                    t.bounds.right >= t.canvasSize.width ? t.particle.position.x = t.canvasSize.width - o : t.bounds.left <= 0 && (t.particle.position.x = o), t.outMode === b.split && t.particle.destroy()
                }({particle: t, outMode: o, direction: e, bounds: c, canvasSize: d, offset: r, size: l}), function (t) {
                    if (t.outMode === b.bounce || t.outMode === b.bounceVertical || "bounceVertical" === t.outMode || t.outMode === b.split) {
                        const i = t.particle.velocity.y;
                        let e = !1;
                        if (t.direction === h.bottom && t.bounds.bottom >= t.canvasSize.height && i > 0 || t.direction === h.top && t.bounds.top <= 0 && i < 0) {
                            const i = F(t.particle.options.bounce.vertical.value);
                            t.particle.velocity.y *= -i, e = !0
                        }
                        if (!e) return;
                        const o = t.offset.y + t.size;
                        t.bounds.bottom >= t.canvasSize.height ? t.particle.position.y = t.canvasSize.height - o : t.bounds.top <= 0 && (t.particle.position.y = o), t.outMode === b.split && t.particle.destroy()
                    }
                }({particle: t, outMode: o, direction: e, bounds: c, canvasSize: d, offset: r, size: l})
            }

            none(t, i) {
                if (t.options.move.distance.horizontal && (i === h.left || i === h.right) || t.options.move.distance.vertical && (i === h.top || i === h.bottom)) return;
                const e = t.options.move.gravity, o = this.container, s = o.canvas.size, n = t.getRadius();
                if (e.enable) {
                    const a = t.position;
                    (!e.inverse && a.y > s.height + n && i === h.bottom || e.inverse && a.y < -n && i === h.top) && o.particles.remove(t)
                } else {
                    if (t.velocity.y > 0 && t.position.y <= s.height + n || t.velocity.y < 0 && t.position.y >= -n || t.velocity.x > 0 && t.position.x <= s.width + n || t.velocity.x < 0 && t.position.x >= -n) return;
                    st(t.position, o.canvas.size, n, i) || o.particles.remove(t)
                }
            }
        }

        class ds extends Ao {
            constructor(t) {
                super(t)
            }

            isEnabled() {
                const t = this.container, i = t.actualOptions, e = t.interactivity.mouse, o = i.interactivity.events,
                    s = o.onDiv, n = lt(f.repulse, s);
                if (!(n || o.onHover.enable && e.position || o.onClick.enable && e.clickPosition)) return !1;
                const a = o.onHover.mode, r = o.onClick.mode;
                return tt(y.repulse, a) || tt(p.repulse, r) || n
            }

            reset() {
            }

            interact() {
                const t = this.container, i = t.actualOptions, e = t.interactivity.status === ft.mouseMoveEvent,
                    o = i.interactivity.events, s = o.onHover.enable, n = o.onHover.mode, a = o.onClick.enable,
                    r = o.onClick.mode, l = o.onDiv;
                e && s && tt(y.repulse, n) ? this.hoverRepulse() : a && tt(p.repulse, r) ? this.clickRepulse() : ct(f.repulse, l, ((t, i) => this.singleSelectorRepulse(t, i)))
            }

            singleSelectorRepulse(t, i) {
                const e = this.container, o = document.querySelectorAll(t);
                o.length && o.forEach((t => {
                    const o = t, s = e.retina.pixelRatio,
                        n = {x: (o.offsetLeft + o.offsetWidth / 2) * s, y: (o.offsetTop + o.offsetHeight / 2) * s},
                        a = o.offsetWidth / 2 * s,
                        r = i.type === E.circle ? new ni(n.x, n.y, a) : new ai(o.offsetLeft * s, o.offsetTop * s, o.offsetWidth * s, o.offsetHeight * s),
                        l = ht(e.actualOptions.interactivity.modes.repulse.divs, o);
                    this.processRepulse(n, a, r, l)
                }))
            }

            hoverRepulse() {
                const t = this.container, i = t.interactivity.mouse.position;
                if (!i) return;
                const e = t.retina.repulseModeDistance;
                this.processRepulse(i, e, new ni(i.x, i.y, e))
            }

            processRepulse(t, i, e, o) {
                var s;
                const n = this.container, a = n.particles.quadTree.query(e),
                    r = n.actualOptions.interactivity.modes.repulse;
                for (const e of a) {
                    const {dx: n, dy: a, distance: l} = G(e.position, t),
                        c = (null !== (s = null == o ? void 0 : o.speed) && void 0 !== s ? s : r.speed) * r.factor,
                        d = L(Q(1 - l / i, r.easing) * c, 0, r.maxSpeed),
                        h = I.create(0 === l ? c : n / l * d, 0 === l ? c : a / l * d);
                    e.position.addTo(h)
                }
            }

            clickRepulse() {
                const t = this.container;
                if (t.repulse.finish || (t.repulse.count || (t.repulse.count = 0), t.repulse.count++, t.repulse.count === t.particles.count && (t.repulse.finish = !0)), t.repulse.clicking) {
                    const i = t.retina.repulseModeDistance, e = Math.pow(i / 6, 3),
                        o = t.interactivity.mouse.clickPosition;
                    if (void 0 === o) return;
                    const s = new ni(o.x, o.y, e), n = t.particles.quadTree.query(s);
                    for (const i of n) {
                        const {dx: s, dy: n, distance: a} = G(o, i.position), r = a ** 2,
                            l = t.actualOptions.interactivity.modes.repulse.speed, c = -e * l / r;
                        if (r <= e) {
                            t.repulse.particles.push(i);
                            const e = I.create(s, n);
                            e.length = c, i.velocity.setTo(e)
                        }
                    }
                } else if (!1 === t.repulse.clicking) {
                    for (const i of t.repulse.particles) i.velocity.setTo(i.initialVelocity);
                    t.repulse.particles = []
                }
            }
        }

        class hs {
            getSidesCount() {
                return 1
            }

            draw(t, i, e) {
                t.moveTo(-e / 2, 0), t.lineTo(e / 2, 0)
            }
        }

        class us extends Ao {
            constructor(t) {
                super(t)
            }

            isEnabled() {
                const t = this.container, i = t.actualOptions, e = t.interactivity.mouse, o = i.interactivity.events,
                    s = o.onDiv;
                return e.position && o.onHover.enable && tt(y.bounce, o.onHover.mode) || lt(f.bounce, s)
            }

            interact() {
                const t = this.container, i = t.actualOptions.interactivity.events,
                    e = t.interactivity.status === ft.mouseMoveEvent, o = i.onHover.enable, s = i.onHover.mode,
                    n = i.onDiv;
                e && o && tt(y.bounce, s) ? this.processMouseBounce() : ct(f.bounce, n, ((t, i) => this.singleSelectorBounce(t, i)))
            }

            reset() {
            }

            processMouseBounce() {
                const t = this.container, i = 10 * t.retina.pixelRatio, e = t.interactivity.mouse.position,
                    o = t.retina.bounceModeDistance;
                e && this.processBounce(e, o, new ni(e.x, e.y, o + i))
            }

            singleSelectorBounce(t, i) {
                const e = this.container, o = document.querySelectorAll(t);
                o.length && o.forEach((t => {
                    const o = t, s = e.retina.pixelRatio,
                        n = {x: (o.offsetLeft + o.offsetWidth / 2) * s, y: (o.offsetTop + o.offsetHeight / 2) * s},
                        a = o.offsetWidth / 2 * s, r = 10 * s,
                        l = i.type === E.circle ? new ni(n.x, n.y, a + r) : new ai(o.offsetLeft * s - r, o.offsetTop * s - r, o.offsetWidth * s + 2 * r, o.offsetHeight * s + 2 * r);
                    this.processBounce(n, a, l)
                }))
            }

            processBounce(t, i, e) {
                const o = this.container.particles.quadTree.query(e);
                for (const s of o) e instanceof ni ? pt(ut(s), {
                    position: t,
                    radius: i,
                    mass: i ** 2 * Math.PI / 2,
                    velocity: I.origin,
                    factor: I.origin
                }) : e instanceof ai && vt(s, at(t, i))
            }
        }

        const ps = ["text", "character", "char"];

        class vs {
            getSidesCount() {
                return 12
            }

            async init(t) {
                const i = t.actualOptions;
                if (ps.find((t => tt(t, i.particles.shape.type)))) {
                    const t = ps.map((t => i.particles.shape.options[t])).find((t => !!t));
                    if (t instanceof Array) {
                        const i = [];
                        for (const e of t) i.push(it(e));
                        await Promise.allSettled(i)
                    } else void 0 !== t && await it(t)
                }
            }

            draw(t, i, e, o) {
                var s, n, a;
                const r = i.shapeData;
                if (void 0 === r) return;
                const l = r.value;
                if (void 0 === l) return;
                const c = i;
                void 0 === c.text && (c.text = l instanceof Array ? ot(l, i.randomIndexData) : l);
                const d = c.text, h = null !== (s = r.style) && void 0 !== s ? s : "",
                    u = null !== (n = r.weight) && void 0 !== n ? n : "400", p = 2 * Math.round(e),
                    v = null !== (a = r.font) && void 0 !== a ? a : "Verdana", f = i.fill, y = d.length * e / 2;
                t.font = `${h} ${u} ${p}px "${v}"`;
                const m = {x: -y, y: e / 2};
                t.globalAlpha = o, f ? t.fillText(d, m.x, m.y) : t.strokeText(d, m.x, m.y), t.globalAlpha = 1
            }
        }

        function fs(t, i, e, o, s) {
            let n = j(t, i);
            if (!s || n <= e) return n;
            if (n = j(t, {x: i.x - o.width, y: i.y}), n <= e) return n;
            if (n = j(t, {x: i.x - o.width, y: i.y - o.height}), n <= e) return n;
            return n = j(t, {x: i.x, y: i.y - o.height}), n
        }

        class ys extends Zo {
            constructor(t) {
                super(t)
            }

            isEnabled(t) {
                return t.options.links.enable
            }

            reset() {
            }

            interact(t) {
                var i;
                t.links = [];
                const e = t.getPosition(), o = this.container, s = o.canvas.size;
                if (e.x < 0 || e.y < 0 || e.x > s.width || e.y > s.height) return;
                const n = t.options.links, a = n.opacity,
                    r = null !== (i = t.retina.linksDistance) && void 0 !== i ? i : o.retina.linksDistance, l = n.warp,
                    c = l ? new ri(e.x, e.y, r, s) : new ni(e.x, e.y, r), d = o.particles.quadTree.query(c);
                for (const i of d) {
                    const o = i.options.links;
                    if (t === i || !o.enable || n.id !== o.id || i.spawning || i.destroyed || -1 !== t.links.map((t => t.destination)).indexOf(i) || -1 !== i.links.map((t => t.destination)).indexOf(t)) continue;
                    const c = i.getPosition();
                    if (c.x < 0 || c.y < 0 || c.x > s.width || c.y > s.height) continue;
                    const d = fs(e, c, r, s, l && o.warp);
                    if (d > r) return;
                    const h = (1 - d / r) * a;
                    this.setColor(t), t.links.push({destination: i, opacity: h})
                }
            }

            setColor(t) {
                const i = this.container, e = t.options.links;
                let o = void 0 === e.id ? i.particles.linksColor : i.particles.linksColors.get(e.id);
                if (!o) {
                    o = _t(e.color, e.blink, e.consent), void 0 === e.id ? i.particles.linksColor = o : i.particles.linksColors.set(e.id, o)
                }
            }
        }

        class ms {
            constructor(t) {
                this.container = t
            }

            particleCreated(t) {
                t.links = []
            }

            particleDestroyed(t) {
                t.links = []
            }

            drawParticle(t, i) {
                const e = i, o = this.container, s = o.particles, n = i.options;
                if (e.links.length > 0) {
                    t.save();
                    const i = e.links.filter((t => o.particles.getLinkFrequency(e, t.destination) <= n.links.frequency));
                    for (const t of i) {
                        const a = t.destination;
                        if (n.links.triangles.enable) {
                            const r = i.map((t => t.destination)),
                                l = a.links.filter((t => o.particles.getLinkFrequency(a, t.destination) <= a.options.links.frequency && r.indexOf(t.destination) >= 0));
                            if (l.length) for (const i of l) {
                                const o = i.destination;
                                s.getTriangleFrequency(e, a, o) > n.links.triangles.frequency || this.drawLinkTriangle(e, t, i)
                            }
                        }
                        t.opacity > 0 && o.retina.linksWidth > 0 && this.drawLinkLine(e, t)
                    }
                    t.restore()
                }
            }

            drawLinkTriangle(t, i, e) {
                var o;
                const s = this.container, n = s.actualOptions, a = i.destination, r = e.destination,
                    l = t.options.links.triangles,
                    c = null !== (o = l.opacity) && void 0 !== o ? o : (i.opacity + e.opacity) / 2;
                if (c <= 0) return;
                const d = t.getPosition(), h = a.getPosition(), u = r.getPosition();
                s.canvas.draw((i => {
                    if (j(d, h) > s.retina.linksDistance || j(u, h) > s.retina.linksDistance || j(u, d) > s.retina.linksDistance) return;
                    let e = bt(l.color);
                    if (!e) {
                        const i = t.options.links,
                            o = void 0 !== i.id ? s.particles.linksColors.get(i.id) : s.particles.linksColor;
                        e = Ft(t, a, o)
                    }
                    e && $t(i, d, h, u, n.backgroundMask.enable, n.backgroundMask.composite, e, c)
                }))
            }

            drawLinkLine(t, i) {
                const e = this.container, o = e.actualOptions, s = i.destination;
                let n = i.opacity;
                const a = t.getPosition(), r = s.getPosition();
                e.canvas.draw((i => {
                    var l, c;
                    let d;
                    const h = t.options.twinkle.lines;
                    if (h.enable) {
                        const t = h.frequency, i = bt(h.color);
                        Math.random() < t && void 0 !== i && (d = i, n = h.opacity)
                    }
                    if (!d) {
                        const i = t.options.links,
                            o = void 0 !== i.id ? e.particles.linksColors.get(i.id) : e.particles.linksColor;
                        d = Ft(t, s, o)
                    }
                    if (!d) return;
                    const u = null !== (l = t.retina.linksWidth) && void 0 !== l ? l : e.retina.linksWidth,
                        p = null !== (c = t.retina.linksDistance) && void 0 !== c ? c : e.retina.linksDistance;
                    Nt(i, u, a, r, p, e.canvas.size, t.options.links.warp, o.backgroundMask.enable, o.backgroundMask.composite, d, n, t.options.links.shadow)
                }))
            }
        }

        class bs {
            constructor() {
                this.id = "links"
            }

            getPlugin(t) {
                return new ms(t)
            }

            needsPlugin() {
                return !0
            }

            loadOptions() {
            }
        }

        async function gs(t) {
            await async function (t) {
                await t.addInteractor("particlesLinks", (t => new ys(t)))
            }(t), await async function (t) {
                const i = new bs;
                await t.addPlugin(i)
            }(t)
        }

        function ws(t, i) {
            var e, o, s, n;
            const a = (null !== (e = t.size.velocity) && void 0 !== e ? e : 0) * i.factor, r = t.size.min,
                l = t.size.max;
            if (!t.destroyed && t.size.enable && ((null !== (o = t.size.loops) && void 0 !== o ? o : 0) <= 0 || (null !== (s = t.size.loops) && void 0 !== s ? s : 0) < (null !== (n = t.size.maxLoops) && void 0 !== n ? n : 0))) {
                switch (t.size.status) {
                    case P.increasing:
                        t.size.value >= l ? (t.size.status = P.decreasing, t.size.loops || (t.size.loops = 0), t.size.loops++) : t.size.value += a;
                        break;
                    case P.decreasing:
                        t.size.value <= r ? (t.size.status = P.increasing, t.size.loops || (t.size.loops = 0), t.size.loops++) : t.size.value -= a
                }
                !function (t, i, e, o) {
                    switch (t.options.size.animation.destroy) {
                        case z.max:
                            i >= o && t.destroy();
                            break;
                        case z.min:
                            i <= e && t.destroy()
                    }
                }(t, t.size.value, r, l), t.destroyed || (t.size.value = L(t.size.value, r, l))
            }
        }

        class xs {
            init() {
            }

            isEnabled(t) {
                var i, e, o;
                return !t.destroyed && !t.spawning && t.size.enable && ((null !== (i = t.size.loops) && void 0 !== i ? i : 0) <= 0 || (null !== (e = t.size.loops) && void 0 !== e ? e : 0) < (null !== (o = t.size.maxLoops) && void 0 !== o ? o : 0))
            }

            update(t, i) {
                this.isEnabled(t) && ws(t, i)
            }
        }

        async function ks(t) {
            await async function (t) {
                await t.addInteractor("externalAttract", (t => new Jo(t)))
            }(t), await async function (t) {
                await t.addInteractor("externalBounce", (t => new us(t)))
            }(t), await async function (t) {
                await t.addInteractor("externalBubble", (t => new Qo(t)))
            }(t), await async function (t) {
                await t.addInteractor("externalConnect", (t => new Do(t)))
            }(t), await async function (t) {
                await t.addInteractor("externalGrab", (t => new Xo(t)))
            }(t), await async function (t) {
                await t.addInteractor("externalRepulse", (t => new ds(t)))
            }(t), await async function (t) {
                await t.addInteractor("particlesAttract", (t => new Ko(t)))
            }(t), await async function (t) {
                await t.addInteractor("particlesCollisions", (t => new rs(t)))
            }(t), await gs(t), await async function (t) {
                await t.addShape("circle", new Eo)
            }(t), await async function (t) {
                const i = new Wo;
                await t.addShape("image", i), await t.addShape("images", i)
            }(t), await async function (t) {
                await t.addShape("line", new hs)
            }(t), await No(t), await async function (t) {
                const i = new is;
                await t.addShape("edge", i), await t.addShape("square", i)
            }(t), await async function (t) {
                await t.addShape("star", new Yo)
            }(t), await async function (t) {
                const i = new vs;
                for (const e of ps) await t.addShape(e, i)
            }(t), await async function (t) {
                await t.addParticleUpdater("life", (t => new Ro(t)))
            }(t), await async function (t) {
                await t.addParticleUpdater("opacity", (t => new Lo(t)))
            }(t), await async function (t) {
                await t.addParticleUpdater("size", (() => new xs))
            }(t), await async function (t) {
                await t.addParticleUpdater("angle", (t => new ls(t)))
            }(t), await async function (t) {
                await t.addParticleUpdater("color", (t => new ns(t)))
            }(t), await async function (t) {
                await t.addParticleUpdater("strokeColor", (t => new os(t)))
            }(t), await async function (t) {
                await t.addParticleUpdater("outModes", (t => new cs(t)))
            }(t)
        }

        class Ps extends Ao {
            constructor(t) {
                super(t), this.delay = 0
            }

            interact(t) {
                var i, e, o, s;
                if (!this.container.retina.reduceFactor) return;
                const n = this.container, a = n.actualOptions.interactivity.modes.trail,
                    r = 1e3 * a.delay / this.container.retina.reduceFactor;
                if (this.delay < r && (this.delay += t.value), this.delay < r) return;
                let l = !0;
                a.pauseOnStop && (n.interactivity.mouse.position === this.lastPosition || (null === (i = n.interactivity.mouse.position) || void 0 === i ? void 0 : i.x) === (null === (e = this.lastPosition) || void 0 === e ? void 0 : e.x) && (null === (o = n.interactivity.mouse.position) || void 0 === o ? void 0 : o.y) === (null === (s = this.lastPosition) || void 0 === s ? void 0 : s.y)) && (l = !1), n.interactivity.mouse.position ? this.lastPosition = {
                    x: n.interactivity.mouse.position.x,
                    y: n.interactivity.mouse.position.y
                } : delete this.lastPosition, l && n.particles.push(a.quantity, n.interactivity.mouse, a.particles), this.delay -= r
            }

            isEnabled() {
                const t = this.container, i = t.actualOptions, e = t.interactivity.mouse, o = i.interactivity.events;
                return e.clicking && e.inside && !!e.position && tt(p.trail, o.onClick.mode) || e.inside && !!e.position && tt(y.trail, o.onHover.mode)
            }

            reset() {
            }
        }

        class Ms {
            constructor(t) {
                this.container = t
            }

            init(t) {
                const i = t.options.tilt;
                t.tilt = {
                    enable: i.enable,
                    value: F(i.value) * Math.PI / 180,
                    sinDirection: Math.random() >= .5 ? 1 : -1,
                    cosDirection: Math.random() >= .5 ? 1 : -1
                };
                let e = i.direction;
                if (e === u.random) {
                    e = Math.floor(2 * Math.random()) > 0 ? u.counterClockwise : u.clockwise
                }
                switch (e) {
                    case u.counterClockwise:
                    case"counterClockwise":
                        t.tilt.status = P.decreasing;
                        break;
                    case u.clockwise:
                        t.tilt.status = P.increasing
                }
                const o = t.options.tilt.animation;
                o.enable && (t.tilt.velocity = o.speed / 360 * this.container.retina.reduceFactor, o.sync || (t.tilt.velocity *= Math.random()))
            }

            isEnabled(t) {
                const i = t.options.tilt.animation;
                return !t.destroyed && !t.spawning && i.enable
            }

            update(t, i) {
                this.isEnabled(t) && function (t, i) {
                    var e;
                    if (!t.tilt) return;
                    const o = t.options.tilt.animation,
                        s = (null !== (e = t.tilt.velocity) && void 0 !== e ? e : 0) * i.factor, n = 2 * Math.PI;
                    if (o.enable) switch (t.tilt.status) {
                        case P.increasing:
                            t.tilt.value += s, t.tilt.value > n && (t.tilt.value -= n);
                            break;
                        case P.decreasing:
                        default:
                            t.tilt.value -= s, t.tilt.value < 0 && (t.tilt.value += n)
                    }
                }(t, i)
            }
        }

        class zs {
            constructor(t) {
                this.container = t
            }

            init(t) {
                const i = t.options.wobble;
                i.enable ? t.wobble = {
                    angle: Math.random() * Math.PI * 2,
                    speed: F(i.speed) / 360
                } : t.wobble = {
                    angle: 0,
                    speed: 0
                }, t.retina.wobbleDistance = F(i.distance) * this.container.retina.pixelRatio
            }

            isEnabled(t) {
                return !t.destroyed && !t.spawning && t.options.wobble.enable
            }

            update(t, i) {
                this.isEnabled(t) && function (t, i) {
                    var e;
                    if (!t.options.wobble.enable || !t.wobble) return;
                    const o = t.wobble.speed * i.factor,
                        s = (null !== (e = t.retina.wobbleDistance) && void 0 !== e ? e : 0) * i.factor / (1e3 / 60),
                        n = 2 * Math.PI;
                    t.wobble.angle += o, t.wobble.angle > n && (t.wobble.angle -= n), t.position.x += s * Math.cos(t.wobble.angle), t.position.y += s * Math.abs(Math.sin(t.wobble.angle))
                }(t, i)
            }
        }

        class Cs {
            constructor(t, i, e, o) {
                var s, n, a;
                this.absorbers = t, this.container = i, this.initialPosition = o ? I.create(o.x, o.y) : void 0, this.options = e, this.dragging = !1, this.name = this.options.name, this.opacity = this.options.opacity, this.size = F(e.size.value) * i.retina.pixelRatio, this.mass = this.size * e.size.density * i.retina.reduceFactor;
                const r = e.size.limit;
                this.limit = void 0 !== r ? r * i.retina.pixelRatio * i.retina.reduceFactor : r;
                const l = "string" == typeof e.color ? {value: e.color} : e.color;
                this.color = null !== (s = bt(l)) && void 0 !== s ? s : {
                    b: 0,
                    g: 0,
                    r: 0
                }, this.position = null !== (a = null === (n = this.initialPosition) || void 0 === n ? void 0 : n.copy()) && void 0 !== a ? a : this.calcPosition()
            }

            attract(t) {
                const i = this.container, e = this.options;
                if (e.draggable) {
                    const t = i.interactivity.mouse;
                    if (t.clicking && t.downPosition) {
                        j(this.position, t.downPosition) <= this.size && (this.dragging = !0)
                    } else this.dragging = !1;
                    this.dragging && t.position && (this.position.x = t.position.x, this.position.y = t.position.y)
                }
                const o = t.getPosition(), {dx: s, dy: n, distance: a} = G(this.position, o), r = I.create(s, n);
                if (r.length = this.mass / Math.pow(a, 2) * i.retina.reduceFactor, a < this.size + t.getRadius()) {
                    const o = .033 * t.getRadius() * i.retina.pixelRatio;
                    this.size > t.getRadius() && a < this.size - t.getRadius() || void 0 !== t.absorberOrbit && t.absorberOrbit.length < 0 ? e.destroy ? t.destroy() : (t.needsNewPosition = !0, this.updateParticlePosition(t, r)) : (e.destroy && (t.size.value -= o), this.updateParticlePosition(t, r)), (void 0 === this.limit || this.size < this.limit) && (this.size += o), this.mass += o * this.options.size.density * i.retina.reduceFactor
                } else this.updateParticlePosition(t, r)
            }

            resize() {
                const t = this.initialPosition;
                this.position = t && st(t, this.container.canvas.size) ? t : this.calcPosition()
            }

            draw(t) {
                t.translate(this.position.x, this.position.y), t.beginPath(), t.arc(0, 0, this.size, 0, 2 * Math.PI, !1), t.closePath(), t.fillStyle = It(this.color, this.opacity), t.fill()
            }

            calcPosition() {
                var t, i;
                const e = this.container, o = this.options.position;
                return I.create((null !== (t = null == o ? void 0 : o.x) && void 0 !== t ? t : 100 * Math.random()) / 100 * e.canvas.size.width, (null !== (i = null == o ? void 0 : o.y) && void 0 !== i ? i : 100 * Math.random()) / 100 * e.canvas.size.height)
            }

            updateParticlePosition(t, i) {
                var e;
                if (t.destroyed) return;
                const o = this.container, s = o.canvas.size;
                if (t.needsNewPosition) {
                    const i = t.getRadius();
                    t.position.x = (s.width - 2 * i) * (.2 * Math.random() - .1 + 1) + i, t.position.y = (s.height - 2 * i) * (.2 * Math.random() - .1 + 1) + i, t.needsNewPosition = !1
                }
                if (this.options.orbits) {
                    if (void 0 === t.absorberOrbit && (t.absorberOrbit = I.create(0, 0), t.absorberOrbit.length = j(t.getPosition(), this.position), t.absorberOrbit.angle = Math.random() * Math.PI * 2), t.absorberOrbit.length <= this.size && !this.options.destroy) {
                        const i = Math.min(s.width, s.height);
                        t.absorberOrbit.length = i * (.2 * Math.random() - .1 + 1)
                    }
                    void 0 === t.absorberOrbitDirection && (t.absorberOrbitDirection = t.velocity.x >= 0 ? d.clockwise : d.counterClockwise);
                    const n = t.absorberOrbit.length, a = t.absorberOrbit.angle, r = t.absorberOrbitDirection;
                    t.velocity.x = 0, t.velocity.y = 0;
                    const l = {x: r === d.clockwise ? Math.cos : Math.sin, y: r === d.clockwise ? Math.sin : Math.cos};
                    t.position.x = this.position.x + n * l.x(a), t.position.y = this.position.y + n * l.y(a), t.absorberOrbit.length -= i.length, t.absorberOrbit.angle += (null !== (e = t.retina.moveSpeed) && void 0 !== e ? e : 0) * o.retina.pixelRatio / 100 * o.retina.reduceFactor
                } else {
                    const e = I.origin;
                    e.length = i.length, e.angle = i.angle, t.velocity.addTo(e)
                }
            }
        }

        class Os extends Ti {
            constructor() {
                super(), this.density = 5, this.random.minimumValue = 1, this.value = 50
            }

            load(t) {
                t && (super.load(t), void 0 !== t.density && (this.density = t.density), void 0 !== t.limit && (this.limit = t.limit), void 0 !== t.limit && (this.limit = t.limit))
            }
        }

        class Ss {
            constructor() {
                this.color = new ki, this.color.value = "#000000", this.draggable = !1, this.opacity = 1, this.destroy = !0, this.orbits = !1, this.size = new Os
            }

            load(t) {
                void 0 !== t && (void 0 !== t.color && (this.color = ki.create(this.color, t.color)), void 0 !== t.draggable && (this.draggable = t.draggable), this.name = t.name, void 0 !== t.opacity && (this.opacity = t.opacity), void 0 !== t.position && (this.position = {
                    x: t.position.x,
                    y: t.position.y
                }), void 0 !== t.size && this.size.load(t.size), void 0 !== t.destroy && (this.destroy = t.destroy), void 0 !== t.orbits && (this.orbits = t.orbits))
            }
        }

        var Ts;
        !function (t) {
            t.absorber = "absorber"
        }(Ts || (Ts = {}));

        class Es {
            constructor(t) {
                this.container = t, this.array = [], this.absorbers = [], this.interactivityAbsorbers = [];
                const i = t;
                i.getAbsorber = t => void 0 === t || "number" == typeof t ? this.array[t || 0] : this.array.find((i => i.name === t)), i.addAbsorber = (t, i) => this.addAbsorber(t, i)
            }

            init(t) {
                var i, e;
                if (!t) return;
                t.absorbers && (t.absorbers instanceof Array ? this.absorbers = t.absorbers.map((t => {
                    const i = new Ss;
                    return i.load(t), i
                })) : (this.absorbers instanceof Array && (this.absorbers = new Ss), this.absorbers.load(t.absorbers)));
                const o = null === (e = null === (i = t.interactivity) || void 0 === i ? void 0 : i.modes) || void 0 === e ? void 0 : e.absorbers;
                if (o && (o instanceof Array ? this.interactivityAbsorbers = o.map((t => {
                    const i = new Ss;
                    return i.load(t), i
                })) : (this.interactivityAbsorbers instanceof Array && (this.interactivityAbsorbers = new Ss), this.interactivityAbsorbers.load(o))), this.absorbers instanceof Array) for (const t of this.absorbers) this.addAbsorber(t); else this.addAbsorber(this.absorbers)
            }

            particleUpdate(t) {
                for (const i of this.array) if (i.attract(t), t.destroyed) break
            }

            draw(t) {
                for (const i of this.array) t.save(), i.draw(t), t.restore()
            }

            stop() {
                this.array = []
            }

            resize() {
                for (const t of this.array) t.resize()
            }

            handleClickMode(t) {
                const i = this.container, e = this.absorbers, o = this.interactivityAbsorbers;
                if (t === Ts.absorber) {
                    let t;
                    o instanceof Array ? o.length > 0 && (t = ot(o)) : t = o;
                    const s = null != t ? t : e instanceof Array ? ot(e) : e, n = i.interactivity.mouse.clickPosition;
                    this.addAbsorber(s, n)
                }
            }

            addAbsorber(t, i) {
                const e = new Cs(this, this.container, t, i);
                return this.array.push(e), e
            }

            removeAbsorber(t) {
                const i = this.array.indexOf(t);
                i >= 0 && this.array.splice(i, 1)
            }
        }

        class Rs {
            constructor() {
                this.id = "absorbers"
            }

            getPlugin(t) {
                return new Es(t)
            }

            needsPlugin(t) {
                var i, e, o;
                if (void 0 === t) return !1;
                const s = t.absorbers;
                let n = !1;
                return s instanceof Array ? s.length && (n = !0) : (void 0 !== s || (null === (o = null === (e = null === (i = t.interactivity) || void 0 === i ? void 0 : i.events) || void 0 === e ? void 0 : e.onClick) || void 0 === o ? void 0 : o.mode) && tt(Ts.absorber, t.interactivity.events.onClick.mode)) && (n = !0), n
            }

            loadOptions(t, i) {
                var e, o;
                if (!this.needsPlugin(t) && !this.needsPlugin(i)) return;
                const s = t;
                if (null == i ? void 0 : i.absorbers) if ((null == i ? void 0 : i.absorbers) instanceof Array) s.absorbers = null == i ? void 0 : i.absorbers.map((t => {
                    const i = new Ss;
                    return i.load(t), i
                })); else {
                    let t = s.absorbers;
                    void 0 === (null == t ? void 0 : t.load) && (s.absorbers = t = new Ss), t.load(null == i ? void 0 : i.absorbers)
                }
                const n = null === (o = null === (e = null == i ? void 0 : i.interactivity) || void 0 === e ? void 0 : e.modes) || void 0 === o ? void 0 : o.absorbers;
                if (n) if (n instanceof Array) s.interactivity.modes.absorbers = n.map((t => {
                    const i = new Ss;
                    return i.load(t), i
                })); else {
                    let t = s.interactivity.modes.absorbers;
                    void 0 === (null == t ? void 0 : t.load) && (s.interactivity.modes.absorbers = t = new Ss), t.load(n)
                }
            }
        }

        class As {
            constructor() {
                this.mode = w.percent, this.height = 0, this.width = 0
            }

            load(t) {
                void 0 !== t && (void 0 !== t.mode && (this.mode = t.mode), void 0 !== t.height && (this.height = t.height), void 0 !== t.width && (this.width = t.width))
            }
        }

        const Ds = new Map;

        class Is {
            static addShape(t, i) {
                Is.getShape(t) || Ds.set(t, i)
            }

            static getShape(t) {
                return Ds.get(t)
            }

            static getSupportedShapes() {
                return Ds.keys()
            }
        }

        var Ls, Hs, qs, Fs, _s, Vs, Bs, Ws = function (t, i, e, o, s) {
            if ("m" === o) throw new TypeError("Private method is not writable");
            if ("a" === o && !s) throw new TypeError("Private accessor was defined without a setter");
            if ("function" == typeof i ? t !== i || !s : !i.has(t)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
            return "a" === o ? s.call(t, e) : s ? s.value = e : i.set(t, e), e
        }, Gs = function (t, i, e, o) {
            if ("a" === e && !o) throw new TypeError("Private accessor was defined without a getter");
            if ("function" == typeof i ? t !== i || !o : !i.has(t)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
            return "m" === e ? o : "a" === e ? o.call(t) : o ? o.value : i.get(t)
        };

        class js {
            constructor(t, i, e, o) {
                var s, n, a, r, l, c, d;
                this.emitters = t, this.container = i, Ls.set(this, void 0), Hs.set(this, void 0), this.currentDuration = 0, this.currentEmitDelay = 0, this.currentSpawnDelay = 0, this.initialPosition = o, this.emitterOptions = rt({}, e), this.spawnDelay = 1e3 * (null !== (s = this.emitterOptions.life.delay) && void 0 !== s ? s : 0) / this.container.retina.reduceFactor, this.position = null !== (n = this.initialPosition) && void 0 !== n ? n : this.calcPosition(), this.name = e.name, this.shape = Is.getShape(e.shape), this.fill = e.fill, Ws(this, Ls, !this.emitterOptions.life.wait, "f"), Ws(this, Hs, !1, "f");
                let h = rt({}, this.emitterOptions.particles);
                null != h || (h = {}), null !== (a = h.move) && void 0 !== a || (h.move = {}), null !== (r = (d = h.move).direction) && void 0 !== r || (d.direction = this.emitterOptions.direction), void 0 !== this.emitterOptions.spawnColor && (this.spawnColor = gt(this.emitterOptions.spawnColor)), this.paused = !this.emitterOptions.autoPlay, this.particlesOptions = h, this.size = null !== (l = this.emitterOptions.size) && void 0 !== l ? l : (() => {
                    const t = new As;
                    return t.load({height: 0, mode: w.percent, width: 0}), t
                })(), this.lifeCount = null !== (c = this.emitterOptions.life.count) && void 0 !== c ? c : -1, this.immortal = this.lifeCount <= 0, this.play()
            }

            externalPlay() {
                this.paused = !1, this.play()
            }

            externalPause() {
                this.paused = !0, this.pause()
            }

            play() {
                var t;
                if (!this.paused && this.container.retina.reduceFactor && (this.lifeCount > 0 || this.immortal || !this.emitterOptions.life.count) && (Gs(this, Ls, "f") || this.currentSpawnDelay >= (null !== (t = this.spawnDelay) && void 0 !== t ? t : 0))) {
                    if (void 0 === this.emitDelay) {
                        const t = F(this.emitterOptions.rate.delay);
                        this.emitDelay = 1e3 * t / this.container.retina.reduceFactor
                    }
                    (this.lifeCount > 0 || this.immortal) && this.prepareToDie()
                }
            }

            pause() {
                this.paused || delete this.emitDelay
            }

            resize() {
                const t = this.initialPosition;
                this.position = t && st(t, this.container.canvas.size) ? t : this.calcPosition()
            }

            update(t) {
                var i, e, o;
                this.paused || (Gs(this, Ls, "f") && (Ws(this, Ls, !1, "f"), this.currentSpawnDelay = null !== (i = this.spawnDelay) && void 0 !== i ? i : 0, this.currentEmitDelay = null !== (e = this.emitDelay) && void 0 !== e ? e : 0), Gs(this, Hs, "f") || (Ws(this, Hs, !0, "f"), this.emitParticles(this.emitterOptions.startCount)), void 0 !== this.duration && (this.currentDuration += t.value, this.currentDuration >= this.duration && (this.pause(), void 0 !== this.spawnDelay && delete this.spawnDelay, this.immortal || this.lifeCount--, this.lifeCount > 0 || this.immortal ? (this.position = this.calcPosition(), this.spawnDelay = 1e3 * (null !== (o = this.emitterOptions.life.delay) && void 0 !== o ? o : 0) / this.container.retina.reduceFactor) : this.destroy(), this.currentDuration -= this.duration, delete this.duration)), void 0 !== this.spawnDelay && (this.currentSpawnDelay += t.value, this.currentSpawnDelay >= this.spawnDelay && (this.play(), this.currentSpawnDelay -= this.currentSpawnDelay, delete this.spawnDelay)), void 0 !== this.emitDelay && (this.currentEmitDelay += t.value, this.currentEmitDelay >= this.emitDelay && (this.emit(), this.currentEmitDelay -= this.emitDelay)))
            }

            prepareToDie() {
                var t;
                if (this.paused) return;
                const i = null === (t = this.emitterOptions.life) || void 0 === t ? void 0 : t.duration;
                this.container.retina.reduceFactor && (this.lifeCount > 0 || this.immortal) && void 0 !== i && i > 0 && (this.duration = 1e3 * i)
            }

            destroy() {
                this.emitters.removeEmitter(this)
            }

            calcPosition() {
                var t, i;
                const e = this.container, o = this.emitterOptions.position;
                return {
                    x: (null !== (t = null == o ? void 0 : o.x) && void 0 !== t ? t : 100 * Math.random()) / 100 * e.canvas.size.width,
                    y: (null !== (i = null == o ? void 0 : o.y) && void 0 !== i ? i : 100 * Math.random()) / 100 * e.canvas.size.height
                }
            }

            emit() {
                if (this.paused) return;
                const t = F(this.emitterOptions.rate.quantity);
                this.emitParticles(t)
            }

            emitParticles(t) {
                var i, e, o;
                const s = this.container, n = this.position, a = {
                    x: this.size.mode === w.percent ? s.canvas.size.width * this.size.width / 100 : this.size.width,
                    y: this.size.mode === w.percent ? s.canvas.size.height * this.size.height / 100 : this.size.height
                };
                for (let r = 0; r < t; r++) {
                    const t = rt({}, this.particlesOptions);
                    if (this.spawnColor) {
                        const e = null === (i = this.emitterOptions.spawnColor) || void 0 === i ? void 0 : i.animation;
                        if (e) {
                            const t = e;
                            if (t.enable) this.spawnColor.h = this.setColorAnimation(t, this.spawnColor.h, 360); else {
                                const t = e;
                                this.spawnColor.h = this.setColorAnimation(t.h, this.spawnColor.h, 360), this.spawnColor.s = this.setColorAnimation(t.s, this.spawnColor.s, 100), this.spawnColor.l = this.setColorAnimation(t.l, this.spawnColor.l, 100)
                            }
                        }
                        t.color ? t.color.value = this.spawnColor : t.color = {value: this.spawnColor}
                    }
                    const r = null !== (o = null === (e = this.shape) || void 0 === e ? void 0 : e.randomPosition(n, a, this.fill)) && void 0 !== o ? o : n;
                    s.particles.addParticle(r, t)
                }
            }

            setColorAnimation(t, i, e) {
                var o;
                const s = this.container;
                if (!t.enable) return i;
                const n = q(t.offset), a = 1e3 * F(this.emitterOptions.rate.delay) / s.retina.reduceFactor;
                return (i + (null !== (o = t.speed) && void 0 !== o ? o : 0) * s.fpsLimit / a + 3.6 * n) % e
            }
        }

        Ls = new WeakMap, Hs = new WeakMap;

        class Us {
            constructor() {
                this.quantity = 1, this.delay = .1
            }

            load(t) {
                void 0 !== t && (void 0 !== t.quantity && (this.quantity = B(t.quantity)), void 0 !== t.delay && (this.delay = B(t.delay)))
            }
        }

        class Ns {
            constructor() {
                this.wait = !1
            }

            load(t) {
                void 0 !== t && (void 0 !== t.count && (this.count = t.count), void 0 !== t.delay && (this.delay = t.delay), void 0 !== t.duration && (this.duration = t.duration), void 0 !== t.wait && (this.wait = t.wait))
            }
        }

        !function (t) {
            t.emitter = "emitter"
        }(qs || (qs = {})), function (t) {
            t.circle = "circle", t.square = "square"
        }(Fs || (Fs = {}));

        class $s {
            constructor() {
                this.autoPlay = !0, this.fill = !0, this.life = new Ns, this.rate = new Us, this.shape = Fs.square, this.startCount = 0
            }

            load(t) {
                void 0 !== t && (void 0 !== t.autoPlay && (this.autoPlay = t.autoPlay), void 0 !== t.size && (void 0 === this.size && (this.size = new As), this.size.load(t.size)), void 0 !== t.direction && (this.direction = t.direction), void 0 !== t.fill && (this.fill = t.fill), this.life.load(t.life), this.name = t.name, void 0 !== t.particles && (this.particles = rt({}, t.particles)), this.rate.load(t.rate), void 0 !== t.shape && (this.shape = t.shape), void 0 !== t.position && (this.position = {
                    x: t.position.x,
                    y: t.position.y
                }), void 0 !== t.spawnColor && (void 0 === this.spawnColor && (this.spawnColor = new Xi), this.spawnColor.load(t.spawnColor)), void 0 !== t.startCount && (this.startCount = t.startCount))
            }
        }

        class Qs {
            constructor(t) {
                this.container = t, this.array = [], this.emitters = [], this.interactivityEmitters = [];
                const i = t;
                i.getEmitter = t => void 0 === t || "number" == typeof t ? this.array[t || 0] : this.array.find((i => i.name === t)), i.addEmitter = (t, i) => this.addEmitter(t, i), i.playEmitter = t => {
                    const e = i.getEmitter(t);
                    e && e.externalPlay()
                }, i.pauseEmitter = t => {
                    const e = i.getEmitter(t);
                    e && e.externalPause()
                }
            }

            init(t) {
                var i, e;
                if (!t) return;
                t.emitters && (t.emitters instanceof Array ? this.emitters = t.emitters.map((t => {
                    const i = new $s;
                    return i.load(t), i
                })) : (this.emitters instanceof Array && (this.emitters = new $s), this.emitters.load(t.emitters)));
                const o = null === (e = null === (i = t.interactivity) || void 0 === i ? void 0 : i.modes) || void 0 === e ? void 0 : e.emitters;
                if (o && (o instanceof Array ? this.interactivityEmitters = o.map((t => {
                    const i = new $s;
                    return i.load(t), i
                })) : (this.interactivityEmitters instanceof Array && (this.interactivityEmitters = new $s), this.interactivityEmitters.load(o))), this.emitters instanceof Array) for (const t of this.emitters) this.addEmitter(t); else this.addEmitter(this.emitters)
            }

            play() {
                for (const t of this.array) t.play()
            }

            pause() {
                for (const t of this.array) t.pause()
            }

            stop() {
                this.array = []
            }

            update(t) {
                for (const i of this.array) i.update(t)
            }

            handleClickMode(t) {
                const i = this.container, e = this.emitters, o = this.interactivityEmitters;
                if (t === qs.emitter) {
                    let t;
                    o instanceof Array ? o.length > 0 && (t = ot(o)) : t = o;
                    const s = null != t ? t : e instanceof Array ? ot(e) : e, n = i.interactivity.mouse.clickPosition;
                    this.addEmitter(rt({}, s), n)
                }
            }

            resize() {
                for (const t of this.array) t.resize()
            }

            addEmitter(t, i) {
                const e = new js(this, this.container, t, i);
                return this.array.push(e), e
            }

            removeEmitter(t) {
                const i = this.array.indexOf(t);
                i >= 0 && this.array.splice(i, 1)
            }
        }

        class Js {
            randomPosition(t, i, e) {
                const [o, s] = [i.x / 2, i.y / 2], n = ((t, i) => {
                        const e = Math.random() / 4, o = Math.atan(i / t * Math.tan(2 * Math.PI * e)), s = Math.random();
                        return s < .25 ? o : s < .5 ? Math.PI - o : s < .75 ? Math.PI + o : -o
                    })(o, s), a = (d = n, (l = o) * (c = s) / Math.sqrt((c * Math.cos(d)) ** 2 + (l * Math.sin(d)) ** 2)),
                    r = e ? a * Math.sqrt(Math.random()) : a;
                var l, c, d;
                return {x: t.x + r * Math.cos(n), y: t.y + r * Math.sin(n)}
            }
        }

        function Xs(t, i) {
            return t + i * (Math.random() - .5)
        }

        class Ys {
            randomPosition(t, i, e) {
                if (e) return {x: Xs(t.x, i.x), y: Xs(t.y, i.y)};
                {
                    const e = i.x / 2, o = i.y / 2, s = Math.floor(4 * Math.random()), n = 2 * (Math.random() - .5);
                    switch (s) {
                        case 0:
                            return {x: t.x + n * e, y: t.y - o};
                        case 1:
                            return {x: t.x - e, y: t.y + n * o};
                        case 2:
                            return {x: t.x + n * e, y: t.y + o};
                        case 3:
                        default:
                            return {x: t.x + e, y: t.y + n * o}
                    }
                }
            }
        }

        class Zs {
            constructor() {
                this.id = "emitters"
            }

            getPlugin(t) {
                return new Qs(t)
            }

            needsPlugin(t) {
                var i, e, o;
                if (void 0 === t) return !1;
                const s = t.emitters;
                return s instanceof Array && !!s.length || void 0 !== s || !!(null === (o = null === (e = null === (i = t.interactivity) || void 0 === i ? void 0 : i.events) || void 0 === e ? void 0 : e.onClick) || void 0 === o ? void 0 : o.mode) && tt(qs.emitter, t.interactivity.events.onClick.mode)
            }

            loadOptions(t, i) {
                var e, o;
                if (!this.needsPlugin(t) && !this.needsPlugin(i)) return;
                const s = t;
                if (null == i ? void 0 : i.emitters) if ((null == i ? void 0 : i.emitters) instanceof Array) s.emitters = null == i ? void 0 : i.emitters.map((t => {
                    const i = new $s;
                    return i.load(t), i
                })); else {
                    let t = s.emitters;
                    void 0 === (null == t ? void 0 : t.load) && (s.emitters = t = new $s), t.load(null == i ? void 0 : i.emitters)
                }
                const n = null === (o = null === (e = null == i ? void 0 : i.interactivity) || void 0 === e ? void 0 : e.modes) || void 0 === o ? void 0 : o.emitters;
                if (n) if (n instanceof Array) s.interactivity.modes.emitters = n.map((t => {
                    const i = new $s;
                    return i.load(t), i
                })); else {
                    let t = s.interactivity.modes.emitters;
                    void 0 === (null == t ? void 0 : t.load) && (s.interactivity.modes.emitters = t = new $s), t.load(n)
                }
            }
        }

        !function (t) {
            t.equidistant = "equidistant", t.onePerPoint = "one-per-point", t.perPoint = "per-point", t.randomLength = "random-length", t.randomPoint = "random-point"
        }(_s || (_s = {})), function (t) {
            t.path = "path", t.radius = "radius"
        }(Vs || (Vs = {})), function (t) {
            t.inline = "inline", t.inside = "inside", t.outside = "outside", t.none = "none"
        }(Bs || (Bs = {}));

        class Ks {
            constructor() {
                this.color = new ki, this.width = .5, this.opacity = 1
            }

            load(t) {
                var i;
                void 0 !== t && (this.color = ki.create(this.color, t.color), "string" == typeof this.color.value && (this.opacity = null !== (i = xt(this.color.value)) && void 0 !== i ? i : this.opacity), void 0 !== t.opacity && (this.opacity = t.opacity), void 0 !== t.width && (this.width = t.width))
            }
        }

        class tn {
            constructor() {
                this.enable = !1, this.stroke = new Ks
            }

            get lineWidth() {
                return this.stroke.width
            }

            set lineWidth(t) {
                this.stroke.width = t
            }

            get lineColor() {
                return this.stroke.color
            }

            set lineColor(t) {
                this.stroke.color = ki.create(this.stroke.color, t)
            }

            load(t) {
                var i;
                if (void 0 !== t) {
                    void 0 !== t.enable && (this.enable = t.enable);
                    const e = null !== (i = t.stroke) && void 0 !== i ? i : {color: t.lineColor, width: t.lineWidth};
                    this.stroke.load(e)
                }
            }
        }

        class en {
            constructor() {
                this.radius = 10, this.type = Vs.path
            }

            load(t) {
                void 0 !== t && (void 0 !== t.radius && (this.radius = t.radius), void 0 !== t.type && (this.type = t.type))
            }
        }

        class on {
            constructor() {
                this.arrangement = _s.onePerPoint
            }

            load(t) {
                void 0 !== t && void 0 !== t.arrangement && (this.arrangement = t.arrangement)
            }
        }

        class sn {
            constructor() {
                this.path = [], this.size = {height: 0, width: 0}
            }

            load(t) {
                void 0 !== t && (void 0 !== t.path && (this.path = t.path), void 0 !== t.size && (void 0 !== t.size.width && (this.size.width = t.size.width), void 0 !== t.size.height && (this.size.height = t.size.height)))
            }
        }

        class nn {
            constructor() {
                this.draw = new tn, this.enable = !1, this.inline = new on, this.move = new en, this.scale = 1, this.type = Bs.none
            }

            get inlineArrangement() {
                return this.inline.arrangement
            }

            set inlineArrangement(t) {
                this.inline.arrangement = t
            }

            load(t) {
                var i;
                if (void 0 !== t) {
                    this.draw.load(t.draw);
                    const e = null !== (i = t.inline) && void 0 !== i ? i : {arrangement: t.inlineArrangement};
                    void 0 !== e && this.inline.load(e), this.move.load(t.move), void 0 !== t.scale && (this.scale = t.scale), void 0 !== t.type && (this.type = t.type), void 0 !== t.enable ? this.enable = t.enable : this.enable = this.type !== Bs.none, void 0 !== t.url && (this.url = t.url), void 0 !== t.data && ("string" == typeof t.data ? this.data = t.data : (this.data = new sn, this.data.load(t.data))), void 0 !== t.position && (this.position = rt({}, t.position))
                }
            }
        }

        function an(t, i, e) {
            const o = bt(e.color);
            if (o) {
                t.beginPath(), t.moveTo(i[0].x, i[0].y);
                for (const e of i) t.lineTo(e.x, e.y);
                t.closePath(), t.strokeStyle = It(o), t.lineWidth = e.width, t.stroke()
            }
        }

        function rn(t, i, e, o) {
            t.translate(o.x, o.y);
            const s = bt(e.color);
            s && (t.strokeStyle = It(s, e.opacity), t.lineWidth = e.width, t.stroke(i))
        }

        function ln(t, i, e) {
            const {dx: o, dy: s} = G(e, t), {dx: n, dy: a} = G(i, t), r = (o * n + s * a) / (n ** 2 + a ** 2);
            let l = t.x + n * r, c = t.y + a * r;
            return r < 0 ? (l = t.x, c = t.y) : r > 1 && (l = i.x, c = i.y), {x: l, y: c, isOnSegment: r >= 0 && r <= 1}
        }

        function cn(t, i, e) {
            const {dx: o, dy: s} = G(t, i), n = Math.atan2(s, o), a = Math.sin(n), r = -Math.cos(n),
                l = 2 * (e.x * a + e.y * r);
            e.x -= l * a, e.y -= l * r
        }

        class dn {
            constructor(t) {
                this.container = t, this.dimension = {
                    height: 0,
                    width: 0
                }, this.path2DSupported = !!window.Path2D, this.options = new nn, this.polygonMaskMoveRadius = this.options.move.radius * t.retina.pixelRatio
            }

            async initAsync(t) {
                this.options.load(null == t ? void 0 : t.polygon);
                const i = this.options;
                this.polygonMaskMoveRadius = i.move.radius * this.container.retina.pixelRatio, i.enable && await this.initRawData()
            }

            resize() {
                const t = this.container, i = this.options;
                i.enable && i.type !== Bs.none && (this.redrawTimeout && clearTimeout(this.redrawTimeout), this.redrawTimeout = window.setTimeout((async () => {
                    await this.initRawData(!0), t.particles.redraw()
                }), 250))
            }

            stop() {
                delete this.raw, delete this.paths
            }

            particlesInitialization() {
                const t = this.options;
                return !(!t.enable || t.type !== Bs.inline || t.inline.arrangement !== _s.onePerPoint && t.inline.arrangement !== _s.perPoint) && (this.drawPoints(), !0)
            }

            particlePosition(t) {
                var i, e;
                if (this.options.enable && (null !== (e = null === (i = this.raw) || void 0 === i ? void 0 : i.length) && void 0 !== e ? e : 0) > 0) return rt({}, t || this.randomPoint())
            }

            particleBounce(t, i, e) {
                return this.polygonBounce(t, i, e)
            }

            clickPositionValid(t) {
                const i = this.options;
                return i.enable && i.type !== Bs.none && i.type !== Bs.inline && this.checkInsidePolygon(t)
            }

            draw(t) {
                var i;
                if (!(null === (i = this.paths) || void 0 === i ? void 0 : i.length)) return;
                const e = this.options, o = e.draw;
                if (!e.enable || !o.enable) return;
                const s = this.raw;
                for (const i of this.paths) {
                    const e = i.path2d, n = this.path2DSupported;
                    t && (n && e && this.offset ? rn(t, e, o.stroke, this.offset) : s && an(t, s, o.stroke))
                }
            }

            polygonBounce(t, i, e) {
                const o = this.options;
                if (!this.raw || !o.enable || e !== h.top) return !1;
                if (o.type === Bs.inside || o.type === Bs.outside) {
                    let i, e, o;
                    const s = t.getPosition(), n = t.getRadius();
                    for (let a = 0, r = this.raw.length - 1; a < this.raw.length; r = a++) {
                        const l = this.raw[a], c = this.raw[r];
                        i = ln(l, c, s);
                        const d = G(s, i);
                        if ([e, o] = [d.dx, d.dy], d.distance < n) return cn(l, c, t.velocity), !0
                    }
                    if (i && void 0 !== e && void 0 !== o && !this.checkInsidePolygon(s)) {
                        const e = {x: 1, y: 1};
                        return t.position.x >= i.x && (e.x = -1), t.position.y >= i.y && (e.y = -1), t.position.x = i.x + 2 * n * e.x, t.position.y = i.y + 2 * n * e.y, t.velocity.mult(-1), !0
                    }
                } else if (o.type === Bs.inline && t.initialPosition) {
                    if (j(t.initialPosition, t.getPosition()) > this.polygonMaskMoveRadius) return t.velocity.x = t.velocity.y / 2 - t.velocity.x, t.velocity.y = t.velocity.x / 2 - t.velocity.y, !0
                }
                return !1
            }

            checkInsidePolygon(t) {
                var i, e;
                const o = this.container, s = this.options;
                if (!s.enable || s.type === Bs.none || s.type === Bs.inline) return !0;
                if (!this.raw) throw new Error(ft.noPolygonFound);
                const n = o.canvas.size,
                    a = null !== (i = null == t ? void 0 : t.x) && void 0 !== i ? i : Math.random() * n.width,
                    r = null !== (e = null == t ? void 0 : t.y) && void 0 !== e ? e : Math.random() * n.height;
                let l = !1;
                for (let t = 0, i = this.raw.length - 1; t < this.raw.length; i = t++) {
                    const e = this.raw[t], o = this.raw[i];
                    e.y > r != o.y > r && a < (o.x - e.x) * (r - e.y) / (o.y - e.y) + e.x && (l = !l)
                }
                return s.type === Bs.inside ? l : s.type === Bs.outside && !l
            }

            parseSvgPath(t, i) {
                var e, o, s;
                const n = null != i && i;
                if (void 0 !== this.paths && !n) return this.raw;
                const a = this.container, r = this.options, l = (new DOMParser).parseFromString(t, "image/svg+xml"),
                    c = l.getElementsByTagName("svg")[0];
                let d = c.getElementsByTagName("path");
                d.length || (d = l.getElementsByTagName("path")), this.paths = [];
                for (let t = 0; t < d.length; t++) {
                    const i = d.item(t);
                    i && this.paths.push({element: i, length: i.getTotalLength()})
                }
                const h = a.retina.pixelRatio, u = r.scale / h;
                this.dimension.width = parseFloat(null !== (e = c.getAttribute("width")) && void 0 !== e ? e : "0") * u, this.dimension.height = parseFloat(null !== (o = c.getAttribute("height")) && void 0 !== o ? o : "0") * u;
                const p = null !== (s = r.position) && void 0 !== s ? s : {x: 50, y: 50};
                return this.offset = {
                    x: a.canvas.size.width * p.x / (100 * h) - this.dimension.width / 2,
                    y: a.canvas.size.height * p.y / (100 * h) - this.dimension.height / 2
                }, function (t, i, e) {
                    var o;
                    const s = [];
                    for (const n of t) {
                        const t = n.element.pathSegList,
                            a = null !== (o = null == t ? void 0 : t.numberOfItems) && void 0 !== o ? o : 0,
                            r = {x: 0, y: 0};
                        for (let o = 0; o < a; o++) {
                            const n = null == t ? void 0 : t.getItem(o), a = window.SVGPathSeg;
                            switch (null == n ? void 0 : n.pathSegType) {
                                case a.PATHSEG_MOVETO_ABS:
                                case a.PATHSEG_LINETO_ABS:
                                case a.PATHSEG_CURVETO_CUBIC_ABS:
                                case a.PATHSEG_CURVETO_QUADRATIC_ABS:
                                case a.PATHSEG_ARC_ABS:
                                case a.PATHSEG_CURVETO_CUBIC_SMOOTH_ABS:
                                case a.PATHSEG_CURVETO_QUADRATIC_SMOOTH_ABS: {
                                    const t = n;
                                    r.x = t.x, r.y = t.y;
                                    break
                                }
                                case a.PATHSEG_LINETO_HORIZONTAL_ABS:
                                    r.x = n.x;
                                    break;
                                case a.PATHSEG_LINETO_VERTICAL_ABS:
                                    r.y = n.y;
                                    break;
                                case a.PATHSEG_LINETO_REL:
                                case a.PATHSEG_MOVETO_REL:
                                case a.PATHSEG_CURVETO_CUBIC_REL:
                                case a.PATHSEG_CURVETO_QUADRATIC_REL:
                                case a.PATHSEG_ARC_REL:
                                case a.PATHSEG_CURVETO_CUBIC_SMOOTH_REL:
                                case a.PATHSEG_CURVETO_QUADRATIC_SMOOTH_REL: {
                                    const t = n;
                                    r.x += t.x, r.y += t.y;
                                    break
                                }
                                case a.PATHSEG_LINETO_HORIZONTAL_REL:
                                    r.x += n.x;
                                    break;
                                case a.PATHSEG_LINETO_VERTICAL_REL:
                                    r.y += n.y;
                                    break;
                                case a.PATHSEG_UNKNOWN:
                                case a.PATHSEG_CLOSEPATH:
                                    continue
                            }
                            s.push({x: r.x * i + e.x, y: r.y * i + e.y})
                        }
                    }
                    return s
                }(this.paths, u, this.offset)
            }

            async downloadSvgPath(t, i) {
                const e = this.options, o = t || e.url, s = null != i && i;
                if (!o || void 0 !== this.paths && !s) return this.raw;
                const n = await fetch(o);
                if (!n.ok) throw new Error("tsParticles Error - Error occurred during polygon mask download");
                return this.parseSvgPath(await n.text(), i)
            }

            drawPoints() {
                if (this.raw) for (const t of this.raw) this.container.particles.addParticle({x: t.x, y: t.y})
            }

            randomPoint() {
                const t = this.container, i = this.options;
                let e;
                if (i.type === Bs.inline) switch (i.inline.arrangement) {
                    case _s.randomPoint:
                        e = this.getRandomPoint();
                        break;
                    case _s.randomLength:
                        e = this.getRandomPointByLength();
                        break;
                    case _s.equidistant:
                        e = this.getEquidistantPointByIndex(t.particles.count);
                        break;
                    case _s.onePerPoint:
                    case _s.perPoint:
                    default:
                        e = this.getPointByIndex(t.particles.count)
                } else e = {x: Math.random() * t.canvas.size.width, y: Math.random() * t.canvas.size.height};
                return this.checkInsidePolygon(e) ? e : this.randomPoint()
            }

            getRandomPoint() {
                if (!this.raw || !this.raw.length) throw new Error(ft.noPolygonDataLoaded);
                const t = ot(this.raw);
                return {x: t.x, y: t.y}
            }

            getRandomPointByLength() {
                var t, i, e;
                const o = this.options;
                if (!this.raw || !this.raw.length || !(null === (t = this.paths) || void 0 === t ? void 0 : t.length)) throw new Error(ft.noPolygonDataLoaded);
                const s = ot(this.paths), n = Math.floor(Math.random() * s.length) + 1,
                    a = s.element.getPointAtLength(n);
                return {
                    x: a.x * o.scale + ((null === (i = this.offset) || void 0 === i ? void 0 : i.x) || 0),
                    y: a.y * o.scale + ((null === (e = this.offset) || void 0 === e ? void 0 : e.y) || 0)
                }
            }

            getEquidistantPointByIndex(t) {
                var i, e, o, s, n, a, r;
                const l = this.container.actualOptions, c = this.options;
                if (!this.raw || !this.raw.length || !(null === (i = this.paths) || void 0 === i ? void 0 : i.length)) throw new Error(ft.noPolygonDataLoaded);
                let d, h = 0;
                const u = this.paths.reduce(((t, i) => t + i.length), 0) / l.particles.number.value;
                for (const i of this.paths) {
                    const e = u * t - h;
                    if (e <= i.length) {
                        d = i.element.getPointAtLength(e);
                        break
                    }
                    h += i.length
                }
                return {
                    x: (null !== (e = null == d ? void 0 : d.x) && void 0 !== e ? e : 0) * c.scale + (null !== (s = null === (o = this.offset) || void 0 === o ? void 0 : o.x) && void 0 !== s ? s : 0),
                    y: (null !== (n = null == d ? void 0 : d.y) && void 0 !== n ? n : 0) * c.scale + (null !== (r = null === (a = this.offset) || void 0 === a ? void 0 : a.y) && void 0 !== r ? r : 0)
                }
            }

            getPointByIndex(t) {
                if (!this.raw || !this.raw.length) throw new Error(ft.noPolygonDataLoaded);
                const i = this.raw[t % this.raw.length];
                return {x: i.x, y: i.y}
            }

            createPath2D() {
                var t, i;
                const e = this.options;
                if (this.path2DSupported && (null === (t = this.paths) || void 0 === t ? void 0 : t.length)) for (const t of this.paths) {
                    const o = null === (i = t.element) || void 0 === i ? void 0 : i.getAttribute("d");
                    if (o) {
                        const i = new Path2D(o),
                            s = document.createElementNS("http://www.w3.org/2000/svg", "svg").createSVGMatrix(),
                            n = new Path2D, a = s.scale(e.scale);
                        n.addPath ? (n.addPath(i, a), t.path2d = n) : delete t.path2d
                    } else delete t.path2d;
                    !t.path2d && this.raw && (t.path2d = new Path2D, t.path2d.moveTo(this.raw[0].x, this.raw[0].y), this.raw.forEach(((i, e) => {
                        var o;
                        e > 0 && (null === (o = t.path2d) || void 0 === o || o.lineTo(i.x, i.y))
                    })), t.path2d.closePath())
                }
            }

            async initRawData(t) {
                const i = this.options;
                if (i.url) this.raw = await this.downloadSvgPath(i.url, t); else if (i.data) {
                    const e = i.data;
                    let o;
                    if ("string" != typeof e) {
                        const t = e.path instanceof Array ? e.path.map((t => `<path d="${t}" />`)).join("") : `<path d="${e.path}" />`;
                        o = `<svg ${'xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"'} width="${e.size.width}" height="${e.size.height}">${t}</svg>`
                    } else o = e;
                    this.raw = this.parseSvgPath(o, t)
                }
                this.createPath2D()
            }
        }

        class hn {
            constructor() {
                this.id = "polygonMask"
            }

            getPlugin(t) {
                return new dn(t)
            }

            needsPlugin(t) {
                var i, e, o;
                return null !== (e = null === (i = null == t ? void 0 : t.polygon) || void 0 === i ? void 0 : i.enable) && void 0 !== e ? e : void 0 !== (null === (o = null == t ? void 0 : t.polygon) || void 0 === o ? void 0 : o.type) && t.polygon.type !== Bs.none
            }

            loadOptions(t, i) {
                if (!this.needsPlugin(i)) return;
                const e = t;
                let o = e.polygon;
                void 0 === (null == o ? void 0 : o.load) && (e.polygon = o = new nn), o.load(null == i ? void 0 : i.polygon)
            }
        }

        class un {
            init(t) {
                const i = t.options.roll;
                if (i.enable) if (t.roll = {
                    angle: Math.random() * Math.PI * 2,
                    speed: F(i.speed) / 360
                }, i.backColor) t.backColor = gt(i.backColor); else if (i.darken.enable && i.enlighten.enable) {
                    const e = Math.random() >= .5 ? M.darken : M.enlighten;
                    t.roll.alter = {type: e, value: e === M.darken ? i.darken.value : i.enlighten.value}
                } else i.darken.enable ? t.roll.alter = {
                    type: M.darken,
                    value: i.darken.value
                } : i.enlighten.enable && (t.roll.alter = {
                    type: M.enlighten,
                    value: i.enlighten.value
                }); else t.roll = {angle: 0, speed: 0}
            }

            isEnabled(t) {
                const i = t.options.roll;
                return !t.destroyed && !t.spawning && i.enable
            }

            update(t, i) {
                this.isEnabled(t) && function (t, i) {
                    const e = t.options.roll;
                    if (!t.roll || !e.enable) return;
                    const o = t.roll.speed * i.factor, s = 2 * Math.PI;
                    t.roll.angle += o, t.roll.angle > s && (t.roll.angle -= s)
                }(t, i)
            }
        }

        const pn = new To;
        pn.init();
        const {particlesJS: vn, pJSDom: fn} = (t => {
            const i = (i, e) => t.load(i, e);
            i.load = (i, e, o) => {
                t.loadJSON(i, e).then((t => {
                    t && o(t)
                })).catch((() => {
                    o(void 0)
                }))
            }, i.setOnClickHandler = i => {
                t.setOnClickHandler(i)
            };
            return {particlesJS: i, pJSDom: t.dom()}
        })(pn);
        return async function (t) {
            await ks(t), await async function (t) {
                await t.addInteractor("externalTrail", (t => new Ps(t)))
            }(t), await async function (t) {
                await t.addParticleUpdater("roll", (() => new un))
            }(t), await async function (t) {
                await t.addParticleUpdater("tilt", (t => new Ms(t)))
            }(t), await async function (t) {
                await t.addParticleUpdater("wobble", (t => new zs(t)))
            }(t), await async function (t) {
                const i = new Rs;
                await t.addPlugin(i)
            }(t), await async function (t) {
                const i = new Zs;
                await t.addPlugin(i), t.addEmitterShape || (t.addEmitterShape = (t, i) => {
                    Is.addShape(t, i)
                }), t.addEmitterShape(Fs.circle, new Js), t.addEmitterShape(Fs.square, new Ys)
            }(t), await async function (t) {
                Y() || window.SVGPathSeg || await r.e(917).then(r.t.bind(r, 167, 23));
                const i = new hn;
                await t.addPlugin(i)
            }(t)
        }(pn), l = r.O(l)
    }()
}))
