$(function () {
    languages();
    menu();
    animation();
    ac_menu();
    faq();
    registr();
    login();
    recovery_pass();
    restore_pass();
    reserve();
    cryptocheck();
    last_transactions();
    $('.field input').on('input', function () {
        let e = $(this);
        if (this.value == '') e.removeClass('dirty'); else e.addClass('dirty')
    })
});
animation = function () {
    let w = $(window).width();
    let num = 40;
    if (w < 769) num = 30;
    if (w < 450) num = 20;
    let d = {
        fpsLimit: 60,
        background: {
            color: "#bdf",
            image: "radial-gradient(circle, #fff 0%, #bdf 100%)",
            size: "100% 100%",
            repeat: "no-repeat"
        },
        backgroundMode: {enable: !0},
        interactivity: {resize: !0,},
        particles: {
            number: {value: num,},
            wobble: {distance: 15, enable: !0, speed: 10,},
            rotate: {direction: 'clockwise', path: !0, value: 200, animation: {enable: !0, speed: 1, sync1: !0,}},
            color: {value: ["#005", "#55c"]},
            links: {color: ["#005", "#55c"], enable: !0, distance: 230, width: 2,},
            move: {
                attract: {enable: !1, rotateX: 600, rotateY: 1200},
                noise: {enable: !0, clamp: !0, delay: {value: 4,},},
                distance: 30,
                enable: !0,
                speed: 1,
                random: !0,
            },
            size: {value: 6, animation: {count: 1000, enable: !0, speed: 2, minimumValue: 3, sync: !0,}},
            opacity: {value: 1,}
        }
    }
    tsParticles.load("root", d);
    $(window).resize(function () {
        let w = $(window).width();
        let v2 = 40;
        if (w < 769) v2 = 30;
        if (w < 450) v2 = 20;
        d.particles.number.value = v2;
        tsParticles.load("root", d)
    })
}
cryptocheck = function () {
    let p = $('#crypto_check');
    if (!p.length) return;
    let btn_submit = p.find('.checkButton');
    let $form = p.find('form');
    p.find('#crypto_check_wallet').on('input', function () {
        let e = $(this);
        let v = e.val();
        btn_submit.prop('disabled', v == '')
    });
    p.find('.pm_select .btn').click(function () {
        let ps = $(this).closest('.pm_select');
        let d = ps.find('.dropdown');
        if (ps.hasClass('is-active')) {
            ps.removeClass('is-active');
            d.hide()
        } else {
            let pa = $('.pm_select.is-active');
            if (pa.length) {
                pa.removeClass('is-active').find('.dropdown').hide()
            }
            ps.addClass('is-active');
            d.show()
        }
    });
    p.find('.pm_select').on('click', '.dropdown .option', function () {
        let option = $(this);
        $('.pm_select.is-active').removeClass('is-active').find('.dropdown').hide();
        let pm_select = option.closest('.pm_select');
        let btn = pm_select.find('.btn');
        btn.find('.pm_select_img').html(option.find('.pm_select_img').html());
        btn.find('span').text(option.find('span').text());
        let ac = option.data('name');
        $('#crypto_check_ac span').text(ac);
        p.find('#pm_id').val(option.data('id'))
    });
    $('body').click(function (e) {
        let pa = p.find('.pm_select.is-active');
        if (!pa.length) return;
        if (pa.has(e.target).length === 0) {
            pa.removeClass('is-active').find('.dropdown').hide()
        }
    });
    // $form.submit(function (ev) {
    //     ev.preventDefault();
    //     let data = $form.serialize();
    //     data.ajax = 1;
    //     overlay_add($form);
    //     $.ajax({url: $form.attr('action'), data: data}).always(function () {
    //         overlay_remove($form)
    //     }).done(function (data) {
    //         if (data.error) {
    //             error(data.error)
    //         } else {
    //             mes(data.message)
    //         }
    //     })
    // })
}
// registr = function () {
//     let p = $('.register-page');
//     if (!p.length) return;
//     let $form = p.find('form');
//     $form.submit(function (ev) {
//         ev.preventDefault();
//         if (!$form.valid()) return !1;
//         let data = $form.serialize();
//         overlay_add($form);
//         $.ajax({url: $form.attr('action'), data: data}).always(function () {
//             overlay_remove($form)
//         }).done(function (data) {
//             if (data.error) {
//                 error(data.error)
//             } else {
//                 mes_post(data.message);
//                 redirect(data.redirect)
//             }
//         })
//     })
// }
// reserve = function () {
//     let p = $('#reserve');
//     if (!p.length) return;
//     let tabs = p.find('.reserve_tab');
//     let t = tabs.find('li');
//     let rc = p.find('.reserve_content > div');
//     t.click(function () {
//         let e = $(this);
//         t.removeClass('active');
//         e.addClass('active');
//         let type = e.find('span:first').text().replace(/[^a-z]+/gi, '').toLowerCase();
//         let list = p.find('.tab_' + type);
//         if (list.length) {
//             rc.removeClass('active');
//             list.addClass('active')
//         }
//     })
// }
// login = function () {
//     let p = $('.login-page');
//     if (!p.length) return;
//     let $form = p.find('form');
//     $form.submit(function (ev) {
//         ev.preventDefault();
//         if (!$form.valid()) return !1;
//         let data = $form.serialize();
//         overlay_add($form);
//         $.ajax({url: $form.attr('action'), data: data}).always(function () {
//             overlay_remove($form)
//         }).done(function (data) {
//             if (data.error) {
//                 error(data.error)
//             } else {
//                 redirect(data.redirect)
//             }
//         })
//     })
// }
// recovery_pass = function () {
//     let p = $('.forgot-page');
//     if (!p.length || p.hasClass('restore_pass-page')) return;
//     let $form = p.find('form');
//     $form.submit(function (ev) {
//         ev.preventDefault();
//         if (!$form.valid()) return !1;
//         let data = $form.serialize();
//         overlay_add($form);
//         $.ajax({url: $form.attr('action'), data: data}).always(function () {
//             overlay_remove($form)
//         }).done(function (data) {
//             if (data.error) {
//                 error(data.error)
//             } else {
//                 mes_post(data.message);
//                 redirect(data.redirect)
//             }
//         })
//     })
// }
// restore_pass = function () {
//     let p = $('.restore_pass-page');
//     if (!p.length || p.hasClass('restore_pass-page')) return;
//     let $form = p.find('form');
//     $form.submit(function (ev) {
//         ev.preventDefault();
//         if (!$form.valid()) return !1;
//         let data = $form.serialize();
//         overlay_add($form);
//         $.ajax({url: $form.attr('action'), data: data}).always(function () {
//             overlay_remove($form)
//         }).done(function (data) {
//             if (data.error) {
//                 error(data.error)
//             } else {
//                 mes(data.message);
//                 $form.trigger('reset');
//                 $form.find('.is-valid').removeClass('is-valid')
//             }
//         })
//     })
// }
ac_menu = function () {
    let am = $('.account-menu');
    if (!am.length) return;
    am.click(function (ev) {
        ev.preventDefault();
        $('.account-menu-list').toggle()
    })
}
faq = function () {
    let faq = $('.faq');
    if (!faq.length) return;
    faq.find('.item__title').click(function () {
        let i = $(this).parent();
        if (i.hasClass('active')) {
            i.removeClass('active');
            return
        }
        faq.find('.item.active').removeClass('active');
        i.addClass('active')
    })
}
languages = function () {
    let lang_select = $('.lang_select .header');
    let lang_list = $('.lang_list');
    let list = $('#lang_list');
    let list_menu = $('.lang_select .lang_list');
    lang_select.click(function (ev) {
        ev.preventDefault();
        if (lang_list.is(':visible')) {
            lang_list.hide()
        } else {
            lang_list.show()
        }
    });
    $('body').click(function (e) {
        if (lang_select.has(e.target).length === 0 && lang_list.is(':visible')) {
            lang_list.hide()
        }
    })
}
menu = function () {
    let menu_top = $('.menu-top');
    if (!menu_top.length) return;
    let menu_top_ul = menu_top.find('.menu');
    let scrollY = 0;
    menu_top.find('.close').click(function () {
        menu_top.removeClass('animate__fadeInLeft').addClass('animate__fadeOutLeft');
        setTimeout(function () {
            document.body.style.position = '';
            document.body.style.top = '';
            $('body,html').scrollTop(parseInt(scrollY || '0'))
        }, 500)
    });
    $('.menu-top-burger').click(function () {
        menu_top.removeClass('animate__fadeOutLeft').addClass('animate__fadeInLeft');
        menu_top.show();
        menu_top_ul.scrollTop(0);
        setTimeout(function () {
            scrollY = window.scrollY;
            document.body.style.position = 'fixed';
            document.body.style.top = '-' + scrollY + 'px'
        }, 500)
    })
}
tickets_chat_update = function () {
    let chat = $('#ticket_chat');
    let $form = $('#ticket_chat form');
    let last_id_message = chat.find('.ticket_messages .direct-msg:last').data('id');
    $.ajax({url: $form.prop('action'), data: {update: 1, ajax: 1}}).done(function (data) {
        if (data.chat) {
            if (typeof data.ticket_closed !== 'undefined' && data.ticket_closed) {
                location.reload();
                return
            }
            if (data.mes_last_id != last_id_message) {
                chat.find('.ticket_messages').html(data.chat + '<div class="ovl"></div>');
                ticket_chat_scroll()
            }
            setTimeout(tickets_chat_update, 5000)
        }
    })
}
ticket_chat_scroll = function () {
    let chat_messages = $('#ticket_chat .ticket_messages');
    chat_messages.scrollTop(chat_messages.prop('scrollHeight'))
}
tickets = function () {
    let ticket_new = $('#ticket_new');
    if (ticket_new.length) {
        ticket_new.submit(function () {
            if (!ticket_new.valid()) return !1;
            return !0
        })
    }
    let chat = $('#ticket_chat');
    if (!chat.length) return;
    ticket_chat_scroll();
    let $form = $('#ticket_chat form');
    let $textarea = $('#ticket_chat textarea');
    $('.chat_send__button_small span').click(function () {
        $form.submit()
    });
    $form.submit(function () {
        if (!$form.valid()) return !1;
        let message = String($textarea.val().trim());
        let data = $form.serialize();
        $.ajax({url: $form.prop('action'), data: data}).done(function (data) {
            if (data.error) {
                error(data.error)
            } else {
                $textarea.val('');
                mes(data.message);
                chat.find('.ticket_messages').html(data.chat + '<div class="ovl"></div>');
                ticket_chat_scroll()
            }
        });
        return !1
    });
    $textarea.keydown(function (ev) {
        if (ev.ctrlKey && ev.keyCode == 13) {
            $form.submit()
        }
    });
    if (!$form.find('button').prop('disabled')) {
        setTimeout(tickets_chat_update, 5000)
    }
}
last_transactions_refresh = function () {
    let num_show = parseInt($('#last_transactions').data('num_show'));
    let tr0 = $('#last_transactions .table-lt-body .tr:first');
    let na = $('#last_transactions .table-lt-body .tr:not(.added)');
    let time = tr0.data('time');
    $.ajax({url: '/ajax/last_transactions/', method: 'post', data: {time: time}}).done(function (data) {
        if (data.error) {
            error(data.error)
        } else {
            if (data.list.length) {
                let html = '', v;
                for (var x in data.list) {
                    v = data.list[x];
                    html += '<div class="tr animate__animated animate__backInUp animate__fast added" style="display: none;" data-time="' + v.time + '"><div class="td"><label>' + lang.time + '</label><span class="date">' + v.time_name + '</span></div><div class="td"><div class="exch"><span class="nw"><img src="/img/pm2/' + v.img_from + '" class="coin protip" data-pt-title="' + v.name_from + '" alt="">' + v.coin_from + '</span><span class="icon-arrow-2"></span><span class="nw"><img src="/img/pm2/' + v.img_to + '" class="coin protip" data-pt-title="' + v.name_to + '" alt="">' + v.coin_to + '</span></div></div><div class="td"><label>' + lang.quantity + '</label>' + v.q + '</div></div>'
                }
                $('#last_transactions .table-lt-body').prepend(html);
                let h = tr0.height() + parseInt(tr0.css('margin-bottom'));
                h += $(window).width() > 767 ? 10 : 30;
                h *= data.list.length;
                tr0.animate({'marginTop': h + 'px'}, 200, 'linear', function () {
                    tr0.css('marginTop', '0');
                    let tr = $('#last_transactions .table-lt-body .tr.added');
                    tr.show();
                    setTimeout(function () {
                        tr.removeClass('added')
                    }, 800);
                    let size = $('#last_transactions .table-lt-body .tr').length;
                    let del = size - num_show;
                    if (del > 0) {
                        del = size - del - 1;
                        $('#last_transactions .table-lt-body .tr:gt(' + del + ')').remove()
                    }
                })
            }
            if (data.time_names) {
                na.each(function (i, e) {
                    e = $(e);
                    let t = parseInt(e.data('time'));
                    if (typeof data.time_names[t] !== 'undefined') {
                        e.find('.date').text(data.time_names[t])
                    }
                })
            }
        }
    }).fail(function () {
        show_ajax_error = !1
    });
    setTimeout('last_transactions_refresh()', 5000)
}
last_transactions = function () {
    if (!$('#last_transactions').length) return;
    setTimeout('last_transactions_refresh()', 5000)
}
