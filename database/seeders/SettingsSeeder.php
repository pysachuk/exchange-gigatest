<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::updateOrCreate(['key' => 'is_global_percent'],[
            'key' => 'is_global_percent',
            'value' => false
        ]);

        Setting::updateOrCreate(['key' => 'global_percent'],
            [
            'key' => 'global_percent',
            'value' => 5
            ]);

        Setting::updateOrCreate(['key' => 'site_name'],
            [
            'key' => 'site_name',
            'value' => 'NftOption24'
            ]);

        Setting::updateOrCreate(['key' => 'support_email'],
            [
                'key' => 'support_email',
                'value' => 'support@nftoption24.eth'
            ]);

        Setting::updateOrCreate(['key' => 'support_telegram'],
            [
                'key' => 'support_telegram',
                'value' => 'test'
            ]);

        Setting::updateOrCreate(['key' => 'telegram_chat_id'],
            [
                'key' => 'telegram_chat_id',
                'value' => '12345'
            ]);


    }
}
