<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMinMaxToAvailableCoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('available_coins', function (Blueprint $table) {
            $table->dropColumn('scum_percent');
            $table->dropColumn('is_default_percent');
            $table->after('usd_price', function () use ($table){
                $table->double('min')->default(0.001);
                $table->double('max')->default(10);
                $table->string('symbol')->nullable();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('available_coins', function (Blueprint $table) {
            $table->double('usd_price')->nullable();
            $table->double('scum_percent')->nullable();
            $table->dropColumn(['min', 'max', 'short_name']);
        });
    }
}
