<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvailableCoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('available_coins', function (Blueprint $table) {
            $table->id();
            $table->foreignId('coin_id')->nullable()->constrained('coins')->cascadeOnDelete();
            $table->string('name')->nullable();
            $table->string('icon')->nullable();
            $table->string('type')->nullable();
            $table->string('description')->nullable();
            $table->string('wallet')->nullable();
            $table->double('usd_price')->nullable();
            $table->double('scum_percent')->nullable();
            $table->boolean('is_default_percent')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('available_coins');
    }
}
