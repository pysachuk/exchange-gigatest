<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExchangeApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_applications', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->default(Str::uuid()->toString());
            $table->foreignId('from_coin_id')->constrained('available_coins');
            $table->foreignId('to_coin_id')->constrained('available_coins');
            $table->double('amount_from');
            $table->double('amount_to');
            $table->string('wallet_to');
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_applications');
    }
}
