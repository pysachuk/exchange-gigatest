<div>
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <h4>Список валют</h4>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <input wire:model="search" type="text" class="form-control" id="search"  placeholder="Search...">
                        </div>
                    </div>
                    <div class="col-4 mt-1">
                        <button wire:click="showCreateModal" class="btn btn-success">Добавить</button>
                    </div>
                </div>

            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Иконка</th>
                        <th scope="col">Название</th>
                        <th scope="col">type</th>
                        <th scope="col">Описание</th>
                        <th scope="col">Кошелек</th>
                        <th scope="col">Цена в USD</th>
                        <th scope="col">MIN</th>
                        <th scope="col">MAX</th>
                        <th scope="col">Управление</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__empty_1 = true; $__currentLoopData = $coins; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coin): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <tr>
                            <th scope="row"><?php echo e($coin->id); ?></th>
                            <td><img src="/<?php echo e($coin->icon); ?>" width="30" alt=""></td>
                            <td><?php echo e($coin->name); ?></td>
                            <td><?php echo e($coin->type); ?></td>
                            <td>
                                RU:<?php echo e($coin->translate('ru')->description ?? ''); ?>

                                <br>
                                EN:<?php echo e($coin->translate('en')->description ?? ''); ?>

                                <br>
                                PL:<?php echo e($coin->translate('pl')->description ?? ''); ?>

                            </td>
                            <td><?php echo e($coin->wallet); ?></td>
                            <td><?php echo e($coin->usd_price); ?></td>
                            <td><?php echo e($coin->min); ?></td>
                            <td><?php echo e($coin->max); ?></td>
                            <td class="text-center">
                                <button wire:click="editCoin(<?php echo e($coin->id); ?>)" class="btn btn-primary btn-sm">
                                    <i class="fas fa-edit"></i>
                                </button>
                                <button wire:click="removeConfirmation(<?php echo e($coin->id); ?>)" class="btn btn-danger btn-sm">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <tr>
                            <td colspan="8" class="text-center">Пусто</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <?php echo e($coins->links()); ?>

        </div>
    </div>
    <div class="modal" <?php if($showCreateModal): ?> style="display:block" <?php endif; ?>>
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Добавить валюту</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" wire:click="closeCreateModal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php if($errors->any()): ?>
                                <div class="row">
                                    <div class="col-12">
                                        <ul class="list-group bg-danger">
                                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li class="list-group-item"><?php echo e($error); ?></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div>
                                </div>

                            <?php endif; ?>
                            <div class="row">
                                <div class="form-group">
                                    <h4>coin_id</h4>
                                    <label for="coinId"></label>
                                    <input wire:model="newCoin.coin_id" type="text" class="form-control <?php $__errorArgs = ['newCoin.coin_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="coinId" placeholder="Coin Id">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="exampleFormControlFile9991">Иконка</label>
                                    <input wire:model="newCoin.icon" type="file" class="form-control-file" id="exampleFormControlFile9991">
                                    <?php if(isset($this->newCoin['icon'])): ?>
                                        <br>
                                        <img src="<?php echo e($this->newCoin['icon']->temporaryUrl()); ?>" alt="" width="100" height="100">
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <h4>Название</h4>
                                    <label for="name"></label>
                                    <input wire:model="newCoin.name" type="text" class="form-control <?php $__errorArgs = ['newCoin.name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="name" placeholder="Coin Id">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <h4>Тип</h4>
                                    <select wire:model="newCoin.type" class="custom-select <?php $__errorArgs = ['newCoin.type'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="inputGroupSelect0123">
                                        <option value="coin">Криптовалюта</option>
                                        <option value="card">Карта</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <h4>Описание RU</h4>
                                    <label for="description_ru"></label>
                                    <input wire:model="newCoin.ru.description" type="text" class="form-control <?php $__errorArgs = ['newCoin.ru.description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="description_ru" placeholder="Описание RU">
                                </div>
                            </div>
                                <div class="row">
                                    <div class="form-group">
                                        <h4>Описание PL</h4>
                                        <label for="description_pl"></label>
                                        <input wire:model="newCoin.pl.description" type="text" class="form-control <?php $__errorArgs = ['newCoin.pl.description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="description_pl" placeholder="Описание PL">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <h4>Описание EN</h4>
                                        <label for="description_en"></label>
                                        <input wire:model="newCoin.en.description" type="text" class="form-control <?php $__errorArgs = ['newCoin.en.description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="description_en" placeholder="Описание EN">
                                    </div>
                                </div>
                            <div class="row">
                                <div class="form-group">
                                    <h4>Кошелек</h4>
                                    <label for="wallet"></label>
                                    <input wire:model="newCoin.wallet" type="text" class="form-control <?php $__errorArgs = ['newCoin.wallet'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="wallet" placeholder="Кошелек">
                                </div>
                            </div>









                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" wire:click="createCoin">Добавить</button>
                    <button type="button" class="btn btn-secondary" wire:click="closeCreateModal" data-dismiss="modal">Закрыть
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" <?php if($showEditModal): ?> style="display:block" <?php endif; ?>>
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Редактировать</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" wire:click="closeEditModal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php if($errors->any()): ?>
                                <div class="row">
                                    <div class="col-12">
                                        <ul class="list-group bg-danger">
                                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li class="list-group-item"><?php echo e($error); ?></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div>
                                </div>

                            <?php endif; ?>
                            <div class="row">
                                <div class="form-group">
                                    <h4>Название</h4>
                                    <label for="name"></label>
                                    <input wire:model="editCoin.name" type="text" class="form-control <?php $__errorArgs = ['editCoin.name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="name" placeholder="Название">
                                </div>
                            </div>
                                <div class="row">
                                    <div class="form-group">
                                        <h4>Описание RU</h4>
                                        <label for="editCoin_description_ru"></label>
                                        <input wire:model="editCoin.ru.description" type="text" class="form-control <?php $__errorArgs = ['editCoin.ru.description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="editCoin_description_ru" placeholder="Описание RU">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <h4>Описание PL</h4>
                                        <label for="editCoin_description_pl"></label>
                                        <input wire:model="editCoin.pl.description" type="text" class="form-control <?php $__errorArgs = ['editCoin.pl.description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="editCoin_description_pl" placeholder="Описание PL">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <h4>Описание EN</h4>
                                        <label for="editCoin_description_en"></label>
                                        <input wire:model="editCoin.en.description" type="text" class="form-control <?php $__errorArgs = ['editCoin.en.description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="editCoin_description_en" placeholder="Описание EN">
                                    </div>
                                </div>
                            <div class="row">
                                <div class="form-group">
                                    <h4>Кошелек</h4>
                                    <label for="wallet"></label>
                                    <input wire:model="editCoin.wallet" type="text" class="form-control <?php $__errorArgs = ['editCoin.wallet'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="wallet" placeholder="Кошелек">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <h4>Min</h4>
                                    <label for="min"></label>
                                    <input wire:model="editCoin.min" type="text" class="form-control <?php $__errorArgs = ['editCoin.min'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="min" placeholder="Min">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <h4>Max</h4>
                                    <label for="max"></label>
                                    <input wire:model="editCoin.max" type="text" class="form-control <?php $__errorArgs = ['editCoin.max'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="min" placeholder="Max">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" wire:click="saveEditCoin">Сохранить</button>
                    <button type="button" class="btn btn-secondary" wire:click="closeEditModal" data-dismiss="modal">Закрыть
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php /**PATH /Users/admin/projects/crypto2/resources/views/livewire/admin/coins/available-coins.blade.php ENDPATH**/ ?>