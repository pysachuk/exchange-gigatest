<div>
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <h4>Список валют</h4>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <input wire:model="search" type="text" class="form-control" id="search"  placeholder="Search...">
                        </div>
                    </div>
                </div>

            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">id</th>
                            <th scope="col">name</th>
                            <th scope="col">coin_id</th>
                            <th scope="col">symbol</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php $__empty_1 = true; $__currentLoopData = $coins; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coin): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <tr>
                            <th scope="row"><?php echo e($coin->id); ?></th>
                            <td><?php echo e($coin->name); ?></td>
                            <td><?php echo e($coin->coin_id); ?></td>
                            <td><?php echo e($coin->symbol); ?></td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                            <tr>
                                <td colspan="4">Пусто</td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <?php echo e($coins->links()); ?>

        </div>
    </div>
</div>
<?php /**PATH /Users/admin/projects/crypto2/resources/views/livewire/admin/coins/all.blade.php ENDPATH**/ ?>