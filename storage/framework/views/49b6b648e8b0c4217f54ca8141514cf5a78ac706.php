<div>












    <form id="trade">
        <div class="overlay"></div>
        <div class="trade_in">

            <div class="trade-part part-1">
                <h2 class="text-header"><?php echo e(__('site.give')); ?></h2>
                <div class="pm_select send">
                    <div class="btn">
                        <div class="pm_select_img"><img src="<?php echo e($coinFrom->icon); ?>" alt="pm"></div>
                        <span><?php echo e($coinFrom->name); ?></span>
                        <svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7 10l5 5 5-5H7z" fill="#000"></path></svg>
                    </div>
                    <div class="dropdown">
                        <div class="dropdown_inner">
                            <?php $__empty_1 = true; $__currentLoopData = $availableCoinsFrom; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coin): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                <div wire:click="selectCoinFrom(<?php echo e($coin->id); ?>)" class="option" data-id="9" data-cur="BTC" id="send_option_9">
                                    <div class="pm_select_img"><img src="<?php echo e($coin->icon); ?>" alt="pm"></div><span><?php echo e($coin->name); ?></span>
                                    <div class="pm_childs">
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="currency_select">
                    <a class="pm_select_child active" href="#" data-child_id="9" data-cur="BTC"><?php echo e($coinFrom->symbol); ?></a>
                </div>
                <div class="amount_input">
                    <div class="input-1 ">
                        <input wire:model="fromData.amountFrom" type="text" inputmode="numeric" name="amount_send" id="amount_send" class="dirty amountFrom <?php $__errorArgs = ['fromData.amountFrom'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" maxlength="20">
                        <?php $__errorArgs = ['fromData.amountFrom'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <span class="invalid-feedback" role="alert">
                            <strong><?php echo e($message); ?></strong>
                        </span>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <div class="addons"><?php echo e($coinFrom->symbol); ?></div>
                    </div>
                    <div class="input-1 ">
                        <input wire:model="fromData.amountFromUsd" type="text" inputmode="numeric" name="amount_send_usd" id="amount_send_usd" class="dirty amountFrom <?php $__errorArgs = ['fromData.amountFromUsd'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" maxlength="20">
                        <?php $__errorArgs = ['fromData.amountFromUsd'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <span class="invalid-feedback" role="alert">
                            <strong><?php echo e($message); ?></strong>
                        </span>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <div class="addons">$</div>
                    </div>
                </div>
                <aside>
                    <div class="wrapper">
                        <p class="range">
                            <span>Min: <span class="send_min"><?php echo e($coinFrom->min); ?> <?php echo e($coinFrom->symbol); ?></span></span>
                            <span>Max: <span class="send_max"><?php echo e($coinFrom->max); ?> <?php echo e($coinFrom->symbol); ?></span></span>
                            <br>
                            <span>1 <?php echo e($coinFrom->symbol); ?> = <?php echo e($coinFrom->usd_price); ?> USD</span>
                        </p>
                        <p class="rate_info"></p>
                    </div>
                </aside>
            </div>

            <div class="trade-part part-2">
                <h4 class="text-header-2"><?php echo e(__('exchange.requisites_sender')); ?></h4>
                <div class="form-group">
                    <div class="field">
                        <input wire:model="fromData.email" class="dirty <?php $__errorArgs = ['fromData.email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="email">
                        <div class="placeholder">Email</div>
                        <?php $__errorArgs = ['fromData.email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <span class="invalid-feedback" role="alert">
                            <strong><?php echo e($message); ?></strong>
                        </span>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                    </div>
                </div>












            </div>

            <div class="trade-part__separator"></div>

            <div class="trade-part part-3">
                <h2 class="text-header"><?php echo e(__('site.receive')); ?></h2>
                <div class="pm_select get">
                    <div class="btn">
                        <div class="pm_select_img"><img src="<?php echo e($coinTo->icon); ?>" alt="pm"></div>
                        <span><?php echo e($coinTo->name); ?></span>
                        <svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7 10l5 5 5-5H7z" fill="#000"></path></svg>
                    </div>
                    <div class="dropdown">
                        <div class="dropdown_inner">
                            <?php $__empty_1 = true; $__currentLoopData = $availableCoinsTo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $coin): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                <div wire:click="selectCoinTo(<?php echo e($coin->id); ?>)" class="option" data-id="9" data-cur="BTC" id="send_option_9">
                                    <div class="pm_select_img"><img src="<?php echo e($coin->icon); ?>" alt="pm"></div><span><?php echo e($coin->name); ?></span>
                                    <div class="pm_childs">
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="currency_select">
                    <a class="pm_select_child active" href="#" data-child_id="3" data-cur="RUB"><?php echo e($coinTo->symbol); ?></a>
                </div>
                <div class="amount_input">
                    <div class="input-1">
                        <input wire:model="toData.amountTo" type="text" inputmode="numeric" name="amount_recive" id="amount_get " class="dirty amountTo" maxlength="20">
                        <div class="addons"><?php echo e($coinTo->symbol); ?></div>
                    </div>
                </div>
                <aside class="vertical">
                    <p class="get_reserves"><?php echo e(__('site.reserves')); ?>: <span><?php echo e($coinTo->max); ?> <?php echo e($coinTo->symbol); ?></span></p>
                </aside>
                <div class="exchange_comment">
                    <?php echo e($coinFrom->name); ?> - <?php echo e($coinFrom->description); ?>

                </div>
            </div>
            <div class="trade-part part-4">
                <h4 class="text-header-2"><?php echo e(__('exchange.requisites_recipment')); ?></h4>
                <div class="form-group" id="field_account_recive" style="">
                    <div class="field">
                        <input wire:model="toData.addressTo" class="<?php $__errorArgs = ['toData.addressTo'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" type="text" name="account_recive">
                        <?php $__errorArgs = ['toData.addressTo'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <span class="invalid-feedback" role="alert">
                            <strong><?php echo e($message); ?></strong>
                        </span>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        <div class="placeholder"><?php echo e(__('site.wallet')); ?> <?php echo e($coinTo->name); ?></div>
                    </div>
                </div>
                <div class="trade_submit">
                    <div class="terms"><?php echo e(__('exchange.term')); ?> <a href="<?php echo e(route('terms')); ?>" target="_blank" rel="noreferrer noopener"><?php echo e(__('site.user_agreement')); ?></a>
                        <?php echo e(__('site.our_resource')); ?></div>
                    <button wire:click.prevent="exchange" class="btn-submit"><span> <?php echo e(__('site.exchange')); ?></span></button>
                </div>
            </div>
        </div>
    </form>
    <!--/#trade-->

</div>
<?php $__env->startSection('js'); ?>
    <script>
        $(document).find('.pm_select .btn').click(function () {
            let ps = $(this).closest('.pm_select');
            let d = ps.find('.dropdown');
            if (ps.hasClass('is-active')) {
                ps.removeClass('is-active');
                d.hide()
            } else {
                let pa = $('.pm_select.is-active');
                if (pa.length) {
                    pa.removeClass('is-active').find('.dropdown').hide()
                }
                ps.addClass('is-active');
                d.show()
            }
        });
    </script>
    <script>
        $('.amountFrom').on('input', function() {
            this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');
        });

        $('.amountTo').on('input', function() {
            this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');
        });
    </script>
<?php $__env->stopSection(); ?>



<?php /**PATH /Users/admin/projects/crypto2/resources/views/livewire/exchange.blade.php ENDPATH**/ ?>