<?php $__env->startSection('content'); ?>
        <div class="reg_login-page login-page">
            <div class="reg_login-form">
                <h2 class="text-header"><?php echo e(__('site.login')); ?></h2>
                <form method="post" action="<?php echo e(route('login')); ?>" class="form-validate">
                    <div>
                        <?php echo csrf_field(); ?>
                        <div class="field">
                            <input  type="text" name="login" value="<?php echo e(old('login')); ?>" required class="<?php $__errorArgs = ['login'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>">
                            <div class="placeholder"><?php echo e(__('site.login')); ?></div>
                            <?php $__errorArgs = ['login'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span id="login-error" class="is-invalid invalid-feedback"><?php echo e($message); ?></span>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>

                        <div class="field">
                            <input type="password" name="password" required class="<?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>">
                            <div class="placeholder"><?php echo e(__('site.password')); ?></div>
                            <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span id="login-error" class="is-invalid invalid-feedback"><?php echo e($message); ?></span>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>

                    </div>
                    <div>
                        <p class="form_terms">protected by reCAPTCHA<a href="https://policies.google.com/privacy?hl=en" target="_blank" rel="noopener noreferrer">Privacy</a><a target="_blank" rel="noopener noreferrer" href="https://policies.google.com/terms?hl=en">Terms</a></p>
                        <button type="submit" class="btn-submit"><span> <?php echo e(__('site.login')); ?></span></button>
                    </div>
                    <input type="hidden" name="user_auth" value="1">
                </form>
                <p class="enter-button"><?php echo e(__('site.not_registered')); ?> <a href="<?php echo e(route('register')); ?>"><?php echo e(__('site.register')); ?></a></p>
            </div>
            <div class="reg_login-desc">
                <img src="/img/d/account.svg" alt="">
                <?php echo __('site.register_text'); ?>

            </div>
        </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/admin/projects/crypto2/resources/views/auth/login.blade.php ENDPATH**/ ?>