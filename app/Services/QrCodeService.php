<?php

namespace App\Services;

use BaconQrCode\Renderer\Image\EpsImageBackEnd;
use BaconQrCode\Renderer\Image\ImagickImageBackEnd;
use BaconQrCode\Renderer\Image\SvgImageBackEnd;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;

class QrCodeService
{
    public const FORMAT_SVG = 'svg';
    public const FORMAT_PNG = 'png';
    public const FORMAT_ESP = 'esp';

    public function getFormatClass($format = null)
    {
        switch ($format) {
            case self::FORMAT_ESP :
                return new EpsImageBackEnd();
            case self::FORMAT_PNG :
                return new ImagickImageBackEnd();
            default :
                return new SvgImageBackEnd();
        }

    }

    public function createQrCode($text, $directory, $name = null, $size = 400, $format = self::FORMAT_SVG)
    {
//        if(! $name) {
//            $name = \Str::random(10);
//        }
//        $renderer = new ImageRenderer(
//            new RendererStyle($size),
//            $this->getFormatClass($format)
//        );
//        $writer = new Writer($renderer);
//        $writer->writeFile($text, $directory.$name.'.'.$format);
//
//        return $writer;
    }
}
