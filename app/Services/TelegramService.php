<?php

namespace App\Services;

use App\Models\ExchangeApplication;
use App\Models\Setting;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramService
{
    public function newOrderNotification(ExchangeApplication $order)
    {
        $view = view('admin.telegram.new-order', compact('order'))->render();
        $chatId = Setting::get('telegram_chat_id');
        if($chatId) {
            try {
                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $view,
                    'parse_mode' => 'HTML']);
            }
            catch (\Exception $e) {
                \Log::error($e);
            }
        }

    }
}
