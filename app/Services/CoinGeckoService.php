<?php

namespace App\Services;

use Codenixsv\CoinGeckoApi\CoinGeckoClient;
use Illuminate\Support\Facades\Log;

class CoinGeckoService
{
    protected $client;

    public function __construct(CoinGeckoClient $client)
    {
        $this->client = $client;
    }

    public function getPrices(array $ids, array $vsCurrencies)
    {
        dd($this->client->ping());
        $ids = implode(',', $ids);
        $vsCurrencies = implode(',', $vsCurrencies);
        dd($this->client->simple()->getPrice('bitcoin', 'usd'));

        try {
            return $this->client->simple()->getPrice($ids, $vsCurrencies);
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return null;
        }

    }
}
