<?php

namespace App\Console\Commands;

use App\Models\Coin;
use Codenixsv\CoinGeckoApi\CoinGeckoClient;
use Illuminate\Console\Command;

class LoadCoins extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coins:load';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new CoinGeckoClient();
        $coins = $client->coins()->getList();
        foreach ($coins as $coin) {
            Coin::create(['coin_id' => $coin['id'], 'symbol' => $coin['symbol'], 'name' => $coin['name']]);
        }

        return 0;
    }
}
