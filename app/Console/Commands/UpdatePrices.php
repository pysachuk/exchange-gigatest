<?php

namespace App\Console\Commands;

use App\Models\AvailableCoin;
use Codenixsv\CoinGeckoApi\CoinGeckoClient;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class UpdatePrices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'prices:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update available coins prices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $coins = AvailableCoin::whereHas('originalCoin')->get();

        $coinsArray = [];
        $coinsPricesArray = [];

        foreach ($coins as $coin) {
            $coinsPricesArray[] = $coin->originalCoin->coin_id;
            $coinsArray[$coin->originalCoin->coin_id] = $coin;
        }


        $prices = $this->getPrices($coinsPricesArray, ['usd']);

        foreach ($prices as $key => $price) {
            $coinsArray[$key]->update(['usd_price' => $price['usd'] ?? null]);
        }

        return 0;
    }

    public function getPrices(array $ids, array $vsCurrencies)
    {
        $ids = implode(',', $ids);
        $vsCurrencies = implode(',', $vsCurrencies);
        $client = new CoinGeckoClient();

        try {
            return $client->simple()->getPrice($ids, $vsCurrencies);
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return null;
        }

    }
}
