<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExchangeApplication extends Model
{
    use HasFactory;

    public const STATUS_NEW = 'new';
    public const STATUS_CANCELLED = 'cancelled';
    public const STATUS_INSPECTION = 'inspection';
    public const STATUS_PAYED = 'payed';
    public const STATUS_CONFIRMATION = 'confirmation';
    public const STATUS_COMPLETED = 'completed';

    protected $guarded = [];

    protected $casts = [
        'created_at' => 'datetime'
    ];

    public function getIsActualAttribute() :bool
    {
        return now() < $this->created_at->addMinutes(config('app.exchange_time'));
    }


    public function fromCoin()
    {
        return $this->belongsTo(AvailableCoin::class, 'from_coin_id');
    }

    public function toCoin()
    {
        return $this->belongsTo(AvailableCoin::class, 'to_coin_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getStatusNameAttribute()
    {
        return __('order_status.'.$this->status);
    }
}
