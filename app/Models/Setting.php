<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    protected $fillable = [
        'key', 'value'
    ];

    public static function get($key)
    {
        $result = self::where('key', $key)->first();

        return $result ? $result->value : null;
    }

    public static function set($key, $value)
    {
        $result = self::where('key', $key)->first();
        if($result) {
            $result->update(['value' => $value]);
            return true;
        }

        return false;
    }
}
