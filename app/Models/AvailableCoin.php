<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AvailableCoin extends Model
{
    use HasFactory, Translatable;

    protected $translationForeignKey = 'available_coin_id';

    public $translatedAttributes = ['description'];

    protected $guarded = [];

    public function originalCoin()
    {
        return $this->belongsTo(Coin::class, 'coin_id');
    }

    public function getSymbolAttribute()
    {
        return mb_strtoupper($this->originalCoin->symbol);
    }

    public function getWalletQrCodeAttribute()
    {
        if(\Storage::disk('public')->exists('qr_codes/'.$this->wallet.'.svg')) {
            return '/storage/qr_codes/'.$this->wallet.'.svg';
        }

        return null;
    }
}
