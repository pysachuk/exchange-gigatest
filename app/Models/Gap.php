<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gap extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function coinFrom()
    {
        return $this->belongsTo(AvailableCoin::class, 'coin_from_id');
    }

    public function coinTo()
    {
        return $this->belongsTo(AvailableCoin::class, 'coin_to_id');
    }

}
