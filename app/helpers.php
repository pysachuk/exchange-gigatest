<?php
if (! function_exists('getSetting')) {
    function getSetting($key) {
        return \App\Models\Setting::get($key);
    }
}
if (! function_exists('setSetting')) {
    function setSetting($key, $value) {
        return \App\Models\Setting::set($key, $value);
    }
}
