<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class FaqRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        foreach(localization()->getSupportedLocalesKeys() as $locale) {
            $rules[$locale.'.question'] = ['nullable', 'sometimes', 'string'];
            $rules[$locale.'.answer'] = ['nullable', 'sometimes', 'string'];
        }

        return $rules;
    }
}
