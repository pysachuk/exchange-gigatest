<?php

namespace App\Http\Livewire\Admin\Coins;

use App\Http\Livewire\Traits\HasSwal;
use App\Models\AvailableCoin;
use App\Models\Coin;
use App\Models\ExchangeApplication;
use App\Models\Gap;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class AvailableCoins extends Component
{
    use WithPagination, HasSwal, WithFileUploads;

    protected $paginationTheme = 'bootstrap';

    protected $listeners = [
        'removeAvailableCoin'
    ];

    public $showCreateModal = false;

    public $newCoin = [];

    public $search;

    public $showEditModal = false;

    public $editCoin = [];

    public function mount()
    {
        $this->newCoin['type'] = 'coin';
    }

    public function getAvailableCoins()
    {
        $query = AvailableCoin::query();

        if($this->search) {
            $query = $query->where('name', 'LIKE', "%$this->search%");
        }

        return $query->paginate(50);
    }

    public function closeCreateModal()
    {
        $this->showCreateModal = false;
    }

    public function showCreateModal()
    {
        $this->showCreateModal = true;
    }

    public function removeConfirmation($id)
    {
        $this->showSwalConfirm('error', 'Вы уверены что хотите удалить монету?', 'removeAvailableCoin', $id);
    }

    public function removeAvailableCoin(AvailableCoin $availableCoin)
    {
        Gap::where('coin_from_id', $availableCoin->id)->orWhere('coin_to_id', $availableCoin->id)->delete();
        ExchangeApplication::where('from_coin_id', $availableCoin->id)->orWhere('to_coin_id', $availableCoin->id)->delete();
        $availableCoin->translations()->delete();
        $availableCoin->delete();
    }

    protected function getRules()
    {
        return [
            'newCoin.coin_id' => ['nullable', 'exists:coins,id'],
            'newCoin.icon' => ['required', 'image'],
            'newCoin.name' => ['required', 'string', 'min:3', 'max:190'],
            'newCoin.type' => ['required', 'in:card,coin'],
            'newCoin.ru.description' => ['required', 'string', 'min:3', 'max:190'],
            'newCoin.en.description' => ['required', 'string', 'min:3', 'max:190'],
            'newCoin.pl.description' => ['required', 'string', 'min:3', 'max:190'],
            'newCoin.wallet' => ['required', 'string', 'min:3', 'max:190'],

        ];
    }

    public function createCoin()
    {
        $data = $this->validate() ? $this->validate()['newCoin'] : null;
        $iconPath = $data['icon']->store('public/icons');
        $iconPath = str_replace('public/icons', 'storage/icons', $iconPath);
        $data['icon'] = $iconPath;

        if( AvailableCoin::create($data) ) {

            $this->resetNewCoin();
            $this->closeCreateModal();
            Artisan::call('prices:update');
            $this->showSwalToast('success', 'Валюта успешно добавлена');
        } else {
            $this->showSwalToast('danger', 'Ошибка добавления');
        }


    }

    protected function resetNewCoin()
    {
        $this->newCoin = [];
        $this->newCoin['type'] = 'coin';
    }

    public function editCoin($id)
    {
        $coin = AvailableCoin::findOrFail($id);
        $this->editCoin['id'] = $coin->id;
        $this->editCoin['name'] = $coin->name;
        $this->editCoin['wallet'] = $coin->wallet;
        $this->editCoin['min'] = $coin->min;
        $this->editCoin['max'] = $coin->max;
        $this->editCoin['ru']['description'] = $coin->translate('ru')->description ?? '';
        $this->editCoin['pl']['description'] = $coin->translate('pl')->description ?? '';
        $this->editCoin['en']['description'] = $coin->translate('en')->description ?? '';
        $this->openEditModal();

    }

    public function openEditModal()
    {
        $this->showEditModal = true;
    }

    public function closeEditModal()
    {
        $this->editCoin = [];
        $this->showEditModal = false;
    }

    protected function getEditCoinRules()
    {
        return [
            'editCoin.id' => ['required', 'exists:available_coins,id'],
            'editCoin.name' => ['required', 'string', 'min:3', 'max:190'],
            'editCoin.wallet' => ['required', 'string', 'min:3', 'max:190'],
            'editCoin.min' => ['required', 'between:0.0001,1000'],
            'editCoin.max' => ['required', 'between:0.0001,1000'],
            'editCoin.ru.description' => ['required', 'string', 'min:3', 'max:190'],
            'editCoin.en.description' => ['required', 'string', 'min:3', 'max:190'],
            'editCoin.pl.description' => ['required', 'string', 'min:3', 'max:190'],
        ];
    }

    public function saveEditCoin()
    {
        $data = $this->validate($this->getEditCoinRules());
        $data = $data['editCoin'];
        $coin = AvailableCoin::findOrFail($this->editCoin['id']);
        $coin->update($data);
        $this->closeEditModal();
        $this->showSwalToast('success', 'Сохранено');
    }

    public function render()
    {
        $coins = $this->getAvailableCoins();

        return view('livewire.admin.coins.available-coins', compact('coins'))
            ->extends('admin.layouts.main')
            ->section('content');
    }
}
