<?php

namespace App\Http\Livewire\Admin\Coins;

use App\Models\Coin;
use Livewire\Component;
use Livewire\WithPagination;

class All extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search;

    public function getCoins()
    {
        $query = Coin::query();
        if($this->search) {
            $query->where('name', 'LIKE', "%$this->search%");
        }

        return $query->paginate(50);
    }

    public function render()
    {
        $coins = $this->getCoins();
        return view('livewire.admin.coins.all', compact('coins'))
            ->extends('admin.layouts.main')
            ->section('content');
    }
}
