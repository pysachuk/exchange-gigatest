<?php

namespace App\Http\Livewire\Admin\Coins;

use App\Http\Livewire\Traits\HasSwal;
use App\Models\AvailableCoin;
use App\Models\Gap;
use Livewire\Component;
use Livewire\WithPagination;

class Gaps extends Component
{
    use WithPagination, HasSwal;

    protected $listeners = ['deleteGap'];

    public $availableCoins;

    public $newGap = [];
    public $editGap;

    public $showCreateModal = false;
    public $showEditModal = false;

    protected $paginationTheme = 'bootstrap';

    public function mount()
    {
        $this->availableCoins = $this->getAvailableCoins();
        $this->newGap['coin_from_id'] = $this->availableCoins->first()->id ?? null;
        $this->newGap['coin_to_id'] = $this->availableCoins->last()->id;
    }

    public function showEditModal(Gap $gap)
    {
        $this->editGap['id'] = $gap->id;
        $this->editGap['percent'] = $gap->percent;
        $this->showEditModal = true;
    }

    public function closeEditModal()
    {
        $this->showEditModal = false;
        $this->editGap = null;

    }

    protected function resetEditGap()
    {
        $this->editGap = null;
        $this->closeEditModal();
    }

    public function saveEditGap()
    {
        if($this->editGap['id']) {
            Gap::findOrFail($this->editGap['id'])->update(['percent' => $this->editGap['percent']]);
            $this->resetEditGap();
            $this->showSwalToast('success', 'Успешно отредактировано');
        }
    }

    public function deleteConfirmation($id)
    {
        $this->showSwalConfirm('error', 'Вы уверены?', 'deleteGap', $id);
    }

    public function deleteGap(Gap $gap)
    {
        $gap->delete();
        $this->showSwalToast('success', 'Удалено');
    }

    public function showCreateModal()
    {
        $this->showCreateModal = true;
    }

    public function closeCreateModal()
    {
        $this->showCreateModal = false;
    }

    protected function getNewGapRules()
    {
        return [
            'newGap.coin_from_id' => ['required', 'exists:available_coins,id'],
            'newGap.coin_to_id' => ['required', 'exists:available_coins,id'],
            'newGap.percent' => ['required', 'between:1,50'],
        ];
    }

    protected function resetNewGap()
    {
        $this->newGap['coin_from_id'] = $this->availableCoins->first()->id ?? null;
        $this->newGap['coin_to_id'] = $this->availableCoins->last()->id;
        $this->newGap['percent'] = null;
    }

    public function saveNewGap()
    {
        $this->validate($this->getNewGapRules());

        Gap::updateOrCreate(
            ['coin_from_id' => $this->newGap['coin_from_id'], 'coin_to_id' => $this->newGap['coin_to_id']],
            ['percent' => $this->newGap['percent']]
        );

        $this->resetNewGap();
        $this->closeCreateModal();
        $this->showSwalToast('success', 'Обновлено');


    }

    protected function getGaps()
    {
        return Gap::paginate(50);
    }

    protected function getAvailableCoins()
    {
        return AvailableCoin::with('originalCoin')->get();
    }

    public function render()
    {
        $gaps = $this->getGaps();

        return view('livewire.admin.coins.gaps', compact('gaps'))
            ->extends('admin.layouts.main')
            ->section('content');
    }
}
