<?php

namespace App\Http\Livewire\Admin;

use App\Http\Livewire\Traits\HasSwal;
use App\Models\ExchangeApplication;
use Livewire\Component;
use Livewire\WithPagination;
use function Sodium\compare;

class ExchangeApplicationsList extends Component
{
    use WithPagination, HasSwal;

    protected $paginationTheme = 'bootstrap';

    protected function getApplications()
    {
        return ExchangeApplication::orderBy('created_at', 'DESC')->paginate(20);
    }

    public function viewOrder($id)
    {
        return redirect()->route('admin.orders.view', ['id' => $id]);
    }

    public function render()
    {
        $applications = $this->getApplications();

        return view('livewire.admin.exchange-applications-list', compact('applications'))
            ->extends('admin.layouts.main')
            ->section('content');
    }
}
