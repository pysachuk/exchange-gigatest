<?php

namespace App\Http\Livewire\Admin;

use App\Http\Livewire\Traits\HasSwal;
use Illuminate\Support\Facades\Artisan;
use Livewire\Component;
use Livewire\WithPagination;
use Spatie\TranslationLoader\LanguageLine;

class LocalizationList extends Component
{
    use WithPagination, HasSwal;

    protected $listeners = [
        'deleteLine'
    ];

    protected $paginationTheme = 'bootstrap';
    protected $rules = [
//        'editLine.group' => 'required|string',
//        'editLine.key' => 'required|string',
        'editLine.text.en' => 'required|string',
        'editLine.text.ru' => 'required|string',
        'editLine.text.pl' => 'required|string',
    ];

    public $search;
    public $newLine = [];
    public $showModal;
    public $editLine;

    public function editLine(LanguageLine $line)
    {
        $this->editLine = $line;
        $this->showModal = true;
    }

    public function saveEditLine()
    {
        $this->validate();
        $this->editLine->save();
        $this->showModal = false;
    }

    public function closeModal()
    {
        $this->showModal = false;
    }

    public function saveLine()
    {
        LanguageLine::create($this->newLine);
        $this->newLine = [];
        $this->render();
    }

    public function deleteLineConfirmation($id)
    {
        $this->showSwalConfirm('error', 'Ви впевнені що хочете видалити переклад?', 'deleteLine', $id);
    }

    public function deleteLine(LanguageLine $line)
    {
        $line->delete();
        $this->render();
    }

    public function refreshSeeder()
    {
        if(app()->environment() !== 'production') {
            Artisan::call('translates:create');
            $this->dispatchBrowserEvent('swal:toast', [
                'type' => 'success',
                'text' => 'Seeder успішно оновлено'
            ]);
        } else {
            $this->dispatchBrowserEvent('swal:toast', [
                'type' => 'error',
                'text' => 'Error. App is production mode!'
            ]);
        }


    }

    public function render()
    {
        $search = $this->search;
        $lines = LanguageLine::where(function ($q) use ($search) {
            return $q->where('key', 'LIKE',   "%$search%")
                ->orWhere('group', 'LIKE', "%$search%");
        })->orWhereIn('text', ["$search"])
            ->orderByDesc('created_at')
            ->paginate(20);

        return view('livewire.admin.localization-list', compact('lines'))
            ->extends('admin.layouts.main')
            ->section('content');
    }
}
