<?php

namespace App\Http\Livewire\Admin;

use App\Http\Livewire\Traits\HasSwal;
use App\Models\Setting;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Settings extends Component
{
    use HasSwal;

    public $siteName;
    public $isGlobalPercent;
    public $globalPercent;
    public $supportEmail;
    public $supportTelegram;
    public $newPassword;
    public $telegramChatId;

    public function mount()
    {
        $this->siteName = Setting::get('site_name');
        $this->isGlobalPercent = (bool)Setting::get('is_global_percent');
        $this->globalPercent = Setting::get('global_percent');
        $this->supportEmail = Setting::get('support_email');
        $this->supportTelegram = Setting::get('support_telegram');
        $this->telegramChatId = Setting::get('telegram_chat_id');
    }

    public function getRules()
    {
        return [
            'siteName' => ['required', 'string', 'min:3', 'max:190'],
            'globalPercent' => ['sometimes', 'nullable', 'between:0.1,50'],
            'isGlobalPercent' => ['required', 'boolean'],
            'supportEmail' => ['required', 'email'],
            'supportTelegram' => ['required', 'string'],
            'newPassword' => ['nullable', 'string', 'min:6', 'max:190'],
            'telegramChatId' => ['sometimes', 'nullable', 'numeric']

        ];
    }

    public function save()
    {
        $this->validate();
        Setting::set('site_name', $this->siteName);
        Setting::set('is_global_percent', (bool) $this->isGlobalPercent);
        Setting::set('global_percent', $this->globalPercent);
        Setting::set('support_email', $this->supportEmail);
        Setting::set('support_telegram', $this->supportTelegram);
        Setting::set('telegram_chat_id', $this->telegramChatId);
        if($this->newPassword) {
            auth()->user()->update(['password' => Hash::make($this->newPassword)]);
        }

        $this->showSwalToast('success', 'Сохранено');
    }

    public function render()
    {
        return view('livewire.admin.settings')
            ->extends('admin.layouts.main')
            ->section('content');
    }
}
