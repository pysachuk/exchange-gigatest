<?php

namespace App\Http\Livewire\Admin;

use App\Http\Livewire\Traits\HasSwal;
use App\Models\ExchangeApplication;
use Livewire\Component;

class ExchangeApplicationView extends Component
{
    use HasSwal;
    public $exchangeApplication;

    protected $listeners = [
        'setStatus', 'deleteOrder'
    ];

    public function deleteConfirmation($id)
    {
        $this->showSwalConfirm('error', 'Вы уверены?', 'deleteOrder', $id);
    }

    public function deleteOrder(ExchangeApplication $order)
    {
        $order->delete();
        return redirect()->route('admin.orders.list');
    }

    public function mount($id)
    {
        $this->exchangeApplication = ExchangeApplication::findOrFail($id);
    }

    public function setStatusConfirmation($status)
    {
        $this->showSwalConfirm('error', 'Вы уверены?', 'setStatus', $status);
    }

    public function setStatus($status)
    {
        $this->exchangeApplication->update(['status' => $status]);
        $this->exchangeApplication->refresh();
    }

    public function render()
    {
        return view('livewire.admin.exchange-application-view')
            ->extends('admin.layouts.main')
            ->section('content');
    }
}
