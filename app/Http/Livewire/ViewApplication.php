<?php

namespace App\Http\Livewire;

use App\Models\ExchangeApplication;
use Livewire\Component;

class ViewApplication extends Component
{
    public $exchangeApplication;

    protected $listeners = ['timeFinish'];


    public function mount($uuid)
    {
        $this->exchangeApplication = ExchangeApplication::whereUuid($uuid)->first();
        if($this->exchangeApplication->status === ExchangeApplication::STATUS_NEW && !$this->exchangeApplication->isActual ) {
            $this->setStatusCancelled($this->exchangeApplication);
        }
    }

    public function setStatusInspection()
    {
        $this->exchangeApplication->update(['status' => ExchangeApplication::STATUS_INSPECTION]);
        $this->exchangeApplication->refresh();
    }

    public function cancelOrder()
    {
        $this->exchangeApplication->update(['status' => ExchangeApplication::STATUS_CANCELLED]);
        $this->exchangeApplication->refresh();
    }

    protected function setStatusCancelled(ExchangeApplication $application)
    {
        $application->update(['status' => ExchangeApplication::STATUS_CANCELLED]);
        $this->exchangeApplication->refresh();
    }


    public function timeFinish(ExchangeApplication $exchangeApplication)
    {
        $this->setStatusCancelled($exchangeApplication);
    }


    public function render()
    {
        return view('livewire.view-application')->extends('layouts.main')->section('content');
    }
}
