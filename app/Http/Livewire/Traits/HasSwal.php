<?php

namespace App\Http\Livewire\Traits;

trait HasSwal
{
    public function showSwalToast($type, $message, $title = null)
    {
        $this->dispatchBrowserEvent('swal:toast', [
            'type' => $type,
            'text' => $message,
            'title' => $title
        ]);
    }

    public function showSwalModal($type, $message, $title = null)
    {
        $this->dispatchBrowserEvent('swal:modal', [
            'type' => $type,
            'text' => $message,
            'title' => $title
        ]);
    }

    public function showSwalConfirm($type, $message, $emit = null, $id = null, $title = null)
    {
        $this->dispatchBrowserEvent('swal:confirm', [
            'type' => $type,
            'title' => $title,
            'text' => $message,
            'emit' => $emit,
            'id' => $id
        ]);
    }
}
