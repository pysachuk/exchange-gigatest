<?php

namespace App\Http\Livewire;

use Livewire\Component;

class LastTransactions extends Component
{
    public function render()
    {
        return view('livewire.last-transactions');
    }
}
