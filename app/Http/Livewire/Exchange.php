<?php

namespace App\Http\Livewire;

use App\Http\Livewire\Traits\HasSwal;
use App\Models\AvailableCoin;
use App\Models\ExchangeApplication;
use App\Models\Gap;
use App\Models\Setting;
use App\Models\User;
use App\Services\CoinGeckoService;
use Codenixsv\CoinGeckoApi\CoinGeckoClient;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class Exchange extends Component
{
    use HasSwal;

    public $availableCoinsFrom;
    public $availableCoinsTo;

    public $coinFrom;
    public $coinTo;

    protected $percent = 1;

    public $fromData = [];
    public $toData = [];

    public function mount()
    {
        $this->init();
    }

    public function refreshAvailableCoins()
    {
        $this->availableCoinsFrom = $this->getAvailableFromCoins();
        $this->availableCoinsTo = $this->getAvailableToCoins();
    }


    protected function init()
    {
        if(! $this->coinFrom && ! $this->coinTo) {
            $this->coinFrom = AvailableCoin::first();
            $this->fromData['amountFrom'] = $this->coinFrom->min;
            $this->coinTo = AvailableCoin::latest()->first();
            $this->toData['amountTo'] = $this->coinTo->min;
            $this->fromData['amountFromUsd'] = round($this->fromData['amountFrom'] * $this->coinFrom->usd_price, 2);
        }

        if( $this->coinFrom->id === $this->coinTo->id) {
            $this->coinTo = AvailableCoin::whereNotIn('id', [$this->coinFrom->id])->first();
            $this->showSwalToast('error', 'Duplicate currency');
        }
        $this->refreshAvailableCoins();

        $this->calculateFrom();
    }

    public function updatedFromDataAmountFromUsd()
    {
        if($this->fromData['amountFromUsd'] > 0) {
            $this->fromData['amountFrom'] = round($this->fromData['amountFromUsd'] / $this->coinFrom->usd_price, 7);
            $this->updatedFromDataAmountFrom();
        } else {
            $this->fromData['amountFrom'] = $this->coinFrom->min;
            $this->updatedFromDataAmountFrom();
        }
    }

    protected function getRules()
    {
        return [
            'fromData.email' => ['required', 'email'],
            'fromData.amountFrom' => ['required', 'numeric', "between:{$this->coinFrom->min},{$this->coinFrom->max}"],
            'toData.amountTo' => ['required', 'numeric'],
            'toData.addressTo' => ['required', 'string'],
            'coinFrom' => ['required'],
            'coinTo' => ['required']

        ];
    }

    protected function getMessages()
    {
        return [
            'fromData.email.required' => 'Email is Required!',
            'fromData.email.email' => 'Email not valid!',
            'fromData.amountFrom.required' => 'Amount is required',
            'fromData.amountFrom.between' => 'Amount is invalid',
            'toData.addressTo.required' => 'Receive address is required!',
            'toData.addressTo.string' => 'Receive address is invalid!',
            'fromData.amountTO' => 'Amount not valid',
            'coinFrom' => 'Coin not selected',
            'coinTo' => 'Coin not selected',
        ];
    }

    public function selectCoinFrom(AvailableCoin $coin)
    {
        $this->coinFrom = $coin;
        if(isset($this->fromData['amountFrom']) && $this->fromData['amountFrom'] < $this->coinFrom->min) {
            $this->fromData['amountFrom'] = $this->coinFrom->min;
        }
        $this->fromData['amountFromUsd'] = round($this->fromData['amountFrom'] * $this->coinFrom->usd_price, 2);
//        if(! isset($this->fromData['amount_from']) || !$this->fromData['amount_from'] || ! isset($this->toData['amount_to'])  || ! $this->toData['amount_to']) {
//            dd(123);
//            $this->fromData['amountFrom'] = $this->coinFrom->min;
//            $this->toData['amountTo'] = $this->coinTo->min;
//        }
        $this->clearValidation();
        $this->init();
    }

    public function selectCoinTo(AvailableCoin $coin)
    {
        $this->coinTo = $coin;
        if(isset($this->fromData['amountFrom']) && $this->fromData['amountFrom'] < $this->coinFrom->min) {
            $this->fromData['amountFrom'] = $this->coinFrom->min;
        }
//        if(! isset($this->fromData['amount_from']) || !$this->fromData['amount_from'] || ! isset($this->toData['amount_to'])  || ! $this->toData['amount_to']) {
//            $this->fromData['amountFrom'] = $this->coinFrom->min;
//            $this->toData['amountTo'] = $this->coinTo->min;
//        }
        $this->clearValidation();
        $this->init();
    }

    public function updatedFromDataAmountFrom()
    {
        if($this->fromData['amountFrom'] > 0) {
            $this->fromData['amountFromUsd'] = round($this->fromData['amountFrom'] * $this->coinFrom->usd_price, 2);
            $this->calculateFrom();
        } else {
            $this->toData['amountTo'] = 0;
            $this->fromData['amountFromUsd'] = 0;
        }

    }

    public function updatedToDataAmountTo()
    {
        if($this->toData['amountTo'] > 0) {
            $this->calculateTo();
        } else {
            $this->fromData['amountFrom'] = 0;
        }

    }

    protected function calculateFrom()
    {
        $priceOneCoinFrom = $this->coinFrom->usd_price;
        $priceOneCoinTo = $this->coinTo->usd_price;

        $gapPercent = $this->getGapPercent($this->coinFrom, $this->coinTo);
        if($gapPercent) {
            $price = ($this->fromData['amountFrom'] * $priceOneCoinFrom) / ($priceOneCoinTo - (($priceOneCoinTo / 100) * $gapPercent));
        } else {
            $price = ($this->fromData['amountFrom'] * $priceOneCoinFrom) / $priceOneCoinTo;
        }

//            $scumPrice = ($bitcoinToExchange * $priceOneBtc) / ($priceOneEth - (( $priceOneEth / 100) * $percent));

        $this->toData['amountTo'] = round($price, 7);
    }

    protected function getGapPercent(AvailableCoin $coinFrom, AvailableCoin $coinTo)
    {
        if(Setting::get('is_global_percent')) {
            return (float) Setting::get('global_percent');
        }
        $gap = Gap::where('coin_from_id', $coinFrom->id)->where('coin_to_id', $coinTo->id)->first();

        if($gap) {
            return $gap->percent;
        }

        return null;
    }

    protected function calculateTo()
    {

        $priceOneCoinFrom = $this->coinFrom->usd_price;
        $priceOneCoinTo = $this->coinTo->usd_price;

        $gapPercent = $this->getGapPercent($this->coinFrom, $this->coinTo);
        if($gapPercent) {
            $price = ($this->toData['amountTo'] * $priceOneCoinTo) / $priceOneCoinFrom;
            $price = $price - (($price / 100) * $gapPercent);
        } else {
            $price = ($this->toData['amountTo'] * $priceOneCoinTo) / $priceOneCoinFrom;
        }

        $this->fromData['amountFrom'] = round($price, 7);

    }

    protected function getAvailableFromCoins()
    {
        return AvailableCoin::get();
    }

    protected function getAvailableToCoins()
    {
        return AvailableCoin::get();
    }

    public function exchange()
    {
        $data = $this->validate();

        $uuid = \Str::uuid()->toString();

        $result = ExchangeApplication::create([
            'user_id' => auth()->id(),
            'uuid' => $uuid,
            'from_coin_id' => $data['coinFrom']['id'],
            'to_coin_id' => $data['coinTo']['id'],
            'amount_from' => $data['fromData']['amountFrom'],
            'amount_to' => $data['toData']['amountTo'],
            'wallet_to' => $data['toData']['addressTo'],
            'status' => ExchangeApplication::STATUS_NEW
        ]);
        if($result) {
            return redirect()->route('order.view', ['uuid' => $uuid]);
        } else {
            $this->showSwalToast('error', 'Error');
        }


    }

    public function render()
    {
        return view('livewire.exchange')->extends('layouts.main')->section('content');
    }
}
