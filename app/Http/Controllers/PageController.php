<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\Question;
use Illuminate\Http\Request;

class PageController extends Controller
{
    protected function getPage($key)
    {
        return Page::where('key', $key)->firstOrFail();
    }

    public function support()
    {
        return view('pages.support');
    }

    public function reserve()
    {
        return view('pages.reserve');
    }

    public function cryptoCheck()
    {
        $page = $this->getPage('crypto-check');

        return view($page->view, compact('page'));
    }

    public function partner()
    {
        $page = $this->getPage('partner');

        return view($page->view, compact('page'));
    }

    public function faq()
    {
        $questions = Question::get();
        return view('pages.faq', compact('questions'));
    }

    public function history()
    {
        $orders = auth()->user()->orders;
        return view('pages.history', compact('orders'));
    }

    public function privacy()
    {
        $page = $this->getPage('privacy');

        return view($page->view, compact('page'));
    }

    public function terms()
    {
        $page = $this->getPage('terms');

        return view($page->view, compact('page'));
    }
}
