<?php

namespace App\Http\Controllers\Admin\Pages;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UpdatePageRequest;
use App\Models\Page;

class EditPageController extends Controller
{
    public function edit($key)
    {
        $page = Page::with('translations')->where('key', $key)->firstOrFail();

        return view('admin.pages.edit', compact('page', 'key'));
    }

    public function store($key, UpdatePageRequest $request)
    {
        $data = $request->validated();

        $page = Page::where('key', $key)->firstOrFail();

        $page->update($data);

        return redirect()->back();
    }
}
