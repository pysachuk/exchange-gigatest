<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\FaqRequest;
use App\Models\Question;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public function list()
    {
        $questions = Question::with('translations')->get();

        return view('admin.faq.list', compact('questions'));

    }

    public function update(Question $question, FaqRequest $request)
    {
        $question->update($request->validated());

        return redirect()->route('admin.faq.list');
    }

    public function create()
    {
        return view('admin.faq.create');
    }

    public function store(FaqRequest $request)
    {
        Question::create($request->validated());

        return redirect()->route('admin.faq.list');
    }

    public function edit(Question $question)
    {
        return view('admin.faq.edit', compact('question'));
    }

    public function delete(Question $question)
    {
        $question->translations()->delete();
        $question->delete();

        return redirect()->route('admin.faq.list');

    }
}
