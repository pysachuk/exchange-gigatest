<?php

namespace App\Http\Controllers;

use App\Models\AvailableCoin;
use App\Services\QrCodeService;
use BaconQrCode\Renderer\Image\EpsImageBackEnd;
use BaconQrCode\Renderer\Image\SvgImageBackEnd;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\Image\ImagickImageBackEnd;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;

class MainController extends Controller
{
    public function index()
    {
        return view('exchange');
    }
}
