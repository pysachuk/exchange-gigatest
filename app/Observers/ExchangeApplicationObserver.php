<?php

namespace App\Observers;

use App\Models\ExchangeApplication;
use App\Services\TelegramService;

class ExchangeApplicationObserver
{
    /**
     * Handle the ExchangeApplication "created" event.
     *
     * @param  \App\Models\ExchangeApplication  $exchangeApplication
     * @return void
     */
    public function created(ExchangeApplication $exchangeApplication)
    {
        $telegramService = resolve(TelegramService::class);
        $telegramService->newOrderNotification($exchangeApplication);
    }

    /**
     * Handle the ExchangeApplication "updated" event.
     *
     * @param  \App\Models\ExchangeApplication  $exchangeApplication
     * @return void
     */
    public function updated(ExchangeApplication $exchangeApplication)
    {
        //
    }

    /**
     * Handle the ExchangeApplication "deleted" event.
     *
     * @param  \App\Models\ExchangeApplication  $exchangeApplication
     * @return void
     */
    public function deleted(ExchangeApplication $exchangeApplication)
    {
        //
    }

    /**
     * Handle the ExchangeApplication "restored" event.
     *
     * @param  \App\Models\ExchangeApplication  $exchangeApplication
     * @return void
     */
    public function restored(ExchangeApplication $exchangeApplication)
    {
        //
    }

    /**
     * Handle the ExchangeApplication "force deleted" event.
     *
     * @param  \App\Models\ExchangeApplication  $exchangeApplication
     * @return void
     */
    public function forceDeleted(ExchangeApplication $exchangeApplication)
    {
        //
    }
}
