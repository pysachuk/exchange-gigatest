<?php

namespace App\Observers;

use App\Models\AvailableCoin;
use App\Services\QrCodeService;

class AvailableCoinObserver
{
    /**
     * Handle the AvailableCoin "created" event.
     *
     * @param  \App\Models\AvailableCoin  $availableCoin
     * @return void
     */

    public $qrCodeService;

    public function __construct(QrCodeService $qrCodeService)
    {
        $this->qrCodeService = $qrCodeService;
    }

    public function created(AvailableCoin $availableCoin)
    {
        $this->qrCodeService
            ->createQrCode($availableCoin->wallet, 'storage/qr_codes/', $availableCoin->wallet);
    }

    /**
     * Handle the AvailableCoin "updated" event.
     *
     * @param  \App\Models\AvailableCoin  $availableCoin
     * @return void
     */
    public function updating(AvailableCoin $availableCoin)
    {
        if($availableCoin->isDirty('wallet')) {
            $this->qrCodeService
                ->createQrCode($availableCoin->wallet, 'storage/qr_codes/', $availableCoin->wallet);
        }

    }

    /**
     * Handle the AvailableCoin "deleted" event.
     *
     * @param  \App\Models\AvailableCoin  $availableCoin
     * @return void
     */
    public function deleted(AvailableCoin $availableCoin)
    {
        //
    }

    /**
     * Handle the AvailableCoin "restored" event.
     *
     * @param  \App\Models\AvailableCoin  $availableCoin
     * @return void
     */
    public function restored(AvailableCoin $availableCoin)
    {
        //
    }

    /**
     * Handle the AvailableCoin "force deleted" event.
     *
     * @param  \App\Models\AvailableCoin  $availableCoin
     * @return void
     */
    public function forceDeleted(AvailableCoin $availableCoin)
    {
        //
    }
}
